#include "QilexConfig.hxx"
#include "QilexFakeDNS.hxx"

int QilexConfig::sampleRate = 0; // initial value

int QilexConfig::stunServerPort = 3478;
int QilexConfig::turnServerPort = 5349;

int QilexConfig::reRegisterTime = 300;
int QilexConfig::presencePublicationExpire = 300;
int QilexConfig::presenceSubscriptionExpire = 3000;
