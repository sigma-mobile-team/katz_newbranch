// Copyright (c) 2012 Sirocco Mobile Sp. z o.o.

#include "QilexCallHandler.hxx"
#include "QilexLogger.hxx"
#include "QilexConfig.hxx"
#include "QilexCallManager.hxx"
#include "QilexSdp.hxx"
#include "QilexRTP.hxx"
#include "QilexConfig.hxx"
#include "QilexSdpSecureNull.hxx"
#include "QilexSdpSecureImpl.hxx"
#include "QilexFakeDNS.hxx"
#include "AudioMgr.h"
#include <rutil/DnsUtil.hxx>
#include <resip/dum/ServerInviteSession.hxx>
#include <resip/dum/ClientInviteSession.hxx>
#include <resip/dum/AppDialogSet.hxx>
#include <sstream>

using namespace resip;

QilexCallHandler::QilexCallHandler() {
}

/// called when an initial INVITE or the intial response to an outoing invite  
void QilexCallHandler::onNewSession(ClientInviteSessionHandle handle,
		InviteSession::OfferAnswerType oat, const SipMessage& msg) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onNewSession");
	QilexCallManager& manager = QilexCallManager::instance();

	manager.addClientInviteSessionHandle(handle);
	mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLNEWSESSION);
	mInterruptingCall = false;
}

void QilexCallHandler::onNewSession(ServerInviteSessionHandle handle,
		InviteSession::OfferAnswerType oat, const SipMessage& msg) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onNewSession2");

	QilexCallManager& manager = QilexCallManager::instance();

	if (manager.isActiveCall()) {
		if (handle.isValid()) {
			mInterruptingCall = true;
			handle->reject(486); //486 == BUSY HERE
			handle->getAppDialogSet()->end();
			mStatemachine.logMissCall(msg.header(h_From).uri().toString().c_str());
		}
		return;
	}

	manager.rtp().prepareStreamTransport();

	mInterruptingCall = false;
	QilexCallManager::instance().setServerInviteSessionHandle(handle);
	mStatemachine.setPeerProfileUri(
			msg.header(h_From).uri().toString().c_str());
}

/// Received a failure response from UAS
void QilexCallHandler::onFailure(ClientInviteSessionHandle,
		const SipMessage& msg) {

	int nCode = msg.header(resip::h_StatusLine).responseCode();
	SIPXLOGI("QilexCallHandler::onFailure: nCode = %d",nCode);
	//busy or rejected
	switch (nCode) {
	case 480:
	case 486:
	case 600:
	case 603:
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLREMOTEEND);
		break;
	case 408:
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLTIMEOUT);
		break;
	case 404:
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLNOTEXIST);
		break;
	case 402:
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLOUTOFPAYMENT);
		break;
	case 403:
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLINVALID);
		break;
	default:
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLFAILED);
		break;
	}

}

/// called when an in-dialog provisional response is received that contains a body
void QilexCallHandler::onEarlyMedia(ClientInviteSessionHandle,
		const SipMessage&, const SdpContents& sdp) {

	SIPXLOGI("QilexCallHandler::onEarlyMedia");

	std::string calleIp, calleRtcpIp;
	unsigned int callePort, calleRtcpPort;

	QilexCallManager& callManager = QilexCallManager::instance();

	callManager.rtp().getStreamTransportSecure();

	SdpContents::Session::MediumContainer::const_iterator mediaIter;
	QilexSdp::getConnectionParams(sdp.session(), calleIp, callePort,
			calleRtcpIp, calleRtcpPort, mediaIter);
	callManager.rtp().secure().setConnectionCipher(mediaIter);
	callManager.rtp().setPeer(calleIp.c_str(), callePort, calleRtcpIp.c_str(),
			calleRtcpPort);

	SDPCodecDescriptor codecDescriptor;
	QilexSdp::setupCodec(sdp.session(), codecDescriptor);
	callManager.rtp().setupCodec(codecDescriptor);

	SIPXLOGI("QilexCallHandler::onEarlyMedia/");
}


/// called when dialog enters the Early state - typically after getting 18x
void QilexCallHandler::onProvisional(ClientInviteSessionHandle,
		const SipMessage&) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onProvisional");
	mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLRINGING);
}

/// called when a dialog initiated as a UAC enters the connected state
void QilexCallHandler::onConnected(ClientInviteSessionHandle,
		const SipMessage& msg) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onConnected");
	mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLANSWERED);
}

/// called when a dialog initiated as a UAS enters the connected state
void QilexCallHandler::onConnected(InviteSessionHandle, const SipMessage& msg) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onConnected2");
	mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLANSWERED);
}

/// called when ACK (with out an answer) is received for initial invite (UAS)
void QilexCallHandler::onConnectedConfirmed(InviteSessionHandle,
		const SipMessage &msg) // parent call?
		{
	LOGI("SIGMA-PHUCPV QilexCallHandler onConnectedConfirmed");
}

/// default behaviour is to send a BYE to end the dialog
void QilexCallHandler::onAckNotReceived(InviteSessionHandle handle) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onAckNotReceived");
	SIPXLOGE("QilexCallHandler::onAckNotReceived: Connection NOT confirmed");
	//handle->end(resip::InviteSession::AckNotReceived); //default behaviour
}

/** UAC gets no final response within the stale call timeout (default is 3
 * minutes). This is just a notification. After the notification is
 * called, the InviteSession will then call
 * InviteSessionHandler::terminate() */
void QilexCallHandler::onStaleCallTimeout(ClientInviteSessionHandle h) // parent call?
		{
}

/** called when an early dialog decides it wants to terminate the
 * dialog. Default behavior is to CANCEL all related early dialogs as
 * well.  */
void QilexCallHandler::terminate(ClientInviteSessionHandle h) // parent call?
		{
	LOGI("SIGMA-PHUCPV QilexCallHandler terminate");
}

/// called when an dialog enters the terminated state - this can happen
/// after getting a BYE, Cancel, or 4xx,5xx,6xx response - or the session
/// times out
void QilexCallHandler::onTerminated(InviteSessionHandle,
		InviteSessionHandler::TerminatedReason reason,
		const SipMessage* related) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onTerminated");
	QilexCallManager& callManager = QilexCallManager::instance();

	if (mInterruptingCall) {
		mInterruptingCall = false;
		return;
	}

	SIPXLOGI("QilexCallHandler::onTerminated");
	try {
		callManager.rtp().terminate();
	} catch (...) {
		SIPXLOGE("AudioMgr stop error");
	}

	switch (reason) {
	case Timeout:
		if (reason == Timeout)
			SIPXLOGE("    => reason: TIMEOUT");
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLTIMEOUT);
		break;
	case Error:
		if (reason == Error)
			SIPXLOGE(" => reason: ERROR");
	case Replaced:
		if (reason == Replaced)
			SIPXLOGE(" => reason: REPLACED");
	case LocalBye:
		if (reason == LocalBye)
			SIPXLOGE(" => reason: LOCAL_BYE");
	case LocalCancel:
		if (reason == LocalCancel)
			SIPXLOGE(" => reason: LOCAL_CANCEL");
	case Rejected: //Only as UAS, UAC has distinct onFailure callback
		if (reason == Rejected)
			SIPXLOGE(" => reason: REJECT");
	case Referred:
		if (reason == Referred)
			SIPXLOGE(" => reason: REFERED");
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLEND);
		break;
	case RemoteCancel:
		if (reason == RemoteCancel)
					SIPXLOGE(" => reason: RemoteCancel");
	case RemoteBye:
		if (reason == RemoteBye)
							SIPXLOGE(" => reason: RemoteBye");
		mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLREMOTEEND);
		break;
	case PaymentRequired:
	       SIPXLOGE("QilexCallHandler::onTerminated PAYMENT REQUIRED");
	       mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLOUTOFPAYMENT);
		break;
	default:
		SIPXLOGI("QilexCallHandler::onTerminated assert");
		assert(0);
	}

	SIPXLOGI("QilexCallHandler::onTerminated/");
}

/// called when a fork that was created through a 1xx never receives a 2xx
/// because another fork answered and this fork was canceled by a proxy. 
void QilexCallHandler::onForkDestroyed(ClientInviteSessionHandle) {
}

/// called when a 3xx with valid targets is encountered in an early dialog     
/// This is different then getting a 3xx in onTerminated, as another
/// request will be attempted, so the DialogSet will not be destroyed.
/// Basically an onTermintated that conveys more information.
/// checking for 3xx respones in onTerminated will not work as there may
/// be no valid targets.
void QilexCallHandler::onRedirected(ClientInviteSessionHandle,
		const SipMessage& msg) {
}

/// called to allow app to adorn a message. default is to send immediately
void QilexCallHandler::onReadyToSend(InviteSessionHandle, SipMessage& msg) // parent call?
		{
}

/// called when an answer is received - has nothing to do with user
/// answering the call 
void QilexCallHandler::onAnswer(InviteSessionHandle, const SipMessage& msg,
		const SdpContents& sdp) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onAnswer");
	std::string calleIp;
	unsigned int callePort;
	std::string calleRtcpIp;
	unsigned int calleRtcpPort;

	QilexCallManager& callManager = QilexCallManager::instance();

	callManager.rtp().getStreamTransportSecure();

	SdpContents::Session::MediumContainer::const_iterator mediaIter;
	QilexSdp::getConnectionParams(sdp.session(), calleIp, callePort,
			calleRtcpIp, calleRtcpPort, mediaIter);
	callManager.rtp().secure().setConnectionCipher(mediaIter);
	callManager.rtp().setPeer(calleIp.c_str(), callePort, calleRtcpIp.c_str(),
			calleRtcpPort);

	SDPCodecDescriptor codecDescriptor;
	QilexSdp::setupCodec(sdp.session(), codecDescriptor);
	callManager.rtp().setupCodec(codecDescriptor);
}

/// called when an offer is received - must send an answer soon after this
void QilexCallHandler::onOffer(InviteSessionHandle handle,
		const SipMessage& msg, const SdpContents& sdp) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onOffer");
	QilexCallManager& callManager = QilexCallManager::instance();

	bool suspend = QilexSdp::checkForSuspend(sdp.session());
	if (suspend) {
		//TODO: suspend
		//stop processing
		//stop stream
	}

	string calleIp;
	unsigned int callePort = 0;
	string calleRtcpIp;
	unsigned int calleRtcpPort = 0;

	SIPXLOGI("QilexCallHandler::onOffer before get stream transport secure");
	callManager.rtp().getStreamTransportSecure();

	SIPXLOGI("QilexCallHandler::onOffer before get connection params");
	SdpContents::Session::MediumContainer::const_iterator mediaIter;
	QilexSdp::getConnectionParams(sdp.session(), calleIp, callePort,
			calleRtcpIp, calleRtcpPort, mediaIter);

	QilexFakeDNS::SetPreferred(DEFAULT_TURN_HOST, calleIp);

	string strCandidateIP;
	unsigned short nCandidatePort = 0;

	bool bICEsupported = false;
	bool bICEmismatch = false;

	SIPXLOGI("QilexCallHandler::onOffer checking for connection candidates");
	QilexSdp::checkForCandidates(sdp.session(), strCandidateIP, nCandidatePort,
			bICEmismatch, bICEsupported);
	SIPXLOGI("QilexCallHandler::onOffer candidate: %s:%d",
			strCandidateIP.c_str(), nCandidatePort);

	bool bNoRelayed = QilexSdp::checkForNoRelayed(sdp.session());

	SIPXLOGI("QilexCallHandler::onOffer before make offer");
	SdpContents contents;
	bool bRelayed;
	bool bEnableRtcp = true;
	QilexSdp::makeSdpOffer(contents, callManager.rtp().net(), false,
			bICEsupported, bICEmismatch, bNoRelayed, callManager.rtp().secure(),
			bRelayed, bEnableRtcp);

	SdpContents::Session::Medium sdpMedium = contents.session().media().front();
	contents.session().clearMedium();

	contents.session().origin().getSessionId() =
			sdp.session().origin().getSessionId();
	contents.session().origin().getVersion() =
			sdp.session().origin().getVersion();

	SDPCodecDescriptor codecDescriptor;
	QilexSdp::matchCodecFromOffer(sdp.session(), contents.session(), sdpMedium,
			codecDescriptor, callManager.rtp().secure());

	SIPXLOGI("QilexCallHandler::onOffer before setup codec");
	callManager.rtp().setupCodec(codecDescriptor);

	SIPXLOGI("QilexCallHandler::onOffer before provide answer");
	callManager.serverInviteSessionHandle()->provideAnswer(contents);

	callManager.rtp().secure().setConnectionCipher(mediaIter);

	SIPXLOGI("QilexCallHandler::onOffer before set peer");
	callManager.rtp().setPeer(calleIp.c_str(), callePort, calleRtcpIp.c_str(),
			calleRtcpPort);

	SIPXLOGI("QilexCallHandler::onOffer %s", calleIp.c_str());

	mStatemachine.sendEvent(QilexStatemachine::EVENT_CALLNEWINCOMING);

	callManager.serverInviteSessionHandle()->provisional(180, true);
}

/// called when a modified body is received in a 2xx response to a
/// session-timer reINVITE. Under normal circumstances where the response
/// body is unchanged from current remote body no handler is called
void QilexCallHandler::onRemoteSdpChanged(InviteSessionHandle,
		const SipMessage& msg, const SdpContents&) // parent call?
		{
}
// You should only override the following method if genericOfferAnswer is true
void QilexCallHandler::onRemoteAnswerChanged(InviteSessionHandle,
		const SipMessage& msg, const Contents&)  // parent call?
		{
}

/// Called when an error response is received for a reinvite-nobody request (via requestOffer)
void QilexCallHandler::onOfferRequestRejected(InviteSessionHandle,
		const SipMessage& msg) // parent call?
		{
}

/// called when an Invite w/out offer is sent, or any other context which
/// requires an offer from the user
void QilexCallHandler::onOfferRequired(InviteSessionHandle handle,
		const SipMessage& msg) {
	LOGI("SIGMA-PHUCPV QilexCallHandler onOfferRequired");
	SdpContents contents;
	QilexCallManager& callManager = QilexCallManager::instance();
	bool bRelayed;
	QilexSdp::makeSdpOffer(contents, callManager.rtp().net(), false, false,
			false, false, callManager.rtp().secure(), bRelayed,callManager.isOUT());
	handle->provideOffer(contents);
}

/// called if an offer in a UPDATE or re-INVITE was rejected - not real
/// useful. A SipMessage is provided if one is available
void QilexCallHandler::onOfferRejected(InviteSessionHandle,
		const SipMessage* msg) {
}

/// called when INFO message is received 
void QilexCallHandler::onInfo(InviteSessionHandle, const SipMessage& msg) {
}

/// called when response to INFO message is received 
void QilexCallHandler::onInfoSuccess(InviteSessionHandle,
		const SipMessage& msg) {
}
void QilexCallHandler::onInfoFailure(InviteSessionHandle,
		const SipMessage& msg) {
}

/// called when MESSAGE message is received 
void QilexCallHandler::onMessage(InviteSessionHandle, const SipMessage& msg) {
}

/// called when response to MESSAGE message is received 
void QilexCallHandler::onMessageSuccess(InviteSessionHandle,
		const SipMessage& msg) {
}
void QilexCallHandler::onMessageFailure(InviteSessionHandle,
		const SipMessage& msg) {
}

/// called when an REFER message is received.  The refer is accepted or
/// rejected using the server subscription. If the offer is accepted,
/// DialogUsageManager::makeInviteSessionFromRefer can be used to create an
/// InviteSession that will send notify messages using the ServerSubscription
void QilexCallHandler::onRefer(InviteSessionHandle, ServerSubscriptionHandle,
		const SipMessage& msg) {
}

void QilexCallHandler::onReferNoSub(InviteSessionHandle,
		const SipMessage& msg) {
}

/// called when an REFER message receives a failure response 
void QilexCallHandler::onReferRejected(InviteSessionHandle,
		const SipMessage& msg) {
}

/// called when an REFER message receives an accepted response 
void QilexCallHandler::onReferAccepted(InviteSessionHandle,
		ClientSubscriptionHandle, const SipMessage& msg) {
}
