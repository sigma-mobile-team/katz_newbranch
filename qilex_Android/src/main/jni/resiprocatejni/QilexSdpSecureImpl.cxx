#include "QilexSdpSecureImpl.hxx"

#include <sstream>
#include "rutil/Data.hxx"
#include <list>
#include "QilexLogger.hxx"
#include "IStreamTransport.h"

// TODO - refactoring to avoid hardcoding of supported encoding policies list in multiple places

QilexSdpSecureImpl::QilexSdpSecureImpl()
{
	nTag = 0;
	pSSRC = 0;
	inSSRC = 0;
}

void QilexSdpSecureImpl::generateKey(SdpContents::Session::Medium & sdpMedium)
{
	m_mSrtpOutKeys.clear();

	generateKeyPriv(sdpMedium, "AES_CM_128_NULL_AUTH", 1);
	generateKeyPriv(sdpMedium, "AES_CM_128_HMAC_SHA1_32", 2);
	generateKeyPriv(sdpMedium, "AES_CM_128_HMAC_SHA1_80", 3);
}

void QilexSdpSecureImpl::generateKeyPriv(SdpContents::Session::Medium & sdpMedium, std::string keyname, unsigned char no)
{
	std::string szKeyBase64;
	std::stringstream ssSRTP;
	char cKey[30]; //240 bits total (128 key + 112 salt) = 30 bytes;

	for (int i=0; i<30; ++i)
		cKey[i] = rand()%256;
	m_mSrtpOutKeys[no] = Data(cKey, 30);

	szKeyBase64 = Data(cKey, 30).base64encode().c_str();
	ssSRTP << (int)no << " " <<  keyname << " inline:" << szKeyBase64;

	sdpMedium.addAttribute("crypto", ssSRTP.str().c_str());

	SRTPLOGI("QilexSdpSecureImpl::generateKey %s", ssSRTP.str().c_str());
}

bool QilexSdpSecureImpl::preprateCipherData(SdpContents::Session::MediumContainer::const_iterator & iter, std::string & strCipherData)
{
	SRTPLOGI("QilexSdpSecureImpl::preprateCipherData");

	lCrypto = (*iter).getValues("crypto");
	std::list<Data>::iterator cryptoIter=lCrypto.end();
	std::stringstream ssCipherData;
	if (!lCrypto.empty())
	{
		std::string strCipherType, strKeyBase64;

		for (cryptoIter=lCrypto.begin(); cryptoIter!=lCrypto.end(); ++cryptoIter)
		{
			std::istringstream ssReader((*cryptoIter).c_str());
			ssReader >> nTag >> strCipherType;

			SRTPLOGI("QilexSdpSecureImpl::preprateCipherData type = %s", strCipherType.c_str());

			std::string szKeyBase64;
			if (strCipherType == "AES_CM_128_NULL_AUTH")
			{
				std::map<unsigned char, Data>::iterator cIter = m_mSrtpOutKeys.find(1);
				if (cIter != m_mSrtpOutKeys.end())
				{
					std::string szKeyBase64 = (*cIter).second.base64encode().c_str();
					ssCipherData << strCipherType << " inline:" << szKeyBase64;
					strCipherData = ssCipherData.str();
					SRTPLOGI("QilexSdpSecureImpl::preprateCipherData %s", ssCipherData.str().c_str());
					return true;
				}
			}
			else if (strCipherType == "AES_CM_128_HMAC_SHA1_32")
			{
				std::map<unsigned char, Data>::iterator cIter = m_mSrtpOutKeys.find(2);
				if (cIter != m_mSrtpOutKeys.end())
				{
					std::string szKeyBase64 = (*cIter).second.base64encode().c_str();
					ssCipherData << strCipherType << " inline:" << szKeyBase64;
					strCipherData = ssCipherData.str();
					SRTPLOGI("QilexSdpSecureImpl::preprateCipherData %s", ssCipherData.str().c_str());
					return true;
				}
			}
			else if (strCipherType == "AES_CM_128_HMAC_SHA1_80")
			{
				std::map<unsigned char, Data>::iterator cIter = m_mSrtpOutKeys.find(3);
				if (cIter != m_mSrtpOutKeys.end())
				{
					std::string szKeyBase64 = (*cIter).second.base64encode().c_str();
					ssCipherData << strCipherType << " inline:" << szKeyBase64;
					strCipherData = ssCipherData.str();
					SRTPLOGI("QilexSdpSecureImpl::preprateCipherData %s", ssCipherData.str().c_str());
					return true;
				}
			}
		}
	}

	SRTPLOGI("QilexSdpSecureImpl::preprateCipherData no crypto");
	return false;
}

void QilexSdpSecureImpl::setCryptoInMedium(SdpContents::Session::Medium & medium, const std::string & ssCipherData)
{
	SRTPLOGI("bool QilexSdpSecureImpl::setCryptoInMedium");
	if (!lCrypto.empty())
	{
		SRTPLOGI("bool QilexSdpSecureImpl::setCryptoInMedium ok");
		std::stringstream ssSRTP;
		ssSRTP << nTag << " " << ssCipherData;
		medium.addAttribute("crypto", ssSRTP.str().c_str());
		medium.protocol() = "RTP/SAVP";
	}
}

bool QilexSdpSecureImpl::setConnectionCipher(SdpContents::Session::MediumContainer::const_iterator& mediumIter)
{
	SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher");

	lCrypto = (*mediumIter).getValues("crypto");
	if (lCrypto.empty())
	{
		SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher no crypto fileds");
		return false;
	}

	SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher %d", lCrypto.size() );
	std::string strCipherType, strKeyBase64;
	std::list<Data>::iterator iter;
	for (iter=lCrypto.begin(); iter!=lCrypto.end(); ++iter)
	{
		std::istringstream ssReader((*iter).c_str());
		ssReader >> nTag >> strCipherType >> strKeyBase64;

		SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher loop(type) %s  %s", strCipherType.c_str(), strKeyBase64.c_str());

		if (strCipherType == "AES_CM_128_NULL_AUTH" || strCipherType == "AES_CM_128_HMAC_SHA1_32" || strCipherType == "AES_CM_128_HMAC_SHA1_80")
			break;
	}
	if (iter == lCrypto.end())
	{
		SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher unsupported cipher!");
		return false; //unsupported cipher!
	}

	if (strKeyBase64.compare(0, 7, "inline:") == 0 && strKeyBase64.size() == 47)
		strKeyBase64 = strKeyBase64.substr(7, 40); //tmp - don't strip parameters - parse them
	else
	{
		SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher parsing error!");
		return false; //parsing error!
	}

	Data key = Data(strKeyBase64.c_str()).base64decode();

	srtp_t pSRTPctx;
	srtp_policy_t policyIn, policyOut;
	policyIn.ssrc.type = ssrc_any_inbound;
	policyOut.ssrc.type = ssrc_specific;
	policyOut.ssrc.value = inSSRC;

	assert(inSSRC!=0);

	policyIn.key = (unsigned char*)key.data();

	std::map<unsigned char, Data>::iterator cryptIter = m_mSrtpOutKeys.end();
	if (strCipherType == "AES_CM_128_NULL_AUTH")
		cryptIter = m_mSrtpOutKeys.find(1);
	else if (strCipherType == "AES_CM_128_HMAC_SHA1_32")
		cryptIter = m_mSrtpOutKeys.find(2);
	else if (strCipherType == "AES_CM_128_HMAC_SHA1_80")
		cryptIter = m_mSrtpOutKeys.find(3);

	if (cryptIter != m_mSrtpOutKeys.end())
	{
		policyOut.key = (unsigned char*)(*cryptIter).second.data();
	}
	else
	{
		SRTPLOGI("bool QilexSdpSecureImpl::setConnectionCipher keys error!");
		return false; //error!
	}

	policyOut.next = NULL;
	policyIn.next = &policyOut;

	if (strCipherType == "AES_CM_128_NULL_AUTH")
	{
		crypto_policy_set_aes_cm_128_null_auth(&policyOut.rtp);
		policyIn.rtp = policyIn.rtcp = policyOut.rtcp = policyOut.rtp;
	}
	else if (strCipherType == "AES_CM_128_HMAC_SHA1_32")
	{
		crypto_policy_set_aes_cm_128_hmac_sha1_32(&policyOut.rtp);
		policyIn.rtp = policyIn.rtcp = policyOut.rtcp = policyOut.rtp;
	}
	else if (strCipherType == "AES_CM_128_HMAC_SHA1_80")
	{
		crypto_policy_set_aes_cm_128_hmac_sha1_80(&policyOut.rtp);
		policyIn.rtp = policyIn.rtcp = policyOut.rtcp = policyOut.rtp;
	}
	else assert(0);

	err_status_t stat;
	if ((stat = srtp_create(&pSRTPctx, &policyIn)) == err_status_ok)
	{
		pSSRC = pSRTPctx;
		SRTPLOGI("QilexSdpSecureImpl::setConnectionCipher %p", pSSRC);
		return true;
	}
	else
		assert(0);

	return false;
}

bool QilexSdpSecureImpl::secureStreamTransport(IStreamTransport & streamtransport)
{
	SRTPLOGI("QilexSdpSecureImpl::secureStreamTransport %p", pSSRC);
	streamtransport.SetSRTPctx( pSSRC );
	return true;
}

bool QilexSdpSecureImpl::getSecureStreamTransport(IStreamTransport & streamtransport)
{
	inSSRC = streamtransport.GetSSRC();
	SRTPLOGI("QilexSdpSecureImpl::getSecureStreamTransport %d", inSSRC);
	return true;
}
