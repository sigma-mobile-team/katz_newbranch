// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_RTP_HXX
#define QILEX_RTP_HXX

#include <string>
#include <boost/shared_ptr.hpp>
#include "CodecDescriptor.h"
#include "QilexSdpSecureImpl.hxx"
#include "QilexSdpSecureNull.hxx"
#include "QilexNET.hxx"

class QilexFlowGraph;
class MpMediaTask;
class IStreamTransport;
class QilexSdpSecure;

class QilexRTP
{
public:
  QilexRTP();

  bool prepareStreamTransport();
  bool bind(const std::string &strLocalIpAddress, unsigned short nLocalPort, bool bUseTURN=false);
  bool setPeer(const char* remote_addres, int remote_port, const char* remote_rtcp_addres, int remote_rtcp_port);
  bool getStreamTransportSecure();
  void setupCodec(const SDPCodecDescriptor & codec);
  bool start();
  float audioQuality();
  bool stop();
  bool terminate();
  QilexSdpSecure& secure() { return sdpSecure; }
  QilexNET& net() { return m_net; }
  void updateAudioConfig(bool speakerOn, bool headphonesOn, bool muted);
  void updateAudioVolume(float volume);
  void gatherCandidates();
  void setToken(const std::string & token) { m_net.setToken( token ); }

protected:
  boost::shared_ptr<IStreamTransport> m_pStreamTransport;
  //Current codec Descriptor
  SDPCodecDescriptor codecDescriptor;
  QILEXSDPSECURE sdpSecure;
  QilexNET m_net;
};

#endif // QILEX_RTP_HXX
