#include "QilexHmac.hxx"
#include "QilexLogger.hxx"
#include <cassert>

#define A(c)            (c) - 0x19
#define UNHIDE_STR(str) do { char *p = str;  while (*p) *p++ += 0x19; } while (0)
#define HIDE_STR(str)   do { char *p = str;  while (*p) *p++ -= 0x19; } while (0)

#define ALGORITHM  "HmacSHA512"

JavaVM* QilexHmac::mVm = 0;
jclass QilexHmac::mMacClass = 0;
jclass QilexHmac::mSecretKeySpecClass = 0;
jstring QilexHmac::jstrAlgorithm = NULL;

QilexHmac::QilexHmac() {
}

QilexHmac::~QilexHmac() {
}

void QilexHmac::setupVm(JavaVM* vm) {
	mVm = vm;
	JNIEnv * env;
	// cache class that cant be found inside native thread because of ClassLoader issue
	mVm->GetEnv((void**) &env, JNI_VERSION_1_6);
	mMacClass = env->FindClass("javax/crypto/Mac");
	mSecretKeySpecClass = env->FindClass("javax/crypto/spec/SecretKeySpec");

	mMacClass = (jclass) env->NewGlobalRef(mMacClass);
	mSecretKeySpecClass = (jclass) env->NewGlobalRef(mSecretKeySpecClass);
	jstrAlgorithm = env->NewStringUTF(ALGORITHM);
	jstrAlgorithm = (jstring) env->NewGlobalRef(jstrAlgorithm);
}
void QilexHmac::attachThread(JNIEnv** env) {
	// TODO: sometimes we're already running jvm thread
	// therefore there's no need to atttach/detach
	QilexHmac::mVm->AttachCurrentThread(env, 0);
}

void QilexHmac::detachThread() {
	// TODO: sometimes we're already running jvm thread
	// therefore there's no need to atttach/detach
	QilexHmac::mVm->DetachCurrentThread();
}

int QilexHmac::min(int x, int y)
{
	return y ^ ((x ^ y) & -(x < y));
}

/*Function to find maximum of x and y*/
int QilexHmac::max(int x, int y)
{
	return x ^ ((x ^ y) & -(x < y));
}

char* QilexHmac::buildSecretKey(const char* number)
{
	 char str[] = {
	    A('X'), A('3'), A('t'), A('7'), A('6'),
	    A('v'), A('b'), A('7'), A('0'), A('1'), A('2'),A('3'), A('f'), A('g'), A('r'), A('9'),
		A('a'), A('s'), A('0'), A('1'), A('7'),A('5'), A('f'), A('5'), A('g'), A('8'),
		A('j'), A('k'), A('9'), A('9'), A('0'),A('u'),0
	  };
	UNHIDE_STR(str);
	const char* secKey = str;
	int j = 0;
	int secLen = strlen(secKey);
	int numberLen = strlen(number);
	int totalLeng = secLen + numberLen;
	char* result;
	result = (char*) malloc(sizeof(char) * (totalLeng + 1));
	int maxLeng = max(secLen,numberLen);
	for(int i = 0; i < maxLeng; i++)
	{
			if (i < secLen) {
				result[j] = secKey[i];
				j++;
			}
			if (i < numberLen){
				result[j] = number[i];
				j++;
			}
	 } // end for
	result[totalLeng] = '\0';
	HIDE_STR(str);
	return result;
}

jbyteArray QilexHmac::hmacSHA512(const std::string& msisdn, const std::string& msg, const char* hash)
{
	char str[] = {
		    A('v'), A('q'), A('s'), A('O'), A('+'),
		    A('X'), A('O'), A('Z'), A('F'), A('K'), A('4'),A('C'), A('4'), A('t'), A('F'), A('G'),
			A('O'), A('i'), A('B'), A('b'), A('t'),A('y'), A('d'), A('X'), A('q'), A('Y'),
			A('g'), A('='),0
		};
//    char str[] = {
//                A('/'), A('o'), A('q'), A('K'), A('Z'),
//                A('S'), A('K'), A('s'), A('x'), A('P'), A('N'),A('a'), A('f'), A('w'), A('v'), A('A'),
//                A('S'), A('/'), A('H'), A('N'), A('1'),A('t'), A('K'), A('C'), A('c'), A('L'),
//                A('U'), A('='),0
//            };
	UNHIDE_STR(str);
	if(strcmp(hash,str) != 0)
	{
		HIDE_STR(str);
		return NULL;
	}
	else
	{
		HIDE_STR(str);
	}
	JNIEnv * env;
	attachThread(&env);
	// get buildKey
	const char* number = msisdn.c_str();
	char* secretChar = buildSecretKey(number);
	int secLeng = strlen(secretChar);
	jbyteArray secret = (env)->NewByteArray(secLeng);
	env->SetByteArrayRegion(secret, 0, secLeng, reinterpret_cast<jbyte*>(secretChar));

	//init
	//secret
	jmethodID secret_constructor = env->GetMethodID(mSecretKeySpecClass, "<init>", "([BLjava/lang/String;)V");
	jobject myKey = env->NewObject(mSecretKeySpecClass, secret_constructor, secret, jstrAlgorithm);
	//mac
	jmethodID mac_instance = env->GetStaticMethodID(mMacClass, "getInstance", "(Ljava/lang/String;)Ljavax/crypto/Mac;");
	jobject mac_object = env->CallStaticObjectMethod(mMacClass, mac_instance,jstrAlgorithm);
	mac_object = env->NewGlobalRef(mac_object);
	// init mac
	jmethodID mac_init = env->GetMethodID(mMacClass, "init", "(Ljava/security/Key;)V");
	env->CallVoidMethod(mac_object,mac_init,myKey);
	// doFinal
	jmethodID do_final_id =env->GetMethodID(mMacClass, "doFinal", "([B)[B");
	jbyteArray msgData = env->NewByteArray(msg.length());
	env->SetByteArrayRegion(msgData, 0, msg.length(), (jbyte*)msg.c_str());
	jbyteArray jResult = reinterpret_cast<jbyteArray>(env->CallObjectMethod(mac_object, do_final_id, msgData));

	// function DeleteLocalRef,
	// In this very moment memory not free
	env->DeleteLocalRef(myKey);
	env->DeleteLocalRef(secret);
	env->DeleteLocalRef(msgData);
	//
	env->DeleteGlobalRef(mac_object);
	return jResult;
}
