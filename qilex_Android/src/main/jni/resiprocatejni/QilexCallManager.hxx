// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_CALL_MANAGER_HXX
#define QILEX_CALL_MANAGER_HXX

#include "QilexBaseManager.hxx"
#include <resip/dum/InviteSessionHandler.hxx>
#include "QilexRTP.hxx"
#include "CodecDescriptor.h"

#include <string>

using namespace std;

class QilexCallHandler;
class QilexIce;


/**
   Call manager
 */
class QilexCallManager : public QilexBaseManager
{

  
private:
  /// keep internal call manager status
  enum ManagerStatus
    {
      MANAGER_UNKNOWN,
      MANAGER_INVITATIONSENT,
      MANAGER_READYFORINVITATION
    };

    
    
public:
  
  static QilexCallManager& instance();

  ~QilexCallManager();

  void sendInvite(bool warnTurnNotAvailable, bool bEnableRtcp, const std::string& imsi);

  void answerCall();

  void pauseCall();

  void resumeCall();

  void rejectCall();

  bool isActiveCall();
  bool isActiveIncommingCall();
  bool isActiveOutgoingCall();

  void initAudio();
  
  void startAudio();

  float audioQuality();

  void stopAudio();
  
  void setPeerUri(const string& peerUri);

  void setOUT(bool out);

  /// server invite session handle setter (incomming call)
  void setServerInviteSessionHandle(resip::ServerInviteSessionHandle handle);
  
  /// server invite session handle getter (incomming call)
  resip::ServerInviteSessionHandle serverInviteSessionHandle();

  /// client invite session handle setter (outgoing call)
  void addClientInviteSessionHandle(resip::ClientInviteSessionHandle handle);
  void endOutgoingCall();

  void resetServerInviteSessionHandle();

  QilexIce* getIce() { return mIce.get(); }

  QilexRTP& rtp() { return mRtp; }

  bool isOUT() { return mOUT; }

  /// updates audio configuration
  void updateAudioConfig(bool speakerOn, bool headphonesOn, bool muted);
  void updateAudioVolume(float volume);

  void resetHandler();

  void setToken(const string& token) { mRtp.setToken(token); }
    
private:  
  
  QilexCallManager();

private:  // data
  
  // singleton handler
  static QilexCallManager* mInstance;
  
  // call handler
  QilexCallHandler* mCallHandler;
  
  /// invite message
  resip::SharedPtr<resip::SipMessage> mMessage;
  
  /// peer uri
  string mPeerUri;

  bool mOUT;

  /// handle for outgoing invitation
  std::set<resip::ClientInviteSessionHandle> mClientInviteSessinHandles;

  /// handle for incoming invitation
  resip::ServerInviteSessionHandle mServerInviteSessionHandle;

  /// realtime transport protocol object
  QilexRTP mRtp;

  /// ICE proto class
  std::auto_ptr<QilexIce> mIce;
};

#endif // QILEX_CALL_MANAGER_HXX
