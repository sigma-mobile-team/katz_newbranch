/*********************************/
/* QilexFakeDNS.cpp              */
/* Lukasz Rychter 2011/11/08     */
/*                               */
/* P2P DNS                       */
/*********************************/

#include "QilexFakeDNS.hxx"
#include "QilexLogger.hxx"
#include <vector>
#include <stdlib.h>

using namespace std;

multimap<string,string>		QilexFakeDNS::m_mmQilexFakeDnsList;
resip::Mutex				QilexFakeDNS::m_csAccess;

map<string, string>			QilexFakeDNS::m_mChosenIPs;
set<string>					QilexFakeDNS::m_sBlacklist;


void QilexFakeDNS::Reset()
{
	CALL_TRACK;
	m_csAccess.lock();
	m_mmQilexFakeDnsList.clear();
	m_mChosenIPs.clear();
	m_csAccess.unlock();
}

void QilexFakeDNS::AddEntry(const string& strHost, const string& strIP)
{
	LOGI("QilexFakeDNS::AddEntry, %s->%s", strHost.c_str(), strIP.c_str());
	m_csAccess.lock();
	m_mmQilexFakeDnsList.insert( pair<string,string>(strHost, strIP) );
	m_csAccess.unlock();
	LOGI("QilexFakeDNS::AddEntry END");
}
	
string QilexFakeDNS::Lookup(const string& strHost, bool bForceNew)
{
	CALL_TRACK;
	LOGI("QilexFakeDNS::Lookup %s, force new:%d", strHost.c_str(), bForceNew?1:0);
	m_csAccess.lock();

	string strRetIP;

	if (bForceNew)
		m_mChosenIPs.erase(strHost);

	//if we have not already chosen IP for this host or chosen host is blacklisted
	if (m_mChosenIPs.find(strHost) == m_mChosenIPs.end() ||						
		m_sBlacklist.find(m_mChosenIPs.find(strHost)->second) != m_sBlacklist.end())
	{
		pair<multimap<string,string>::iterator,multimap<string,string>::iterator> ret;
		ret = m_mmQilexFakeDnsList.equal_range(strHost); //find all possible IPs for that host

		vector<string> vNoBlacklisted;
		vector<string> vBlacklisted;

		for (multimap<string,string>::iterator iter=ret.first; iter!=ret.second; ++iter)
		{
			//split them into blacklisted and no-blacklisted
			if (m_sBlacklist.find(iter->second) == m_sBlacklist.end())
				vNoBlacklisted.push_back(iter->second);
			else
				vBlacklisted.push_back(iter->second);
		}

		int nIPs = vNoBlacklisted.size();
		if (nIPs > 0) 
		{
			//found no-blacklisted IP(s) for that host
			strRetIP = vNoBlacklisted[rand()%nIPs]; //takes random one from possible IPs
		}
		else if (ret.first != ret.second) 
		{
			//only blacklisted IPs found
			LOGI("QilexFakeDNS::Lookup only blacklisted IPs found");
			if (m_mChosenIPs.find(strHost) == m_mChosenIPs.end()) //if we don't have already chosen IP, chose random blacklisted
			{
				LOGI("QilexFakeDNS::Lookup if we don't have already chosen IP, chose random blacklisted");
				nIPs = vBlacklisted.size();
				strRetIP = vBlacklisted[rand()%nIPs];
			}
			else
			{
				LOGI("QilexFakeDNS::Lookup we have already chosen IP, it is blacklisted but we have only blacklisted ones. Stay with that one");
				strRetIP = m_mChosenIPs.find(strHost)->second; //we have already chosen IP, it is blacklisted but we have only blacklisted ones. Stay with that one
			}
		}

		if (!strRetIP.empty())
		{
			LOGI("QilexFakeDNS::Lookup we have chosen IP for that host. Remember that");
			m_mChosenIPs[strHost] = strRetIP; // we have chosen IP for that host. Remember that
		}
	}
	else
	{
		strRetIP = m_mChosenIPs.find(strHost)->second; //we already have chosen IP for that host and it is not blacklisted. Stay with it
	}
	
	m_csAccess.unlock();
	LOGI("QilexFakeDNS::Lookup return with IP: %s", strRetIP.c_str());
	return strRetIP;
}

void QilexFakeDNS::AddToBlacklist(const std::string& strIP)
{
	LOGI("QilexFakeDNS::AddToBlacklist, IP: %s", strIP.c_str());
	m_csAccess.lock();
	m_sBlacklist.insert(strIP);
	m_csAccess.unlock();
}

void QilexFakeDNS::RemoveFromBlacklist(const std::string& strIP)
{
	CALL_TRACK;
	m_csAccess.lock();
	m_sBlacklist.erase(strIP);
	m_csAccess.unlock();
}

void QilexFakeDNS::ResetBlacklistForHost(const std::string& strHost)
{
	CALL_TRACK;
	m_csAccess.lock();

	pair<multimap<string,string>::iterator,multimap<string,string>::iterator> ret;
	ret = m_mmQilexFakeDnsList.equal_range(strHost); //find all possible IPs for that host

	for (multimap<string,string>::iterator iter=ret.first; iter!=ret.second; ++iter)
	{
		m_sBlacklist.erase(iter->second);
	}

	m_csAccess.unlock();
}

bool QilexFakeDNS::IsBlacklisted(const std::string& strIP)
{
	CALL_TRACK;
	bool bRet = false;

	m_csAccess.lock();
	if (m_sBlacklist.find(strIP) != m_sBlacklist.end())
		bRet = true;
	m_csAccess.unlock();
	
	return bRet;
}

bool QilexFakeDNS::SetPreferred(const std::string& strHost, const std::string& strIP)
{
	bool bRet = false;

	LOGI("QilexFakeDNS::SetPreferred1, Host:%s  IP:%s", strHost.c_str(), strIP.c_str());
	m_csAccess.lock();

	pair<multimap<string,string>::iterator,multimap<string,string>::iterator> ret;
	ret = m_mmQilexFakeDnsList.equal_range(strHost); //find all possible IPs for that host

	for (multimap<string,string>::iterator iter=ret.first; iter!=ret.second; ++iter)
	{
		string strFullIP = iter->second;
		if (strFullIP.find(strIP) != string::npos)
		{
			LOGI("QilexFakeDNS::SetPreferred2, Host:%s  IP:%s", strHost.c_str(), strFullIP.c_str());
			m_mChosenIPs[strHost] = strFullIP;
			bRet = true;
			break;
		}
	}

	m_csAccess.unlock();

	return bRet;
}
