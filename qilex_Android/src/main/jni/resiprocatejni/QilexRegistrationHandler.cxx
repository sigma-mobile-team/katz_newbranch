// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexRegistrationHandler.hxx"
#include "QilexRegistrationManager.hxx"
#include "QilexStatemachine.hxx"
#include "QilexLogger.hxx"
#include "QilexNET.hxx"
#include <resip/stack/SipMessage.hxx>
#include <resip/dum/ClientRegistration.hxx>
#include <resip/dum/AppDialogSet.hxx>
#include <sstream>
#include "QilexFakeDNS.hxx"
#include "QilexConfig.hxx"
#include "QilexPresenceManager.hxx"

#define FIRST_REGISTER_RETRY_TIME 5
#define MAX_RETRY_TIMEOUT 2

using namespace resip;

QilexRegistrationHandler::QilexRegistrationHandler() 
{ 
}

QilexRegistrationHandler::~QilexRegistrationHandler() 
{ 
}

/// Called when registraion succeeds or each time it is sucessfully
/// refreshed. 
void QilexRegistrationHandler::onSuccess(ClientRegistrationHandle handle, const SipMessage& response)
{
    QilexRegistrationManager& pRegManager = QilexRegistrationManager::instance();
    ClientRegistrationHandle& currentHandle = pRegManager.getClientRegistrationHandle();
    if (currentHandle.isValid() && !(currentHandle == handle) && !currentHandle->myContacts().empty())
    {
    	LOGE("REGISTER onSuccess - for ended dialog");
    	try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error9!");
        }
        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error10!");
        }
    	return;
    }

    if(!response.exists(h_Contacts))
    {
        LOGI("QilexRegistrationHandler::onSuccess no contacts");
        return;
    }

    unsigned short lastPublicPort = pRegManager.mPublicPort;
    string lastPublicIP = pRegManager.mPublicIP;

    if (response.header(h_Vias).front().exists(p_rport))
        pRegManager.mPublicPort = response.header(h_Vias).back().param(p_rport).port();
    else
    	pRegManager.mPublicPort = 0;

        
    if (response.header(h_Vias).front().exists(p_received))
        pRegManager.mPublicIP = response.header(h_Vias).back().param(p_received).c_str();
    else
    	pRegManager.mPublicIP = "";

    if (lastPublicPort != 0 && (pRegManager.mPublicPort != lastPublicPort || pRegManager.mPublicIP != lastPublicIP))
    {
    	LOGI("IP binding changed. Last=%s:%d  New=%s:%d", lastPublicIP.c_str(), lastPublicPort, pRegManager.mPublicIP.c_str(), pRegManager.mPublicPort);
    	QilexPresenceManager::instance().updateSubscriptionsBindings();
    }

    LOGI("REGISTER onSuccess");
    //Contact on the server is finally correct. Tell the app that we are registered
    pRegManager.setClientRegistrationHandle(handle);
    mSm.sendEvent(QilexStatemachine::EVENT_REGISTERED);
    mSm.setRetryTimeOutCount(0);
    onSuccess_dbgPrint(handle, response);
}

void QilexRegistrationHandler::onSuccess_dbgPrint(ClientRegistrationHandle & handle, const SipMessage& response)
{
	 std::stringstream ss;
	  ss << "(Re)registration success:\nContact:";
	  ss << response.header(h_Contacts).front().uri().toString().c_str();
	  ss << "\nVia: rport=";
	  if ( response.header(h_Vias).front().exists(p_rport) )
	    {
	      ss << response.header(h_Vias).front().param(p_rport).port();
	    }

	  ss << "; received=";
	  if ( response.header(h_Vias).front().exists(p_received) )
	    {
	      ss << response.header(h_Vias).front().param(p_received);
	    }
	  ss << "\n";

	  LOGI("%s", ss.str().c_str());
}

// Called when all of my bindings have been removed
void QilexRegistrationHandler::onRemoved(ClientRegistrationHandle handle, const SipMessage& response)
{
	ClientRegistrationHandle& currentHandle = QilexRegistrationManager::instance().getClientRegistrationHandle();
	if (currentHandle == handle)
	{
		LOGI("REGISTER onRemoved - current dialog");
		QilexRegistrationManager::instance().resetRegistrationHandle();

		if (QilexRegistrationManager::instance().wasRegisterRequested() == false) //do not notify Statemachine about unregister if another register was requested because it does weird things then...
			mSm.sendEvent(QilexStatemachine::EVENT_REGISTERALLBINDINGSREMOVED);
	}
	else
		LOGI("REGISTER onRemoved - ended dialog");
}
      
/// call on Retry-After failure. 
/// return values: -1 = fail, 0 = retry immediately, N = retry in N seconds
int QilexRegistrationHandler::onRequestRetry(ClientRegistrationHandle handle, int retrySeconds, const SipMessage& response)
{
	ClientRegistrationHandle& currentHandle = QilexRegistrationManager::instance().getClientRegistrationHandle();
	if (currentHandle.isValid() && !(currentHandle == handle))
	{
		LOGE("REGISTER onRequestRetry - for ended dialog");

	    try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error5!");
        }
        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error6!");
        }
		return -1;
	}
	QilexRegistrationManager& pRegManager = QilexRegistrationManager::instance();

	if (pRegManager.wasRegisterRequested() == false)
	{
		LOGI("REGISTER onRequestRetry - ending dialog because unregister was requested");
		try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error7!");
        }
        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error8!");
        }
		return -1;
	}

	pRegManager.setClientRegistrationHandle(handle);

	int nRetry = retrySeconds;

  	int nCode = response.header(resip::h_StatusLine).responseCode();
  	string strHost = pRegManager.nameAddress().uri().host().c_str();

	//if it is internal 503 - probably we lost internet connection. Retry almost immediately with different server
	if (nCode == 503 && response.getReceivedTransport() == 0)
	{
		LOGI("QilexRegistrationHandler::onRequestRetry internal %s", strHost.c_str());
		string strCurrentServer = QilexFakeDNS::Lookup(strHost);
		QilexFakeDNS::AddToBlacklist(strCurrentServer);
		QilexFakeDNS::Lookup(strHost, true); //force try another server
		pRegManager.setDefaultRegistrationRetryTime(1);
	}
	else if(nCode == 408)
	{
		int nRetryCound = mSm.getRetryTimeOutCount();
	  	LOGI("REGISTER onRequestRetry - getRetryTimeOutCount = %d",nRetryCound);
		if(nRetryCound >= MAX_RETRY_TIMEOUT)
		{
			mSm.sendEvent(QilexStatemachine::EVENT_REGISTER_SIPSTACKRESTART);
			mSm.setRetryTimeOutCount(0);
			return -1;
		}
		else
		{
			nRetryCound +=1;
			mSm.setRetryTimeOutCount(nRetryCound);
		}
	}
	else
	{
		if (nCode == 400)
		{
			//whops, it's our fault. Invoke OnFailure (and then retry with newly created request)
			return -1;
		}
		//it is probably server failure, or non-local network failure
		int nRetry = pRegManager.getDefaultRegistrationRetryTime();
		if (nRetry != FIRST_REGISTER_RETRY_TIME)
		{
			//first fail on specific server
			LOGI("QilexRegistrationHandler::onRequestRetry first fail on specific server %s", strHost.c_str());
			pRegManager.setDefaultRegistrationRetryTime(FIRST_REGISTER_RETRY_TIME);
		}
		else
		{
			LOGI("QilexRegistrationHandler::onRequestRetry failed %s", strHost.c_str());
			//second fail on specific server
			string strCurrentServer = QilexFakeDNS::Lookup(strHost);
			QilexFakeDNS::AddToBlacklist(strCurrentServer);
			string strNewServer = QilexFakeDNS::Lookup(strHost);
			if (QilexFakeDNS::IsBlacklisted(strNewServer))
			{
				QilexFakeDNS::Lookup(strHost, true); //force try another blacklisted server
				int nMean = QilexConfig::getReRegisterTime() * 4;
				float nVariance = QilexConfig::getReRegisterTime() * 0.33f;
				float nRand1 = rand()/(float)RAND_MAX;
				float nRand2 = rand()/(float)RAND_MAX;
				int nRetryAfter = (int)(sqrt(-2*log(nRand1)) * cos(2*3.141592f*nRand2) * nVariance + nMean + 0.5f); //Box-Muller transform for normal distribution
				pRegManager.setDefaultRegistrationRetryTime(nRetryAfter);
			}
			else
			{
				pRegManager.setDefaultRegistrationRetryTime(1); //try new server (almost) immediately
			}
		}
	}
  	nRetry = pRegManager.getDefaultRegistrationRetryTime();
  	LOGI("REGISTER onRequestRetry - getDefaultRegistrationRetryTime = %d",nRetry);
  return nRetry;
}
      
/// Called if registration fails, usage will be destroyed (unless a 
/// Registration retry interval is enabled in the Profile)
void QilexRegistrationHandler::onFailure(ClientRegistrationHandle handle, const SipMessage& response)
{
	if (!handle.isValid())
	{
		LOGE("REGISTER onFailure - invalid handle");
		return;
	}

	int responseCode = response.header(resip::h_StatusLine).responseCode();

	ClientRegistrationHandle& currentHandle = QilexRegistrationManager::instance().getClientRegistrationHandle();
	if (currentHandle.isValid() && !(currentHandle == handle))
  	{
  		if (!(responseCode == 503 && response.getReceivedTransport() == 0))
  			LOGE("REGISTER onFailure, err code: %d - for ended dialog", responseCode);
        try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error0!");
        }
        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error1!");
        }

  		return;
  	}

	QilexRegistrationManager& pRegManager = QilexRegistrationManager::instance();

	if (pRegManager.wasRegisterRequested() == false)
	{
		LOGI("REGISTER onFailure - ending dialog because unregister was requested");
		try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error2!");
        }

        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error3!");
        }
		return;
	}

	pRegManager.setClientRegistrationHandle(handle);
	//pRegManager.sendUnregister();
	if (responseCode == 408)
	{
		mSm.sendEvent(QilexStatemachine::EVENT_REGISTRATIONTIMEOUT);
	}
	else
    {
		mSm.sendEvent(QilexStatemachine::EVENT_REGISTERFAILED);
    }

	std::stringstream ss;
	ss << "Registration failed, error ";
	ss << responseCode;
	LOGI("%s", ss.str().c_str());
}


void QilexRegistrationHandler::onFlowTerminated(ClientRegistrationHandle handle)
{
	ClientRegistrationHandle& currentHandle = QilexRegistrationManager::instance().getClientRegistrationHandle();
	if (currentHandle.isValid() && !(currentHandle == handle))
	{
		LOGE("REGISTER onFlowTerminated - for ended dialog");
		try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error11!");
        }
        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error12!");
        }
		return;
	}

	if (QilexRegistrationManager::instance().wasRegisterRequested() == false)
	{
		LOGI("REGISTER onFlowTerminated - ending dialog because unregister was requested");
		try
        {
            handle->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error13!");
        }
        try
        {
            handle->getAppDialogSet()->end();
        }
        catch (...)
        {
           LOGE("REGISTER - error14!");
        }
		return;
	}

	if (handle.isValid() && !handle->allContacts().empty())
		LOGE("REGISTER onFlowTerminated (registered)");
	else
		LOGE("REGISTER onFlowTerminated (not registered)");

	if (!handle.isValid() || handle->allContacts().empty())
		sleep(1); //it is the case when TCP/TLS connecting failed. Wait for a moment, do not overload machine nor server
	QilexRegistrationManager::instance().updateBinding();
}

