// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef __EXCEPTIONS
#error This source file SHOULD be built with -fexceptions!
#endif

#include "ResiprocateJni.h"
#include "QilexRegistrationManager.hxx"
#include "QilexCallManager.hxx"
#include "QilexLogger.hxx"
#include "QilexFakeDNS.hxx"
#include "QilexStatemachine.hxx"
#include "QilexHmac.hxx"
#include "QilexRTP.hxx"
#include "QilexSdp.hxx"
#include "QilexPagerMessageManager.hxx"
#include "QilexPresenceManager.hxx"
#include "../audiomgr/QilexAndroidAudioWrapper.h"
#include <rutil/Logger.hxx>
#include <string>
#include <vector>
#include <sstream>
#include "QilexConfig.hxx"
#include <reflow/FlowManagerSubsystem.hxx>
#include <reTurn/ReTurnSubsystem.hxx>
#include "QilexCrypto.hxx"
#include "AudioMgr.h"
#include  "RootCert.h"
#include "QilexOutOfDialogManager.hxx"
#include <openssl/evp.h>
#include <openssl/hmac.h>
using namespace resip;
using namespace std;

JavaVM* qilex_jvm;

bool connected = false;

resip::Mutex registerMutex;

typedef long unsigned int* _Unwind_Ptr;

extern "C" _Unwind_Ptr dl_unwind_find_exidx(_Unwind_Ptr pc, int* pcount);
extern "C" _Unwind_Ptr __gnu_Unwind_Find_exidx(_Unwind_Ptr pc, int* pcount) {
	return dl_unwind_find_exidx(pc, pcount);
}

static void* g_func_ptr;

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
	g_func_ptr = (void*) __gnu_Unwind_Find_exidx;

	try {
		throw "exceptions test";
	} catch (...) {
	}

	CALL_TRACK
	;
	// enable subsystems' debug

#ifdef DEBUG

	Log::setLevel(Log::Debug,Subsystem::DUM);
	//Log::setLevel(Log::Debug,Subsystem::DNS);
	Log::setLevel(Log::Debug,Subsystem::SDP);
	Log::setLevel(Log::Debug,Subsystem::SIP);
	Log::setLevel(Log::Debug,Subsystem::TRANSPORT);

	//Log::setLevel(Log::Debug,Subsystem::TRANSACTION);
	Log::setLevel(Log::Debug, FlowManagerSubsystem::FLOWMANAGER);
	Log::setLevel(Log::Debug, ReTurnSubsystem::RETURN);

#endif

	QilexStatemachine::setupVm(vm);
	QilexHmac::setupVm(vm);
	qilex_jvm = vm;

	return JNI_VERSION_1_6;
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    initialize
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_initialize
  (JNIEnv *env, jclass)
{
  CALL_TRACK;
  QilexBaseManager::initialize();

	// initialize all managers so we're ready for receiving registration, call and message events
  QilexRegistrationManager& registrationManager = QilexRegistrationManager::instance();
  registrationManager.startThread();
  QilexCallManager::instance();
  QilexPagerMessageManager::instance();
  QilexPresenceManager& presenceManager = QilexPresenceManager::instance();
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    shutdown
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_shutdown
  (JNIEnv *env, jclass)
{
  CALL_TRACK;
  QilexBaseManager::shutdown();
}

JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_stackRestart(JNIEnv *, jobject)
{
	CALL_TRACK;
	QilexResipPool::restartStack();
}

JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_pingToAddress
    (JNIEnv *env, jobject, jstring jSipAddress) {
	QilexOutOfDialogManager& outOfDialogManager = QilexOutOfDialogManager::instance();

	const char* peerUri = env->GetStringUTFChars(jSipAddress, NULL);
	assert( peerUri);

	#if SIP_USE_SIPS
        string nameAddr = "sips:";
    #else
        string nameAddr = "sip:";
    #endif

    nameAddr += peerUri;
    nameAddr += "@";
    nameAddr += DEFAULT_SIP_HOST;

	outOfDialogManager.sendPingToContact(nameAddr);
	env->ReleaseStringUTFChars(jSipAddress, peerUri);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    register
 * Signature: (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_register
  (JNIEnv * env, 
   jobject ,
   jstring jUsername, 
   jstring jPassword,
   jstring jProfileInstanceId,
   jstring jToken,
   jint jExpiration, 
   jint jRepublish,
   jint jResubsribe,
   jstring jIpAddress,
   jobjectArray jAliases,
   jstring jKey,
   jstring jCert,
   jstring jPub,
   jstring jPushOS,
   jstring jDeviceId,
   jstring jPushToken)
{
	LOGI(
		"SIGMA-PHUCPV Java_sigma_qilex_sip_resiprocate_ResiprocateJni_register start");CALL_TRACK;
//	LOGI("SIGMA-PHUCPV Java_sigma_qilex_sip_resiprocate_ResiprocateJni_register2");
//  LOGI("SIGMA-PHUCPV DEBUG: jUsername = %s", jUsername);
//	LOGI("SIGMA-PHUCPV Java_sigma_qilex_sip_resiprocate_ResiprocateJni_register2");
//  LOGI("SIGMA-PHUCPV DEBUG: jPassword = %s", jPassword);
//  LOGI("SIGMA-PHUCPV DEBUG: jProfileInstanceId = %s", jProfileInstanceId);
//  LOGI("SIGMA-PHUCPV DEBUG: jToken = %s", jToken);
  LOGI(
		"SIGMA-PHUCPV DEBUG: jExpiration = %d", jExpiration);LOGI(
		"SIGMA-PHUCPV DEBUG: jRepublish = %d", jRepublish);LOGI(
		"SIGMA-PHUCPV DEBUG: jResubsribe = %d", jResubsribe);

//  LOGI("SIGMA-PHUCPV DEBUG: jIpAddress = %s", jIpAddress);
//  LOGI("SIGMA-PHUCPV DEBUG: jUsername = " + jAliases);
//  LOGI("SIGMA-PHUCPV DEBUG: jKey = %s", jKey);
//  LOGI("SIGMA-PHUCPV DEBUG: jCert = %s", jCert);
//  LOGI("SIGMA-PHUCPV DEBUG: jPub = %s", jPub);

// initialize all managers so we're ready for receiving registration, call and message events
QilexRegistrationManager& registrationManager = QilexRegistrationManager::instance();
  QilexCallManager& callManager = QilexCallManager::instance();
  QilexPagerMessageManager::instance();
  QilexPresenceManager& presenceManager = QilexPresenceManager::instance();
  QilexOutOfDialogManager& outOfDialogManager = QilexOutOfDialogManager::instance();
    
  QilexConfig::setReRegisterTime(jExpiration);
  QilexConfig::setPresenceSubscriptionExpire(jResubsribe);
  QilexConfig::setPresencePublicationExpire(jRepublish);
  LOGI("SIGMA-PHUCPV Java_sigma_qilex_sip_resiprocate_ResiprocateJni_register 2");LOGI(
		"Refresh params %d %d %d", jExpiration, jRepublish, jResubsribe);

const char* username = env->GetStringUTFChars(jUsername, NULL);
  const char* password = env->GetStringUTFChars(jPassword, NULL);
  const char* instanceId = env->GetStringUTFChars(jProfileInstanceId, NULL);
  const char* token = env->GetStringUTFChars(jToken, NULL);
  vector<string> aliases;
  const char* key = env->GetStringUTFChars(jKey, NULL);
  const char* cert = env->GetStringUTFChars(jCert, NULL);
  const char* pub = env->GetStringUTFChars(jPub, NULL);
  const char* pushOs = env->GetStringUTFChars(jPushOS, NULL);
  const char* pushDeviceId = env->GetStringUTFChars(jDeviceId, NULL);
  const char* pushToken = env->GetStringUTFChars(jPushToken, NULL);

  assert( username && password && instanceId && token);

  if (jAliases)
  {
      int aliasesSize = env->GetArrayLength(jAliases);
      for ( int i = 0; i < aliasesSize; i++)
	  {
		 jstring item = static_cast<jstring>(env->GetObjectArrayElement(jAliases,i));
		 if (item)
		 {
		    const char* alias = env->GetStringUTFChars(item,NULL);
		    aliases.push_back(string(alias));
		    env->ReleaseStringUTFChars(item, alias);
		  }
	  }
  }
  
  if (QilexNET::activeInterfaces() < 1 )
    {
      QilexStatemachine sm;
      sm.sendEvent(QilexStatemachine::EVENT_REGISTERFAILED);
      return;
    }
  registrationManager.setCredentials( DEFAULT_SIP_HOST, string(username), string(password), string(instanceId));
#if SIP_USE_SIPS
  registrationManager.setIpAddress( DEFAULT_SIP_HOST );
#else
  registrationManager.setIpAddress( DEFAULT_SIP_HOST );
#endif
  registrationManager.setToken(string(token));
  callManager.setToken(string(token));
  registrationManager.setAliases(aliases);
  registrationManager.setKeyandCert(string(key), string(pub), string(cert));
  registrationManager.setPushInfo(string(pushOs), string(pushDeviceId), string(pushToken));
  env->ReleaseStringUTFChars(jUsername, username);
  env->ReleaseStringUTFChars(jPassword, password );
  env->ReleaseStringUTFChars(jProfileInstanceId, instanceId);
  env->ReleaseStringUTFChars(jToken, token);
  env->ReleaseStringUTFChars(jKey, key);
  env->ReleaseStringUTFChars(jCert, cert);
  env->ReleaseStringUTFChars(jPub, pub);


  registrationManager.sendRegister();
  LOGI(
		"SIGMA-PHUCPV Java_sigma_qilex_sip_resiprocate_ResiprocateJni_register Finish");
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    unregister
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_unregister
(JNIEnv *, jobject)
{
CALL_TRACK;

QilexPresenceManager::instance().resetClientPublicationHandle();
QilexRegistrationManager::instance().sendUnregister();

}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    resetServerIPs
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_resetServerIPs
(JNIEnv *, jobject)
{
QilexFakeDNS::Reset();
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    addSipServer
 * Signature: (Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_addSipServer
(JNIEnv *env, jobject, jstring jIp, jint port)
{
const char* ip = env->GetStringUTFChars(jIp, NULL);

string ipwithport;
ipwithport += ip;
stringstream ss;
ss << ":" << port;
ipwithport += ss.str();

QilexFakeDNS::AddEntry(DEFAULT_SIP_HOST, ipwithport);

env->ReleaseStringUTFChars(jIp, ip);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    addTurnServer
 * Signature: (Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_addTurnServer
(JNIEnv *env, jobject, jstring jIp, jint port)
{
const char* ip = env->GetStringUTFChars(jIp, NULL);

string ipwithport;
ipwithport += ip;
stringstream ss;
ss << ":" << port;
ipwithport += ss.str();

QilexFakeDNS::AddEntry(DEFAULT_TURN_HOST, ipwithport);

env->ReleaseStringUTFChars(jIp, ip);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    makeCall
 * Signature: (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_makeCall
(JNIEnv * env,
	jobject ,
	jstring jUsername,
	jstring jPassword,
	jstring jProfileInstanceId,
	jstring jIpAddress,
	jstring jPeerUri,
	jint jExpirationTimeMs,
	jboolean jShowNoTurnWaring,
	jboolean jOutCall)
{
CALL_TRACK;

QilexRegistrationManager& registrationManager = QilexRegistrationManager::instance();
QilexCallManager& callManager = QilexCallManager::instance();

const char* peerUri = env->GetStringUTFChars(jPeerUri, NULL);
const char* imsi = env->GetStringUTFChars(jPassword, NULL);

assert( peerUri);

callManager.setPeerUri(peerUri);

callManager.setOUT(jOutCall);

env->ReleaseStringUTFChars(jPeerUri, peerUri);

callManager.sendInvite(jShowNoTurnWaring, jOutCall, string(imsi));
env->ReleaseStringUTFChars(jPassword, imsi);

}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    answerCall
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_answerCall
(JNIEnv *, jobject)
{
CALL_TRACK;
QilexCallManager::instance().answerCall();
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    pauseCall
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_pauseCall
(JNIEnv *, jobject)
{
CALL_TRACK;
QilexCallManager::instance().pauseCall();

}
/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    resumeCall
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_resumeCall
(JNIEnv *, jobject)
{
CALL_TRACK;
QilexCallManager::instance().resumeCall();

}
/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    rejectCall
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_rejectCall
(JNIEnv *, jobject)
{
CALL_TRACK;

QilexCallManager::instance().rejectCall();
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    startAudio
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_startAudio
(JNIEnv *, jobject)
{
QilexCallManager::instance().startAudio();
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    updateAudioConfig
 * Signature: (ZZZ)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_updateAudioConfig
(JNIEnv *, jobject, jboolean jSpeakerOn, jboolean jHeadphonesOn, jboolean jMuted)
{
CALL_TRACK;
QilexCallManager::instance().updateAudioConfig(jSpeakerOn, jHeadphonesOn, jMuted);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    sendMessage
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_sendMessage
(JNIEnv *env,
	jobject,
	jstring jFrom,
	jstring jTo,
	jstring jId,
	jstring jTimestamp,
	jstring jContent,
	jstring jType,
	jstring jUuid)
{
CALL_TRACK;

QilexPagerMessageManager& manager = QilexPagerMessageManager::instance();
const char* content = env->GetStringUTFChars(jContent, NULL);
const char* to = env->GetStringUTFChars(jTo, NULL);
const char* from = env->GetStringUTFChars(jFrom, NULL);
const char* id = env->GetStringUTFChars(jId, NULL);
const char* timestamp = env->GetStringUTFChars(jTimestamp, NULL);
const char* type = env->GetStringUTFChars(jType, NULL);
const char* uuid = env->GetStringUTFChars(jUuid, NULL);

assert(content && to && from && id && timestamp);

manager.setPeerUri(string(to));
manager.setFromUri(string(from));
manager.setTimestamp(string(timestamp));
manager.sendPagerMessage(string(content),string(id),string(type),string(uuid));

env->ReleaseStringUTFChars(jContent, content);
env->ReleaseStringUTFChars(jTo, to);
env->ReleaseStringUTFChars(jFrom, from);
env->ReleaseStringUTFChars(jId, id);
env->ReleaseStringUTFChars(jTimestamp, timestamp);
env->ReleaseStringUTFChars(jType, type);
env->ReleaseStringUTFChars(jUuid, uuid);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    sendGroupChatMessage
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_sendGroupChatMessageWithParticipant
(JNIEnv *env,
	jobject,
	jstring jFrom,
	jstring jTo,
	jstring jId,
	jstring jTimestamp,
	jstring jContent,
	jstring jType,
	jstring jUuid,
	jstring jGroupChatMsgType,
	jstring jGroupChatUuid,
	jstring jGroupChatPaticipants,
	jstring jGroupChatAdministrator)
{
CALL_TRACK;

QilexPagerMessageManager& manager = QilexPagerMessageManager::instance();
const char* content = env->GetStringUTFChars(jContent, NULL);
const char* to = env->GetStringUTFChars(jTo, NULL);
const char* from = env->GetStringUTFChars(jFrom, NULL);
const char* id = env->GetStringUTFChars(jId, NULL);
const char* timestamp = env->GetStringUTFChars(jTimestamp, NULL);
const char* type = env->GetStringUTFChars(jType, NULL);
const char* uuid = env->GetStringUTFChars(jUuid, NULL);
const char* groupChatMsgType = env->GetStringUTFChars(jGroupChatMsgType, NULL);
const char* groupChatUuid = env->GetStringUTFChars(jGroupChatUuid, NULL);
const char* groupChatPaticipants = env->GetStringUTFChars(jGroupChatPaticipants, NULL);
const char* groupChatAdministrator = env->GetStringUTFChars(jGroupChatAdministrator, NULL);

assert(content && to && from && id && timestamp);

manager.setPeerUri(string(to));
manager.setFromUri(string(from));
manager.setTimestamp(string(timestamp));
manager.sendPagerGroupMessage(string(content),string(id),string(type),string(uuid),
		string(groupChatMsgType),string(groupChatUuid),string(groupChatPaticipants),string(groupChatAdministrator));

env->ReleaseStringUTFChars(jContent, content);
env->ReleaseStringUTFChars(jTo, to);
env->ReleaseStringUTFChars(jFrom, from);
env->ReleaseStringUTFChars(jId, id);
env->ReleaseStringUTFChars(jTimestamp, timestamp);
env->ReleaseStringUTFChars(jType, type);
env->ReleaseStringUTFChars(jUuid, uuid);
env->ReleaseStringUTFChars(jGroupChatMsgType, groupChatMsgType);
env->ReleaseStringUTFChars(jGroupChatUuid, groupChatUuid);
env->ReleaseStringUTFChars(jGroupChatPaticipants, groupChatPaticipants);
env->ReleaseStringUTFChars(jGroupChatAdministrator, groupChatAdministrator);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni

 * Method:    sendGroupChatMessage
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_sendGroupChatMessage
(JNIEnv *env,
	jobject,
	jstring jFrom,
	jstring jTo,
	jstring jId,
	jstring jTimestamp,
	jstring jContent,
	jstring jType,
	jstring jUuid,
	jstring jGroupChatMsgType,
	jstring jGroupChatUuid)
{
CALL_TRACK;

QilexPagerMessageManager& manager = QilexPagerMessageManager::instance();
const char* content = env->GetStringUTFChars(jContent, NULL);
const char* to = env->GetStringUTFChars(jTo, NULL);
const char* from = env->GetStringUTFChars(jFrom, NULL);
const char* id = env->GetStringUTFChars(jId, NULL);
const char* timestamp = env->GetStringUTFChars(jTimestamp, NULL);
const char* type = env->GetStringUTFChars(jType, NULL);
const char* uuid = env->GetStringUTFChars(jUuid, NULL);
const char* groupChatMsgType = env->GetStringUTFChars(jGroupChatMsgType, NULL);
const char* groupChatUuid = env->GetStringUTFChars(jGroupChatUuid, NULL);

assert(content && to && from && id && timestamp);

manager.setPeerUri(string(to));
manager.setFromUri(string(from));
manager.setTimestamp(string(timestamp));
manager.sendPagerGroupMessage(string(content),string(id),string(type),string(uuid),
		string(groupChatMsgType),string(groupChatUuid));

env->ReleaseStringUTFChars(jContent, content);
env->ReleaseStringUTFChars(jTo, to);
env->ReleaseStringUTFChars(jFrom, from);
env->ReleaseStringUTFChars(jId, id);
env->ReleaseStringUTFChars(jTimestamp, timestamp);
env->ReleaseStringUTFChars(jType, type);
env->ReleaseStringUTFChars(jUuid, uuid);
env->ReleaseStringUTFChars(jGroupChatMsgType, groupChatMsgType);
env->ReleaseStringUTFChars(jGroupChatUuid, groupChatUuid);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    sendComposeMessage
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_sendComposeMessage
(JNIEnv *env,
	jobject,
	jstring jFrom,
	jstring jTo,
	jstring jId,
	jstring jTimestamp,
	jstring jContent)
{
CALL_TRACK;

QilexPagerMessageManager& manager = QilexPagerMessageManager::instance();
const char* content = env->GetStringUTFChars(jContent, NULL);
const char* to = env->GetStringUTFChars(jTo, NULL);
const char* from = env->GetStringUTFChars(jFrom, NULL);
const char* id = env->GetStringUTFChars(jId, NULL);
const char* timestamp = env->GetStringUTFChars(jTimestamp, NULL);

assert(content && to && from && id && timestamp);

manager.setPeerUri(string(to));
manager.setFromUri(string(from));
manager.setTimestamp(string(timestamp));
manager.sendComposingPagerMessage(string(content),string(id));

env->ReleaseStringUTFChars(jContent, content);
env->ReleaseStringUTFChars(jTo, to);
env->ReleaseStringUTFChars(jFrom, from);
env->ReleaseStringUTFChars(jId, id);
env->ReleaseStringUTFChars(jTimestamp, timestamp);
}

/*

 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    sendComposeMessage
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_sendComposeGroupMessage
(JNIEnv *env,
	jobject,
	jstring jFrom,
	jstring jTo,
	jstring jId,
	jstring jTimestamp,
	jstring jContent,
	jstring jGroupChatMsgType,
	jstring jGroupChatUuid)
{
CALL_TRACK;

QilexPagerMessageManager& manager = QilexPagerMessageManager::instance();
const char* content = env->GetStringUTFChars(jContent, NULL);
const char* to = env->GetStringUTFChars(jTo, NULL);
const char* from = env->GetStringUTFChars(jFrom, NULL);
const char* id = env->GetStringUTFChars(jId, NULL);
const char* timestamp = env->GetStringUTFChars(jTimestamp, NULL);
const char* groupChatMsgType = env->GetStringUTFChars(jGroupChatMsgType, NULL);
const char* groupChatUuid = env->GetStringUTFChars(jGroupChatUuid, NULL);

assert(content && to && from && id && timestamp);

manager.setPeerUri(string(to));
manager.setFromUri(string(from));
manager.setTimestamp(string(timestamp));
manager.sendComposingPagerGroupMessage(string(content),string(id),string(groupChatMsgType),string(groupChatUuid));

env->ReleaseStringUTFChars(jContent, content);
env->ReleaseStringUTFChars(jTo, to);
env->ReleaseStringUTFChars(jFrom, from);
env->ReleaseStringUTFChars(jId, id);
env->ReleaseStringUTFChars(jTimestamp, timestamp);
env->ReleaseStringUTFChars(jGroupChatMsgType, groupChatMsgType);
env->ReleaseStringUTFChars(jGroupChatUuid, groupChatUuid);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    publishPresence
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_publishPresence
(JNIEnv*,
	jobject,
	jint jPresence)
{
CALL_TRACK;
QilexPresenceManager& presenceManager = QilexPresenceManager::instance();
QilexPresenceHandler::Status status = static_cast<QilexPresenceHandler::Status>(jPresence);
presenceManager.publish(status,string(""));
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    subscribePresence
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_subscribePresence
(JNIEnv* env, jobject, jstring jPeer)
{
CALL_TRACK;
QilexPresenceManager& presenceManager = QilexPresenceManager::instance();
const char* peer = env->GetStringUTFChars(jPeer,NULL);

assert(peer);

presenceManager.subscribe(string(peer));

env->ReleaseStringUTFChars(jPeer,peer);

}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    unsubscribePresence
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_unsubscribePresence
(JNIEnv* env, jobject, jstring jPeer)
{
CALL_TRACK;

QilexPresenceManager& presenceManager = QilexPresenceManager::instance();
const char* peer = env->GetStringUTFChars(jPeer,NULL);

assert(peer);

presenceManager.unsubscribe(string(peer));

env->ReleaseStringUTFChars(jPeer,peer);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    setConnection
 * Signature: (ZZZZ)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_setConnection
(JNIEnv *, jobject, jboolean jWiFi, jboolean jMobile3G, jboolean jMobile2G, jboolean jLTE)
{
QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper()->setAvailableConnectivity(jWiFi, jMobile3G, jMobile2G, jLTE);

}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    setPhonePerformance
 * Signature: (FZI)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_setPhonePerformance
(JNIEnv *, jobject, jfloat jPerformance, jboolean jLowLatencyAudio, jint jAndroidVersion)
{
QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper()->setDevicePerformance(jPerformance, jLowLatencyAudio, jAndroidVersion);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    updateAudioVolume
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_updateAudioVolume
(JNIEnv *, jobject, jfloat jVolume)
{
// jVolume wartosc glosnosci od 0.0 do 1.0 - 0.0 oznacza najnizszy poziom a nie wylaczenie dzwieku!
CALL_TRACK;
if (jVolume != -1.0f)
QilexCallManager::instance().updateAudioVolume(jVolume);
}

/*
 * Class:     sigma_qilex_sip_resiprocate_ResiprocateJni
 * Method:    updateBinding
 * Signature: (F)V
 */
JNIEXPORT void JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_updateBinding
(JNIEnv *, jobject)
{
CALL_TRACK;
QilexRegistrationManager::instance().updateBinding();
}

JNIEXPORT jbyteArray JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_hmacSHA512(
		JNIEnv *env, jobject job, jstring msisdn, jstring msg, jstring hash){
	const char* number = env->GetStringUTFChars(msisdn,NULL);
	const char* msgchar = env->GetStringUTFChars(msg,NULL);
	const char* hashchar = env->GetStringUTFChars(hash,NULL);
	 QilexHmac hm;
	return hm.hmacSHA512(string(number),string(msgchar),hashchar);
}
JNIEXPORT jstring JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_certificatePEM(
	JNIEnv *env, jobject) {
	const char* certPEM = QilexCrypto::Get()->getCertificateRequestPEM();
	jstring jstr = (env)->NewStringUTF(certPEM);
	return jstr;
}
JNIEXPORT jstring JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_publicKey(
		JNIEnv *env, jobject){
	const char* pubKey = QilexCrypto::Get()->getPublicKey();
	jstring jstr = (env)->NewStringUTF(pubKey);
	return jstr;
}
JNIEXPORT jstring JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_privateKey(
		JNIEnv *env, jobject){
	const char* privKey = QilexCrypto::Get()->getPrivateKey();
	jstring jstr = (env)->NewStringUTF(privKey);
	return jstr;
}

JNIEXPORT jfloat JNICALL Java_sigma_qilex_sip_resiprocate_ResiprocateJni_audioQuality(
	JNIEnv *, jobject) {
float quality = QilexCallManager::instance().audioQuality();
return quality;
}
