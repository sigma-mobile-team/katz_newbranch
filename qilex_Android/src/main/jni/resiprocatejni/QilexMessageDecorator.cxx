#include "QilexLogger.hxx"
#include <QilexMessageDecorator.hxx>
#include <QilexRegistrationManager.hxx>
#include <sstream>
#include "QilexFakeDNS.hxx"

#include <android/log.h>

QilexMessageDecorator::QilexMessageDecorator(std::string username) :
		mUsername(username) {
}

void QilexMessageDecorator::decorateMessage(resip::SipMessage &msg,
		const resip::Tuple &source, const resip::Tuple &destination,
		const resip::Data& sigcompId) {
	QilexRegistrationManager& regManager = QilexRegistrationManager::instance();

	if (msg.isRequest() && msg.method() != resip::ACK
			&& msg.method() != resip::BYE
			&& msg.header(h_RequestLine).uri().exists(p_transport))
		msg.header(h_RequestLine).uri().remove(p_transport);

	if (msg.isRequest()) {
		string strQilexHost = QilexFakeDNS::Lookup(
				msg.header(h_RequestLine).uri().host().c_str());
		if (!strQilexHost.empty()) {
			try {
#if SIP_USE_SIPS
				Uri uri = Uri(string("sips:" + strQilexHost).c_str());
#else
				Uri uri = Uri(string("sip:" + strQilexHost).c_str());
#endif
				msg.header(h_RequestLine).uri().host() = uri.host();
				msg.header(h_RequestLine).uri().port() = uri.port();
			} catch (...) {
			}
		}

		msg.header(h_RequestLine).uri().user() = msg.header(h_To).uri().user();
	}

	if (msg.exists(h_To) && msg.exists(h_From)) {
		if (msg.header(h_To).uri().scheme() != "tel") {
			msg.header(h_To).uri().host() = DEFAULT_SIP_HOST;
			msg.header(h_To).uri().port() = 0;
		}

		msg.header(h_From).uri().host() = DEFAULT_SIP_HOST;
		msg.header(h_From).uri().port() = 0;
	}

	if (msg.exists(h_Contacts)) {
#if SIP_SERVER_USE_SSL
		msg.header(h_Contacts).front().uri().param(p_transport) = "tls";
#else
		msg.header(h_Contacts).front().uri().param(p_transport) = "tcp";
#endif

		if (msg.isResponse()
				|| (msg.method() == resip::REGISTER
						&& msg.header(h_Contacts).front().exists(p_expires)
						&& msg.header(h_Contacts).front().param(p_expires) == 0)) {
			if (!regManager.mPublicIP.empty())
				msg.header(h_Contacts).front().uri().host() =
						regManager.mPublicIP.c_str();
			if (regManager.mPublicPort != 0)
				msg.header(h_Contacts).front().uri().port() =
						regManager.mPublicPort;
		} else {
			//server-side contact filling
			msg.header(h_Contacts).front().uri().host() = "0.0.0.0";
			msg.header(h_Contacts).front().uri().port() = 0;
		}

#if SIP_USE_SIPS
		msg.header(h_Contacts).front().uri().scheme() = "sips";
#else
		msg.header(h_Contacts).front().uri().scheme() = "sip";
#endif
	}

	if (msg.isRequest()) {
		if (msg.method() == REGISTER || msg.method() == PUBLISH) {
			for (vector<string>::const_iterator i =
					regManager.aliases().begin();
					i != regManager.aliases().end(); i++) {
				try {
					resip::NameAddr aliasUri((*i).c_str());
					msg.header(resip::h_XAliass).push_back(aliasUri);
				} catch (...) {
				}
			}
		} else if (msg.method() == SUBSCRIBE) {
#if SIP_USE_SIPS
			msg.header(resip::h_From).uri().scheme() = "sips";
#else
			msg.header(resip::h_From).uri().scheme() = "sip";
#endif
			msg.header(resip::h_From).uri().user() = mUsername.c_str();
		}

	}
}

void QilexMessageDecorator::rollbackMessage(resip::SipMessage& msg) {
	if (msg.exists(resip::h_XAliass)) {
		msg.remove(resip::h_XAliass);
	}
	return;
}

resip::MessageDecorator* QilexMessageDecorator::clone() const {
	return new QilexMessageDecorator(mUsername);
}
