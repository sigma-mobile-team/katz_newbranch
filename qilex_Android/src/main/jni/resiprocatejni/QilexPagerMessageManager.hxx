// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef __Qilex_Pager_Message_Manager_HXX__
#define __Qilex_Pager_Message_Manager_HXX__

//#include "QilexPagerMessageHandler.hxx"
#include "QilexBaseManager.hxx"
#include <resip/dum/PagerMessageHandler.hxx>
#include <string>

class QilexPagerMessageHandler;

class QilexPagerMessageManager : public QilexBaseManager
{
public:
  static QilexPagerMessageManager& instance();
  
  virtual ~QilexPagerMessageManager();
  
  void sendPagerMessage(const std::string& strMessage, const std::string& nId, const std::string& type, const std::string& nUuid);

  void sendComposingPagerMessage(const std::string& strMessage, const std::string& nId);
  
  void sendComposingPagerGroupMessage(const std::string& strMessage, const std::string& nId,
		  const std::string& nGroupChatMsgType,
		  const std::string& nGroupChatUuid);

  void sendPagerGroupMessage(
		  const std::string& strMessage,
		  const std::string& nId,
		  const std::string& type,
		  const std::string& nUuid,
		  const std::string& nGroupChatMsgType,
		  const std::string& nGroupChatUuid,
		  const std::string& nGroupChatPaticipants,
		  const std::string& nGroupChatAdministrator);

  void sendPagerGroupMessage(
		  const std::string& strMessage,
		  const std::string& nId,
		  const std::string& type,
		  const std::string& nUuid,
		  const std::string& nGroupChatMsgType,
		  const std::string& nGroupChatUuid);

  void setMessageHandle(resip::ClientPagerMessageHandle& handle);

  void setPeerUri(const string& peerUri);
  
  void setFromUri(const string& fromUri);

  void setTimestamp(const string& timestamp);

  void setId(const string& id);

  void setClientPagerMessageManager(resip::ClientPagerMessageHandle handle);
  
  void setServerPagerMessageManager(resip::ServerPagerMessageHandle handle);

  resip::ClientPagerMessageHandle getClientPagerMessageManager();
  
  resip::ServerPagerMessageHandle getServerPagerMessageManager();

  void resetClientMessageHandle();
  void resetServerMessageHandle();

  void resetHandler();

private:
  QilexPagerMessageManager();
  
private: //data
  
  /// singleton handler
  static QilexPagerMessageManager* mInstance;
  
  // call handler
  QilexPagerMessageHandler* mMessageHandler;
  
  /// client handle
  resip::ClientPagerMessageHandle mClientMessageHandle;
  
  /// server handle
  resip::ServerPagerMessageHandle mServerMessageHandle;
  
  /// peer uri
  string mPeerUri;
  
  /// from uri
  string mFromUri;

  /// timestamp uri
  string mTimestamp;

};
#endif
