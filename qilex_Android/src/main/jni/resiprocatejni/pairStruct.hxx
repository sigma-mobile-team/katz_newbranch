// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef pairStruct_HXX
#define pairStruct_HXX


#include "ICE_Candidate.hxx"

class pairStruct
{
	public:	
		ICE_Candidate m_Local;
		ICE_Candidate m_Remote;
		unsigned long long m_llPriority;
		bool m_bChecked;
		enum states { frozen, waiting, in_progress, failed, success } m_State;

		pairStruct(ICE_Candidate local, ICE_Candidate remote)
		{
			m_Local = local;
			m_Remote = remote;
			m_llPriority = 0;
			m_bChecked = false;
			m_State = frozen;
		}
		void computePriority(bool isControlling)
		{
			//priotytet = 2^32*min(G, D) + 2*max(G, D), (G>D?1:0)
			if(isControlling)
			{
		
				if(m_Local.m_nPriority > m_Remote.m_nPriority)
					m_llPriority = m_Remote.m_nPriority << 32 + m_Local.m_nPriority << 2 + 1;
				else
					m_llPriority = m_Local.m_nPriority << 32 + m_Remote.m_nPriority << 2 + 0;
			}
			else
			{
				if(m_Remote.m_nPriority > m_Local.m_nPriority)
					m_llPriority = m_Local.m_nPriority << 32 + m_Remote.m_nPriority << 2 + 1;
				else
					m_llPriority = m_Remote.m_nPriority << 32 + m_Local.m_nPriority << 2 + 0;
			}
		}

		bool operator<(const pairStruct& a) const
		{
			return m_llPriority > a.m_llPriority;
		}
};

//pairStruct::pairStruct(ICE_Candidate local, ICE_Candidate remote)


//void pairStruct::computePriority(bool isControlling)


#endif
