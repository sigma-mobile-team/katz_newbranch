// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_SDP_HXX
#define QILEX_SDP_HXX


#include <resip/stack/SdpContents.hxx>
#include <string>
#include <boost/shared_ptr.hpp>

#include "CodecDescriptor.h"
#include "ICE_Candidate.hxx"

class QilexNET;
class QilexSdpSecure;
class IStreamTransport;

using namespace resip;

class QilexSdp
{
public:
	QilexSdp();
	static std::string getLocalIP();

	static void setupSession(SdpContents & session, const char* outIp, bool sendOnly);
	static void setupMedium(SdpContents::Session::Medium & medium, int outPort,bool enableSecurity);
	static void setupRtcp(SdpContents::Session::Medium & medium, const std::string & outIp, const int outPort);
	static void makeSdpOffer(SdpContents & session, QilexNET& nat, bool suspended, bool iceSupported, bool bicemismatch, bool bNoRelayed, QilexSdpSecure & pSdpSecure, bool & bConnectedAsRelayed,bool bEnableRtcp);
	static bool checkForSuspend(const SdpContents::Session & session);
	static bool getConnectionParams(const SdpContents::Session & session, std::string & remoteIP, unsigned int & remotePort, std::string & remoteRtcpIP, unsigned int & remoteRtcpPort, SdpContents::Session::MediumContainer::const_iterator& mediaIter);
	static bool setupCodec(const SdpContents::Session & session, SDPCodecDescriptor& codecDescriptor);
	static void checkForCandidates(const SdpContents::Session & session, std::string & strCandidateIP, unsigned short & nCandidatePort, bool & mismatch, bool & icesupport);
	static bool checkForNoRelayed(const SdpContents::Session & session);
	static void matchCodecFromOffer(const SdpContents::Session & session, SdpContents::Session & contents_session, SdpContents::Session::Medium & sdpMedium, SDPCodecDescriptor& codecDescriptor, QilexSdpSecure& pSdpSecure);



protected:
	static void addICEAttributes(SdpContents::Session & session, SdpContents::Session::Medium & medium, bool bICE, bool bICEmismatch);
	static bool checkMediumName(const SdpContents::Session::Medium & m);
	static bool checkMediumProtocol(const SdpContents::Session::Medium & m);
	static void offerCandidates(SdpContents::Session::Medium & medium, bool bICE, std::set<ICE_Candidate>::iterator iter, std::set<ICE_Candidate>::iterator end);
};

#endif
