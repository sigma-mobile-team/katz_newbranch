// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_CONFIG_HXX
#define QILEX_CONFIG_HXX

#include <vector>
#include <string>

class QilexConfig
{
public:
	static const unsigned short getStunServerPort() { return stunServerPort; }
	static const unsigned short getTrunServerPort() { return turnServerPort; }
	static const int getSamplesPerFrame() { return sampleRate/50; }
	static const int getSamplesPerSecond() { return sampleRate; }
	static const int getBuffersToBufferOnOutput() { return 150; }
	static const int getBuffersToBufferOnInput() { return 50; }
	static const int getReRegisterTime() { return reRegisterTime; }
	static const int getPresenceSubscriptionExpire() { return presenceSubscriptionExpire; }
	static const int getPresencePublicationExpire() { return presencePublicationExpire; }
	
	static void setReRegisterTime(int t) { reRegisterTime = t; }
	static void setPresenceSubscriptionExpire(int s) { presenceSubscriptionExpire = s; }
	static void setPresencePublicationExpire(int p) { presencePublicationExpire = p; }
	static void setSamplesPerSecond(int s) { sampleRate = s; }

protected:
	static int sampleRate;
	static int turnServerPort;
	static int stunServerPort;
	static int reRegisterTime;
	static int presencePublicationExpire;
	static int presenceSubscriptionExpire;
};

#endif
