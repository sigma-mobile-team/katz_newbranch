#ifndef ICE_Candidate_HXX
#define ICE_Candidate_HXX


#include <string>
#include "ICEtypes.hxx"

class ICE_Candidate
{
	public:
		std::string				m_strIP;
		unsigned short			m_nPort;
		mutable ICEtypes::ICE_ADDRESS_TYPE	m_eType;
		mutable unsigned long int			m_nPriority;
		unsigned char			m_nFoundation;	
		//componentID
		//LocalPreference
		
		bool operator < (const ICE_Candidate& right) const 
		{ 
			if (m_strIP == right.m_strIP && m_nPort == right.m_nPort)
				return false;

			if (m_nPriority >  right.m_nPriority) return true;
			if (m_nPriority == right.m_nPriority)
			{
				if (m_strIP <  right.m_strIP) return true;
				if (m_strIP == right.m_strIP)
				{
					if (m_nPort <  right.m_nPort) return true;
				}
			}
			return false; 
		}
};
#endif
