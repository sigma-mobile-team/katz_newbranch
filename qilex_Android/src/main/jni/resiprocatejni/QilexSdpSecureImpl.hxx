// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_SDP_SECURE_IMPL_HXX
#define QILEX_SDP_SECURE_IMPL_HXX

#include "QilexSdpSecure.hxx"
#include <map>
#include "srtp.h"

class QilexSdpSecureImpl : public QilexSdpSecure
{
public:
	QilexSdpSecureImpl();
	virtual void generateKey(SdpContents::Session::Medium & sdpMedium);
	virtual void setCryptoInMedium(SdpContents::Session::Medium & sdpMedium, const std::string & ssCipherData);
	virtual bool preprateCipherData(SdpContents::Session::MediumContainer::const_iterator & iter, std::string & strCipherData);
	virtual bool setConnectionCipher(SdpContents::Session::MediumContainer::const_iterator& iter);
	virtual bool secureStreamTransport(IStreamTransport & streamtransport);
	virtual bool getSecureStreamTransport(IStreamTransport & streamtransport);

protected:
	void generateKeyPriv(SdpContents::Session::Medium & sdpMedium, std::string keyname, unsigned char no);

	std::map<unsigned char, resip::Data> m_mSrtpOutKeys;
	std::list<Data> lCrypto;
	int nTag;
	srtp_t pSSRC;
	unsigned int inSSRC;
};

#endif
