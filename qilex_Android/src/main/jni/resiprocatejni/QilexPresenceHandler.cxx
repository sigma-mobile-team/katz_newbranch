// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexPresenceHandler.hxx"
#include "QilexPresenceManager.hxx"
#include "QilexRegistrationManager.hxx"
#include "QilexLogger.hxx"
#include <resip/dum/ClientSubscription.hxx>
#include <resip/dum/AppDialogSet.hxx>
#include <resip/stack/Pidf.hxx>
#include <string>
#include "QilexFakeDNS.hxx"

const int PRESENCE_SUBSCRIPTION_RETRY_AFTER = 120;
const int PRESENCE_PUBLISH_RETRY_AFTER = 5;

using namespace std;


void QilexPresenceHandler::onSuccess(resip::ClientPublicationHandle handle, const resip::SipMessage& status)
{
  CALL_TRACK;
  QilexPresenceManager::instance().setClientPublicationHandle(handle);
}


void QilexPresenceHandler::onRemove(resip::ClientPublicationHandle, const resip::SipMessage& status)
{
  CALL_TRACK;
}


void QilexPresenceHandler::onFailure(resip::ClientPublicationHandle, const resip::SipMessage& status)
{
  CALL_TRACK;
}


int QilexPresenceHandler::onRequestRetry(resip::ClientPublicationHandle, int retrySeconds, const resip::SipMessage& status)
{
	CALL_TRACK;
	return PRESENCE_PUBLISH_RETRY_AFTER;
}


// from ClientSubscriptionHandler


void QilexPresenceHandler::onUpdatePending(resip::ClientSubscriptionHandle handle, const resip::SipMessage& notify, bool outOfOrder)
{
	CALL_TRACK;
	handle->acceptUpdate();
}


void QilexPresenceHandler::onUpdateActive(resip::ClientSubscriptionHandle handle, const resip::SipMessage& notify, bool outOfOrder)
{  
	CALL_TRACK;
	if (outOfOrder)
    {
		SIPXLOGI("QilexPresenceHandler::onUpdateActive out of order notify - ignoring");
		handle->acceptUpdate();
		return;
	}

	QilexStatemachine sm;
	string peer = notify.header(resip::h_From).uri().toString().c_str();

	resip::Contents* contents = notify.getContents();
	if (!contents)
    {
		sm.addPresenceUpdate(peer, NOT_AVAILABLE, std::string(""));
		sm.sendEvent(QilexStatemachine::EVENT_PRESENCESUBSCRIPTIONUPDATE);

		handle->acceptUpdate();
		return;
    }

	if (contents->getType() == resip::Mime("application", "pidf+xml") && contents->isWellFormed())
    {
		resip::Pidf* pidf = dynamic_cast<resip::Pidf*>(contents);
      
		if (pidf)
		{
			Status status = HIDDEN;

			if (pidf->getSimpleStatus())
				status = AVAILABLE;

			sm.addPresenceUpdate(peer, (int)status, std::string(""));
			sm.sendEvent(QilexStatemachine::EVENT_PRESENCESUBSCRIPTIONUPDATE);
			handle->acceptUpdate();
		}
		else
			handle->rejectUpdate();
    }
	else
		handle->rejectUpdate();
}


void QilexPresenceHandler::onUpdateExtension(resip::ClientSubscriptionHandle handle, const resip::SipMessage& notify, bool outOfOrder)
{
  CALL_TRACK;
  handle->acceptUpdate();
}

  
int QilexPresenceHandler::onRequestRetry(resip::ClientSubscriptionHandle, int retrySeconds, const resip::SipMessage& notify)
{
  CALL_TRACK;
  return PRESENCE_SUBSCRIPTION_RETRY_AFTER;
}
  

void QilexPresenceHandler::onTerminated(resip::ClientSubscriptionHandle handle, const resip::SipMessage* msg)
{
	CALL_TRACK;

	handle->end();
	handle->getAppDialogSet()->end();

	if (!msg)
    {
      QilexPresenceManager::instance().terminateHandler(handle);
      return;
    }

	QilexStatemachine sm;
	std::string peer = msg->header(resip::h_From).uri().toString().c_str();
	sm.addPresenceUnsubscribed(peer);
	sm.sendEvent(QilexStatemachine::EVENT_PRESENCESUBSCRIPTIONTERMINATED);
}


void QilexPresenceHandler::onNewSubscription(resip::ClientSubscriptionHandle handle, const resip::SipMessage& notify)
{
  CALL_TRACK;
  QilexPresenceManager& presenceManager = QilexPresenceManager::instance();
  
  std::string peer = notify.header(resip::h_From).uri().toString().c_str();
  LOGI("%s", peer.c_str());
  //  presenceManager.presenceSubscriptionAdded(peer,handle);
  // QilexStatemachine sm;
  // sm.addPresenceSubscribed(peer);
  // sm.sendEvent(QilexStatemachine::EVENT_PRESENCESUBSCRIBED);
  size_t pos = peer.find("@");
  if (pos!=std::string::npos)
    {
      peer.replace(pos+1, peer.size()-pos, DEFAULT_SIP_HOST );
      pos = peer.find(":");
      peer.replace(0,pos,"sips");
      LOGI("%s", peer.c_str());
      
      presenceManager.presenceSubscriptionAdded(peer,handle);
      QilexStatemachine sm;
      sm.addPresenceSubscribed(peer);
      sm.sendEvent(QilexStatemachine::EVENT_PRESENCESUBSCRIBED);
    }
}

  


