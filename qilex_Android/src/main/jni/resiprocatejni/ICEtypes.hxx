#ifndef ICEtypes_HXX
#define ICEtypes_HXX

namespace ICEtypes
{

	enum ICE_ADDRESS_TYPE
	{
		RELAYED_TYPE = 0,
		REFLEXIVE_TYPE = 63,
		UPNP_TYPE = 100,
		UPNP_REFLEXIVE_TYPE = 110,
		HOST_TYPE = 120,
		HOST_REFLEXIVE_TYPE = 126
	};
};

#endif
