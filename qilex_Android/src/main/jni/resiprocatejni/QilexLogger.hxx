// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_LOGGER_HXX
#define QILEX_LOGGER_HXX

#include "QilexStatemachine.hxx"
#include <android/log.h>
#include <string>
#include <jni.h>

#define LOG_TAG "resiprocatejni"
#define COMMONLIB_TAG "commonlib"
#define AUDIOM_TAG "audiomgr"
#define SIPX_LOG_TAG "sipX"
#define SRTP_LOG_TAG "srtp"
#define TLS_LOG_TAG "tlsSSL"
#define STREAMTRLIB_TAG "streamtr"


// log for rx/tx RTP sequence for some packets, causes crash 
// #define LOG_RTP_RXTX_SEQUENCES 

//#ifdef DEBUG

//#define ENABLE_CALL_TRACK
//#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
//#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
//#define SIPXLOGI(...) __android_log_print(ANDROID_LOG_INFO,SIPX_LOG_TAG,__VA_ARGS__)
//#define SIPXLOGE(...) __android_log_print(ANDROID_LOG_ERROR,SIPX_LOG_TAG,__VA_ARGS__)
//#define AUDIOMLOGI(...) __android_log_print(ANDROID_LOG_INFO,AUDIOM_LOG_TAG,__VA_ARGS__)
//#define AUDIOMLOGE(...) __android_log_print(ANDROID_LOG_ERROR,AUDIOM_LOG_TAG,__VA_ARGS__)
//#define COMMLIBLOGI(...) __android_log_print(ANDROID_LOG_INFO,COMMONLIB_TAG,__VA_ARGS__)
//#define COMMLIBLOGE(...) __android_log_print(ANDROID_LOG_ERROR,COMMONLIB_TAG,__VA_ARGS__)
//#define STREAMTRLOGI(...) __android_log_print(ANDROID_LOG_INFO,STREAMTRLIB_TAG,__VA_ARGS__)
//#define STREAMTRLOGE(...) __android_log_print(ANDROID_LOG_ERROR,STREAMTRLIB_TAG,__VA_ARGS__)
//#define SRTPLOGI(...) __android_log_print(ANDROID_LOG_INFO,SRTP_LOG_TAG,__VA_ARGS__)
//#define SRTPLOGE(...) __android_log_print(ANDROID_LOG_ERROR,SRTP_LOG_TAG,__VA_ARGS__)
//#define TLSLOGI(...) __android_log_print(ANDROID_LOG_INFO,TLS_LOG_TAG,__VA_ARGS__)
//#define TLSLOGE(...) __android_log_print(ANDROID_LOG_ERROR,TLS_LOG_TAG,__VA_ARGS__)

//#else

#define LOGI(...)
#define LOGE(...)
#define SIPXLOGI(...)
#define SIPXLOGE(...)
#define AUDIOMLOGI(...)
#define AUDIOMLOGE(...)
#define COMMLIBLOGI(...)
#define COMMLIBLOGE(...)
#define STREAMTRLOGI(...)
#define STREAMTRLOGE(...)
#define SRTPLOGI(...)
#define SRTPLOGE(...)
#define TLSLOGI(...) 
#define TLSLOGE(...) 

//#endif


#define LOGAPP(msg) {	  \
  QilexStatemachine __sm; \
  __sm.logMsg(msg);	  \
  }
#define LOGAPPKEYVALUE(key,value) { \
  QilexStatemachine __sm;	    \
  __sm.logKeyValue(key,value);	    \
  }



class QilexCallLogger
{
public:
  inline QilexCallLogger(const char* file, const char* func)
    :mFile(file),
     mFunc(func)
  {
    
    std::string logMsg;
    logMsg += mFile;
    logMsg += " > ";
    logMsg += mFunc;
    LOGI("%s", logMsg.c_str());
  }

  inline ~QilexCallLogger()
  {
 std::string logMsg;
    logMsg += mFile;
    logMsg += " < ";
    logMsg += mFunc;
    LOGI("%s", logMsg.c_str());
  }
private:
  std::string mFile;
  std::string mFunc;
};






#ifdef ENABLE_CALL_TRACK
#define CALL_TRACK QilexCallLogger _myQilexCallLogger(__FILE__,__func__);
#else
#define CALL_TRACK
#endif // ENABLE_CALL_TRACK

#endif //QILEX_LOGGER_HXX
