#ifndef QILEX_CRYPTO_HXX
#define QILEX_CRYPTO_HXX

#include <boost/shared_ptr.hpp>

class QilexCrypto {
public:
	QilexCrypto(void);
	virtual ~QilexCrypto(void);
	static QilexCrypto* Get();
	const char* getCertificateRequestPEM();
	const char* getPublicKey();
	const char* getPrivateKey();
	boost::shared_ptr<QilexCrypto> self() {
		return m_pSelf;
	}
private:
	void setSelf(QilexCrypto* inst);
	char* initCrypto();
private:
	static QilexCrypto* instance;
	char* mCertRequestPEM;
	char* mPrivateKey;
	char* mPublicKey;

	boost::shared_ptr<QilexCrypto> m_pSelf;
};
#endif
