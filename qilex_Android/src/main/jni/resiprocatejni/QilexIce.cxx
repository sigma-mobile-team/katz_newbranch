// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexIce.hxx"
#include "QilexLogger.hxx"
#include "ICEtypes.hxx"

#include <list>
#include <algorithm>
#include <ctime>

#include <boost/system/error_code.hpp>

#include <reTurn/client/TurnUdpSocket.hxx>
#include <reTurn/StunTuple.hxx>

#include <rutil/Data.hxx>
#include <rutil/DnsUtil.hxx>

#define TURN_SERVER_PORT 3480

using namespace reTurn;
using namespace resip;


QilexIce::QilexIce(const std::string& address, unsigned short port)
  :mIoService(),
   mReTurnConfig(),
   mTurnManager(mIoService, mReTurnConfig),
   mRequestHandler(mTurnManager, &mReTurnConfig.mTurnAddress, &mReTurnConfig.mTurnPort, &mReTurnConfig.mAltStunAddress, &mReTurnConfig.mAltStunPort)
{

  mReTurnConfig.mTurnPort = port;
  mReTurnConfig.mTurnAddress = boost::asio::ip::address::from_string(address);

  try
  {
    mServer.reset(new reTurn::UdpServer(mIoService, mRequestHandler, mReTurnConfig.mTurnAddress, mReTurnConfig.mTurnPort));
    mServer->start();
    boost::shared_ptr<boost::asio::detail::thread> thread(new boost::asio::detail::thread(boost::bind(&boost::asio::io_service::run, &mIoService)));

    /*Dorobić dodawanie nowego peer kandydata jesli okaze sie, ze jest za symetrycznym NAT'em*/

  }
  catch(std::runtime_error& e)
  {
    std::string msg;
    msg += "STUN server ERROR: ";
    msg += e.what();
    LOGE("%s", msg.c_str());
  }

}

void QilexIce::GatherCandidates()
{
	//przygotowanie do stworzenia listy kandydatów
	m_Candidates.clear();

	ICE_Candidate tmpCandidate;
	unsigned char nFoundation=1;

	boost::system::error_code rc;
	StunTuple tupleReflex, tupleRelay;

	// pobranie lokalnych kandydatów

	getLocalIP();

	if(m_localIP!="0.0.0.0" && !m_localIP.empty())
	{
		tmpCandidate.m_strIP = m_localIP;
		tmpCandidate.m_nPort = m_localPort;
		tmpCandidate.m_eType = ICEtypes::HOST_TYPE;
		tmpCandidate.m_nFoundation = nFoundation;
		tmpCandidate.m_nPriority = (tmpCandidate.m_eType<<24) + (0xFFFF<<8) + (0xFF);
		//tmpCandidate.m_nPriority = tmpCandidate.m_eType<<24 + LocalPref<<8 + (256 - componentID);  // preferencje co do

		unsigned int nLastSize = m_Candidates.size();
		m_Candidates.insert(tmpCandidate);
		if (m_Candidates.size() > nLastSize) //if added unique address
			++nFoundation;
	}

	//GatherCandidatesFromStunTurn();
}

void QilexIce::GatherCandidatesFromStunTurn()
{
	/*
	//połączenie ze STUN I TURN i pobranie danych
	TurnUdpSocket turnSocket(boost::asio::ip::address::from_string(m_localIP), m_localPort); // otwieramy socket

	//rc = turnSocket.connect("stun.sipgate.net",10000); //przykładowy server stun
	rc = turnSocket.connect("sip.vm.redefine.pl", TURN_SERVER_PORT);
	if(rc)
	{
		std::string resp;
		resp += "CLIENT: Error calling connect: rc=";
		resp += rc.message();
		LOGI(resp.c_str());
		return;
	}

	std::string username = "redefine";
	std::string password = "redefine";

	turnSocket.setUsernameAndPassword(username.c_str(), password.c_str());// login-hasło do servera stun i turn


	//bind request do STUN'a
	rc = turnSocket.bindRequest();
	if(rc)
	{
		std::string resp;
	 	resp += "CLIENT: Error calling bindRequest: rc=";
	 	resp += rc.message().c_str(); 
	 	resp += ", value=";
	 	resp += rc.value();
	 	LOGI(resp.c_str());
	 	return;
	}
	else
	{
		tupleReflex = turnSocket.getReflexiveTuple();
		std::string reflexiveIP = tupleReflex.getAddress().to_string();
		unsigned short reflexivePort = tupleReflex.getPort();

		//dodanie do listy
		tmpCandidate.m_strIP = reflexiveIP;
		tmpCandidate.m_nPort = reflexivePort;
		tmpCandidate.m_eType = ICEtypes::REFLEXIVE_TYPE; 
		tmpCandidate.m_nFoundation = nFoundation;

		tmpCandidate.m_nPriority = (tmpCandidate.m_eType<<24) + (0xFFFF << 8) + 0xFF;
		//tmpCandidate.m_nPriority = tmpCandidate.m_eType<<24 + LocalPref<<8 + (256 - componentID);
 		//LocalPref - preferencje co do servera STUN - narazie jest tylko jeden, ComponentID - RTP || RTCP
		
		unsigned int nLastSize = m_Candidates.size();
		m_Candidates.insert(tmpCandidate);
		if (m_Candidates.size() > nLastSize) //if added unique address
			++nFoundation;

		
	}

	//Allocate dla TURN'a
	rc = turnSocket.createAllocation( //30,
									TurnSocket::UnspecifiedLifetime,
                                       TurnSocket::UnspecifiedBandwidth, 
                                       StunMessage::PropsPortPair,
                                       TurnSocket::UnspecifiedToken,
                                       StunTuple::UDP);

	if(rc)
	{
		std::string resp;
		resp += "CLIENT: Error creating allocation: rc=";
		resp += rc.message();
		LOGI(resp.c_str());
	}
	else
	{
		tupleRelay = turnSocket.getRelayTuple();
		std::string relayIP = tupleRelay.getAddress().to_string();
		unsigned short relayPort = tupleRelay.getPort();

		tmpCandidate.m_strIP = relayIP;
		tmpCandidate.m_nPort = relayPort;
		tmpCandidate.m_eType = ICEtypes::RELAYED_TYPE; 
		tmpCandidate.m_nFoundation = nFoundation;
		
		tmpCandidate.m_nPriority = (tmpCandidate.m_eType<<24) + (0xFFFF<<8) + 0xFF;
		//tmpCandidate.m_nPriority = tmpCandidate.m_eType<<24 + LocalPref<<8 + (256 - componentID); 
		// preferencje co do servera TURN narazie jest tylko jeden, ComponentID = 1 bo narazie tylko RTP
		unsigned int nLastSize = m_Candidates.size();
		m_Candidates.insert(tmpCandidate);
		if (m_Candidates.size() > nLastSize) //if added unique address
			++nFoundation;


		tupleReflex = turnSocket.getReflexiveTuple();
		std::string reflexiveIP = tupleReflex.getAddress().to_string();
		unsigned short reflexivePort = tupleReflex.getPort();

		tmpCandidate.m_strIP = reflexiveIP;
		tmpCandidate.m_nPort = reflexivePort;
		tmpCandidate.m_eType = ICEtypes::REFLEXIVE_TYPE;
		tmpCandidate.m_nFoundation = nFoundation;

		tmpCandidate.m_nPriority = (tmpCandidate.m_eType<<24) + (0xFFFF<<8) + 0xFF;
		//tmpCandidate.m_nPriority = tmpCandidate.m_eType<<24 + LocalPref<<8 + (256 - componentID);
		// preferencje co do servera TURN narazie jest tylko jeden, ComponentID = 1 bo narazie tylko RTP
		nLastSize = m_Candidates.size();
		m_Candidates.insert(tmpCandidate);
		if (m_Candidates.size() > nLastSize) //if added unique address
			++nFoundation;
	}
	*/
}

void QilexIce::getLocalIP()
{
  std::list< std::pair< Data, Data > > interfaces;
  std::list< std::pair< Data, Data > >::iterator i;


  interfaces = DnsUtil::getInterfaces();
  for ( i = interfaces.begin() ; i!= interfaces.end(); i++ )
  {
    m_localIP = (*i).second.c_str();
  }
	
	m_localPort = 6000; //przypisane na sztywno

}

void QilexIce::DecodeSDP(SdpContents& sdp)
{
	m_peerCandidates.clear();

	ICE_Candidate tmpICECandidate;
	SdpContents::Session::MediumContainer::const_iterator iterMedia;
	for (iterMedia=sdp.session().media().begin(); iterMedia!=sdp.session().media().end(); ++iterMedia)
	{
		std::list<Data> Candidtes = (*iterMedia).getValues("candidate");
		for (std::list<Data>::iterator iterAttr=Candidtes.begin(); iterAttr!=Candidtes.end(); ++iterAttr)
		{
			std::string tmpStr;
			std::string ICEtyp;
			std::istringstream ssReader((*iterAttr).c_str());
			ssReader >> tmpICECandidate.m_nFoundation >> tmpStr >> tmpStr >> tmpICECandidate.m_nPriority >> tmpICECandidate.m_strIP >> tmpICECandidate.m_nPort >> tmpStr >> ICEtyp;

			if(ICEtyp == "host")
				tmpICECandidate.m_eType = ICEtypes::HOST_TYPE;
			else if(ICEtyp == "relay")
				tmpICECandidate.m_eType = ICEtypes::RELAYED_TYPE;
			else 
				tmpICECandidate.m_eType = ICEtypes::REFLEXIVE_TYPE;
			
			m_peerCandidates.insert(tmpICECandidate);
		}
	}
}


//wysłanie ICE_Candidates w SDP offer jako a=cantidates:nFoundation ComponentID UDP priority str_IP nPost typ eType
void QilexIce::MakeCandidateSDP(SdpContents::Session::Medium &sdpMedium)
{
	if(m_Candidates.empty())
		LOGE("QilexIce::MakeCandidateSDP no cands");
	
	for (std::set<ICE_Candidate>::iterator iter=m_Candidates.begin(); iter!=m_Candidates.end(); ++iter)
	{
		ICE_Candidate candidate = *iter;
		std::stringstream ssData;
		
		ssData << (int)candidate.m_nFoundation << " 1 UDP " << candidate.m_nPriority << " " << candidate.m_strIP << " " << candidate.m_nPort << " typ "; // componentID = 1
		if (candidate.m_eType == ICEtypes::HOST_TYPE || candidate.m_eType == ICEtypes::HOST_REFLEXIVE_TYPE)
			ssData << "host";
		else if (candidate.m_eType == ICEtypes::RELAYED_TYPE)
			ssData << "relay";
		else
			ssData << "srflx";

		sdpMedium.addAttribute("candidate", ssData.str().c_str());

		LOGI("QilexIce::MakeCandidateSDP  %s", ssData.str().c_str());
	}

}

//isControlling - czy Agent jest "inicjującym" rozmowę
void QilexIce::Pairing(bool isControlling)
{

	//pair and proritize
	for (std::set<ICE_Candidate>::iterator iterL=m_Candidates.begin(); iterL!=m_Candidates.end(); ++iterL)
	{
	
		ICE_Candidate candidateLocal = *iterL;
	
		for (std::set<ICE_Candidate>::iterator iterR=m_peerCandidates.begin(); iterR!=m_peerCandidates.end(); ++iterR)
		{
		
			ICE_Candidate candidateRemote = *iterR;
		
			pairStruct tmpTab(candidateLocal,candidateRemote);
			tmpTab.computePriority(isControlling);
		
			m_CandidatesPairs.push_back(tmpTab);
		
		}
	}	
	//sort
	std::sort(m_CandidatesPairs.begin(), m_CandidatesPairs.end());

	//Replace local candidates with their bases
	for(std::vector<pairStruct>::iterator iter = m_CandidatesPairs.begin(); iter !=m_CandidatesPairs.end(); ++iter)
	{
		pairStruct tmp = *iter;
		if(tmp.m_Local.m_eType == ICEtypes::REFLEXIVE_TYPE)
		{
			tmp.m_Local.m_strIP = m_localIP;
			tmp.m_Local.m_nPort	= m_localPort;
			tmp.m_Local.m_eType = ICEtypes::HOST_TYPE;
		}

	}
	//prune duplicates
	std::vector<pairStruct>::iterator it = m_CandidatesPairs.begin();

	for(;;)
	{
		if(it == m_CandidatesPairs.end())
			break;

		std::vector<pairStruct>::iterator it2 = it+1;
	
		for(;;)
		{
			if(it2 == m_CandidatesPairs.end())
				break;
			else
			{
				pairStruct tmp = *it;
				pairStruct tmp2 = *it2;

				if((tmp.m_Local.m_eType == tmp2.m_Local.m_eType) && (tmp.m_Remote.m_eType == tmp2.m_Remote.m_eType) && (tmp.m_llPriority >= tmp2.m_llPriority))
				{
					m_CandidatesPairs.erase(it2);
					it2 = it+1;
				}
				else
				{
					it2++;
				}
			}

		}
		it++;
	}
}

//TODO: check connections
void QilexIce::CheckConnection()
{
	
	boost::system::error_code rc;
	StunTuple tuple;
	ICE_Candidate tmpCandidate;

	wait(20);

	for(std::vector<pairStruct>::iterator iter = m_CandidatesPairs.begin(); iter!=m_CandidatesPairs.end(); ++iter)
	{
		pairStruct tmpStruct = *iter;
		
		TurnUdpSocket turnSocket(boost::asio::ip::address::from_string(tmpStruct.m_Local.m_strIP), tmpStruct.m_Local.m_nPort);
		
		rc = turnSocket.connect(tmpStruct.m_Remote.m_strIP, tmpStruct.m_Remote.m_nPort);
		if(rc)
		{
			std::string resp;
			resp += "CLIENT: Error calling connect: rc=";
			resp += rc.message();
			LOGI("%s", resp.c_str());
			return;
		}
		else
		{
			rc = turnSocket.bindRequest();
			if(rc)
			{
				std::string resp;
				resp += "CLIENT: Error calling bindRequest: rc=";
				resp += rc.message().c_str();
				resp += ", value=";
				resp += rc.value();
				LOGI("%s", resp.c_str());
				return;
			}
			else
			{
				/*tuple = turnSocket.getReflexiveTuple();
				std::string reflexiveIP = tuple.getAddress().to_string();
				unsigned short reflexivePort = tuple.getPort();

				tmpCandidate.m_strIP = reflexiveIP;
				tmpCandidate.m_nPort = reflexivePort;
				tmpCandidate.m_eType = ICEtypes::PEER_REFLEXIVE_TYPE;
				tmpCandidate.m_nFoundation = '1';//?
				//tmpCandidate.m_nPriority = tmpCandidate.m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
				tmpCandidate.m_nPriority = (tmpCandidate.m_eType<<24) + (0xFFFF<<8) + 0xFF; //?
				//tmpCandidate.m_nPriority = tmpCandidate.m_eType<<24 + LocalPref<<8 + (256 - componentID);

				unsigned int nLastSize = m_Candidates.size();
				m_Candidates.insert(tmpCandidate);
				//?if (m_sICE_Candidates.size() > nLastSize) //if added unique address
				//?	++nFoundation;*/

				tmpStruct.m_bChecked = true;
				m_CandidatesPairs.erase(iter);
				m_CandidatesPairs.insert(iter, tmpStruct);

			}
		}
	}
}

void QilexIce::wait(int miliseconds)
{
  clock_t endwait;
  endwait = clock () + (miliseconds * CLOCKS_PER_SEC)/1000 ;
  while (clock() < endwait) {}
}

void QilexIce::ChooseBestCandidate()
{
	m_strBestCandIP = "";
	m_nBestCandPort = 0;
	bool bRelayed = false;

	if(m_Candidates.empty())
		return;

	//use best UPNP_REFLEXIVE, if any
	for (std::set<ICE_Candidate>::iterator iter=m_Candidates.begin(); iter!=m_Candidates.end(); ++iter)
	{
		if ((*iter).m_eType == ICEtypes::UPNP_REFLEXIVE_TYPE)
		{
			m_strBestCandIP = (*iter).m_strIP;
			m_nBestCandPort = (*iter).m_nPort;
			break;
		}
	}

	//if no UPNP_REFLEXIVE, use best HOST_REFLEXIVE, if any
	if (!m_nBestCandPort)
	{
		for (std::set<ICE_Candidate>::iterator iter=m_Candidates.begin(); iter!=m_Candidates.end(); ++iter)
		{
			if ((*iter).m_eType == ICEtypes::HOST_REFLEXIVE_TYPE)
			{
				m_strBestCandIP = (*iter).m_strIP;
				m_nBestCandPort = (*iter).m_nPort;
				break;
			}
		}
	}

	//if no HOST_REFLEXIVE, use best RELAYED, if any
	if (!m_nBestCandPort)
	{
		for (std::set<ICE_Candidate>::iterator iter=m_Candidates.begin(); iter!=m_Candidates.end(); ++iter)
		{
			if ((*iter).m_eType == ICEtypes::RELAYED_TYPE)
			{
				m_strBestCandIP = (*iter).m_strIP;
				m_nBestCandPort = (*iter).m_nPort;
				bRelayed = true;
				break;
			}
		}
	}

	//if no RELAYED, use best UPNP, if any
	if (!m_nBestCandPort)
	{
		for (std::set<ICE_Candidate>::iterator iter=m_Candidates.begin(); iter!=m_Candidates.end(); ++iter)
		{
			if ((*iter).m_eType == ICEtypes::UPNP_TYPE)
			{
				m_strBestCandIP = (*iter).m_strIP;
				m_nBestCandPort = (*iter).m_nPort;
				break;
			}
		}
	}

	//if no UPNP, use best REFLEXIVE, if any
	if (!m_nBestCandPort)
	{
		for (std::set<ICE_Candidate>::iterator iter=m_Candidates.begin(); iter!=m_Candidates.end(); ++iter)
		{
			if ((*iter).m_eType == ICEtypes::REFLEXIVE_TYPE)
			{
				m_strBestCandIP = (*iter).m_strIP;
				m_nBestCandPort = (*iter).m_nPort;
				break;
			}
		}
	}

	//if no REFLEXIVE, use whatever aviable!
	if (!m_nBestCandPort && !m_Candidates.empty())
	{
		m_strBestCandIP = (*m_Candidates.rbegin()).m_strIP;
		m_nBestCandPort = (*m_Candidates.rbegin()).m_nPort;
		bRelayed = (*m_Candidates.rbegin()).m_eType==ICEtypes::RELAYED_TYPE ? true : false;
	}

	LOGI("ICE: Best cand is %s %d", m_strBestCandIP.c_str(), m_nBestCandPort);
}

/*
//TODO: Główna funkcja ICE'a
void Qilex::doICE()
{
	Pairing();
	CheckConnection();
	EstablishConnection();
}
*/
