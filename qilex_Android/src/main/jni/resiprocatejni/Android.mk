# Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := freefonjni

LOCAL_CPP_EXTENSION := .cxx
LOCAL_CFLAGS += -DUSE_SSL
LOCAL_CFLAGS += -DSIP_USE_SIPS=0
LOCAL_CFLAGS += -DSIP_SERVER_USE_SSL=1

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../resiprocate/resip/stack \
	$(LOCAL_PATH)/../resiprocate \
	$(LOCAL_PATH)/../resiprocate/contrib/srtp/include \
	$(LOCAL_PATH)/../resiprocate/contrib/srtp/crypto/include \
	$(LOCAL_PATH)/../commonlib \
	$(LOCAL_PATH)/../audiomgr \
	$(LOCAL_PATH)/../streamtransport \
	$(LOCAL_PATH)/../sipXportLib/include \
	$(LOCAL_PATH)/../sipXsdpLib/include \
	$(LOCAL_PATH)/../sipXmediaLib/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/libspeex/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/libopus/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/libspandsp/src \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/android \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/android/android_2_0_headers/frameworks/base/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/android/android_2_0_headers/system/core/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/android/android_2_0_headers/system/core/include/arch/linux-arm \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/android \
	$(LOCAL_PATH)/../boost_1_57_0/boost \
	$(LOCAL_PATH)/../boost_1_57_0 \
	$(LOCAL_PATH)/../openssl-1.0.2h/include \
	$(LOCAL_PATH)/../streamtransport \
	$(LOCAL_PATH)/../resiprocate/contrib/srtp/include \
	$(LOCAL_PATH)/../resiprocate/contrib/srtp/crypto/include



LOCAL_SRC_FILES := \
	ResiprocateJni.cxx \
	QilexResipPool.cxx \
	QilexStatemachine.cxx \
	QilexHmac.cxx \
	QilexBaseManager.cxx \
	QilexRegistrationHandler.cxx \
	QilexRegistrationManager.cxx \
	QilexCallHandler.cxx \
	QilexCallManager.cxx \
	QilexRTP.cxx \
	QilexSdp.cxx \
	QilexConfig.cxx \
	QilexIce.cxx \
	QilexNET.cxx \
	QilexMessageDecorator.cxx \
	QilexPagerMessageManager.cxx \
	QilexPagerMessageHandler.cxx \
	QilexPresenceManager.cxx \
	QilexPresenceHandler.cxx \
	QilexSdpSecureImpl.cxx \
	QilexFakeDNS.cxx \
	QilexCrypto.cxx \
	QilexOutOfDialogManager.cxx \
	QilexOutOfDialogHandler.cxx
	

LOCAL_STATIC_LIBRARIES += \
	libaudiomgr \
	libstreamtransport \
	libcommonlib \
	libsrtp \
	libdum \
	libresip \
	librutil \
	libares \
	libreflow \
	libreTurn \
	libsipXmedia \
	libsipXsdp \
	libsipXport \
	libcodec_pcmapcmu \
	libcodec_tones \
	libcodec_speex \
	libwebrtc_audio_preprocessing \
	libspeex \
	libspeexdsp \
	libcodec_opus \
	libopus \
	libboost_system \
	libssl \
	libcrypto 

LOCAL_LDLIBS += -llog


#include $(BUILD_STATIC_LIBRARY)
include $(BUILD_SHARED_LIBRARY)
