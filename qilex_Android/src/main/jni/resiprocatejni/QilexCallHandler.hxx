// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_CALL_HANDLER_HXX
#define QILEX_CALL_HANDLER_HXX

#include "QilexStatemachine.hxx"
#include <resip/dum/InviteSessionHandler.hxx>
#include <resip/stack/SdpContents.hxx>
#include <memory>
#include <string>

class QilexCallManager;
class QilexRTP;
class QilexSdp;


using namespace resip;


class QilexCallHandler : public InviteSessionHandler
{
public:
  QilexCallHandler();

  /// called when an initial INVITE or the intial response to an outoing invite  
  virtual void onNewSession(ClientInviteSessionHandle, InviteSession::OfferAnswerType oat, const SipMessage& msg);
  virtual void onNewSession(ServerInviteSessionHandle, InviteSession::OfferAnswerType oat, const SipMessage& msg);
  
      /// Received a failure response from UAS
  virtual void onFailure(ClientInviteSessionHandle, const SipMessage& msg);
  
  /// called when an in-dialog provisional response is received that contains a body
  virtual void onEarlyMedia(ClientInviteSessionHandle, const SipMessage&, const SdpContents&);
  //  virtual void onEarlyMedia(ClientInviteSessionHandle, const SipMessage&, const Contents&);
  
  /// called when dialog enters the Early state - typically after getting 18x
  virtual void onProvisional(ClientInviteSessionHandle, const SipMessage&);
  
  /// called when a dialog initiated as a UAC enters the connected state
  virtual void onConnected(ClientInviteSessionHandle, const SipMessage& msg);
  
  /// called when a dialog initiated as a UAS enters the connected state
  virtual void onConnected(InviteSessionHandle, const SipMessage& msg);
  
  /// called when ACK (with out an answer) is received for initial invite (UAS)
  virtual void onConnectedConfirmed(InviteSessionHandle, const SipMessage &msg);
  
  /** UAC gets no final response within the stale call timeout (default is 3
   * minutes). This is just a notification. After the notification is
   * called, the InviteSession will then call
   * InviteSessionHandler::terminate() */
  virtual void onStaleCallTimeout(ClientInviteSessionHandle h);
  
  /** called when an early dialog decides it wants to terminate the
   * dialog. Default behavior is to CANCEL all related early dialogs as
   * well.  */
  virtual void terminate(ClientInviteSessionHandle h);
  
  /// called when an dialog enters the terminated state - this can happen
  /// after getting a BYE, Cancel, or 4xx,5xx,6xx response - or the session
  /// times out
  virtual void onTerminated(InviteSessionHandle, InviteSessionHandler::TerminatedReason reason, const SipMessage* related=0);
  
  /// called when a fork that was created through a 1xx never receives a 2xx
  /// because another fork answered and this fork was canceled by a proxy. 
  virtual void onForkDestroyed(ClientInviteSessionHandle);
  
  /// called when a 3xx with valid targets is encountered in an early dialog     
  /// This is different then getting a 3xx in onTerminated, as another
  /// request will be attempted, so the DialogSet will not be destroyed.
  /// Basically an onTermintated that conveys more information.
  /// checking for 3xx respones in onTerminated will not work as there may
  /// be no valid targets.
  virtual void onRedirected(ClientInviteSessionHandle, const SipMessage& msg);
  
  /// called to allow app to adorn a message. default is to send immediately
  virtual void onReadyToSend(InviteSessionHandle, SipMessage& msg);
  
  /// called when an answer is received - has nothing to do with user
  /// answering the call 
  virtual void onAnswer(InviteSessionHandle, const SipMessage& msg, const SdpContents&);
  // You should only override the following method if genericOfferAnswer is true
  //  virtual void onAnswer(InviteSessionHandle, const SipMessage& msg, const Contents&);
  
  /// called when an offer is received - must send an answer soon after this
  virtual void onOffer(InviteSessionHandle, const SipMessage& msg, const SdpContents&);      
  // You should only override the following method if genericOfferAnswer is true
  //  virtual void onOffer(InviteSessionHandle, const SipMessage& msg, const Contents&);      
  
  /// called when a modified body is received in a 2xx response to a
  /// session-timer reINVITE. Under normal circumstances where the response
  /// body is unchanged from current remote body no handler is called
  virtual void onRemoteSdpChanged(InviteSessionHandle, const SipMessage& msg, const SdpContents&);
  // You should only override the following method if genericOfferAnswer is true
  virtual void onRemoteAnswerChanged(InviteSessionHandle, const SipMessage& msg, const Contents&);  

  /// Called when an error response is received for a reinvite-nobody request (via requestOffer)
  virtual void onOfferRequestRejected(InviteSessionHandle, const SipMessage& msg);
  
  /// called when an Invite w/out offer is sent, or any other context which
  /// requires an offer from the user
  virtual void onOfferRequired(InviteSessionHandle, const SipMessage& msg);      
  
  /// called if an offer in a UPDATE or re-INVITE was rejected - not real
  /// useful. A SipMessage is provided if one is available
  virtual void onOfferRejected(InviteSessionHandle, const SipMessage* msg);
  
  /// called when INFO message is received 
  virtual void onInfo(InviteSessionHandle, const SipMessage& msg);
  
  /// called when response to INFO message is received 
  virtual void onInfoSuccess(InviteSessionHandle, const SipMessage& msg);
  virtual void onInfoFailure(InviteSessionHandle, const SipMessage& msg);
  
  /// called when MESSAGE message is received 
  virtual void onMessage(InviteSessionHandle, const SipMessage& msg);
  
  /// called when response to MESSAGE message is received 
  virtual void onMessageSuccess(InviteSessionHandle, const SipMessage& msg);
  virtual void onMessageFailure(InviteSessionHandle, const SipMessage& msg);
  
  /// called when an REFER message is received.  The refer is accepted or
  /// rejected using the server subscription. If the offer is accepted,
  /// DialogUsageManager::makeInviteSessionFromRefer can be used to create an
  /// InviteSession that will send notify messages using the ServerSubscription
  virtual void onRefer(InviteSessionHandle, ServerSubscriptionHandle, const SipMessage& msg);
  
  virtual void onReferNoSub(InviteSessionHandle, const SipMessage& msg);
  
  /// called when an REFER message receives a failure response 
  virtual void onReferRejected(InviteSessionHandle, const SipMessage& msg);
  
  /// called when an REFER message receives an accepted response 
  virtual void onReferAccepted(InviteSessionHandle, ClientSubscriptionHandle, const SipMessage& msg);
  
  /// called when ACK is received
  //  virtual void onAckReceived(InviteSessionHandle, const SipMessage& msg);
  
  /// default behaviour is to send a BYE to end the dialog
  virtual void onAckNotReceived(InviteSessionHandle handle);
  
  /// UAC gets no final response within the stale re-invite timeout (default is 40
  /// seconds).  Default behaviour is to send a BYE to end the dialog.
  //  virtual void onStaleReInviteTimeout(InviteSessionHandle h);
  
  /// will be called if reINVITE or UPDATE in dialog fails
  //  virtual void onIllegalNegotiation(InviteSessionHandle, const SipMessage& msg);     
  
  /// will be called if Session-Timers are used and Session Timer expires
  /// default behaviour is to send a BYE to send the dialog
  //  virtual void onSessionExpired(InviteSessionHandle);
  
  /// Called when a TCP or TLS flow to the server has terminated.  This can be caused by socket
  /// errors, or missing CRLF keep alives pong responses from the server.
  //  Called only if clientOutbound is enabled on the UserProfile and the first hop server 
  /// supports RFC5626 (outbound).
  /// Default implementation is to do nothing
  //  virtual void onFlowTerminated(InviteSessionHandle);
  
private:
  // data
  /// statemachine interface
  QilexStatemachine mStatemachine;

  /// interrupting incomming call flag
  bool mInterruptingCall;

};




#endif // QILEX_INVITE_HANDLER_HXX
