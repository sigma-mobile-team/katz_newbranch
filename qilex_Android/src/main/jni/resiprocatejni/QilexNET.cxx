#include "QilexNET.hxx"

#include <rutil/DnsUtil.hxx>
#include "rutil/Data.hxx"
#include <list>
#include <sstream>
#include <string>
#include <reTurn/client/TurnTcpSocket.hxx>
#include <reTurn/StunTuple.hxx>
#include <resip/stack/NameAddr.hxx>
#include "QilexLogger.hxx"
#include "ICEtypes.hxx"
#include "QilexConfig.hxx"
#include "QilexFakeDNS.hxx"

using namespace resip;

std::string QilexNET::m_strUser;
std::string QilexNET::m_strUserCert;
std::string QilexNET::m_strUserPrivKey;

QilexNET::QilexNET()
{
	m_nLocalPort = 0;
}

std::string QilexNET::getLocalIP()
{
	std::list< std::pair< Data, Data > > 	interfaces;
	std::list< std::pair< Data, Data > >::iterator i;

	std::string mMyIp;

	interfaces = DnsUtil::getInterfaces();
	for ( i = interfaces.begin() ; i!= interfaces.end(); i++ )
	{
		mMyIp = (*i).second.c_str();
	}

	return mMyIp;
}

void QilexNET::gatherAddresses(bool noRelayed,bool bEnableRtcp)
{
//	if (!m_sICE_Candidates.empty())
//		return;	//tmp!

	//SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed)");

	m_sICE_Candidates.clear();

	ICE_Candidate tmpCandidate;
	m_nLocalPort = 49152 + rand()%10000;
	unsigned char nFoundation=1;
	bool bReflexiveSuccess=false;


	//SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 2");

	//local
	m_strLocalIP = getLocalIP();

	if (m_strLocalIP != "0.0.0.0" && !m_strLocalIP.empty())
	{
		tmpCandidate.m_strIP = m_strLocalIP;
		tmpCandidate.m_nPort = m_nLocalPort;
		tmpCandidate.m_eType = ICEtypes::HOST_TYPE;
		tmpCandidate.m_nFoundation = nFoundation;
		tmpCandidate.m_nPriority = tmpCandidate.m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
		unsigned int nLastSize = m_sICE_Candidates.size();
		m_sICE_Candidates.insert(tmpCandidate);
		if (m_sICE_Candidates.size() > nLastSize) //if added unique address
			++nFoundation;
	}


	//SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 3");

	//tmp TODO - gather local IPs from other interfaces too
	//mobile 3g

	//STUN & TURN
    std::string strAddr = QilexFakeDNS::Lookup(DEFAULT_TURN_HOST);

    if (QilexFakeDNS::IsBlacklisted(strAddr)) // only blacklisted IPs
    	QilexFakeDNS::ResetBlacklistForHost(DEFAULT_TURN_HOST); // reset blacklist to try again with all IPs

    int nPort = DEFAULT_TURN_PORT;
    int nPos;
    
    if(!strAddr.empty())
    {
        SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) turn  from fakedns=%s", strAddr.c_str());
        if ((nPos=strAddr.find(":")) != strAddr.npos)
        {
            std::string strPort = strAddr.substr(nPos+1);
            strAddr = strAddr.substr(0, nPos);

            std::stringstream ssAddr;
            ssAddr << strPort;
            ssAddr >> nPort;
        }
    }
    else
        SIPXLOGE("QilexNET::gatherAddresses(bool noRelayed) no turn server");
    

	SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 4");

	if (!strAddr.empty() && !noRelayed && m_strAuthToken.length() > 10) //== 50
	{

		SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 4a");

		std::stringstream ssLoginToken;
//		std::string nameAddr = m_strUser.c_str();  //MakeNameAddr(m_strUser).c_str()
		ssLoginToken << NameAddr(m_strUser.c_str()).uri().user().c_str() << ";" << m_strAuthToken.substr(0,10);

		//SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 4b");
        
   		SIPXLOGI("QilexNET::gatherAddresses m_strLocalIP=%s", m_strLocalIP.c_str());
        SIPXLOGI("QilexNET::gatherAddresses m_nLocalPort=%d", m_nLocalPort);
        SIPXLOGI("QilexNET::gatherAddresses strAddr=%s", strAddr.c_str());
        SIPXLOGI("QilexNET::gatherAddresses nPort=%d", nPort);
        SIPXLOGI("QilexNET::gatherAddresses ssLoginToken=%s", ssLoginToken.str().c_str());
   		SIPXLOGI("QilexNET::gatherAddresses m_strAuthToken=%s", m_strAuthToken.c_str());
   		/*SIPXLOGI("QilexNET::gatherAddresses m_strUserCert=%s", m_strUserCert.c_str());
   		SIPXLOGI("QilexNET::gatherAddresses m_strUserPrivKey=%s", m_strUserPrivKey.c_str());*/
        
        
		while (m_pStreamTransport->Bind(m_strLocalIP, m_nLocalPort, true, bEnableRtcp, strAddr, nPort, ssLoginToken.str().c_str(), m_strAuthToken.c_str()/*, m_strUserCert, m_strUserPrivKey*/) != StreamTransportResults_Success)
		{
            SIPXLOGI("QilexNET::gatherAddresses bind failed");
            
            std::stringstream ssAddrPort;
			ssAddrPort << strAddr << ":" << nPort;
			QilexFakeDNS::AddToBlacklist(ssAddrPort.str());
			strAddr = QilexFakeDNS::Lookup(DEFAULT_TURN_HOST); // choose another non-blacklisted IP

			if (QilexFakeDNS::IsBlacklisted(strAddr))
				break; // we've already tried with all non-blacklisted IPs, retreat

			if ((nPos=strAddr.find(":")) != strAddr.npos)
			{
                std::string strPort = strAddr.substr(nPos+1);
				strAddr = strAddr.substr(0, nPos);

                std::stringstream ssAddr;
				ssAddr << strPort;
				ssAddr >> nPort;
			}
			else nPort = DEFAULT_TURN_PORT;
		}
	}
	else
	{
		SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 4a*");
		m_pStreamTransport->Bind(m_strLocalIP, m_nLocalPort, false, bEnableRtcp);
	}

	SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 5");

	try
	{
		IPAddrContainer reflexive, relayed;
		reflexive = m_pStreamTransport->getReflexiveTuple();
		relayed = m_pStreamTransport->getRelayTuple();

        SIPXLOGI("QilexNET::gatherAddresses relayed %s", relayed.m_strIP.c_str());
        
		std::string strReflexiveAddr = reflexive.m_strIP;
		unsigned short nReflexivePort =  reflexive.m_nPort;
		if (!strReflexiveAddr.empty() && strReflexiveAddr != "0.0.0.0")
		{
			bool bFoundReflexiveUPNP=false;
			bool bFoundReflexiveHost=false;
            
            SIPXLOGI("QilexNET::gatherAddresses reflexive %s", reflexive.m_strIP.c_str());            

			for (std::set<ICE_Candidate>::iterator iter=m_sICE_Candidates.begin(); iter!=m_sICE_Candidates.end(); ++iter)
			{
                SIPXLOGI("QilexNET::gatherAddresses for %s", (*iter).m_strIP.c_str());            
                
				if ((*iter).m_strIP==strReflexiveAddr && (*iter).m_nPort==nReflexivePort)
				{

					//if ((*iter).m_eType==ICEtypes::UPNP_TYPE)
					//{
					//(*iter).m_eType = ICEtypes::UPNP_REFLEXIVE_TYPE;
					//(*iter).m_nPriority = (*iter).m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
					//bFoundReflexiveUPNP = true;
					//}
					//else
					if ((*iter).m_eType==ICEtypes::HOST_TYPE)
					{
						(*iter).m_eType = ICEtypes::HOST_REFLEXIVE_TYPE;
						(*iter).m_nPriority = (*iter).m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
						bFoundReflexiveHost = true;
                        
                        SIPXLOGI("QilexNET::gatherAddresses bFoundReflexiveHost = true");                        
					}
				}
			}

			if (!bFoundReflexiveUPNP && !bFoundReflexiveHost)
			{
				tmpCandidate.m_strIP = strReflexiveAddr;
				tmpCandidate.m_nPort = nReflexivePort;
				tmpCandidate.m_eType = ICEtypes::REFLEXIVE_TYPE;
				tmpCandidate.m_nFoundation = nFoundation;
				tmpCandidate.m_nPriority = tmpCandidate.m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
				unsigned int nLastSize = m_sICE_Candidates.size();
				m_sICE_Candidates.insert(tmpCandidate);
				if (m_sICE_Candidates.size() > nLastSize) //if added unique address
					++nFoundation;
			}

			bReflexiveSuccess = true;
		}

		std::string strRelayedAddr = relayed.m_strIP;
		unsigned short nRelayedPort = relayed.m_nPort;
		if (strRelayedAddr != "0.0.0.0" && !strRelayedAddr.empty())
		{
			tmpCandidate.m_strIP = strRelayedAddr;
			tmpCandidate.m_nPort = nRelayedPort;
			tmpCandidate.m_eType = ICEtypes::RELAYED_TYPE;
			tmpCandidate.m_nFoundation = nFoundation;
			tmpCandidate.m_nPriority = tmpCandidate.m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
			unsigned int nLastSize = m_sICE_Candidates.size();
			m_sICE_Candidates.insert(tmpCandidate);
			if (m_sICE_Candidates.size() > nLastSize) //if added unique address
				++nFoundation;
		}
	}
	catch(std::runtime_error& e)
	{
		//WarningLog(<< "WRAPPER: Error connecting to STUN server: err=" << e.what() << std::endl);
		//SIPXLOGE("QilexNET::gatherAddresses");
	}

	//SIPXLOGI("QilexNET::gatherAddresses(bool noRelayed) 6");

    SIPXLOGI("QilexNET::gatherAddresses %s", m_strPublicIP.c_str());
    
	if (!bReflexiveSuccess && m_strPublicIP != m_strLocalIP && !m_strPublicIP.empty() && m_strPublicIP != "0.0.0.0")
	{
		tmpCandidate.m_strIP = m_strPublicIP;
		tmpCandidate.m_nPort = m_nLocalPort;
		tmpCandidate.m_eType = ICEtypes::REFLEXIVE_TYPE;
		tmpCandidate.m_nFoundation = nFoundation;
		tmpCandidate.m_nPriority = tmpCandidate.m_eType*0x1000000 + 0xFFFF*0x100 + 0xFF;
		m_sICE_Candidates.insert(tmpCandidate);
	}

	SIPXLOGI("QilexNET::gatherAddresses /");
}


int QilexNET::activeInterfaces()
{
  int retValue;
  retValue= DnsUtil::getInterfaces().size();
  std::stringstream ss;
  ss << retValue << " interfaces are up";
  
  LOGI("%s", ss.str().c_str());
  return retValue;
}

void QilexNET::setCrypto(const std::string &strUser, const std::string &strUserCert,const std::string &strUserPrivKey)
{
	m_strUser = strUser;
	m_strUserCert = strUserCert;
	m_strUserPrivKey = strUserPrivKey;
}

void QilexNET::getBestCandidate(std::string& strIP, unsigned short& nPort, bool & bRelayed, bool bNoRelayed)
{
	std::string strOurIP;
	int nOurPort = 0;

	//if (m_bCPuser) //for CP users always try to use TURN
	{
		//for now force to use relayed
		if (!bNoRelayed)
		{
			for (std::set<ICE_Candidate>::iterator iter=m_sICE_Candidates.begin(); iter!=m_sICE_Candidates.end(); ++iter)
			{
				if ((*iter).m_eType == ICEtypes::RELAYED_TYPE)
				{
					strOurIP = (*iter).m_strIP;
					nOurPort = (*iter).m_nPort;
					bRelayed = true;
					break;
				}
			}
		}
	}

	//use best HOST_REFLEXIVE, if any
	//if no RELAYED_TYPE, use best HOST_REFLEXIVE_TYPE, if any
	if (!nOurPort)
	for (std::set<ICE_Candidate>::iterator iter=m_sICE_Candidates.begin(); iter!=m_sICE_Candidates.end(); ++iter)
	{
		if ((*iter).m_eType == ICEtypes::HOST_REFLEXIVE_TYPE)
		{
			strOurIP = (*iter).m_strIP;
			nOurPort = (*iter).m_nPort;
			break;
		}
	}

	//use best REFLEXIVE, if any
	if (!nOurPort)
	{
		for (std::set<ICE_Candidate>::iterator iter=m_sICE_Candidates.begin(); iter!=m_sICE_Candidates.end(); ++iter)
		{
			if ((*iter).m_eType == ICEtypes::REFLEXIVE_TYPE)
			{
				strOurIP = (*iter).m_strIP;
				nOurPort = (*iter).m_nPort;
				break;
			}
		}
	}

	//if no REFLEXIVE, use whatever aviable!
	if (!nOurPort && !m_sICE_Candidates.empty())
	{
		strOurIP = (*m_sICE_Candidates.rbegin()).m_strIP;
		nOurPort = (*m_sICE_Candidates.rbegin()).m_nPort;
		bRelayed = (*m_sICE_Candidates.rbegin()).m_eType==ICEtypes::RELAYED_TYPE ? true : false;
	}

	if (!nOurPort)
	{
		nOurPort = 49152 + rand()%10000;
	}
    
    SIPXLOGI("QilexNET::getBestCandidate %s %d", strOurIP.c_str(), nOurPort);

	strIP = strOurIP;
	nPort = nOurPort;
}

void QilexNET::rebindWithoutTurn()
{
    m_pStreamTransport->Bind(m_strLocalIP, m_nLocalPort, false);
}
