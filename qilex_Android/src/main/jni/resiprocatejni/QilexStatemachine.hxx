// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef STATEMACHINE_HXX
#define STATEMACHINE_HXX

#include <string>
#include <jni.h>

/// forward declarations
namespace resip
{
  class SipMessage;
}

/**
 * An interface to statemachine running on Java side
 */
class QilexStatemachine
{
public:

  // include enum values generated from Java file
  enum Events
    {
#include "QilexEvents.gen"
    };


public:

  QilexStatemachine();
  virtual ~QilexStatemachine();

  /**
   * Keeps a handle to VM
   */
  static void setupVm(JavaVM* vm);

  /**
   * Sends an event to statemachine
   */
  void sendEvent(Events event);

  /**
   * Sets peer profile uri in statemachine context
   */
  void setPeerProfileUri(const std::string& uri);

  /**
   * Sets Call-Id in statemachine context
   */
  void setCallId(const std::string& callId);

  /**
   * Adds incoming message to received message queue
   */
  void addIncomingMessage(const std::string& from,
			  const std::string& to,
			  const std::string& id,
			  const std::string& timestamp,
			  const std::string& content,
			  const std::string& type,
			  const std::string& uuid);

  /**
     * Adds incoming message to received message queue
     */
    void addIncomingMessage(const std::string& from,
  			  const std::string& to,
  			  const std::string& id,
  			  const std::string& timestamp,
  			  const std::string& content,
  			  const std::string& type,
  			  const std::string& uuid,
  			const std::string& groupChatMsgType,
  			const std::string& groupChatUuid,
  			const std::string& groupChatParticipants,
  			const std::string& groupChatAdministrator);

  /**
   * Adds message to dispatched message queue
   */
  void addSentMessage(const std::string& id);

  /**
   * Adds message to confirmed message queue
   */
  void addConfirmedMessage(const std::string& id,const std::string& h_code);

  /**
   * Adds message to failed message queue
   */
  void addFailedMessage(const std::string& id, const std::string& strErr);

  /**
   * Adds a peer to subscribed list
   */
  void addPresenceSubscribed(const std::string& peer);

  /**
   * Adds a peer to unsubscribed queue
   */
  void addPresenceUnsubscribed(const std::string& peer);
  /**
   * Updates peer's status
   */
  void addPresenceUpdate(const std::string& id, int status, const std::string& statusMsg);

  void connectionWithoutTurn();

  /**
   * Output message to java log
   */
  void logMsg(const char* msg);

  /**
    * Log Miss Call when have incomingcall while calling
  */
  void logMissCall(const std::string& from);

  /**
   * Output key, value to java log
   */
  void logKeyValue(const char* key, const char* value);

  int getRetryTimeOutCount(){
	  return mRetryTimeOutCount;
  };

  void setRetryTimeOutCount(int retryCount){
	  mRetryTimeOutCount = retryCount;
  };
  /**
   * dettach thread
   */
  void detachThread();

  int isTestNewServer();

  void handlerPingResponse(const std::string& from, const std::string& to, int status, int offlineMinute);

  void handlerPingRequest(const std::string& from);

private:

  /**
   * attach thread
   */
  void attachThread(JNIEnv** env);

  /**
   * Sets a string value in statemachine context using particular setterMethod
   */
  void setContextString(const std::string& setterMethod, const std::string& value);

  /**
   * Sets a string value in statemachine context using particular setterMethod
   */
  void setContext2String(const std::string& setterMethod, const std::string& value1, const std::string& value2);


private: // data

  /// vm  handler
  static JavaVM* mVm;

  /// Statemachine java class
  static jclass mStatemachineClass;

  /// SipContext java class
  static jclass mSipContext;

  int mRetryTimeOutCount;
};


#endif // STATEMACHINE_HXX
