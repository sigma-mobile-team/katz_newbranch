//
//  QilexOutOfDialogManager.cpp
//  Qilek-iOS
//
//  Created by Sigma Vietnam on 7/15/15.
//  Copyright (c) 2015 Sigma Solutions. All rights reserved.
//

#include "QilexOutOfDialogManager.hxx"
#include "QilexLogger.hxx"

#include <resip/stack/Pidf.hxx>
#include <resip/dum/DialogUsageManager.hxx>

using namespace std;
using namespace resip;

QilexOutOfDialogManager* QilexOutOfDialogManager::mInstance = 0;

QilexOutOfDialogManager& QilexOutOfDialogManager::instance()
{
    CALL_TRACK;
    if (!mInstance)
    {
        mInstance = new QilexOutOfDialogManager();
        mInstance->resetHandler();
    }
    
    return *mInstance;
}

QilexOutOfDialogManager::~QilexOutOfDialogManager()
{
    CALL_TRACK;
}

void QilexOutOfDialogManager::resetHandler()
{
    CALL_TRACK;
    dum().addOutOfDialogHandler(OPTIONS, mQilexOutOfDialogHandler);
}
/// send pint to a contact
void QilexOutOfDialogManager::sendPingToContact(const std::string& peerName)
{
    CALL_TRACK;
    LOGI("QilexOutOfDialogManager::sendPingToContact ====== %s", peerName.c_str());
    try
    {
        DumLocker lock(this);
        resip::SharedPtr<resip::SipMessage> msg = dum().makeOutOfDialogRequest(resip::NameAddr(peerName.c_str()), OPTIONS);
        dum().send(msg);
    }
    catch (std::exception& e)
    {
        LOGI("QilexOutOfDialogManager::sendPingToContact error %s", e.what());
    }
    catch(...)
    {
        LOGI("QilexOutOfDialogManager::sendPingToContact exception");
    }

}

resip::OutOfDialogHandler& QilexOutOfDialogManager::outOfDialogHandler()
{
    return *mQilexOutOfDialogHandler;
}

//void QilexOutOfDialogManager::setOutOfDialogHandler(
//		resip::OutOfDialogHandler handle) {
//	CALL_TRACK
//	;
//	//DumLocker lock(this);
//	mQilexOutOfDialogHandler = handle;
//}

QilexOutOfDialogManager::QilexOutOfDialogManager()
{
    CALL_TRACK;
    mQilexOutOfDialogHandler = new QilexOutOfDialogHandler();
}

