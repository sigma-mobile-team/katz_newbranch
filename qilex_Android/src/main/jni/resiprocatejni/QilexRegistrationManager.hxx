
// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_REGISTRATION_MANAGER_HXX
#define QILEX_REGISTRATION_MANAGER_HXX

#include "resip/stack/ssl/Security.hxx"
#include "QilexRegistrationHandler.hxx"
#include "QilexBaseManager.hxx"
#include <string>

using namespace std;


class QilexCallHandler;
class QilexCallManager;

class QilexRegistrationManager : public QilexBaseManager
{

public:

  static QilexRegistrationManager& instance();

  virtual ~QilexRegistrationManager();

  void sendRegister();
  void sendUnregister();
  void updateBinding();

  void setClientRegistrationHandle(const resip::ClientRegistrationHandle& handle);
  resip::ClientRegistrationHandle& getClientRegistrationHandle();

  bool wasRegisterRequested()
  {
	  return mRegisterRequested;
  }

  /// sets user's credentials
  void setCredentials(const string& domain,
		      const string& username,
		      const string& password,
		      const string& instanceId);

  /// sets user public profile's ip address
  void setIpAddress(const string& ipAddress);
  
  /// sets token
  void setToken(const string& token);

  /// sets aliases
  void setAliases(vector<string>& aliases);

  /// gets aliases
  const vector<string>& aliases() const;

  /// KEY AND CERT setter + security update
  void setKeyandCert(const string& key, const string& pubkey, const string& cert);

  void setPushInfo(const string& pushOs, const string& pushDeviceId, const string& pushToken);

  const resip::NameAddr& nameAddress() const;

  ///public IP address & port number
  string mPublicIP;
  unsigned short mPublicPort;
    
  //token
  string mToken;

  //token
  string mPushOs;

  //token
  string mPushDeviceId;

  //token
  string mPushToken;

  void resetHandler();

  void resetRegistrationHandle();

  void setDefaultRegistrationRetryTime(int i);
  int getDefaultRegistrationRetryTime();
  
private:

  QilexRegistrationManager();
    
  void fillMasterProfile(resip::SharedPtr<resip::MasterProfile> masterProf);

  

  resip::SharedPtr<QilexMessageDecorator> m_pQilexMessageDecorator;


private: // data

  /// manager singleton instance
  static QilexRegistrationManager* mInstance;

  /// registration handler
  QilexRegistrationHandler* mRegistrationHandler;

  /// was register requested?
  bool mRegisterRequested;

  /// invite session manager
  QilexCallManager* mInviteManager;

  /// registration message
  SharedPtr<SipMessage> mMessage;

  /// registration handler
  resip::ClientRegistrationHandle mClientRegistrationHandle;

  /// user name
  string mUsername;

  /// domain
  string mDomain;

  /// name addres
  resip::NameAddr* mNameAddr;

  /// aliases
  vector<string> mAliases;

  /// certificate
  string mCertificate;

  /// user private key
  string mKey;

  /// user public key
  string mPubKey;

	///domain ctx
  SSL_CTX* mDomainCtx;

};



#endif // QILEX_REGISTRATION_MANAGER_HXX
