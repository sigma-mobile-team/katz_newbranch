//
//  QilexOutOfDialogManager.h
//  Qilek-iOS
//
//  Created by Sigma Vietnam on 7/15/15.
//  Copyright (c) 2015 Sigma Solutions. All rights reserved.
//

#ifndef __Qilek_iOS__QilexOutOfDialogManager__
#define __Qilek_iOS__QilexOutOfDialogManager__

#include "QilexBaseManager.hxx"
#include "QilexOutOfDialogHandler.hxx"
#include <string>

class QilexOutOfDialogManager : public QilexBaseManager
{
    
public:
    
    static QilexOutOfDialogManager& instance();
    
    virtual ~QilexOutOfDialogManager();
    
    void resetHandler();
    
    /// send pint to a contact
    void sendPingToContact(const std::string& peerName);
    
    resip::OutOfDialogHandler& outOfDialogHandler();

//    void setOutOfDialogHandler(resip::OutOfDialogHandler handle);
private:
    
    QilexOutOfDialogManager();
    
    static QilexOutOfDialogManager* mInstance;
    
    QilexOutOfDialogHandler* mQilexOutOfDialogHandler;
};

#endif /* defined(__Qilek_iOS__QilexOutOfDialogManager__) */
