// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "ICE_Candidate.hxx"
#include "pairStruct.hxx"

#include <set>
#include <string>
#include <vector>
#include <iostream>

#include <reTurn/UdpServer.hxx>
#include <reTurn/StunMessage.hxx>

#include <resip/stack/SdpContents.hxx>


class QilexIce
{
public:
	QilexIce(const std::string& address, unsigned short port);
	void GatherCandidates();
	void ChooseBestCandidate();
	void DecodeSDP(resip::SdpContents&);
	void MakeCandidateSDP(resip::SdpContents::Session::Medium&);
	void doICE();

private:
	void wait(int miliseconds);
	void getLocalIP();
	void Pairing(bool);
	void CheckConnection();
	void EstablishConnection();
	void GatherCandidatesFromStunTurn();

	boost::asio::io_service mIoService;
	reTurn::ReTurnConfig mReTurnConfig;
	reTurn::TurnManager mTurnManager;
	reTurn::RequestHandler mRequestHandler;
	boost::shared_ptr<reTurn::UdpServer> mServer;

	std::set<ICE_Candidate> m_Candidates;
	std::set<ICE_Candidate> m_peerCandidates;

	std::string m_localIP;
	unsigned short m_localPort;

	std::vector <pairStruct> m_CandidatesPairs;

	std::string m_strBestCandIP;
	unsigned short m_nBestCandPort;


	//void ConnectionEstablish();

};
