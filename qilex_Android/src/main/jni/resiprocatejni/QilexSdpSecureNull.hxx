// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_SDP_SECURE_NULL_HXX
#define QILEX_SDP_SECURE_NULL_HXX

#include "QilexSdpSecure.hxx"
#include "srtp.h"

class QilexSdpSecureNull : public QilexSdpSecure
{
public:
	virtual void generateKey(SdpContents::Session::Medium & sdpMedium) {}
	virtual bool preprateCipherData(SdpContents::Session::MediumContainer::const_iterator & iter, std::string & strCipherData) { return true; }
	virtual void setCryptoInMedium(SdpContents::Session::Medium & sdpMedium, const std::string & ssCipherData) {}
	virtual bool setConnectionCipher(SdpContents::Session::MediumContainer::const_iterator& iter) { return true; }
	virtual bool secureStreamTransport(IStreamTransport & streamtransport) { return true; }
	virtual bool getSecureStreamTransport(IStreamTransport & streamtransport) { return true; }
};

#endif
