// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexStatemachine.hxx"
#include "QilexLogger.hxx"
#include <resip/stack/SipMessage.hxx>
#include <cassert>

JavaVM* QilexStatemachine::mVm = 0;
jclass QilexStatemachine::mStatemachineClass = 0;
jclass QilexStatemachine::mSipContext = 0;

QilexStatemachine::QilexStatemachine() {
	mRetryTimeOutCount = 0;
}

QilexStatemachine::~QilexStatemachine() {
}

void QilexStatemachine::setupVm(JavaVM* vm) {
	mVm = vm;
	JNIEnv * env;

	// cache class that cant be found inside native thread because of ClassLoader issue
	mVm->GetEnv((void**) &env, JNI_VERSION_1_6);
	mStatemachineClass = env->FindClass(
			"sigma/qilex/sip/resiprocate/Statemachine");
	mSipContext = env->FindClass("sigma/qilex/sip/resiprocate/SipContext");
	mStatemachineClass = (jclass) env->NewGlobalRef(mStatemachineClass);
	mSipContext = (jclass) env->NewGlobalRef(mSipContext);
}

void QilexStatemachine::sendEvent(Events event) {
	JNIEnv * env;
	attachThread(&env);

	jmethodID sendID = env->GetMethodID(mStatemachineClass, "sendEvent",
			"(I)V");

	jmethodID instanceID = env->GetStaticMethodID(mStatemachineClass,
			"instance", "()Lsigma/qilex/sip/resiprocate/Statemachine;");
	jobject sm = env->CallStaticObjectMethod(mStatemachineClass, instanceID);

	env->CallVoidMethod(sm, sendID, static_cast<int>(event));

	env->DeleteLocalRef(sm);
}

void QilexStatemachine::setPeerProfileUri(const std::string& username) {
	setContextString(std::string("setPeerUri"), username);
}

void QilexStatemachine::setCallId(const std::string& callId) {
	//setContextString(std::string("setCallId"),callId);
}

void QilexStatemachine::addIncomingMessage(const std::string& from,
		const std::string& to, const std::string& id,
		const std::string& timestamp, const std::string& content, const std::string& type, const std::string& uuid) {
	JNIEnv * env;
	attachThread(&env);
	jmethodID methodId =
			env->GetMethodID(mSipContext, "addIncomingMessage",
					"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)V");
	jstring fromString = env->NewStringUTF(from.c_str());
	jstring toString = env->NewStringUTF(to.c_str());
	jstring idString = env->NewStringUTF(id.c_str());
	jstring timestampString = env->NewStringUTF(timestamp.c_str());
	//jstring contentString = env->NewStringUTF(content.c_str());
	jstring typeString = env->NewStringUTF(type.c_str());
	jstring uuidString = env->NewStringUTF(uuid.c_str());

	// Get byte of content - START
	jbyteArray contentArray = env->NewByteArray(content.length());
	env->SetByteArrayRegion(contentArray, 0, content.length(), (jbyte*)content.c_str());
	// Get byte of content - END

	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(fromString),
			env->NewGlobalRef(toString), env->NewGlobalRef(idString),
			env->NewGlobalRef(timestampString),
			env->NewGlobalRef(contentArray),
			env->NewGlobalRef(typeString),
			env->NewGlobalRef(uuidString));

	env->DeleteLocalRef(context);
	env->DeleteLocalRef(fromString);
	env->DeleteLocalRef(toString);
	env->DeleteLocalRef(idString);
	env->DeleteLocalRef(timestampString);
	//env->DeleteLocalRef(contentString);
	env->DeleteLocalRef(typeString);
	env->DeleteLocalRef(uuidString);
	env->DeleteLocalRef(contentArray);// Content Array
}

void QilexStatemachine::addIncomingMessage(
		const std::string& from,
		const std::string& to,
		const std::string& id,
		const std::string& timestamp,
		const std::string& content,
		const std::string& type,
		const std::string& uuid,
			const std::string& groupChatMsgType,
			const std::string& groupChatUuid,
			const std::string& groupChatParticipants,
			const std::string& groupChatAdministrator) {
	JNIEnv * env;
	attachThread(&env);
	jmethodID methodId =
			env->GetMethodID(mSipContext, "addIncomingMessage",
					"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
	jstring fromString = env->NewStringUTF(from.c_str());
	jstring toString = env->NewStringUTF(to.c_str());
	jstring idString = env->NewStringUTF(id.c_str());
	jstring timestampString = env->NewStringUTF(timestamp.c_str());
	//jstring contentString = env->NewStringUTF(content.c_str());
	jstring typeString = env->NewStringUTF(type.c_str());
	jstring uuidString = env->NewStringUTF(uuid.c_str());
	jstring groupChatMsgTypeString = env->NewStringUTF(groupChatMsgType.c_str());
	jstring groupChatUuidString = env->NewStringUTF(groupChatUuid.c_str());
	jstring groupChatParticipantsString = env->NewStringUTF(groupChatParticipants.c_str());
	jstring groupChatAdministratorString = env->NewStringUTF(groupChatAdministrator.c_str());

	// Get byte of content - START
	jbyteArray contentArray = env->NewByteArray(content.length());
	env->SetByteArrayRegion(contentArray, 0, content.length(), (jbyte*)content.c_str());
	// Get byte of content - END

	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(fromString),
			env->NewGlobalRef(toString), env->NewGlobalRef(idString),
			env->NewGlobalRef(timestampString),
			env->NewGlobalRef(contentArray),
			env->NewGlobalRef(typeString),
			env->NewGlobalRef(uuidString),
			env->NewGlobalRef(groupChatMsgTypeString),
			env->NewGlobalRef(groupChatUuidString),
			env->NewGlobalRef(groupChatParticipantsString),
			env->NewGlobalRef(groupChatAdministratorString));

	env->DeleteLocalRef(context);
	env->DeleteLocalRef(fromString);
	env->DeleteLocalRef(toString);
	env->DeleteLocalRef(idString);
	env->DeleteLocalRef(timestampString);
	//env->DeleteLocalRef(contentString);
	env->DeleteLocalRef(typeString);
	env->DeleteLocalRef(uuidString);
	env->DeleteLocalRef(groupChatMsgTypeString);
	env->DeleteLocalRef(groupChatUuidString);
	env->DeleteLocalRef(groupChatParticipantsString);
	env->DeleteLocalRef(groupChatAdministratorString);

	env->DeleteLocalRef(contentArray);// Content Array
}

void QilexStatemachine::addSentMessage(const std::string& id) {
	setContextString(std::string("addSentMessage"), id);
}

void QilexStatemachine::addConfirmedMessage(const std::string& id,
		const std::string& h_code) {
	setContext2String(std::string("addConfirmedMessage"), id, h_code);
}

void QilexStatemachine::addFailedMessage(const std::string& id,
		const std::string& strErr) {
	JNIEnv * env;
	attachThread(&env);
	jmethodID methodId = env->GetMethodID(mSipContext, "addFailedMessage",
			"(Ljava/lang/String;Ljava/lang/String;)V");
	jstring idString = env->NewStringUTF(id.c_str());
	jstring errorString = env->NewStringUTF(strErr.c_str());

	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(idString),
			env->NewGlobalRef(errorString));

	env->DeleteLocalRef(context);
	env->DeleteLocalRef(errorString);
	env->DeleteLocalRef(idString);
}

void QilexStatemachine::addPresenceSubscribed(const std::string& peer) {
	setContextString(std::string("addPresenceSubscribed"), peer);
}

void QilexStatemachine::addPresenceUnsubscribed(const std::string& peer) {
	setContextString(std::string("addPresenceUnsubscribed"), peer);
}

void QilexStatemachine::addPresenceUpdate(const std::string& id, int status,
		const std::string& statusMsg) {
	JNIEnv * env;
	attachThread(&env);

	jmethodID methodId = env->GetMethodID(mSipContext, "addPresenceUpdate",
			"(Ljava/lang/String;ILjava/lang/String;)V");
	jstring jId = env->NewStringUTF(id.c_str());
	jint jStatus = status;
	jstring jStatusMsg = env->NewStringUTF(statusMsg.c_str());
	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(jId), jStatus,
			env->NewGlobalRef(jStatusMsg));
	env->DeleteLocalRef(context);
	env->DeleteLocalRef(jId);
	env->DeleteLocalRef(jStatusMsg);
}

void QilexStatemachine::attachThread(JNIEnv** env) {
	// TODO: sometimes we're already running jvm thread
	// therefore there's no need to attach/detach
	QilexStatemachine::mVm->AttachCurrentThread(env, 0);
}

void QilexStatemachine::detachThread() {
	// TODO: sometimes we're already running jvm thread
	// therefore there's no need to attach/detach
	mVm->DetachCurrentThread();
}

void QilexStatemachine::setContextString(const std::string& setterMethod,
		const std::string& value) {
	JNIEnv * env;
	attachThread(&env);
	jmethodID methodId = env->GetMethodID(mSipContext, setterMethod.c_str(),
			"(Ljava/lang/String;)V");
	jstring valueString = env->NewStringUTF(value.c_str());

	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(valueString));

	env->DeleteLocalRef(context);
	env->DeleteLocalRef(valueString);
}

void QilexStatemachine::setContext2String(const std::string& setterMethod,
		const std::string& value1, const std::string& value2) {
	JNIEnv * env;
	attachThread(&env);
	jmethodID methodId = env->GetMethodID(mSipContext, setterMethod.c_str(),
			"(Ljava/lang/String;Ljava/lang/String;)V");
	jstring value1String = env->NewStringUTF(value1.c_str());
	jstring value2String = env->NewStringUTF(value2.c_str());

	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(value1String),
			env->NewGlobalRef(value2String));

	env->DeleteLocalRef(context);
	env->DeleteLocalRef(value1String);
	env->DeleteLocalRef(value2String);
}

void QilexStatemachine::logMsg(const char* msg) {
	setContextString(std::string("logMessage"), msg);
}

void QilexStatemachine::logMissCall(const std::string& from) {
	JNIEnv * env;
	attachThread(&env);

	jmethodID methodId = env->GetMethodID(mStatemachineClass, "addMissCall",
			"(Ljava/lang/String;)V");

	jmethodID instanceID = env->GetStaticMethodID(mStatemachineClass,
			"instance", "()Lsigma/qilex/sip/resiprocate/Statemachine;");
	jobject context = env->CallStaticObjectMethod(mStatemachineClass, instanceID);

	jstring jFrom = env->NewStringUTF(from.c_str());

	env->CallVoidMethod(context, methodId, env->NewGlobalRef(jFrom));

	env->DeleteLocalRef(context);
	env->DeleteLocalRef(jFrom);
}

void QilexStatemachine::logKeyValue(const char* key, const char* value) {
	setContext2String("publishKeyValue", key, value);
}

void QilexStatemachine::connectionWithoutTurn() {
	JNIEnv * env;
	attachThread(&env);

	jmethodID methodId = env->GetMethodID(mSipContext, "connectionWithoutTurn",
			"()V");

	jfieldID fieldId = env->GetStaticFieldID(mStatemachineClass, "mContext",
			"Lsigma/qilex/sip/resiprocate/SipContext;");
	jobject context = env->GetStaticObjectField(mStatemachineClass, fieldId);

	env->CallVoidMethod(context, methodId);
	env->DeleteLocalRef(context);
}

int QilexStatemachine::isTestNewServer() {
	return 1;
}

void QilexStatemachine::handlerPingResponse(const std::string& from,
		const std::string& to, int status, int offlineMinute) {
	JNIEnv * env;
	attachThread(&env);

	jmethodID sendID =
			env->GetMethodID(mStatemachineClass, "handlerPingResponse",
					"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

	jmethodID instanceID = env->GetStaticMethodID(mStatemachineClass,
			"instance", "()Lsigma/qilex/sip/resiprocate/Statemachine;");
	jobject sm = env->CallStaticObjectMethod(mStatemachineClass, instanceID);

	jstring jFrom = env->NewStringUTF(from.c_str());
	jstring jTo = env->NewStringUTF(to.c_str());
	jstring jStatus;

	jint jIntOflineMinute = offlineMinute;
	char buf[64]; // assumed large enough to cope with result
	sprintf(buf, "%d", jIntOflineMinute);
	jstring jOfflineMinute = env->NewStringUTF(buf);

	if (status == 0) {
		std::string strStatus = "0";
		jStatus = env->NewStringUTF(strStatus.c_str());
	} else if (status == 1) {
		std::string strStatus = "1";
		jStatus = env->NewStringUTF(strStatus.c_str());
	} else {
		std::string strStatus = "-1";
		jStatus = env->NewStringUTF(strStatus.c_str());
	}

	env->CallVoidMethod(sm, sendID, jStatus, env->NewGlobalRef(jFrom),
			env->NewGlobalRef(jTo), env->NewGlobalRef(jOfflineMinute));

	env->DeleteLocalRef(sm);
	env->DeleteLocalRef(jTo);
	env->DeleteLocalRef(jFrom);
	env->DeleteLocalRef(jStatus);
	env->DeleteLocalRef(jOfflineMinute);
}

void handlerPingRequest(const std::string& from) {
	LOGI("QilexOutOfDialogManager::sendPingToContact handlerPingRequest");
}
