//
//  QilexOutOfDialogHandler.cpp
//  Qilek-iOS
//
//  Created by Sigma Vietnam on 7/15/15.
//  Copyright (c) 2015 Sigma Solutions. All rights reserved.
//

#include "QilexOutOfDialogHandler.hxx"
#include "QilexLogger.hxx"

//#include <resip/dum/ClientSubscription.hxx>
//#include <resip/dum/AppDialogSet.hxx>
//#include <resip/stack/Pidf.hxx>
#include <string>
//#include "QilexFakeDNS.hxx"

using namespace resip;

QilexOutOfDialogHandler::QilexOutOfDialogHandler() {
	CALL_TRACK;
}

void QilexOutOfDialogHandler::onSuccess(ClientOutOfDialogReqHandle handler,
		const SipMessage& successResponse) {
	CALL_TRACK;
	std::string to = successResponse.header(h_To).uri().toString().c_str();
	std::string from = successResponse.header(h_From).uri().toString().c_str();
	LOGI(
			"QilexOutOfDialogManager::sendPingToContact %s  TO %s  FAIL", from.c_str(), to.c_str());
	qlexStateMachine.handlerPingResponse(from, to, 1, 0);
}

void QilexOutOfDialogHandler::onFailure(ClientOutOfDialogReqHandle handler,
		const SipMessage& errorResponse) {
	CALL_TRACK;

	int nCode = errorResponse.header(resip::h_StatusLine).responseCode();
	LOGI("QilexOutOfDialogHandler::onFailure  error code: %d", nCode);

	std::string to = errorResponse.header(h_To).uri().toString().c_str();
	std::string from = errorResponse.header(h_From).uri().toString().c_str();
	LOGI(
			"QilexOutOfDialogManager::sendPingToContact %s  TO %s  FAIL", from.c_str(), to.c_str());
	int offlineSecond = 0;
	if (errorResponse.exists(h_XOfflineSecond)) {
		offlineSecond = errorResponse.header(h_XOfflineSecond).value();
		LOGI("Header h_XOfflineSecond is exists %d", offlineSecond);
	} else {
		LOGI("Header h_XOfflineSecond is NOT exists");
	}
	switch (nCode) {
	case 404:
		// user offline
		qlexStateMachine.handlerPingResponse(from, to, 0, offlineSecond);
		break;
	case 408:
		qlexStateMachine.handlerPingResponse(from, to, -1, offlineSecond);
		// request time out
		/// TODO: distinguish from 408 local (lost network connection) and 408 server (contact offline)
		break;

	default:
		// another case
		break;
	}



//	QilexOutOfDialogManager::instance().setOutOfDialogHandler(handler);
}

void QilexOutOfDialogHandler::onReceivedRequest(ServerOutOfDialogReqHandle,
		const SipMessage& request) {
//	CALL_TRACK;
//	LOGI("QilexOutOfDialogManager::sendPingToContact onReceivedRequest");
//    qlexStateMachine.handlerPingRequest(from, to);
}

