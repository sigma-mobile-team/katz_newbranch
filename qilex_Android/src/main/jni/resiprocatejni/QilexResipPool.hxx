// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_RESIP_POOL_HXX
#define QILEX_RESIP_POOL_HXX


#include <memory>
#include <pthread.h>
#include <resip/dum/DumShutdownHandler.hxx>

// forward declarations
namespace resip
{
  class SipStackOptions;
  class SipStack;
  class ThreadIf;
  class SelectInterruptor;
  class DialogUsageManager;
  class DumThread;
  class AppDialogSetFactory;
#ifdef SIP_SERVER_USE_SSL
  class Security;
#endif
}


/**
   Resiprocate objects' pool that owns sip stack, options,
   dialog usage manager, threads etc.
*/
class QilexResipPool : public resip::DumShutdownHandler
{

public:

  /// constructor
  QilexResipPool();
  
  ~QilexResipPool();
 
  /// stack options getter
  resip::SipStackOptions& stackOptions();
  
  /// sip stack getter
  resip::SipStack& stack();
  resip::SipStack* pstack();
  
  /// dialog usage manager getter
  resip::DialogUsageManager& dum();
  resip::DialogUsageManager* pdum();

  /// creates a treate for sip & dum processing
  void startThread(bool forced = false);
  
  /// creates a treate for sip & dum processing
  void stopThread();

  /// delete
  void deleteStack();

  /// locks dum object in separate thread
  void dumLock();

  /// unlocks dum object in separate thread
  void dumUnlock();

  /// transport setup
  void setupTransport();

  void setupStack();

  // from DumShutdownHandler
  virtual void onDumCanBeDeleted();


  void shutdown(bool forced);

  static void restartStack();

private: // data

  /// sip stack options
  std::auto_ptr<resip::SipStackOptions> mSipStackOptions;

  /// sip stack
  std::auto_ptr<resip::SipStack> mSipStack;

  /// dialog usage manager
  std::auto_ptr<resip::DialogUsageManager> mDum;

  /// thread processing messages
  pthread_t mProcessThread;

  /// SipStack thread mutex
  pthread_mutex_t mProcessThreadMutex;

#ifdef SIP_SERVER_USE_SSL
  /// security
  //std::auto_ptr<resip::Security> mSecurity;
#endif
};


#endif // QILEX_RESIP_POOL_HXX
