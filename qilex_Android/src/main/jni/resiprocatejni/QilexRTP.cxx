// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexRTP.hxx"

#include "QilexConfig.hxx"
#include "mp/MpMediaTask.h"
#include "mp/MpMisc.h"
#include "QilexLogger.hxx"
#include "QilexSdp.hxx"
#include <sstream>
#include "StreamTransportFactory.h"
#include "StreamTransportResults.h"
#include "IStreamTransport.h"
#include "AudioMgr.h"
#include "AudioMgrEvent.h"
#include "QilexSdpSecure.hxx"

QilexRTP::QilexRTP()
{
}

bool QilexRTP::prepareStreamTransport()
{
	SIPXLOGI("QilexRTP::prepareStreamTransport()");

	unsigned long nSSRC;
	unsigned char r1, r2, r3, r4;
	r1 = rand()%256;
	r2 = rand()%256;
	r3 = rand()%256;
	r4 = rand()%256;
	nSSRC = (r1<<24) | (r2<<16) | (r3<<8) | r4;

	m_pStreamTransport = StreamTransportFactory::Create(nSSRC);
	this->net().setStreamTransport(m_pStreamTransport);
	SIPXLOGI("/QilexRTP::prepareStreamTransport()");

	return m_pStreamTransport != NULL;
}

bool QilexRTP::setPeer(const char* remote_addres, int remote_port, const char* remote_rtcp_addres, int remote_rtcp_port)
{
	SIPXLOGI("QilexRTP::setPeer %s %d %s %d", remote_addres, remote_port, remote_rtcp_addres, remote_rtcp_port);

	if(!m_pStreamTransport)
	{
		SIPXLOGE("QilexRTP::setPeer m_pStreamTransport==null");
		return false;
	}

	sdpSecure.secureStreamTransport(*m_pStreamTransport);

	if(m_pStreamTransport->SetPeer(remote_addres, remote_port, remote_rtcp_addres, remote_rtcp_port)!=StreamTransportResults_Success)
	{
		SIPXLOGE("QilexRTP::setStreamingDest m_pStreamTransport->SetPeer");
		return false;
	}

	SIPXLOGI("/QilexRTP::setPeer");
	return true;
}

bool QilexRTP::bind(const std::string &strLocalIpAddress, unsigned short nLocalPort, bool bUseTURN)
{
	SIPXLOGI("QilexRTP::bind");
	if(!m_pStreamTransport)
		return false;

	if(m_pStreamTransport->Bind(strLocalIpAddress.c_str(), nLocalPort, bUseTURN)!=StreamTransportResults_Success)
	{
		SIPXLOGE("QilexRTP::bind failed");
		return false;
	}
	SIPXLOGI("QilexRTP::bind/");
	return true;
}

void QilexRTP::gatherCandidates()
{

}

void QilexRTP::setupCodec(const SDPCodecDescriptor & codec)
{
	codecDescriptor = codec;
}

bool QilexRTP::start()
{
	SIPXLOGI("QilexRTP::startTransmition() **************************************************");
	if(!m_pStreamTransport)
	{
		SIPXLOGE("QilexRTP::startTransmition() no stream transport");
		return false;
	}

    if(m_pStreamTransport->StartTransmission()!=StreamTransportResults_Success)
    {
		SIPXLOGE("QilexRTP::startTransmition() stream transport start failed");
        return false;
    }

	SIPXLOGI("QilexRTP::startTransmition() 1");

	QilexConfig::setSamplesPerSecond(codecDescriptor.sampleRate);
	AudioMgr* audiomgr = AudioMgr::Get();
	audiomgr->AttachTrafficTransport(m_pStreamTransport);
	audiomgr->startAudio(codecDescriptor);

	SIPXLOGI("QilexRTP::startTransmition()//////////////*************************************");

    return true;
}
float QilexRTP::audioQuality(){
	return AudioMgr::Get()->GetJitterBufferUsage();
}
bool QilexRTP::stop()
{
	AudioMgr::Get()->stopAudio();
}
bool QilexRTP::terminate()
{
	SIPXLOGI("bool QilexRTP::terminate()**************************************************");
	AudioMgr::Get()->stopAudio();
	SIPXLOGI("bool QilexRTP::terminate()//////////////*************************************");

	m_pStreamTransport->StopSend();
	m_pStreamTransport->CloseConnection();
}

bool QilexRTP::getStreamTransportSecure()
{
	sdpSecure.getSecureStreamTransport( *m_pStreamTransport.get() );
}

void QilexRTP::updateAudioConfig(bool speakerOn, bool headphonesOn, bool muted)
{
	AudioMgr::Get()->OnEvent(AudioMgrEvent::CreateSetInputState(muted));
	AudioMgr::Get()->OnEvent(AudioMgrEvent::CreateOutputAudioConfigChanged(speakerOn, headphonesOn));
}

void QilexRTP::updateAudioVolume(float volume)
{
	AudioMgr::Get()->OnEvent(AudioMgrEvent::CreateOutputVolumeChanged(volume));
}
