// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEXPRESENCEHANDLER_HXX
#define QILEXPRESENCEHANDLER_HXX

#include <resip/dum/SubscriptionHandler.hxx>
#include <resip/dum/PublicationHandler.hxx>

class QilexPresenceHandler:
  public resip::ClientSubscriptionHandler,
  public resip::ClientPublicationHandler
{

public:

  enum Status
    {
      NOT_AVAILABLE,
      HIDDEN,
      BUSY,
      AVAILABLE
    };

  // from resip::ClientPublicationHandler
  virtual void onSuccess(resip::ClientPublicationHandle, const resip::SipMessage& status);
  virtual void onRemove(resip::ClientPublicationHandle, const resip::SipMessage& status);
  virtual void onFailure(resip::ClientPublicationHandle, const resip::SipMessage& status);
  virtual int onRequestRetry(resip::ClientPublicationHandle, int retrySeconds, const resip::SipMessage& status);

  // from resip::ClientSubscriptionHandler
  virtual void onUpdatePending(resip::ClientSubscriptionHandle, const resip::SipMessage& notify, bool outOfOrder);
  virtual void onUpdateActive(resip::ClientSubscriptionHandle, const resip::SipMessage& notify, bool outOfOrder);
  virtual void onUpdateExtension(resip::ClientSubscriptionHandle, const resip::SipMessage& notify, bool outOfOrder);
  virtual int onRequestRetry(resip::ClientSubscriptionHandle, int retrySeconds, const resip::SipMessage& notify);
  virtual void onTerminated(resip::ClientSubscriptionHandle, const resip::SipMessage* msg);
  virtual void onNewSubscription(resip::ClientSubscriptionHandle, const resip::SipMessage& notify);
};

#endif // QILEXPRESENCEHANDLER_HXX
