// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef REGISTER_SESSION_HANDLER_HXX
#define REGISTER_SESSION_HANDLER_HXX

#include "QilexStatemachine.hxx"
#include <resip/dum/RegistrationHandler.hxx>

class QilexRegistrationManager;

using namespace resip;

class QilexRegistrationHandler : public ClientRegistrationHandler
{
public:
  QilexRegistrationHandler();
  virtual ~QilexRegistrationHandler();
  
  /// Called when registraion succeeds or each time it is sucessfully
  /// refreshed. 
  virtual void onSuccess(ClientRegistrationHandle, const SipMessage& response);
  
  // Called when all of my bindings have been removed
  virtual void onRemoved(ClientRegistrationHandle, const SipMessage& response);
  
  /// call on Retry-After failure. 
  /// return values: -1 = fail, 0 = retry immediately, N = retry in N seconds
  virtual int onRequestRetry(ClientRegistrationHandle, int retrySeconds, const SipMessage& response);
  
  /// Called if registration fails, usage will be destroyed (unless a 
  /// Registration retry interval is enabled in the Profile)
  virtual void onFailure(ClientRegistrationHandle, const SipMessage& response);
  
  /// Called when a TCP or TLS flow to the server has terminated.  This can be caused by socket
  /// errors, or missing CRLF keep alives pong responses from the server.
  //  Called only if clientOutbound is enabled on the UserProfile and the first hop server 
  /// supports RFC5626 (outbound).
  /// Default implementation is to immediately re-Register in an attempt to form a new flow.
  virtual void onFlowTerminated(ClientRegistrationHandle);

private:
  QilexStatemachine mSm;

  void onSuccess_dbgPrint(ClientRegistrationHandle & handle, const SipMessage& response);
};




#endif // REGISTER_SESSION_HANDLER_HXX
