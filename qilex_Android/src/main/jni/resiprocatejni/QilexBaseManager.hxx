// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.
#ifndef QILEX_SIP_BASE_MANAGER_HXX
#define QILEX_SIP_BASE_MANAGER_HXX

#include "QilexResipPool.hxx"
#include <QilexMessageDecorator.hxx>

#include <resip/dum/ClientAuthManager.hxx>
#include <resip/dum/MasterProfile.hxx>

#include <string>

using namespace std;

class DumLocker;

/**
   Base class for managers, you have to initialize it by initialize() before usage, 
   and shutdown() on the other hand when not used any more.
 */
class QilexBaseManager
{
  friend class DumLocker;

public:   

  /// initializes resiprocate dialog usage manager
  static void initialize();
  
  /// shutdown procedure
  static void shutdown();

  static void sync();

  /// starts a separate thread for dum & sipstack processing
  void startThread();

  /// stops thread for dum & sipstack processing
  void stopThread();

protected:

   QilexBaseManager();

   ~QilexBaseManager();

  /// dialog usage manager singleton getter
  static resip::DialogUsageManager& dum();

  // locks dum object in separate thread
  void dumLock();

  // unlocks dum object in separate thread
  void dumUnlock();

private: // data

  /// pool for resiprocate global objects
  static std::auto_ptr<QilexResipPool> mResipPool;

};

/**
 * Tool class for automate lock/unlock dum
 */
class DumLocker
{
public:
	DumLocker(QilexBaseManager* pm)
	{
		mpm = pm;
		pm->dumLock();
	}

	~DumLocker()
	{
		mpm->dumUnlock();
	}

private:
	QilexBaseManager* mpm;
};

#endif // QILEX_SIP_BASE_MANAGER_HXX
