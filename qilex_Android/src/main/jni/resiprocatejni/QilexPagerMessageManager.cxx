// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.


#include "QilexPagerMessageManager.hxx"
#include "QilexPagerMessageHandler.hxx"
#include "QilexLogger.hxx"
#include <resip/dum/DialogUsageManager.hxx>
#include <resip/stack/Contents.hxx>
#include <resip/stack/PlainContents.hxx>
#include <resip/stack/SipMessage.hxx>
#include <resip/dum/ClientPagerMessage.hxx>
#include <rutil/Data.hxx>
#include <resip/stack/NameAddr.hxx>
#include <sstream>
#include <string>
#include "QilexConfig.hxx"

QilexPagerMessageManager* QilexPagerMessageManager::mInstance = 0;

QilexPagerMessageManager& QilexPagerMessageManager::instance()
{
	CALL_TRACK;

	if (!mInstance)
	{
		mInstance = new QilexPagerMessageManager();
    }

	return *mInstance;
}

QilexPagerMessageManager::QilexPagerMessageManager()
{
  // use QilexPagerMessageManager::instance() inside handler
  mMessageHandler = new QilexPagerMessageHandler();
  dumLock();
  resetHandler();
  dumUnlock();
}

QilexPagerMessageManager::~QilexPagerMessageManager()
{
	delete mMessageHandler;
}

void QilexPagerMessageManager::resetHandler()
{
	CALL_TRACK;
	// One
	dum().setClientPagerMessageHandler(mMessageHandler);
	dum().setServerPagerMessageHandler(mMessageHandler);
}

void QilexPagerMessageManager::sendComposingPagerMessage(const std::string& strContent, const std::string& nId)
{
  CALL_TRACK;

  dumLock();
  resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
  dumUnlock();

  LOGI("%s", mFromUri.c_str());

  userProfile->setDefaultFrom(resip::NameAddr(mFromUri.c_str()));

  try
  {
	  resip::ClientPagerMessageHandle handle;
	  {
		  DumLocker lock(this);
		  userProfile->setInstanceId(dum().getMasterProfile().get()->getInstanceId());
		  handle = dum().makePagerMessage(resip::NameAddr(mPeerUri.c_str()),userProfile);
	  }

	  resip::SipMessage& msg = handle->getMessageRequest();

	  msg.addHeader(resip::Headers::XMessageComposingInfo, "X-Message-Composing-Info", 24, strContent.c_str(), strContent.length());
	  msg.addHeader(resip::Headers::XMessageType, "X-MessageType", 13, "2003", 4);
	  msg.addHeader(resip::Headers::Timestamp, "Timestamp", 9, mTimestamp.c_str(), mTimestamp.length());
	  std::string strCallId = msg.header(resip::h_CallId).value().c_str();
	  mMessageHandler->mMessageIDs[strCallId] = nId;

	  auto_ptr<resip::Contents> content(new resip::PlainContents(resip::Data("")));
	  {
		  DumLocker lock(this);
		  handle->page(content);
		  LOGI("sending");
	  }
	  //startThread();

	  QilexStatemachine sm;
	  sm.addSentMessage(nId); // msg id
	  sm.sendEvent(QilexStatemachine::EVENT_MESSAGESENT);
  }
  catch (std::exception& e)
  {
	  std::stringstream es;
	  es << e.what();
	  LOGI("ec.str().c_str()");
  }
}

void QilexPagerMessageManager::sendComposingPagerGroupMessage(const std::string& strContent, const std::string& nId,
		  const std::string& nGroupChatMsgType,
		  const std::string& nGroupChatUuid)
{
  CALL_TRACK;

  dumLock();
  resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
  dumUnlock();

  LOGI("%s", mFromUri.c_str());

  userProfile->setDefaultFrom(resip::NameAddr(mFromUri.c_str()));

  try
  {
	  resip::ClientPagerMessageHandle handle;
	  {
		  DumLocker lock(this);
		  userProfile->setInstanceId(dum().getMasterProfile().get()->getInstanceId());
		  handle = dum().makePagerMessage(resip::NameAddr(mPeerUri.c_str()),userProfile);
	  }

	  resip::SipMessage& msg = handle->getMessageRequest();

	  msg.addHeader(resip::Headers::XMessageComposingInfo, "X-Message-Composing-Info", 24, strContent.c_str(), strContent.length());
	  msg.addHeader(resip::Headers::XMessageType, "X-MessageType", 13, "2003", 4);
	  msg.addHeader(resip::Headers::Timestamp, "Timestamp", 9, mTimestamp.c_str(), mTimestamp.length());
	  msg.addHeader(resip::Headers::XGroupChatMessageType, "X-GroupChat-MessageType", 23, nGroupChatMsgType.c_str(), nGroupChatMsgType.length());
	  msg.addHeader(resip::Headers::XGroupChatUUID, "X-GroupChat-UUID", 16, nGroupChatUuid.c_str(), nGroupChatUuid.length());
	  std::string strCallId = msg.header(resip::h_CallId).value().c_str();
	  mMessageHandler->mMessageIDs[strCallId] = nId;

	  auto_ptr<resip::Contents> content(new resip::PlainContents(resip::Data("")));
	  {
		  DumLocker lock(this);
		  handle->page(content);
		  LOGI("sending");
	  }
	  //startThread();

	  QilexStatemachine sm;
	  sm.addSentMessage(nId); // msg id
	  sm.sendEvent(QilexStatemachine::EVENT_MESSAGESENT);
  }
  catch (std::exception& e)
  {
	  std::stringstream es;
	  es << e.what();
	  LOGI("ec.str().c_str()");
  }
}

void QilexPagerMessageManager::sendPagerMessage(const std::string& strMessage, const std::string& nId, const std::string& type, const std::string& nUuid)
{
  CALL_TRACK;
  
  dumLock();
  resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
  dumUnlock();
  
  LOGI("%s", mFromUri.c_str());
  
  userProfile->setDefaultFrom(resip::NameAddr(mFromUri.c_str()));

  try
  {
	  resip::ClientPagerMessageHandle handle;
	  {
		  DumLocker lock(this);
		  userProfile->setInstanceId(dum().getMasterProfile().get()->getInstanceId());
		  handle = dum().makePagerMessage(resip::NameAddr(mPeerUri.c_str()),userProfile);
	  }

	  resip::SipMessage& msg = handle->getMessageRequest();

	  msg.addHeader(resip::Headers::Timestamp, "Timestamp", 9, mTimestamp.c_str(), mTimestamp.length());
	  msg.addHeader(resip::Headers::XMessageType, "X-MessageType", 13, type.c_str(), type.length());
	  msg.addHeader(resip::Headers::XMessageUUID, "X-Message-UUID", 14, nUuid.c_str(), nUuid.length());
	  std::string strCallId = msg.header(resip::h_CallId).value().c_str();
	  mMessageHandler->mMessageIDs[strCallId] = nId;

	  auto_ptr<resip::Contents> content(new resip::PlainContents(resip::Data(strMessage)));
	  {
		  DumLocker lock(this);
		  handle->page(content);
		  LOGI("sending");
	  }
	  //startThread();

	  QilexStatemachine sm;
	  sm.addSentMessage(nId); // msg id
	  sm.sendEvent(QilexStatemachine::EVENT_MESSAGESENT);
  }
  catch (std::exception& e)
  {
	  std::stringstream es;
	  es << e.what();
	  LOGI("ec.str().c_str()");
  }
}

void QilexPagerMessageManager::sendPagerGroupMessage(
		const std::string& strMessage,
		const std::string& nId,
		const std::string& type,
		const std::string& nUuid,
		const std::string& nGroupChatMsgType,
		const std::string& nGroupChatUuid,
		const std::string& nGroupChatPaticipants,
		const std::string& nGroupChatAdministrator)
{
  CALL_TRACK;

  dumLock();
  resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
  dumUnlock();

  LOGI("SEND GROUP MSG %s", mFromUri.c_str());

  userProfile->setDefaultFrom(resip::NameAddr(mFromUri.c_str()));

  try
  {
	  resip::ClientPagerMessageHandle handle;
	  {
		  DumLocker lock(this);
		  userProfile->setInstanceId(dum().getMasterProfile().get()->getInstanceId());
		  handle = dum().makePagerMessage(resip::NameAddr(mPeerUri.c_str()),userProfile);
	  }

	  resip::SipMessage& msg = handle->getMessageRequest();

	  msg.addHeader(resip::Headers::Timestamp, "Timestamp", 9, mTimestamp.c_str(), mTimestamp.length());
	  msg.addHeader(resip::Headers::XMessageType, "X-MessageType", 13, type.c_str(), type.length());
	  msg.addHeader(resip::Headers::XMessageUUID, "X-Message-UUID", 14, nUuid.c_str(), nUuid.length());

	  // Group Chat Headers
	  msg.addHeader(resip::Headers::XGroupChatMessageType, "X-GroupChat-MessageType", 23, nGroupChatMsgType.c_str(), nGroupChatMsgType.length());
	  msg.addHeader(resip::Headers::XGroupChatUUID, "X-GroupChat-UUID", 16, nGroupChatUuid.c_str(), nGroupChatUuid.length());
	  msg.addHeader(resip::Headers::XGroupChatParticipants, "X-GroupChat-Participants", 24, nGroupChatPaticipants.c_str(), nGroupChatPaticipants.length());
	  msg.addHeader(resip::Headers::XGroupChatAdministrator, "X-GroupChat-Administrator", 25, nGroupChatAdministrator.c_str(), nGroupChatAdministrator.length());

	  std::string strCallId = msg.header(resip::h_CallId).value().c_str();
	  mMessageHandler->mMessageIDs[strCallId] = nId;

	  auto_ptr<resip::Contents> content(new resip::PlainContents(resip::Data(strMessage)));
	  {
		  DumLocker lock(this);
		  handle->page(content);
		  LOGI("sending");
	  }
	  //startThread();

	  QilexStatemachine sm;
	  sm.addSentMessage(nId); // msg id
	  sm.sendEvent(QilexStatemachine::EVENT_MESSAGESENT);
  }
  catch (std::exception& e)
  {
	  std::stringstream es;
	  es << e.what();
	  LOGI("ec.str().c_str()");
  }
}

void QilexPagerMessageManager::sendPagerGroupMessage(
		const std::string& strMessage,
		const std::string& nId,
		const std::string& type,
		const std::string& nUuid,
		const std::string& nGroupChatMsgType,
		const std::string& nGroupChatUuid)
{
  CALL_TRACK;

  dumLock();
  resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
  dumUnlock();

  LOGI("SEND GROUP MSG %s", mFromUri.c_str());

  userProfile->setDefaultFrom(resip::NameAddr(mFromUri.c_str()));

  try
  {
	  resip::ClientPagerMessageHandle handle;
	  {
		  DumLocker lock(this);
		  userProfile->setInstanceId(dum().getMasterProfile().get()->getInstanceId());
		  handle = dum().makePagerMessage(resip::NameAddr(mPeerUri.c_str()),userProfile);
	  }

	  resip::SipMessage& msg = handle->getMessageRequest();

	  msg.addHeader(resip::Headers::Timestamp, "Timestamp", 9, mTimestamp.c_str(), mTimestamp.length());
	  msg.addHeader(resip::Headers::XMessageType, "X-MessageType", 13, type.c_str(), type.length());
	  msg.addHeader(resip::Headers::XMessageUUID, "X-Message-UUID", 14, nUuid.c_str(), nUuid.length());

	  // Group Chat Headers
	  msg.addHeader(resip::Headers::XGroupChatMessageType, "X-GroupChat-MessageType", 23, nGroupChatMsgType.c_str(), nGroupChatMsgType.length());
	  msg.addHeader(resip::Headers::XGroupChatUUID, "X-GroupChat-UUID", 16, nGroupChatUuid.c_str(), nGroupChatUuid.length());

	  std::string strCallId = msg.header(resip::h_CallId).value().c_str();
	  mMessageHandler->mMessageIDs[strCallId] = nId;

	  auto_ptr<resip::Contents> content(new resip::PlainContents(resip::Data(strMessage)));
	  {
		  DumLocker lock(this);
		  handle->page(content);
		  LOGI("sending");
	  }
	  //startThread();

	  QilexStatemachine sm;
	  sm.addSentMessage(nId); // msg id
	  sm.sendEvent(QilexStatemachine::EVENT_MESSAGESENT);
  }
  catch (std::exception& e)
  {
	  std::stringstream es;
	  es << e.what();
	  LOGI("ec.str().c_str()");
  }
}

void QilexPagerMessageManager::setPeerUri(const string& peerUri)
{
  mPeerUri = peerUri;
}

void QilexPagerMessageManager::setClientPagerMessageManager(resip::ClientPagerMessageHandle handle)
{
  mClientMessageHandle = handle;
}

void QilexPagerMessageManager::setServerPagerMessageManager(resip::ServerPagerMessageHandle handle)
{
  mServerMessageHandle = handle;
}

resip::ClientPagerMessageHandle QilexPagerMessageManager::getClientPagerMessageManager()
{
  return mClientMessageHandle;
}

resip::ServerPagerMessageHandle QilexPagerMessageManager::getServerPagerMessageManager()
{
  return mServerMessageHandle;
}

void QilexPagerMessageManager::setFromUri(const string& fromUri)
{
	mFromUri = fromUri;
}

void QilexPagerMessageManager::setTimestamp(const string& timestamp)
{
	mTimestamp = timestamp;
}

void QilexPagerMessageManager::resetClientMessageHandle()
{
	mClientMessageHandle = resip::ClientPagerMessageHandle();
}

void QilexPagerMessageManager::resetServerMessageHandle()
{
	mServerMessageHandle = resip::ServerPagerMessageHandle();
}

