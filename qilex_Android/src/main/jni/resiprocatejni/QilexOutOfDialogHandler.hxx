//
//  QilexOutOfDialogHandler.h
//  Qilek-iOS
//
//  Created by Sigma Vietnam on 7/15/15.
//  Copyright (c) 2015 Sigma Solutions. All rights reserved.
//

#ifndef __Qilek_iOS__QilexOutOfDialogHandler__
#define __Qilek_iOS__QilexOutOfDialogHandler__

#include <resip/dum/OutOfDialogHandler.hxx>
#include "QilexStatemachine.hxx"

class QilexOutOfDialogHandler: public resip::OutOfDialogHandler
{
public:
    QilexOutOfDialogHandler();
    // Client Handlers
    virtual void onSuccess(resip::ClientOutOfDialogReqHandle, const resip::SipMessage& successResponse);
    virtual void onFailure(resip::ClientOutOfDialogReqHandle, const resip::SipMessage& errorResponse);
    
    // Server Handlers
    virtual void onReceivedRequest(resip::ServerOutOfDialogReqHandle, const resip::SipMessage& request);
    
private:
    QilexStatemachine qlexStateMachine;
};

#endif /* defined(__Qilek_iOS__QilexOutOfDialogHandler__) */
