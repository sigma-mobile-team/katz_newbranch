// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_NAT_HXX
#define QILEX_NAT_HXX

#include <string>
#include <set>
#include "ICE_Candidate.hxx"
#include "IStreamTransport.h"

class QilexNET
{
public:
	QilexNET();
	static std::string getLocalIP();
	void gatherAddresses(bool noRelayed,bool bEnableRtcp);
	void getBestCandidate(std::string& strIP, unsigned short& nPort, bool & bRelayed, bool bNoRelayed);

	const boost::shared_ptr<IStreamTransport>& getStreamTransport() { return m_pStreamTransport; }
	void setStreamTransport(const boost::shared_ptr<IStreamTransport>& pStreamTransport) { m_pStreamTransport = pStreamTransport; }
	static void setCrypto(const std::string &strUser, const std::string &strUserCert,const std::string &strUserPrivKey);
    void setToken(const std::string & token) { m_strAuthToken = token; }
    void rebindWithoutTurn();
  static int activeInterfaces();
    
protected:
	std::set<ICE_Candidate>	m_sICE_Candidates;
	boost::shared_ptr<IStreamTransport> m_pStreamTransport;
	std::string m_strLocalIP;
	int m_nLocalPort;
	std::string m_strPublicIP;
	std::string m_strAuthToken;
	static std::string m_strUser;
	static std::string m_strUserCert;
	static std::string m_strUserPrivKey;
};

#endif
