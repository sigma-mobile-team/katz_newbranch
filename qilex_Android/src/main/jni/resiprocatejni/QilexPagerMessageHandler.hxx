// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef __Qilex_Pager_Message_Handler_HXX__
#define __Qilex_Pager_Message_Handler_HXX__


#include "QilexStatemachine.hxx"
#include <resip/dum/PagerMessageHandler.hxx>
#include <resip/stack/Contents.hxx>
#include <resip/stack/SipMessage.hxx>
#include <map>
#include <string>


class QilexPagerMessageHandler : public resip::ClientPagerMessageHandler, public resip::ServerPagerMessageHandler
{
public:
	//client part
	virtual void onSuccess(resip::ClientPagerMessageHandle handle, const resip::SipMessage& status);
	virtual void onFailure(resip::ClientPagerMessageHandle handle, const resip::SipMessage& status, std::auto_ptr<resip::Contents> contents);

	//server part
	virtual void onMessageArrived(resip::ServerPagerMessageHandle handle, const resip::SipMessage& message);

protected:
	std::map<std::string, std::string> mMessageIDs;
	friend class QilexPagerMessageManager;

private:
    QilexStatemachine mStatemachine;
};

#endif
