// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexPresenceManager.hxx"
#include "QilexPresenceHandler.hxx"
#include "QilexRegistrationManager.hxx"
#include "QilexLogger.hxx"
#include <resip/stack/Pidf.hxx>
#include <resip/dum/DialogUsageManager.hxx>
#include <resip/dum/ClientSubscription.hxx>
#include <resip/dum/ClientPublication.hxx>
#include <sstream>
#include "QilexConfig.hxx"


// local constants

const resip::Data presenceEventType("presence");

// static variables

QilexPresenceManager* QilexPresenceManager::mInstance=0;


// implementation

QilexPresenceManager& QilexPresenceManager::instance()
{
	CALL_TRACK;
	if (!mInstance)
    {
		mInstance = new QilexPresenceManager();
    }

	return *mInstance;
}

QilexPresenceManager::QilexPresenceManager()
{
	CALL_TRACK;
	mPresenceHandler.reset(new QilexPresenceHandler());

	DumLocker lock(this);
	resetHandler();
}


QilexPresenceManager::~QilexPresenceManager()
{
	CALL_TRACK;
}


void QilexPresenceManager::resetHandler()
{
	CALL_TRACK;
	mPresenceSubscriptions.clear();
	dum().addClientSubscriptionHandler(presenceEventType,mPresenceHandler.get());
	dum().addClientPublicationHandler(presenceEventType,mPresenceHandler.get());
}


void QilexPresenceManager::terminateHandler(const resip::ClientSubscriptionHandle& handle)
{
	std::map<std::string,SubscriptionStruct>::iterator iterator;

	for (iterator = mPresenceSubscriptions.begin(); iterator!=mPresenceSubscriptions.end(); iterator++)
	{
		if (iterator->second.mHandle==handle)
		{
			iterator->second.mHandle = resip::ClientSubscriptionHandle();
			iterator->second.mPending = false;
			break;
		}
	}
}


void QilexPresenceManager::subscribe(const std::string& peerName)
{
	LOGI("QilexPresenceManager::subscribe: %s", peerName.c_str());

  try
    {
      DumLocker lock(this);
      mPresenceSubscriptions[peerName].mHandle = ClientSubscriptionHandle();
      mPresenceSubscriptions[peerName].mPending = false;
      resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
      userProfile->setInstanceId(dum().getMasterUserProfile().get()->getInstanceId());
        resip::SharedPtr<resip::SipMessage> msg = dum().makeSubscription(resip::NameAddr(peerName.c_str()),userProfile,presenceEventType, QilexConfig::getPresenceSubscriptionExpire());
      dum().send(msg);

    }
  catch (...)
    {
      LOGE("QilexPresenceManager::subscribe exception");
    }
}
 

void QilexPresenceManager::unsubscribe(const std::string& peerName)
{
  CALL_TRACK;
  DumLocker lock(this);
  LOGI("%s", peerName.c_str());
  try
    {
      LOGI("getting handle");
      if(mPresenceSubscriptions.count(peerName)==0)
	{
	  LOGI("no handles for peer");
	  return;
	}
      resip::ClientSubscriptionHandle& handle = mPresenceSubscriptions[peerName].mHandle;

      // LOGI("Subscriptions: ");
      // for (std::map<std::string,SubscriptionStruct>::iterator iterator = mPresenceSubscriptions.begin();
      // 	   iterator != mPresenceSubscriptions.end();
      // 	   iterator++ )
      // 	{
      // 	  LOGI((*iterator).first.c_str());
      // 	  resip::ClientSubscriptionHandle& handle = (*iterator).second.mHandle;
      // 	  std::stringstream ss;
      // 	  ss << "handle is ";
      // 	  if (handle.isValid())
      // 	    ss << "valid :";
      // 	  else
      // 	    ss << "invalid :";
      // 	  ss << handle.getId();
      // 	  LOGI( ss.str().c_str());
      // 	}

      LOGI("getting handle success");
      if (handle.isValid())
	{
	  LOGI("unsubscribe: handle is valid");
	  handle->end();
	  LOGI("unsubscribe: handle ended");
	  {
	    std::stringstream ss;
	    ss << "unsubscribe: " << mPresenceSubscriptions.size();
	    LOGI("%s", ss.str().c_str());
	  }
				   
	  //	  mPresenceSubscriptions.erase(peerName);	  
	  {
	    std::stringstream ss;
	    ss << "unsubscribe: " << mPresenceSubscriptions.size();
	    LOGI("%s", ss.str().c_str());
	  }

	}
    }
  catch(...)
    {
      LOGE("Unsubscription exception");
    }

}

void QilexPresenceManager::publish(QilexPresenceHandler::Status status, const std::string& message)
{
	CALL_TRACK;
	try
	{
		if (status==QilexPresenceHandler::NOT_AVAILABLE)
		{
			this->dumLock();
			if (mClientPublicationHandle.isValid())
				mClientPublicationHandle->end();
			this->dumUnlock();
			return;
		}

		resip::Pidf pidfContents;
		resip::NameAddr user(QilexRegistrationManager::instance().nameAddress());

		pidfContents.setEntity(user.uri());
		pidfContents.setSimpleStatus(status==QilexPresenceHandler::AVAILABLE, message.c_str());
		pidfContents.setSimpleId("0");

		DumLocker lock(this);
		if (mClientPublicationHandle.isValid())
		{
			mClientPublicationHandle->update(&pidfContents);
		}
		else
		{
			resip::SharedPtr<resip::UserProfile> userProfile = resip::SharedPtr<resip::UserProfile>(new resip::UserProfile(dum().getMasterProfile()));
			userProfile->setInstanceId(dum().getMasterUserProfile().get()->getInstanceId());

			resip::SharedPtr<resip::SipMessage> msg = dum().makePublication(user,userProfile,pidfContents,presenceEventType, QilexConfig::getPresencePublicationExpire());
			dum().send(msg);

		}
	}
	catch (...)
	{
		LOGE("QilexPresenceMangaer::publish exception");
	}
}



resip::ClientPublicationHandle& QilexPresenceManager::clientPublicationHandle()
{
	CALL_TRACK;
	return mClientPublicationHandle;
}

void QilexPresenceManager::setClientPublicationHandle(const resip::ClientPublicationHandle& handle)
{
	CALL_TRACK;
	mClientPublicationHandle = handle;
}
  

void QilexPresenceManager::presenceSubscriptionAdded(std::string& uri,const resip::ClientSubscriptionHandle& handle)
{
  CALL_TRACK;
  LOGI("%s", uri.c_str());
  // std::pair<std::map<std::string,resip::ClientSubscriptionHandle>::iterator,bool> insertTry;
  // insertTry = mPresenceSubscriptions.insert(std::pair<std::string,resip::ClientSubscriptionHandle>(std::string(uri),resip::ClientSubscriptionHandle()));
  // if (!insertTry.second)
  //   {
  //     LOGI("handle already exist");
  //     return;
  //   }
  mPresenceSubscriptions[uri].mHandle = handle;
  mPresenceSubscriptions[uri].mPending = false;
  std::stringstream ss;
  ss << "handle is ";
  ss << mPresenceSubscriptions[uri].mHandle.isValid() ? "valid" : "invaldi";
  ss << mPresenceSubscriptions[uri].mHandle.getId();
  LOGI("%s", ss.str().c_str());
}



void QilexPresenceManager::updateSubscription(std::string& uri, const resip::ClientSubscriptionHandle& handle, bool pending)
{
	CALL_TRACK;
	mPresenceSubscriptions[uri].mHandle = handle;
	mPresenceSubscriptions[uri].mPending = pending;
}

void QilexPresenceManager::resetClientPublicationHandle()
{
	CALL_TRACK;
	if (mClientPublicationHandle.isValid())
    {
		mClientPublicationHandle->end();
    }
	else
    {
		LOGI("invalid publication handle");
    }
	mClientPublicationHandle = resip::ClientPublicationHandle();
}

void QilexPresenceManager::updateSubscriptionsBindings()
{
	CALL_TRACK;

	DumLocker lock(this);

	std::map<std::string,SubscriptionStruct>::iterator iterator;
	for (iterator = mPresenceSubscriptions.begin(); iterator!=mPresenceSubscriptions.end(); iterator++)
	{
		if (iterator->second.mHandle.isValid() && !iterator->second.mPending)
		{
			iterator->second.mHandle->requestRefresh();
		}
	}
}
