// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexSdp.hxx"

#include <sstream>
#include "QilexLogger.hxx"
#include "QilexIce.hxx"
#include "QilexSdpSecure.hxx"
#include "srtp.h"
#include "IStreamTransport.h"
#include "QilexNET.hxx"
#include "QilexStatemachine.hxx"

#include "AudioMgr.h"

//#define ENABLE_ICE

QilexSdp::QilexSdp() {
}

void QilexSdp::setupSession(SdpContents & contents, const char* outIp,
		bool sendOnly) {
	contents.session().origin().setAddress(outIp);
	contents.session().origin().user() = "Qilex";
	UInt64 id = time(0);
	contents.session().origin().getSessionId() = id;
	contents.session().origin().getVersion() = id;
	contents.session().name() = "Qilex";

	if (sendOnly)
		contents.session().addAttribute("sendonly");
	else
		contents.session().addAttribute("sendrecv");

	SdpContents::Session::Connection sdpConnection(SdpContents::IP4, outIp);
	contents.session().connection() = sdpConnection;
}

void QilexSdp::setupMedium(SdpContents::Session::Medium & medium, int outPort,bool enableSecurity) {
	medium.name() = "audio";
	medium.protocol() = enableSecurity? "RTP/SAVP":"RTP/AVP";
	medium.setPort(outPort);
}

void QilexSdp::setupRtcp(SdpContents::Session::Medium & medium,
		const std::string & outIp, const int outPort) {
	std::stringstream ssRCTP;
	ssRCTP << outPort + 1 << " IN IP4 " << outIp;
	medium.addAttribute("rtcp", ssRCTP.str().c_str());
}

void QilexSdp::addICEAttributes(SdpContents::Session & session,
		SdpContents::Session::Medium & medium, bool bICE, bool bICEmismatch) {
#ifdef ENABLE_ICE
	if (bICEmismatch)
	medium.addAttribute("ice-mismatch");

	if (bICE)
	{
		char dictionary[65] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+/"; //64 + '\0'
		char ufrag[5]= {0};
		char pwd[23]= {0};
		for (int i=0; i<sizeof(ufrag)-1; ++i) ufrag[i] = dictionary[rand()%(sizeof(dictionary)-1)];
		for (int i=0; i<sizeof(pwd)-1; ++i) pwd[i] = dictionary[rand()%(sizeof(dictionary)-1)];

		session.addAttribute("ice-pwd", pwd);
		session.addAttribute("ice-ufrag", ufrag);
	}
#endif
}

void QilexSdp::offerCandidates(SdpContents::Session::Medium & medium, bool bICE,
		std::set<ICE_Candidate>::iterator iter,
		std::set<ICE_Candidate>::iterator end) {
#ifdef ENABLE_ICE
	if(bICE)
	{
		for (; iter!=end; ++iter)
		{
			ICE_Candidate candidate = *iter;
			std::stringstream ssData;

			ssData << (int)candidate.m_nFoundation << " 1 UDP " << candidate.m_nPriority << " " << candidate.m_strIP << " " << candidate.m_nPort << " typ ";
			if (candidate.m_eType == ICEtypes::HOST_TYPE || candidate.m_eType == ICEtypes::HOST_REFLEXIVE_TYPE)
			ssData << "host";
			else if (candidate.m_eType == ICEtypes::RELAYED_TYPE)
			ssData << "relay";
			else
			ssData << "srflx";

			medium.addAttribute("candidate", ssData.str().c_str());

			SIPXLOGI("QilexSdp::offerCandidates %s", ssData.str().c_str());
		}
	}
#endif
}

void QilexSdp::makeSdpOffer(SdpContents & contents, QilexNET & net,
		bool suspended, bool iceSupported, bool bicemismatch, bool bNoRelayed,
		QilexSdpSecure & pSdpSecure, bool & bConnectedAsRelayed,bool bOutCall) {
	SdpContents::Session::Medium sdpMedium;

	std::string connectionIP;
	unsigned short connectionPort;
	bConnectedAsRelayed = false;

	net.gatherAddresses(bNoRelayed, !bOutCall);
	net.getBestCandidate(connectionIP, connectionPort, bConnectedAsRelayed,
			bNoRelayed);

	if (!bConnectedAsRelayed) {
		net.rebindWithoutTurn();
		SIPXLOGI("QilexSdp::makeSdpOffer connectionWithoutTurn");
	}

	QilexSdp::setupSession(contents, connectionIP.c_str(), false);
	QilexSdp::addICEAttributes(contents.session(), sdpMedium, iceSupported,
			bicemismatch);
	QilexSdp::setupMedium(sdpMedium, connectionPort, bOutCall);
	if(!bOutCall)
	    QilexSdp::setupRtcp(sdpMedium, connectionIP.c_str(), connectionPort);

	pSdpSecure.generateKey(sdpMedium);

	AudioMgr *audioManager = AudioMgr::Get();
	audioManager->setOutCall(bOutCall);
	std::list<SDPCodecDescriptor> codecs =
			audioManager->GetSupportedCodecList();
	for (std::list<SDPCodecDescriptor>::const_iterator it = codecs.begin(),
			end = codecs.end(); it != end; ++it) {
		Data type(it->sdpCodecType.c_str());
		int payload = it->payloadType;
		int sampleRate = it->sampleRate;
		LOGE("CODES: %d, %d", payload, sampleRate);
		resip::SdpContents::Session::Codec codec(type, payload, sampleRate);
		sdpMedium.addCodec(codec);
	}
	contents.session().addMedium(sdpMedium);
}

bool QilexSdp::checkForSuspend(const SdpContents::Session & session) {
	bool suspend = false;
	if (session.exists("sendonly")
			|| session.connection().getAddress() == "0.0.0.0") {
		suspend = true;
	} else {
		SdpContents::Session::MediumContainer::const_iterator iter;
		for (iter = session.media().begin(); iter != session.media().end();
				iter++) {
			if ((*iter).exists("sendonly")) {
				suspend = true;
			}
		}
	}
	return suspend;
}

bool QilexSdp::getConnectionParams(const SdpContents::Session & session,
		std::string & remoteIP, unsigned int & remotePort,
		std::string & remoteRtcpIP, unsigned int & remoteRtcpPort,
		SdpContents::Session::MediumContainer::const_iterator& mediaIter) {
	bool bFound = false;

	SdpContents::Session::MediumContainer::const_iterator iter;
	for (iter = session.media().begin();
			!bFound && iter != session.media().end(); iter++) {
		int remoteP = (*iter).port();

		if (remoteP == 0)
			continue;

		std::string strRtcpIp;
		unsigned short nRtcpPort = 0;
		std::list<Data> lRTCP = (*iter).getValues("rtcp");
		if (!lRTCP.empty()) {
			std::string strTmp;
			std::istringstream ssReader(lRTCP.front().c_str());
			ssReader >> nRtcpPort >> strTmp >> strTmp >> strRtcpIp;
		}

		mediaIter = iter;

		std::list<SdpContents::Session::Connection> lConnections =
				(*iter).getConnections();

		std::list<SdpContents::Session::Connection>::const_iterator iterCon;
		for (iterCon = lConnections.begin(); iterCon != lConnections.end();
				iterCon++) {
			Data strRemoteIP = (*iterCon).getAddress();
			if (strRtcpIp.empty() && nRtcpPort != 0) {
				strRtcpIp = strRemoteIP.c_str();
			}

			if (!strRemoteIP.empty()) {
				remoteIP = strRemoteIP.c_str();
				remotePort = remoteP;
				remoteRtcpIP = strRtcpIp.c_str();
				remoteRtcpPort = nRtcpPort;
				bFound = true;
				SIPXLOGI("Connection params ip=%s:%d    rtcp:  ip=%s:%d",
						remoteIP.c_str(), remotePort, remoteRtcpIP.c_str(),
						remoteRtcpPort);
				break;
			}
		}
	}

	return bFound;
}

bool QilexSdp::setupCodec(const SdpContents::Session & session,
		SDPCodecDescriptor& codecDescriptor) {
	SdpContents::Session::MediumContainer::const_iterator iter;
	for (iter = session.media().begin(); iter != session.media().end();
			++iter) {
		SdpContents::Session::Codec codec = (*iter).codecs().front();
		if ((*iter).port() != 0) {
			codecDescriptor.sdpCodecType = codec.getName().c_str();
			codecDescriptor.sampleRate = codec.getRate();
			codecDescriptor.payloadType = codec.payloadType();
			break;
		}
	}
}

void QilexSdp::checkForCandidates(const SdpContents::Session & session,
		std::string & strCandidateIP, unsigned short & nCandidatePort,
		bool & mismatch, bool & icesupport) {
	icesupport = true;
	mismatch = false;

#ifdef ENABLE_ICE
	SdpContents::Session::MediumContainer::const_iterator iterMedia;
	for (iterMedia=session.media().begin(); iterMedia!=session.media().end(); ++iterMedia)
	{
		if((*iterMedia).getConnections().size()==0)
		return;

		std::string strMediaIp = (*iterMedia).getConnections().front().getAddress().c_str();
		unsigned short nMediaPort = (*iterMedia).port();

		bool bMatch = false;
		std::list<Data> candidates = (*iterMedia).getValues("candidate");
		for(std::list<Data>::iterator iterD = candidates.begin(); iterD!=candidates.end(); iterD++)
		{
			std::string tmpStr;
			std::istringstream ssReader((*iterD).c_str());
			ssReader >> tmpStr >> tmpStr >> tmpStr >> tmpStr >> strCandidateIP >> nCandidatePort;

			SIPXLOGI("QilexSdp::checkForCandidates candidate: %s %d", strCandidateIP.c_str(), nCandidatePort);

			if(nCandidatePort==nMediaPort && strMediaIp==strCandidateIP)
			{
				bMatch = true;
				break;
			}
		}
		if(!bMatch)
		{
			mismatch = true;
			icesupport = false;
		}
	}
#else
	icesupport = false;
#endif
}

bool QilexSdp::checkForNoRelayed(const SdpContents::Session & session) {
	bool bNoRelayed = false;
	if (session.exists("nortpproxy")) {
		bNoRelayed = true;
	} else {
		SdpContents::Session::MediumContainer::const_iterator iter;
		for (iter = session.media().begin(); iter != session.media().end();
				++iter) {
			if ((*iter).exists("nortpproxy")) {
				bNoRelayed = true;
				break;
			}
		}
	}
	return bNoRelayed;
}

bool QilexSdp::checkMediumName(const SdpContents::Session::Medium & m) {
	return m.name() == "audio";
}

bool QilexSdp::checkMediumProtocol(const SdpContents::Session::Medium & m) {
	return m.protocol() == "RTP/AVP";
}

void QilexSdp::matchCodecFromOffer(const SdpContents::Session & session,
		SdpContents::Session & contents_session,
		SdpContents::Session::Medium & sdpMedium,
		SDPCodecDescriptor& codecDescriptor, QilexSdpSecure& pSdpSecure) {

	SIPXLOGI("QilexSdp::matchCodecFromOffer");

	SIPXLOGI("QilexSdp::matchCodecFromOffer +");
	SdpContents::Session::MediumContainer::const_iterator iter;
	SIPXLOGI("QilexSdp::matchCodecFromOffer -");
	for (iter = session.media().begin(); iter != session.media().end();
			++iter) {
		std::string strCipherData;
		SdpContents::Session::Medium medium = (*iter);

		bool cipherDataOK;
		cipherDataOK = pSdpSecure.preprateCipherData(iter, strCipherData);

		SdpContents::Session::Codec codec = medium.findFirstMatchingCodecs(
				sdpMedium);

		SIPXLOGI("QilexSdp::matchCodecFromOffer 2");

		SIPXLOGI("QilexSdp::matchCodecFromOffer 3");

		//srtp impl
		SIPXLOGI("QilexSdp::matchCodecFromOffer offer name: %s  protocol: %s ",
				medium.name().c_str(), medium.protocol().c_str());

		if (checkMediumName(medium) && checkMediumProtocol(medium)
				&& medium.port() != 0 && !codec.getName().empty()
				&& cipherDataOK) {
			sdpMedium.clearAttribute("crypto");

			pSdpSecure.setCryptoInMedium(sdpMedium, strCipherData);

			sdpMedium.clearCodecs();
			sdpMedium.addCodec(codec);
			contents_session.addMedium(sdpMedium);

			codecDescriptor.sdpCodecType = codec.getName().c_str();
			codecDescriptor.sampleRate = codec.getRate();
			codecDescriptor.payloadType = codec.payloadType();

			break; //temp
		} else {
			medium.clearCodecs();
			medium.setPort(0);
			contents_session.addMedium(medium);
		}
	}
}
