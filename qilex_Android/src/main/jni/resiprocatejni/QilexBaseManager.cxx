// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexBaseManager.hxx"
#include "QilexLogger.hxx"
#include "QilexRegistrationManager.hxx"
#include <resip/dum/DialogUsageManager.hxx>
#include <rutil/Data.hxx>
#include <string>
#include <list>


extern bool gRestartStack;


// Static data initialization
auto_ptr<QilexResipPool> QilexBaseManager::mResipPool;


// class implementation

void QilexBaseManager::initialize()
{
  mResipPool.reset(new QilexResipPool);
}


void QilexBaseManager::shutdown()
{
  mResipPool.reset(0);
}


resip::DialogUsageManager& QilexBaseManager::dum()
{
  return mResipPool->dum();
}

QilexBaseManager::QilexBaseManager()
{
  
}

QilexBaseManager::~QilexBaseManager()
{
}


void QilexBaseManager::startThread()
{
  mResipPool->startThread();
}

void QilexBaseManager::stopThread()
{
	mResipPool->stopThread();
}

void QilexBaseManager::dumLock()
{
  mResipPool->dumLock();
}

void QilexBaseManager::dumUnlock()
{
  mResipPool->dumUnlock();
}

void QilexBaseManager::sync()
{
	mResipPool->dumLock();
	mResipPool->dumUnlock();
}

// const string& QilexBaseManager::username() const
// {
//   return QilexRegistrationManager::mUsername;
// }





