// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.
#include <android/log.h>

#include "QilexRegistrationManager.hxx"
#include "QilexLogger.hxx"
#include "QilexStatemachine.hxx"
#include "QilexSdp.hxx"
#include <resip/dum/DialogUsageManager.hxx>
#include <resip/dum/ClientRegistration.hxx>
#include <resip/dum/AppDialogSet.hxx>
#include <resip/stack/SipStack.hxx>
#ifdef USE_SSL
#include "resip/stack/ssl/Security.hxx"
#endif
#include <rutil/BaseException.hxx>
#include <rutil/DnsUtil.hxx>
#include <boost/system/error_code.hpp>
#include <cassert>
#include <list>
#include <utility>
#include <sstream>
#include "QilexNET.hxx"
#include "QilexFakeDNS.hxx"
#include "QilexConfig.hxx"

using namespace std;

QilexRegistrationManager* QilexRegistrationManager::mInstance = 0;

QilexRegistrationManager& QilexRegistrationManager::instance() {
	//CALL_TRACK;
	if (!mInstance) {
		mInstance = new QilexRegistrationManager();
	}
	return *mInstance;
}

QilexRegistrationManager::QilexRegistrationManager() :
		mNameAddr(0), mPublicPort(0), mDomainCtx(NULL), mRegisterRequested(
				false) {
	CALL_TRACK
	;
	mRegistrationHandler = new QilexRegistrationHandler();
	dumLock();
	resetHandler();
	dumUnlock();
}

QilexRegistrationManager::~QilexRegistrationManager() {
	delete mRegistrationHandler;
}

void QilexRegistrationManager::resetRegistrationHandle() {
	mClientRegistrationHandle = resip::ClientRegistrationHandle();
}

void QilexRegistrationManager::resetHandler() {
	dum().setClientRegistrationHandler(mRegistrationHandler);
}

void QilexRegistrationManager::sendRegister() {
	CALL_TRACK
	;
	LOGI("SIP REGISTER REQUESTED");

	if (!mNameAddr) {
		LOGE("Ip address not set");
		return;
	}
	sendUnregister();

	LOGI("%s", mKey.c_str());
	LOGI("%s", mCertificate.c_str());
	LOGI("%s", mPubKey.c_str());

	dumLock();
	mRegisterRequested = true;
	SharedPtr<UserProfile> userProfile = SharedPtr<UserProfile>(
			new resip::UserProfile(dum().getMasterProfile()));
	userProfile->setInstanceId(dum().getMasterProfile().get()->getInstanceId());

	try {
		mMessage = dum().makeRegistration(*mNameAddr, userProfile,
				QilexConfig::getReRegisterTime());
	} catch (std::exception& e) {
		std::stringstream es;
		es << e.what();
		LOGI("QilexRegistrationManager::sendRegister() exc. %s",
				es.str().c_str());
	}

	mMessage->header(h_XAuthenticate).value() = Data("fastauth " + mToken);
	mMessage->header(h_XPushOS).value() = Data(mPushOs);
	mMessage->header(h_XPushDeviceID).value() = Data(mPushDeviceId);
	mMessage->header(h_XPushToken).value() = Data(mPushToken);

	//LOGI("SIGMA-PHUCPV debug message %s", mMessage);
	dum().send(mMessage);
	dumUnlock();

	QilexStatemachine sm;
	sm.sendEvent(QilexStatemachine::EVENT_REGISTERING);
}

void QilexRegistrationManager::sendUnregister() {
	CALL_TRACK
	;
	LOGI("SIP UNREGISTER REQUESTED");

	DumLocker lock(this);

	mRegisterRequested = false;

	if (mClientRegistrationHandle.isValid()) {
		LOGI(
				"QilexRegistrationManager::sendUnregister() trying to end registration handle");
		try {
			if (!mClientRegistrationHandle->allContacts().empty()) {
				mClientRegistrationHandle->end();
				mClientRegistrationHandle->getAppDialogSet()->end();
			} else
				mClientRegistrationHandle->stopRegistering();
		} catch (BaseException& be) {
			LOGI("QilexRegistrationManager::sendUnregister() exception caught");
		}
	}
}

void QilexRegistrationManager::updateBinding() {
	CALL_TRACK
	;
	DumLocker lock(this);
	if (mClientRegistrationHandle.isValid()
			&& !mClientRegistrationHandle->allContacts().empty())
		mClientRegistrationHandle->requestRefresh(0);
}

void QilexRegistrationManager::setClientRegistrationHandle(
		const resip::ClientRegistrationHandle& handle) {
	if (mClientRegistrationHandle == handle)
		return;

	CALL_TRACK
	;
	if (mClientRegistrationHandle.isValid()) {
		LOGI(
				"setClientRegistrationHandle: Previous RegistrationHandle is still valid");
		if (!mClientRegistrationHandle->myContacts().empty())
			LOGE(
					"setClientRegistrationHandle: Previous RegistrationHandle is not ended!");
	}
	mClientRegistrationHandle = handle;
}

resip::ClientRegistrationHandle& QilexRegistrationManager::getClientRegistrationHandle() {
	return mClientRegistrationHandle;
}

void QilexRegistrationManager::setCredentials(const string& domain,
		const string& username, const string& password,
		const string& instanceId) {
	CALL_TRACK
	;
	mUsername = username;
	mDomain = domain;

	resip::SharedPtr<resip::MasterProfile> masterProfile;

	dumLock();
	masterProfile = dum().getMasterProfile();

	bool bHasMasterProfile = masterProfile.get() == NULL ? false : true;

	if (!bHasMasterProfile) {
		masterProfile = resip::SharedPtr<resip::MasterProfile>(
				new resip::MasterProfile);
		fillMasterProfile(masterProfile);
	}

	masterProfile.get()->setDigestCredential(resip::Data(domain.c_str()),
			resip::Data(username.c_str()), resip::Data(password.c_str()));
	if (!instanceId.empty())
		masterProfile.get()->setInstanceId(resip::Data(instanceId.c_str()));

#if SIP_USE_SIPS
	string defaultForm = "sips:";
#else
	string defaultForm = "sip:";
#endif
	defaultForm += username;
	defaultForm += "@";
	defaultForm += domain;

	masterProfile.get()->setDefaultFrom(NameAddr(defaultForm.c_str()));

	if (!bHasMasterProfile) {
		auto_ptr<resip::ClientAuthManager> clientAuth(
				new resip::ClientAuthManager);
		dum().setClientAuthManager(clientAuth);
		dum().setMasterProfile(masterProfile);
	}
	dumUnlock();
}

void QilexRegistrationManager::setIpAddress(const string& ipAddress) {
#if SIP_USE_SIPS
	string nameAddr = "sips:";
#else
	string nameAddr = "sip:";
#endif
	nameAddr += mUsername;
	nameAddr += "@";
	nameAddr += ipAddress;

	LOGI("QilexRegistrationManager::setIpAddress %s", nameAddr.c_str());
	mNameAddr = new resip::NameAddr(nameAddr.c_str());
}

void QilexRegistrationManager::setToken(const string& token) {
	mToken = token;
}

void QilexRegistrationManager::setAliases(vector<string>& aliases) {
	mAliases = aliases;
}

void QilexRegistrationManager::setPushInfo(const string& pushOs,
		const string& pushDeviceId, const string& pushToken) {
	mPushOs = pushOs;
	mPushDeviceId = pushDeviceId;
	mPushToken = pushToken;
}

const vector<string>& QilexRegistrationManager::aliases() const {
	return mAliases;
}
void QilexRegistrationManager::setKeyandCert(const string& privkey,
		const string& pubkey, const string& cert) {
	mKey = privkey;
	mCertificate = cert;
	mPubKey = pubkey;
	QilexNET::setCrypto(nameAddress().uri().toString().c_str(), mCertificate,
			mKey);
	dumLock();
	if (dum().getSecurity())
		try {
			dum().getSecurity()->removeDomainCert("sip.ct.plus.pl");
			dum().getSecurity()->removeDomainPrivateKey("sip.ct.plus.pl");

			dum().getSecurity()->addDomainCertPEM("sip.ct.plus.pl",
					resip::Data(mCertificate));
			dum().getSecurity()->addDomainPrivateKeyPEM("sip.ct.plus.pl",
					resip::Data(mKey));

			if (mDomainCtx) {
				SSL_CTX_free(mDomainCtx);
				mDomainCtx = NULL;
			}

			mDomainCtx = dum().getSecurity()->createDomainCtx(TLSv1_method(),
					"sip.ct.plus.pl");
			dum().getSecurity()->setDomainCtx(mDomainCtx);
		} catch (...) {
			TLSLOGI("Setting user certificate failed");
		}
	else
		TLSLOGI("Brak Security");

	dumUnlock();
}
const resip::NameAddr& QilexRegistrationManager::nameAddress() const {
	assert(mNameAddr);
	return *mNameAddr;
}

void QilexRegistrationManager::fillMasterProfile(
		resip::SharedPtr<resip::MasterProfile> masterProfile) {
	LOGI("filling MasterProfile");
	masterProfile->clearSupportedMethods();
	masterProfile->addSupportedMethod(resip::INVITE);
	masterProfile->addSupportedMethod(resip::ACK);
	masterProfile->addSupportedMethod(resip::CANCEL);
	masterProfile->addSupportedMethod(resip::OPTIONS);
	masterProfile->addSupportedMethod(resip::BYE);
	//	masterProfile->addSupportedMethod(resip::REFER);
	masterProfile->addSupportedMethod(resip::NOTIFY);
	masterProfile->addSupportedMethod(resip::SUBSCRIBE);
	//	masterProfile->addSupportedMethod(resip::UPDATE);
	//	masterProfile->addSupportedMethod(resip::INFO);
	masterProfile->addSupportedMethod(resip::MESSAGE);
	//	masterProfile->addSupportedMethod(resip::PRACK);

#if SIP_USE_SIPS
	masterProfile->addSupportedScheme("sips");
#else
	masterProfile->addSupportedScheme("sip");
#endif

	masterProfile->addSupportedMimeType(resip::MESSAGE,
			resip::Mime("text", "plain"));
	masterProfile->addSupportedMimeType(resip::INVITE,
			resip::Mime("application", "sdp"));
	masterProfile->addSupportedMimeType(resip::ACK,
			resip::Mime("application", "sdp"));
	masterProfile->addSupportedMimeType(resip::NOTIFY,
			resip::Mime("application", "pidf+xml"));

	masterProfile->addSupportedOptionTag(
			resip::Token(resip::Symbols::Outbound));
	masterProfile->addSupportedOptionTag(resip::Token(resip::Symbols::Path));
	masterProfile->setExpressOutboundAsRouteSetEnabled(true);
	masterProfile->setRegId(1);
	masterProfile->clientOutboundEnabled() = true;
	//LOGI(mPublicIP.c_str());
	masterProfile->setOutboundDecorator(
			m_pQilexMessageDecorator = resip::SharedPtr<QilexMessageDecorator>(
					new QilexMessageDecorator(mUsername)));

	//masterProfile->setDefaultRegistrationRetryTime(-1);
}

void QilexRegistrationManager::setDefaultRegistrationRetryTime(int i) {
	dum().getMasterProfile()->setDefaultRegistrationRetryTime(i);
}

int QilexRegistrationManager::getDefaultRegistrationRetryTime() {
	return dum().getMasterProfile()->getDefaultRegistrationRetryTime();
}
