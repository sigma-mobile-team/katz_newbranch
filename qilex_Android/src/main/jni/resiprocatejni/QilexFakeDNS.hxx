/*********************************/
/* QilexFakeDNS.h                */
/* Lukasz Rychter 2011/11/08     */
/*                               */
/* P2P DNS                       */
/*********************************/

#pragma once 

#include <map>
#include <set>
#include <string>
#include "../resiprocate/rutil/Mutex.hxx"

#define DEFAULT_SIP_HOST "sip.ct.plus.pl"
#define DEFAULT_TURN_HOST "turn.halo.pl"
#define DEFAULT_TURN_PORT 5349

class QilexFakeDNS
{
public:
	static void AddEntry(const std::string& strHost, const std::string& strIP);
	
	static void Reset();

	static std::string Lookup(const std::string& strHost, bool bForceNew=false);

	static void AddToBlacklist(const std::string& strIP);
	static void ResetBlacklistForHost(const std::string& strHost);
	static void RemoveFromBlacklist(const std::string& strIP);
	static bool IsBlacklisted(const std::string& strIP);

	static bool SetPreferred(const std::string& strHost, const std::string& strIP);

protected:
	static std::multimap<std::string, std::string>	m_mmQilexFakeDnsList;
	static resip::Mutex								m_csAccess;

	static std::map<std::string, std::string>		m_mChosenIPs;
	static std::set<std::string>					m_sBlacklist;
};
