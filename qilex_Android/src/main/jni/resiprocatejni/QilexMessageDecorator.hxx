// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_MESSAGE_DECORATOR_HXX
#define QILEX_MESSAGE_DECORATOR_HXX

#include <resip/stack/MessageDecorator.hxx>
#include <resip/stack/SipMessage.hxx>
#include <resip/stack/Tuple.hxx>
#include <rutil/Data.hxx>

class QilexMessageDecorator : public resip::MessageDecorator
{
public:

  QilexMessageDecorator(std::string username);

  virtual void decorateMessage(resip::SipMessage &msg, const resip::Tuple &source, const resip::Tuple &destination, const resip::Data& sigcompId);

  virtual void rollbackMessage(resip::SipMessage& msg);

  virtual resip::MessageDecorator* clone() const;
  
private:
  
  std::string mUsername;
};

#endif
