// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexPagerMessageManager.hxx"
#include "QilexPagerMessageHandler.hxx"
#include "QilexLogger.hxx"
#include <resip/dum/PagerMessageHandler.hxx>
#include <resip/dum/ServerPagerMessage.hxx>
#include <resip/dum/ClientPagerMessage.hxx>
#include <resip/stack/SipMessage.hxx>
#include <resip/stack/Contents.hxx>
#include <rutil/SharedPtr.hxx>
#include <algorithm>
#include <string>
#include <cassert>
#include <sstream>

using namespace resip;

void QilexPagerMessageHandler::onSuccess(resip::ClientPagerMessageHandle handle, const resip::SipMessage& status)
{
  CALL_TRACK;

  assert(handle.isValid());
  handle->end();

  std::string strCallId = status.header(resip::h_CallId).value().c_str();
  std::stringstream ss;
  ss << status.header(resip::h_StatusLine).responseCode();
  std::string strCode = ss.str();

  LOGE("msg::onSuccess %s", strCode.c_str());

  std::string nId = mMessageIDs[strCallId];
  mMessageIDs.erase(strCallId);

  mStatemachine.addConfirmedMessage(nId, strCode.c_str());
  mStatemachine.sendEvent(QilexStatemachine::EVENT_MESSAGESENTCONFIRMED);
}

void QilexPagerMessageHandler::onFailure(resip::ClientPagerMessageHandle handle, const resip::SipMessage& status, std::auto_ptr<resip::Contents> contents)
{
  CALL_TRACK;

  assert(handle.isValid());
  handle->end();


  std::stringstream ss;
  ss << status.header(resip::h_StatusLine).responseCode();
  std::string strCode = ss.str();
  LOGI("msg::onFailure %s", strCode.c_str());

  std::string strCallId = status.header(resip::h_CallId).value().c_str();
  std::string nId = mMessageIDs[strCallId];
  mMessageIDs.erase(strCallId);
  LOGI("msg::onFailure msgId %s", nId.c_str());
  if(strcmp(nId.c_str(),"-1000") == 0 || strcmp(nId.c_str(),"-1001") == 0){
	 // NOT SEND EVENT
//	  LOGE("msg::onFailure not send Event");
  }
  else
  {
	 mStatemachine.addFailedMessage(nId, strCode);
	 mStatemachine.sendEvent(QilexStatemachine::EVENT_MESSAGESENDFAILURE);
  }
}

void QilexPagerMessageHandler::onMessageArrived(resip::ServerPagerMessageHandle handle, const resip::SipMessage& message)
{
	CALL_TRACK;

	// Check is composing message
	if (message.exists(resip::h_XMessageComposingInfo)) {
		const char* strTimestmp;
		const char* strType;
		  const char* strGroupChatUUID = std::string("").c_str();
		  const char* strGroupChatMsgType = std::string("").c_str();
		  const char* strGroupChatParticipants = std::string("").c_str();
		  const char* strGroupChatAdministrator = std::string("").c_str();
		  if (message.exists(resip::h_XGroupChatUUID)) {
  		          strGroupChatUUID = message.header(resip::h_XGroupChatUUID).value().c_str();
		  }
		  if (message.exists(resip::h_XGroupChatMessageType)) {
			  strGroupChatMsgType = message.header(resip::h_XGroupChatMessageType).value().c_str();
		  }
		  if (message.exists(resip::h_XGroupChatParticipants)) {
			  strGroupChatParticipants = message.header(resip::h_XGroupChatParticipants).value().c_str();
		  }
		  if (message.exists(resip::h_XGroupChatAdministrator)) {
			  strGroupChatAdministrator = message.header(resip::h_XGroupChatAdministrator).value().c_str();
		  }
		if (message.exists(resip::h_Timestamp))
		{
			strTimestmp = message.header(resip::h_Timestamp).value().c_str();
		}
		else {
			assert(0);
		}
		std::string compose = message.header(resip::h_XMessageComposingInfo).value().c_str();
		LOGE("Send Messge Composing: %s", compose.c_str());
		mStatemachine.addIncomingMessage(
			string(message.header(resip::h_From).uri().getAorNoPort().c_str()), //message.header(resip::h_From)
			string(message.header(resip::h_To).uri().getAorNoPort().c_str()), //message.header(resip::h_To)
			string(std::string("-1000")), // id
			string(strTimestmp), // timestamp
			string(compose.c_str()), // content
			string(std::string("2003")), // Type
			string(std::string("")), // uuid
			string(strGroupChatMsgType),
			string(strGroupChatUUID),
			string(strGroupChatParticipants),
			string(strGroupChatAdministrator)
			);

		mStatemachine.sendEvent(QilexStatemachine::EVENT_MESSAGENEWINCOMING);
		return;
	}

	resip::SharedPtr<resip::SipMessage> ok = handle->accept();
	handle->send(ok);

	const char* strTimestmp;
	const char* strType = std::string("").c_str();
	const char* struuid = std::string("").c_str();

	if (message.exists(resip::h_Timestamp))
	{
		strTimestmp = message.header(resip::h_Timestamp).value().c_str();
	}
	else
		assert(0);

	if (message.exists(resip::h_XMessageType)) {
		strType = message.header(resip::h_XMessageType).value().c_str();
	}

	if (message.exists(resip::h_XMessageUUID)) {
		struuid = message.header(resip::h_XMessageUUID).value().c_str();
	}

  resip::Contents* pContents = message.getContents();
  LOGE("Send Messge Content: %s", pContents->getBodyData().c_str());

  if (message.exists(resip::h_XGroupChatUUID)) {
	  const char* strGroupChatUUID = message.header(resip::h_XGroupChatUUID).value().c_str();
	  const char* strGroupChatMsgType = std::string("").c_str();
	  const char* strGroupChatParticipants = std::string("").c_str();
	  const char* strGroupChatAdministrator = std::string("").c_str();
	  if (message.exists(resip::h_XGroupChatMessageType)) {
		  strGroupChatMsgType = message.header(resip::h_XGroupChatMessageType).value().c_str();
	  }
	  if (message.exists(resip::h_XGroupChatParticipants)) {
		  strGroupChatParticipants = message.header(resip::h_XGroupChatParticipants).value().c_str();
	  }
	  if (message.exists(resip::h_XGroupChatAdministrator)) {
		  strGroupChatAdministrator = message.header(resip::h_XGroupChatAdministrator).value().c_str();
	  }
	  mStatemachine.addIncomingMessage(
			string(message.header(resip::h_From).uri().getAorNoPort().c_str()), //message.header(resip::h_From)
			string(message.header(resip::h_To).uri().getAorNoPort().c_str()), //message.header(resip::h_To)
			string(std::string("0")), // id
			string(strTimestmp), // timestamp
			string(pContents->getBodyData().c_str()), // content
			string(strType), // type
			string(struuid), // uuid
			string(strGroupChatMsgType), // groupChatMsgType
			string(strGroupChatUUID), // groupChatUuid
			string(strGroupChatParticipants), // groupChatParticipants
			string(strGroupChatAdministrator)  // groupChatAdministrator
			);
  } else {
	  mStatemachine.addIncomingMessage(
			string(message.header(resip::h_From).uri().getAorNoPort().c_str()), //message.header(resip::h_From)
			string(message.header(resip::h_To).uri().getAorNoPort().c_str()), //message.header(resip::h_To)
			string(std::string("0")), // id
			string(strTimestmp), // timestamp
			string(pContents->getBodyData().c_str()), // content
			string(strType), // type
			string(struuid) // uuid
			);
  }

  mStatemachine.sendEvent(QilexStatemachine::EVENT_MESSAGENEWINCOMING);

}


