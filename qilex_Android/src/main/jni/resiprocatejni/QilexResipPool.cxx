// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexResipPool.hxx"
#include "QilexLogger.hxx"
#include <resip/dum/DialogUsageManager.hxx>
#include <resip/dum/AppDialogSetFactory.hxx>
#include <resip/stack/SipStack.hxx>
#include <resip/stack/SelectInterruptor.hxx>
#include <resip/stack/InterruptableStackThread.hxx>
#include <resip/stack/ssl/Security.hxx>
#include <resip/dum/DumThread.hxx>
#include <rutil/TransportType.hxx>
#include <rutil/Data.hxx>
#ifdef USE_SSL
#include <resip/stack/ssl/Security.hxx>
#endif
#include "../resiprocate/rutil/Condition.hxx"
#include "../resiprocate/rutil/Mutex.hxx"

#include "QilexPagerMessageManager.hxx"
#include "QilexPresenceManager.hxx"
#include "QilexRegistrationManager.hxx"
#include "QilexCallManager.hxx"
#include "QilexConfig.hxx"

#include "QilexOutOfDialogManager.hxx"

#include <sstream>
#include "RootCert.h"

#define QILEX_CUSTOM_THREADS

bool gThreadShutdown = true;
bool gRestartStack = false;
bool gPrepareNewStack = false;
time_t gShutdownStartTime = 0;
QilexResipPool* gPool = 0;
resip::Condition gShutdownCond;
resip::Mutex shutdownMutex;

void* processLoop(void* ptr)
{
	CALL_TRACK;
	while (gPool && !gThreadShutdown)
	{
		if(gRestartStack)
		{
			gShutdownStartTime = time(0);
			LOGE("processLoop restart");
			gPool->dumLock();
			((QilexResipPool*)ptr)->shutdown(false);
			gPool->dumUnlock();
			gRestartStack = false;
			gPrepareNewStack = true;
		}

		if (gShutdownStartTime != 0 && time(0)-gShutdownStartTime >= 5)
		{
			gShutdownStartTime = 0;

			gPool->dumLock();
			((QilexResipPool*)ptr)->shutdown(true);
			gPool->dumUnlock();
		}

		// init mNumReady
		resip::FdSet fdset;
		int err = fdset.selectMilliSeconds(200);

		gPool->dumLock();
		if(((QilexResipPool*)ptr)->pstack())
			((QilexResipPool*)ptr)->stack().process(250);
		gPool->dumUnlock();
		bool processResult = true;
		while(processResult)
		{
			gPool->dumLock();
			try
			{
				if(((QilexResipPool*)ptr)->pdum())
					processResult = ((QilexResipPool*)ptr)->dum().process();
			}
			catch (std::exception& e)
			{
				LOGE("Dum->Process() exception: %s", e.what());
			}
			catch(...)
			{
				LOGE("Unknown Dum->Process() exception!");
			}
			gPool->dumUnlock();
		}
	}

	//delete the old one
	gPool->dumLock();
	((QilexResipPool*)ptr)->deleteStack();

	QilexStatemachine sm;

	//and create a new one
	if(gPrepareNewStack)
	{
		gPrepareNewStack = false;
		((QilexResipPool*)ptr)->setupStack();

		QilexRegistrationManager::instance().resetHandler();
		QilexRegistrationManager::instance().resetRegistrationHandle();

		QilexCallManager::instance().resetHandler();
		QilexCallManager::instance().resetServerInviteSessionHandle();

		QilexPagerMessageManager::instance().resetHandler();
		QilexPagerMessageManager::instance().resetClientMessageHandle();
		QilexPagerMessageManager::instance().resetServerMessageHandle();

		QilexPresenceManager::instance().resetHandler();
		QilexPresenceManager::instance().resetClientPublicationHandle();

		QilexOutOfDialogManager::instance().resetHandler();
		((QilexResipPool*)ptr)->startThread(true);

		resip::Lock lock(shutdownMutex);
		gShutdownCond.signal();

		sm.sendEvent(QilexStatemachine::EVENT_REGISTER_SIPSTACKRESTARTED);
	}
	gPool->dumUnlock();

	sm.detachThread();
	return NULL;
}

void QilexResipPool::onDumCanBeDeleted()
{
	gShutdownStartTime = 0;
	gThreadShutdown = true;
}

QilexResipPool::QilexResipPool():mSipStackOptions(0), mSipStack(0) ,mDum(0)
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&mProcessThreadMutex, &attr);
	pthread_mutexattr_destroy(&attr);

	setupStack();
}

void QilexResipPool::setupStack()
{
	mSipStackOptions.reset(new resip::SipStackOptions());
	#if SIP_SERVER_USE_SSL
	{
		LOGI("QilexResipPool::setupStack ---: USE_SSL");
		resip::Security* pSecurity =  new resip::Security();
		pSecurity->addRootCertPEM(ROOT_CERT_QILEX);
		mSipStack.reset(new resip::SipStack(pSecurity));
	}
	#else
	{
		LOGI("QilexResipPool::setupStack ---: NOT_USE_SSL");
		resip::Security* pSecurity =  new resip::Security();
		mSipStack.reset(new resip::SipStack(pSecurity));
//		mSipStack.reset(new resip::SipStack(*mSipStackOptions));
		LOGI("QilexResipPool::setupStack ---: NOT_USE_SSL END");
	}
	#endif //SIP_SERVER_USE_SSL

	std::auto_ptr<resip::AppDialogSetFactory> apAppDialogSetFactory(new resip::AppDialogSetFactory());

	mDum.reset(new resip::DialogUsageManager(*mSipStack));

	setupTransport();

	mDum->setAppDialogSetFactory(apAppDialogSetFactory);
}

void QilexResipPool::setupTransport()
{
	try
	{
		#if SIP_SERVER_USE_SSL
		{
			LOGI("QilexResipPool::setupTransport ---: USE_SSL");
			mDum->addTransport(resip::TLS, 0, resip::V4, resip::Data::Empty, "sip.ct.plus.pl", resip::Data::Empty, resip::SecurityTypes::TLSv1);
		}
		#else
		{
			LOGI("QilexResipPool::setupTransport ---: NOT USE SSL");
			mDum->addTransport(resip::TCP, 0, resip::V4);
			LOGI("QilexResipPool::setupTransport ---: NOT USE SSL END");
		}
		#endif
	}
	catch(...)
	{
		LOGE("Nie mozna ustawic transportu TLS/TCP");
	}
}

QilexResipPool::~QilexResipPool()
{
  CALL_TRACK;
  stopThread();
  pthread_mutex_destroy(&mProcessThreadMutex);
}


resip::SipStackOptions& QilexResipPool::stackOptions()
{
  return *mSipStackOptions;
}

resip::SipStack& QilexResipPool::stack()
{
  return *mSipStack;
}

resip::DialogUsageManager& QilexResipPool::dum()
{
  return *mDum;
}

resip::SipStack* QilexResipPool::pstack()
{
	return mSipStack.get();
}

resip::DialogUsageManager* QilexResipPool::pdum()
{
	return mDum.get();
}

void QilexResipPool::startThread(bool forced)
{
#ifdef QILEX_CUSTOM_THREADS
  CALL_TRACK;
  gThreadShutdown = false;

  if (!gPool || forced)
    {
	  gPool = this;
      pthread_create(&mProcessThread,
                     NULL,
                     processLoop,
                     (void*)this);
    }
  gPool = this; //prawdopodobnie zbedny
#endif
}

void QilexResipPool::stopThread()
{
#ifdef QILEX_CUSTOM_THREADS
  CALL_TRACK;

  gThreadShutdown = true;

  LOGI("QilexResipPool::stopThread() gThreadShutdown true");

  timespec delay;
  delay.tv_sec = 5;
  delay.tv_nsec = 50 * 1000000;

  nanosleep(&delay, NULL);

  LOGI("QilexResipPool::stopThread() sleep end");

  if(mDum.get())
    mDum->forceShutdown(this);

  LOGI("QilexResipPool::stopThread() forceShutdown end");

  LOGI("QilexResipPool::stopThread() shutdown end");

  pthread_join(mProcessThread,NULL);

#endif
}

void QilexResipPool::shutdown(bool forced)
{
	if (forced)
		mDum->forceShutdown(this);
	else
		mDum->shutdown(this);
}

/// delete
void QilexResipPool::deleteStack()
{
  CALL_TRACK;
  mDum.reset();
  mSipStack->shutdown();
  mSipStackOptions.reset();
  mSipStack.reset();
}

void QilexResipPool::dumLock()
{
#ifdef QILEX_CUSTOM_THREADS
  //CALL_TRACK;
  pthread_mutex_lock(&mProcessThreadMutex);
#endif
}


void QilexResipPool::dumUnlock()
{
#ifdef QILEX_CUSTOM_THREADS
  //CALL_TRACK;
  pthread_mutex_unlock(&mProcessThreadMutex);
#endif
}

void QilexResipPool::restartStack()
{
	CALL_TRACK
	gRestartStack = true;
	resip::Lock lock(shutdownMutex);
	gShutdownCond.wait(shutdownMutex, 500);
}
