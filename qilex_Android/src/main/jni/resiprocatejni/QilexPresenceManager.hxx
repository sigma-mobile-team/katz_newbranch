// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEXPRESENCEMANAGER_HXX
#define QILEXPRESENCEMANAGER_HXX

#include "QilexBaseManager.hxx"
#include "QilexPresenceHandler.hxx"
#include <string>
#include <memory>
#include <map>

struct SubscriptionStruct
{
  resip::ClientSubscriptionHandle mHandle;
  bool mPending;
};

class QilexPresenceManager : public QilexBaseManager
{

public:

  static QilexPresenceManager& instance();

  virtual ~QilexPresenceManager();

  /// subscribe a contact
  void subscribe(const std::string& peerName);
 
  /// unsubscribe a contact
  void unsubscribe(const std::string& peerName);

  /// publish a status
  void publish(QilexPresenceHandler::Status, const std::string& message);

  resip::ClientPublicationHandle& clientPublicationHandle();

  void setClientPublicationHandle(const resip::ClientPublicationHandle& handle);

  void presenceSubscriptionAdded(std::string& uri,const resip::ClientSubscriptionHandle& handle);

  void updateSubscription(std::string& uri, const resip::ClientSubscriptionHandle& handle, bool pending);

  void resetHandler();

  void terminateHandler(const resip::ClientSubscriptionHandle& handle);

  void resetClientPublicationHandle();

  void updateSubscriptionsBindings();

private:

  QilexPresenceManager();


private: // data

  static QilexPresenceManager* mInstance;

  /// comprehensive presence handler
  std::auto_ptr<QilexPresenceHandler> mPresenceHandler;

  /// publication handle
  resip::ClientPublicationHandle mClientPublicationHandle;

  std::map<std::string,SubscriptionStruct> mPresenceSubscriptions;

};

#endif // QILEXPRESENCEMANAGER_HXX
