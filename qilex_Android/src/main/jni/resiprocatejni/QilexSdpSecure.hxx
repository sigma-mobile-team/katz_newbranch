// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_SDP_SECURE_HXX
#define QILEX_SDP_SECURE_HXX

#include <resip/stack/SdpContents.hxx>
#include "srtp.h"

using namespace resip;
class IStreamTransport;

#define QILEXSDPSECURE QilexSdpSecureImpl
//#define QILEXSDPSECURE QilexSdpSecureNull

class QilexSdpSecure
{
public:
	virtual void generateKey(SdpContents::Session::Medium & sdpMedium) = 0;
	virtual void setCryptoInMedium(SdpContents::Session::Medium & sdpMedium, const std::string & ssCipherData) = 0;
	virtual bool preprateCipherData(SdpContents::Session::MediumContainer::const_iterator & iter, std::string & strCipherData) = 0;
	virtual bool setConnectionCipher(SdpContents::Session::MediumContainer::const_iterator& iter) = 0;
	virtual bool secureStreamTransport(IStreamTransport & streamtransport) = 0;
	virtual bool getSecureStreamTransport(IStreamTransport & streamtransport) = 0;
};

#endif
