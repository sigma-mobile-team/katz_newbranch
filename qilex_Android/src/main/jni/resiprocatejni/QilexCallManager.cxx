// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexCallManager.hxx"
#include "QilexCallHandler.hxx"
#include "QilexLogger.hxx"
#include "QilexStatemachine.hxx"
#include "QilexSdp.hxx"
#include "QilexRTP.hxx"
#include "QilexIce.hxx"
#include "QilexConfig.hxx"
#include "QilexRegistrationManager.hxx"
#include <resip/stack/SipMessage.hxx>
#include <resip/dum/AppDialogSet.hxx>
#include <resip/dum/ServerInviteSession.hxx>
#include <resip/dum/ClientInviteSession.hxx>
#include "QilexSdp.hxx"
#include "os/OsTask.h"
#include "AudioMgr.h"
#include "QilexSdpSecureNull.hxx"
#include "QilexSdpSecureImpl.hxx"
#include <rutil/SharedPtr.hxx>
#include <algorithm>
#include <string>
#include <cassert>
#include <sstream>

using namespace std;

QilexCallManager* QilexCallManager::mInstance = 0;

QilexCallManager& QilexCallManager::instance() {
	if (!mInstance) {
		mInstance = new QilexCallManager();
		mInstance->dumLock();
		mInstance->resetHandler();
		mInstance->dumUnlock();
	}

	return *mInstance;
}

QilexCallManager::QilexCallManager() {
	mCallHandler = new QilexCallHandler();
	//mIce.reset(new QilexIce("77.79.238.232", QilexConfig::getTrunServerPort()));
}

QilexCallManager::~QilexCallManager() {
	delete mCallHandler;
}

void QilexCallManager::setPeerUri(const string& peerUri) {
	mPeerUri = peerUri;
}
void QilexCallManager::setOUT(bool out) {
	mOUT = out;
}

void QilexCallManager::resetHandler() {
	try {
		dum().setInviteSessionHandler(mCallHandler);
	} catch (...) {
		LOGI("QilexCallManager::resetHandler() except.");
	}
}

void QilexCallManager::sendInvite(bool warnTurnNotAvailable, bool bOutCall, const std::string& imsi) {
	DumLocker lock(this);

	try {
		SharedPtr<UserProfile> userProfile;
		LOGI("QilexCallManager::sendInvite()");
		userProfile = dum().getMasterUserProfile();

		// Add SIM IMSI to Header [Contact].
		userProfile->setImsi(Data(imsi));

		LOGI("QilexCallManager::sendInvite() 1 %s", imsi.c_str());
		// string defaultForm = "sip:";
		// defaultForm += username();
		// defaultForm += "@";
		// defaultForm += domain();
		// userProfile->setDefaultFrom(NameAddr(defaultForm.c_str()));

		rtp().prepareStreamTransport();

		LOGI("QilexCallManager::sendInvite() 1a");
		resip::SdpContents contents;
		bool bConnectedAsRelayed;
		QilexSdp::makeSdpOffer(contents, rtp().net(), false, false, false,
				false, rtp().secure(), bConnectedAsRelayed, bOutCall);
		if (warnTurnNotAvailable) {
			if (!bConnectedAsRelayed) {
				QilexStatemachine sm;
				sm.sendEvent(QilexStatemachine::EVENT_CALLTURNNOTAVAILABLE);
				LOGI("QilexCallManager::sendInvite() end");
				return;
			}
		}

		LOGI("QilexCallManager::sendInvite() 1c");
		mMessage = dum().makeInviteSession(resip::NameAddr(mPeerUri.c_str()),
				userProfile, &contents);

		LOGI("QILEX: %s",mPeerUri.c_str());
		dum().send(mMessage);
	} catch (std::exception& e) {
		std::stringstream es;
		es << e.what();
		LOGI("QilexCallManager::sendInvite() %s", es.str().c_str());
	} catch (...) {
		LOGI("QilexCallManager::sendInvite()");
	}

	//  mIce.reset(new QilexIce(domain(),TURN_SERVER_PORT));

}

void QilexCallManager::answerCall() {
	DumLocker lock(this);
	if (mServerInviteSessionHandle.isValid()) {
		mServerInviteSessionHandle->accept();
	} else {
		LOGI("QilexCallManager::answerCall() handle failure");
	}
}
void QilexCallManager::pauseCall() {

}

void QilexCallManager::resumeCall() {

}
void QilexCallManager::rejectCall() {
	DumLocker lock(this);
	if (mServerInviteSessionHandle.isValid()) {
		mServerInviteSessionHandle->reject(603);
		mServerInviteSessionHandle->end();
		resetServerInviteSessionHandle();
	}
	endOutgoingCall();
}

bool QilexCallManager::isActiveCall() {
	LOGI("SIGMA-PHUCPV QiLexCallManager isActiveCall");
	DumLocker lock(this);
	return isActiveIncommingCall() || isActiveOutgoingCall();
}

bool QilexCallManager::isActiveIncommingCall() {
	DumLocker lock(this);
	return mServerInviteSessionHandle.isValid();
}

bool QilexCallManager::isActiveOutgoingCall() {
	LOGI("SIGMA-PHUCPV QiLexCallManager isActiveOutgoingCall");
	DumLocker lock(this);

	for (set<resip::ClientInviteSessionHandle>::iterator iter =
			mClientInviteSessinHandles.begin();
			iter != mClientInviteSessinHandles.end(); ++iter) {
		if (iter->isValid())
			return true;
	}

	return false;
}

void QilexCallManager::initAudio() {
}

void QilexCallManager::startAudio() {
	SIPXLOGI("QilexCallManager::startAudio()");

	if (rtp().start()) {
		SIPXLOGI("QilexCallManager::startAudio() sucesss");
		QilexStatemachine sm;
		sm.sendEvent(QilexStatemachine::EVENT_CALLAUDIOSTARTED);
	}

	SIPXLOGI("/QilexCallManager::startAudio()");
}
float QilexCallManager::audioQuality() {
	return rtp().audioQuality();
}

void QilexCallManager::stopAudio() {
	rtp().terminate();
}

void QilexCallManager::setServerInviteSessionHandle(
		resip::ServerInviteSessionHandle handle) {
	//DumLocker lock(this);
	mServerInviteSessionHandle = handle;
}

resip::ServerInviteSessionHandle QilexCallManager::serverInviteSessionHandle() {
	//DumLocker lock(this);
	return mServerInviteSessionHandle;
}

void QilexCallManager::addClientInviteSessionHandle(
		resip::ClientInviteSessionHandle handle) {
	//DumLocker lock(this);
	mClientInviteSessinHandles.insert(handle);
}

void QilexCallManager::endOutgoingCall() {
	LOGI("SIGMA-PHUCPV QiLexCallManager endOutgoingCall");
	DumLocker lock(this);
	for (set<resip::ClientInviteSessionHandle>::iterator iter =
			mClientInviteSessinHandles.begin();
			iter != mClientInviteSessinHandles.end(); ++iter) {
		resip::ClientInviteSessionHandle h = *iter;

		if (h.isValid())
			h->end();

		if (h.isValid())
			h->getAppDialogSet()->end();
	}

	mClientInviteSessinHandles.clear();
}

void QilexCallManager::resetServerInviteSessionHandle() {
	//DumLocker lock(this);
	mServerInviteSessionHandle = resip::ServerInviteSessionHandle();
}

void QilexCallManager::updateAudioConfig(bool speakerOn, bool headphonesOn,
		bool muted) {
	SIPXLOGI(
			"QilexCallManager::updateAudioConfig(bool speakerOn=%d, bool headphonesOn=%d, bool muted=%d)",
			speakerOn, headphonesOn, muted);
	rtp().updateAudioConfig(speakerOn, headphonesOn, muted);
}

void QilexCallManager::updateAudioVolume(float volume) {
	SIPXLOGI("QilexCallManager::updateAudioVolume(float volume=%f)", volume);
	rtp().updateAudioVolume(volume);
}
