#include <boost/bind.hpp>

#include "AsyncUdpSocketBase.hxx"
#include "AsyncSocketBaseHandler.hxx"
#include <rutil/Logger.hxx>
#include "ReTurnSubsystem.hxx"

#ifdef _WIN32
#include <Qossp.h>
#elif defined(ANDROID)
#include <sys/types.h>
#include <sys/socket.h>
#endif

#define RESIPROCATE_SUBSYSTEM ReTurnSubsystem::RETURN

using namespace std;

namespace reTurn {

AsyncUdpSocketBase::AsyncUdpSocketBase(boost::asio::io_service& ioService) 
   : AsyncSocketBase(ioService),
	 mSocket(ioService),
     mResolver(ioService)
{
#ifdef _WIN32
	//QoS related

	LPWSAPROTOCOL_INFO pInstalledProtocols = NULL;
	LPWSAPROTOCOL_INFO pQosProtocol = NULL;

	DWORD nBufferSize;
	int nNumProtocols = WSAEnumProtocols(NULL, NULL, &nBufferSize);
	if((nNumProtocols != SOCKET_ERROR) && (WSAGetLastError() != WSAENOBUFS))
	{
		WarningLog(<<"WSAEnumProtocols() failed with error code: " << WSAGetLastError() << std::endl); 
	}
	else 
	{
		// Allocate a buffer to hold the list of protocol info structures
		pInstalledProtocols = (LPWSAPROTOCOL_INFO)malloc(nBufferSize);

		// Enumerate the protocols, find the QoS enabled one
		nNumProtocols = WSAEnumProtocols(NULL, pInstalledProtocols, &nBufferSize);
		if(nNumProtocols == SOCKET_ERROR)
		{
			WarningLog(<<"WSAEnumProtocols() failed with error code: " << WSAGetLastError() << std::endl); 
		}
		else 
		{
			pQosProtocol = pInstalledProtocols;

			for(int i=0; i<nNumProtocols; ++pQosProtocol, ++i)
			{
				if  ((pQosProtocol->dwServiceFlags1 & XP1_QOS_SUPPORTED) && 
					 (pQosProtocol->iSocketType == SOCK_DGRAM) &&
					 (pQosProtocol->iProtocol == IPPROTO_UDP) &&
					 (pQosProtocol->iAddressFamily == AF_INET))
				{
					break;
				}              
			}
		}
	}

	SOCKET sock = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, pQosProtocol, 0, WSA_FLAG_OVERLAPPED);
	if (sock != INVALID_SOCKET)
		mSocket.assign(boost::asio::ip::udp::v4(), sock);
	else
		WarningLog(<<"WSASocket() failed with error code: " << WSAGetLastError() << std::endl); 

	free(pInstalledProtocols);
#endif
}

AsyncUdpSocketBase::~AsyncUdpSocketBase() 
{
}

unsigned int 
AsyncUdpSocketBase::getSocketDescriptor() 
{ 
   return mSocket.native(); 
}

boost::system::error_code 
AsyncUdpSocketBase::bind(const boost::asio::ip::address& address, unsigned short port)
{
	boost::system::error_code errorCode;
	
	if (!mSocket.is_open())
		mSocket.open(address.is_v6() ? boost::asio::ip::udp::v6() : boost::asio::ip::udp::v4(), errorCode);
	
	if(!errorCode)
	{
		mSocket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
		mSocket.bind(boost::asio::ip::udp::endpoint(address, port), errorCode);
	}
	return errorCode;
}

void 
AsyncUdpSocketBase::connect(const std::string& address, unsigned short port)
{
   // Start an asynchronous resolve to translate the address
   // into a list of endpoints.
   resip::Data service(port);
   boost::asio::ip::udp::resolver::query query(address, service.c_str());   
   mResolver.async_resolve(query,
        boost::bind(&AsyncSocketBase::handleUdpResolve, shared_from_this(),
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::iterator));


   //QoS
#ifdef _WIN32
	SOCKET sock = mSocket.native();
	if (sock)
	{
		//Solution for older Windowses
		int nToS = 184; //Expedited Forwarding (EF) PHB
		int nRet = setsockopt(sock, IPPROTO_IP, IP_TOS, (char*)&nToS, sizeof(nToS));
		if (nRet == SOCKET_ERROR)
			WarningLog(<< "Setting ToS unsuccessful! Error code = " << WSAGetLastError() << std::endl);


		QOS qos = {0};
		qos.SendingFlowspec.DelayVariation = QOS_NOT_SPECIFIED;
		qos.SendingFlowspec.Latency = QOS_NOT_SPECIFIED;
		qos.SendingFlowspec.MaxSduSize = QOS_NOT_SPECIFIED;
		qos.SendingFlowspec.MinimumPolicedSize = QOS_NOT_SPECIFIED;
		qos.SendingFlowspec.PeakBandwidth = QOS_NOT_SPECIFIED;
		qos.SendingFlowspec.ServiceType = SERVICETYPE_GUARANTEED | SERVICE_NO_QOS_SIGNALING;
		qos.SendingFlowspec.TokenBucketSize = QOS_NOT_SPECIFIED;
		qos.SendingFlowspec.TokenRate = 50000;
		qos.ReceivingFlowspec.DelayVariation = QOS_NOT_SPECIFIED;
		qos.ReceivingFlowspec.Latency = QOS_NOT_SPECIFIED;
		qos.ReceivingFlowspec.MaxSduSize = QOS_NOT_SPECIFIED;
		qos.ReceivingFlowspec.MinimumPolicedSize = QOS_NOT_SPECIFIED;
		qos.ReceivingFlowspec.PeakBandwidth = QOS_NOT_SPECIFIED;
		qos.ReceivingFlowspec.ServiceType = SERVICETYPE_GUARANTEED | SERVICE_NO_QOS_SIGNALING;
		qos.ReceivingFlowspec.TokenBucketSize = QOS_NOT_SPECIFIED;
		qos.ReceivingFlowspec.TokenRate = QOS_NOT_SPECIFIED;

		DWORD nBytesRet;
		if(WSAIoctl(sock, SIO_SET_QOS, &qos, sizeof(qos), NULL, NULL, &nBytesRet, NULL, NULL) == SOCKET_ERROR)
		{
			int nError = WSAGetLastError();
			WarningLog(<< "Setting QoS failed! Error code: " << nError << "\n");
		}
	}
	else
		WarningLog(<< "Invalid socket\n");
#elif defined(ANDROID)
	int sock = mSocket.native();
	if (sock)
	{
		int nToS = 184; //Expedited Forwarding (EF) PHB
		int nRet = setsockopt(sock, IPPROTO_IP, IP_TOS, (char*)&nToS, sizeof(nToS));
		if (nRet == SOCKET_ERROR)
			WarningLog(<< "Setting ToS unsuccessful! Error code = " << errno << std::endl);

		int nPriority = 7;
		nRet = setsockopt(sock, SOL_SOCKET, SO_PRIORITY, &nPriority, sizeof(nPriority));
		if (nRet == SOCKET_ERROR)
		{
			nPriority = 6; //for higher priorities extra privileges may be needed
			nRet = setsockopt(sock, SOL_SOCKET, SO_PRIORITY, &nPriority, sizeof(nPriority));
			if (nRet == SOCKET_ERROR)
				WarningLog(<< "Setting socket priority unsuccessful! Error code = " << errno << std::endl);
		}
	}
#endif
}

void 
AsyncUdpSocketBase::handleUdpResolve(const boost::system::error_code& ec,
                                     boost::asio::ip::udp::resolver::iterator endpoint_iterator)
{
   if (!ec)
   {
      // Use the first endpoint in the list. 
      // Nothing to do for UDP except store the connected address/port
      mConnected = true;
      mConnectedAddress = endpoint_iterator->endpoint().address();
      mConnectedPort = endpoint_iterator->endpoint().port();

      onConnectSuccess();
   }
   else
   {
      onConnectFailure(ec);
   }
}

const boost::asio::ip::address 
AsyncUdpSocketBase::getSenderEndpointAddress() 
{ 
   return mSenderEndpoint.address(); 
}

unsigned short 
AsyncUdpSocketBase::getSenderEndpointPort() 
{ 
   return mSenderEndpoint.port(); 
}

void 
AsyncUdpSocketBase::transportSend(const StunTuple& destination, std::vector<boost::asio::const_buffer>& buffers)
{
   //InfoLog(<< "AsyncUdpSocketBase::transportSend " << buffers.size() << " buffer(s) to " << destination << " - buf1 size=" << buffer_size(buffers.front()));
   mSocket.async_send_to(buffers, 
                         boost::asio::ip::udp::endpoint(destination.getAddress(), destination.getPort()), 
                         boost::bind(&AsyncUdpSocketBase::handleSend, shared_from_this(), boost::asio::placeholders::error));
}

void 
AsyncUdpSocketBase::transportReceive()
{
   mSocket.async_receive_from(boost::asio::buffer((void*)mReceiveBuffer->data(), RECEIVE_BUFFER_SIZE), mSenderEndpoint,
               boost::bind(&AsyncUdpSocketBase::handleReceive, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void 
AsyncUdpSocketBase::transportFramedReceive()
{
   // For UDP these two functions are the same
   transportReceive();
}

void 
AsyncUdpSocketBase::transportClose()
{
   mSocket.close();
}

}


/* ====================================================================

 Copyright (c) 2007-2008, Plantronics, Inc.
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are 
 met:

 1. Redistributions of source code must retain the above copyright 
    notice, this list of conditions and the following disclaimer. 

 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution. 

 3. Neither the name of Plantronics nor the names of its contributors 
    may be used to endorse or promote products derived from this 
    software without specific prior written permission. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ==================================================================== */

