#ifdef USE_SSL
#include "TurnAsyncTlsSocket.hxx"
#include "rutil/Lock.hxx"
#include <boost/bind.hpp>

#define RESIPROCATE_SUBSYSTEM ReTurnSubsystem::RETURN

using namespace std;
using namespace resip;

namespace reTurn {

#pragma warning(push)
#pragma warning(disable:4355)

TurnAsyncTlsSocket::TurnAsyncTlsSocket(boost::asio::io_service& ioService,
                                       boost::asio::ssl::context& sslContext,
                                       bool validateServerCertificateHostname,
                                       TurnAsyncSocketHandler* turnAsyncSocketHandler,
                                       const boost::asio::ip::address& address, 
                                       unsigned short port) : 
   TurnAsyncSocket(ioService, *this, turnAsyncSocketHandler, address, port),
   AsyncTlsSocketBase(ioService, sslContext, validateServerCertificateHostname)
{
	mLocalBinding.setTransportType(StunTuple::TLS);

	bind(address, port);
}

#pragma warning(pop)
 
void 
TurnAsyncTlsSocket::onConnectSuccess()
{
	Lock lock(mTurnHandlerMutex);
	if(mTurnAsyncSocketHandler) mTurnAsyncSocketHandler->onConnectSuccess(getSocketDescriptor(), mConnectedAddress, mConnectedPort);
	turnReceive();
}

void 
TurnAsyncTlsSocket::onConnectFailure(const boost::system::error_code& e)
{
	Lock lock(mTurnHandlerMutex);
	if(mTurnAsyncSocketHandler) mTurnAsyncSocketHandler->onConnectFailure(getSocketDescriptor(), e);
}

void 
TurnAsyncTlsSocket::onReceiveSuccess(const boost::asio::ip::address& address, unsigned short port, boost::shared_ptr<DataBuffer>& data)
{    
	Lock lock(mTurnHandlerMutex);
	handleReceivedData(address, port, data);
	turnReceive();
}

void 
TurnAsyncTlsSocket::onReceiveFailure(const boost::system::error_code& e)
{
	Lock lock(mTurnHandlerMutex);
	if(mTurnAsyncSocketHandler) mTurnAsyncSocketHandler->onReceiveFailure(getSocketDescriptor(), e);
}
 
void 
TurnAsyncTlsSocket::onSendSuccess()
{
	Lock lock(mTurnHandlerMutex);
	if(mTurnAsyncSocketHandler) mTurnAsyncSocketHandler->onSendSuccess(getSocketDescriptor());
}
 
void 
TurnAsyncTlsSocket::onSendFailure(const boost::system::error_code& e)
{
	Lock lock(mTurnHandlerMutex);
	if(mTurnAsyncSocketHandler) mTurnAsyncSocketHandler->onSendFailure(getSocketDescriptor(), e);
}

} // namespace
#endif

/* ====================================================================

 Copyright (c) 2007-2008, Plantronics, Inc.
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are 
 met:

 1. Redistributions of source code must retain the above copyright 
    notice, this list of conditions and the following disclaimer. 

 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution. 

 3. Neither the name of Plantronics nor the names of its contributors 
    may be used to endorse or promote products derived from this 
    software without specific prior written permission. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ==================================================================== */
