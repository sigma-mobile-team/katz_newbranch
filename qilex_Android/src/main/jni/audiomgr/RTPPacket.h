/*********************************/
/* RTPPcket.h                    */
/* Lukasz Rychter 2012/01/12     */
/*							     */
/* RTPPacket wrapper class       */
/*********************************/

#ifndef __RTP_PACKET_H__
#define __RTP_PACKET_H__

#include <boost/shared_ptr.hpp>

bool IsSeqEarlier(unsigned short nSeqLeft, unsigned short nSeqRight);

class RTPPacket
{
public:
	RTPPacket(unsigned short nSeq=0);
	RTPPacket(const void* pData, unsigned short nSize, unsigned char nFECdistance=0);

	virtual ~RTPPacket();

	bool isValid()
	{
		return m_bValid;
	}

	unsigned short GetSeq()
	{
		return m_nSeq;
	}

	unsigned long GetTimestamp()
	{
		return m_nTimestamp;
	}

	unsigned short GetDataSize()
	{
		return m_nDataSize;
	}

	const void* GetDataPtr()
	{
		return m_pData;
	}

	unsigned char GetFECdistance()
	{
		return m_nFECdistance;
	}

	bool IsFEC()
	{
		return m_nFECdistance > 0;
	}

	bool IsMarked()
	{
		return (m_nMPT & 0x80) > 0;
	}

	bool operator < (const RTPPacket& right)
	{
		return IsSeqEarlier(m_nSeq, right.m_nSeq);
	}

	bool operator < (const boost::shared_ptr<RTPPacket>& right)
	{
		return IsSeqEarlier(m_nSeq, right->m_nSeq);
	}
	
protected:
	unsigned char	m_nVPXCC;		// Version, Padding, Extension and CSRC Count bits.
	unsigned char	m_nMPT;			// Marker and Payload Type bits.
	unsigned short	m_nSeq;			// Sequence Number (saved as Little Endian!)
	unsigned long	m_nTimestamp;	// Timestamp (saved as Little Endian!)
	unsigned long	m_nSSRC;		// SSRC (saved as Little Endian!)

	unsigned short	m_nDataSize;	// Payload size
	unsigned char*  m_pData;		// Payload

	unsigned char	m_nFECdistance;

	bool m_bValid;
};

#pragma pack()

struct SmartRTPPacketComparer : std::binary_function<boost::shared_ptr<RTPPacket>,boost::shared_ptr<RTPPacket>,bool>
{
	bool operator() (const boost::shared_ptr<RTPPacket>& left, const boost::shared_ptr<RTPPacket>& right) const
	{
		return left->operator <(right);
	}
};

#endif
