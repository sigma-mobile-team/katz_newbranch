// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexAndroidAudioWrapper.h"
#include "../resiprocatejni/QilexLogger.hxx"
#include "../resiprocatejni/QilexConfig.hxx"

extern JavaVM* qilex_jvm;

QilexAndroidAudioWrapper* QilexAndroidAudioWrapper::instance = NULL;

QilexAndroidAudioWrapper::QilexAndroidAudioWrapper()
{
	//audiotrack
	audiotrack = 0;
	audiotrack_class = 0;
	audiotrack_write_b = 0;
	audiotrack_write_sh = 0;
	audiotrack_getPlaybackHeadPosition_m = 0;
	audiotrack_minBufSize = 0;
	audiotrack_bufSize = 0;
	audiotrack_nativeSamplingRate = 0;
	audiotrack_play_m = 0;

	//audiorecord
	audiorecord = 0;
	audiorecord_class = 0;
	audiorecord_read_b = 0;
	audiorecord_read_sh = 0;
	audiorecord_minBufSize = 0;
	audiorecord_bufSize = 0;
	audiorecord_nativeSamplingRate = 0;

	//audio effects
	aec_object = 0;
	aec_class = 0;

	//device params
	m_bWiFi = false;
	m_bMobile3G = false;
	m_bMobile2G = false;
	m_bLTE = false;

	m_fDevicePerformance = 1.0;
	m_bLowLatencyAudio = false;

	m_nAndroidVersion = 0;
}

QilexAndroidAudioWrapper* QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper()
{
	if(!instance)
	{
		instance = new QilexAndroidAudioWrapper();
	}
	return instance;
}

int QilexAndroidAudioWrapper::attachJVM(JNIEnv **env)
{
	JNIEnv *globalenv;
	int envstat = qilex_jvm->GetEnv((void**)&globalenv, JNI_VERSION_1_6);
	
	jint attachres = qilex_jvm->AttachCurrentThread(env, NULL);

	if(attachres<0)
		SIPXLOGI("QilexAndroidAudioWrapper::attachJVM");
	else
	{
		*env = NULL;
		SIPXLOGE("QilexAndroidAudioWrapper::attachJVM FAIL");
	}

	return envstat;
 
}

void QilexAndroidAudioWrapper::detachJVM(int stat)
{
  
	//if(stat==JNI_EDETACHED)
	{
	  //	qilex_jvm->DetachCurrentThread();
	  //	SIPXLOGI("QilexAndroidAudioWrapper::detachJVM");
	}
	stat = 0;
}

void QilexAndroidAudioWrapper::createAudioTrackObject(JNIEnv *env)
{
	audiotrack_class = (jclass)env->NewGlobalRef(env->FindClass("android/media/AudioTrack"));
	if(audiotrack_class == 0) SIPXLOGE("Java stubs : audio track class not pos");

	int audioStreamType = 0 /*STREAM_VOICE_CALL*/;

	jmethodID getnativeoutputsamplerate_m = env->GetStaticMethodID(audiotrack_class, "getNativeOutputSampleRate", "(I)I");
	audiotrack_nativeSamplingRate = env->CallStaticIntMethod(audiotrack_class, getnativeoutputsamplerate_m, audioStreamType, 4, 2); //QilexConfig::getSamplesPerSecond();

	int native_samples_per_frame = audiotrack_nativeSamplingRate / 50;

	jmethodID getminbuffersize_m = env->GetStaticMethodID(audiotrack_class, "getMinBufferSize", "(III)I");
	audiotrack_minBufSize = env->CallStaticIntMethod(audiotrack_class, getminbuffersize_m, audiotrack_nativeSamplingRate, 4, 2);

	const int MIN_BUFFER_FRAMES = 4; //20ms each
	audiotrack_bufSize = audiotrack_minBufSize;
	if (audiotrack_bufSize < native_samples_per_frame*MIN_BUFFER_FRAMES*2) //minimum MIN_BUFFER_FRAMES frames, 2 bytes each
		audiotrack_bufSize = native_samples_per_frame*MIN_BUFFER_FRAMES*2;

	SIPXLOGI("QilexAndroidAudioWrapper::createAudioTrackObject minBufSize=%d(%dms), bufSize=%d(%dms), samplingRate=%d",
				audiotrack_minBufSize, audiotrack_minBufSize*10/native_samples_per_frame, audiotrack_bufSize, audiotrack_bufSize*10/native_samples_per_frame, audiotrack_nativeSamplingRate);

	jmethodID constructor = env->GetMethodID(audiotrack_class, "<init>", "(IIIIII)V");

	audiotrack = env->NewObject(audiotrack_class, constructor, audioStreamType, audiotrack_nativeSamplingRate, 4, 2, audiotrack_bufSize, 1);
	audiotrack = env->NewGlobalRef(audiotrack);

	SIPXLOGI("QilexAndroidAudioWrapper::createAudioTrackObject audiotrack %x", audiotrack);
}

void QilexAndroidAudioWrapper::deleteAudioTrackObject(JNIEnv *env)
{
	env->DeleteGlobalRef(audiotrack);
	env->DeleteGlobalRef(audiotrack_class);

	audiotrack = 0;
	audiotrack_class = 0;
}

void QilexAndroidAudioWrapper::enableInternalAEC(JNIEnv *env)
{
	if (m_nAndroidVersion < 16)
		return; // AcousticEchoCanceler is available from API 16

	aec_class = (jclass)env->NewGlobalRef(env->FindClass("android/media/audiofx/AcousticEchoCanceler"));
	if(aec_class == 0) SIPXLOGE("Cannot get reference to AcousticEchoCanceler class");
	else
	{
		jmethodID getaudiosessionid_m = env->GetMethodID(audiorecord_class, "getAudioSessionId", "()I");
		if(getaudiosessionid_m == 0) SIPXLOGE("Cannot get reference to AudioTrack.getAudioSessionId() method");
		else
		{
			int audio_session_id = env->CallIntMethod(audiorecord, getaudiosessionid_m);

			jmethodID aeccreate_m = env->GetStaticMethodID(aec_class, "create", "(I)Landroid/media/audiofx/AcousticEchoCanceler;");
			if(aeccreate_m == 0) SIPXLOGE("Cannot get reference to AcousticEchoCanceler.create() method");
			else
			{
				aec_object = env->CallStaticObjectMethod(aec_class, aeccreate_m, audio_session_id);
				if(aec_object == 0) SIPXLOGI("Internal AEC not available");
				else
				{
					aec_object = env->NewGlobalRef(aec_object);

					jmethodID setaecenabled_m = env->GetMethodID(aec_class, "setEnabled", "(Z)I");
					if(setaecenabled_m == 0) SIPXLOGE("Cannot get reference to AcousticEchoCanceler.setEnabled() method");
					else
					{
						int status = env->CallIntMethod(aec_object, setaecenabled_m, true);
						if (status != 0)
							SIPXLOGE("Failed to enable internal AEC");
						else
							SIPXLOGI("INTERNAL AEC ENABLED");
					}
				}
			}
		}
	}
}

void QilexAndroidAudioWrapper::releaseInternalAEC(JNIEnv *env)
{
	if (aec_object)
	{
		jmethodID release_m = env->GetMethodID(aec_class, "release", "()V");
		if (release_m == 0) SIPXLOGE("Cannot get reference to AcousticEchoCanceler.release() method");
		else
			env->CallVoidMethod(aec_object, release_m);

		env->DeleteGlobalRef(aec_object);
		aec_object = 0;
		aec_class = 0;
	}
}

void QilexAndroidAudioWrapper::createAudioRecordObject(JNIEnv *env)
{
	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject START");
	audiorecord_class = (jclass)env->NewGlobalRef(env->FindClass("android/media/AudioRecord"));
	if(audiorecord_class == 0) SIPXLOGE("Java stubs : audio track class not pos");

	audiorecord_nativeSamplingRate = QilexConfig::getSamplesPerSecond();

	jmethodID getminbuffersize_m = env->GetStaticMethodID(audiorecord_class, "getMinBufferSize", "(III)I");
	audiorecord_minBufSize = env->CallStaticIntMethod(audiorecord_class, getminbuffersize_m, audiorecord_nativeSamplingRate, 16, 2);

	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  1");
	if (audiorecord_minBufSize == -2)
	{
		//sampling rate not supported
		SIPXLOGE("QilexAndroidAudioWrapper::createAudioRecordObject sampling rate %d not supported. Trying 44100", audiorecord_nativeSamplingRate);
		audiorecord_nativeSamplingRate = 44100;
		audiorecord_minBufSize = env->CallStaticIntMethod(audiorecord_class, getminbuffersize_m, audiorecord_nativeSamplingRate, 16, 2);
	}

	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  2");
	const int MIN_BUFFER_FRAMES = 4; //20ms each
	audiorecord_bufSize = audiorecord_minBufSize;
	if (audiorecord_bufSize < audiorecord_nativeSamplingRate/50*MIN_BUFFER_FRAMES*2) //minimum MIN_BUFFER_FRAMES frames, 2 bytes each
		audiorecord_bufSize = audiorecord_nativeSamplingRate/50*MIN_BUFFER_FRAMES*2;

	jmethodID constructor = env->GetMethodID(audiorecord_class, "<init>", "(IIIII)V");

	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject minBufSize=%d(%dms), bufSize=%d(%dms), samplingRate=%d",
			audiorecord_minBufSize, audiorecord_minBufSize*10/QilexConfig::getSamplesPerFrame(), audiorecord_bufSize, audiorecord_bufSize*10/QilexConfig::getSamplesPerFrame(), audiorecord_nativeSamplingRate);

	int audioSource = 1; //MIC
	if (m_nAndroidVersion >= 11)
		audioSource = 7; //VOICE_COMMUNICATION - needed for some newer devices like Xperia Z / Xperia SP, buggy on Huawei Ascend P1

	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  2.1");
	audiorecord = env->NewObject(
			audiorecord_class,
			constructor,
			audioSource,
			audiorecord_nativeSamplingRate,
			16, 2,
			audiorecord_bufSize);
	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  2.2");
	if (env->ExceptionCheck())
	{
		SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  3");
		env->ExceptionClear();

		if (audiorecord_nativeSamplingRate != 44100)
		{
			SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  4");
			audiorecord_nativeSamplingRate = 44100;

			audiorecord_minBufSize = env->CallStaticIntMethod(audiorecord_class, getminbuffersize_m, audiorecord_nativeSamplingRate, 16, 2);

			audiorecord_bufSize = audiorecord_minBufSize;
			if (audiorecord_bufSize < audiorecord_nativeSamplingRate/50*MIN_BUFFER_FRAMES*2) //minimum MIN_BUFFER_FRAMES frames, 2 bytes each
				audiorecord_bufSize = audiorecord_nativeSamplingRate/50*MIN_BUFFER_FRAMES*2;

			SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject minBufSize=%d(%dms), bufSize=%d(%dms), samplingRate=%d",
					audiorecord_minBufSize, audiorecord_minBufSize*10/QilexConfig::getSamplesPerFrame(), audiorecord_bufSize, audiorecord_bufSize*10/QilexConfig::getSamplesPerFrame(), audiorecord_nativeSamplingRate);

			audiorecord = env->NewObject(audiorecord_class, constructor, audioSource, audiorecord_nativeSamplingRate, 16, 2, audiorecord_bufSize);
			SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject STEP  5");
			if (env->ExceptionCheck())
				env->ExceptionClear();

		}
	}

	if (audiorecord == 0)
	{
		SIPXLOGE("QilexAndroidAudioWrapper::createAudioRecordObject FAILED");
		return;
	}

	audiorecord = env->NewGlobalRef(audiorecord);
	SIPXLOGI("QilexAndroidAudioWrapper::createAudioRecordObject END");

	enableInternalAEC(env);
}

void QilexAndroidAudioWrapper::deleteAudioRecordObject(JNIEnv *env)
{
	releaseInternalAEC(env);

	env->DeleteGlobalRef(audiorecord);
	env->DeleteGlobalRef(audiorecord_class);

	audiorecord = 0;
	audiorecord_class = 0;
}

void QilexAndroidAudioWrapper::createAudioTrackMethodsStubs(JNIEnv *env)
{
	SIPXLOGI("Java stubs createAudioTrackMethodsStubs begin %x %x", env, audiotrack_class);

	audiotrack_play_m = env->GetMethodID(audiotrack_class, "play", "()V");
	audiotrack_stop_m = env->GetMethodID(audiotrack_class, "stop", "()V");
	audiotrack_write_b = env->GetMethodID(audiotrack_class, "write", "([BII)I");
	audiotrack_write_sh = env->GetMethodID(audiotrack_class, "write", "([SII)I");
	audiotrack_getPlaybackHeadPosition_m = env->GetMethodID(audiotrack_class, "getPlaybackHeadPosition", "()I");
	audiotrack_release_m = env->GetMethodID(audiotrack_class, "release", "()V");

	SIPXLOGI("Java stubs createAudioTrackMethodsStubs end");
}

void QilexAndroidAudioWrapper::createAudioRecordMethodsStubs(JNIEnv *env)
{
	SIPXLOGI("Java stubs createAudioRecordMethodsStubs begin");

	audiorecord_read_b = env->GetMethodID(audiorecord_class, "read", "([BII)I");
	audiorecord_read_sh = env->GetMethodID(audiorecord_class, "read", "([SII)I");
	audiorecord_startRecording_m = env->GetMethodID(audiorecord_class, "startRecording", "()V");
	audiorecord_stop_m = env->GetMethodID(audiorecord_class, "stop", "()V");
	audiorecord_release_m = env->GetMethodID(audiorecord_class, "release", "()V");

	SIPXLOGI("audiorecord_stop_m = %d", audiorecord_stop_m);

	SIPXLOGI("Java stubs createAudioRecordMethodsStubs end");
}

void QilexAndroidAudioWrapper::audiotrack_play(JNIEnv *env)
{
	env->CallVoidMethod(audiotrack, audiotrack_play_m);
}

void QilexAndroidAudioWrapper::audiotrack_stop(JNIEnv *env)
{
	SIPXLOGI("QilexAndroidAudioWrapper::audiotrack_stop");
	env->CallVoidMethod(audiotrack, audiotrack_stop_m);
	SIPXLOGI("QilexAndroidAudioWrapper::audiotrack_stop end");
}

int QilexAndroidAudioWrapper::audiotrack_write(JNIEnv *env, jbyteArray audio_buffer, int offset, int size)
{
	return env->CallIntMethod(audiotrack, audiotrack_write_b, audio_buffer, offset, size);
}

int QilexAndroidAudioWrapper::audiotrack_write(JNIEnv *env, jshortArray audio_buffer, int offset, int size)
{
	return env->CallIntMethod(audiotrack, audiotrack_write_sh, audio_buffer, offset, size);
}

int QilexAndroidAudioWrapper::audiotrack_getPlaybackHeadPosition(JNIEnv *env)
{
	return env->CallIntMethod(audiotrack, audiotrack_getPlaybackHeadPosition_m);
}


void QilexAndroidAudioWrapper::audiotrack_release(JNIEnv *env)
{
	env->CallVoidMethod(audiotrack, audiotrack_release_m);
}

int QilexAndroidAudioWrapper::audiorecord_read(JNIEnv *env, jbyteArray audio_buffer, int offset, int size)
{
	int res = env->CallIntMethod(audiorecord, audiorecord_read_b, audio_buffer, offset, size);
	return res;
}

int QilexAndroidAudioWrapper::audiorecord_read(JNIEnv *env, jshortArray audio_buffer, int offset, int size)
{
	int res = env->CallIntMethod(audiorecord, audiorecord_read_sh, audio_buffer, offset, size);
	return res;
}

void QilexAndroidAudioWrapper::audiorecord_release(JNIEnv *env)
{
	env->CallVoidMethod(audiorecord, audiorecord_release_m);
}

void QilexAndroidAudioWrapper::audiorecord_startRecording(JNIEnv *env)
{
	env->CallVoidMethod(audiorecord, audiorecord_startRecording_m);
}


void QilexAndroidAudioWrapper::audiorecord_stop(JNIEnv *env)
{
	env->CallVoidMethod(audiorecord, audiorecord_stop_m);
}

void QilexAndroidAudioWrapper::setAudioPriority(JNIEnv *env)
{
	jclass process_class = env->FindClass("android/os/Process");

	if(process_class==0)
		LOGE("process class not found");

	jmethodID setThreadPriority_m = env->GetStaticMethodID(process_class, "setThreadPriority", "(I)V");
	if(!setThreadPriority_m) 
	{
		LOGE("setThreadPriority met. not found");
	}
	else
	{
		int prior = -20;
		do
		{
			env->CallStaticVoidMethod(process_class, setThreadPriority_m, prior);
			if (env->ExceptionCheck())
			{
				env->ExceptionClear();
				LOGE("QilexAndroidAudioWrapper::setThreadPriority: setting priority %d failed", prior);
				++prior;
			}
			else
				break;
		}
		while (prior < 0);
	}
}

int QilexAndroidAudioWrapper::getDeviceLatencyMs()
{
	//THIS EQUATION MAY NOT BE CORRECT FOR ALL DEVICES!
	//Heurystics for AudioTrack. Would not be valid if we switch to OpenSL, which could decrease latencies.

	//Typically there are 4 internal driver buffers, 1024 samples each. They work on native device's sampling rate (48k on most devices, but may be 44.1k on older ones. For now, there is no API to check that)
	int nDriverLatency = 87; //4*1024/48.000

	if (m_bLowLatencyAudio) //fast audio path implemented
		nDriverLatency = 10; //CHECK ME! device driver specific. There is no way to check that. Just giving typical value

	//There are 3 main latency parts. Driver output latency, Android latency (which seems to be equal audiotrack_minBufSize) and AudioTrack buffer latency
	int nLatency = nDriverLatency + (audiotrack_minBufSize + audiotrack_bufSize) * 500 / audiotrack_nativeSamplingRate;

	if (audiorecord_minBufSize * 500 / audiorecord_nativeSamplingRate > 200) // if audiorecord_minBufSize > 200ms
		nLatency += 50; //ugly heuristic. "Usually should be helpful" strength

	if (m_nAndroidVersion < 21)
		nLatency += 80; //empirical, heuristic. Add 80ms for additional delays in audio path. Since Android 5 audio path is optimized.

	return nLatency;
}

void QilexAndroidAudioWrapper::setAvailableConnectivity(bool bWiFi, bool bMobile3G, bool bMobile2G, bool bLTE)
{
	if (bWiFi != m_bWiFi)
	{
		SIPXLOGI("QilexAndroidAudioWrapper::setAvailableConnectivity: WiFi:%d", bWiFi?1:0);
		m_bWiFi = bWiFi;
	}

	if (bMobile3G != m_bMobile3G)
	{
		SIPXLOGI("QilexAndroidAudioWrapper::setAvailableConnectivity: Mobile3G:%d", bMobile3G?1:0);
		m_bMobile3G = bMobile3G;
	}

	if (bMobile2G != m_bMobile2G)
	{
		SIPXLOGI("QilexAndroidAudioWrapper::setAvailableConnectivity: Mobile2G:%d", bMobile2G?1:0);
		m_bMobile2G = bMobile2G;
	}

	if (bLTE != m_bLTE)
	{
		SIPXLOGI("QilexAndroidAudioWrapper::setAvailableConnectivity: LTE:%d", bLTE?1:0);
		m_bLTE = bLTE;
	}
}
