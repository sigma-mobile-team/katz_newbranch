// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "MpidQilexAndroid.h"
#include <mp/MpInputDeviceManager.h>
#include "../resiprocatejni/QilexLogger.hxx"
#include "../resiprocatejni/QilexConfig.hxx"
#include <math.h>

MpAudioSample  temp[8000];

//MpAudioSample mic_dummy_audio[QilexConfig:];

MpidQilexAndroid::MpidQilexAndroid(const UtlString& name, MpInputDeviceManager& deviceManager)
	: MpInputDeviceDriver(name, deviceManager)
{
	//fillBuffer(mic_dummy_audio);
}

MpidQilexAndroid::~MpidQilexAndroid()
{
}

OsStatus MpidQilexAndroid::enableDevice(unsigned samplesPerFrame,
                                  unsigned samplesPerSec,
                                  MpFrameTime currentFrameTime)
{

 //	SIPXLOGI("MpidQilexAndroid::enableDevice");


	mSamplesPerFrame = samplesPerFrame;
	mSamplesPerSec = samplesPerSec;
	mCurrentFrameTime = currentFrameTime;

	mIsEnabled = true;

	return OS_SUCCESS;
}

OsStatus MpidQilexAndroid::disableDevice()
{
	SIPXLOGI("MpidQilexAndroid::disableDevice");
	mIsEnabled = false;
	return OS_SUCCESS;
}

UtlBoolean MpidQilexAndroid::isDeviceValid()
{
	return true;
}

void MpidQilexAndroid::audioRecord_read()
{
}

OsStatus MpidQilexAndroid::pushFrame(MpInputDeviceHandle deviceId,
        unsigned numSamples,
        MpAudioSample *samples,
        MpFrameTime frameTime)
{
	if(!mpInputDeviceManager)
	{
	  	LOGE("MpidQilexAndroid::pushFrame no mpInputDeviceManager");
		return OS_FAILED;
	}


	if(!isEnabled())
	{
	  	LOGE("MpidQilexAndroid::pushFrame not enabled");
		return OS_FAILED;
	}

	mpInputDeviceManager->pushFrame(mDeviceId, numSamples, samples, mCurrentFrameTime);
	mCurrentFrameTime += (numSamples*1000)/mSamplesPerSec;

	return OS_SUCCESS;
}

OsStatus MpidQilexAndroid::pushFrame(MpAudioSample *samples)
{
	if(!mpInputDeviceManager)
	{
		LOGE("MpidQilexAndroid::pushFrame no mpInputDeviceManager");
		return OS_FAILED;
	}

	if(!isEnabled())
	{
		LOGE("MpidQilexAndroid::pushFrame not enabled");
		return OS_FAILED;
	}

	mpInputDeviceManager->pushFrame(mDeviceId, mSamplesPerFrame, temp, mCurrentFrameTime);
	mCurrentFrameTime += (mSamplesPerFrame*1000)/mSamplesPerSec;

	return OS_SUCCESS;
}

void MpidQilexAndroid::dummySend()
{
	if(isEnabled())
	{
		//mpInputDeviceManager->pushFrame(mDeviceId, mSamplesPerFrame, mic_dummy_audio, mCurrentFrameTime);
		mCurrentFrameTime += (mSamplesPerFrame*1000)/mSamplesPerSec;
	}
}
