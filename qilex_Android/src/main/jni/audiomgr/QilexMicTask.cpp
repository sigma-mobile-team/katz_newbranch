// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexMicTask.h"
#include "MpidQilexAndroid.h"
#include "QilexAndroidAudioWrapper.h"
#include "QualityStats.h"
#include "../resiprocatejni/QilexConfig.hxx"
#include "../resiprocatejni/QilexLogger.hxx"
#include "../resiprocate/rutil/Lock.hxx"

extern JavaVM* qilex_jvm;

QilexMicTask::QilexMicTask(const UtlString& name, void* pArg,
		const int priority, const int options, const int stackSize) :
		OsTask(name, pArg, priority, options, stackSize) {
	idQilexAndroid = NULL;
	working = false;
}

int QilexMicTask::run(void* pArg) {
	SIPXLOGI("SIGMA-PHUCPV / Class QilexMicTask / method run start");
	JNIEnv *env;
	QilexAndroidAudioWrapper* audioWrapper =
			QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();

	JNIEnv *globalenv;
	int envstat = qilex_jvm->GetEnv((void**) &globalenv, JNI_VERSION_1_6);
	bool isLl = (audioWrapper->getAndroidVersion() >= 21);
	jint attachres = qilex_jvm->AttachCurrentThread(&env, NULL);

	if (attachres < 0)
		SIPXLOGE("QilexSpkrTask::run env corrputed");

	//SIPXLOGI("QilexMicTask:: thread mic przed createAudioRecordMethodsStubs");
	audioWrapper->createAudioRecordMethodsStubs(env);

	jshort* nat_buffer;
	jshortArray inputBuffer;

	unsigned short destSamplingRate = QilexConfig::getSamplesPerSecond();
	unsigned short nativeSamplingRate =
			audioWrapper->getAudioRecordNativeSamplingRate();
	unsigned short destFrameSize = destSamplingRate / 50;
	unsigned short nativeFrameSize = nativeSamplingRate / 50;

	if (destFrameSize > nativeFrameSize)
		SIPXLOGE(
				"QilexMicTask: destSamplingRate > nativeFrameSize. Upsampling not supported!");

	//SIPXLOGI("QilexMicTask:: thread mic przed NewShortArray");
	inputBuffer = env->NewShortArray(nativeFrameSize);
	if (inputBuffer == 0)
		SIPXLOGE("QilexMicTask:: output buff not cretated");

	nat_buffer = env->GetShortArrayElements(inputBuffer, 0);
//	VM_JNIGenericHelpers.setBoolStar(isCopyAddress, true);

	audioWrapper->setAudioPriority(env);
	audioWrapper->audiorecord_startRecording(env);

	bool running;
	{
		resip::Lock lock(mutex);
		running = !streamDone;
	}
	try {
		while (running) {
			{
				resip::Lock lock(mutex);
				running = !streamDone;
				working = true;
			}
			if (idQilexAndroid->isEnabled()) {
				audioWrapper->audiorecord_read(env, inputBuffer, 0,
						nativeFrameSize);
				if (isLl) // maybe avoid memory leak with machine Ll  Dalvik runtime.
				{
					nat_buffer = env->GetShortArrayElements(inputBuffer, 0);
				}
				if (destFrameSize < nativeFrameSize) {
				for (int i = 0; i < destFrameSize; ++i) {
					int dest_index = i * nativeFrameSize / destFrameSize;
					nat_buffer[i] = nat_buffer[dest_index];
				}
				}
				idQilexAndroid->pushFrame(1, destFrameSize, nat_buffer, 0);
			} else {
				SIPXLOGE("MIC DISABLED !!!!!!!!!");
				OsTask::delay(20);
			}
		}
		audioWrapper->audiorecord_stop(env);
		audioWrapper->audiorecord_release(env);
		audioWrapper->deleteAudioRecordObject(env);
		env->ReleaseShortArrayElements(inputBuffer, nat_buffer, 0);
		env->DeleteLocalRef(inputBuffer);
	} catch (...) {
		SIPXLOGE("QilexMicTask:: wyjatek !!!");
		{
			resip::Lock lock(mutex);
			working = false;
		}
	}
	SIPXLOGI("QilexMicTask:: thread mikro wyjscie z petli");

	qilex_jvm->DetachCurrentThread();
	SIPXLOGI("QilexMicTask:: thread mikro detached");
	{
		resip::Lock lock(mutex);
		working = false;
	}
}

void QilexMicTask::startStream() {
	SIPXLOGI("QilexMicTask::startStream()");

	JNIEnv *env = 0;
	JNIEnv *globalenv;
	int envstat = qilex_jvm->GetEnv((void**) &globalenv, JNI_VERSION_1_6);
	jint attachres = qilex_jvm->AttachCurrentThread(&env, NULL);

	QilexAndroidAudioWrapper* audioWrapper =
			QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();
	audioWrapper->createAudioRecordObject(env);

	{
		resip::Lock lock(mutex);
		streamDone = false;
		working = true;
	}
	this->start();

	SIPXLOGI("QilexMicTask::startStream()/");
}

void QilexMicTask::stopStream() {
	SIPXLOGI("SIGMA-PHUCPV / Class QilexMicTask / method stopStream");
	resip::Lock lock(mutex);
	streamDone = true;
}
