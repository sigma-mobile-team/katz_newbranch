#ifndef SDPCodecDescriptor_h
#define SDPCodecDescriptor_h

#include <string>

typedef struct tag_codec_info
{
	std::string sdpCodecType;
	unsigned int sampleRate;
	unsigned char payloadType;
} SDPCodecDescriptor;

#endif //SDPCodecDescriptor_h
