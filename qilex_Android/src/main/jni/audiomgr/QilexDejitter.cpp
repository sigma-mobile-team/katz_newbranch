/*********************************/
/* QilexDejitter.cpp             */
/* Lukasz Rychter 2012/01/12     */
/*							     */
/* Dejitter for RTP packets      */
/*********************************/

#include "QilexDejitter.h"

#include "QualityStats.h"
#include "QilexLogger.hxx"
#include "QilexAndroidAudioWrapper.h"

#include "../resiprocate/rutil/Lock.hxx"

#include <sys/timeb.h>
#include <stdint.h>

using namespace std;

QilexDejitter::QilexDejitter() : 
	m_nBufferSize(DEFAULT_DEJITTER_BUFFER_LEN), 
	m_nLastPulledSeq(-1),
	m_nBufferOverrunTime(0),
	m_nLastDiscardTime(0),
	m_nBufferUnderrunTime(0),
	m_nLastNullInsertedTime(0),
	m_bWasFilled(false),
	m_bSilence(false)
{
	QilexAndroidAudioWrapper* aw = QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();
	if (aw->isLTE())
		m_nBufferSize = 15; //x20ms
	else if (aw->isMobile2G())
		m_nBufferSize = 35;

	QualityStats::Get().SetJitterBufferSize(m_nBufferSize);
	m_nLastJitterBufferSizeChanged = GetTimeMS();
	m_nJitterBufferLowLevel = m_nBufferSize * LOW_LEVEL_RATIO;
	m_nLastRegeneratedBufferSize = m_nBufferSize;
}


void QilexDejitter::pushPacket(const boost::shared_ptr<RTPPacket>& pRtp)
{
	if (!pRtp || !pRtp->isValid())
		return;

	unsigned long nTime = GetTimeMS();
	unsigned short nNewSeq = pRtp->GetSeq();

	resip::Lock lock(m_csAccess);

	if (IsSeqEarlier(nNewSeq, m_nLastPulledSeq) && m_nLastPulledSeq != -1)
	{
		//late packet
		if (!pRtp->IsFEC())
		{
			QualityStats::Get().AddLatePacket();
			QualityStats::Get().AddReceivedPacket();
		}

		return;
	}

	QualityStats::Get().AddReceivedPacket();

	if (m_nLastPulledSeq == nNewSeq)
	{
		//duplicated packet
		return;
	}

	if (!m_sPackets.empty())
	{
		unsigned short nEarliestSeq = (*m_sPackets.begin())->GetSeq();
		unsigned short nOldestSeq = (*m_sPackets.rbegin())->GetSeq();
		unsigned short nPacketsInBuf = m_sPackets.size();

		//marking silent periods
		if (IsSeqEarlier(nOldestSeq, nNewSeq)) //omit late (but still usable, not too late) packets
		{
			if (pRtp->IsMarked())
				m_bSilence = true;
			else
				m_bSilence = false;
		}

	
		PacketsSet::iterator find_iter;
		if ((find_iter=m_sPackets.find(pRtp)) != m_sPackets.end() && (*find_iter)->isValid())
		{
			//duplicated packet
			return;
		}

		if (nPacketsInBuf >= m_nBufferSize)
		{
			//buffer is full
			unsigned char nOverrunSize = nPacketsInBuf-m_nBufferSize + 1;

			float fOverrunRatio = nOverrunSize/(float)m_nBufferSize;
			
			//drop packets in time deltas inversely proportional to the sqare of overrun ratio
			//always drops packet if OverrunSize >= BufferSize
			if (m_nBufferOverrunTime && nTime - m_nBufferOverrunTime > MINIMUM_OVERRUN_TIME && nTime - m_nLastDiscardTime >= (1.0f - fOverrunRatio*fOverrunRatio) * OVERRUN_DISCARDING_TIME_BASE)
			{
				//it is time to drop next packet
				m_nLastDiscardTime = nTime;

				QualityStats::Get().AddDiscardedPacket();
				
				if (IsSeqEarlier(nNewSeq, nEarliestSeq))
				{
					//discarded too late packet
					m_sDiscarded.insert(nNewSeq);
					return;
				}
				else
				{
					//discarding oldest packet from buf
					m_sDiscarded.insert(nEarliestSeq);
					m_sPackets.erase(m_sPackets.begin());
				}
			}			
		}
	}
	else
	{
		//marking silent periods
		if (pRtp->IsMarked())
			m_bSilence = true;
		else
			m_bSilence = false;
	}

	if (pRtp->GetDataSize() > 0)
		m_sPackets.insert(pRtp);
	else
		m_sSilent.insert(pRtp->GetSeq());

	if (!m_bWasFilled && (int)m_sPackets.size() >= m_nBufferSize*UNDERRUN_RATIO)
		m_bWasFilled = true;

	//Checking if we have gone out of underrun condition
	if ((int)m_sPackets.size() >= m_nBufferSize * UNDERRUN_RATIO)
		m_nBufferUnderrunTime = 0;

	//Checking for overrun condition, noting overrun start time
	if (m_nBufferOverrunTime == 0 && (int)m_sPackets.size() > m_nBufferSize)
		m_nBufferOverrunTime = nTime;
}

boost::shared_ptr<RTPPacket> QilexDejitter::pullPacket()
{
	boost::shared_ptr<RTPPacket> pPacket;

	resip::Lock lock(m_csAccess);

	if (!m_bWasFilled)
		return boost::shared_ptr<RTPPacket>(new RTPPacket());

	unsigned long nTime = GetTimeMS();
	unsigned short nPacketsInBuf = m_sPackets.size();

	if (nPacketsInBuf > m_nBufferSize)
	{
		//buffer is full
		unsigned char nOverrunSize = nPacketsInBuf-m_nBufferSize;

		float fOverrunRatio = nOverrunSize/(float)m_nBufferSize;
		
		//drop packets in time deltas inversely proportional to the sqare of overrun ratio
		//always drops packet if OverrunSize >= BufferSize
		if (m_nBufferOverrunTime && nTime - m_nBufferOverrunTime > MINIMUM_OVERRUN_TIME && nTime - m_nLastDiscardTime >= (1.0f - fOverrunRatio*fOverrunRatio) * OVERRUN_DISCARDING_TIME_BASE)
		{
			//it is time to drop next packet
			m_nLastDiscardTime = nTime;

			//discarding oldest packet from buf
			unsigned short nEarliestSeq = (*m_sPackets.begin())->GetSeq();
			QualityStats::Get().AddDiscardedPacket();
			m_sDiscarded.insert(nEarliestSeq);
			m_sPackets.erase(m_sPackets.begin());
		}			
	}


	//if there is less than LowLevel packets left we should consider increasing jitter buffer length 
	if ((int)m_sPackets.size() < m_nJitterBufferLowLevel)
	{
		int nNewBufferSize; 
		/*if (m_sPackets.empty())
		{
				nNewBufferSize = m_nBufferSize + 1;
		}
		else*/
			nNewBufferSize = m_nLastRegeneratedBufferSize + (m_nJitterBufferLowLevel - m_sPackets.size());
		
		if (nNewBufferSize > MAXIMUM_JITTER_BUFFER_LEN)
			nNewBufferSize = MAXIMUM_JITTER_BUFFER_LEN;

		if (nNewBufferSize > m_nBufferSize)
		{
			m_nBufferSize = nNewBufferSize;
			m_nLastJitterBufferSizeChanged = nTime;
			QualityStats::Get().SetJitterBufferSize(m_nBufferSize);
		}
	}
	
	//if there is at least _BUFFER_LOW_LEVEL_ packets, treat it as buffer regeneration. Consider decreasing buffer length
	if ((int)m_sPackets.size() >= m_nBufferSize * LOW_LEVEL_RATIO)
	{
		if (!m_bSilence && m_nBufferSize > MINIMUM_JITTER_BUFFER_LEN && nTime - m_nLastJitterBufferSizeChanged > (1.0f/m_nBufferSize) * JITTER_BUFFER_SHRINK_TIME_BASE)
		{
			--m_nBufferSize;
			m_nLastJitterBufferSizeChanged = nTime;
			QualityStats::Get().SetJitterBufferSize(m_nBufferSize);
		}

		m_nLastRegeneratedBufferSize = m_nBufferSize;
		m_nJitterBufferLowLevel = m_nBufferSize * LOW_LEVEL_RATIO;
	}


	unsigned short nLastBufSeq = m_nLastPulledSeq;
	
	if (!m_sPackets.empty())
		nLastBufSeq = (*m_sPackets.rbegin())->GetSeq();

	//checking for buffer underrun condition. Deciding if we should insert null packet.
	char nUnderrunSize = m_nBufferSize * UNDERRUN_RATIO - m_sPackets.size();
	float fUnderrunRatio = nUnderrunSize/(float)(m_nBufferSize * UNDERRUN_RATIO);

	if (nUnderrunSize >= 0)
	{
		if (m_bSilence)
			m_sPackets.insert(m_sPackets.end(), boost::shared_ptr<RTPPacket>(new RTPPacket(nLastBufSeq))); //keep buffer filled. Insert silence
	
		if (m_nBufferUnderrunTime && nTime-m_nBufferUnderrunTime > MINIMUM_UNDERRUN_TIME && nTime-m_nLastNullInsertedTime > (1.0f - fUnderrunRatio*fUnderrunRatio) * UNDERRUN_INSERTING_TIME_BASE)
		{
			m_nLastNullInsertedTime = nTime;

			if (m_bSilence)
				return boost::shared_ptr<RTPPacket>(new RTPPacket());
			else
			{
				QualityStats::Get().AddInsertedNullPacket();
				return boost::shared_ptr<RTPPacket>(); //NULL
			}
		}
	}


	if (!m_sPackets.empty())
	{
		pPacket = *m_sPackets.begin();
		
		unsigned short nNewSeq = pPacket->GetSeq();

		if (pPacket->isValid())
		{
			//counting lost packets
			unsigned short nLostPackets = 0;
			if (nNewSeq-m_nLastPulledSeq != 1 && m_nLastPulledSeq != -1)
			{
				//looking for seq twist (65535 -> 0)
				if (m_nLastPulledSeq - nNewSeq > 32768)
				{
					for (int seq=m_nLastPulledSeq+1; seq<=65535; ++seq)
						++nLostPackets;

					for (int seq=0; seq<nNewSeq; ++seq)
						++nLostPackets;
				}
				else
				{
					for (int seq=m_nLastPulledSeq+1; seq<nNewSeq; ++seq)
						++nLostPackets;
				}
			}

			if (m_nLastPulledSeq < 65535)
				++m_nLastPulledSeq;
			else
				m_nLastPulledSeq = 0;

			if (nLostPackets > 0 && LostPacket(m_nLastPulledSeq))
			{
				//packet is lost
				if (m_nBufferOverrunTime && nTime - m_nBufferOverrunTime > MINIMUM_OVERRUN_TIME)
				{
					//we are shrinking the buffer so it is a good opportunity to use lost packet for it
					m_nLastDiscardTime = nTime;
				}
				else
					pPacket.reset(); //insert null (PLC) packet
			}

			if (pPacket)
			{
				//we are using packet from queue
				m_sPackets.erase(m_sPackets.begin());
				m_nLastPulledSeq = nNewSeq;

				if (pPacket->IsFEC())
					QualityStats::Get().AddRecoveredPacket();
			}
		}
		else
			m_sPackets.erase(m_sPackets.begin());
	}

	//Checking for underrun condition (less than 2/3 packet in buffer). Saving underrun start time
	if (m_nBufferUnderrunTime == 0 && (int)m_sPackets.size() < m_nBufferSize*UNDERRUN_RATIO)
		m_nBufferUnderrunTime = nTime;

	//checking for overrun condition end
	if ((int)m_sPackets.size() < m_nBufferSize)
		m_nBufferOverrunTime = 0;
	
	return pPacket;
}



unsigned char QilexDejitter::GetPacketsCount()
{
	unsigned char nRet;
	m_csAccess.lock();
	nRet = m_sPackets.size();
	m_csAccess.unlock();
	return nRet;

}


bool QilexDejitter::LostPacket(unsigned short nSeq)
{
	set<unsigned short>::iterator found_discarded = m_sDiscarded.find(nSeq);
	if (found_discarded == m_sDiscarded.end())
	{
		set<unsigned short>::iterator found_silent = m_sSilent.find(nSeq);
		if (found_silent == m_sSilent.end())
		{
			QualityStats::Get().AddLostPacket();
			return true;
		}
		else
			m_sSilent.erase(found_silent);								
	}
	else
		m_sDiscarded.erase(found_discarded);

	return false;

}


unsigned long QilexDejitter::GetTimeMS()
{
	timeb stLocalTime;
	ftime(&stLocalTime);
	unsigned long nTimestamp = (unsigned long)(((int64_t)stLocalTime.time*1000 + (int64_t)stLocalTime.millitm) & 0xffffffff);
	return nTimestamp;
}
