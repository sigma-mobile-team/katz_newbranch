// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "QilexSpkrTask.h"
#include "QilexAndroidAudioWrapper.h"
#include <jni.h>
#include <climits>
#include "MpodQilexAndroid.h"
#include "QualityStats.h"
#include "../resiprocatejni/QilexConfig.hxx"
#include "../resiprocatejni/QilexLogger.hxx"
#include "../resiprocate/rutil/Lock.hxx"

extern JavaVM* qilex_jvm;

#define AUDIO_BUF_SIZE 640


QilexSpkrTask::QilexSpkrTask(const UtlString& name, void* pArg, const int priority, const int options, const int stackSize)
	: OsTask(name, pArg, priority, options, stackSize)
{
	odQilexAndroid = NULL;
	bytesInBuffer = 0;
	working = false;
}

int QilexSpkrTask::run(void* pArg)
{
	SIPXLOGI("SIGMA-PHUCPV / Class QilexSpkrTask / method run start");
	//SIPXLOGI("QilexSpkrTask:: thread glosnika przed petla");

	JNIEnv *env;
	jshort* audio_buffer;
	QilexAndroidAudioWrapper* audioWrapper = QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();

	JNIEnv *globalenv;
	int envstat = qilex_jvm->GetEnv((void**)&globalenv, JNI_VERSION_1_6);
	bool isLl = (audioWrapper->getAndroidVersion() >= 21);

	jint attachres = qilex_jvm->AttachCurrentThread(&env, NULL);

	if(attachres<0) SIPXLOGE("QilexSpkrTask::run env corrputed");

	//	SIPXLOGI("QilexSpkrTask:: thread glosnika przed petla createAudioTrackMethodsStubs");
	audioWrapper->createAudioTrackMethodsStubs(env);

	jshort* nat_buffer;
	jshort* resampled_nat_buffer;
	jshortArray outputBuffer;
	jshortArray resampled_outputBuffer;

	int frame_size = QilexConfig::getSamplesPerFrame();
	int native_frame_size = audioWrapper->getAudioTrackNativeSamplingRate()/50;
	bool resampling = frame_size != native_frame_size;


	outputBuffer = env->NewShortArray(AUDIO_BUF_SIZE);
	if(outputBuffer==0)
		SIPXLOGE("QilexSpkrTask:: output buff not created");
	nat_buffer = env->GetShortArrayElements(outputBuffer, 0);

	if (resampling)
	{
		resampled_outputBuffer = env->NewShortArray(native_frame_size);
		if(resampled_outputBuffer==0)
			SIPXLOGE("QilexSpkrTask:: native output buff not created");
		resampled_nat_buffer = env->GetShortArrayElements(resampled_outputBuffer, 0);
	}


	audioWrapper->setAudioPriority(env);
	audioWrapper->audiotrack_play(env);

	//SIPXLOGI("QilexSpkrTask:: thread glosnika wejscie do petli");

	bool running;
	{
		resip::Lock lock(mutex);
		running = !streamDone;
	}
	try
	{
		int fsize=0;
		unsigned long long lastwrite = 0;
		unsigned long framesWritten = 0;

		while(running)
		{
			{
				resip::Lock lock(mutex);
				running = !this->streamDone;
				working = true;
			}

			if(odQilexAndroid->isEnabled())
			{
				fsize = odQilexAndroid->fillBuffer(nat_buffer, frame_size);
				if (fsize > 0)
				{
					if (isLl)
						env->SetShortArrayRegion(outputBuffer, 0, fsize, nat_buffer);

					odQilexAndroid->notifyFrameDone();

					unsigned long long t1 = GetUSec();

					unsigned long tdelta = t1 - lastwrite;
					//SIPXLOGI("QilexSpkrTask delta:%d", tdelta);
					lastwrite = t1;

					int native_samples = fsize;
					if (resampling)
					{
						// TODO FIXME - this is simple implementation of linear interpolation, which will downgrade audio quality.
						// Replace this with better resampling algorithm, possibly from WebRTC or SipXtapi
						native_samples = fsize * native_frame_size / frame_size;
						float resamplingRatio =  frame_size / (float)native_frame_size;
						for (int i=0; i<native_samples; ++i)
						{
							float targetSample = i * resamplingRatio;
							int targetSampleFraction = targetSample - (int)targetSample;
							resampled_nat_buffer[i] = (short)((nat_buffer[(int)targetSample]   * (1.0f-targetSampleFraction) +
													           nat_buffer[(int)targetSample+1] * targetSampleFraction) / 2);
						}

						if (isLl)
							env->SetShortArrayRegion(resampled_outputBuffer, 0, native_samples, resampled_nat_buffer);
					}

					++framesWritten;
					audioWrapper->audiotrack_write(env, resampling ? resampled_outputBuffer : outputBuffer, 0, native_samples);
					int undertime = tdelta - 21000;
					if (undertime < 0)
						undertime = 0;
					while ((int)(GetUSec() - t1) < 19000 - undertime/3)
						OsTask::delay(0);
				}
				else
				{
					int headPos = audioWrapper->audiotrack_getPlaybackHeadPosition(env);
					unsigned long long bufferLength = framesWritten*native_frame_size - headPos;
					if (bufferLength > native_frame_size)
					{
						OsTask::delay(0);
					}
					else
					{
						//SIPXLOGI("QilexSpkrTask no spkr data to fetch");
						QualityStats::Get().AddLateAudioFrame();
						unsigned long long t2 = GetUSec();
						++framesWritten;
						audioWrapper->audiotrack_write(env, resampling ? resampled_outputBuffer : outputBuffer, 0, native_frame_size);
						while (GetUSec() - t2 < 20000)
							OsTask::delay(0);
					}
				}
			}
		}
		audioWrapper->audiotrack_stop(env);
		audioWrapper->audiotrack_release(env);
		audioWrapper->deleteAudioTrackObject(env);
		if (resampling)
		{
			env->ReleaseShortArrayElements(resampled_outputBuffer, resampled_nat_buffer, 0);
			env->DeleteLocalRef(resampled_outputBuffer);
		}
		env->ReleaseShortArrayElements(outputBuffer, nat_buffer, 0);
		env->DeleteLocalRef(outputBuffer);
	}
	catch(...)
	{
		SIPXLOGE("QilexSpkrTask:: wyjatek !!!");
		working = false;
	}

	{
		resip::Lock lock(mutex);
		working = false;
	}
	qilex_jvm->DetachCurrentThread();
	SIPXLOGI("QilexSpkrTask:: thread glosnika detached");
}

void QilexSpkrTask::startStream()
{
	SIPXLOGI("QilexSpkrTask::startStream() beg");
	JNIEnv *env = 0;
	JNIEnv *globalenv;
	int envstat = qilex_jvm->GetEnv((void**)&globalenv, JNI_VERSION_1_6);
	jint attachres = qilex_jvm->AttachCurrentThread(&env, NULL);

	QilexAndroidAudioWrapper* audioWrapper = QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();
	audioWrapper->createAudioTrackObject(env);

	{
		resip::Lock lock(mutex);
		this->streamDone = false;
		working = true;
	}

	this->start();
	SIPXLOGE("QilexSpkrTask::startStream() end");
}

void QilexSpkrTask::stopStream()
{
	SIPXLOGE("QilexSpkrTask::stopStream() beg");
	resip::Lock lock(mutex);
	this->streamDone = true;
}
