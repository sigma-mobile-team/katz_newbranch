/**********************************/
/* IRTCP.h                        */
/* Lukasz Rychter 2011/10/07      */
/*                                */
/* RTCP wrapper/handler interface */
/**********************************/

#ifndef __IRTCP_H__
#define __IRTCP_H__

#include "rtcp/IRTCPConnection.h"

#define	QILEX_FEC_TYPE_RS21 1
#define __int64 long long

class IRTCPwrapper
{
public:
	virtual IRTCPConnection& GetIRTCPConnection() const = 0;
};


#endif
