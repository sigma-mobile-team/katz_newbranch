#include "AudioMgr.h"

#include <algorithm>
#include <fstream>
#include <boost/make_shared.hpp>

#include "AudioMgrEvent.h"

#include "mp/MpMisc.h"
#include "mp/MpInputDeviceManager.h"
#include "mp/MpOutputDeviceManager.h"
#include "mp/MpInputDeviceDriver.h"
#include "mp/MpOutputDeviceDriver.h"
#include "mp/MprToOutputDevice.h"
#include "mp/MprFromInputDevice.h"
#include "mp/MpMediaTask.h"
#include "mp/MpCodec.h"
#include "mp/MprWebRTCEchoControlMobile.h"
#include "mp/MprWebRTCEchoCancellation.h"
#include "mp/MprVad.h"
#include "mp/MpVadWebRTC.h"
#include "mp/MprWebRTCGainControl.h"
#include "mp/MpFlowGraphBase.h"
#include "MpidQilexAndroid.h"
#include "MpodQilexAndroid.h"
#include "QilexSpkrTask.h"
#include "QilexMicTask.h"

#include "InQilexDecoder.h"
#include "OutQilexEncoder.h"

#include "RTCP.h"
#include "QilexLogger.hxx"
#include "QualityStats.h"

#include "QilexAndroidAudioWrapper.h"

#include <webrtc/system_wrappers/interface/cpu_features_wrapper.h>

//TODO
#include "../AudioStats/AudioStats.h"
std::unique_ptr<AudioStats> _audioStats;

#define BUFFERS_TO_BUFFER_ON_INPUT    20      ///< Number of buffers to buffer in input manager.

#define CHECK_PTR(x)
#define CHECK_OSRESULT(x,y)
#define SAFE_DELETE(x) { delete x; x = NULL; }

AudioMgr* AudioMgr::instance = NULL;

class NullOsNotification: public OsNotification {
	virtual OsStatus signal(const intptr_t eventData) {
		return MpMediaTask::signalFrameStart();
	}
};

class sipXtapiContext {
public:
	sipXtapiContext() :
			sourceDeviceId(0), sinkDeviceId(0), sourceResource(NULL), sinkResource(
			NULL), webrtcEchoControlMobile(NULL), webrtcEchoCancellation(
			NULL), pVAD(NULL), pAGC(NULL), pEncoder(NULL), pDecoder(
			NULL), mpMediaTask(NULL), mpFlowGraph(NULL), mpInputDeviceManager(
			NULL), mpOutputDeviceManager(NULL), sInputDriverNames(NULL), sOutputDriverNames(
			NULL), nInputDrivers(0), nOutputDrivers(0), mInputDeviceNumber(0), mOutputDeviceNumber(
					0) {
	}
public:
	//! Manager for input devices.
	MpInputDeviceManager *mpInputDeviceManager;
	//! Manager for output devices.
	MpOutputDeviceManager *mpOutputDeviceManager;

	//! input output devices
	MpInputDeviceHandle sourceDeviceId;
	MpOutputDeviceHandle sinkDeviceId;

	MprFromInputDevice* sourceResource;
	MprToOutputDevice* sinkResource;

	NullOsNotification* m_pOsNotification;

	//! data flow processing filters
	MprWebRTCEchoControlMobile* webrtcEchoControlMobile;
	MprWebRTCEchoCancellation* webrtcEchoCancellation;
	MprVad* pVAD;
	MprWebRTCGainControl* pAGC;

	OutQilexEncoder* pEncoder;
	InQilexDecoder* pDecoder;

	//! main media processing task
	MpMediaTask* mpMediaTask;

	//! Flowgraph for our input/output
	MpFlowGraphBase* mpFlowGraph;

	UtlString* sInputDriverNames;
	UtlString* sOutputDriverNames;

	size_t nInputDrivers;
	size_t nOutputDrivers;

	size_t mInputDeviceNumber;
	size_t mOutputDeviceNumber;

public:
	MpInputDeviceHandle manageInputDevice(MpInputDeviceDriver* pDriver) {
		if (!mpInputDeviceManager)
			return -1;
		// Add driver to manager
		MpInputDeviceHandle deviceId = mpInputDeviceManager->addDevice(
				*pDriver);
		// Driver is successfully added
		mInputDeviceNumber++;

		return deviceId;
	}

	/// Add passed device to output device manager.
	MpOutputDeviceHandle manageOutputDevice(MpOutputDeviceDriver* pDriver) {
		if (!mpOutputDeviceManager)
			return -1;
		// Add driver to manager
		MpOutputDeviceHandle deviceId = mpOutputDeviceManager->addDevice(
				pDriver);
		// Driver is successfully added
		mOutputDeviceNumber++;

		return deviceId;
	}
};

AudioMgr::AudioMgr(void) :
		m_bIsInitialized(true), m_bFlowGraphInitialized(false), m_pContext(
		NULL), m_eProcessingState(ProcessingState_Stopped), m_bMute(false), m_fOutputVolume(
				1.0), m_bSpeakerOn(false), m_bHeadphonesOn(false), m_nInputDeviceId(
				0), m_nOutputDeviceId(0), m_bLoopback(false), m_nLoopbackDelay(
				0), m_nDelayCnt(0), m_nSSRC(0), m_nLastDeviceCheck(0), mMicTask(
		NULL), mSpkrTask(NULL) {
	m_fDevicePerformance =
			QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper()->devicePerformance();
	m_pContext = new sipXtapiContext();
}

AudioMgr::~AudioMgr(void) {
	if (m_pContext) {
		delete m_pContext;
		m_pContext = NULL;
	}

	delete mMicTask;
	delete mSpkrTask;

	m_pTrafficSender.reset();
	m_pRTCP.reset();
}

void AudioMgr::setSelf(AudioMgr* inst) {
	m_pSelf.reset(inst);
}

AudioMgr* AudioMgr::Get() {
	if (!instance) {
		instance = new AudioMgr();
		instance->setSelf(instance);
	}
	return instance;
}
const std::list<SDPCodecDescriptor>& AudioMgr::GetSupportedCodecList() {
	static std::list<SDPCodecDescriptor> codecs;

	if (codecs.size() == 0) {
		SDPCodecDescriptor descriptor;
		descriptor.payloadType = 96;
		descriptor.sampleRate = 16000;
		descriptor.sdpCodecType = "opus";
		codecs.push_back(descriptor);

		descriptor.payloadType = 98;
		descriptor.sampleRate = 16000;
		descriptor.sdpCodecType = "speex";
		codecs.push_back(descriptor);

		descriptor.payloadType = 99;
		descriptor.sampleRate = 8000;
		descriptor.sdpCodecType = "speex";
		codecs.push_back(descriptor);

		//NAMND ADD: G711
		//2 type: PCMA and PCMU (8 KHz)
//		descriptor.payloadType = 100;
//		descriptor.sampleRate = 8000;
//		descriptor.sdpCodecType = "PCMA";
//		codecs.push_back(descriptor);
//
//		descriptor.payloadType = 101;
//		descriptor.sampleRate = 8000;
//		descriptor.sdpCodecType = "PCMU";
//		codecs.push_back(descriptor);
	}

	return codecs;
}

void AudioMgr::WaitToStopGraphFlow() {
	m_semFlowStop.lock();
	m_semFlowStop.unlock();
}

bool AudioMgr::Initialize() {
	m_pTrafficSender.reset();
	return true;
}

void AudioMgr::Deinitialize() {
	SIPXLOGI("AudioMgr::Deinitialize()");

	OnEvent(AudioMgrEvent::CreateShutdown());
	WaitUntilStopped();

	if (m_pTrafficSender) {
		m_pTrafficSender->UninstallEventHandler(ZEventType::StreamTransport,
				self());
		m_pTrafficSender.reset();
	}
}

void AudioMgr::AttachTrafficTransport(
		boost::shared_ptr<IStreamTransport> pTransport) {
	resip::Lock lock(m_csAccess);
	if (m_pTrafficSender) {
		m_pTrafficSender->UninstallEventHandler(ZEventType::StreamTransport,
				self());
		m_pTrafficSender.reset();
	}

	m_pTrafficSender = pTransport;
	if (m_pTrafficSender)
		m_pTrafficSender->InstallEventHandler(ZEventType::StreamTransport,
				self());

	if (pTransport)
		m_nSSRC = pTransport->GetSSRC();
	if (!m_OutCall) {
		m_pRTCP.reset(new RTCPwrapper(m_nSSRC, pTransport));
		assert(m_pRTCP);
	}
}

void AudioMgr::DetachTrafficTransport() {
	resip::Lock lock(m_csAccess);
	m_pTrafficSender.reset(); // = NULL;
}

void AudioMgr::OnEventListenerTStarted(void) {
}

int AudioMgr::OnEventListenerTEnded(void) {
	return 0;
}

const bool AudioMgr::ProcessEvent(const boost::shared_ptr<ZEvent>& event) {
	if (event->GetType() == ZEventType::StreamTransport) {
		StreamTransportEvent* pEvent =
				dynamic_cast<StreamTransportEvent*>(event.get());

		switch (event->GetSubtypeId()) {
		case StreamTransportEvent::DataReceived:
			if (m_pContext->pDecoder) {
				m_pContext->pDecoder->AddBufferToDecode(pEvent->GetData(),
						pEvent->GetDataSize());
				return false;
			}
			break;

		case StreamTransportEvent::RtcpDataReceived:
			if (m_pRTCP) {
				INetDispatch* pINetDispatch;
				IRTPDispatch* pIRTPDispatch;
				ISetSenderStatistics* pISetSenderStatistics;
				//ZCONV(RTCPwrapper, m_pRTCP)->GetIRTCPConnection().GetDispatchInterfaces(&pINetDispatch, &pIRTPDispatch, &pISetSenderStatistics);
				//IRTCPwrapper* irtcpwrap = m_pRTCP.get();
				//dynamic_cast<RTCPwrapper*>(irtcpwrap)->GetIRTCPConnection().GetDispatchInterfaces(&pINetDispatch, &pIRTPDispatch, &pISetSenderStatistics);
				m_pRTCP->GetIRTCPConnection().GetDispatchInterfaces(
						&pINetDispatch, &pIRTPDispatch, &pISetSenderStatistics);
				if (pINetDispatch) {
					pINetDispatch->ProcessPacket(
							(unsigned char*) pEvent->GetData(),
							pEvent->GetDataSize());
				} else
					assert(0);
			}
			return false;
		}
	}

	AudioMgrEvent* pEvent = dynamic_cast<AudioMgrEvent*>(event.get());
	if (pEvent) {
		switch (pEvent->GetSubtypeId()) {

		case AudioMgrEvent::StartProcessing: {
			m_SelectedCodecDescriptor = pEvent->m_spdCodecSelected;
			StartProcessing();
		}
			break;

		case AudioMgrEvent::StopProcessing: {
			int hr = StopProcessing();
			if (!hr)
				SIPXLOGE("AudioMgr::ProcessEvent:StopProcessing failed.");
		}
			break;

		case AudioMgrEvent::SetInputState: {
			SetInputState(pEvent->m_bBoolData);
		}
			break;

		case AudioMgrEvent::RestartProcessing: {
			RestartProcessing();
		}
			break;

		case AudioMgrEvent::EnableLoopbackMode: {
			m_bLoopback = pEvent->m_bBoolData;
			m_nLoopbackDelay = (int) pEvent->m_dwData;
			m_nLoopbackDelay = m_nLoopbackDelay / 20;
		}
			break;

		case AudioMgrEvent::LoopbackData: {
			if (m_eProcessingState != ProcessingState_Started)
				return false;
			if (m_bLoopback) {
				if (m_nDelayCnt > m_nLoopbackDelay) {
					if (pEvent->m_pBuffer) {
						//AKA m_pContext->pEncoder->AddLoopbackData(pEvent->m_pBuffer);
					}
				}
				m_nDelayCnt++;
			}
		}
			break;

		case AudioMgrEvent::Shutdown: {
			SIPXLOGI("AudioMgrEvent::Shutdown:");
			int hr = true;
			hr = StopProcessing();
			if (!hr)
				SIPXLOGE(
						"AudioMgr::ProcessEvent:Shutdown StopProcessing failed.");

			hr = ShutdownFlow();
			if (!hr)
				SIPXLOGE("AudioMgr::ProcessEvent:Shutdown failed.");

			break;
		}

		case AudioMgrEvent::OutputAudioConfigChanged: {
			SIPXLOGI(
					"AudioMgrEvent::OutputAudioConfigChanged: speaker=%d, headphones=%d",
					pEvent->m_bSpeakerOn, pEvent->m_bHeadphonesOn);
			if (m_bSpeakerOn != pEvent->m_bSpeakerOn
					|| m_bHeadphonesOn != pEvent->m_bHeadphonesOn) {
				m_bSpeakerOn = pEvent->m_bSpeakerOn;
				m_bHeadphonesOn = pEvent->m_bHeadphonesOn;
				OnOutputAudioConfigChanged();
			}
			break;
		}

		case AudioMgrEvent::OutputVolumeChanged: {
			SIPXLOGI("AudioMgrEvent::OutputVolumeChanged: volume=%f",
					pEvent->m_fData);
			if (m_fOutputVolume != pEvent->m_fData) {
				m_fOutputVolume = pEvent->m_fData;
				OnOutputVolumeChanged();
			}
			break;
		}
		}
	}

	return false;
}

int AudioMgr::InitializeFlow() {
	uint64_t CPUFeatures = WebRtc_GetCPUFeaturesARM();
	bool bARMv7 = CPUFeatures & kCPUFeatureARMv7;
	bool bVFPv3 = CPUFeatures & kCPUFeatureVFPv3;
	bool bNEON = CPUFeatures & kCPUFeatureNEON;

	SIPXLOGI(
			"AudioMgr::InitializeFlow, ARMv7:%d, VFPv3:%d, NEON:%d, DevicePerformance=%.2f",
			bARMv7 ? 1 : 0, bVFPv3 ? 1 : 0, bNEON ? 1 : 0,
			m_fDevicePerformance);

	bool bEnableEchoCanceller = m_fDevicePerformance >= 0.25;
	bool bEnableFullAEC = m_fDevicePerformance >= 0.5
			&& (CPUFeatures & kCPUFeatureARMv7)
			&& (CPUFeatures & kCPUFeatureVFPv3);
	SIPXLOGI("AudioMgr::InitializeFlow, echoCanceller:%d, fullAEC:%d",
			bEnableEchoCanceller ? 1 : 0, bEnableFullAEC ? 1 : 0);

	OsStatus final_result = OS_SUCCESS;
	OsStatus result = mpStartUp(m_SelectedCodecDescriptor.sampleRate,
			m_SelectedCodecDescriptor.sampleRate / 50, 1000, 0, 0, NULL);

	if (result != OS_SUCCESS) {
		SIPXLOGE("AudioMgr::InitializeFlow: mpStartup failed.");
		return false;
	}

	// Create flowgraph
	m_pContext->mpFlowGraph = new MpFlowGraphBase(
			m_SelectedCodecDescriptor.sampleRate / 50,
			m_SelectedCodecDescriptor.sampleRate);
	CHECK_PTR(m_pContext->mpFlowGraph);

	// Create media task
	m_pContext->mpMediaTask = MpMediaTask::getMediaTask();
	if (!m_pContext->mpMediaTask)
		m_pContext->mpMediaTask = MpMediaTask::createMediaTask(1);
	CHECK_PTR(m_pContext->mpMediaTask);

	// Create input and output device managers
	m_pContext->mpInputDeviceManager = new MpInputDeviceManager(
			m_SelectedCodecDescriptor.sampleRate / 50,
			m_SelectedCodecDescriptor.sampleRate,
			BUFFERS_TO_BUFFER_ON_INPUT, *GetMpMisc().RawAudioPool);
	CHECK_PTR(m_pContext->mpInputDeviceManager);

	m_pContext->mpOutputDeviceManager = new MpOutputDeviceManager(
			m_SelectedCodecDescriptor.sampleRate / 50,
			m_SelectedCodecDescriptor.sampleRate, 1);
	CHECK_PTR(m_pContext->mpOutputDeviceManager);

#ifdef WIN32
	///! DEFAULT WIN32 INPUT DEVICE
	m_pContext->nInputDrivers = 1;// Make sure we want only one driver.
	assert(m_pContext->sInputDriverNames == NULL);
	m_pContext->sInputDriverNames = new UtlString[m_pContext->nInputDrivers];

	char SomeAsciiStr[1024];
	memset(SomeAsciiStr, 0, sizeof(SomeAsciiStr));
	WideCharToMultiByte(CP_ACP, 0, m_mInputDevices[m_nInputDeviceId].strShortName.c_str(), -1, SomeAsciiStr, 1024, NULL, NULL);
	m_pContext->sInputDriverNames[0] = SomeAsciiStr;
	{

		// Create driver
		MpidWinMM *pDriver = new MpidWinMM(m_pContext->sInputDriverNames[0], *m_pContext->mpInputDeviceManager);
		CHECK_PTR(pDriver);
		// Add driver to manager
		m_pContext->manageInputDevice(pDriver);
	}

	///! DEFAULT WIN32 OUTPUT DEVICE
	m_pContext->nOutputDrivers = 1;// Make sure we want only one driver.
	m_pContext->sOutputDriverNames = new UtlString[m_pContext->nOutputDrivers];
	WideCharToMultiByte(CP_ACP, 0, m_mOutputDevices[m_nOutputDeviceId].strShortName.c_str(), -1, SomeAsciiStr, 1024, NULL, NULL);
	m_pContext->sOutputDriverNames[0] = SomeAsciiStr;
	{
		// Create driver
		MpodWinMM *pDriver= new MpodWinMM(m_pContext->sOutputDriverNames[0]);
		CHECK_PTR(pDriver);
		// Add driver to manager
		m_pContext->manageOutputDevice(pDriver);
	}

	m_pContext->mpInputDeviceManager->getDeviceId(m_pContext->sInputDriverNames[0], m_pContext->sourceDeviceId);

	m_pContext->mpOutputDeviceManager->getDeviceId(m_pContext->sOutputDriverNames[0], m_pContext->sinkDeviceId);

#endif

#ifdef ANDROID
	{
		SIPXLOGI("KUNLQT / Class AudioMgr / init MpidQilexAndroid");
		MpidQilexAndroid *pDriver = new MpidQilexAndroid("MpidQilexAndroid", *m_pContext->mpInputDeviceManager);
		CHECK_PTR(pDriver);
		// Add driver to manager
		m_pContext->sourceDeviceId = m_pContext->manageInputDevice(pDriver);
		mMicTask = new QilexMicTask("QilexMicTask", NULL);
		mMicTask->setidQilexAndroid(pDriver);
		mMicTask->startStream();
	}

	{
		// Create driver
		SIPXLOGI("KUNLQT / Class AudioMgr / init MpodQilexAndroid");
		MpodQilexAndroid *pDriver = new MpodQilexAndroid("MpodQilexAndroid");
		CHECK_PTR(pDriver);
		// Add driver to manager
		m_pContext->sinkDeviceId = m_pContext->manageOutputDevice(pDriver);
		mSpkrTask = new QilexSpkrTask("QilexSpkrTask", NULL, 0);
		mSpkrTask->setodQilexAndroid(pDriver);
		mSpkrTask->startStream();
	}

#endif

	// Create source (input) and sink (output) resources.
	m_pContext->sourceResource = new MprFromInputDevice("MprFromInputDevice",
			m_pContext->mpInputDeviceManager, m_pContext->sourceDeviceId);
	CHECK_PTR(m_pContext->sourceResource);

	m_pContext->sinkResource = new MprToOutputDevice("MprToOutputDevice",
			m_pContext->mpOutputDeviceManager, m_pContext->sinkDeviceId);
	CHECK_PTR(m_pContext->sinkResource);

	if (bEnableEchoCanceller) {
		result = m_pContext->sinkResource->enableCopyQ(true);
		CHECK_OSRESULT(result, final_result);
	}

	if (bEnableEchoCanceller) {
		if (bEnableFullAEC) {
			m_pContext->webrtcEchoCancellation = new MprWebRTCEchoCancellation(
					"WebRTCEchoCancellation",
					m_pContext->sinkResource->getCopyQ());
			CHECK_PTR(m_pContext->webrtcEchoCancellation);
		} else {
			m_pContext->webrtcEchoControlMobile =
					new MprWebRTCEchoControlMobile("WebRTCEchoControlMobile",
							m_pContext->sinkResource->getCopyQ());
			CHECK_PTR(m_pContext->webrtcEchoControlMobile);
		}
	}

	m_pContext->pVAD = new MprVad("WebRtcVAD", MpVadWebRTC::name);
	CHECK_PTR(m_pContext->pVAD);

	m_pContext->pAGC = new MprWebRTCGainControl("WebRTCGainControl");
	CHECK_PTR(m_pContext->pAGC);

	m_pContext->pEncoder = new OutQilexEncoder("OutQilexEncoder", m_nSSRC,
			m_pRTCP,m_OutCall);

	CHECK_PTR(m_pContext->pEncoder);

	m_pContext->pDecoder = new InQilexDecoder("InQilexDecoder", m_nSSRC,
			m_pRTCP);
	CHECK_PTR(m_pContext->pDecoder);

	m_pContext->pEncoder->AttachTrafficSender(m_pTrafficSender);

	m_pContext->pEncoder->Initialize(m_SelectedCodecDescriptor.sdpCodecType,
			m_SelectedCodecDescriptor.sampleRate);

	m_pContext->pDecoder->Initialize(m_SelectedCodecDescriptor.sdpCodecType,
			m_SelectedCodecDescriptor.sampleRate);

	m_pContext->pEncoder->EnableLoopbackMode(m_bLoopback);
	m_pContext->pDecoder->EnableLoopbackMode(m_bLoopback);

	/*
	 if(m_bLoopback)
	 {
	 m_pContext->pDecoder->SetupParent(this);
	 }
	 else
	 m_pContext->pDecoder->SetupParent(NULL);
	 */

	// add resources to data flow
	result = m_pContext->mpFlowGraph->addResource(*m_pContext->sourceResource);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->addResource(*m_pContext->sinkResource);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->addResource(*m_pContext->pVAD);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->addResource(*m_pContext->pAGC);
	CHECK_OSRESULT(result, final_result);
	if (bEnableEchoCanceller) {
		if (bEnableFullAEC)
			result = m_pContext->mpFlowGraph->addResource(
					*m_pContext->webrtcEchoCancellation);
		else
			result = m_pContext->mpFlowGraph->addResource(
					*m_pContext->webrtcEchoControlMobile);

		CHECK_OSRESULT(result, final_result);
	}
	result = m_pContext->mpFlowGraph->addResource(*m_pContext->pEncoder);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->addResource(*m_pContext->pDecoder);
	CHECK_OSRESULT(result, final_result);

	// input filter chain
	if (bEnableEchoCanceller) {
		if (bEnableFullAEC) {
			result = m_pContext->mpFlowGraph->addLink(
					*m_pContext->sourceResource, 0,
					*m_pContext->webrtcEchoCancellation, 0);
			CHECK_OSRESULT(result, final_result);
			result = m_pContext->mpFlowGraph->addLink(
					*m_pContext->webrtcEchoCancellation, 0, *m_pContext->pVAD,
					0);
			CHECK_OSRESULT(result, final_result);
		} else {
			result = m_pContext->mpFlowGraph->addLink(
					*m_pContext->sourceResource, 0,
					*m_pContext->webrtcEchoControlMobile, 0);
			CHECK_OSRESULT(result, final_result);
			result = m_pContext->mpFlowGraph->addLink(
					*m_pContext->webrtcEchoControlMobile, 0, *m_pContext->pVAD,
					0);
			CHECK_OSRESULT(result, final_result);
		}
	} else {
		result = m_pContext->mpFlowGraph->addLink(*m_pContext->sourceResource,
				0, *m_pContext->pVAD, 0);
		CHECK_OSRESULT(result, final_result);
	}
	result = m_pContext->mpFlowGraph->addLink(*m_pContext->pVAD, 0,
			*m_pContext->pAGC, 0);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->addLink(*m_pContext->pAGC, 0,
			*m_pContext->pEncoder, 0);
	CHECK_OSRESULT(result, final_result);

	// output filter chain
	result = m_pContext->mpFlowGraph->addLink(*m_pContext->pDecoder, 0,
			*m_pContext->sinkResource, 0);
	CHECK_OSRESULT(result, final_result);

	m_pContext->m_pOsNotification = new NullOsNotification();

	// Set flowgraph ticker  
	result = m_pContext->mpOutputDeviceManager->setFlowgraphTickerSource(
			m_pContext->sinkDeviceId, m_pContext->m_pOsNotification);
	CHECK_OSRESULT(result, final_result);

	// Manage flowgraph with media task.
	result = m_pContext->mpMediaTask->manageFlowGraph(*m_pContext->mpFlowGraph);
	CHECK_OSRESULT(result, final_result);
	SIPXLOGI("SIGMA-PHUCPV / Class AudioMgr / method InitializeFlow: END");
	return final_result == OS_SUCCESS ? true : false;
}

int AudioMgr::StartProcessing() {
	/*
	 assert(m_bIsInitialized != false);
	 if(!m_bIsInitialized)
	 return false;
	 */

	if (m_eProcessingState == ProcessingState_Started)
		return true;

	if (m_bLoopback)
		m_nDelayCnt = 0;

	if (!m_bFlowGraphInitialized) {
		SIPXLOGI(
				"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing");
		int hr = InitializeFlow();
		if (hr) {
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing OK");
			m_bFlowGraphInitialized = true;
		} else {
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing ERROR");
			SIPXLOGE(
					"AudioMgr::StartProcessing failed to initialize audio data flow graph.");
			return false;
		}
	} else
		SIPXLOGE("AudioMgr::StartProcessing m_bFlowGraphInitialized as start");

#if _DEBUG
	m_pContext->mpMediaTask->setDebug(TRUE);
#endif
	OsTask::delay(20);

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 1");
	m_pContext->mpOutputDeviceManager->setFlowgraphTickerSource(
			m_pContext->sinkDeviceId, m_pContext->m_pOsNotification);

	m_pContext->mpInputDeviceManager->enableDevice(m_pContext->sourceDeviceId);
	m_pContext->mpOutputDeviceManager->enableDevice(m_pContext->sinkDeviceId);

	OsTask::delay(1);

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 2");
	m_pContext->pEncoder->SetupPayloadType(
			(uint8_t) m_SelectedCodecDescriptor.payloadType);
	m_pContext->pEncoder->AttachTrafficSender(m_pTrafficSender);

	// Enable resources
	m_pContext->mpFlowGraph->enable();

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 3");
	m_pContext->mpMediaTask->startFlowGraph(*m_pContext->mpFlowGraph);
	m_pContext->mpMediaTask->setFocus(m_pContext->mpFlowGraph);

	QualityStats::Get().ResetStats();

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 4");
	MpMediaTask::signalFrameStart();

	m_semFlowStop.lock();
	m_eProcessingState = ProcessingState_Started;

	SetInputState(m_bMute);
	UpdateEchoCancellerMode();
	UpdateCNGMode();
	UpdateDeviceLatency();

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 5");
	_audioStats.reset(new AudioStats);

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 6-RTCP init");
	auto rtcp = dynamic_cast<RTCPwrapper*>(m_pRTCP.get());
	if (rtcp) {
		typedef boost::signals2::signal<void(float seconds, float jitter)> signal_type;
		typedef boost::signals2::signal<void(unsigned int)> int_signal_type;

		SIPXLOGI(
				"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart");
		_audioStats->chartContainer().SetTimeOffset(TimeChart::clock::now());
		{
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart Jitter");
			auto jitterChart = boost::make_shared<MeanTimeChart>();
			auto slot = signal_type::slot_type(&TimeChart::AddValueNow,
					jitterChart.get(), _2).track(jitterChart);
			rtcp->onJitterStats.connect(slot);
			_audioStats->chartContainer().AddChart("Jitter", jitterChart);
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart Jitter END");
		}

		{
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart RTT");
			auto rttChart = boost::make_shared<MeanTimeChart>();
			auto slot = signal_type::slot_type(&TimeChart::AddValueNow,
					rttChart.get(), _2).track(rttChart);
			rtcp->onRTTStats.connect(slot);
			_audioStats->chartContainer().AddChart("RTT", rttChart);
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart RTT END");
		}

		{
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart Loss");
			auto lossChart = boost::make_shared<MeanTimeChart>();
			auto slot = signal_type::slot_type(&TimeChart::AddValueNow,
					lossChart.get(), _2).track(lossChart);
			rtcp->onLossPercentData.connect(slot);
			_audioStats->chartContainer().AddChart("Loss", lossChart);
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart Loss END");
		}

		{
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart ReceivedPacket");
			auto chart = boost::make_shared<TimeChart>();
			auto slot = int_signal_type::slot_type(&TimeChart::AddValueNow,
					chart.get(), _1).track(chart);
			QualityStats::Get().onReceivedPacket.connect(slot);
			_audioStats->chartContainer().AddChart("ReceivedPacket", chart);
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart ReceivedPacket END");
		}

		{
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart LostPacket");
			auto chart = boost::make_shared<TimeChart>();
			auto slot = int_signal_type::slot_type(&TimeChart::AddValueNow,
					chart.get(), _1).track(chart);
			QualityStats::Get().onLostPacket.connect(slot);
			_audioStats->chartContainer().AddChart("LostPacket", chart);
			SIPXLOGI(
					"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing 7-Add Chart LostPacket END");
		}

	}
	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StartProcessing FINISH");

	return true;
}

int AudioMgr::StopProcessing() {
	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StopProcessing START");
	SIPXLOGI("int AudioMgr::StopProcessing()");

	/*
	 assert(m_bIsInitialized != false);
	 if(!m_bIsInitialized)
	 return false;
	 */

	if (m_eProcessingState == ProcessingState_Stopped)
		return true;

	_audioStats.reset();
	m_pRTCP.reset();
	m_pContext->pEncoder->DetachTrafficSender();

	m_pContext->mpMediaTask->stopFlowGraph(*m_pContext->mpFlowGraph);
	MpMediaTask::signalFrameStart();

	m_pContext->mpMediaTask->setFocus(NULL);

	m_pContext->mpFlowGraph->disable();

	do {
		OsTask::delay(0);
	} while (m_pContext->mpFlowGraph->isStarted());

	m_pContext->mpOutputDeviceManager->setFlowgraphTickerSource(
	MP_INVALID_OUTPUT_DEVICE_HANDLE, NULL);

	OsTask::delay(20);

	m_eProcessingState = ProcessingState_Stopped;
	m_semFlowStop.unlock();
	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call StopProcessing END");
	return true;
}

int AudioMgr::ShutdownFlow() {
	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call ShutdownFlow START");
	OsStatus result;
	SIPXLOGI("int AudioMgr::ShutdownFlow()");

	if (!m_bFlowGraphInitialized)
		return true;

	m_pContext->mpMediaTask->stopFlowGraph(*m_pContext->mpFlowGraph);
	MpMediaTask::signalFrameStart();

	SIPXLOGI("int AudioMgr::ShutdownFlow() stopFlowGraph done");

	OsStatus final_result = OS_SUCCESS;

	result = m_pContext->mpFlowGraph->disable();
	CHECK_OSRESULT(result, final_result);

	do {
		OsTask::delay(10);
	} while (m_pContext->mpFlowGraph->isStarted());

	SIPXLOGI("int AudioMgr::ShutdownFlow() +");

	mMicTask->stopStream();
	mSpkrTask->stopStream();

	do {
		OsTask::delay(10);
	} while (mMicTask->isWorking() || mSpkrTask->isWorking());

	SIPXLOGI("int AudioMgr::ShutdownFlow() -");

	result = m_pContext->mpOutputDeviceManager->setFlowgraphTickerSource(
	MP_INVALID_OUTPUT_DEVICE_HANDLE, NULL);
	CHECK_OSRESULT(result, final_result);

	SAFE_DELETE(m_pContext->m_pOsNotification);

	result = m_pContext->mpMediaTask->unmanageFlowGraph(
			*m_pContext->mpFlowGraph);
	CHECK_OSRESULT(result, final_result);

	// Disable devices
	result = m_pContext->mpInputDeviceManager->disableDevice(
			m_pContext->sourceDeviceId);
	CHECK_OSRESULT(result, final_result);

	result = m_pContext->mpOutputDeviceManager->disableDevice(
			m_pContext->sinkDeviceId);
	CHECK_OSRESULT(result, final_result);

	MpMediaTask::signalFrameStart();
	OsTask::delay(20);

	m_pContext->mpFlowGraph->processNextFrame();
	OsTask::delay(20);

	// Remove resources from flowgraph. We should remove them explicitly
	// here, because they are stored on the stack and will be destroyed.
	result = m_pContext->mpFlowGraph->removeResource(*m_pContext->sinkResource);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->removeResource(
			*m_pContext->sourceResource);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->removeResource(*m_pContext->pAGC);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->removeResource(*m_pContext->pVAD);
	CHECK_OSRESULT(result, final_result);
	if (m_pContext->webrtcEchoCancellation) {
		result = m_pContext->mpFlowGraph->removeResource(
				*m_pContext->webrtcEchoCancellation);
		CHECK_OSRESULT(result, final_result);
	}
	if (m_pContext->webrtcEchoControlMobile) {
		result = m_pContext->mpFlowGraph->removeResource(
				*m_pContext->webrtcEchoControlMobile);
		CHECK_OSRESULT(result, final_result);
	}
	result = m_pContext->mpFlowGraph->removeResource(*m_pContext->pEncoder);
	CHECK_OSRESULT(result, final_result);
	result = m_pContext->mpFlowGraph->removeResource(*m_pContext->pDecoder);
	CHECK_OSRESULT(result, final_result);

	SIPXLOGI("int AudioMgr::ShutdownFlow() 8");

	// Free all input device drivers
	for (; m_pContext->mInputDeviceNumber > 0;
			m_pContext->mInputDeviceNumber--) {
		MpInputDeviceDriver *pDriver =
				m_pContext->mpInputDeviceManager->removeDevice(
						m_pContext->mInputDeviceNumber);
		if (pDriver)
			delete pDriver;
	}

	// Free all output device drivers
	for (; m_pContext->mOutputDeviceNumber > 0;
			m_pContext->mOutputDeviceNumber--) {
		MpOutputDeviceDriver *pDriver =
				m_pContext->mpOutputDeviceManager->removeDevice(
						m_pContext->mOutputDeviceNumber);
		if (pDriver)
			delete pDriver;
	}

	SIPXLOGI("int AudioMgr::ShutdownFlow() 9");

	SAFE_DELETE(m_pContext->sourceResource);
	SAFE_DELETE(m_pContext->sinkResource);
	SAFE_DELETE(m_pContext->pAGC);
	SAFE_DELETE(m_pContext->pVAD);
	SAFE_DELETE(m_pContext->webrtcEchoControlMobile);
	SAFE_DELETE(m_pContext->webrtcEchoCancellation);
	SAFE_DELETE(m_pContext->pEncoder);
	SAFE_DELETE(m_pContext->pDecoder);

	// Free device managers
	SAFE_DELETE(m_pContext->mpOutputDeviceManager)
	SAFE_DELETE(m_pContext->mpInputDeviceManager)

	// Free flowgraph
	SAFE_DELETE(m_pContext->mpFlowGraph)

	SIPXLOGI("int AudioMgr::ShutdownFlow() A");

	// Free the input and output driver names, if any.
	if (m_pContext->sInputDriverNames) {
		delete[] m_pContext->sInputDriverNames;
		m_pContext->sInputDriverNames = NULL;
	}
	if (m_pContext->sOutputDriverNames) {
		delete[] m_pContext->sOutputDriverNames;
		m_pContext->sOutputDriverNames = NULL;
	}

	SIPXLOGI("int AudioMgr::ShutdownFlow() B");

	m_pContext->mpMediaTask = NULL;

	SIPXLOGI("int AudioMgr::ShutdownFlow() B1");

	result = mpShutdown();
	if (result != OS_SUCCESS) {
		SIPXLOGI("int AudioMgr::ShutdownFlow() mpShut failed");
	}

	SIPXLOGI("int AudioMgr::ShutdownFlow() C");

	m_bFlowGraphInitialized = false;

	SIPXLOGI("int AudioMgr::ShutdownFlow()/");

	SIPXLOGI(
			"SIGMA-PHUCPV / Class AudioMgr / method AudioMgr: Call ShutdownFlow END");
	return final_result == OS_SUCCESS ? true : false;
}

void AudioMgr::SetInputState(bool bMute) {
	m_csAccess.lock();
	m_bMute = bMute;
	m_csAccess.unlock();

	if (m_pContext->webrtcEchoControlMobile) {
		if (bMute)
			m_pContext->webrtcEchoControlMobile->disable();
		else
			m_pContext->webrtcEchoControlMobile->enable();
	}

	if (m_pContext->webrtcEchoCancellation) {
		if (bMute)
			m_pContext->webrtcEchoCancellation->disable();
		else
			m_pContext->webrtcEchoCancellation->enable();
	}

	if (m_pContext->pVAD) {
		if (bMute)
			m_pContext->pVAD->disable();
		else
			m_pContext->pVAD->enable();
	}

	if (m_pContext->pAGC) {
		if (bMute)
			m_pContext->pAGC->disable();
		else
			m_pContext->pAGC->enable();
	}
}

void AudioMgr::DispachError(int dwCode) {
	boost::shared_ptr<AudioMgrEvent> pEvent = AudioMgrEvent::CreateAudioError(
			dwCode);
	DispatchEvent(pEvent);
}

void AudioMgr::RestartProcessing() {
	if (m_eProcessingState == ProcessingState_Stopped)
		return;

	StopProcessing();
	ShutdownFlow();
	StartProcessing();
}

void AudioMgr::OnOutputAudioConfigChanged() {
	UpdateEchoCancellerMode();
	UpdateCNGMode();
}

void AudioMgr::OnOutputVolumeChanged() {
	UpdateEchoCancellerMode();
}

void AudioMgr::UpdateEchoCancellerMode() {
	//AECM
	if (m_pContext->webrtcEchoControlMobile) {
		int nEchoMode = 2;

		if (m_bSpeakerOn) {
			if (m_fOutputVolume >= 0.6)
				nEchoMode = 4;
			else if (m_fOutputVolume >= 0.2)
				nEchoMode = 3;
			else
				nEchoMode = 2;
		} else if (m_bHeadphonesOn) {
			if (m_fOutputVolume >= 0.8)
				nEchoMode = 1;
			else
				nEchoMode = 0;
		} else {
			if (m_fOutputVolume >= 0.6)
				nEchoMode = 1;
			else
				nEchoMode = 0;
		}

		m_pContext->webrtcEchoControlMobile->setEchoMode(
				*m_pContext->webrtcEchoControlMobile,
				*m_pContext->mpFlowGraph->getMsgQ(), nEchoMode);
	}
}

void AudioMgr::UpdateCNGMode() {
	if (m_pContext->pDecoder) {
		bool bEnabled = true;

		if (m_bSpeakerOn) {
			bEnabled = false;
		} else if (m_bHeadphonesOn) {
			bEnabled = true;
		} else
			bEnabled = true;

		m_pContext->pDecoder->SetCNGEnabled(*m_pContext->pDecoder,
				*m_pContext->mpFlowGraph->getMsgQ(), bEnabled);
	}
}

void AudioMgr::UpdateDeviceLatency() {
	if (m_pContext->webrtcEchoCancellation) {
		int nDelayMs =
				QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper()->getDeviceLatencyMs();

		m_pContext->webrtcEchoCancellation->setSpkrQDelayMs(
				*m_pContext->webrtcEchoCancellation,
				*m_pContext->mpFlowGraph->getMsgQ(), nDelayMs);
	}
}

//Sirocco Mobile

bool AudioMgr::Bind(const std::string &strLocalIpAddress,
		unsigned short nLocalPort, bool bUseTURN) {
	resip::Lock lock(m_csAccess);
	assert(m_pTrafficSender != NULL);
	m_pTrafficSender->Bind(strLocalIpAddress, nLocalPort, bUseTURN);
}

void AudioMgr::startAudio(const SDPCodecDescriptor& desc) {
	boost::shared_ptr<AudioMgrEvent> pEvent =
			AudioMgrEvent::CreateStartProcessing();
	pEvent->m_spdCodecSelected = desc;
	OnEvent(pEvent);
}

void AudioMgr::stopAudio() {
	OnEvent(AudioMgrEvent::CreateShutdown());
	WaitToStopGraphFlow();

	resip::Lock lock(m_csAccess);
	if (m_pTrafficSender) {
		m_pTrafficSender->StopSend();
		m_pTrafficSender->StopRecv();
		m_pTrafficSender->UninstallEventHandler(ZEventType::StreamTransport,
				self());
		m_pTrafficSender->CloseConnection();
		m_pTrafficSender.reset();
	}
}

const boost::shared_ptr<IStreamTransport> AudioMgr::getStreamTransport() {
	resip::Lock lock(m_csAccess);
	return m_pTrafficSender;
}

void AudioMgr::SetEncoderBitrate(unsigned bitrate) {
	m_pContext->pEncoder->SetBitrate(bitrate);
}

float AudioMgr::GetJitterBufferUsage() {
	return QualityStats::Get().GetJitterBufferUsage();
}

