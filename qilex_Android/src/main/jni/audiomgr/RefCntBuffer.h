#ifndef __REFCNTBUFFER__
#define __REFCNTBUFFER__

class RefCntBuffer
{

public:
	unsigned char *pBuffer;
	unsigned int BufferSize;

//private:
	RefCntBuffer(
		const unsigned char *pInBuffer,
		unsigned int nInBufferSize)
	{
		if(pInBuffer && nInBufferSize>0)
		{
			pBuffer = new unsigned char[nInBufferSize];
			memcpy(pBuffer, pInBuffer, nInBufferSize);
			BufferSize = nInBufferSize;
		}
	}

	~RefCntBuffer()
	{
		if(pBuffer)
		{
			delete [] pBuffer;
			BufferSize = 0;
			pBuffer = NULL;
		}
	}
};

#endif
