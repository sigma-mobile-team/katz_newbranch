#ifndef _AUDIO_MGR_
#define _AUDIO_MGR_

#include <boost/shared_ptr.hpp>

#include "IStreamTransport.h"
#include "StreamTransportEvent.h"
#include "IRTCP.h"
#include "../resiprocate/rutil/RecursiveMutex.hxx"
#include "../resiprocate/rutil/Lock.hxx"
#include "../commonlib/ZEventListenerT.h"
#include "../commonlib/ZEventHandler.h"
#include "CodecDescriptor.h"

#include <memory>

class sipXtapiContext;
class QilexMicTask;
class QilexSpkrTask;
class AudioMgrEvent;

class AudioMgr: public ZEventListenerT, public ZEventHandler {
public:
	typedef struct device_descriptor_tag {
		int nId;
		std::string strShortName;
		std::string strDeviceName;
		bool bIsDefault;
	} AudioDevice;

	AudioMgr(void);
	virtual ~AudioMgr(void);
	static AudioMgr* Get();
	bool IsMuted() {
		resip::Lock lock(m_csAccess);
		return m_bMute;
	}

	const std::list<SDPCodecDescriptor>& GetSupportedCodecList();

public:
	bool Initialize();
	void Deinitialize();
	void AttachTrafficTransport(boost::shared_ptr<IStreamTransport> pTransport);
	void DetachTrafficTransport();

	void WaitToStopGraphFlow();

	boost::shared_ptr<AudioMgr> self() {
		return m_pSelf;
	}
	void SetEncoderBitrate(unsigned bitrate);

	//*SM*
	bool Bind(const std::string &strLocalIpAddress, unsigned short nLocalPort,
			bool bUseTURN = false);
	//*SM*
	void setOutCall(bool out) { // SIGMA TEAM
		m_OutCall = out;
	}
public:

	void startAudio(const SDPCodecDescriptor& desc);
	void stopAudio();
	float GetJitterBufferUsage(); //percent of jitter buffer currently filled with packets
	void OnEventListenerTStarted(void);
	int OnEventListenerTEnded(void);
	const boost::shared_ptr<IStreamTransport> getStreamTransport();

private:
	typedef enum tag_processing_state {
		ProcessingState_Stopped, ProcessingState_Started
	} ProcessingState;

private:
	//! creates all resources and setup flowgraph
	int InitializeFlow();

	//! start data processing
	int StartProcessing();

	//! stop data processing 
	int StopProcessing();

	//! stop processing and destroy flowgraph and resources
	int ShutdownFlow();

	//! mute input
	void SetInputState(bool bMute);

	//! dispatch audio error
	void DispachError(int dwCode);

	//! restart processing 
	void RestartProcessing();

	//! gets full name of the sound devices
	std::string GetFullName(const char* lpShortName,
			std::list<std::string>& strList);

	void setSelf(AudioMgr* inst);

	void OnOutputAudioConfigChanged();
	void OnOutputVolumeChanged();

	void UpdateEchoCancellerMode();
	void UpdateCNGMode();
	void UpdateDeviceLatency();

protected:
	virtual const bool ProcessEvent(const boost::shared_ptr<ZEvent>& event);

private:
	static AudioMgr* instance;
	bool m_bIsInitialized;
	bool m_bFlowGraphInitialized;
	ProcessingState m_eProcessingState;
	SDPCodecDescriptor m_SelectedCodecDescriptor;
	bool m_bMute;
	float m_fOutputVolume;
	bool m_bSpeakerOn;
	bool m_bHeadphonesOn;
	int m_nInputDeviceId;
	int m_nOutputDeviceId;

	bool m_bLoopback;
	int m_nLoopbackDelay;
	int m_nDelayCnt;

	unsigned long m_nSSRC;

	unsigned long m_nLastDeviceCheck;

	float m_fDevicePerformance;
	bool m_OutCall;
private:
	boost::shared_ptr<IStreamTransport> m_pTrafficSender;
	sipXtapiContext* m_pContext;

	resip::RecursiveMutex m_csAccess;
	resip::RecursiveMutex m_semFlowStop;

	boost::shared_ptr<IRTCPwrapper> m_pRTCP;

	QilexMicTask* mMicTask;
	QilexSpkrTask* mSpkrTask;

	boost::shared_ptr<AudioMgr> m_pSelf;
	//std::unique_ptr<AudioStats> _audioStats;
};

#endif

