// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef _MPIDQILEXANDROID_H
#define _MPIDQILEXANDROID_H

// SIPX INCLUDES
#include <mp/MpInputDeviceDriver.h>

class MpidQilexAndroid : public MpInputDeviceDriver
{
public:

	MpidQilexAndroid(const UtlString& name, MpInputDeviceManager& deviceManager);
	virtual ~MpidQilexAndroid();

    virtual OsStatus enableDevice(unsigned samplesPerFrame, 
                                  unsigned samplesPerSec,
                                  MpFrameTime currentFrameTime=0);

    virtual OsStatus disableDevice();

    virtual UtlBoolean isDeviceValid();

    OsStatus pushFrame(MpInputDeviceHandle deviceId,
            unsigned numSamples,
            MpAudioSample *samples,
            MpFrameTime frameTime);

    OsStatus pushFrame(MpAudioSample *samples);

    void dummySend();

protected:
    void audioRecord_read();

};

#endif  // _MPIDQILEXANDROID_H
