#include "OutQilexEncoder.h"
#include <sys/timeb.h>
#include <mp/MpCodecFactory.h>
#include "QualityStats.h"

#include "rtcp/SenderReport.h"
#include "QilexLogger.hxx"
#include "AudioMgr.h"
#include "QilexAndroidAudioWrapper.h"

#define RESIPROCATE_SUBSYSTEM resip::Subsystem::APP

using namespace std;

/// Maximum size of encoded frame (in bytes).
#define ENCODED_FRAME_MAX_SIZE   1480
#define MINIMUM_LOSS_PERCENT_TO_ENABLE_FEC 1.0
#define ENABLE_VAD
#define ENABLE_DTX
#define DTX_CUTOFF_FACTOR 0.1f //we stop transmitting if scaling factor is less than this
#define VAD_FADEOUT_FACTOR 0.1f //how fast sound is silenced after active period
#define VAD_RISING_FACTOR 0.4f //how fast volume raises after silence period
#define VAD_MINIMUM_SILENT_TIME 500 //[ms] minimum period without voice activity to start silencing voice

static const int MAX_PACKETS_BREAK_TIME = 500; //ms

static const int QUALITY_LOG_INTERVAL = 10000; //ms

//audio bitrate (bps) + headers overhead (~23kbps or ~12kpbs in 25p/s mode)
static const int BITRATE_2G = 5 * 1024;
static const int BITRATE_3G = 13 * 1024;
static const int BITRATE_MAX = 20 * 1024;

OutQilexEncoder::OutQilexEncoder(const UtlString& rName, unsigned long nSSRC,
		const boost::shared_ptr<IRTCPwrapper>& pRTCP, bool outCall) :
		MpAudioResource(rName, 1, 1, 0, 0), mcodecFrameSamples(0), mpEncoder(
		NULL), m_PayloadType(0), m_bLoopback(false), m_nSilentPacketsSent(0), m_fScalingFactor(
				0.0f), m_nInactivityStartTime(0), m_nCurrentBitrate(BITRATE_MAX) {
	mProcessedCnt = getTickCount();
	mEncodedCnt = 0;
	m_nSSRC = nSSRC;
	m_pRTCP = pRTCP;
	m_OutCall = outCall;
}

OutQilexEncoder::~OutQilexEncoder() {
	if (mpEncoder) {
		mpEncoder->freeEncode();
		delete mpEncoder;
		mpEncoder = NULL;
	}

	if (m_pTrafficSender) {
		m_pTrafficSender.reset();
	}
	//QualityStats::Get().CPU().ResetThreadMeter();
}

void OutQilexEncoder::AttachTrafficSender(
		boost::shared_ptr<IStreamTransport> pSender) {
	m_pTrafficSender = pSender;
}

void OutQilexEncoder::DetachTrafficSender() {
	m_pTrafficSender.reset();
}

void OutQilexEncoder::SetupPayloadType(uint8_t type) {
	m_PayloadType = type;
}

void OutQilexEncoder::EnableLoopbackMode(bool bEnable) {
	m_bLoopback = bEnable;
}

void OutQilexEncoder::AddLoopbackData(boost::shared_ptr<RefCntBuffer>& pBuffer,
		unsigned int nLoopbackDelay) {
	//m_qLoopbackQueue.Shrink(nLoopbackDelay);
	m_qLoopbackQueue.AddItem(pBuffer);
}

bool OutQilexEncoder::Initialize(const std::string& strCoderType,
		unsigned short nSampleRate) {
	MpCodecFactory *mpCodecFactory = MpCodecFactory::getMpCodecFactory();
	if (!mpCodecFactory)
		return false;

	QilexAndroidAudioWrapper* aw =
			QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();
	float fDevicePerformance = aw->devicePerformance();
	bool bMobile3G = aw->isMobile3G();
	bool bMobile2G = aw->isMobile2G();

	const MppCodecInfoV1_1 **pCodecInfo;
	unsigned codecInfoNum;

	mpCodecFactory->getCodecInfoArray(codecInfoNum, pCodecInfo);
	SIPXLOGE("OutQilexEncoder::codecInfoNum = %d", codecInfoNum);
	for (unsigned i = 0; i < codecInfoNum; i++) {
		const MppCodecInfoV1_1* pCodec = pCodecInfo[i];
		SIPXLOGE("OutQilexEncoder::Initialize - %s, %s, %d, %d",
				pCodec->mimeSubtype, strCoderType.c_str(), pCodec->sampleRate,
				nSampleRate);
		if (pCodec && pCodec->mimeSubtype
				&& strcasecmp(pCodec->mimeSubtype, strCoderType.c_str()) == 0
				&& pCodec->sampleRate == nSampleRate) {
			const char** pCodecFmtps = pCodecInfo[i]->fmtps;
			const char* strFmtps = NULL;

			int fmtpsIndex = 0;

			if (strcasecmp(pCodec->mimeSubtype, "speex") == 0) {
				if (fDevicePerformance <= 0.25 || bMobile2G)
					fmtpsIndex = 4;
				else if (fDevicePerformance < 0.75 || bMobile3G)
					fmtpsIndex = 6;
				else
					fmtpsIndex = 0;
			}
			if (pCodecFmtps)
				strFmtps = pCodecFmtps[fmtpsIndex];

			// strFmtps == NULL WHY?
			SIPXLOGE("OutQilexEncoder::Initialize - %s : mode=%d",
					pCodec->mimeSubtype, fmtpsIndex);
//			for(unsigned j = 0; j < sizeof(pCodecFmtps); j++){
//				SIPXLOGE("pCodecFmtps[%d] = %s",j,pCodecFmtps[j]);
//			}

			OsStatus result = mpCodecFactory->createEncoder(pCodec->mimeSubtype,
					strFmtps, pCodec->sampleRate, pCodec->numChannels, 0,
					mpEncoder);
			if (result != OS_SUCCESS || !mpEncoder) {
				//! log
				return false;
			}

			result = mpEncoder->initEncode();
			if (result != OS_SUCCESS) {
				//! log
				delete mpEncoder;
				mpEncoder = NULL;
				return false;
			}

			if (bMobile2G)
				m_nCurrentBitrate = BITRATE_2G;
			else if (bMobile3G)
				m_nCurrentBitrate = BITRATE_3G;
			else
				m_nCurrentBitrate = BITRATE_MAX;
			SIPXLOGE("OutQilexEncoder::Initialize - %s", pCodec->mimeSubtype);
			SetBitrate(m_nCurrentBitrate);
			break;
		}
	}

	if (!mpEncoder) {
		//! log
		return false;
	}

	mcodecFrameSamples = mpEncoder->getInfo()->getNumSamplesPerFrame();

	m_nLastPacketSentTime = getTickCount() - MAX_PACKETS_BREAK_TIME * 2; //force to send first packet

	return true;
}

void OutQilexEncoder::SetBitrate(unsigned bitrate) {
	EncoderCtl(ENCODER_CTL_SET_BITRATE, bitrate);
}

int OutQilexEncoder::EncoderCtl(int request, int value) {
	mpEncoder->EncoderCtl(request, value);
}

UtlBoolean OutQilexEncoder::doProcessFrame(MpBufPtr inBufs[],
		MpBufPtr outBufs[], int inBufsSize, int outBufsSize,
		UtlBoolean isEnabled, int samplesPerFrame, int samplesPerSecond) {
	timeb stLocalTime;
	ftime(&stLocalTime);
	unsigned long nTimestamp = (unsigned long) (((__int64) stLocalTime.time
			* 1000 + (__int64) stLocalTime.millitm) & 0xffffffff); //done here to also count encoding time as latency

	QilexAndroidAudioWrapper* aw =
			QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();
	float fDevicePerformance = aw->devicePerformance();
	bool bMobile3G = aw->isMobile3G();
	bool bMobile2G = aw->isMobile2G();

	unsigned long nBitrate;
	if (bMobile2G)
		nBitrate = BITRATE_2G;
	else if (bMobile3G)
		nBitrate = BITRATE_3G;
	else
		nBitrate = BITRATE_MAX;

	if (nBitrate != m_nCurrentBitrate) {
		m_nCurrentBitrate = nBitrate;
		SetBitrate(m_nCurrentBitrate);
	}

	static unsigned int nLastQualityLog = 0;
	unsigned int nCurrTime = getTickCount();
	if (nCurrTime - nLastQualityLog > QUALITY_LOG_INTERVAL) {
		nLastQualityLog = nCurrTime;
		QualityStats::Get().WriteStatsToLog();
	}

	if (mpEncoder && isInputConnected(0) && isEnabled) {
		for (int i = 0; i < inBufsSize; i++) {
			int maxPacketSamples = samplesPerFrame * 5;

			MpRtpBufPtr pRtpPacket = GetMpMisc().RtpPool->getBuffer();
			unsigned char *pRtpPayloadPtr =
					(unsigned char*) pRtpPacket->getDataWritePtr();

			MpAudioSample* pAudioSample;
			MpAudioBufPtr pAudio;

			bool bMuted = AudioMgr::Get()->IsMuted() || !inBufs[i].isValid();

			if (!bMuted) {
				pAudio = inBufs[i];
				pAudioSample = pAudio->getSamplesWritePtr();
			} else {
				pAudioSample = new MpAudioSample[samplesPerFrame];
				for (int i = 0; i < samplesPerFrame; ++i)
					pAudioSample[i] = 0;
			}

			if (!bMuted) {
#if 0
				for(int n =0;n<samplesPerFrame;n++)
				{
					pAudioSample[n] = (int16_t)(sinf( n * 2 * 3.14f/(float)samplesPerFrame * 10 ) * 20000.0f );
				}
#endif
				if (m_bLoopback) {
					memset(pAudioSample, 0, samplesPerFrame * 2);
					if (!m_qLoopbackQueue.IsEmpty()) {
						boost::shared_ptr<RefCntBuffer> pBuffer =
								m_qLoopbackQueue.GetItem();
						if (pBuffer->pBuffer && pBuffer->BufferSize > 0) {
							if (pBuffer->BufferSize == samplesPerFrame * 2) {
								memcpy(pAudioSample, pBuffer->pBuffer,
										pBuffer->BufferSize);
							}
						}
					}
				}
			}

			bool bForceSend = false;

			if (getTickCount() - m_nLastPacketSentTime
					> MAX_PACKETS_BREAK_TIME) {
				bForceSend = true;
			}

			int nOutEncodedSize = 0;
			UtlBoolean bIsPacketReady = false;
			UtlBoolean bIsPacketSilent = bMuted;
			int nPayloadSize = 0;
			int nSamplesInPacket = 0;
			int nOutSamplesDecoded = 0;

#ifdef ENABLE_VAD
			MpSpeechType speechType = MP_SPEECH_UNKNOWN;
			if (pAudio.isValid())
				speechType = pAudio->getSpeechType();

			if (speechType != MP_SPEECH_ACTIVE
					&& speechType != MP_SPEECH_UNKNOWN) {
				if (m_nInactivityStartTime == 0) {
					m_nInactivityStartTime = getTickCount();
				} else {
					if (getTickCount()
							- m_nInactivityStartTime> VAD_MINIMUM_SILENT_TIME) {
						m_fScalingFactor -= m_fScalingFactor
								* VAD_FADEOUT_FACTOR;

#ifdef ENABLE_DTX
						if (m_fScalingFactor < DTX_CUTOFF_FACTOR)
							bIsPacketSilent = true;
#endif
					}
				}
			} else {
				m_nInactivityStartTime = 0;
				m_fScalingFactor += (1.0f - m_fScalingFactor)
						* VAD_RISING_FACTOR;
			}

			if (m_fScalingFactor <= 0.99f) {
				for (int n = 0; n < samplesPerFrame; n++)
					pAudioSample[n] =
							(int) (pAudioSample[n] * m_fScalingFactor);
			}
#endif //ENABLE_VAD

			bool bSendData = !bIsPacketSilent || m_nSilentPacketsSent == 0;

			if (bSendData) {
				do {
					UtlBoolean bSilent;

					OsStatus result = mpEncoder->encode(
							pAudioSample + nSamplesInPacket,
							samplesPerFrame - nSamplesInPacket,
							nOutSamplesDecoded, pRtpPayloadPtr + nPayloadSize,
							ENCODED_FRAME_MAX_SIZE - nPayloadSize,
							nOutEncodedSize, bIsPacketReady, bSilent);

					if (result != OS_SUCCESS)
						assert(0);

					nPayloadSize += nOutEncodedSize;
					nSamplesInPacket += nOutSamplesDecoded;

					if (bIsPacketReady
							&& samplesPerFrame - nSamplesInPacket != 0)
						assert(0);

				} while (samplesPerFrame - nSamplesInPacket > 0);

				++mEncodedCnt;
			}

			if (bMuted)
				delete[] pAudioSample;

			const int ADDITIONAL_SILENCE_MARKER_PACKETS = 2;
			if (bIsPacketSilent
					&& m_nSilentPacketsSent
							<= ADDITIONAL_SILENCE_MARKER_PACKETS)
				bForceSend = true;

			if (bForceSend || bSendData) {
				unsigned short nCurrentSeq = mProcessedCnt;

				pRtpPacket->setRtpVersion(2);
				pRtpPacket->setRtpPayloadType(m_PayloadType);
				pRtpPacket->setPayloadSize(nPayloadSize);
				pRtpPacket->setRtpTimestamp(nTimestamp);
				pRtpPacket->setRtpSequenceNumber(nCurrentSeq);
				pRtpPacket->setRtpCSRCCount(0);
				pRtpPacket->setRtpSSRC(m_nSSRC);
				pRtpPacket->disableRtpPadding();
				pRtpPacket->disableRtpExtension();

				if (bIsPacketSilent) {
					++m_nSilentPacketsSent;
					pRtpPacket->enableRtpMarker();
				} else {
					m_nSilentPacketsSent = 0;
					pRtpPacket->disableRtpMarker();
				}

				RtpHeader& header = pRtpPacket->getRtpHeader();

				//FEC
				unsigned char bFECdelay = 3;
				bool bFEC = false;
				bool bSkipData = false;

				if (!bMobile2G && !bMobile3G)
					bFEC =
							QualityStats::Get().GetPeerFractionalLossPercent() > MINIMUM_LOSS_PERCENT_TO_ENABLE_FEC ?
									true : false;
				else if (bMobile2G && !m_OutCall) {
					bFEC = true; //HACK! It is NOT a real FEC. On EDGE we are using FEC mechanism to save bandwidth by reducing IP+UDP+TURN+RTP headers overhead. We are sending only half of packets and the other half is delivered as FEC data
					bFECdelay = 1;
					bSkipData = (mEncodedCnt % 2) == 0;
				}

				if (bSendData) {
					//adding newly encoded packet to FEC queue
					unsigned short nBasePacketSize = sizeof(header)
							+ nPayloadSize;
					unsigned char* pBasePacket =
							new unsigned char[nBasePacketSize];
					memcpy(pBasePacket, &header, sizeof(RtpHeader));
					memcpy(&pBasePacket[sizeof(RtpHeader)],
							pRtpPacket->getDataPtr(), nPayloadSize);

					boost::shared_ptr<RTPPacket> pNewFECpacket(
							new RTPPacket(pBasePacket, nBasePacketSize));
					delete[] pBasePacket;
					m_sLastPackets.insert(m_sLastPackets.end(), pNewFECpacket);
				}

				//removing too old packets
				std::set<boost::shared_ptr<RTPPacket>, SmartRTPPacketComparer>::iterator FEC_iter;
				for (FEC_iter = m_sLastPackets.begin();
						FEC_iter != m_sLastPackets.end();) {
					if (IsSeqEarlier((*FEC_iter)->GetSeq(),
							nCurrentSeq - bFECdelay))
						m_sLastPackets.erase(FEC_iter++);
					else
						break;
				}

				unsigned char* pPacket = NULL;
				unsigned short nPacketSize = sizeof(header);
				if (!bSkipData) {
					//building final packet
					unsigned short nFECsize = 0;
					boost::shared_ptr<RTPPacket> pFECpacket;
					if (bFEC) {
						std::set<boost::shared_ptr<RTPPacket>,
								SmartRTPPacketComparer>::iterator FEC_packet_iter;
						FEC_packet_iter = m_sLastPackets.find(
								boost::shared_ptr<RTPPacket>(
										new RTPPacket(
												nCurrentSeq - bFECdelay)));

						if (FEC_packet_iter != m_sLastPackets.end()) {
							pFECpacket = *FEC_packet_iter;
							if (pFECpacket && pFECpacket->isValid()) {
								pRtpPacket->enableRtpExtension();
								nFECsize = 4
										+ (pFECpacket->GetDataSize() + 3) / 4
												* 4;
							}
						}
					}

					//building actual packet
					nPacketSize = sizeof(header) + nFECsize + nPayloadSize;
					pPacket = new unsigned char[nPacketSize];

					//header
					memcpy(pPacket, &header, sizeof(RtpHeader));

					//FEC part
					if (pFECpacket && pFECpacket->isValid()) {
						pPacket[sizeof(RtpHeader)] = QILEX_FEC_TYPE_RS21; //FEC data type ID
						pPacket[sizeof(RtpHeader) + 1] = bFECdelay; //Seq distance
						*(unsigned short*) (pPacket + sizeof(RtpHeader) + 2) =
								htons((nFECsize - 4) / 4);
						memcpy(pPacket + sizeof(RtpHeader) + 4,
								pFECpacket->GetDataPtr(), nFECsize - 4);
						int nPaddingLen = nFECsize - 4
								- pFECpacket->GetDataSize();
						memset(
								pPacket + sizeof(RtpHeader) + 4
										+ pFECpacket->GetDataSize(), 0,
								nPaddingLen); //32 bit padding
					}

					//regular data part
					if (bSendData)
						memcpy(pPacket + sizeof(RtpHeader) + nFECsize,
								pRtpPacket->getDataPtr(), nPayloadSize);
				} else {
					pPacket = new unsigned char[nPacketSize];
					memcpy(pPacket, &header, sizeof(RtpHeader));
				}

				boost::shared_ptr<IStreamTransport> pSender = m_pTrafficSender;
				if (pSender && (!bSkipData || bForceSend)) {
					if (m_pRTCP) {
						INetDispatch* pINetDispatch;
						IRTPDispatch* pIRTPDispatch;
						ISetSenderStatistics* pISetSenderStatistics;
						m_pRTCP->GetIRTCPConnection().GetDispatchInterfaces(
								&pINetDispatch, &pIRTPDispatch,
								&pISetSenderStatistics);
						if (pISetSenderStatistics) {
							pISetSenderStatistics->IncrementCounts(nPacketSize);
						}
						else
							assert(0);
					}
//					else
//						assert(0);

					pSender->SendData(pPacket, nPacketSize);
					m_nLastPacketSentTime = getTickCount();
				}
				mProcessedCnt++;

				delete[] pPacket;

#if 0 && _DEBUG
				FILE *pFile = fopen("e:\\test_ql.speex", "ab");
				DWORD size = payloadSize;
				fseek(pFile, 0, SEEK_END);
				fwrite(&size, 1, sizeof(size), pFile);
				fwrite(pRtpPacket->getDataPtr(), 1, payloadSize, pFile);
				fclose(pFile);
#endif
			}
		}
	}

	return TRUE;
}

unsigned long long OutQilexEncoder::getTickCount() {
	timeb stLocalTime;
	ftime(&stLocalTime);
	unsigned long nTimestamp = (unsigned long) (((__int64) stLocalTime.time
			* 1000 + (__int64) stLocalTime.millitm) & 0xffffffff);

	return nTimestamp;
}
