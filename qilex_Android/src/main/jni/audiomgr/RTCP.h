/*********************************/
/* RTCP.h                        */
/* Lukasz Rychter 2011/10/07     */
/*                               */
/* RTCP wrapper/handler          */
/*********************************/

#ifndef __RTCP_H__
#define __RTCP_H__

#include "IRTCP.h"

#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>

#include "rtcp/BaseClass.h"
#include "rtcp/INetworkRender.h"
#include "rtcp/IRTCPConnection.h"
#include "rtcp/IRTCPNotify.h"
#include "rtcp/NetworkChannel.h"
#include "rtcp/ISDESReport.h"
#include "IStreamTransport.h"

#include "../commonlib/GTimeWindowCounter.h"

class RTCPwrapper;

class RTCP_Handler : public IRTCPNotify, public CBaseClass
{
public:
	RTCP_Handler(RTCPwrapper *wrapper) : m_FractionalLossCounter(30000), m_TimerDifferenceCounter(120000), m_nLastRTT(0), parent_wrapper(wrapper) {}

protected:
	DECLARE_IBASE_M

	virtual unsigned long GetEventInterest() 
	{ 
		return RTCP_RR_RCVD | RTCP_SR_RCVD; 
	}

	virtual void ReceiverReportReceived(IGetReceiverStatistics *piGetReceiverStatistics,
						                IRTCPConnection        *piRTCPConnection=NULL,
									    IRTCPSession           *piRTCPSession=NULL);

	virtual void SenderReportReceived(IGetSenderStatistics *piGetSenderStatistics,
									  IRTCPConnection      *piRTCPConnection=NULL,
									  IRTCPSession         *piRTCPSession=NULL);

	virtual void RTCPReportingAlarm(IRTCPConnection  *piRTCPConnection=NULL,
                                    IRTCPSession     *piRTCPSession=NULL)
	{
		if (piRTCPConnection)
		{
			piRTCPConnection->GenerateRTCPReports();
		}
		else assert(0);
	}

	GTimeWindowCounter m_FractionalLossCounter;
	GTimeWindowCounter m_TimerDifferenceCounter;
	unsigned long long m_nLastRTT;

	RTCPwrapper* parent_wrapper;

};


class RTCP_Sender : public INetworkRender, public CBaseClass
{
public:
	RTCP_Sender(const boost::shared_ptr<IStreamTransport>& pSender)
	{
		m_pSender = pSender;
	}

protected:
	DECLARE_IBASE_M

	virtual bool Initialize() 
	{
		return true;
	}

	virtual bool Connect(unsigned char *puchIPAddress, unsigned short usPortID)
	{
		return true;
	}

	virtual bool Close()
	{
		return true;
	}

	virtual unsigned int GetSocket()
	{
		return 0;
	}

	virtual unsigned short GetPort()
	{
		return 0;
	}

	virtual bool IsOpened()
	{
		return true;
	}

	virtual bool IsConnected()
	{
		return true;
	}

	virtual int Send(unsigned char* puchDataBuffer, unsigned long ulBytesToSend);

protected:
	boost::shared_ptr<IStreamTransport> m_pSender;
};




class RTCPwrapper : public IRTCPwrapper
{
protected:

public:
	RTCPwrapper(unsigned long nSSRC, const boost::shared_ptr<IStreamTransport>& pSender);
	~RTCPwrapper();

	IRTCPConnection& GetIRTCPConnection() const
	{
		return *m_pRTCPConnection;
	}

	boost::signals2::signal<void (float seconds, float jitter)> onJitterStats;
	boost::signals2::signal<void (float seconds, float rtt)> onRTTStats;
	boost::signals2::signal<void (float seconds, float loss_percent)> onLossPercentData;

protected:
	boost::shared_ptr<IStreamTransport> m_pSender;

	IRTCPConnection*	m_pRTCPConnection;

	std::unique_ptr<RTCP_Handler>		m_pRTCP_Handler;
	std::unique_ptr<RTCP_Sender>		m_pRTCP_Sender;

	ISDESReport*		m_pSDES;

	unsigned long m_nSSRC;


};

#endif
