#ifndef _AUDIO_MGR_EVENT_
#define _AUDIO_MGR_EVENT_

#include "RefCntBuffer.h"
#include "zevent.h"
#include "CodecDescriptor.h"

class AudioMgrEvent : public ZAsyncEvent
{
public:
	static const int MINVAL                     = 0;
	static const int StartProcessing            = 1;
	static const int StopProcessing             = 2;
	static const int Shutdown                   = 3;
	static const int AudioMgrError              = 4; // error response event
	static const int NotifyContextInitialized   = 5;
	static const int NotifyContextShutdown      = 6;
	static const int SetInputState              = 7; // bBoolData - mute audio input device
	static const int QueryInputDevices          = 8;
	static const int QueryOutputDevices         = 9;
	static const int QueryInputDevicesResponse  = 10;
	static const int QueryOutputDevicesResponse = 11;
	static const int NewInputDeviceNotify		= 12;
	static const int NewOutputDeviceNotify		= 13;
	static const int RemovedInputDeviceNotify	= 14; // m_bWasUsed - device was selected
	static const int RemovedOutputDeviceNotify	= 15; // ^
	static const int SelectInputDevice          = 16;
	static const int SelectOutputDevice         = 17;
	static const int RestartProcessing          = 18;
	static const int EnableLoopbackMode         = 19; // enables client in loopback mode ( dwData | m_bBoolData )
	static const int LoopbackData               = 20;
	static const int OutputAudioConfigChanged   = 21; // m_bSpeakerOn, m_bHeadphonesOn
	static const int OutputVolumeChanged        = 22; // m_fData - 0.0 - 1.0. 0.0 means lowest level NOT muted!
	static const int MAXVAL                     = LoopbackData;

public:

	virtual ~AudioMgrEvent(void)
	{
	}

	const ZEventType::EventType GetType() const
	{
		return ZEventType::Custom;
	}

	const bool ValidateSubtypeId(const int nSubtypeId) const
	{
		if (nSubtypeId < MINVAL || nSubtypeId > MAXVAL)
			return false;
		return true;
	}

	bool m_bBoolData;
	float m_fData;
	int m_dwData;
	int m_dwErrorCode;
	SDPCodecDescriptor m_spdCodecSelected;
	boost::shared_ptr<RefCntBuffer> m_pBuffer;
	bool m_bWasUsed;
	bool m_bSpeakerOn;
	bool m_bHeadphonesOn;

public:

	static boost::shared_ptr<AudioMgrEvent> CreateStartProcessing()
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::StartProcessing));
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateStopProcessing()
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::StopProcessing));
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateShutdown()
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::Shutdown));
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateAudioError(int dwCode)
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::AudioMgrError));
		ret->m_dwErrorCode = dwCode;
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateSetInputState(bool bMute)
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::SetInputState));
		ret->m_bBoolData = bMute;
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateRestartProcessing()
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::RestartProcessing));
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateEnableLoopbackMode(bool bEnable, int dwDelay)
	{
		AudioMgrEvent* pObj = new AudioMgrEvent(AudioMgrEvent::EnableLoopbackMode);
		pObj->m_bBoolData = bEnable;
		pObj->m_dwData = dwDelay;
		boost::shared_ptr<AudioMgrEvent> ret( pObj );
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateLoopbackData(unsigned char* pData, int size)
	{
		AudioMgrEvent* pObj = new AudioMgrEvent(AudioMgrEvent::LoopbackData);
		pObj->m_pBuffer.reset( new RefCntBuffer(pData, size) );
		boost::shared_ptr<AudioMgrEvent> ret( pObj );
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateOutputAudioConfigChanged(bool bSpeakerOn, bool bHeadphonesOn)
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::OutputAudioConfigChanged));
		ret->m_bSpeakerOn = bSpeakerOn;
		ret->m_bHeadphonesOn = bHeadphonesOn;
		return ret;
	}

	static boost::shared_ptr<AudioMgrEvent> CreateOutputVolumeChanged(float fVolume)
	{
		boost::shared_ptr<AudioMgrEvent> ret(new AudioMgrEvent(AudioMgrEvent::OutputVolumeChanged));
		ret->m_fData = fVolume;
		return ret;
	}

protected:
	AudioMgrEvent(int nSubType) : ZAsyncEvent(nSubType)
	{
	};


};

#endif
