// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef QILEX_MIC_TASK_HXX
#define QILEX_MIC_TASK_HXX

#include <os/OsTask.h>
#include "../resiprocate/rutil/RecursiveMutex.hxx"

class MpidQilexAndroid;

class QilexMicTask : public OsTask
{
public:
	QilexMicTask(const UtlString& name, void* pArg, const int priority = DEF_PRIO, const int options = DEF_OPTIONS, const int stackSize = DEF_STACKSIZE);
	MpidQilexAndroid* getidQilexAndroid() { return idQilexAndroid; }
	void setidQilexAndroid(MpidQilexAndroid* mpid) { idQilexAndroid = mpid; }
	void startStream();
	void stopStream();
	bool isWorking() { return working; }

protected:
	virtual int run(void* pArg);
	MpidQilexAndroid* idQilexAndroid;
	bool streamDone;
	bool working;
	resip::RecursiveMutex mutex;
};

#endif // QILEX_MIC_TASK_HXX
