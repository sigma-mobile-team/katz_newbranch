#ifndef __DATAQUEUE__
#define __DATAQUEUE__

#include <list>

template<typename T> 
class DataQueue
{
public:
	void AddItem(const T& task)
	{
		/*rede
		GAutoLock sync(&m_csSync);
		m_lItems.push_back(task);
		*/
	}

	bool IsEmpty()
	{
		/*rede
		GAutoLock sync(&m_csSync);
		return m_lItems.empty();
		*/
		return false;
	}

	T GetItem()
	{
		/*rede
		GAutoLock sync(&m_csSync);
		T val = m_lItems.front();
		m_lItems.pop_front();
		return val;
		*/
		T val;
		return val;
	}

	unsigned int GetSize()
	{
		/*rede
		GAutoLock sync(&m_csSync);
		return m_lItems.size();
		*/
		return 0;
	}

private:
	//rede GCriticalSection m_csSync;
	std::list<T> m_lItems;
};

#endif
