#ifndef _OutputSpeexEncoder_h_
#define _OutputSpeexEncoder_h_

#include "utl/UtlString.h"
#include "mp/MpFlowGraphMsg.h"
#include "mp/MpAudioResource.h"
#include <mp/MpCodecFactory.h>

#include "IStreamTransport.h"
#include "StreamTransportEvent.h"

#include "DataQueue.h"
#include "RefCntBuffer.h"

#include "RTCP.h"
#include "RTPPacket.h"

#include <set>

class mpFlowGraphMsg;

// @speex voice encoding processing filter
class OutQilexEncoder : public MpAudioResource
{
public:
	OutQilexEncoder(const UtlString& rName, unsigned long nSSRC, const boost::shared_ptr<IRTCPwrapper>& pRTCP,bool outCall);
	virtual	~OutQilexEncoder();

public:
	/// @brief : setup one codec factory for all coders / decoders components
	void SetupCodecFactory(MpCodecFactory* pCodecFactory);

	/// @enables loopback Mode
	void EnableLoopbackMode(bool bEnable);
	void AddLoopbackData(boost::shared_ptr<RefCntBuffer>& pBuffer, unsigned int nLoopbackDelay=0);

	/// @brief : initialize encoder
	bool Initialize(const std::string& strCoderType, unsigned short nSampleRate);

	/// Returns the count of the number of frames processed by this resource.
	int numFramesProcessed(void);

	void AttachTrafficSender(boost::shared_ptr<IStreamTransport> pSender);
	void DetachTrafficSender();
	void SetupPayloadType(uint8_t type);

	void SetBitrate(unsigned bitrate);
	int EncoderCtl(int request, int value);

private:
	/// Processes the next frame interval's worth of media.
	UtlBoolean doProcessFrame(
		MpBufPtr inBufs[], MpBufPtr outBufs[],
		int inBufsSize, int outBufsSize,
		UtlBoolean isEnabled, int samplesPerFrame = 80,
		int samplesPerSecond = 8000);

	/// Copy constructor (not implemented for this class)
	OutQilexEncoder(const OutQilexEncoder& rMpTestResource);

	/// Assignment operator (not implemented for this class)
	OutQilexEncoder& operator=(const OutQilexEncoder& rhs);

	unsigned long long getTickCount();

private:
	MpEncoderBase*				mpEncoder;
	int							mcodecFrameSamples;
	boost::shared_ptr<IStreamTransport> m_pTrafficSender;
	uint8_t						m_PayloadType;
	unsigned long				m_nSSRC;
	boost::shared_ptr<IRTCPwrapper>	m_pRTCP;
	unsigned long				m_nLastPacketSentTime;
	unsigned long				m_nSilentPacketsSent;
	float						m_fScalingFactor;
	unsigned long				m_nInactivityStartTime;
	unsigned long 				m_nCurrentBitrate;
	bool m_OutCall;
	std::set<boost::shared_ptr<RTPPacket>, SmartRTPPacketComparer>	m_sLastPackets;
	
	bool m_bLoopback;
	DataQueue< boost::shared_ptr<RefCntBuffer> > m_qLoopbackQueue;

protected:
	unsigned long long            mProcessedCnt;      ///< Number of processed frames
	unsigned long long			  mEncodedCnt;	      ///< Number of encoded frames
	int            				  mMsgCnt;            ///< Number received messages
	
};

#endif
