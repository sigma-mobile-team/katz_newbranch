// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef _QILEXANDROIDAUDIOWRAPPER_HXX
#define _QILEXANDROIDAUDIOWRAPPER_HXX

#include <jni.h>

class QilexAndroidAudioWrapper
{
public:
	static QilexAndroidAudioWrapper* getQilexAndroidAudioWrapper();
	static int attachJVM(JNIEnv **env);
	static void detachJVM(int stat);

	void createAudioTrackObject(JNIEnv *env);
	void deleteAudioTrackObject(JNIEnv *env);

	void createAudioRecordObject(JNIEnv *env);
	void deleteAudioRecordObject(JNIEnv *env);

	void createAudioTrackMethodsStubs(JNIEnv *env);
	void createAudioRecordMethodsStubs(JNIEnv *env);

	void audiotrack_play(JNIEnv *env);
	void audiotrack_stop(JNIEnv *env);
	int audiotrack_write(JNIEnv *env, jbyteArray audio_buffer, int offset, int size);
	int audiotrack_write(JNIEnv *env, jshortArray audio_buffer, int offset, int size);
	int audiotrack_getPlaybackHeadPosition(JNIEnv *env);
	void audiotrack_release(JNIEnv *env);

	void audiorecord_startRecording(JNIEnv *env);
	void audiorecord_stop(JNIEnv *env);
	int audiorecord_read(JNIEnv *env, jbyteArray audio_buffer, int offset, int size);
	int audiorecord_read(JNIEnv *env, jshortArray audio_buffer, int offset, int size);
	void audiorecord_release(JNIEnv *env);

	void enableInternalAEC(JNIEnv *env);
	void releaseInternalAEC(JNIEnv *env);

	int getAudioPriority(JNIEnv *env);
	void setAudioPriority(JNIEnv *env);

	int getAudioTrackBufferLen() { return audiotrack_minBufSize; }
	int getAudioRecordBufferLen() { return audiorecord_minBufSize; }

	int getAudioRecordNativeSamplingRate() { return audiorecord_nativeSamplingRate; }

	int getAudioTrackNativeSamplingRate() { return audiotrack_nativeSamplingRate; }

	int getDeviceLatencyMs();


	//device params
	void setAvailableConnectivity(bool bWiFi, bool bMobile3G, bool bMobile2G, bool bLTE);

	bool isWiFi() { return m_bWiFi; }
	bool isMobile3G() { return m_bMobile3G; }
	bool isMobile2G() { return m_bMobile2G; }
	bool isLTE() { return m_bLTE; }

	void setDevicePerformance(float fPerformance, bool bLowLatencyAudio, int nAndroidVersion)
	{
		if (fPerformance == -1)
			fPerformance = 1.0;

		m_fDevicePerformance = fPerformance;
		m_bLowLatencyAudio = bLowLatencyAudio;

		m_nAndroidVersion = nAndroidVersion;
	}

	float devicePerformance() { return m_fDevicePerformance; }
	int getAndroidVersion() { return m_nAndroidVersion; }

protected:
	static QilexAndroidAudioWrapper* instance;

	QilexAndroidAudioWrapper();

	//****java stubs*****

	//audiotrack
	jobject audiotrack;
	jclass audiotrack_class;
	jmethodID audiotrack_write_sh;
	jmethodID audiotrack_write_b;
	jmethodID audiotrack_getPlaybackHeadPosition_m;
	jmethodID audiotrack_play_m;
	jmethodID audiotrack_stop_m;
	jmethodID audiotrack_release_m;
	int audiotrack_minBufSize;
	int audiotrack_bufSize;
	int audiotrack_nativeSamplingRate;

	//audiorecord
	jobject audiorecord;
	jclass audiorecord_class;
	jmethodID audiorecord_startRecording_m;
	jmethodID audiorecord_stop_m;
	jmethodID audiorecord_read_b;
	jmethodID audiorecord_read_sh;
	jmethodID audiorecord_release_m;
	int audiorecord_minBufSize;
	int audiorecord_bufSize;
	int audiorecord_nativeSamplingRate;

	//Audio effects
	jobject aec_object;
	jclass aec_class;


	//device params
	bool m_bWiFi;
	bool m_bMobile3G;
	bool m_bMobile2G;
	bool m_bLTE;

	float m_fDevicePerformance; //0.0 - 1.0
	bool m_bLowLatencyAudio;

	int m_nAndroidVersion;
};

#endif // _QILEXANDROIDAUDIOWRAPPER_HXX
