#include "InQilexDecoder.h"
#include <sys/timeb.h>

#include "mp/MpCodecFactory.h"

#include "utl/UtlString.h"
#include "mp/MpFlowGraphMsg.h"
#include "mp/MpAudioResource.h"
#include <mp/MpIntResourceMsg.h>
#include <mp/MprnIntMsg.h>
#include "mp/MpCodecFactory.h"
#include "mp/dsplib.h"

#include "AudioMgr.h"
#include "QualityStats.h"
#include "QilexDejitter.h"
#include "QilexConfig.hxx"
#include "RTPPacket.h"

#include "rtcp/RTPHeader.h"
#include "RTCP.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include "QilexAndroidAudioWrapper.h"
#include "QilexLogger.hxx"

InQilexDecoder::InQilexDecoder(const UtlString& rName, unsigned long nSSRC, const boost::shared_ptr<IRTCPwrapper>& pRTCP) :
	MpAudioResource(rName, 0, 0, 1, 1),
	m_nCodecFrameSamples(0),
	m_nProcessedCnt(0),
	m_pDecoder(NULL),
	m_bLoopback(false),
	m_pSender(NULL),
	m_bCNG(true)
{
	m_nSSRC = nSSRC;
	m_pRTCP = pRTCP;
}

InQilexDecoder::~InQilexDecoder()
{
	if(m_pDecoder)
	{
		m_pDecoder->freeDecode();
		delete m_pDecoder;
		m_pDecoder = NULL;
	}
}

void InQilexDecoder::EnableLoopbackMode(bool bEnable)
{
	m_bLoopback = bEnable;
}

OsStatus InQilexDecoder::SetCNGEnabled(const UtlString& namedResource,
                                       OsMsgQ& fgQ,
                                       UtlBoolean enabled)
{
   MpResourceMsg msg(enabled ? (MpResourceMsg::MpResourceMsgType)MPRM_ENABLE_CNG :
                     	 	   (MpResourceMsg::MpResourceMsgType)MPRM_DISABLE_CNG,
                     namedResource);
   return fgQ.send(msg);
}

/*rede
void InQilexDecoder::SetupParent(ZEventListener* pSender)
{
	//rede m_pSender = pSender;
}
*/

bool InQilexDecoder::Initialize(const std::string& strCoderType, unsigned short nSampleRate)
{
	MpCodecFactory *mpCodecFactory = MpCodecFactory::getMpCodecFactory();
	if(!mpCodecFactory)
		return false;

	const MppCodecInfoV1_1 **pCodecInfo;
	unsigned         codecInfoNum;

	mpCodecFactory->getCodecInfoArray(codecInfoNum, pCodecInfo);

	for (unsigned i=0; i<codecInfoNum; i++)
	{
		const MppCodecInfoV1_1* pCodec = pCodecInfo[i];
		if (pCodec && pCodec->mimeSubtype
				&& strcasecmp(pCodec->mimeSubtype, strCoderType.c_str()) == 0
				&& pCodec->sampleRate == nSampleRate)
		{
			const char **pCodecFmtps = pCodecInfo[i]->fmtps;
			const char* strFmtps = NULL;

			if ( pCodecFmtps )
				strFmtps = pCodecFmtps[0];

			OsStatus result = mpCodecFactory->createDecoder(
				pCodec->mimeSubtype,
				strFmtps,
				pCodec->sampleRate,
				pCodec->numChannels,
				0, m_pDecoder);
			if(result != OS_SUCCESS || !m_pDecoder)
			{
				//! log
				assert(0);
				return false;
			}

			result = m_pDecoder->initDecode();
			if(result != OS_SUCCESS)
			{
				//! log
				assert(0);
				if(m_pDecoder)
				{
					delete m_pDecoder;
					m_pDecoder = NULL;
				}
				return false;
			}

			break;
		}
	}

	if(!m_pDecoder)
	{
		//! log
	    // TODO
		//assert(0);
		return false;
	}

	init_CNG();

	m_nCodecFrameSamples = m_pDecoder->getInfo()->getNumSamplesPerFrame();

	return true;
}

void InQilexDecoder::AddBufferToDecode(unsigned const char* pData, unsigned int nDataSize)
{
	if(pData && nDataSize>0)
	{
		const unsigned char nRTPHeaderSize = 12;

		CRTPHeader RTPheader(m_nSSRC, (unsigned char*)pData, nDataSize);
		bool isFEC = RTPheader.GetExtension() > 0;

		boost::shared_ptr<RTPPacket> pOriginalPacket(new RTPPacket(pData, nDataSize));
		boost::shared_ptr<RTPPacket> pFECPacket;

		if (isFEC)
		{
			//extracting original packet and FEC packet

			unsigned char* pPayloadData = (unsigned char*)pOriginalPacket->GetDataPtr();
			unsigned short nPayloadSize = pOriginalPacket->GetDataSize();

			unsigned char nFEC_Type = pPayloadData[0];
			if (nFEC_Type == QILEX_FEC_TYPE_RS21)
			{
				unsigned char nFEC_Distance = pPayloadData[1];
				unsigned short nFEC_Size = ntohs(*(unsigned short*)(pPayloadData + 2)) * 4;

				unsigned char* pFECPacketData = new unsigned char[nRTPHeaderSize + nFEC_Size];
				memcpy(pFECPacketData, pData, nRTPHeaderSize);
				memcpy(pFECPacketData+nRTPHeaderSize, pPayloadData+4, nFEC_Size);
				*(unsigned short*)(pFECPacketData + 2) = ntohs(pOriginalPacket->GetSeq() - nFEC_Distance);
				pFECPacket.reset( new RTPPacket(pFECPacketData, nRTPHeaderSize + nFEC_Size, nFEC_Distance));
				delete[] pFECPacketData;

				unsigned short nOriginalDataSize = nPayloadSize-nFEC_Size-4;
				unsigned char* pOriginalPacketData = new unsigned char[nRTPHeaderSize + nOriginalDataSize];
				memcpy(pOriginalPacketData, pData, nRTPHeaderSize);
				memcpy(pOriginalPacketData+nRTPHeaderSize, pPayloadData+4+nFEC_Size, nOriginalDataSize);
				pOriginalPacket.reset( new RTPPacket(pOriginalPacketData, nRTPHeaderSize + nOriginalDataSize));
				delete[] pOriginalPacketData;
			}
		}



		if (m_pRTCP)
		{
			INetDispatch* pINetDispatch;
			IRTPDispatch* pIRTPDispatch;
			ISetSenderStatistics* pISetSenderStatistics;
			m_pRTCP->GetIRTCPConnection().GetDispatchInterfaces(&pINetDispatch, &pIRTPDispatch, &pISetSenderStatistics);
			if (pIRTPDispatch)
			{
				timeb stLocalTime;
				ftime(&stLocalTime);
				unsigned long nTimestamp = (unsigned long)(((__int64)stLocalTime.time*1000 + (__int64)stLocalTime.millitm) & 0xffffffff);

				RTPheader.SetRecvTimestamp(nTimestamp);
				pIRTPDispatch->ForwardRTPHeader(&RTPheader);
			}
			else assert(0);
		}
//		else assert(0);

		m_Dejitter.pushPacket(pOriginalPacket);

		if (pFECPacket)
			m_Dejitter.pushPacket(pFECPacket);
	}
}

UtlBoolean InQilexDecoder::handleMessage(MpResourceMsg& rMsg)
{
	UtlBoolean msgHandled = FALSE;

	switch (rMsg.getMsg())
	{
		case MPRM_ENABLE_CNG:
	    {
	    	SIPXLOGI("Enable CNG");
	        MpIntResourceMsg *pMsg = (MpIntResourceMsg*)&rMsg;
	        m_bCNG = true;
	        msgHandled = TRUE;
	    }
	    break;
		case MPRM_DISABLE_CNG:
	   	{
	   		SIPXLOGI("Disable CNG");
	   		MpIntResourceMsg *pMsg = (MpIntResourceMsg*)&rMsg;
	   	    m_bCNG = false;
	   	    msgHandled = TRUE;
	   	}
	   	break;

	    default:
	    	msgHandled = MpResource::handleMessage(rMsg);
	    	break;
	}

	return msgHandled;
}

UtlBoolean InQilexDecoder::doProcessFrame(
	MpBufPtr inBufs[], MpBufPtr outBufs[],
	int inBufsSize, int outBufsSize,
	UtlBoolean isEnabled, int samplesPerFrame,
	int samplesPerSecond)
{
	if (m_pDecoder && isOutputConnected(0) && isEnabled)
	{
			for (int i=0; i < outBufsSize; i++)
			{
				outBufs[i].release();

				if(!outBufs[i].isValid())
				{
					assert(GetMpMisc().RawAudioPool != NULL);
					MpAudioBufPtr pAudio = GetMpMisc().RawAudioPool->getBuffer();
					MpAudioSample* pAudioSample = pAudio->getSamplesWritePtr();


					int nDecodedSamplesNum = 0;
					bool bSamplesDecoded = false;

					// file transport
#if 0 && _DEBUG
					nDecodedSamplesNum = samplesPerFrame;
					if(mProcessedCnt>20)
					{
						if(!pFile)
							pFile = fopen("e:\\test_ql.speex", "rb");
						DWORD size = 0;
						fread(&size, 1, sizeof(size), pFile);
						fread(pRtpPayloadPtr, 1, size, pFile);
						pRtpPacket->setPayloadSize(size);
						nDecodedSamplesNum = mpDecoder->decode(pRtpPacket, samplesPerFrame, pAudioSample);
						bSamplesDecoded = true;
					}
#endif 


					QualityStats::Get().SetPacketsInJitterBufferNum(m_Dejitter.GetPacketsCount());

					boost::shared_ptr<RTPPacket> pRtpPacket = m_Dejitter.pullPacket();

					if (pRtpPacket && pRtpPacket->isValid() && pRtpPacket->GetDataSize() > 0)
					{
						MpRtpBufPtr pMpRtpPacket = GetMpMisc().RtpPool->getBuffer();
						unsigned char* pMpRtpDataPtr = (unsigned char*)pMpRtpPacket->getDataWritePtr();
						memcpy(pMpRtpDataPtr, pRtpPacket->GetDataPtr(), pRtpPacket->GetDataSize());
						pMpRtpPacket->setPayloadSize(pRtpPacket->GetDataSize());

						nDecodedSamplesNum = m_pDecoder->decode(pMpRtpPacket, samplesPerFrame, pAudioSample);

						if (m_bCNG)
							comfort_noise_generator(pAudioSample, samplesPerFrame, 20000, true);
					}
					else if (!pRtpPacket && m_pDecoder->getInfo()->haveInternalPLC())
					{
						//PLC
						nDecodedSamplesNum = m_pDecoder->decode(MpRtpBufPtr(), samplesPerFrame, pAudioSample);

						if (m_bCNG)
							comfort_noise_generator(pAudioSample, samplesPerFrame, 20000, true);
					}
					else
					{
						pAudio->setSpeechType(MP_SPEECH_COMFORT_NOISE);
						if (m_bCNG)
						{
							nDecodedSamplesNum = samplesPerFrame;
							comfort_noise_generator(pAudioSample, samplesPerFrame, 20000, false);
						}
					}

					if(nDecodedSamplesNum>0)
					{
						bSamplesDecoded = true;
					}


					if (pRtpPacket && pRtpPacket->isValid())
					{
						int nRemoteTimeDiff = QualityStats::Get().GetRemoteTimerDifference();
						if (nRemoteTimeDiff != INVALID_TIMER_DIFFERENCE)
						{
//							QilexAndroidAudioWrapper* audiowrapper = QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper();
							timeb stLocalTime;
							ftime(&stLocalTime);
							long nLocalTimestamp = (long)((__int64)(stLocalTime.time & 0xffffffff)*1000 + (__int64)stLocalTime.millitm);
							long nCurrentRemoteTimestamp = nLocalTimestamp + nRemoteTimeDiff;

							long nReceivedPacketTimestamp = pRtpPacket->GetTimestamp();
							long audioBufferSize = QualityStats::Get().GetSamplesInDriverBufNum();
							long nAudioBufLatency = (audioBufferSize * 1000)/QilexConfig::getSamplesPerSecond();
							long nCurrentLatency = nCurrentRemoteTimestamp - nReceivedPacketTimestamp + nAudioBufLatency;
							QualityStats::Get().SetCurrentLatency((unsigned short)nCurrentLatency);
//							SIPXLOGE("nCurrentRemoteTimestamp = %ld",nCurrentRemoteTimestamp);
//							SIPXLOGE("nReceivedPacketTimestamp = %ld",nReceivedPacketTimestamp);
//							SIPXLOGE("audioBufferSize = %ld",audioBufferSize);
//							SIPXLOGE("nAudioBufLatency = %ld",nAudioBufLatency);
//							SIPXLOGE("nCurrentLatency = %ld",nCurrentLatency);
						}
					}
					else if (!pRtpPacket)
						QualityStats::Get().AddMissingFrame();

					if(!bSamplesDecoded)
					{
						memset(pAudioSample, 0, pAudio->getSamplesNumber()*2);
						nDecodedSamplesNum = samplesPerFrame;
					}

					/*AKA
					if(m_bLoopback && bSamplesDecoded)
					{
						if (m_pSender)
						{
							boost::shared_ptr<AudioMgrEvent> pEvent = AudioMgrEvent::CreateLoopbackData((unsigned char*)pAudioSample, nDecodedSamplesNum*2);
							m_pSender->OnEvent(pEvent);
						}
						memset(pAudioSample, 0, pAudio->getSamplesNumber()*2);
					}
					*/

					// sinusoidal wave gen
#if 0
					for(int n =0;n<samplesPerFrame;n++)
					{
						pAudioSample[n] = (int16_t)(sinf( n * 2 * 3.14f/(float)samplesPerFrame * 10 ) * 30000.0f );
					}
					nDecodedSamplesNum = samplesPerFrame;
#endif

					outBufs[i] = pAudio;
				}
				else
				{
					assert(false);
				}
			}
	}

	m_nProcessedCnt++;
	return TRUE;
}
