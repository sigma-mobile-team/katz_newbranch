/*                               */
/* module gathering audio        */
/* quality relatd informations   */
/*********************************/

#include "QualityStats.h"

#include "rutil/Subsystem.hxx"
#include "rutil/Logger.hxx"
#include "QilexLogger.hxx"
#include <sstream>

//#define QUALITY_LOG

#define RESIPROCATE_SUBSYSTEM resip::Subsystem::APP

using namespace std;

QualityStats* QualityStats::m_pObject = NULL;
resip::RecursiveMutex QualityStats::m_csAccess;

void QualityStats::InitializeInstance() {
	m_csAccess.lock();
	if (!m_pObject) {
		try {
			m_pObject = new QualityStats;
#ifdef _WIN32
			char strEncodedPointer[9];
			for (int i=0; i<8; ++i)
			strEncodedPointer[i] = (char)(((int)m_pObject & (0xf << (28-i*4))) >> (28-i*4)) + 64;
			strEncodedPointer[8] = 0;
			SetEnvironmentVariableA("Qilex_QualityStats", strEncodedPointer);
#endif
		} catch (...) {
		}
	}
	m_csAccess.unlock();
}

void QualityStats::DeinitializeInstance() {
	m_csAccess.lock();
	delete m_pObject;
	m_pObject = NULL;
#ifdef _WIN32
	SetEnvironmentVariableA("Qilex_QualityStats", NULL);
#endif
	m_csAccess.unlock();
}

QualityStats& QualityStats::Get() {
	if (m_pObject)
		return *m_pObject;
	else {

#ifdef _WIN32
		char strEncodedPointer[9];
		DWORD ret = GetEnvironmentVariableA("Qilex_QualityStats", strEncodedPointer, 9);

		if (ret == 8)
		{
			int nVoidPtr=0;
			for (int i=0; i<8; ++i)
			nVoidPtr |= (strEncodedPointer[i]-64) << (28 - i*4);

			m_pObject = (QualityStats*)nVoidPtr;
		}
#endif

#ifdef ANDROID
		InitializeInstance();
#endif

		if (m_pObject)
			return *m_pObject;
		else
			return *((QualityStats*) NULL);
	}
}

QualityStats::QualityStats() {
	ResetStats();
	m_nJitterBufferSize = 0;
}

void QualityStats::ResetStats() {
	m_nReceivedPacketsNum = 0;
	m_nLostPacketsNum = 0;
	m_nRecoveredPacketsNum = 0;
	m_nLatePacketsNum = 0;
	m_nMissingFramesNum = 0;
	m_nDiscardedPacketsNum = 0;
	m_nInsertedNullPacketsNum = 0;
	m_nPacketsInJitterBufferNum = 0;

	m_nSamplesInDriverBufNum = 0;
	m_nLateAudioFramesNum = 0;

	m_fPeerFractionalLossPercent = 0;
	m_nJitter = 0;
	m_nRTT = 0;

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::LOST_PACKET));
	pEvent->m_nVal = m_nLostPacketsNum;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::RECOVERED_PACKET));
	pEvent->m_nVal = m_nRecoveredPacketsNum;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::LATE_PACKET));
	pEvent->m_nVal = m_nLatePacketsNum;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::MISSING_FRAME));
	pEvent->m_nVal = m_nMissingFramesNum;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::DISCARDED_PACKET));
	pEvent->m_nVal = m_nDiscardedPacketsNum;
	DispatchEvent(pEvent);

	pEvent.reset(
			new QualityStatsEvent(QualityStatsEvent::INSERTED_NULL_PACKET));
	pEvent->m_nVal = m_nInsertedNullPacketsNum;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::LATE_AUDIO_FRAME));
	pEvent->m_nVal = m_nLateAudioFramesNum;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::JITTER_CHANGED));
	pEvent->m_nVal = m_nJitter;
	DispatchEvent(pEvent);

	pEvent.reset(new QualityStatsEvent(QualityStatsEvent::RTT_CHANGED));
	pEvent->m_nVal = m_nRTT;
	DispatchEvent(pEvent);

	pEvent.reset(
			new QualityStatsEvent(
					QualityStatsEvent::PEER_FRACTIONAL_LOSS_CHANGED));
	pEvent->m_fVal = m_fPeerFractionalLossPercent;
	DispatchEvent(pEvent);
}

void QualityStats::WriteStatsToLog() {
	unsigned int nCurrentLatency = GetCurrentLatency();
	unsigned int nLateAudioFramesNum = GetLateAudioFramesNum();
	unsigned int nReallyMissingFramesNum = GetReallyMissingFramesNum();
	unsigned int nReceivedPacketsNum = GetReceivedPacketsNum();
	unsigned int nDiscardedPacketsNum = GetDiscardedPacketsNum();
	unsigned int nInsertedNullPacketsNum = GetInsertedNullPacketsNum();
	unsigned int nLatePacketsNum = GetLatePacketsNum();
	unsigned int nLostPacketsNum = GetLostPacketsNum();
	unsigned int nRecoveredPacketsNum = GetRecoveredPacketsNum();
	unsigned int nJitterBufferSize = GetJitterBufferSize();
	float fJitterBufferUsage = GetJitterBufferUsage() * 100;

	std::stringstream ss;
	ss << "QUALITY STATS:" << std::endl << " Latency(in):" << nCurrentLatency
			<< "ms " << "  Jitter buffer(" << nJitterBufferSize * 20 << "ms): "
			<< (int) (fJitterBufferUsage + 0.5f) << "%" << std::endl
			<< " Late audio frames:" << nLateAudioFramesNum
			<< "  Missing frames:" << nReallyMissingFramesNum << std::endl
			<< " Packets:" << std::endl << "\t" << "Received:"
			<< nReceivedPacketsNum << "  Discarded:" << nDiscardedPacketsNum
			<< "  Inserted empty:" << nInsertedNullPacketsNum << std::endl
			<< "\t" << "Late:" << nLatePacketsNum << "  Lost:"
			<< nLostPacketsNum << "  Recovered:" << nRecoveredPacketsNum
			<< std::endl;
	SIPXLOGI(" \r\n");
	SIPXLOGI("%s", ss.str().c_str());
}

// GET
unsigned int QualityStats::GetReceivedPacketsNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nReceivedPacketsNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetLostPacketsNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nLostPacketsNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetRecoveredPacketsNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nRecoveredPacketsNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetLatePacketsNum() //newer packet was already played, so this one was discarded
{
	int nRet;
	m_csAccess.lock();
	nRet = m_nLatePacketsNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetMissingFramesNum() //it was time for decoder to fetch next packet but there wasn't any in OR there was an inserted NULL packet
{
	int nRet;
	m_csAccess.lock();
	nRet = m_nMissingFramesNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetReallyMissingFramesNum() //it was time for decoder to fetch next packet but there wasn't any in buffer (doesn't count inserted NULL packets)
{
	int nRet;
	m_csAccess.lock();
	nRet = m_nMissingFramesNum - m_nInsertedNullPacketsNum - m_nLostPacketsNum;
	if (nRet < 0) //cause there might be a slighty desync in those stats
		nRet = 0;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetDiscardedPacketsNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nDiscardedPacketsNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetInsertedNullPacketsNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nInsertedNullPacketsNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetJitterBufferSize() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nJitterBufferSize;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetPacketsInJitterBufferNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nPacketsInJitterBufferNum;
	m_csAccess.unlock();
	return nRet;
}

float QualityStats::GetJitterBufferUsage() //percent of jitter buffer currently filled with packets
{
	float fRet;
	m_csAccess.lock();
	fRet = m_nPacketsInJitterBufferNum / (float) m_nJitterBufferSize;
	m_csAccess.unlock();
	return fRet;
}

unsigned int QualityStats::GetSamplesInDriverBufNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nSamplesInDriverBufNum;
	m_csAccess.unlock();
	return nRet;
}

unsigned int QualityStats::GetLateAudioFramesNum() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nLateAudioFramesNum;
	m_csAccess.unlock();
	return nRet;
}

float QualityStats::GetPeerFractionalLossPercent() {
	float fRet;
	m_csAccess.lock();
	fRet = m_fPeerFractionalLossPercent;
	m_csAccess.unlock();
	return fRet;
}

unsigned short QualityStats::GetJitter() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nJitter;
	m_csAccess.unlock();
	return nRet;
}

unsigned short QualityStats::GetRTT() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nRTT;
	m_csAccess.unlock();
	return nRet;
}

int QualityStats::GetRemoteTimerDifference() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nRemoteTimerDifference;
	m_csAccess.unlock();
	return nRet;
}

unsigned short QualityStats::GetCurrentLatency() {
	int nRet;
	m_csAccess.lock();
	nRet = m_nCurrentLatency;
	m_csAccess.unlock();
	return nRet;
}

// SET
void QualityStats::AddReceivedPacket(unsigned int nCount) {
	m_csAccess.lock();
	m_nReceivedPacketsNum += nCount;
	m_csAccess.unlock();
	onReceivedPacket(nCount);
}

void QualityStats::AddLostPacket(unsigned int nCount) {
	m_csAccess.lock();
	m_nLostPacketsNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nLostPacketsNum;
	LOGAPPKEYVALUE("Bezpowrotnie stracone pakiety: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::LOST_PACKET));
	pEvent->m_nVal = m_nLostPacketsNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
	onLostPacket(nCount);
}

void QualityStats::AddRecoveredPacket(unsigned int nCount) {
	m_csAccess.lock();
	m_nRecoveredPacketsNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nRecoveredPacketsNum;
	LOGAPPKEYVALUE("Odtworzone stracone pakiety: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::RECOVERED_PACKET));
	pEvent->m_nVal = m_nRecoveredPacketsNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
	onRecoveredPacket(nCount);
}

void QualityStats::AddLatePacket(unsigned int nCount) {
	m_csAccess.lock();
	m_nLostPacketsNum -= nCount;
	m_nLatePacketsNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nLatePacketsNum;
	LOGAPPKEYVALUE("Spoznione pakiety: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::LATE_PACKET));
	pEvent->m_nVal = m_nLatePacketsNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
}

void QualityStats::AddMissingFrame(unsigned int nCount) {
	m_csAccess.lock();
	m_nMissingFramesNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nMissingFramesNum;
	LOGAPPKEYVALUE("Brakujace pakiety dla dekodera: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::MISSING_FRAME));
	pEvent->m_nVal = m_nMissingFramesNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
}

void QualityStats::AddDiscardedPacket(unsigned int nCount) {
	m_csAccess.lock();
	m_nDiscardedPacketsNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nDiscardedPacketsNum;
	LOGAPPKEYVALUE("Odrzucone pakiety: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::DISCARDED_PACKET));
	pEvent->m_nVal = m_nDiscardedPacketsNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
	onDiscardedPacket(nCount);
}

void QualityStats::AddInsertedNullPacket(unsigned int nCount) {
	m_csAccess.lock();
	m_nInsertedNullPacketsNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nInsertedNullPacketsNum;
	LOGAPPKEYVALUE("Wstawione puste pakiety: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::INSERTED_NULL_PACKET));
	pEvent->m_nVal = m_nInsertedNullPacketsNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
	onInsertedNullPacket(nCount);
}

void QualityStats::SetJitterBufferSize(unsigned int nSize) {
	m_csAccess.lock();

	if (m_nJitterBufferSize != nSize) {
		m_nJitterBufferSize = nSize;

#ifdef QUALITY_LOG
		std::stringstream val;
		val << (m_nJitterBufferSize*20);
		LOGAPPKEYVALUE("Stan JitterBuf (dl. w ms): ", val.str().c_str() );
#endif

		boost::shared_ptr<QualityStatsEvent> pEvent(
				new QualityStatsEvent(
						QualityStatsEvent::JITTER_BUFFER_SIZE_CHANGED));
		pEvent->m_nVal = m_nJitterBufferSize;
		DispatchEvent(pEvent);
	}

	m_csAccess.unlock();
}

void QualityStats::SetPacketsInJitterBufferNum(unsigned int nCount) {
	m_csAccess.lock();
	m_nPacketsInJitterBufferNum = nCount;
#ifdef QUALITY_LOG
	std::stringstream strVal;
	strVal << (m_nPacketsInJitterBufferNum > m_nJitterBufferSize ? m_nPacketsInJitterBufferNum : m_nJitterBufferSize);
	LOGAPPKEYVALUE("Stan JitterBuffer: ", strVal.str().c_str());

	std::stringstream overVal;
	int res = (int)m_nPacketsInJitterBufferNum - (int)m_nJitterBufferSize;
	overVal << (res > 0 ? res : 0 );
	LOGAPPKEYVALUE("Stan JitterBuffer (overrun): ", overVal.str().c_str());
#endif
	m_csAccess.unlock();
}

void QualityStats::SetSamplesInDriverBufNum(unsigned int nCount) {
	m_csAccess.lock();
	m_nSamplesInDriverBufNum = nCount;
	m_csAccess.unlock();
}

void QualityStats::AddLateAudioFrame(unsigned int nCount) {
	m_csAccess.lock();
	m_nLateAudioFramesNum += nCount;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << m_nLateAudioFramesNum;
	LOGAPPKEYVALUE("Spoznione dla sterownika ramki audio: ", val.str().c_str());
#endif

	boost::shared_ptr<QualityStatsEvent> pEvent(
			new QualityStatsEvent(QualityStatsEvent::LATE_AUDIO_FRAME));
	pEvent->m_nVal = m_nLateAudioFramesNum;
	DispatchEvent(pEvent);

	m_csAccess.unlock();
}

void QualityStats::SetPeerFractionalLossPercent(float fPercent) {
	m_csAccess.lock();
	if (m_fPeerFractionalLossPercent != fPercent) {
		m_fPeerFractionalLossPercent = fPercent;
#ifdef QUALITY_LOG
		std::stringstream val;
		val << fPercent;
		LOGAPPKEYVALUE("Peer loss: ", val.str().c_str());
#endif
		boost::shared_ptr<QualityStatsEvent> pEvent(
				new QualityStatsEvent(
						QualityStatsEvent::PEER_FRACTIONAL_LOSS_CHANGED));
		pEvent->m_fVal = m_fPeerFractionalLossPercent;
		DispatchEvent(pEvent);
	}
	m_csAccess.unlock();
}

void QualityStats::SetJitter(unsigned short nVal) {
	m_csAccess.lock();
	if (m_nJitter != nVal) {
		m_nJitter = nVal;

#ifdef QUALITY_LOG
		std::stringstream val;
		val << nVal;
		LOGAPPKEYVALUE("Jitter: ", val.str().c_str());
#endif

		boost::shared_ptr<QualityStatsEvent> pEvent(
				new QualityStatsEvent(QualityStatsEvent::JITTER_CHANGED));
		pEvent->m_nVal = m_nJitter;
		DispatchEvent(pEvent);
	}
	m_csAccess.unlock();
}

void QualityStats::SetRTT(unsigned short nVal) {
	m_csAccess.lock();
	if (m_nRTT != nVal) {
		m_nRTT = nVal;

#ifdef QUALITY_LOG
		std::stringstream val;
		val << nVal;
		LOGAPPKEYVALUE("RTT: ", val.str().c_str());
#endif

		boost::shared_ptr<QualityStatsEvent> pEvent(
				new QualityStatsEvent(QualityStatsEvent::RTT_CHANGED));
		pEvent->m_nVal = m_nRTT;
		DispatchEvent(pEvent);
	}
	m_csAccess.unlock();
}

void QualityStats::SetRemoteTimerDifference(int nVal) {
	m_csAccess.lock();
	m_nRemoteTimerDifference = nVal;
	m_csAccess.unlock();
}

void QualityStats::SetCurrentLatency(unsigned short nVal) {
	m_csAccess.lock();
	m_nCurrentLatency = nVal;

#ifdef QUALITY_LOG
	std::stringstream val;
	val << nVal;
	LOGAPPKEYVALUE("Delay: ", val.str().c_str());
#endif
	m_csAccess.unlock();
}

//utility func
unsigned long long GetUSec() {
	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec * 1000000 + t.tv_usec;
}
