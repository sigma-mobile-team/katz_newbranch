#ifndef _InputSpeexEncoder_h_
#define _InputSpeexEncoder_h_

#include "utl/UtlString.h"
#include "mp/MpFlowGraphMsg.h"
#include "mp/MpAudioResource.h"
#include "mp/MpResourceMsg.h"
#include "mp/MpCodecFactory.h"

#include <boost/shared_ptr.hpp>
#include "IRTCP.h"
#include "QilexDejitter.h"

class ZEventListener;


class InQilexDecoder : public MpAudioResource
{
public:
	InQilexDecoder(const UtlString& rName, unsigned long nSSRC, const boost::shared_ptr<IRTCPwrapper>& pRTCP);
	virtual	~InQilexDecoder();

public:
	bool Initialize(const std::string& strCoderType, unsigned short nSampleRate);

	void EnableLoopbackMode(bool bEnable);

	//comfort noise
	OsStatus SetCNGEnabled(const UtlString& namedResource, OsMsgQ& fgQ, UtlBoolean enabled);

	/// @parent sender for loopback
	//rede void SetupParent(ZEventListener* pSender);

	/// Returns the count of the number of frames processed by this resource.
	int numFramesProcessed(void);

public:
	void AddBufferToDecode(unsigned const char* pData, unsigned int nDataSize);

private:
	enum
	   {
	      MPRM_ENABLE_CNG = MpResourceMsg::MPRM_EXTERNAL_MESSAGE_START,
	      MPRM_DISABLE_CNG,
	   };

	/// Processes the next frame interval's worth of media.
	UtlBoolean doProcessFrame(
		MpBufPtr inBufs[], MpBufPtr outBufs[],
		int inBufsSize, int outBufsSize,
		UtlBoolean isEnabled, int samplesPerFrame = 80,
		int samplesPerSecond = 8000);

	/// Handles messages for this resource.
	virtual UtlBoolean handleMessage(MpResourceMsg& rMsg);

	/// Copy constructor (not implemented for this class)
	InQilexDecoder(const InQilexDecoder& rMpTestResource);

	/// Assignment operator (not implemented for this class)
	InQilexDecoder& operator=(const InQilexDecoder& rhs);

protected:
	MpDecoderBase*			m_pDecoder;
	QilexDejitter			m_Dejitter;
	unsigned int			m_nCodecFrameSamples;
	unsigned long			m_nSSRC;
	boost::shared_ptr<IRTCPwrapper>	m_pRTCP;
	bool					m_bLoopback;
	ZEventListener*			m_pSender;
	unsigned int            m_nProcessedCnt;
	bool					m_bCNG;
};


#endif
