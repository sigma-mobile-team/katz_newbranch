// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#include "MpodQilexAndroid.h"
#include "QualityStats.h"
#include "QilexAndroidAudioWrapper.h"
#include "../resiprocatejni/QilexLogger.hxx"
#include "os/OsCallback.h"
#include "../resiprocate/rutil/Lock.hxx"
#include <math.h>

#define TEMP_BUFFER_SIZE (3*640)
MpAudioSample temp_buffer[TEMP_BUFFER_SIZE];

MpodQilexAndroid::MpodQilexAndroid(const UtlString& name)
	: MpOutputDeviceDriver(name)
{
	mpTickerNotification = NULL;
	mSamples = NULL;
	mNumSamples = 0;
	ready = true;
	mSamplesInTemp = 0;
}

MpodQilexAndroid::~MpodQilexAndroid()
{
}

OsStatus MpodQilexAndroid::enableDevice(unsigned samplesPerFrame,
								   unsigned samplesPerSec,
								   MpFrameTime currentFrameTime,
								   OsCallback &frameTicker)
{
	//SIPXLOGI("MpodQilexAndroid::enableDevice");

	mSamplesPerFrame = samplesPerFrame;
	mSamplesPerSec = samplesPerSec;
	mCurFrameTime = currentFrameTime;
	mpTickerNotification = &frameTicker;

	mIsEnabled = true;

	return OS_SUCCESS;
}


OsStatus MpodQilexAndroid::disableDevice()
{
	//SIPXLOGI("MpodQilexAndroid::disableDevice");
	mIsEnabled = false;
	return OS_SUCCESS;
}

OsStatus MpodQilexAndroid::pushFrame(unsigned int numSamples,
                 const MpAudioSample* samples,
                 MpFrameTime frameTime)
{
	resip::Lock lock(mutex);
	//SIPXLOGI("MpodQilexAndroid::pushFrame mSamplesInTemp=%d", mSamplesInTemp);

	if(!isEnabled() || samples==NULL)
	{
		SIPXLOGE("MpodQilexAndroid::pushFrame not enabled ");
		return OS_FAILED;
	}

	unsigned int nFramesNum = mSamplesInTemp + QilexAndroidAudioWrapper::getQilexAndroidAudioWrapper()->getAudioTrackBufferLen()/2;
	QualityStats::Get().SetSamplesInDriverBufNum(nFramesNum);

	if(mSamplesInTemp + numSamples <= TEMP_BUFFER_SIZE)
	{
		memcpy(temp_buffer + mSamplesInTemp, samples, numSamples*2);
		mNumSamples = numSamples;
		mSamplesInTemp += numSamples;
	}
	else
	{
        SIPXLOGE("MpodQilexAndroid::pushFrame Out of buffer");
	}

	//struct timeval ttt;
	//gettimeofday(&ttt, NULL);
	//SIPXLOGI("MpodQilexAndroid::pushFrame mSamplesInTemp=%d %d.%d", mSamplesInTemp, ttt.tv_sec, ttt.tv_usec);

	//SIPXLOGI("/MpodQilexAndroid::pushFrame");
	return OS_SUCCESS;
}

void MpodQilexAndroid::notifyFrameDone()
{
	//SIPXLOGI("MpodQilexAndroid::notifyFrameDone");
	if(mpTickerNotification)
	{
		mpTickerNotification->signal(mCurFrameTime);
	}
	mCurFrameTime += getFramePeriod(mNumSamples, mSamplesPerSec);

	//SIPXLOGI("MpodQilexAndroid::notifyFrameDone/");
}

int MpodQilexAndroid::fillBuffer(int16_t* buffer, unsigned long maxSamplesToFetch)
{
	//SIPXLOGI("MpodQilexAndroid::fillBuffer(int16_t* buffer)");
	resip::Lock lock(mutex);

	if (mSamplesInTemp == 0)
		return 0;

	if (maxSamplesToFetch == 0)
		maxSamplesToFetch = mSamplesInTemp;

    memcpy(buffer, temp_buffer, maxSamplesToFetch*2);
    mNumSamples = maxSamplesToFetch;
	mSamplesInTemp -= maxSamplesToFetch;

	if (mSamplesInTemp > 0)
		memcpy(temp_buffer, temp_buffer + maxSamplesToFetch, mSamplesInTemp*2);

	//SIPXLOGI("/MpodQilexAndroid::fillBuffer(int16_t* buffer)");
    return mNumSamples;
}
