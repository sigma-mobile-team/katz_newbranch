/*********************************/
/* QualityStats.h                */
/* Lukasz Rychter 2012/01/03     */
/*                               */
/* module gathering audio        */
/* quality relatd informations   */
/*********************************/

#ifndef __QUALITY_STATS_H__
#define __QUALITY_STATS_H__

#include "zevent.h"
#include "ZEventHandler.h"
#include "../resiprocate/rutil/RecursiveMutex.hxx"
#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>

#define INVALID_TIMER_DIFFERENCE 0xFFFFFFFF

class QualityStats : public ZEventHandler 
{
private:
	QualityStats(const QualityStats&); //disallow copying

protected:

	QualityStats();
public:
	static void InitializeInstance();
	static void DeinitializeInstance();
	static QualityStats& Get();

public:
	void ResetStats();
	void WriteStatsToLog();

//GET
	unsigned int GetReceivedPacketsNum();
	unsigned int GetLostPacketsNum();
	unsigned int GetRecoveredPacketsNum();
	unsigned int GetLatePacketsNum(); //newer packet was already played, so this one was discarded
	unsigned int GetMissingFramesNum(); //it was time for decoder to fetch next packet but there wasn't any in buffer OR there was an inserted NULL packet
	unsigned int GetReallyMissingFramesNum(); //it was time for decoder to fetch next packet but there wasn't any in buffer (doesn't count inserted NULL packets)
	unsigned int GetDiscardedPacketsNum(); //there wasn't any more space in buffer, so some packets were dropped
	unsigned int GetInsertedNullPacketsNum(); //there was persistent buffer underrun, so we inserted null packets (less noticeable than empty buffer situaton on packets delay)
	unsigned int GetJitterBufferSize();
	unsigned int GetPacketsInJitterBufferNum();
	float GetJitterBufferUsage(); //percent of jitter buffer currently filled with packets

	unsigned int GetSamplesInDriverBufNum();
	unsigned int GetLateAudioFramesNum(); //audio frame was decoded but too late. Driver needed it earlier. We had to push silent frame or something

	float GetPeerFractionalLossPercent(); //in some time window
	unsigned short GetJitter(); 
	unsigned short GetRTT(); 

	int GetRemoteTimerDifference();
	unsigned short GetCurrentLatency();


//SET
	void AddReceivedPacket(unsigned int nCount=1);
	void AddLostPacket(unsigned int nCount=1);
	void AddRecoveredPacket(unsigned int nCount=1);
	void AddLatePacket(unsigned int nCount=1);
	void AddMissingFrame(unsigned int nCount=1);
	void AddDiscardedPacket(unsigned int nCount=1);
	void AddInsertedNullPacket(unsigned int nCount=1);
	void SetJitterBufferSize(unsigned int nSize);
	void SetPacketsInJitterBufferNum(unsigned int nCount);


	boost::signals2::signal<void (unsigned int count)> onReceivedPacket;
	boost::signals2::signal<void (unsigned int count)> onLostPacket;
	boost::signals2::signal<void (unsigned int count)> onRecoveredPacket;
	boost::signals2::signal<void (unsigned int count)> onDiscardedPacket;
	boost::signals2::signal<void (unsigned int count)> onInsertedNullPacket;



	void SetSamplesInDriverBufNum(unsigned int nCount);
	void AddLateAudioFrame(unsigned int nCount=1);

	void SetPeerFractionalLossPercent(float fPercent);
	void SetJitter(unsigned short nVal);
	void SetRTT(unsigned short nVal);

	void SetRemoteTimerDifference(int nVal);
	void SetCurrentLatency(unsigned short nVal);


protected:
	unsigned int	m_nReceivedPacketsNum;
	unsigned int	m_nLostPacketsNum;
	unsigned int	m_nRecoveredPacketsNum;
	unsigned int	m_nLatePacketsNum;
	unsigned int	m_nMissingFramesNum;
	unsigned int	m_nDiscardedPacketsNum;
	unsigned int	m_nInsertedNullPacketsNum;
	unsigned int	m_nJitterBufferSize;
	unsigned int	m_nPacketsInJitterBufferNum;

	unsigned int	m_nSamplesInDriverBufNum;
	unsigned int	m_nLateAudioFramesNum;

	float			m_fPeerFractionalLossPercent;

	unsigned short	m_nJitter;
	unsigned short	m_nRTT;

	int				m_nRemoteTimerDifference;
	unsigned short	m_nCurrentLatency;

	//GCPUMonitor		m_CPUMonitor;

protected:
	static QualityStats*		m_pObject;		
	static resip::RecursiveMutex		m_csAccess;
};



class QualityStatsEvent : public ZAsyncEvent
{
public:
	QualityStatsEvent(const int nSubtypeId)
	{
		m_nSubtypeId = nSubtypeId;
	}


	// Implementation of virtual methods from ZAsyncEvent
	virtual const bool ValidateSubtypeId(const int nSubtypeId) const
	{
		if (nSubtypeId < MINID+1 || nSubtypeId > MAXID-1)
			return false;
		return true;
	}
	virtual const ZEventType::EventType GetType() const
	{
		return ZEventType::QualityStats;
	}

	virtual const ZEvent::CollapseType GetCollapsingType()
	{
		return ZEvent::ByTypeAndSubtype;
	}

	//EVENTS
	enum EVENT_ID
	{
		MINID = -1,
		LATE_AUDIO_FRAME,
		MISSING_FRAME,
		DISCARDED_PACKET,
		INSERTED_NULL_PACKET,
		LATE_PACKET,
		LOST_PACKET,
		RECOVERED_PACKET,
		JITTER_BUFFER_SIZE_CHANGED,
		JITTER_CHANGED,
		RTT_CHANGED,
		PEER_FRACTIONAL_LOSS_CHANGED,
		MAXID
	};

	float m_fVal;
	int m_nVal;
};


//utility func
unsigned long long GetUSec();

#endif
