// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.
#ifndef QILEX_SPKR_TASK_HXX
#define QILEX_SPKR_TASK_HXX

#include <os/OsTask.h>
#include "../resiprocate/rutil/RecursiveMutex.hxx"

#include <jni.h>

class MpodQilexAndroid;

class QilexSpkrTask : public OsTask
{
public:
	QilexSpkrTask(const UtlString& name, void* pArg, const int priority = DEF_PRIO, const int options = DEF_OPTIONS, const int stackSize = DEF_STACKSIZE);
	MpodQilexAndroid* getodQilexAndroid() { return odQilexAndroid; }
	void setodQilexAndroid(MpodQilexAndroid* mpod) { odQilexAndroid = mpod; }
	void startStream();
	void stopStream();
	bool isWorking() { return working; }

protected:
	virtual int run(void* pArg);

	MpodQilexAndroid* odQilexAndroid;
	bool streamDone;
	bool working;
	int bytesInBuffer;
	resip::RecursiveMutex mutex;
};

#endif // QILEX_SPKR_TASK_HXX
