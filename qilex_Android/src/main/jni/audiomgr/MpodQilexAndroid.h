// Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

#ifndef _MPODQILEXANDROID_H
#define _MPODQILEXANDROID_H

// SIPX INCLUDES
#include "mp/MpOutputDeviceDriver.h"
#include "../resiprocate/rutil/RecursiveMutex.hxx"


class MpodQilexAndroid : public MpOutputDeviceDriver
{
public:

   explicit MpodQilexAndroid(const UtlString& name);
   virtual ~MpodQilexAndroid();

   /// Initialize device driver and state.
   virtual OsStatus enableDevice(unsigned samplesPerFrame,
								   unsigned samplesPerSec,
								   MpFrameTime currentFrameTime,
								   OsCallback &frameTicker);

   /// Uninitialize device driver.
   virtual OsStatus disableDevice();

   /// @brief Send data to output device.
   virtual
   OsStatus pushFrame(unsigned int numSamples,
                    const MpAudioSample* samples,
                    MpFrameTime frameTime);

   void notifyFrameDone();

   int fillBuffer(int16_t* buffer, unsigned long maxSamplesToFetch=0); //maxSamplesToFetch == 0 means no limit

   const MpAudioSample* mSamples;
   unsigned int mNumSamples;

protected:

   bool ready;
   unsigned int mSamplesInTemp;
   MpFrameTime mCurFrameTime;

   resip::RecursiveMutex mutex;
};

#endif  // _MPODQILEXANDROID_H
