/*********************************/
/* RTPPcket.cpp                  */
/* Lukasz Rychter 2012/01/12     */
/*							     */
/* RTPPacket wrapper class       */
/*********************************/

#include "RTPPacket.h"
#include "QilexLogger.hxx"

inline short ntohs(short nVal)
{
	return (nVal << 8) | ((nVal >> 8) & 0x00ff);
}

inline long ntohl(long nVal)
{
	return (nVal << 24) | ((nVal & 0x0000ff00) << 8) | ((nVal & 0x00ff0000) >> 8) | ((nVal >> 24) & 0x000000ff);
}

bool IsSeqEarlier(unsigned short nSeqLeft, unsigned short nSeqRight)
{
	return (nSeqLeft < nSeqRight && nSeqRight - nSeqLeft < 32768) || ((int)nSeqLeft - (int)nSeqRight > 32768);
}



RTPPacket::RTPPacket(unsigned short nSeq) : m_bValid(false), m_pData(NULL)
{
	m_nSeq = nSeq;
}

RTPPacket::RTPPacket(const void* pData, unsigned short nSize, unsigned char nFECdistance)
{
	if (nSize >= 12) //valid packet must have at least header of 12 bytes size
	{
		m_bValid = true;

		m_nVPXCC = *(unsigned char*)pData;
		m_nMPT = *((unsigned char*)pData+1);
		m_nSeq = ntohs( *(unsigned short*)((unsigned char*)pData+2) );
		m_nTimestamp = ntohl( *(int*)((unsigned char*)pData+4) );
		m_nSSRC = ntohl( *(int*)((unsigned char*)pData+8) );

		m_nFECdistance = nFECdistance;
		
		m_nDataSize = nSize - 12;

		if (nSize > 12)
		{
			try
			{
				m_pData = new unsigned char[m_nDataSize];
				memcpy(m_pData, (unsigned char*)pData+12, m_nDataSize);
			}
			catch (...) {}
		}
		else
			m_pData = NULL;
	}
	else
		m_bValid = false;
}

RTPPacket::~RTPPacket()
{
	if (m_pData)
		delete[] m_pData;
}
