# Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := audiomgr

LOCAL_CPP_EXTENSION := .cpp
LOCAL_CFLAGS += -std=gnu++11

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../ \
	$(LOCAL_PATH)/../boost_1_57_0 \
	$(LOCAL_PATH)/../boost_1_57_0/boost \
	$(LOCAL_PATH)/../commonlib \
	$(LOCAL_PATH)/../resiprocate \
	$(LOCAL_PATH)/../resiprocate/reTurn \
	$(LOCAL_PATH)/../resiprocate/reTurn/client \
	$(LOCAL_PATH)/../resiprocate/contrib/srtp/include \
	$(LOCAL_PATH)/../resiprocate/contrib/srtp/crypto/include \
	$(LOCAL_PATH)/../resiprocatejni \
	$(LOCAL_PATH)/../sipXportLib/include \
	$(LOCAL_PATH)/../sipXmediaLib/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/android \
	$(LOCAL_PATH)/../sipXsdpLib/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/libspeex/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/libopus/include \
	$(LOCAL_PATH)/../sipXmediaLib/contrib/libspandsp/src \
	$(LOCAL_PATH)/../streamtransport \
	$(LOCAL_PATH)/../webrtc

LOCAL_SRC_FILES := \
	AudioMgr.cpp \
	RTCP.cpp \
	InQilexDecoder.cpp \
	OutQilexEncoder.cpp \
	MpidQilexAndroid.cpp \
	MpodQilexAndroid.cpp \
	QilexSpkrTask.cpp \
	QilexMicTask.cpp \
	QilexAndroidAudioWrapper.cpp \
	QualityStats.cpp \
	QilexDejitter.cpp \
	RTPPacket.cpp


LOCAL_STATIC_LIBRARIES += \
	libstreamtransport \
	libsipXmedia \
	libsipXsdp \
	libsipXport \
	libcodec_pcmapcmu \
	libcodec_tones \
	libcodec_speex \
	libcodec_opus \
	libspeex \
	libopus \
	libspeexdsp \
	AudioStats

include $(BUILD_STATIC_LIBRARY)