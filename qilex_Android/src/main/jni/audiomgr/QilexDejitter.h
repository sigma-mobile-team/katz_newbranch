/*********************************/
/* QilexDejitter.h               */
/* Lukasz Rychter 2012/01/12     */
/*							     */
/* Dejitter for RTP packets      */
/*********************************/

#ifndef __QILEX_DEJITTER_H__
#define __QILEX_DEJITTER_H__

#include <set>
#include "RTPPacket.h"
#include <boost/shared_ptr.hpp>
#include "../resiprocate/rutil/RecursiveMutex.hxx"

#define DEFAULT_DEJITTER_BUFFER_LEN 10	// x 20ms
#define MAXIMUM_JITTER_BUFFER_LEN 25
#define MINIMUM_JITTER_BUFFER_LEN 3

#define LOW_LEVEL_RATIO 1/4 //if there is less than BufferSize * LOW_LEVEL_RATIO packets in buffer we should increase buffer size
#define JITTER_BUFFER_SHRINK_TIME_BASE 100000 //ms, defines how fast buffer is shrinked when there is no low level situation. It depends also on current buffer size

#define UNDERRUN_RATIO 2/3 //if there is less than BufferSize * UNDERRUN_RATIO packets in buffer we count it as underrun
#define MINIMUM_UNDERRUN_TIME 1500 //ms, after this time we start adding null packets
#define UNDERRUN_INSERTING_TIME_BASE 500 //ms, defines how fast we add null packets in underrun condition. It depends also on current underrun ratio

#define MINIMUM_OVERRUN_TIME 1000 //ms, after this time we start discarding packets
#define OVERRUN_DISCARDING_TIME_BASE 1000 //ms, defines how fast we discard packets in overrun condition. It depends also on current overrun ratio


#ifdef _WIN32
#define GetTimeMS()	GetTickCount()
#endif

//Dynamic jitter buffer. It tries to keep from UNDERRUN_RATIO*BUFFER_SIZE to BUFFER_SIZE packets in buffer. 
//If there is less than UNDERRUN_RATIO*BUFFER_SIZE packets in buf (underrun condition) for longer than MINIMUM_UNDERRUN_TIME 
//we are inserting null packets to regenerate the buffer. Packets are inserted in time deltas based on current underrun rato and UNDERRUN_INSERTING_TIME_BASE
//If there is more than BUFFER_SIZE packets in buf (overrun condition) for longer than MINIMUM_OVERRUN_TIME
//we are dropping packets to reduce latency. Packets are dropped in time deltas based on current overrun ratio and OVERRUN_DISCARDING_TIME_BASE
//BUFFER_SIZE changes in time to maintaint least possible latency. Buffer is shrinked in time deltas based on current size and JITTER_BUFFER_SHRINK_TIME_BASE.
//When packets count drops under BUFFER_SIZE*LOW_LEVEL_RATIO we are increasing BUFFER_SIZE - proportional to num packets missing to the BUFFER_SIZE*LOW_LEVEL_RATIO level.

class QilexDejitter
{
public:
	QilexDejitter();

	void pushPacket(const boost::shared_ptr<RTPPacket>& pRtp);
	boost::shared_ptr<RTPPacket> pullPacket();

	unsigned char GetPacketsCount();
	unsigned long GetTimeMS();

protected:
	bool LostPacket(unsigned short nSeq);
	typedef std::multiset< boost::shared_ptr<RTPPacket>, SmartRTPPacketComparer>	PacketsSet;

	PacketsSet							m_sPackets;

	std::set<unsigned short>			m_sDiscarded;
	std::set<unsigned short>			m_sSilent;
	unsigned long						m_nBufferOverrunTime;
	unsigned long						m_nLastDiscardTime;
		
	unsigned char						m_nBufferSize;
	int									m_nLastPulledSeq;

	unsigned long						m_nBufferUnderrunTime;
	unsigned long						m_nLastNullInsertedTime;

	unsigned long						m_nLastJitterBufferSizeChanged;
	bool								m_bWasFilled;
	unsigned char						m_nJitterBufferLowLevel;
	unsigned char						m_nLastRegeneratedBufferSize;

	bool								m_bSilence; //silent period

	resip::RecursiveMutex						m_csAccess;
};

#endif
