/*********************************/
/* RTCP.cpp                      */
/* Lukasz Rychter 2011/10/07     */
/*                               */
/* RTCP wrapper/handler          */
/*********************************/

#include "RTCP.h"

#include <boost/shared_ptr.hpp>
#include <sys/timeb.h>
#include <sstream>
#include <iomanip>
#include "rtcp/SourceDescription.h"
#include "rtcp/RTCPConnection.h"
#include "QualityStats.h"
#include "QilexLogger.hxx"

const unsigned long WALLTIMEOFFSET  = 2208992400UL; // Difference between LocalTime and Wall Time:
const double        FOUR_GIGABYTES  = (65536.0*65536.0); // 2**32 ("4 Gig"):

int RTCP_Sender::Send(unsigned char* puchDataBuffer, unsigned long ulBytesToSend)
{
	if (m_pSender)
	{
		m_pSender->SendRtcpData(puchDataBuffer, ulBytesToSend);
	}
	else assert(0);

	return 0;
}


RTCPwrapper::RTCPwrapper(unsigned long nSSRC, const boost::shared_ptr<IStreamTransport>& pSender)
{
	m_nSSRC = nSSRC;
	m_pSender = pSender;

	m_pSDES = new CSourceDescription(nSSRC, (unsigned char*)"", (unsigned char*)"", (unsigned char*)"", (unsigned char*)"", (unsigned char*)"sip.ct.plus.pl");
	assert(m_pSDES);

	if (m_pSDES)
	{
		((CSourceDescription*)m_pSDES)->SetName((unsigned char*)"sip.ct.plus.pl");
		((CSourceDescription*)m_pSDES)->SetNotes((unsigned char*)"");
		((CSourceDescription*)m_pSDES)->SetPrivate((unsigned char*)"");
	}

	m_pRTCP_Handler.reset(new RTCP_Handler(this));
	assert(m_pRTCP_Handler);

	m_pRTCPConnection = new CRTCPConnection(nSSRC, m_pRTCP_Handler.get(), m_pSDES);
	assert(m_pRTCPConnection);

	if (m_pRTCPConnection)
	{
		if (((CRTCPConnection*)m_pRTCPConnection)->Initialize())
		{
			m_pRTCP_Sender.reset(new RTCP_Sender(pSender));
			assert(m_pRTCP_Sender);

			if (!m_pRTCPConnection->StartRenderer(m_pRTCP_Sender.get()))
				assert(0);
		}
		else assert(0);
	}
}


RTCPwrapper::~RTCPwrapper()
{
	if (m_pRTCPConnection)
	{		
		//TODO check this for leaks
		if (!((CRTCPConnection*)m_pRTCPConnection)->Terminate())
			assert(0);
	}
}



void RTCP_Handler::SenderReportReceived(IGetSenderStatistics *piGetSenderStatistics,
						  IRTCPConnection      *piRTCPConnection,
						  IRTCPSession         *piRTCPSession)
{
	if (piGetSenderStatistics)
	{
		unsigned long nPacketCount;
		unsigned long nOctetCount;
		unsigned long nRTPTimestamp;
		unsigned long* anNTPTimestamp;
		piGetSenderStatistics->GetSenderStatistics(&nPacketCount, &nOctetCount, &nRTPTimestamp, &anNTPTimestamp);

		//synchronizing local and remote timestamp timers
		unsigned long nSec = anNTPTimestamp[0]-WALLTIMEOFFSET;
		unsigned short nMilli = (short)((anNTPTimestamp[1] / (double)(FOUR_GIGABYTES/1000000.0)) / 1000);

		unsigned long nRemoteTimestamp;
		nRemoteTimestamp = (unsigned long)((nSec*1000 + nMilli) & 0xffffffff); 
		nRemoteTimestamp += m_nLastRTT/2;

		timeb stLocalTime;
		ftime(&stLocalTime);
		unsigned long nCurrentTimestamp = (unsigned long)(((__int64)stLocalTime.time*1000 + (__int64)stLocalTime.millitm) & 0xffffffff);

		int nCurrentTimerDifference = nRemoteTimestamp - nCurrentTimestamp;

		m_TimerDifferenceCounter.AddSample((float)nCurrentTimerDifference);
		int nMeanTimerDifference = (int)m_TimerDifferenceCounter.GetMeanValue();
		
		QualityStats::Get().SetRemoteTimerDifference(nMeanTimerDifference);
	}
}



void RTCP_Handler::ReceiverReportReceived(IGetReceiverStatistics *piGetReceiverStatistics,
						                  IRTCPConnection        *piRTCPConnection,
							              IRTCPSession           *piRTCPSession)
{
	if (piGetReceiverStatistics)
	{
		unsigned long nFractionalLoss;
		unsigned long nCumulativeLoss;
		unsigned long nHighestSequenceNo;
		unsigned long nInterarrivalJitter;
		unsigned long nSRTimestamp;
		unsigned long nPacketDelay;

		piGetReceiverStatistics->GetReceiverStatistics(&nFractionalLoss, &nCumulativeLoss, &nHighestSequenceNo, &nInterarrivalJitter, &nSRTimestamp, &nPacketDelay);


		unsigned long NTPtime[2] = {0};
#ifdef WIN32
		_timeb stLocalTime;

		// Get the LocalTime expressed as seconds since 1/1/70 (UTC)
		_ftime(&stLocalTime);
#elif ANDROID
		timeb stLocalTime;

		// Get the LocalTime expressed as seconds since 1/1/70 (UTC)
		ftime(&stLocalTime);
#endif

		// Load Most Significant word with Wall time seconds
		NTPtime[0] = (unsigned long)stLocalTime.time + WALLTIMEOFFSET;

		// Load Least Significant word with Wall time microseconds
		double dTime;
		dTime = stLocalTime.millitm * 1000;
		dTime *= (double)(FOUR_GIGABYTES/1000000.0);
		NTPtime[1] = (unsigned long)dTime;

		unsigned long nCurrentTime;
		nCurrentTime = ((NTPtime[0] & 0xFFFF) << 16);
		nCurrentTime |= ((NTPtime[1] >> 16) & 0xFFFF);

		unsigned short nRTT = 0;
		if (nSRTimestamp != 0)
		{
			nRTT = (unsigned short)((nCurrentTime - nSRTimestamp - nPacketDelay) / 65536.0 * 1000.0 + 0.5);
			m_nLastRTT = nRTT;
		}
		
		float fCurrentLossPercent = nFractionalLoss/(float)256 * 100;

		m_FractionalLossCounter.AddSample(fCurrentLossPercent);
		float fAveragedLossPercent = m_FractionalLossCounter.GetAveragedValue();

		QualityStats::Get().SetJitter((unsigned short)nInterarrivalJitter);
		QualityStats::Get().SetRTT(nRTT);
		QualityStats::Get().SetPeerFractionalLossPercent(fAveragedLossPercent);

		//TODO check this
		parent_wrapper->onJitterStats(nRTT * 1000.0f, nInterarrivalJitter);
		parent_wrapper->onRTTStats(nRTT * 1000.0f, nRTT);
		parent_wrapper->onLossPercentData(nRTT * 1000.0f, fCurrentLossPercent);



		std::stringstream ss;
		ss	<< "RTCP stats:" << std::endl
			<< " Frac.Loss.Curr.:" << std::setprecision(1) << std::fixed << fCurrentLossPercent << "%%" << std::endl
			<< " Frac.Loss.Av.:" << fAveragedLossPercent << "%%" << std::endl
			<< " Cum.Loss:" << nCumulativeLoss << std::endl
			<< " Jitter:" << nInterarrivalJitter << "ms" << std::endl
			<< " RTT:" << nRTT << "ms" << std::endl;
		SIPXLOGI(" \r\n");
		SIPXLOGI("%s", ss.str().c_str());
	}
}
