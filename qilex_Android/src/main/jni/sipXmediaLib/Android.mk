#
# Copyright (C) 2009 SIPfoundry Inc.
# Licensed by SIPfoundry under the LGPL license.
#
# Copyright (C) 2009 SIPez LLC.
# Licensed to SIPfoundry under a Contributor Agreement.
#
#
#//////////////////////////////////////////////////////////////////////////
#
# Author: Dan Petrie (dpetrie AT SIPez DOT com)
#
#
# This Makefile is for building sipXmediaLib as a part of Android NDK.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

# Set up the target identity.
# LOCAL_MODULE/_CLASS are required for local-intermediates-dir to work.
LOCAL_MODULE := libsipXmedia
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
#intermediates := $(call local-intermediates-dir)

LOCAL_SRC_FILES := \
	src/mp/dmaTaskPosix.cpp \
	src/mp/MpBuf.cpp \
	src/mp/MpOutputDeviceDriver.cpp \
	src/mp/MpOutputDeviceManager.cpp \
	src/mp/MpDecoderBase.cpp \
	src/mp/MpResource.cpp \
	src/mp/MpFlowGraphMsg.cpp \
	src/mp/mpG711.cpp \
	src/mp/MpAudioOutputConnection.cpp \
	src/mp/MpInputDeviceManager.cpp \
	src/mp/MprFromInputDevice.cpp \
	src/mp/MpFlowGraphBase.cpp \
	src/mp/MpAudioResource.cpp \
	src/mp/MpEncoderBase.cpp \
	src/mp/MpResNotificationMsg.cpp \
	src/mp/MpResourceSortAlg.cpp \
	src/mp/MprToOutputDevice.cpp \
	src/mp/MprSpeexEchoCancel.cpp \
	src/mp/MprSpeexPreProcess.cpp \
	src/mp/MprWebRTCEchoControlMobile.cpp \
	src/mp/MprWebRTCEchoCancellation.cpp \
	src/mp/MpAudioBuf.cpp \
	src/mp/MpArrayBuf.cpp \
	src/mp/MpResourceMsg.cpp \
	src/mp/MpInputDeviceDriver.cpp \
	src/mp/MpMediaTask.cpp \
	src/mp/MpBufferMsg.cpp \
	src/mp/MpMediaTaskMsg.cpp \
	src/mp/MpMisc.cpp \
	src/mp/MpResampler.cpp \
	src/mp/MpBufPool.cpp \
	src/mp/MpCodec.cpp \
	src/mp/MpCodecFactory.cpp \
	src/mp/MpDataBuf.cpp \
	src/mp/MpResamplerSpeex.cpp \
	src/mp/MpRtpBuf.cpp \
	src/mp/MpUdpBuf.cpp \
	src/mp/MpStaticCodecInit.cpp \
	src/mp/MpAudioUtils.cpp \
	src/mp/MpPlgStaffV1.cpp \
	src/mp/MpVadBase.cpp \
	src/mp/MpVadSimple.cpp \
	src/mp/MpVadWebRTC.cpp \
	src/mp/MprVad.cpp \
	src/mp/MprWebRTCGainControl.cpp \
	src/mp/DSPlib.cpp \
	src/rtcp/MsgQueue.cpp \
	src/rtcp/Message.cpp \
	src/rtcp/BaseClass.cpp \
	src/rtcp/ByeReport.cpp \
	src/rtcp/NetworkChannel.cpp \
	src/rtcp/ReceiverReport.cpp \
	src/rtcp/RTCManager.cpp \
	src/rtcp/RTCPConnection.cpp \
	src/rtcp/RTCPHeader.cpp \
	src/rtcp/RTCPRender.cpp \
	src/rtcp/RTCPSession.cpp \
	src/rtcp/RTCPSource.cpp \
	src/rtcp/RTCPTimer.cpp \
	src/rtcp/RTPHeader.cpp \
	src/rtcp/SenderReport.cpp \
	src/rtcp/SourceDescription.cpp

LOCAL_CFLAGS += -DCODEC_SPEEX_STATIC=1 \
				-DCODEC_OPUS_STATIC=1 \
				-DCODEC_PCMA_PCMU_STATIC=1 \
                -DDISABLE_STREAM_PLAYER
LOCAL_CFLAGS += -DHAVE_SPEEX
LOCAL_CFLAGS += -DHAVE_SPEEX_RESAMPLER

LOCAL_C_INCLUDES += \
	$(SIPX_HOME)/libpcre \
	$(SIPX_HOME)/sipXportLib/include \
	$(SIPX_HOME)/sipXsdpLib/include \
	$(SIPX_HOME)/sipXmediaLib/include

LOCAL_STATIC_LIBRARIES := libsipXsdp libsipXport

#LOCAL_LDLIBS += -lstdc++ -ldl

# Android audio related stuff
#SIPX_MEDIA_SHARED_LIBS += libmedia libutils libcutils
SIPX_MEDIA_STATIC_LIBS += libspeex libspeexdsp libopus libwebrtc_audio_preprocessing
SIPX_MEDIA_CFLAGS += -include AndroidConfig.h -DANDROID_2_0
SIPX_MEDIA_C_INCLUDES += \
	$(SIPX_HOME)/sipXmediaLib/contrib/android/android_2_0_headers/frameworks/base/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/android/android_2_0_headers/system/core/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/android/android_2_0_headers/system/core/include/arch/linux-arm \
	$(SIPX_HOME)/sipXmediaLib/contrib/libspeex/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/libopus/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/libspandsp/src \
	$(SIPX_HOME)/sipXmediaLib/contrib/android \
	$(SIPX_HOME)/webrtc

LOCAL_SHARED_LIBRARIES += $(SIPX_MEDIA_SHARED_LIBS)
LOCAL_STATIC_LIBRARIES += $(SIPX_MEDIA_STATIC_LIBS)
LOCAL_LDLIBS += $(SIPX_MEDIA_LDLIBS)
LOCAL_CFLAGS += $(SIPX_MEDIA_CFLAGS)
LOCAL_C_INCLUDES += $(SIPX_MEDIA_C_INCLUDES)

#LOCAL_CFLAGS += -DEXTERNAL_VAD
#LOCAL_CFLAGS += -DEXTERNAL_PLC
#LOCAL_CFLAGS += -DEXTERNAL_JB_ESTIMATION
#LOCAL_CFLAGS += -DEXTERNAL_AGC
#LOCAL_CFLAGS += -DEXTERNAL_SS

include $(BUILD_STATIC_LIBRARY)

################################################################################
#  Speex Codec Plug-in
################################################################################

include $(CLEAR_VARS)

# Set up the target identity.
LOCAL_MODULE := libcodec_speex

LOCAL_SRC_FILES := \
	src/mp/codecs/plgspeex/PlgSpeex.c \
	src/mp/codecs/plgspeex/speex_nb.c \
	src/mp/codecs/plgspeex/speex_wb.c \
	src/mp/codecs/plgspeex/speex_uwb.c

LOCAL_CFLAGS += \
	-DDISABLE_STREAM_PLAYER

LOCAL_C_INCLUDES += \
	$(SIPX_HOME)/libpcre \
	$(SIPX_HOME)/sipXportLib/include \
	$(SIPX_HOME)/sipXsdpLib/include \
	$(SIPX_HOME)/sipXmediaLib/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/libspeex/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/android

#LOCAL_LDLIBS += -lstdc++ -ldl

include $(BUILD_STATIC_LIBRARY)

################################################################################
#  Opus Codec Plug-in
################################################################################

include $(CLEAR_VARS)

# Set up the target identity.
LOCAL_MODULE := libcodec_opus

LOCAL_SRC_FILES := src/mp/codecs/plgopus/PlgOpus.c
	
LOCAL_CFLAGS += \
	-DDISABLE_STREAM_PLAYER

LOCAL_C_INCLUDES += \
	$(SIPX_HOME)/libpcre \
	$(SIPX_HOME)/sipXportLib/include \
	$(SIPX_HOME)/sipXsdpLib/include \
	$(SIPX_HOME)/sipXmediaLib/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/libopus/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/android

#LOCAL_LDLIBS += -lstdc++ -ldl

include $(BUILD_STATIC_LIBRARY)

################################################################################
#  G.711 Codec Plug-in
################################################################################

include $(CLEAR_VARS)

# Set up the target identity.
LOCAL_MODULE := libcodec_pcmapcmu

LOCAL_SRC_FILES := \
    src/mp/codecs/plgpcmapcmu/CodecPcmaWrapper.c \
    src/mp/codecs/plgpcmapcmu/CodecPcmuWrapper.c \
    src/mp/codecs/plgpcmapcmu/G711.c \
    src/mp/codecs/plgpcmapcmu/PlgPcmaPcmu.c \
    contrib/libspandsp/src/g711.c
				
LOCAL_CFLAGS += \
	-DDISABLE_STREAM_PLAYER

LOCAL_C_INCLUDES += \
	$(SIPX_HOME)/libpcre \
	$(SIPX_HOME)/sipXportLib/include \
	$(SIPX_HOME)/sipXsdpLib/include \
	$(SIPX_HOME)/sipXmediaLib/include \
	$(SIPX_HOME)/sipXmediaLib/contrib/libspandsp/src \
	$(SIPX_HOME)/sipXmediaLib/contrib/android

#LOCAL_LDLIBS += -lstdc++ -ldl
include $(BUILD_STATIC_LIBRARY)