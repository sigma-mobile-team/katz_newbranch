//
// Copyright (C) 2008 SIPez LLC.
// Licensed to SIPfoundry under a Contributor Agreement.
//
// Copyright (C) 2008 SIPfoundry Inc.
// Licensed by SIPfoundry under the LGPL license.
//
// $$
///////////////////////////////////////////////////////////////////////////////

// Author: Andrzej K. Haczewski <ahaczewski AT gmail DOT com>

#ifndef _MpVadWebRTC_h_
#define _MpVadWebRTC_h_

#include <mp/MpVadBase.h>

// FORWARD DECLARATIONS
typedef struct WebRtcVadInst VadInst;

/**
*  VAD using WebRTC voice detection algorithm.
*/
class MpVadWebRTC : public MpVadBase
{
/* //////////////////////////// PUBLIC //////////////////////////////////// */
public:
   static const char *name; ///< Name of this VAD algorithm for use in MpVadBase::createVad().

   enum {
      DEFAULT_MODE = 3
   };

/* ============================ CREATORS ================================== */
///@name Creators
//@{

     /// Constructor
   MpVadWebRTC();

     /// @copydoc MpVadBase::init()
   OsStatus init(int samplesPerSec);

     /// Destructor
   ~MpVadWebRTC();

//@}

/* ============================ MANIPULATORS ============================== */
///@name Manipulators
//@{

     /// @copydoc MpVadBase::processFrame()
   MpSpeechType processFrame(uint32_t packetTimeStamp,
                             const MpAudioSample* pBuf,
                             unsigned inSamplesNum,
                             const MpSpeechParams &speechParams,
                             UtlBoolean calcEnergyOnly = FALSE);

     /// @copydoc MpVadBase::setParam()
   OsStatus setParam(const char* paramName, void* value);

     /// Sets aggressiveness mode.
   void setMode(int mode);
     /**<
     *  A more aggressive (higher mode) VAD is more restrictive in reporting
     *  speech.
     *
     *  @param[in] mode Aggressiveness mode (0, 1, 2 or 3).
     */

     /// @copydoc MpVadBase::reset()
   void reset();

//@}

/* ============================ ACCESSORS ================================= */
///@name Accessors
//@{
     /// @copydoc MpVadBase::getEnergy()
   int getEnergy() const;

//@}

/* ============================ INQUIRY =================================== */
///@name Inquiry
//@{

//@}

/* //////////////////////////// PROTECTED ///////////////////////////////// */
protected:

/* //////////////////////////// PRIVATE /////////////////////////////////// */
private:
   int      mLastEnergy;
   int      mSamplesPerSecond;
   int      mMode;
   VadInst *mpVad;
};

#endif // _MpVadWebRTC_h_
