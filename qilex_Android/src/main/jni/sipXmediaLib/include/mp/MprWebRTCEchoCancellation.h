// Copyright (c) 2013 Redefine Sp. z o.o.
// Author: Maciej Sikora <maciej.sikora@redefine.pl>

#ifndef _MprWebRTCEchoCancellation_h_
#define _MprWebRTCEchoCancellation_h_

// SYSTEM INCLUDES
// APPLICATION INCLUDES
#include "mp/MpAudioResource.h"
#include "mp/MpBufPool.h"
#include "mp/MpResourceMsg.h"

// DEFINES
#if !defined(ANDROID) && defined(__ANDROID__)
#define ANDROID 1
#endif

// MACROS
// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// CONSTANTS
// STRUCTS
// TYPEDEFS
// FORWARD DECLARATIONS

/// The "WebRTC Echo Cancellation" media processing resource.
/**
 *  Uses Google's implementation of acoustic echo control for mobile devices
 *  used in their WebRTC voice engine implementation.
 */
class MprWebRTCEchoCancellation : public MpAudioResource
{
/* //////////////////////////// PUBLIC //////////////////////////////////// */
public:

   static const UtlContainableType TYPE; ///< Class name, used for run-time checks.

   enum {
#ifdef WIN32
      DEFAULT_ECHO_QUEUE_LEN=90,
#elif defined(ANDROID)
      DEFAULT_ECHO_QUEUE_LEN=300,
#else
      DEFAULT_ECHO_QUEUE_LEN=90,
#endif
      DEFAULT_NLP_MODE=2,
      DEFAULT_NOISE_SUPPRESSION_MODE=3
   };

   enum GlobalEnableState {
      GLOBAL_ENABLE,   ///< All MprWebRTCEchoCancellation resources are forced to enable
      GLOBAL_DISABLE,  ///< All MprWebRTCEchoCancellation resources are forced to disable
      LOCAL_MODE       ///< Each resource respect its own enable/disable state
   };

/* ============================ CREATORS ================================== */
///@name Creators
//@{

     /// Constructor
   MprWebRTCEchoCancellation(const UtlString& rName,
                              OsMsgQ* pSpkrQ=NULL,
                              int spkrQDelayMs=DEFAULT_ECHO_QUEUE_LEN,
                              int nlpMode=DEFAULT_NLP_MODE,
                              bool suppressNoise=true,
                              int noiseMode=DEFAULT_NOISE_SUPPRESSION_MODE);

     /**<
     *  @param[in] rName - resource name.
     *  @param[in] pSpkrQ - pointer to a queue with speaker audio data.
     *  @param[in] spkrQDelayMs - number of milliseconds we should delay
     *             delay data from speaker queue. Set this value to maximum
     *             of the delay audio device will *definitely* add while
     *             playing and recording audio. This value is used to minimize
     *             delay between far and near end audio in AEC and reduce
     *             computational cost of AEC.
     */

     /// Destructor
   virtual
   ~MprWebRTCEchoCancellation();

//@}

/* ============================ MANIPULATORS ============================== */
///@name Manipulators
//@{

     /// Send message to enable/disable copy queue.
   OsStatus setSpkrQ(const UtlString& namedResource, OsMsgQ& fgQ, OsMsgQ *pSpkrQ);

     /// Send a message setting new nlp stage.
   OsStatus setNlpMode(const UtlString& namedResource, OsMsgQ& fgQ, int nlpMode);

     /// Send a message enabling or disabling noise suppression.
   OsStatus setSuppressNoise(const UtlString& namedResource, OsMsgQ& fgQ, bool suppressNoise);

     /// Send a message setting new noise suppression mode.
   OsStatus setNoiseMode(const UtlString& namedResource, OsMsgQ& fgQ, int noiseMode);

   /// Send a message setting new speaker queue delay.
   OsStatus setSpkrQDelayMs(const UtlString& namedResource, OsMsgQ& fgQ, int noiseMode);

     /// Set global enable/disable state.
   static inline void setGlobalEnableState(GlobalEnableState state);

     /// Get global state.
   static inline GlobalEnableState getGlobalEnableState();

//@}

/* ============================ ACCESSORS ================================= */
///@name Accessors
//@{

     /// @copydoc UtlContainable::getContainableType()
   UtlContainableType getContainableType() const;

     /// Return currently selected echo control mode.
   inline int getNlpMode() const;

     /// Return state of noise suppressor, whether it is enabled or not.
   inline bool getSuppressNoise() const;

     /// Return noise suppression mode.
   inline int getNoiseMode() const;

//@}

/* ============================ INQUIRY =================================== */
///@name Inquiry
//@{

//@}

/* //////////////////////////// PROTECTED ///////////////////////////////// */
protected:

/* //////////////////////////// PRIVATE /////////////////////////////////// */
private:

   typedef enum
   {
      MPRM_SET_SPEAKER_QUEUE = MpResourceMsg::MPRM_EXTERNAL_MESSAGE_START,
      MPRM_SET_NLP_MODE,
      MPRM_SET_SUPPRESS_NOISE,
      MPRM_SET_NOISE_SUPPRESSION_MODE,
      MPRM_SET_SPEAKER_QUEUE_DELAY
   } AddlResMsgTypes;

   NsxHandle     *mpNsx;             ///< Noise suppression module instance.
   void          *mpAec;             ///< AEC module instance.
   OsMsgQ        *mpSpkrQ;           ///< Queue with echo reference (far end) data.
   int		      mSpkrQDelayMs;     ///< How much should we delay data from mpSpkrQ (in ms)?
   bool           mSuppressNoise;    ///< Is noise suppression enabled?
   int            mNoiseMode;        ///< Noise suppression mode (from 0 to 3).

   /*
    * Non-linear processing stage (kAecNlpConservative - 0, kAecNlpModerate - 1,
    * kAecNlpAggressive - 2)
    */
   int            mNlpMode;
   MpAudioBufPtr  mpSilenceBuf;      ///< Buffer with silence - used when empty message
                                     ///< arrives from mpSpkrQ or mic input

   static volatile GlobalEnableState smGlobalEnableState;
     ///< Global enable/disable switch for all Speex AEC resources. We need
     ///< this switch because sipXmediaAdapterLib exports only a static method
     ///< to turn AEC on and off.

   virtual UtlBoolean doProcessFrame(MpBufPtr inBufs[],
                                     MpBufPtr outBufs[],
                                     int inBufsSize,
                                     int outBufsSize,
                                     UtlBoolean isEnabled,
                                     int samplesPerFrame,
                                     int samplesPerSecond);

     /// @copydoc MpResource::handleMessage()
   virtual UtlBoolean handleMessage(MpResourceMsg& rMsg);

     /// @brief Associates this resource with the indicated flow graph.
   virtual OsStatus setFlowGraph(MpFlowGraphBase* pFlowGraph);
     /**<
     *  We use this overloaded method for initialization of some of our member
     *  variables, which depend on flowgraph's properties (like frame size).
     *
     *  @retval OS_SUCCESS - for now, this method always returns success
     */

     /// Copy constructor (not implemented for this class)
   MprWebRTCEchoCancellation(const MprWebRTCEchoCancellation& rMprWebRTCEchoCancellation);

     /// Assignment operator (not implemented for this class)
   MprWebRTCEchoCancellation& operator=(const MprWebRTCEchoCancellation& rhs);

};

/* ============================ INLINE METHODS ============================ */

void MprWebRTCEchoCancellation::setGlobalEnableState(GlobalEnableState state)
{
   smGlobalEnableState = state;
}

MprWebRTCEchoCancellation::GlobalEnableState  MprWebRTCEchoCancellation::getGlobalEnableState()
{
   return(smGlobalEnableState);
}

int MprWebRTCEchoCancellation::getNlpMode() const
{
   return mNlpMode;
}

bool MprWebRTCEchoCancellation::getSuppressNoise() const
{
   return mSuppressNoise;
}

int MprWebRTCEchoCancellation::getNoiseMode() const
{
   return mNoiseMode;
}

#endif  // _MprWebRTCEchoCancellation_h_
