// Copyright (c) 2013 Redefine Sp. z o.o.
// Author: Andrzej K. Haczewski <ahaczewski@gmail.com>

#ifndef _MprWebRTCEchoControlMobile_h_
#define _MprWebRTCEchoControlMobile_h_

// SYSTEM INCLUDES
// APPLICATION INCLUDES
#include "mp/MpAudioResource.h"
#include "mp/MpBufPool.h"
#include "mp/MpResourceMsg.h"

// DEFINES
#if !defined(ANDROID) && defined(__ANDROID__)
#define ANDROID 1
#endif

// MACROS
// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// CONSTANTS
// STRUCTS
// TYPEDEFS
// FORWARD DECLARATIONS
typedef struct NsxHandleT NsxHandle;

/// The "WebRTC Echo Control Mobile" media processing resource.
/**
 *  Uses Google's implementation of acoustic echo control for mobile devices
 *  used in their WebRTC voice engine implementation.
 */
class MprWebRTCEchoControlMobile : public MpAudioResource
{
/* //////////////////////////// PUBLIC //////////////////////////////////// */
public:

   enum {
#ifdef WIN32
      DEFAULT_ECHO_MODE=2,
      DEFAULT_ECHO_QUEUE_LEN=90,
#elif defined(ANDROID)
      DEFAULT_ECHO_MODE=2,
      DEFAULT_ECHO_QUEUE_LEN=60,
#else
      DEFAULT_ECHO_MODE=2,
      DEFAULT_ECHO_QUEUE_LEN=90,
#endif
      DEFAULT_NOISE_SUPPRESSION_MODE=3
   };

   enum GlobalEnableState {
      GLOBAL_ENABLE,   ///< All MprWebRTCEchoControlMobile resources are forced to enable
      GLOBAL_DISABLE,  ///< All MprWebRTCEchoControlMobile resources are forced to disable
      LOCAL_MODE       ///< Each resource respect its own enable/disable state
   };

/* ============================ CREATORS ================================== */
///@name Creators
//@{

     /// Constructor
   MprWebRTCEchoControlMobile(const UtlString& rName,
                              OsMsgQ* pSpkrQ=NULL,
                              int spkrQDelayMs=DEFAULT_ECHO_QUEUE_LEN,
                              int echoMode=DEFAULT_ECHO_MODE,
                              bool suppressNoise=true,
                              int noiseMode=DEFAULT_NOISE_SUPPRESSION_MODE);
     /**<
     *  @param[in] rName - resource name.
     *  @param[in] pSpkrQ - pointer to a queue with speaker audio data.
     *  @param[in] spkrQDelayMs - number of milliseconds we should delay
     *             delay data from speaker queue. Set this value to maximum
     *             of the delay audio device will *definitely* add while
     *             playing and recording audio. This value is used to minimize
     *             delay between far and near end audio in AEC and reduce
     *             computational cost of AEC.
     */

     /// Destructor
   virtual
   ~MprWebRTCEchoControlMobile();

//@}

/* ============================ MANIPULATORS ============================== */
///@name Manipulators
//@{

     /// Send message to enable/disable copy queue.
   OsStatus setSpkrQ(const UtlString& namedResource,
                     OsMsgQ& fgQ,
                     OsMsgQ *pSpkrQ);
     /**<
      *  @param[in] namedResource - the name of the resource to send a message to.
      *  @param[in] fgQ - the queue of the flowgraph containing the resource which
      *             the message is to be received by.
      *  @param[in] pSpkrQ - pointer to a queue with speaker data.
      *  @returns the result of attempting to queue the message to this resource.
      */

     /// Send a message setting new echo control mode.
   OsStatus setEchoMode(const UtlString& namedResource,
                        OsMsgQ& fgQ,
                        int echoMode);
     /**<
      *  @param[in] namedResource - the name of the resource to send a message to.
      *  @param[in] fgQ - the queue of the flowgraph containing the resource which
      *             the message is to be received by.
      *  @param[in] echoMode - echo control mode.
      *  @returns the result of attempting to queue the message to this resource.
      */

      /// Send a message enabling or disabling noise suppression.
   OsStatus setSuppressNoise(const UtlString& namedResource,
                             OsMsgQ& fgQ,
                             bool suppressNoise);
      /**<
       *  @param[in] namedResource - the name of the resource to send a message to.
       *  @param[in] fgQ - the queue of the flowgraph containing the resource which
       *             the message is to be received by.
       *  @param[in] suppressNoise - whether to enable noise suppression or not.
       *  @returns the result of attempting to queue the message to this resource.
       */

      /// Send a message setting new noise suppression mode.
    OsStatus setNoiseMode(const UtlString& namedResource,
                          OsMsgQ& fgQ,
                          int noiseMode);
      /**<
       *  @param[in] namedResource - the name of the resource to send a message to.
       *  @param[in] fgQ - the queue of the flowgraph containing the resource which
       *             the message is to be received by.
       *  @param[in] noiseMode - mode of noise suppression (from 0 to 3, 3 being most aggressive).
       *  @returns the result of attempting to queue the message to this resource.
       */

     /// Set global enable/disable state.
   static inline void setGlobalEnableState(GlobalEnableState state);
     /**<
     *  @see smGlobalEnableState for more details.
     */

     /// Get global state.
   static inline GlobalEnableState getGlobalEnableState();
     /**<
     *  @see smGlobalEnableState for more details.
     */

//@}

/* ============================ ACCESSORS ================================= */
///@name Accessors
//@{

     /// Return currently selected echo control mode.
   inline int getEchoMode() const;

     /// Return state of noise suppressor, whether it is enabled or not.
   inline bool getSuppressNoise() const;

     /// Return noise suppression mode.
   inline int getNoiseMode() const;


//@}

/* ============================ INQUIRY =================================== */
///@name Inquiry
//@{

//@}

/* //////////////////////////// PROTECTED ///////////////////////////////// */
protected:

/* //////////////////////////// PRIVATE /////////////////////////////////// */
private:

   typedef enum
   {
      MPRM_SET_SPEAKER_QUEUE = MpResourceMsg::MPRM_EXTERNAL_MESSAGE_START,
      MPRM_SET_ECHO_MODE,
      MPRM_SET_SUPPRESS_NOISE,
      MPRM_SET_NOISE_SUPPRESSION_MODE
   } AddlResMsgTypes;

   NsxHandle     *mpNsx;             ///< Noise suppression module instance.
   void          *mpAecm;            ///< AECM module instance.
   bool           mStartedCanceling; ///< Have we started cancelling?
   OsMsgQ        *mpSpkrQ;           ///< Queue with echo reference (far end) data.
   const int      mSpkrQDelayMs;     ///< How much should we delay data from mpSpkrQ (in ms)?
   int            mEchoMode;         ///< Echo control mode (from 0 to 4).
   bool           mSuppressNoise;    ///< Is noise suppression enabled?
   int            mNoiseMode;        ///< Noise suppression mode (from 0 to 3).
   size_t         mFarEndSamples;    ///< Number of buffered Far End samples.
   MpAudioBufPtr  mpSilenceBuf;      ///< Buffer with silence - used when empty message
                                     ///< arrives from mpSpkrQ or mic input

   static volatile GlobalEnableState smGlobalEnableState;
     ///< Global enable/disable switch for all Speex AEC resources. We need
     ///< this switch because sipXmediaAdapterLib exports only a static method
     ///< to turn AEC on and off.

   virtual UtlBoolean doProcessFrame(MpBufPtr inBufs[],
                                     MpBufPtr outBufs[],
                                     int inBufsSize,
                                     int outBufsSize,
                                     UtlBoolean isEnabled,
                                     int samplesPerFrame,
                                     int samplesPerSecond);

     /// @copydoc MpResource::handleMessage()
   virtual UtlBoolean handleMessage(MpResourceMsg& rMsg);

     /// @brief Associates this resource with the indicated flow graph.
   virtual OsStatus setFlowGraph(MpFlowGraphBase* pFlowGraph);
     /**<
     *  We use this overloaded method for initialization of some of our member
     *  variables, which depend on flowgraph's properties (like frame size).
     *
     *  @retval OS_SUCCESS - for now, this method always returns success
     */

     /// Copy constructor (not implemented for this class)
   MprWebRTCEchoControlMobile(
         const MprWebRTCEchoControlMobile& rMprWebRTCEchoControlMobile);

     /// Assignment operator (not implemented for this class)
   MprWebRTCEchoControlMobile& operator=(const MprWebRTCEchoControlMobile& rhs);

};

/* ============================ INLINE METHODS ============================ */

void MprWebRTCEchoControlMobile::setGlobalEnableState(GlobalEnableState state)
{
   smGlobalEnableState = state;
}

MprWebRTCEchoControlMobile::GlobalEnableState  MprWebRTCEchoControlMobile::getGlobalEnableState()
{
   return(smGlobalEnableState);
}

int MprWebRTCEchoControlMobile::getEchoMode() const
{
   return mEchoMode;
}

bool MprWebRTCEchoControlMobile::getSuppressNoise() const
{
   return mSuppressNoise;
}

int MprWebRTCEchoControlMobile::getNoiseMode() const
{
   return mNoiseMode;
}

#endif  // _MprWebRTCEchoControlMobile_h_
