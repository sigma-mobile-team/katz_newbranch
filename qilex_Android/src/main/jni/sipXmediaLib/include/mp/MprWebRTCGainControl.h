//
// Copyright (C) 2008 SIPfoundry Inc.
// Licensed by SIPfoundry under the LGPL license.
//
// Copyright (C) 2008 SIPez LLC.
// Licensed to SIPfoundry under a Contributor Agreement.
//
// $$
//////////////////////////////////////////////////////////////////////////////

// Author: Andrzej K. Haczewski <ahaczewski AT gmail DOT com>

#ifndef _MprWebRTCGainControl_h_
#define _MprWebRTCGainControl_h_

// SYSTEM INCLUDES
// APPLICATION INCLUDES
#include "mp/MpAudioResource.h"
#include "mp/MpResourceMsg.h"

// DEFINES
// MACROS
// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// STRUCTS
// TYPEDEFS
// FORWARD DECLARATIONS

/**
*  @brief Gain Control based on WebRTC AGC module.
*
*  @nosubgrouping
*/
class MprWebRTCGainControl : public MpAudioResource
{
/* //////////////////////////////// PUBLIC //////////////////////////////// */
public:

   enum GainControlMode {
      MODE_UNCHANGED = 0,       ///< Gain will not be changed.
      MODE_ADAPTIVE_ANALOG,     ///< Adaptive analog automatic gain control (-3dBOv).
                                ///< Use with analog mic gain control.
                                ///< FIXME: DO NOT USE ON ANDROID!
      MODE_ADAPTIVE_DIGITAL,    ///< Adaptive digital automatic gain control (-3dBOv).
      MODE_FIXED_DIGITAL        ///< Fixed digital gain (0dB).
   };

   enum {
      DEFAULT_MODE = MODE_ADAPTIVE_DIGITAL,
   };

/* =============================== CREATORS =============================== */
///@name Creators
//@{

     /// Constructor
   MprWebRTCGainControl(const UtlString& rName,
                        int mode=DEFAULT_MODE);
     /**<
     *  @param[in] mode - automatic gain control mode.
     */

     /// Destructor
   ~MprWebRTCGainControl();

//@}

/* ============================= MANIPULATORS ============================= */
///@name Manipulators
//@{

     /// Change automatic gain control mode.
   static
   OsStatus changeMode(const UtlString& namedResource,
                       OsMsgQ& fgQ,
                       int mode);
     /**<
      *  @note Changing gain control mode requires full restart of AGC.
      *  @param[in] namedResource - the name of the resource to send a message to.
      *  @param[in] fgQ - the queue of the flowgraph containing the resource which
      *             the message is to be received by.
      *  @param[in] mode - mode of automatic gain control. See GainControlMode.
      *  @returns the result of attempting to queue the message to this resource.
      */

     /// Set target gain level.
   static
   OsStatus setTargetLevel(const UtlString& namedResource,
                           OsMsgQ& fgQ,
                           int targetLevel);
     /**<
      *  @param[in] namedResource - the name of the resource to send a message to.
      *  @param[in] fgQ - the queue of the flowgraph containing the resource which
      *             the message is to be received by.
      *  @param[in] targetLevel - target gain level in dBfs.
      *  @returns the result of attempting to queue the message to this resource.
      */

     /// Set compression gain.
   static
   OsStatus setCompressionGain(const UtlString& namedResource,
                               OsMsgQ& fgQ,
                               int compressionGain);
     /**<
      *  @param[in] namedResource - the name of the resource to send a message to.
      *  @param[in] fgQ - the queue of the flowgraph containing the resource which
      *             the message is to be received by.
      *  @param[in] compressionGain - compression gain in dB.
      *  @returns the result of attempting to queue the message to this resource.
      */

     /// Enable or disable limiter.
   static
   OsStatus enableLimiter(const UtlString& namedResource,
                          OsMsgQ& fgQ,
                          UtlBoolean enabled);
     /**<
      *  @param[in] namedResource - the name of the resource to send a message to.
      *  @param[in] fgQ - the queue of the flowgraph containing the resource which
      *             the message is to be received by.
      *  @param[in] enabled - should limiter be enabled?
      *  @returns the result of attempting to queue the message to this resource.
      */

//@}

/* ============================== ACCESSORS =============================== */
///@name Accessors
//@{


//@}

/* =============================== INQUIRY ================================ */
///@name Inquiry
//@{


//@}

/* ////////////////////////////// PROTECTED /////////////////////////////// */
protected:

   enum
   {
      MPRM_CHANGE_MODE = MpResourceMsg::MPRM_EXTERNAL_MESSAGE_START,
      MPRM_SET_TARGET_LEVEL,
      MPRM_SET_COMPRESSION_GAIN,
      MPRM_ENABLE_LIMITER,
      MPRM_DISABLE_LIMITER,
   };

     /// @copydoc MpAudioResource::doProcessFrame
   UtlBoolean doProcessFrame(MpBufPtr inBufs[],
                             MpBufPtr outBufs[],
                             int inBufsSize,
                             int outBufsSize,
                             UtlBoolean isEnabled,
                             int samplesPerFrame,
                             int samplesPerSecond);

     /// @copydoc MpResource::handleMessage
   UtlBoolean handleMessage(MpResourceMsg& rMsg);

     /// Handle MPRM_CHANGE_MODE message.
   UtlBoolean handleChangeMode(int mode);

     /// Handle MPRM_SET_TARGET_LEVEL message.
   UtlBoolean handleSetTargetLevel(int targetLevel);

     /// Handle MPRM_SET_COMPRESSION_GAIN message.
   UtlBoolean handleSetCompressionGain(int compressionGain);

     /// Handle MPRM_ENABLE_LIMITER and MPRM_DISABLE_LIMITER messages.
   UtlBoolean handleLimiterEnabled(UtlBoolean enabled);

     /// @copydoc MpResource::setFlowGraph()
   OsStatus setFlowGraph(MpFlowGraphBase* pFlowGraph);

/* /////////////////////////////// PRIVATE //////////////////////////////// */
private:
     /// Sets mMode bounding mode to valid range.
   void setMode(int mode);

     /// Initializes WebRTC AGC module.
   OsStatus initAgc();

     /// Frees WebRTC AGC module.
   void freeAgc();

   void      *mpAgc;
   int        mMode;
   int        mTargetLevel_dBfs;
   int        mCompressionGain_dB;
   UtlBoolean mIsLimiterEnabled;
   int        mMicLevel;
};

/* ============================ INLINE METHODS ============================ */

#endif  // _MprWebRTCGainControl_h_
