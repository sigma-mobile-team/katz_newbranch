//
// Copyright (C) 2008 SIPfoundry Inc.
// Licensed by SIPfoundry under the LGPL license.
//
// Copyright (C) 2008 SIPez LLC.
// Licensed to SIPfoundry under a Contributor Agreement.
//
// $$
//////////////////////////////////////////////////////////////////////////////

// Author: Andrzej K. Haczewski <ahaczewski AT gmail DOT com>

// SYSTEM INCLUDES
#include <os/OsIntTypes.h>
#include <assert.h>
#include <limits>
#include <webrtc/modules/audio_processing/agc/include/gain_control.h>

// APPLICATION INCLUDES
#include <mp/MprWebRTCGainControl.h>
#include <mp/MpIntResourceMsg.h>
#include <mp/MprnIntMsg.h>
#include <mp/MpFlowGraphBase.h>

// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// CONSTANTS
#define MIN_CAPTURE_LEVEL       0
#define MAX_CAPTURE_LEVEL       255
#define DEFAULT_TARGET_LEVEL    6
#define DEFAULT_COMP_GAIN       20

// TYPEDEFS
// DEFINES
// MACROS
// STATIC VARIABLE INITIALIZATIONS

/* //////////////////////////////// PUBLIC //////////////////////////////// */

MprWebRTCGainControl::MprWebRTCGainControl(const UtlString& rName,
                                           int mode)
: MpAudioResource(rName, 1, 1, 0, 1)
, mpAgc(NULL)
, mMode(0)
, mTargetLevel_dBfs(DEFAULT_TARGET_LEVEL)
, mCompressionGain_dB(DEFAULT_COMP_GAIN)
, mIsLimiterEnabled(TRUE)
, mMicLevel(MAX_CAPTURE_LEVEL)
{
   setMode(mode);
}

MprWebRTCGainControl::~MprWebRTCGainControl()
{
   freeAgc();
}

/* =============================== CREATORS =============================== */

/* ============================= MANIPULATORS ============================= */

OsStatus MprWebRTCGainControl::changeMode(const UtlString& namedResource,
                                          OsMsgQ& fgQ,
                                          int mode)
{
   MpIntResourceMsg msg((MpResourceMsg::MpResourceMsgType)MPRM_CHANGE_MODE,
                        namedResource,
                        mode);
   return fgQ.send(msg);
}

OsStatus MprWebRTCGainControl::setTargetLevel(const UtlString& namedResource,
                                              OsMsgQ& fgQ,
                                              int targetLevel)
{
   MpIntResourceMsg msg((MpResourceMsg::MpResourceMsgType)MPRM_SET_TARGET_LEVEL,
                        namedResource,
                        targetLevel);
   return fgQ.send(msg);
}

OsStatus MprWebRTCGainControl::setCompressionGain(const UtlString& namedResource,
                                                  OsMsgQ& fgQ,
                                                  int compressionGain)
{
   MpIntResourceMsg msg((MpResourceMsg::MpResourceMsgType)MPRM_SET_COMPRESSION_GAIN,
                        namedResource,
                        compressionGain);
   return fgQ.send(msg);
}

OsStatus MprWebRTCGainControl::enableLimiter(const UtlString& namedResource,
                                             OsMsgQ& fgQ,
                                             UtlBoolean enabled)
{
   MpResourceMsg msg(enabled ? (MpResourceMsg::MpResourceMsgType)MPRM_ENABLE_LIMITER :
                     (MpResourceMsg::MpResourceMsgType)MPRM_DISABLE_LIMITER,
                     namedResource);
   return fgQ.send(msg);
}

/* ============================== ACCESSORS =============================== */

/* =============================== INQUIRY ================================ */

/* ////////////////////////////// PROTECTED /////////////////////////////// */

UtlBoolean MprWebRTCGainControl::doProcessFrame(MpBufPtr inBufs[],
                                                MpBufPtr outBufs[],
                                                int inBufsSize,
                                                int outBufsSize,
                                                UtlBoolean isEnabled,
                                                int samplesPerFrame,
                                                int samplesPerSecond)
{
   // We're disabled or have nothing to process.
   if ((outBufsSize == 0) || (inBufsSize == 0))
   {
      return TRUE;
   }

   if (!isEnabled || (mpAgc == NULL))
   {
      outBufs[0].swap(inBufs[0]);
      return TRUE;
   }

   MpAudioBufPtr pBuf;

   pBuf.swap(inBufs[0]);

   // Send energy level notifications.
   if (pBuf.isValid())
   {
      bool writtable = pBuf.requestWrite();

      if (writtable && (mMode != MODE_UNCHANGED))
      {
         MpAudioSample* samples = pBuf->getSamplesWritePtr();
         assert(samples != NULL);
         uint16_t samplesNumber = pBuf->getSamplesNumber();

         int result = 0;

         // Analysis stage depends on mode.
         if (mMode == MODE_ADAPTIVE_ANALOG)
         {
            result = WebRtcAgc_AddMic(mpAgc, samples, NULL, samplesNumber);
         }
         else
         {
            int32_t newMicLevel = mMicLevel;
            result = WebRtcAgc_VirtualMic(
                  mpAgc, samples, NULL, samplesNumber, mMicLevel,
                  &newMicLevel);
            if (result >= 0)
            {
               mMicLevel = newMicLevel;
            }
         }

         // Processing stage.
         if (result >= 0)
         {
            int32_t newMicLevel = mMicLevel;
            uint8_t saturation = 0;
            uint16_t echo = pBuf->getEchoPresent() || (pBuf->getSpeechType() != MP_SPEECH_ACTIVE && pBuf->getSpeechType() != MP_SPEECH_UNKNOWN) ? 1 : 0;

            result = WebRtcAgc_Process(
                  mpAgc, samples, NULL, samplesNumber, samples, NULL, mMicLevel,
                  &newMicLevel, echo, &saturation);
         }
      }
   }

   outBufs[0].swap(pBuf);

   return TRUE;
}

UtlBoolean MprWebRTCGainControl::handleMessage(MpResourceMsg& rMsg)
{
   UtlBoolean msgHandled = FALSE;

   switch (rMsg.getMsg())
   {
   case MPRM_CHANGE_MODE:
      {
         MpIntResourceMsg *pMsg = (MpIntResourceMsg*)&rMsg;
         msgHandled = handleChangeMode(pMsg->getData());
      }
      break;

   case MPRM_SET_TARGET_LEVEL:
      {
         MpIntResourceMsg *pMsg = (MpIntResourceMsg*)&rMsg;
         msgHandled = handleSetTargetLevel(pMsg->getData());
      }
      break;

   case MPRM_SET_COMPRESSION_GAIN:
      {
         MpIntResourceMsg *pMsg = (MpIntResourceMsg*)&rMsg;
         msgHandled = handleSetCompressionGain(pMsg->getData());
      }
      break;

   case MPRM_ENABLE_LIMITER:
      msgHandled = handleLimiterEnabled(TRUE);
      break;

   case MPRM_DISABLE_LIMITER:
      msgHandled = handleLimiterEnabled(FALSE);
      break;

   default:
      msgHandled = MpResource::handleMessage(rMsg);
      break;
   }

   return msgHandled;
}

UtlBoolean MprWebRTCGainControl::handleChangeMode(int mode)
{
   if ((mpFlowGraph != NULL) && (mpAgc != NULL))
   {
      // Save current configuration since it will get overwritten by Init().
      WebRtcAgc_config_t config;
      int result = WebRtcAgc_get_config(mpAgc, &config);
      if (result < 0)
         return TRUE;

      result = WebRtcAgc_Init(
            mpAgc, MIN_CAPTURE_LEVEL, MAX_CAPTURE_LEVEL, mode,
            mpFlowGraph->getSamplesPerSec());
      // On error we do not update mMode, just notify that we have handled
      // the message.
      if (result < 0)
         return TRUE;

      // Restore configuration.
      if (WebRtcAgc_set_config(mpAgc, config) < 0)
         return TRUE;
   }
   mMode = mode;
   return TRUE;
}

UtlBoolean MprWebRTCGainControl::handleSetTargetLevel(int targetLevel)
{
   if (targetLevel > std::numeric_limits<int16_t>::max())
      targetLevel = std::numeric_limits<int16_t>::max();
   if (targetLevel < std::numeric_limits<int16_t>::min())
      targetLevel = std::numeric_limits<int16_t>::min();

   if (mpAgc != NULL)
   {
      WebRtcAgc_config_t config;
      if (WebRtcAgc_get_config(mpAgc, &config) < 0)
         return TRUE;

      config.targetLevelDbfs = static_cast<int16_t>(targetLevel);
      if (WebRtcAgc_set_config(mpAgc, config) < 0)
         return TRUE;
   }
   mTargetLevel_dBfs = targetLevel;
   return TRUE;
}

UtlBoolean MprWebRTCGainControl::handleSetCompressionGain(int compressionGain)
{
   if (compressionGain > std::numeric_limits<int16_t>::max())
      compressionGain = std::numeric_limits<int16_t>::max();
   if (compressionGain < std::numeric_limits<int16_t>::min())
      compressionGain = std::numeric_limits<int16_t>::min();

   if (mpAgc != NULL)
   {
      WebRtcAgc_config_t config;
      if (WebRtcAgc_get_config(mpAgc, &config) < 0)
         return TRUE;

      config.compressionGaindB = static_cast<int16_t>(compressionGain);
      if (WebRtcAgc_set_config(mpAgc, config) < 0)
         return TRUE;
   }
   mCompressionGain_dB = compressionGain;
   return TRUE;
}

UtlBoolean MprWebRTCGainControl::handleLimiterEnabled(UtlBoolean enabled)
{
   if (mpAgc != NULL)
   {
      WebRtcAgc_config_t config;
      if (WebRtcAgc_get_config(mpAgc, &config) < 0)
         return TRUE;

      config.limiterEnable = enabled ? kAgcTrue : kAgcFalse;
      if (WebRtcAgc_set_config(mpAgc, config) < 0)
         return TRUE;
   }
   mIsLimiterEnabled = enabled;
   return TRUE;
}

OsStatus MprWebRTCGainControl::setFlowGraph(MpFlowGraphBase* pFlowGraph)
{
   OsStatus res =  MpAudioResource::setFlowGraph(pFlowGraph);

   if (res == OS_SUCCESS)
   {
      // Check whether we've been added to flowgraph or removed.
      if (pFlowGraph != NULL)
      {
         res = initAgc();
         if (res == OS_SUCCESS)
         {
            mMicLevel = MAX_CAPTURE_LEVEL;
         }
      }
      else
      {
         freeAgc();
      }
   }

   return res;
}

/* /////////////////////////////// PRIVATE //////////////////////////////// */

void MprWebRTCGainControl::setMode(int mode)
{
   mMode = (mode < MODE_UNCHANGED) ? MODE_UNCHANGED :
         ((mode > MODE_FIXED_DIGITAL) ? MODE_FIXED_DIGITAL : mode);
}

OsStatus MprWebRTCGainControl::initAgc()
{
   if (mpFlowGraph == NULL)
      return OS_FAILED;

   void* newAgc = NULL;
   if (WebRtcAgc_Create(&newAgc) < 0)
   {
      return OS_FAILED;
   }

   int ret = WebRtcAgc_Init(
         newAgc, MIN_CAPTURE_LEVEL, MAX_CAPTURE_LEVEL, mMode,
         mpFlowGraph->getSamplesPerSec());
   if (ret < 0)
   {
      WebRtcAgc_Free(newAgc);
      return OS_FAILED;
   }

   WebRtcAgc_config_t config;
   ret = WebRtcAgc_get_config(newAgc, &config);
   if (ret < 0)
   {
      WebRtcAgc_Free(newAgc);
      return OS_FAILED;
   }

   config.limiterEnable = mIsLimiterEnabled ? kAgcTrue : kAgcFalse;
   config.targetLevelDbfs = mTargetLevel_dBfs;
   config.compressionGaindB = mCompressionGain_dB;

   ret = WebRtcAgc_set_config(newAgc, config);
   if (ret < 0)
   {
      WebRtcAgc_Free(newAgc);
      return OS_FAILED;
   }

   if (mpAgc != NULL)
      freeAgc();
   mpAgc = newAgc;
   mMicLevel = MAX_CAPTURE_LEVEL;

   return OS_SUCCESS;
}

void MprWebRTCGainControl::freeAgc()
{
   if (mpAgc != NULL)
   {
      WebRtcAgc_Free(mpAgc);
      mpAgc = NULL;
   }
}

/* ============================== FUNCTIONS =============================== */
