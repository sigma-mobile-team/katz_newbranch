//
// Copyright (C) 2008 SIPez LLC.
// Licensed to SIPfoundry under a Contributor Agreement.
//
// Copyright (C) 2008 SIPfoundry Inc.
// Licensed by SIPfoundry under the LGPL license.
//
// $$
///////////////////////////////////////////////////////////////////////////////

// Author: Andrzej K. Haczewski <ahaczewski AT gmail DOT com>

// SYSTEM INCLUDES
#include <string.h>
#include <webrtc/common_audio/vad/include/webrtc_vad.h>

// APPLICATION INCLUDES
#include "mp/MpVadBase.h"
#include "mp/MpVadWebRTC.h"

// DEFINES
// CONSTANTS
// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// STATIC VARIABLE INITIALIZATIONS
const char *MpVadWebRTC::name = "WebRTC";

/* //////////////////////////// PUBLIC //////////////////////////////////// */

/* ============================ CREATORS ================================== */

MpVadWebRTC::MpVadWebRTC()
: mLastEnergy(0)
, mSamplesPerSecond(0)
, mMode(0)
, mpVad(NULL)
{
   setMode(DEFAULT_MODE);
}

OsStatus MpVadWebRTC::init(int samplesPerSec)
{
   mLastEnergy = 0;
   mSamplesPerSecond = samplesPerSec;

   if (mpVad != NULL)
   {
      WebRtcVad_Free(mpVad);
      mpVad = NULL;
   }

   if (WebRtcVad_Create(&mpVad) < 0)
   {
      return OS_FAILED;
   }

   if (WebRtcVad_Init(mpVad) < 0)
   {
      WebRtcVad_Free(mpVad);
      mpVad = NULL;

      return OS_FAILED;
   }

   // Ignoring return value of this call.
   // Failure to set requested VAD mode is not an error, because we can't
   // properly fail in setMode() method below in the same case.
   // This ensures consistent behavior.
   WebRtcVad_set_mode(mpVad, mMode);

   return OS_SUCCESS;
}

MpVadWebRTC::~MpVadWebRTC()
{
   if (mpVad != NULL)
   {
      WebRtcVad_Free(mpVad);
      mpVad = NULL;
   }
}

/* ============================ MANIPULATORS ============================== */

void MpVadWebRTC::setMode(int mode)
{
   mMode = (mode < 0) ? 0 : ((mode > 3) ? 3 : mode);

   if (mpVad != NULL)
   {
      WebRtcVad_set_mode(mpVad, mMode);
   }
}

int MpVadWebRTC::getEnergy() const
{
   if (mSamplesPerSecond == 0)
      return 0;
   return mLastEnergy;
}

MpSpeechType MpVadWebRTC::processFrame(uint32_t packetTimeStamp,
                                       const MpAudioSample* pBuf,
                                       unsigned inSamplesNum,
                                       const MpSpeechParams &speechParams,
                                       UtlBoolean calcEnergyOnly)
{
   if ((mSamplesPerSecond == 0) || (mpVad == NULL))
      return MP_SPEECH_UNKNOWN;

   if (WebRtcVad_ValidRateAndFrameLength(mSamplesPerSecond, inSamplesNum) < 0)
      return MP_SPEECH_UNKNOWN;

   int total_energy = 0;
   int result = WebRtcVad_Process(mpVad, mSamplesPerSecond, pBuf, inSamplesNum,
         &total_energy);

   if (result < 0)
      return MP_SPEECH_UNKNOWN;

   mLastEnergy = total_energy;

   if (calcEnergyOnly)
   {
      return speechParams.mSpeechType;
   }

   if (result > 0)
      return MP_SPEECH_ACTIVE;

   return MP_SPEECH_SILENT;
}

OsStatus MpVadWebRTC::setParam(const char* paramName, void* value)
{
   if (strcmp(paramName, "Mode") == 0)
   {
      setMode(*(int*)value);
      return OS_SUCCESS;
   }
   return OS_FAILED;
}

void MpVadWebRTC::reset()
{
   mLastEnergy = 0;
}
