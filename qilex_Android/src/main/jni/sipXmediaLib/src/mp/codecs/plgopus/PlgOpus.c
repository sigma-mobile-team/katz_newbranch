// APPLICATION INCLUDES
#include <mp/codecs/PlgDefsV1.h>

#include <opus.h>
#include <android/log.h>
#include <stdlib.h>

#define OPUS_MIN_BITRATE 6000
#define OPUS_MAX_BITRATE 510000
#define OPUS_SAMPLING_RATE 16000 //according to RFC should be 48000, but it complicates things
#define OPUS_CHANNELS 1
#define OPUS_SAMPLES_FRAME 320 // frame size is 20 ms, sampling rate 16kHz
#define OPUS_MIN_FRAME_BYTES 120 // bit rate 6 kb/s
#define OPUS_MAX_FRAME_BYTES 10200 // bit rate 510 kb/s

// TODO for tests
#ifdef SIPXLOGI
#undef SIPXLOGI
#endif
#define SIPXLOGI(...) __android_log_print(4, "PLGOPUS", __VA_ARGS__) // 4 = INFO


static const struct MppCodecInfoV1_1 sgCodecInfo =
{
///////////// Implementation and codec info /////////////
    "opus",                      // codecManufacturer
    "opus",                      // codecName
    "opus",                      // codecVersion
    CODEC_TYPE_FRAME_BASED,      // codecType

/////////////////////// SDP info ///////////////////////
    "opus",                      // mimeSubtype
    0,                           // fmtpsNum
    NULL,                        // fmtps
    OPUS_SAMPLING_RATE,          // sampleRate
    OPUS_CHANNELS,               // numChannels
    CODEC_FRAME_PACKING_NONE     // framePacking
};

/* ============================== FUNCTIONS =============================== */

CODEC_API int PLG_GET_INFO_V1_1(opus)(const struct MppCodecInfoV1_1 **codecInfo)
{
    SIPXLOGI("PlgOpus::opus_get_info start");
    if (codecInfo)
    {
        *codecInfo = &sgCodecInfo;
    }
    SIPXLOGI("PlgOpus::opus_get_info success");
    return RPLG_SUCCESS;
}

CODEC_API void *PLG_INIT_V1_2(opus)(const char* fmtp, int isDecoder,
                                      struct MppCodecFmtpInfoV1_2* pCodecInfo)
{
    SIPXLOGI("PlgOpus::opus_init start");
    if (pCodecInfo == NULL)
    {
        return NULL;
    }

    pCodecInfo->signalingCodec = FALSE;
    pCodecInfo->minBitrate = OPUS_MIN_BITRATE;
    pCodecInfo->maxBitrate = OPUS_MAX_BITRATE;
    pCodecInfo->numSamplesPerFrame = OPUS_SAMPLES_FRAME;
    pCodecInfo->minFrameBytes = OPUS_MIN_FRAME_BYTES;
    pCodecInfo->maxFrameBytes = OPUS_MAX_FRAME_BYTES;
    pCodecInfo->packetLossConcealment = CODEC_PLC_INTERNAL;
    pCodecInfo->vadCng = CODEC_CNG_INTERNAL;
    pCodecInfo->algorithmicDelay = 0;

    if (isDecoder)
    {
        OpusDecoder *decoder = opus_decoder_create(OPUS_SAMPLING_RATE,
                OPUS_CHANNELS, NULL);
        SIPXLOGI("PlgOpus:opus_get_init returning decoder handle");
        return decoder;
    }
    else
    {
        OpusEncoder *encoder = opus_encoder_create(OPUS_SAMPLING_RATE,
                OPUS_CHANNELS, OPUS_APPLICATION_VOIP, NULL);
        SIPXLOGI("PlgOpus:opus_get_init returning encoder handle");
        return encoder;
    }
}

CODEC_API int PLG_FREE_V1(opus)(void* handle, int isDecoder)
{
    SIPXLOGI("PlgOpus::opus_free start");
    if (handle)
    {
        if (isDecoder)
        {
            opus_decoder_destroy((OpusDecoder *) handle);
            SIPXLOGI("PlgOpus::opus_free decoder freed");
        }
        else
        {
            opus_encoder_destroy((OpusEncoder *) handle);
            SIPXLOGI("PlgOpus::opus_free encoder freed");
        }
        return RPLG_SUCCESS;
    }
    return RPLG_INVALID_ARGUMENT;
}


CODEC_API int PLG_DECODE_V1(opus)(void* handle, const void* pCodedData,
        unsigned cbCodedPacketSize, void* pAudioBuffer, unsigned cbBufferSize,
        unsigned *pcbDecodedSize, const struct RtpHeader* pRtpHeader)
{
    if(handle)
    {
        // Reset number of decoded samples
        *pcbDecodedSize = 0;
        OpusDecoder *decoder = (OpusDecoder *) handle;

        unsigned char *data = (unsigned char *) pCodedData;
        opus_int32 len = (opus_int32) cbCodedPacketSize;

        opus_int16 *pcm = (opus_int16 *) pAudioBuffer;
        int frame_size = cbBufferSize / OPUS_CHANNELS;

        // If data == NULL, then it's a PLC
        int decoded_samples = opus_decode(decoder, data, len, pcm, frame_size, 0);
        *pcbDecodedSize = decoded_samples * SIZE_OF_SAMPLE;
        return RPLG_SUCCESS;
    }
    else
        return RPLG_INVALID_ARGUMENT;

}

CODEC_API int PLG_ENCODE_V1(opus)(void* handle, const void* pAudioBuffer,
        unsigned cbAudioSamples, int* rSamplesConsumed, void* pCodedData,
        unsigned cbMaxCodedData, int* pcbCodedSize, unsigned* pbSendNow)
{
    if (handle)
    {
        const opus_int16 *pcm = (opus_int16 *) pAudioBuffer;
        int frame_size = cbAudioSamples / OPUS_CHANNELS;
        unsigned char *data = (unsigned char *) pCodedData;
        opus_int32 max_data_bytes = cbMaxCodedData;

        OpusEncoder *encoder = (OpusEncoder *) handle;
        int result = opus_encode(encoder, pcm, frame_size, data, max_data_bytes);

        if (result > 0)
        {
        	*rSamplesConsumed = cbAudioSamples;
        	*pcbCodedSize = result;
        	*pbSendNow = 1;
        	return RPLG_SUCCESS;
        }
        else
        {
        	*rSamplesConsumed = 0;
        	*pcbCodedSize = 0;
        	*pbSendNow = 0;
        	SIPXLOGI("PlgOpus::opus_encode error: %d", result);
        	return RPLG_FAILED;
        }
    }
    else
        return RPLG_INVALID_ARGUMENT;
}

CODEC_API int PLG_ENCODER_CTL(opus)(void* handle, int request, int value)
{
    SIPXLOGI("PlgOpus::opus_encoder_ctl - request=%d, value=%d", request, value);
    if (handle)
    {
    	OpusEncoder* encoder = (OpusEncoder*)handle;

    	switch (request)
    	{
    		case ENCODER_CTL_SET_BITRATE:
        	   	return opus_encoder_ctl(encoder, OPUS_SET_BITRATE(value));

        	default:
        		SIPXLOGI("PlgOpus::opus_encoder_ctl unhandled request: %d", request);
        		break;
    	}
    }
    else
        return OPUS_BAD_ARG;
}


DECLARE_FUNCS_V1(opus)

PLG_ENUM_CODEC_START(opus)
   PLG_ENUM_CODEC(opus)
   PLG_ENUM_CODEC_NO_SPECIAL_PACKING(opus)
   PLG_ENUM_CODEC_NO_SIGNALING(opus)
   PLG_ENUM_CODEC_CTL(opus)
PLG_ENUM_CODEC_END
