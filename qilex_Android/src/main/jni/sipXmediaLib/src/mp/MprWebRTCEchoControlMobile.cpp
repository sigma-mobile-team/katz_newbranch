// Copyright (c) 2013 Redefine Sp. z o.o.
// Author: Andrzej K. Haczewski <ahaczewski@gmail.com>

// SYSTEM INCLUDES
#ifdef __ANDROID__
#include <android/log.h>
#endif
#include <webrtc/modules/audio_processing/aecm/include/echo_control_mobile.h>
#include <webrtc/modules/audio_processing/ns/include/noise_suppression_x.h>

// APPLICATION INCLUDES
#include "os/OsDefs.h"
#include "os/OsSysLog.h"
#include "mp/MpMisc.h"
#include "mp/MpBuf.h"
#include "mp/MpBufPool.h"
#include "mp/MpBufferMsg.h"
#include "mp/MpPackedResourceMsg.h"
#include "mp/MpFlowGraphBase.h"
#include "mp/MprWebRTCEchoControlMobile.h"
#ifdef RTL_ENABLED
#  include <rtl_macro.h>
#  ifdef RTL_AUDIO_ENABLED
#     include <SeScopeAudioBuffer.h>
#  endif
#endif

// MACROS
#ifdef __ANDROID__
#define LOG_TAG "MprWebRTCEcho"
#if 0
#define ALOGI(...) __android_log_print(ANDROID_LOG_INFO,  LOG_TAG, __VA_ARGS__)
#define ALOGW(...) __android_log_print(ANDROID_LOG_WARN,  LOG_TAG, __VA_ARGS__)
#define ALOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#else
#define ALOGI(...)
#define ALOGW(...)
#define ALOGE(...)
#endif
#else // !__ANDROID__
#define LOG_TAG
#define ALOGI(...)
#define ALOGW(...)
#define ALOGE(...)
#endif // !__ANDROID__

// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// CONSTANTS
// STATIC VARIABLE INITIALIZATIONS
volatile MprWebRTCEchoControlMobile::GlobalEnableState
      MprWebRTCEchoControlMobile::smGlobalEnableState =
            MprWebRTCEchoControlMobile::LOCAL_MODE;

// LOCAL FUNCTIONS
/// Translates error code returned by WebRTC AECM module to sipX OS_* code.
static OsStatus TranslateAecmError(void *mpAecm)
{
   int32_t error = WebRtcAecm_get_error_code(mpAecm);
   switch (error)
   {
   default:
      return OS_FAILED;
   case AECM_UNSPECIFIED_ERROR:
      return OS_UNSPECIFIED;
   case AECM_UNSUPPORTED_FUNCTION_ERROR:
      return OS_NOT_SUPPORTED;
   case AECM_UNINITIALIZED_ERROR:
      return OS_INVALID_STATE;
   case AECM_NULL_POINTER_ERROR:
   case AECM_BAD_PARAMETER_ERROR:
      return OS_INVALID_ARGUMENT;
   }
}

/* //////////////////////////// PUBLIC //////////////////////////////////// */

/* ============================ CREATORS ================================== */

// Constructor
MprWebRTCEchoControlMobile::MprWebRTCEchoControlMobile(const UtlString& rName,
                                                       OsMsgQ* pSpkrQ,
                                                       int spkrQDelayMs,
                                                       int echoMode,
                                                       bool suppressNoise,
                                                       int noiseMode)
: MpAudioResource(rName, 1, 1, 1, 1)
, mpNsx(NULL)
, mpAecm(NULL)
, mStartedCanceling(false)
, mpSpkrQ(pSpkrQ)
, mSpkrQDelayMs(spkrQDelayMs)
, mEchoMode(echoMode < 0 ? 0 : echoMode > 4 ? 4 : echoMode)
, mSuppressNoise(suppressNoise)
, mNoiseMode(noiseMode < 0 ? 0 : noiseMode > 3 ? 3 : noiseMode)
, mFarEndSamples(0)
{
}

// Destructor
MprWebRTCEchoControlMobile::~MprWebRTCEchoControlMobile()
{
}

/* ============================ MANIPULATORS ============================== */

OsStatus MprWebRTCEchoControlMobile::setSpkrQ(const UtlString& namedResource,
                                              OsMsgQ& fgQ,
                                              OsMsgQ *pSpkrQ)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_SPEAKER_QUEUE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   stat = msgData.serialize((void*)pSpkrQ);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

OsStatus MprWebRTCEchoControlMobile::setEchoMode(const UtlString& namedResource,
                                                 OsMsgQ& fgQ,
                                                 int echoMode)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_ECHO_MODE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   // Bound echoMode between 0 and 4.
   echoMode = echoMode < 0 ? 0 : echoMode > 4 ? 4 : echoMode;

   stat = msgData.serialize(echoMode);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

OsStatus MprWebRTCEchoControlMobile::setSuppressNoise(
      const UtlString& namedResource,
      OsMsgQ& fgQ,
      bool suppressNoise)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_SUPPRESS_NOISE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   int serializedSuppressNoise = suppressNoise ? 1 : 0;

   stat = msgData.serialize(serializedSuppressNoise);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}


OsStatus MprWebRTCEchoControlMobile::setNoiseMode(const UtlString& namedResource,
                                                  OsMsgQ& fgQ,
                                                  int noiseMode)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_NOISE_SUPPRESSION_MODE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   // Bound noiseMode between 0 and 3.
   noiseMode = noiseMode < 0 ? 0 : noiseMode > 3 ? 3 : noiseMode;

   stat = msgData.serialize(noiseMode);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

/* ============================ ACCESSORS ================================= */
/* ============================ INQUIRY =================================== */

/* //////////////////////////// PROTECTED ///////////////////////////////// */

/* //////////////////////////// PRIVATE /////////////////////////////////// */

UtlBoolean MprWebRTCEchoControlMobile::doProcessFrame(MpBufPtr inBufs[],
                                                      MpBufPtr outBufs[],
                                                      int inBufsSize,
                                                      int outBufsSize,
                                                      UtlBoolean isEnabled,
                                                      int samplesPerFrame,
                                                      int samplesPerSecond)
{
   MpAudioBufPtr   outBuffer;
   MpAudioBufPtr   inputBuffer;
   MpAudioBufPtr   echoRefBuffer;
   MpBufferMsg*    bufferMsg;
   bool            res = false;

   // If disabled pass buffer through.
   if (  smGlobalEnableState == GLOBAL_DISABLE
      || (smGlobalEnableState == LOCAL_MODE && !isEnabled))
   {
      outBufs[0].swap(inBufs[0]);
      return TRUE;
   }

   // Get incoming data.
   inputBuffer.swap(inBufs[0]);

   // If we've already started cancellation, but received empty buffer from mic,
   // just replace it with silence to keep AEC in sync.
   if (mStartedCanceling && !inputBuffer.isValid())
   {
      inputBuffer = mpSilenceBuf;
   }

   // Filling Far End buffer with data coming from speaker.
   while (  mpSpkrQ->numMsgs() > 0
         && (mpSpkrQ->receive((OsMsg*&) bufferMsg, OsTime::NO_WAIT_TIME) == OS_SUCCESS))
   {
      int32_t result;

      // Get the buffer from the message and free the message
      echoRefBuffer = bufferMsg->getBuffer();
      bufferMsg->releaseMsg();

      // Use silence buffer if we received empty message.
      if (!echoRefBuffer.isValid()
         || (echoRefBuffer->getSamplesNumber() % 80) != 0)
      {
         ALOGW("unsupported number of echo reference samples: %d",
               echoRefBuffer->getSamplesNumber());
         echoRefBuffer = mpSilenceBuf;
      }

      // Put echo reference samples into Far End buffer in chunks of supported
      // sizes (80 or 160 at once).
      const MpAudioSample *pSamples         = echoRefBuffer->getSamplesPtr();
      size_t               samplesAvailable = echoRefBuffer->getSamplesNumber();
      const size_t         inputSize   =
            (samplesAvailable % 160) == 0 ? 160 : 80;

      while (samplesAvailable > 0)
      {
         result = WebRtcAecm_BufferFarend(mpAecm, pSamples, inputSize);
         if (result < 0)
         {
            ALOGE("failed to buffer Far End: %d", result);
            break;
         }

         if (!mStartedCanceling)
            mFarEndSamples += inputSize;
         pSamples          += inputSize;
         samplesAvailable  -= inputSize;
      }

      if (result < 0)
         break;
   }

   if (!mStartedCanceling && getFlowGraph() != NULL)
   {
      const size_t samplesPerMs = getFlowGraph()->getSamplesPerSec() / 1000;
      if (mFarEndSamples >= (mSpkrQDelayMs * samplesPerMs))
      {
         mStartedCanceling = true;
         ALOGI("started canceling after buffering %d Far End samples",
               mFarEndSamples);
      }
   }

   if (mStartedCanceling && inputBuffer.isValid())
   {
      if ((inputBuffer->getSamplesNumber() % 80) == 0)
      {
         // Get new buffer.
         outBuffer = MpMisc.RawAudioPool->getBuffer();
         assert(outBuffer.isValid());
         outBuffer->setSamplesNumber(samplesPerFrame);
         assert(outBuffer->getSamplesNumber() == samplesPerFrame);
         outBuffer->setSpeechType(inputBuffer->getSpeechType());

         size_t               numSamples  = inputBuffer->getSamplesNumber();
         const MpAudioSample *pInSamples  = inputBuffer->getSamplesPtr();
         MpAudioSample       *pOutSamples = outBuffer->getSamplesWritePtr();

         const size_t         inputSize   =
               (numSamples % 160) == 0 ? 160 : 80;

         while (numSamples > 0)
         {
            // Noise suppression works with 10ms frames only.
            assert(mpFlowGraph != NULL);
            int requiredFrameSize = mpFlowGraph->getSamplesPerSec() / 100;

            MpAudioSample* pAecmCleanSamples = NULL;

            // Run noise suppression on input samples.
            if (mSuppressNoise && (inputSize == requiredFrameSize))
            {
               assert(mpNsx != NULL);

               int nsxresult = WebRtcNsx_Process(mpNsx,
                     pInSamples, NULL,
                     pOutSamples, NULL);

               if (nsxresult >= 0)
            	   pAecmCleanSamples = pOutSamples;
            }

            int32_t result = WebRtcAecm_Process(
                  mpAecm,
                  pInSamples,
                  pAecmCleanSamples,
                  pOutSamples,
                  inputSize,
                  mSpkrQDelayMs);

            if (result < 0)
            {
               ALOGE("failed to process echo: %d", result);
               outBuffer.swap(inputBuffer);
               break;
            }

            numSamples  -= inputSize;
            pInSamples  += inputSize;
            pOutSamples += inputSize;
         }
      }
      else
      {
         ALOGW("unsupported number of input samples: %d",
               inputBuffer->getSamplesNumber());
         outBuffer.swap(inputBuffer);
      }
   }
   else
   {
      if (mStartedCanceling)
      {
         ALOGW("invalid buffer");
      }
      outBuffer.swap(inputBuffer);
   }

   outBufs[0].swap(outBuffer);
   return TRUE;
}

UtlBoolean MprWebRTCEchoControlMobile::handleMessage(MpResourceMsg& rMsg)
{
   OsSysLog::add(FAC_MP, PRI_DEBUG,
      "MprHook::handleMessage(%d)", rMsg.getMsg());
   switch (rMsg.getMsg())
   {
   case MPRM_SET_SPEAKER_QUEUE:
      {
         OsStatus stat;
         OsMsgQ *pSpkrQ;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize((void*&)pSpkrQ);
         assert(stat == OS_SUCCESS);

         mpSpkrQ = pSpkrQ;
         return TRUE;
      }
      break;
   case MPRM_SET_ECHO_MODE:
      {
         OsStatus stat;
         int echoMode;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize(echoMode);
         assert(stat == OS_SUCCESS);

         ALOGI("Setting AECM echo mode:%d", echoMode);
         if (mpAecm != NULL)
         {
            AecmConfig config;
            config.cngMode = AecmFalse;
            config.echoMode = echoMode;
            if (WebRtcAecm_set_config(mpAecm, config) >= 0)
               mEchoMode = echoMode;
         }
         else
         {
            mEchoMode = echoMode;
         }

         return TRUE;
      }
      break;
   case MPRM_SET_SUPPRESS_NOISE:
      {
         OsStatus stat;
         int suppressNoise;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize(suppressNoise);
         assert(stat == OS_SUCCESS);

         mSuppressNoise = suppressNoise;

         return TRUE;
      }
      break;
   case MPRM_SET_NOISE_SUPPRESSION_MODE:
      {
         OsStatus stat;
         int noiseMode;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize(noiseMode);
         assert(stat == OS_SUCCESS);

         ALOGI("Setting noise suppression mode:%d", noiseMode);

         if (mpNsx != NULL)
         {
            if (WebRtcNsx_set_policy(mpNsx, noiseMode) >= 0)
               mNoiseMode = noiseMode;
         }
         else
         {
            mNoiseMode = noiseMode;
         }

         return TRUE;
      }
      break;
   }
   return MpAudioResource::handleMessage(rMsg);
}

OsStatus MprWebRTCEchoControlMobile::setFlowGraph(MpFlowGraphBase* pFlowGraph)
{
   OsStatus res =  MpAudioResource::setFlowGraph(pFlowGraph);

   if (res == OS_SUCCESS)
   {
      // Check whether we're being added or removed from |pFlowGraph|.
      if (pFlowGraph != NULL)
      {
         // Allocate and initialize Noise Suppression module.
         if (WebRtcNsx_Create(&mpNsx) < 0)
            return OS_FAILED;

         if (WebRtcNsx_Init(mpNsx, mpFlowGraph->getSamplesPerSec()) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            return OS_FAILED;
         }

         if (WebRtcNsx_set_policy(mpNsx, mNoiseMode) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            return OS_FAILED;
         }

         // Allocate and initialize AECM module.
         if (WebRtcAecm_Create(&mpAecm) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            return OS_FAILED;
         }

         if (WebRtcAecm_Init(mpAecm, mpFlowGraph->getSamplesPerSec()) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            res = TranslateAecmError(mpAecm);
            WebRtcAecm_Free(mpAecm);
            mpAecm = NULL;

            return res;
         }

         // Configure echo control mode.
         AecmConfig config;
         config.cngMode = AecmFalse;
         config.echoMode = mEchoMode;
         if (WebRtcAecm_set_config(mpAecm, config) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            res = TranslateAecmError(mpAecm);
            WebRtcAecm_Free(mpAecm);
            mpAecm = NULL;

            return res;
         }

         ALOGI("AECM created, sampling rate: %d\n",
               mpFlowGraph->getSamplesPerSec());

         // Allocate buffer for silence and fill it with 0s
         if (!mpSilenceBuf.isValid())
         {
            mpSilenceBuf = MpMisc.RawAudioPool->getBuffer();
         }
         mpSilenceBuf->setSamplesNumber(mpFlowGraph->getSamplesPerFrame());
         memset(mpSilenceBuf->getSamplesWritePtr(), 0,
                mpSilenceBuf->getSamplesNumber()*sizeof(MpAudioSample));

         mStartedCanceling = false;
      }
      else
      {
         if (mpNsx != NULL)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;
         }

         if (mpAecm != NULL)
         {
            WebRtcAecm_Free(mpAecm);
            mpAecm = NULL;
         }

         mStartedCanceling = false;
      }
   }

   return res;
}

/* ============================ FUNCTIONS ================================= */
