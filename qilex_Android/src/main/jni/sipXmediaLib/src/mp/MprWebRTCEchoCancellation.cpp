// Copyright (c) 2013 Redefine Sp. z o.o.
// Author: Maciej Sikora <maciej.sikora@redefine.pl>

// SYSTEM INCLUDES
#ifdef __ANDROID__
#include <android/log.h>
#endif
#include <webrtc/modules/audio_processing/aec/include/echo_cancellation.h>
#include <webrtc/modules/audio_processing/ns/include/noise_suppression_x.h>

// APPLICATION INCLUDES
#include "os/OsDefs.h"
#include "os/OsSysLog.h"
#include "mp/MpMisc.h"
#include "mp/MpBuf.h"
#include "mp/MpBufPool.h"
#include "mp/MpBufferMsg.h"
#include "mp/MpPackedResourceMsg.h"
#include "mp/MpFlowGraphBase.h"
#include "mp/MprWebRTCEchoCancellation.h"
#ifdef RTL_ENABLED
#  include <rtl_macro.h>
#  ifdef RTL_AUDIO_ENABLED
#     include <SeScopeAudioBuffer.h>
#  endif
#endif

// MACROS
#ifdef __ANDROID__
#define LOG_TAG "MprWebRTCEcho"
#if 0
#define ALOGI(...) __android_log_print(ANDROID_LOG_INFO,  LOG_TAG, __VA_ARGS__)
#define ALOGW(...) __android_log_print(ANDROID_LOG_WARN,  LOG_TAG, __VA_ARGS__)
#define ALOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#else
#define ALOGI(...)
#define ALOGW(...)
#define ALOGE(...)
#endif
#else // !__ANDROID__
#define LOG_TAG
#define ALOGI(...)
#define ALOGW(...)
#define ALOGE(...)
#endif // !__ANDROID__

// EXTERNAL FUNCTIONS
// EXTERNAL VARIABLES
// CONSTANTS
// STATIC VARIABLE INITIALIZATIONS
const UtlContainableType MprWebRTCEchoCancellation::TYPE =
      "MprWebRTCEchoCancellation";
volatile MprWebRTCEchoCancellation::GlobalEnableState
      MprWebRTCEchoCancellation::smGlobalEnableState =
            MprWebRTCEchoCancellation::LOCAL_MODE;

// LOCAL FUNCTIONS
/// Translates error code returned by WebRTC AEC module to sipX OS_* code.
static OsStatus TranslateAecError(void *aecInst)
{
   int32_t error = WebRtcAec_get_error_code(aecInst);
   switch (error)
   {
   default:
      return OS_FAILED;
   case AEC_UNSPECIFIED_ERROR:
      return OS_UNSPECIFIED;
   case AEC_UNSUPPORTED_FUNCTION_ERROR:
      return OS_NOT_SUPPORTED;
   case AEC_UNINITIALIZED_ERROR:
      return OS_INVALID_STATE;
   case AEC_NULL_POINTER_ERROR:
   case AEC_BAD_PARAMETER_ERROR:
      return OS_INVALID_ARGUMENT;
   }
}

/* //////////////////////////// PUBLIC //////////////////////////////////// */

/* ============================ CREATORS ================================== */

// Constructor
MprWebRTCEchoCancellation::MprWebRTCEchoCancellation(const UtlString& rName,
                                                       OsMsgQ* pSpkrQ,
                                                       int spkrQDelayMs,
                                                       int nlpMode,
                                                       bool suppressNoise,
                                                       int noiseMode)
: MpAudioResource(rName, 1, 1, 1, 1)
, mpAec(NULL)
, mpSpkrQ(pSpkrQ)
, mSpkrQDelayMs(spkrQDelayMs)
, mNlpMode(nlpMode < 0 ? 0 : nlpMode > 2 ? 2 : nlpMode)
, mSuppressNoise(suppressNoise)
, mNoiseMode(noiseMode < 0 ? 0 : noiseMode > 3 ? 3 : noiseMode)
{
}

// Destructor
MprWebRTCEchoCancellation::~MprWebRTCEchoCancellation()
{
}

/* ============================ MANIPULATORS ============================== */

OsStatus MprWebRTCEchoCancellation::setSpkrQ(const UtlString& namedResource, OsMsgQ& fgQ, OsMsgQ *pSpkrQ)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_SPEAKER_QUEUE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   stat = msgData.serialize((void*)pSpkrQ);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

OsStatus MprWebRTCEchoCancellation::setNlpMode(const UtlString& namedResource, OsMsgQ& fgQ, int nlpMode)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_NLP_MODE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   // Bound nlpMode between 0 and 2.
   nlpMode = nlpMode < 0 ? 0 : nlpMode > 2 ? 2 : nlpMode;

   stat = msgData.serialize(nlpMode);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

OsStatus MprWebRTCEchoCancellation::setSuppressNoise(const UtlString& namedResource, OsMsgQ& fgQ, bool suppressNoise)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_SUPPRESS_NOISE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   int serializedSuppressNoise = suppressNoise ? 1 : 0;

   stat = msgData.serialize(serializedSuppressNoise);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

OsStatus MprWebRTCEchoCancellation::setNoiseMode(const UtlString& namedResource, OsMsgQ& fgQ, int noiseMode)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_NOISE_SUPPRESSION_MODE,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   // Bound noiseMode between 0 and 3.
   noiseMode = noiseMode < 0 ? 0 : noiseMode > 3 ? 3 : noiseMode;

   stat = msgData.serialize(noiseMode);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

OsStatus MprWebRTCEchoCancellation::setSpkrQDelayMs(const UtlString& namedResource, OsMsgQ& fgQ, int delayMs)
{
   OsStatus stat;
   MpPackedResourceMsg msg(
         (MpResourceMsg::MpResourceMsgType)MPRM_SET_SPEAKER_QUEUE_DELAY,
         namedResource);
   UtlSerialized &msgData = msg.getData();

   if (delayMs < 0)
	   delayMs = DEFAULT_ECHO_QUEUE_LEN;

   stat = msgData.serialize(delayMs);
   assert(stat == OS_SUCCESS);
   msgData.finishSerialize();
   return fgQ.send(msg, sOperationQueueTimeout);
}

/* ============================ ACCESSORS ================================= */

UtlContainableType MprWebRTCEchoCancellation::getContainableType() const
{
   return TYPE;
}

/* ============================ INQUIRY =================================== */

/* //////////////////////////// PROTECTED ///////////////////////////////// */

/* //////////////////////////// PRIVATE /////////////////////////////////// */

UtlBoolean MprWebRTCEchoCancellation::doProcessFrame(MpBufPtr inBufs[],
                                                      MpBufPtr outBufs[],
                                                      int inBufsSize,
                                                      int outBufsSize,
                                                      UtlBoolean isEnabled,
                                                      int samplesPerFrame,
                                                      int samplesPerSecond)
{
   MpAudioBufPtr   outBuffer;
   MpAudioBufPtr   inputBuffer;
   MpAudioBufPtr   echoRefBuffer;
   MpBufferMsg*    bufferMsg;
   bool            res = false;

   // If disabled pass buffer through.
   if (  smGlobalEnableState == GLOBAL_DISABLE
      || (smGlobalEnableState == LOCAL_MODE && !isEnabled))
   {
      outBufs[0].swap(inBufs[0]);
      return TRUE;
   }

   // Get incoming data.
   inputBuffer.swap(inBufs[0]);

   // If we've received empty buffer from mic just replace it with silence to keep AEC in sync.
   if (!inputBuffer.isValid())
      inputBuffer = mpSilenceBuf;

   // Filling Far End buffer with data coming from speaker.
   while (  mpSpkrQ->numMsgs() > 0
         && (mpSpkrQ->receive((OsMsg*&) bufferMsg, OsTime::NO_WAIT_TIME) == OS_SUCCESS))
   {
      int32_t result;

      // Get the buffer from the message and free the message
      echoRefBuffer = bufferMsg->getBuffer();
      bufferMsg->releaseMsg();

      // Use silence buffer if we received empty message.
      if (!echoRefBuffer.isValid()
         || (echoRefBuffer->getSamplesNumber() % 80) != 0)
      {
         ALOGW("unsupported number of echo reference samples: %d",
               echoRefBuffer->getSamplesNumber());
         echoRefBuffer = mpSilenceBuf;
      }

      // Put echo reference samples into Far End buffer in chunks of supported
      // sizes (80 or 160 at once).
      const MpAudioSample *pSamples         = echoRefBuffer->getSamplesPtr();
      size_t               samplesAvailable = echoRefBuffer->getSamplesNumber();
      const size_t         inputSize   =
            (samplesAvailable % 160) == 0 ? 160 : 80;

      while (samplesAvailable > 0)
      {
         result = WebRtcAec_BufferFarend(mpAec, pSamples, inputSize);
         if (result < 0)
         {
            ALOGE("failed to buffer Far End: %d", result);
            break;
         }

         pSamples          += inputSize;
         samplesAvailable  -= inputSize;
      }

      if (result < 0)
         break;
   }

   if (inputBuffer.isValid())
   {
      if ((inputBuffer->getSamplesNumber() % 80) == 0)
      {
         // Get new buffer.
         outBuffer = MpMisc.RawAudioPool->getBuffer();
         assert(outBuffer.isValid());
         outBuffer->setSamplesNumber(samplesPerFrame);
         assert(outBuffer->getSamplesNumber() == samplesPerFrame);
         outBuffer->setSpeechType(inputBuffer->getSpeechType());

         size_t               numSamples  = inputBuffer->getSamplesNumber();
         const MpAudioSample *pInSamples  = inputBuffer->getSamplesPtr();
         MpAudioSample       *pOutSamples = outBuffer->getSamplesWritePtr();

         const size_t         inputSize   =
               (numSamples % 160) == 0 ? 160 : 80;

         while (numSamples > 0)
         {
            // Noise suppression works with 10ms frames only.
            assert(mpFlowGraph != NULL);
            int requiredFrameSize = mpFlowGraph->getSamplesPerSec() / 100;

            const MpAudioSample* pAecInSamples = pInSamples;

            // Run noise suppression on input samples.
            if (mSuppressNoise && (inputSize == requiredFrameSize))
            {
               assert(mpNsx != NULL);

               int nsxresult = WebRtcNsx_Process(mpNsx,
                     pInSamples, NULL,
                     pOutSamples, NULL);

               if (nsxresult >= 0)
            	   pAecInSamples = pOutSamples;
            }

            int32_t result = WebRtcAec_Process(mpAec, pAecInSamples, NULL,
                     pOutSamples, NULL, inputSize, mSpkrQDelayMs, 0);

            if (result < 0)
            {
               ALOGE("failed to process echo: %d", result);
               outBuffer.swap(inputBuffer);
               break;
            }

            // Set the echo status
            int echoStatus;
            result = WebRtcAec_get_echo_status(mpAec, &echoStatus);
            if (result < 0)
                ALOGE("failed to get echo status");
            else if (echoStatus == 1)
            	outBuffer->setEchoPresent(TRUE);
            else
            	outBuffer->setEchoPresent(FALSE);

            numSamples  -= inputSize;
            pInSamples  += inputSize;
            pOutSamples += inputSize;
         }
      }
      else
      {
         ALOGW("unsupported number of input samples: %d",
               inputBuffer->getSamplesNumber());
         outBuffer.swap(inputBuffer);
      }
   }
   else
   {
      ALOGW("invalid buffer");
      outBuffer.swap(inputBuffer);
   }

   outBufs[0].swap(outBuffer);
   return TRUE;
}

UtlBoolean MprWebRTCEchoCancellation::handleMessage(MpResourceMsg& rMsg)
{
   OsSysLog::add(FAC_MP, PRI_DEBUG,
      "MprHook::handleMessage(%d)", rMsg.getMsg());
   switch (rMsg.getMsg())
   {
   case MPRM_SET_SPEAKER_QUEUE:
      {
         OsStatus stat;
         OsMsgQ *pSpkrQ;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize((void*&)pSpkrQ);
         assert(stat == OS_SUCCESS);

         mpSpkrQ = pSpkrQ;
         return TRUE;
      }
      break;

   case MPRM_SET_NLP_MODE:
      {
         OsStatus stat;
         int nlpMode;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize(nlpMode);
         assert(stat == OS_SUCCESS);

         AecConfig config;
         config.nlpMode = nlpMode;

         // the defaults
         config.skewMode = kAecFalse;
         config.metricsMode = kAecFalse;
         config.delay_logging = kAecFalse;

         if (WebRtcAec_set_config(mpAec, config) >= 0)
            mNlpMode = nlpMode;

         return TRUE;
      }
      break;

   case MPRM_SET_SUPPRESS_NOISE:
      {
         OsStatus stat;
         int suppressNoise;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize(suppressNoise);
         assert(stat == OS_SUCCESS);

         mSuppressNoise = suppressNoise;

         return TRUE;
      }
      break;

   case MPRM_SET_NOISE_SUPPRESSION_MODE:
      {
         OsStatus stat;
         int noiseMode;

         UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
         stat = msgData.deserialize(noiseMode);
         assert(stat == OS_SUCCESS);

         if (mpNsx != NULL)
         {
            if (WebRtcNsx_set_policy(mpNsx, noiseMode) >= 0)
               mNoiseMode = noiseMode;
         }
         else
         {
            mNoiseMode = noiseMode;
         }

         return TRUE;
      }
      break;

   case MPRM_SET_SPEAKER_QUEUE_DELAY:
        {
           OsStatus stat;
           int delayMs;

           UtlSerialized &msgData = ((MpPackedResourceMsg*)(&rMsg))->getData();
           stat = msgData.deserialize(delayMs);
           assert(stat == OS_SUCCESS);

           mSpkrQDelayMs = delayMs;

           return TRUE;
        }
        break;

   }
   return MpAudioResource::handleMessage(rMsg);
}

OsStatus MprWebRTCEchoCancellation::setFlowGraph(MpFlowGraphBase* pFlowGraph)
{
   OsStatus res =  MpAudioResource::setFlowGraph(pFlowGraph);

   if (res == OS_SUCCESS)
   {
      // Check whether we're being added or removed from |pFlowGraph|.
      if (pFlowGraph != NULL)
      {
         // Allocate and initialize Noise Suppression module.
         if (WebRtcNsx_Create(&mpNsx) < 0)
            return OS_FAILED;

         if (WebRtcNsx_Init(mpNsx, mpFlowGraph->getSamplesPerSec()) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            return OS_FAILED;
         }

         if (WebRtcNsx_set_policy(mpNsx, mNoiseMode) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            return OS_FAILED;
         }

         // Allocate and initialize AEC module.
         if (WebRtcAec_Create(&mpAec) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            return TranslateAecError(mpAec);
         }

         if (WebRtcAec_Init(mpAec, mpFlowGraph->getSamplesPerSec(),
                 mpFlowGraph->getSamplesPerSec()) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            res = TranslateAecError(mpAec);
            WebRtcAec_Free(mpAec);
            mpAec = NULL;
            return res;
         }

         AecConfig config;
         config.nlpMode = DEFAULT_NLP_MODE;
         config.skewMode = kAecFalse;
         config.metricsMode = kAecFalse;
         config.delay_logging = kAecFalse;
         if (WebRtcAec_set_config(mpAec, config) < 0)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;

            res = TranslateAecError(mpAec);
            WebRtcAec_Free(mpAec);
            mpAec = NULL;
            return res;
         }

         ALOGI("AEC created, sampling rate: %d\n",
               mpFlowGraph->getSamplesPerSec());

         // Allocate buffer for silence and fill it with 0s
         if (!mpSilenceBuf.isValid())
         {
            mpSilenceBuf = MpMisc.RawAudioPool->getBuffer();
         }
         mpSilenceBuf->setSamplesNumber(mpFlowGraph->getSamplesPerFrame());
         memset(mpSilenceBuf->getSamplesWritePtr(), 0,
                mpSilenceBuf->getSamplesNumber()*sizeof(MpAudioSample));
      }
      else
      {
         if (mpNsx != NULL)
         {
            WebRtcNsx_Free(mpNsx);
            mpNsx = NULL;
         }

         if (mpAec != NULL)
         {
            WebRtcAec_Free(mpAec);
            mpAec = NULL;
         }
      }
   }

   return res;
}

/* ============================ FUNCTIONS ================================= */
