# Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := commonlib

LOCAL_CPP_EXTENSION := .cpp
LOCAL_CFLAGS += -std=gnu++11

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../boost_1_57_0
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../boost_1_57_0/boost
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocate
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocatejni

FILE_LIST := $(wildcard $(LOCAL_PATH)/*.cpp)
LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)
	
LOCAL_STATIC_LIBRARIES += \
	librutil \

#LOCAL_LDLIBS += -llog
        
include $(BUILD_STATIC_LIBRARY)