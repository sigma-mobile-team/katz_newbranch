#include "FunctorsQueue.h"


FunctorsQueue::functorHolder::functorHolder(const std::function<void(void)> & functor) : _functor(functor)
{
}



void FunctorsQueue::functorHolder::operator ()()
{
	boost::lock_guard<boost::mutex> lock(_mutex);
	if (_functor)
		_functor();
}

void FunctorsQueue::functorHolder::cancel()
{
	boost::lock_guard<boost::mutex> lock(_mutex);
	_functor = nullptr;
}


FunctorsQueue::FunctorsQueue() :
		_work(_io), _thread(boost::bind(&boost::asio::io_service::run, &_io))
{

}

FunctorsQueue::~FunctorsQueue() {
	StopAndJoin();
}

void FunctorsQueue::AddFunctor(const std::function<void(void)> &func) {
	_io.post(func);
}


void FunctorsQueue::Stop() {
	_io.stop();
}

void FunctorsQueue::Join() {
	_thread.join();
}

void FunctorsQueue::StopAndJoin() {
	Stop();
	Join();
}

FunctorsQueue::connection FunctorsQueue::AddFunctorAt(const clock::time_point &time, const std::function<void(void)> &func)
{

	boost::lock_guard<boost::recursive_mutex> lock(_mutex);
	auto holder = functorHolder::Create(func);

    _delayedFunctors.insert(std::make_pair(time, holder));

	if (_delayedFunctors.empty())
		return connection(holder);
	auto delta = _delayedFunctors.begin()->first - clock::now();

	FunctorsQueue::scheduleTimeout(boost::chrono::duration_cast<boost::chrono::milliseconds>(delta));

	return connection(holder);
}


FunctorsQueue::connection FunctorsQueue::DelayFunctor(const boost::chrono::milliseconds &milliseconds, const std::function<void(void)> &func)
{
	return AddFunctorAt(clock::now() + milliseconds, func);
}

void FunctorsQueue::scheduleTimeout(const boost::chrono::milliseconds &milliseconds)
{
	long long count = milliseconds.count();
	auto ptime = boost::posix_time::milliseconds(count);
	_timer.reset(new boost::asio::deadline_timer(_io, ptime));
	_timer->async_wait(boost::bind(&FunctorsQueue::timeoutHandler, this, _1));
}

void FunctorsQueue::timeoutHandler(const boost::system::error_code& error)
{
	if (error)
		return;
	auto now = clock::now();
	boost::lock_guard<boost::recursive_mutex> lock(_mutex);
	for (auto it = _delayedFunctors.begin(); it != _delayedFunctors.end();)
	{
		if (it->first > now)
		{
			FunctorsQueue::scheduleTimeout(boost::chrono::duration_cast<boost::chrono::milliseconds>(_delayedFunctors.begin()->first - now));
			return;
		}
		auto &holder = *(it->second);
		holder();
		it = _delayedFunctors.erase(it);
	}
}






CyclicTask::CyclicTask()
{
}


CyclicTask::~CyclicTask()
{
	_connection.cancel();
}

void CyclicTask::Cancel()
{
	_connection.cancel();
	_lastStamp = FunctorsQueue::clock::now();
	_queue.reset();
	_functor = nullptr;
}



void CyclicTask::SetTask(const boost::weak_ptr<FunctorsQueue>& queue, const boost::chrono::milliseconds &milliseconds, const std::function<void(void)> &func)
{
	_lastStamp = FunctorsQueue::clock::now();
	_queue = queue;
	_period = milliseconds;
	_functor = func;
	repeatTask();
}

void CyclicTask::repeatTask()
{


	auto queue = _queue.lock();
	if (queue)
	{
		_lastStamp = _lastStamp + _period;

		queue->AddFunctorAt(_lastStamp, [&]()
		{
			onTimeout();
		}, shared_from_this());
	}


}

void CyclicTask::onTimeout()
{
	_functor();
	repeatTask();
}
