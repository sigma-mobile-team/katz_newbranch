/**
**  $Id: zevent.h,v 1.26 2008/07/21 12:33:27 jmrozin Exp $
**  Basic Event handlig classes. Jacek Piszczek/Viet Hoang
*/
#ifndef _ZEVENT_H_
#define _ZEVENT_H_

#include <functional>
#include <boost/shared_ptr.hpp>

class ZEventType
{
public:
	/**
	* Event types.
	*/
	typedef enum
	{
		// Generic events
		Custom = 0,             //!< Custom event (use dynamic_cast to find out which one it is)

		// Propagated GUI core Events

		GUI,                   //!< GUI event related to window actions
		Mouse,                 //!< Mouse event
		Keyboard,              //!< Keyboard event
		Drag,                  //!< Drag & drop support
		Tick,                  //!< 10Hz periodic tick events

		// Other GUI Core Events

		SynchronizedEvent, //!< Event sychronized to the main thread
		Menu,                  //!< ZMenu events
		Scroller,              //!< events emitted by ZScrollerPane
		WebBrowser,
		DeviceChange,			//!< produced by WM_DEVICECHANGE

		
		// Networking events

		HTTPClient,            //!< events emitted by HTTP clients 
		StreamTransport,	   //!< audio stream transport related events
		Billing,			   //!< billing related events

		// Other workers and misc stuff
		LoginStatus,           //!< Events eimmited by the login process
		MailslotServer,        //!< Events by the mailslot server
		SystemTrayIcon,

		PassResetStatus,		//!< emmited by PassResetTask (by password resseting process)
		PassEditStatus,			//!< emmited by PassEditTask (password modification process)

		SIPwrapper,				//!< SIP-level call control related events
		DeviceMonitor,			//!< produced by DeviceChangeMonitor
		CPUMonitor,
		QualityStats,

		UpdateStatus,          //!< Update status events
		UpdateWindowComm,
		TraceLog,
		// End

		NumTypes
	} EventType;
};

/**
*  A pair event type and event subtype ID which uniquely identify each event.
*/
class ZEventID 
{
public:
	ZEventType::EventType m_nType;
	int m_nSubtypeId;

	ZEventID(void)
	{
		m_nType = ZEventType::Custom;
		m_nSubtypeId = 0;
	}
	
	ZEventID(const ZEventType::EventType nType,const int nSubtypeId)
	{
		m_nType = nType;
		m_nSubtypeId = nSubtypeId;
	}

	friend const bool operator<(const ZEventID& objLeft,const ZEventID& objRight)
	{
		if (objLeft.m_nType < objRight.m_nType)
			return true;
		if (objLeft.m_nType > objRight.m_nType)
			return false;
		return (objLeft.m_nSubtypeId < objRight.m_nSubtypeId);
	}
};

/**
*  A binary function used to compare the IDs of two events
*/
struct ZEventIDComparer: public std::binary_function <ZEventID, ZEventID, bool>
{
	bool operator()(const ZEventID& objLeft,const ZEventID& objRight) const 
	{
		return (objLeft < objRight);
	}
};

/**
*  This class implements an interface for events
*/

class ZEvent
{
protected:
	/**
	* Create a new event of a given type given its unique subtype ID.
	* Derived classes should extend this.
	*/
	ZEvent(const int nSubtypeId) 
	{
		// This is an interface, so there is no way for validating
		// You should perform validating in the derived classed
		m_nSubtypeId = nSubtypeId;
	}

	ZEvent(void)
	{
	}
	
	virtual ~ZEvent(){};
	
	/**
	* Check if a subtype ID is OK
	*/
	virtual const bool ValidateSubtypeId(const int nSubtypeId) const = 0;

public:
	/**
	*  Each event is of specific type, such as MouseEvent, KeyEvent etc.
	*  Implement this so that listener can determine the event's type
	*/
	// TODO: some event might be of more than one type.
	virtual const ZEventType::EventType GetType(void) const = 0;
	
	/**
	*	Get the Event's sender.
	*/
	/*
	virtual const boost::shared_ptr<ZRefCountedObject>& GetSender(void) const
	{
		return m_staticUselessPtr;
	}
	*/

	/**
	*  Each event is either synch. or asych.
	*  A sych. MUST be processed by listeners (OnEvent) in synchronous mode, i.e. one listener by other. 
	*  Any listener in the CHAIN may stop propagating the given event by having OnEvent return FALSE.
	*  An asynchronous event may be processed either in synch. or asynchronous mode. 
	*  Asynchronous event are ALWAYS propagated through ALL associated listeners.
	*  Implement this method to help listener determine whether the given event is SYNCH or ASYNCH
	*/
	virtual const bool IsAsync(void) const = 0;

	/**
	** If the event is synchronous, it can be aborted. Is this is false, it means DispatchEvent will
	** return false if any of the listeners return false, but will NOT abort the event chain
	*/
	virtual const bool IsAbortable(void) = 0;

	/**
	* Get the event's subtype ID
	*/
	const int GetSubtypeId(void) const 
	{
		return m_nSubtypeId;
	}

	/**
	* Obtain the ZEventID which identifies the event type + subtype
	*/
	const ZEventID GetEventID(void) const 
	{
		return ZEventID(GetType(), m_nSubtypeId);
	}
	
	///Async events are queued - so when new event arrives it can remove (collapse) some previous events 
	///This enum defines behaviour when adding to queue
	typedef enum
	{
	    NoCollapse,         ///< No collapsing. All old events will be processed
	    ByType,             ///< All events of same type will be removed
	    ByTypeAndSubtype,   ///< All events of same type AND subtype will be removed
	    ByFunction          ///< event provides custom function to determine if it wants to remove given event. Queue adding will call this function on every event.
	
	}CollapseType;

    /// what old events should be discarded	
	virtual const ZEvent::CollapseType      GetCollapsingType(void){return ZEvent::NoCollapse;}
	///@param old event in list to look at
	///@return true to discard this event, return false to keep it
	virtual const bool                      DetermineCollapse(const boost::shared_ptr<ZEvent> &OldEvent){return false;}


protected:
	//static boost::shared_ptr<ZRefCountedObject> m_staticUselessPtr; // do NOT use!

	/**
	*  Each event has an unique ID, within the space of all events of a given type.
	*/
	int m_nSubtypeId;
};

/**
*  This is an interface for synchronous events, i.e. events which must be processed by each listeners in 
*  one by another
*/

class ZSyncEvent: public ZEvent
{
protected:
	/**
	*  Create a new instance of ZEvent given its sender
	*/
	/*
	ZSyncEvent(const int nSubtypeId, const boost::shared_ptr<ZRefCountedObject>& pSender): ZEvent(nSubtypeId), m_pParent(pSender)
	{
		//ASSERT((pSender));
		m_pParent = pSender;
	}
	*/
	ZSyncEvent(const int nSubtypeId): ZEvent(nSubtypeId)
	{	
		//m_pParent = 0;
	}

	ZSyncEvent(void) {}
	virtual ~ZSyncEvent(){};

public:
	/**
	*	Get the Event's sender.
	*/
	/*
	const boost::shared_ptr<ZRefCountedObject>& GetSender(void) const
	{
		return m_pParent;
	}
	*/

	/**
	*  Each event is either synch. or asych.
	*  A sych. MUST be processed by listeners (OnEvent) in synchronous mode, i.e. one listener by other. 
	*  Any listener in the CHAIN may stop propagating the given event by having OnEvent return FALSE.
	*  An asynchronous event may be processed either in synch. or asynchronous mode. 
	*  Asynchronous event are ALWAYS propagated through ALL associated listeners.
	*  Implement this method to help listener determine whether the given event is SYNCH or ASYNCH
	*/
	const bool IsAsync(void) const {return false;}

	const bool IsAbortable(void) {return true;};

protected:
	//! Pointer to parent
	//boost::shared_ptr<ZRefCountedObject> m_pParent;
};

class ZSyncEvent_Unsafe : public ZSyncEvent
{
public:
	/*
	ZSyncEvent_Unsafe(const int nSubtypeId, const boost::shared_ptr<ZRefCountedObject>& pSender): ZSyncEvent(nSubtypeId,pSender)
	{
	}
	*/

	ZSyncEvent_Unsafe(const int nSubtypeId): ZSyncEvent(nSubtypeId)
	{	
	}

	/*
	long AddRef(void)
	{
		m_nRefCount++;
		return m_nRefCount;
	}

	long Release(void)
	{
		m_nRefCount--; 
		LONG count = m_nRefCount;
		if (count == 0)
		{
			// this protects us against something incrementing the lock in its destructor!
			m_nRefCount = 1;
			delete this;
		}

		return count;
	}
	*/
};

/**
*	This is an interface for asynchronous events, i.e. events which can be processed by listeners 
*	asynchronously (for examples by use of ZEventListenerT)
*/

class ZAsyncEvent: public ZEvent
{
protected:
	/**
	* Create a new instance of ZEvent given its sender
	*/
	ZAsyncEvent(const int nSubtypeId): ZEvent(nSubtypeId)
	{
	}

	ZAsyncEvent(void) {}
	virtual ~ZAsyncEvent(){};

public:
	const bool IsAsync(void) const {return true;}
	const bool IsAbortable(void) {return false;}

};

/**
** This is used to transport events to the main thread. SynchronizeEvent 
*/
class SynchronizedEvent : public ZSyncEvent
{
protected:
	SynchronizedEvent(const boost::shared_ptr<ZEvent>& pCarriedEvent)
	{
		m_pEvent = pCarriedEvent;
	}
public:
	boost::shared_ptr<ZEvent> GetCarriedEvent(void) const { return m_pEvent; };
	void SetCarriedEvent(const boost::shared_ptr<ZEvent> &pCarriedEvent) { m_pEvent = pCarriedEvent; };
	void ClearCarriedEvent(void) { m_pEvent.reset(); };

protected:
	boost::shared_ptr<ZEvent> m_pEvent;
	const bool ValidateSubtypeId(const int) const {return true;};
	const ZEventType::EventType GetType(void) const {return ZEventType::SynchronizedEvent;};
};

#endif // _ZEVE..

