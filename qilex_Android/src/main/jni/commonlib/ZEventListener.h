/**
** $Id: ZEventListener.h,v 1.1 2008/05/05 16:52:39 viet Exp $
** Event Listener implementation by Jacek Piszczek
*/

#ifndef _ZEVENTLISTENER_H_
#define _ZEVENTLISTENER_H_

#include "zevent.h"
#include <boost/shared_ptr.hpp>

class ZEventListener
{
protected:
	ZEventListener(void){}
	virtual ~ZEventListener(void) {}

public:
	virtual const bool OnEvent(const boost::shared_ptr< ZEvent >& pEvent) = 0;
};

#endif // _ZEVENTLISTENER_H_

