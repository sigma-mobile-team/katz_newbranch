/*
 * FunctorsQueue.h
 *
 *  Created on: 05-06-2013
 *      Author: Mateusz
 */

#ifndef FUNCTORSQUEUE_H_
#define FUNCTORSQUEUE_H_
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <functional>
#include <chrono>
#include <boost/asio/detail/thread.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>


class FunctorsQueue
{
protected:
	struct functorHolder
	{
		typedef boost::shared_ptr<functorHolder> pointer;
		typedef boost::weak_ptr<functorHolder> weak_pointer;

		functorHolder(const std::function<void(void)> & functor);

		static functorHolder::pointer Create(const std::function<void(void)> & functor)
		{
			return boost::make_shared<functorHolder>(functor);
		}

		void cancel();
		void operator ()();

		std::function<void(void)> _functor;

	protected:
		boost::mutex _mutex;
	};

public:
	class connection
	{
	public:
		connection() {}
		connection(const functorHolder::pointer &pointer) : _holder(pointer) {};
		void cancel() { auto holder = _holder.lock(); if (holder) holder->cancel(); }
	protected:
		functorHolder::weak_pointer _holder;
	};


	typedef boost::chrono::steady_clock clock;

	FunctorsQueue();
	virtual ~FunctorsQueue();

	void Stop();
	void Join();
	void StopAndJoin();

	void AddFunctor(const std::function<void(void)> &func);
	connection DelayFunctor(const boost::chrono::milliseconds &milliseconds, const std::function<void(void)> &func);
	connection AddFunctorAt(const clock::time_point &time, const std::function<void(void)> &func);

	template<typename T>
	connection DelayFunctorWeak(const boost::chrono::milliseconds &milliseconds, const std::function<void(void)> &func, const boost::shared_ptr<T> &weak)
	{
		boost::weak_ptr<T> ptr(weak);
		auto functor = [=]()
		{
			auto p = ptr.lock();
			if (!p)
				return;
			func();
		};
		return DelayFunctor(milliseconds, functor);
	}

	template<typename T>
	connection AddFunctorAt(const clock::time_point &time, const std::function<void(void)> &func, const boost::shared_ptr<T> &weak)
	{
		boost::weak_ptr<T> ptr(weak);
		auto functor = [=]()
		{
			auto p = ptr.lock();
			if (!p)
				return;
			func();
		};
		return AddFunctorAt(time, functor);
	}

	void timeoutHandler(const boost::system::error_code& error);
protected:
	void scheduleTimeout(const boost::chrono::milliseconds &milliseconds);


	boost::asio::io_service _io;
	boost::asio::io_service::work _work;
	std::unique_ptr<boost::asio::deadline_timer> _timer;
	boost::thread _thread;
	boost::recursive_mutex _mutex;



	std::multimap<clock::time_point, functorHolder::pointer> _delayedFunctors;
};

class CyclicTask : public boost::enable_shared_from_this<CyclicTask>
{
public:
	CyclicTask();
	virtual ~CyclicTask();

	typedef boost::shared_ptr<CyclicTask> pointer;

	static CyclicTask::pointer Create(const boost::weak_ptr<FunctorsQueue>& queue, const boost::chrono::milliseconds &milliseconds, const std::function<void(void)> &func)
	{
		auto task = boost::make_shared<CyclicTask>();
		task->SetTask(queue, milliseconds, func);
		return task;
	}

	void SetTask(const boost::weak_ptr<FunctorsQueue>& queue, const boost::chrono::milliseconds &milliseconds, const std::function<void(void)> &func);
	void Cancel();
protected:
	void repeatTask();
	void onTimeout();


	boost::weak_ptr<FunctorsQueue> _queue;
	boost::chrono::milliseconds _period;
	FunctorsQueue::clock::time_point _lastStamp;
	std::function<void(void)> _functor;
	FunctorsQueue::connection _connection;
};


#endif /* FUNCTORSQUEUE_H_ */
