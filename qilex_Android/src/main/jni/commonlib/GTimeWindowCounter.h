/*********************************/
/* GTimeWindowCounter.h          */
/* Lukasz Rychter 2012/01/19     */
/*							     */
/* TimeWindow counter class      */
/*********************************/

#ifndef __TIME_WINDOW_COUNTER_H__
#define __TIME_WINDOW_COUNTER_H__

#include "../resiprocate/rutil/RecursiveMutex.hxx"
#include <list>

#ifdef _WIN32
#define GetTimeMS() GetTickCount()
#endif


class GTimeWindowCounter
{
public:
	GTimeWindowCounter(unsigned int nWindowLength); //ms

	void Reset();
	void SetWindowLength(unsigned int nMiliseconds);

	void AddSample(float fValue);
	float GetAveragedValue();
	float GetMeanValue();
	float GetSum();
	float GetMinValue();
	float GetMaxValue();

	unsigned short GetSamplesNum()
	{
		return m_lSamples.size();
	}
	
protected:
	class Sample
	{
	public:
		unsigned int m_nTimeGathered;
		float m_fValue;
		unsigned int m_nWeight;

		bool operator < (Sample& right)
		{
			return m_fValue < right.m_fValue;
		}
	};

	void UpdateWindow();
#ifdef ANDROID
	long long GetTimeMS();
#endif

	unsigned int		m_nWindowLength;
	std::list<Sample>	m_lSamples;
	float				m_fValSum;
	unsigned int		m_nWeightSum;

	resip::RecursiveMutex		m_csAccess;
};

#endif
