/*********************************/
/* GTimeWindowCounter.cpp        */
/* Lukasz Rychter 2012/01/19     */
/*							     */
/* TimeWindow counter class      */
/*********************************/

#include "GTimeWindowCounter.h"

using namespace std;


GTimeWindowCounter::GTimeWindowCounter(unsigned int nWindowLength) : m_nWindowLength(nWindowLength)
{
	Reset();
}

#ifdef ANDROID
long long GTimeWindowCounter::GetTimeMS()
{
	struct timeval time;
	gettimeofday(&time, NULL);
	long long ticks = time.tv_sec*1000 + (time.tv_usec / 1000);

	return ticks;
}
#endif

void GTimeWindowCounter::Reset()
{
	m_csAccess.lock();
	m_lSamples.clear();
	m_fValSum = 0;
	m_nWeightSum = 0;
	m_csAccess.unlock();
}


void GTimeWindowCounter::SetWindowLength(unsigned int nMiliseconds)
{
	m_csAccess.lock();
	m_nWindowLength = nMiliseconds;
	m_csAccess.unlock();

	UpdateWindow();
}


void GTimeWindowCounter::AddSample(float fValue)
{
	UpdateWindow();

	unsigned int nCurrentTime = GetTimeMS();

	m_csAccess.lock();

	int nSampleWeight = 1;
	if (!m_lSamples.empty())
		nSampleWeight = nCurrentTime - m_lSamples.back().m_nTimeGathered + 1;

	//add a new sample
	Sample sample;
	sample.m_nTimeGathered = nCurrentTime;
	sample.m_fValue = fValue;
	sample.m_nWeight = nSampleWeight;

	m_lSamples.push_back(sample);

	m_fValSum += fValue * nSampleWeight;
	m_nWeightSum += nSampleWeight;

	m_csAccess.unlock();
}


float GTimeWindowCounter::GetAveragedValue()
{
	UpdateWindow();

	m_csAccess.lock();

	if (m_nWeightSum != 0)
	{
		float fRet =  m_fValSum / m_nWeightSum;
		m_csAccess.unlock();
		return fRet;
	}
	else
	{
		m_csAccess.unlock();
		return 0;
	}
}


float GTimeWindowCounter::GetMeanValue()
{
	UpdateWindow();

	m_csAccess.lock();

	if (m_nWeightSum != 0)
	{
		float fRet;
		
		std::list<Sample> lSort = m_lSamples;
		lSort.sort();
		
		int nSize = lSort.size();

		std::list<Sample>::iterator iter = lSort.begin();
		std::advance(iter, nSize/2);
		if (nSize%2 == 1)
			fRet = iter->m_fValue;
		else
			fRet = (iter->m_fValue + (--iter)->m_fValue)/2;

		m_csAccess.unlock();
		return fRet;
	}
	else
	{
		m_csAccess.unlock();
		return 0;
	}

	m_csAccess.unlock();
}


float GTimeWindowCounter::GetSum()
{
	UpdateWindow();

	m_csAccess.lock();

	float fSum = 0;
	for (std::list<Sample>::iterator iter=m_lSamples.begin(); iter!=m_lSamples.end(); ++iter) //a little bit slow implementation
		fSum += iter->m_fValue;

	m_csAccess.unlock();

	return fSum;
}


float GTimeWindowCounter::GetMinValue()
{
	UpdateWindow();

	m_csAccess.lock();

	float fMin = -1;
	for (std::list<Sample>::iterator iter=m_lSamples.begin(); iter!=m_lSamples.end(); ++iter) //a little bit slow implementation
	{
		if (iter->m_fValue < fMin || fMin == -1)
			fMin = iter->m_fValue;
	}

	m_csAccess.unlock();

	return fMin;
}


float GTimeWindowCounter::GetMaxValue()
{
	UpdateWindow();

	m_csAccess.lock();

	float fMax = -1;
	for (std::list<Sample>::iterator iter=m_lSamples.begin(); iter!=m_lSamples.end(); ++iter) //a little bit slow implementation
	{
		if (iter->m_fValue > fMax || fMax == -1)
			fMax = iter->m_fValue;
	}

	m_csAccess.unlock();

	return fMax;
}


void GTimeWindowCounter::UpdateWindow()
{
	//removing old (outside time window) samples
	unsigned int nCurrentTime = GetTimeMS();

	for (std::list<Sample>::iterator iter=m_lSamples.begin(); iter!=m_lSamples.end();)
	{
		if (nCurrentTime - iter->m_nTimeGathered > m_nWindowLength)
		{
			//old sample, discard and substract from samples sum
			m_fValSum -= iter->m_fValue * iter->m_nWeight;
			m_nWeightSum -= iter->m_nWeight;

			m_lSamples.pop_front();
			iter = m_lSamples.begin();
		}
		else
			break;
	}
}
