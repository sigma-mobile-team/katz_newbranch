/**
** $Id: ZEventHandler.h,v 1.2 2008/06/05 09:45:24 jpiszczek Exp $
** EventHandler implementation by Jacek Piszczek
*/

#ifndef _ZEVENTHANDLER_H_
#define _ZEVENTHANDLER_H_

#include "zevent.h"
#include "../resiprocate/rutil/RecursiveMutex.hxx"
#include <list>
#include <vector>


#include "ZEventListener.h"

/**
** The Golden Rule of event handling is objcount:
** Installation increases your usage count, which means that installing you as a listener
** in your constructor and uninstalling in a deconstructor will lead to a memleak!
*/


typedef std::list< boost::shared_ptr<ZEventListener> >                       ZEventHandler_EventList;
typedef std::vector< ZEventHandler_EventList >                               ZEventHandler_NamedEventMap;

struct ZEventHandler_TypeEntry
{
	ZEventHandler_EventList     m_lEventListeners;
	ZEventHandler_NamedEventMap m_mNamedListeners;
};

/*
** Delayed entries are added/removed after DispatchEvent has finished doing its job for safety reasons.
** Do note that the execution order DOES MATTER. change it and things will break randomly
*/ 


class ZEventHandler_DelayedEntry
{
public:
	typedef enum
	{
		ZEventHandler_DelayedEntry_Type_Install = 0,
		ZEventHandler_DelayedEntry_Type_Uninstall,
		ZEventHandler_DelayedEntry_Type_InstallNamed,
		ZEventHandler_DelayedEntry_Type_UninstallNamed,
		ZEventHandler_DelayedEntry_Type_Prioritize
	} ZEventHandler_DelayedEntry_Type;

	boost::shared_ptr< ZEventListener >   m_pListener;
	ZEventID                         m_eID;
	ZEventHandler_DelayedEntry_Type  e_actionType;
};

typedef std::list< ZEventHandler_DelayedEntry > ZEventHandler_DelayedList;

class ZEventHandler_SynchronizeHandler
{
public:
	virtual bool SynchronizeEvent(const boost::shared_ptr<ZEvent> &pEvent, const boost::shared_ptr<ZEventListener> &pDestination, bool bSyncOnRootThread = false) = 0;
};

class ZSyncDispatchListener : public ZEventListener
{
public:
	//IMPLEMENT_CREATEOBJ_INLINE(ZSyncDispatchListener,(void),());
	ZSyncDispatchListener(void){}
	virtual ~ZSyncDispatchListener(void) {}
	const bool OnEvent(const boost::shared_ptr< ZEvent >& pEvent);
};

class ZSyncMethodListener : public ZEventListener
{

public:
	//IMPLEMENT_CREATEOBJ_INLINE(ZSyncMethodListener,(void),());
	ZSyncMethodListener(void){}
	virtual ~ZSyncMethodListener(void) {}
	const bool OnEvent(const boost::shared_ptr< ZEvent >& pEvent);
};


class ZEventHandler
{

protected:
	void ReinitializeEH(void);

public:

	ZEventHandler(void);
	virtual ~ZEventHandler(void);

	/**
	** Installs an event listener for a given event type, also limited to a type and name
	** Do note that recurrency is NOT supported and will assert
	*/
	virtual bool InstallEventHandler(ZEventType::EventType eType, const boost::shared_ptr<ZEventListener>& pListener);
	virtual bool InstallNamedEventHandler(const ZEventID& eventID, const boost::shared_ptr<ZEventListener>& pListener);

	/**
	** Uninstalls a previously installed listener
	*/
	virtual void UninstallEventHandler(ZEventType::EventType eType, const boost::shared_ptr<ZEventListener>& pListener);
	virtual void UninstallNamedEventHandler(const ZEventID& eventID, const boost::shared_ptr<ZEventListener>& pListener);

	/**
	** Dispatches a given event to its registered listeners. Returns false if the chain was aborted
	** by any of the listeners (meaningful only for synchronous events!)
	*/
	virtual bool DispatchEvent(const boost::shared_ptr<ZEvent>& pEvent);

	/**
	** Shifts the already installed listener to the front of the list, making it receive events
	** earlier than other listeners
	*/
	virtual void PrioritizeEventHandler(ZEventType::EventType eType, const boost::shared_ptr<ZEventListener>& pListener);

	/**
	** Ensures the event will be synchronized to the main thread using OnEvent() of the pDestination
	** The event will be encapsulated in SynchronizedEvent. bSyncOnRootThread = true means the dispatch will
	** happen even when called from the root thread, otherwise it'd be OnEvent()ed immediately
	*/
	static bool SynchronizeEvent(const boost::shared_ptr<ZEvent> &pEvent, const boost::shared_ptr<ZEventListener> &pDestination, bool bSyncOnRootThread = false);

	/**
	** Sets the instance responsible for synchronized events handling
	*/
	static void SetSychronizeHandler(ZEventHandler_SynchronizeHandler *pHandler);

	/**
	** Returns true, if m_pSynchronizeHandler != 0
	*/
	static bool IsSychronizeHandlerSet();

	/**
	** Obtains the handler's critical section. Useful if you want to do several install/uninstall operations in a sequence
	*/
	void LockEventHandler(void);
	void UnlockEventHandler(void);

	/**
	** Calls given method m of object a on root thread (always synchronizes)  
	*/
	/*rede
	template <typename T> static bool SynchronizeFunctor(const T &a)
	{
		boost::shared_ptr<SynchronizedTemplate_EventFunctor<T>> pEvent = SynchronizedTemplate_EventFunctor<T>::CreateObj(a);
		return ZEventHandler::SynchronizeEvent(ZCONV(ZEvent, pEvent), ZCONV(ZEventListener, s_pMethodListener), true);
	}
	template <typename T> static bool SynchronizeMethod(const T &a)
	{
		boost::shared_ptr<SynchronizedTemplate_Event<T>> pEvent = SynchronizedTemplate_EventFunctor<T>::CreateObj(a);
		return ZEventHandler::SynchronizeEvent(ZCONV(ZEvent, pEvent), ZCONV(ZEventListener, s_pMethodListener), true);
	}
	template <typename T, typename M> static bool SynchronizeMethod(const T &a, M m)
	{
		boost::shared_ptr<SynchronizedTemplate_Event<T,M>> pEvent = SynchronizedTemplate_Event<T,M>::CreateObj(a,m);
		return ZEventHandler::SynchronizeEvent(ZCONV(ZEvent, pEvent), ZCONV(ZEventListener, s_pMethodListener), true);
	}
	template <typename T, typename M, typename P1> static bool SynchronizeMethod(const T &a, M m, const P1 &p1)
	{
		boost::shared_ptr<SynchronizedTemplate_Event1<T,M,P1>> pEvent = SynchronizedTemplate_Event1<T,M,P1>::CreateObj(a,m,p1);
		return ZEventHandler::SynchronizeEvent(ZCONV(ZEvent, pEvent), ZCONV(ZEventListener, s_pMethodListener), true);
	}
	
	template <typename T, typename M, typename P1, typename P2> static bool SynchronizeMethod(const T &a, M m, const P1 &p1, const P2 &p2)
	{
		boost::shared_ptr<SynchronizedTemplate_Event2<T,M,P1,P2>> pEvent = SynchronizedTemplate_Event2<T,M,P1,P2>::CreateObj(a,m,p1,p2);
		return ZEventHandler::SynchronizeEvent(ZCONV(ZEvent, pEvent), ZCONV(ZEventListener, s_pMethodListener), true);
	}
	*/

	/*
	** Dispatches pEvent in pObject's DispatchEvent on root thread
	*/
	static bool SynchronizeDispatch(const boost::shared_ptr<ZEventHandler> &pDispatcher, const boost::shared_ptr<ZEvent> &pEvent, bool bSyncOnRootThread = true);

#ifdef _DEBUG
	void DumpListenerChain(unsigned nCurrentDepth, ZEventType::EventType eType);
#endif

protected:
	struct ZEventHandler_TypeEntry **m_aEventListeners;   //!< normal, installed listeners
	ZEventHandler_DelayedList       *m_lDelayed;          //!< listeners changed from within DispatchEvent, to be updated once its complete
	unsigned                         m_uDispatching;      //!< != 0 when inside DispatchEvent, nests
	resip::RecursiveMutex           		      m_gLock;             //!< protection nesting lock (allows multiple locks from the same thread)

	static boost::shared_ptr<ZSyncDispatchListener> s_pDispatchListener;
	static boost::shared_ptr<ZSyncMethodListener>   s_pMethodListener;

protected:
	static ZEventHandler_SynchronizeHandler *m_pSynchronizeHandler; //!< pointer to the class providing synchronization capabilities
};

#endif // _ZEVE..

