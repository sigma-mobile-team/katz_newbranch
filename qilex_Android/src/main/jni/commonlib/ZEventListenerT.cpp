/**
** $Id: ZEventListenerT.cpp,v 1.6 2008/06/02 13:02:58 viet Exp $
** Threaded event listener implementation by Viet Hoang
*/

#include "ZEventListenerT.h"
#include "QilexLogger.hxx"
#include "../resiprocate/rutil/Lock.hxx"


using namespace std;

ZEventListenerT::ZEventListenerT(void)
{
LOGI("ZEventListenerT::ZEventListenerT");
	m_bStopping	= false;
}

ZEventListenerT::~ZEventListenerT(void)
{
}

const bool ZEventListenerT::OnEvent(const boost::shared_ptr< ZEvent >& event)
{
	//COMMLIBLOGI("ZEventListenerT::OnEvent");

	if (!event) 
		return true;


	if (!event->IsAsync()) 
		return	ProcessEvent(event);


	//	at this point, always return TRUE since the event is asynch.
	// This will create the thread if it has not been created / stopped before
	if (0 > StartThread())
		COMMLIBLOGI("ZEventListenerT::OnEvent thread fail");

	resip::Lock lock(m_semAccess);

    //if event does any colapsing other then NONE
	if(ZEvent::NoCollapse != event->GetCollapsingType())
	    RemoveCollapsibleEvents(event);

	m_lEvents.push_back(event);

	// if this is the only event in the list, then the event processing loop might be waiting
	if (m_lEvents.size() == 1)
		m_semWaitEL.signal();

	return true;
}

//caution : call only when m_semAccess is acquired
void ZEventListenerT::RemoveCollapsibleEvents(const boost::shared_ptr< ZEvent >& NewEvent)
{
    if(m_lEvents.empty())
        return;
        
    list< boost::shared_ptr< ZEvent > > ::iterator  Iter;

    Iter = m_lEvents.begin();

    if(ZEvent::ByType == NewEvent->GetCollapsingType())
    {
        while (m_lEvents.end() != Iter)
        {
            if( (*Iter)->GetType() == NewEvent->GetType() )
            {
                Iter = m_lEvents.erase(Iter);
                continue;
            }
            ++Iter;
        }
    }
    else if(ZEvent::ByTypeAndSubtype == NewEvent->GetCollapsingType())
    {
        while (m_lEvents.end() != Iter)
        {
            if( (*Iter)->GetType() == NewEvent->GetType() && (*Iter)->GetSubtypeId() == NewEvent->GetSubtypeId())
            {
                Iter = m_lEvents.erase(Iter);
                continue;
            }
            ++Iter;
        }
    }
    else if(ZEvent::ByFunction == NewEvent->GetCollapsingType())
    {
        while (m_lEvents.end() != Iter)
        {
            if(  NewEvent->DetermineCollapse(*Iter))
            {
                Iter = m_lEvents.erase(Iter);
                continue;
            }
            ++Iter;
        }
    }


}

void ZEventListenerT::SignalStop(void)
{
	if(isShutdown())
		return;

	COMMLIBLOGI("ZEventListenerT::SignalStop(void) lock(m_threadShut)");

	shutdown();
	ZEventListenerT::PrepareEndThread();
	//	The processing thread may be waiting for some event
	//	so just release the semaphore
	resip::Lock lock(m_semAccess);
	m_semWaitEL.signal();

	COMMLIBLOGI("ZEventListenerT::SignalStop m_threadShut-unlock");
	return;
}

void ZEventListenerT::StopThenWait(void)
{
	SignalStop();
	WaitUntilStopped();
}

int ZEventListenerT::OnThreadEnded(void)
{
	resip::Lock lock(m_semAccess);
	m_lEvents.clear();
	return true;
}

void ZEventListenerT::OnThreadStarted(void)
{
}

void ZEventListenerT::thread()
{
	this->InternalRun();
}

const int ZEventListenerT::Run(void)
{
	COMMLIBLOGI("ZEventListenerT::Run(void)");


	resip::Lock lock(m_semAccess);

	while (true)
	{
		while (!m_lEvents.empty())
		{
			std::list< boost::shared_ptr< ZEvent > > lCurrentlyProcessedEvents;
			lCurrentlyProcessedEvents.swap(m_lEvents);

			m_semAccess.unlock();
			for (auto &pEvent : lCurrentlyProcessedEvents)
				ProcessEvent(pEvent);
			m_semAccess.lock();
		}

		if (isShutdown())
			return 0;

		m_semWaitEL.wait(m_semAccess);
	}

	COMMLIBLOGI("ZEventListenerT::Run(void)/");
}

void ZEventListenerT::WaitUntilStopped()
{
	COMMLIBLOGI("ZEventListenerT::SignalStop m_threadShut-lock");
	this->join();
	COMMLIBLOGI("ZEventListenerT::SignalStop m_threadShut-unlock");
}

void ZEventListenerT::PrepareEndThread(void)
{
	COMMLIBLOGI("ZEventListenerT::PrepareEndThread(void)");
	PrepareEventHandlerTEndThread();
	COMMLIBLOGI("ZEventListenerT::PrepareEndThread(void)/");
}

int ZEventListenerT::InternalRun(void)
{	
	OnEventListenerTStarted();
	
	//	now, call the main processing part
	int	nRet = 0;
	try
	{
		nRet = ZEventListenerT::Run(); // must use ZEventListenerT:: here
	}
	catch(...)
	{
		assert(false);
		nRet = -1;
	}

	{
		resip::Lock lock(m_semAccess);
		m_nReturnState	= nRet;
	}
	OnEventListenerTEnded();
	return ZEventListenerT::OnThreadEnded();
}


const int ZEventListenerT::StartThread(void) {

	if (mId != 0)
		return 0;

	//COMMLIBLOGI("ZEventListenerT::StartThread 2");

	if(isShutdown())
		return 0;
	this->run();
	//COMMLIBLOGI("ZEventListenerT::StartThread /");
	return 0;
}
