/**
** $Id: ZEventListenerT.h,v 1.4 2008/06/02 13:02:58 viet Exp $
** Threaded async event listener implementation by Viet Hoang
*/

#ifndef	__ZEventListenerT_h__
#define	__ZEventListenerT_h__

#include "ZEventListener.h"
#include <list>

#include "../resiprocate/rutil/ThreadIf.hxx"
#include "../resiprocate/rutil/RecursiveMutex.hxx"
//#include "GThread.h"
//#include "GSemaphore.h"
//#include "GCriticalSection.h"
//#include "GWindowsEvent.h"

/**
*	This class implements an event listener which has a FIFO event list
*	and a separate thread which reads events from that list and perform
*	neccessary actions.
*	We will make the based GThead frozen, in the sense that derived class of this one
*	can again inherite GThread.
*   ALL virtual function from GThread MUST be called with "ZEventListenerT::"
*/

class ZEventListenerT: public ZEventListener, public resip::ThreadIf
{
public:
	ZEventListenerT(void);
protected:
	virtual ~ZEventListenerT(void);

public:
	/**
	*	Our implementation of the OnEvent method. This method only puts the given 
	*	event into the event list
	*/

	virtual const bool OnEvent(const boost::shared_ptr< ZEvent >& event);


/**
	*	Implementation of SignalStop method from GThread
	*/
	void SignalStop(void);
	void WaitUntilStopped();

protected:

	/**
	*	Implement this to process a single event.
	*	\return FALSE if the event is async and no propagation is allowed. TRUE otherwise.
	*/
	virtual const bool ProcessEvent(const boost::shared_ptr< ZEvent >& event) = 0;
	virtual void OnEventListenerTStarted(void) = 0;
	virtual int OnEventListenerTEnded(void) = 0;


	/**
	*	Implementation of StopThenWait method from GThread
	*/
	void StopThenWait(void);

	bool m_bStopping;
	int m_nReturnState;
private:

	/**
	*	Implement the Run method from GThread
	*/
	const int Run(void);
	//! Reimplmenet this to make GThread frozen
	const int StartThread(void);

	virtual void thread();


	/**
		And other, not-used virtual methods from GThread
	*/

	/*
	static int __stdcall EventListenerT_ThreadMain(
		LPVOID	pParam
		);

		*/

	int InternalRun(void);
	/**
	*	Implementation of PrepareEndThread method from GThread
	*/
	void PrepareEndThread(void);

	/**
	*	This should implements some specific actions for ZEventHandlerT to end.
	*/
	virtual void PrepareEventHandlerTEndThread(void)
	{
		return;
	}



	/**
	*	Implementation of OnThreadEnded method from GThread
	*/

	int OnThreadEnded(void) ;
	/**
	*	Implementation of OnThreadStarted method from GThread
	*/

	void OnThreadStarted(void);

	//!	A list of all event
	std::list< boost::shared_ptr< ZEvent > > m_lEvents;

	//!	A semaphore used to control access to event list
	resip::RecursiveMutex m_semAccess;
	resip::RecursiveMutex m_csGThreadAccess;



	/**
	*	This semaphore is used to force the event processing thread to wait when 
	*	the event list is empty or if thread is shutting down
	*/
	resip::Condition m_semWaitEL;


	
	///Iterates list to look for events to remove
	///caution : call only when m_semAccess is acquired
	void RemoveCollapsibleEvents(const boost::shared_ptr< ZEvent >& NewEvent);

};

#endif	//	__ZEventListenerT_h__

