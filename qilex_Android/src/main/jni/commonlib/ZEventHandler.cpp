/**
** $Id: ZEventHandler.cpp,v 1.4 2008/06/05 10:02:05 jpiszczek Exp $
** EventHandler implementation by Jacek Piszczek
*/

#include "ZEventHandler.h"
#include "QilexLogger.hxx"

//boost::shared_ptr<ZRefCountedObject> ZEvent::m_staticUselessPtr; // do NOT use!

class SynchronizedDispatch_Event : public ZSyncEvent
{
public:
	//IMPLEMENT_CREATEOBJ_INLINE(SynchronizedDispatch_Event, (const boost::shared_ptr<ZEventHandler> &pDispatcher, const boost::shared_ptr<ZEvent> &pEvent), (pDispatcher, pEvent) );

	SynchronizedDispatch_Event(const boost::shared_ptr<ZEventHandler> &pDispatcher, const boost::shared_ptr<ZEvent> &pEvent): ZSyncEvent()
	{
		m_pDispatcher = pDispatcher;
		m_pEvent = pEvent;
	};

	~SynchronizedDispatch_Event()
	{

	}

	const ZEventType::EventType GetType(void) const
	{
		if (m_pDispatcher)
			m_pDispatcher->DispatchEvent(m_pEvent);
		return ZEventType::Custom;
	}

	virtual const bool ValidateSubtypeId(const int nSubtypeId) const
	{
		return true;
	}
protected:
	boost::shared_ptr<ZEventHandler> m_pDispatcher;
	boost::shared_ptr<ZEvent>        m_pEvent;
};


#define ZDEBUG(x) 
#define ZDEBUG_DISP(x)
#define PROPER_REFCOUNT(x) 
#define ZCOUNTLISTENERS(x) 

#ifdef _DEBUG
#define EXTRACHECKS(x) x
#else
#define EXTRACHECKS(x) 
#endif

#define UNINSTALL_WITH_REVERSE_ITERATORS

ZCOUNTLISTENERS(LONG nListenCount = 0);

ZEventHandler_SynchronizeHandler *ZEventHandler::m_pSynchronizeHandler = NULL;
boost::shared_ptr<ZSyncDispatchListener>  ZEventHandler::s_pDispatchListener;
boost::shared_ptr<ZSyncMethodListener>    ZEventHandler::s_pMethodListener;

const bool ZSyncDispatchListener::OnEvent(const boost::shared_ptr< ZEvent >& pEvent)
{
	/*rede
	ZDECL(pE, SynchronizedEvent, pEvent);
	if (pE && pE->GetCarriedEvent())
		pE->GetCarriedEvent()->GetType();
		*/
	return false;
}

const bool ZSyncMethodListener::OnEvent(const boost::shared_ptr< ZEvent >& pEvent)
{
	/*rede
	ZDECL(pE, SynchronizedEvent, pEvent);
	if (pE && pE->GetCarriedEvent())
		ZCONV(SynchronizedTemplateV_Event, pE->GetCarriedEvent())->Do();
		*/
	return false;
}

ZEventHandler::ZEventHandler(void)
{
	ZDEBUG(ZTRACE("%s: hello!\n",__FUNCTION__));
	m_uDispatching = 0;
	m_aEventListeners = NULL;
	m_lDelayed = NULL;
}

ZEventHandler::~ZEventHandler(void)
{
	if (m_aEventListeners)
	{
		for (int i = 0; i < ZEventType::NumTypes; i++)
			if (m_aEventListeners[i] != NULL) delete m_aEventListeners[i];
		free(m_aEventListeners);
	}
	if (m_lDelayed) delete m_lDelayed;

	ZDEBUG(ZTRACE("%s: bye!\n",__FUNCTION__));
}

void ZEventHandler::ReinitializeEH(void)
{
	if (m_aEventListeners)
	{
		for (int i = 0; i < ZEventType::NumTypes; i++)
			if (m_aEventListeners[i] != NULL) delete m_aEventListeners[i];
		free(m_aEventListeners);
		m_aEventListeners = NULL;
	}
	if (m_lDelayed) delete m_lDelayed;
	m_lDelayed = NULL;
	m_uDispatching = 0;
}

bool ZEventHandler::InstallEventHandler(ZEventType::EventType eType, const boost::shared_ptr<ZEventListener>& pListener)
{
	// must be valid input!
	assert(pListener);
	if (!pListener) return false;

	// must be valid input
	assert(eType < ZEventType::NumTypes);

	ZCOUNTLISTENERS(ZTRACE("listeners: %ld\n",InterlockedIncrement(&nListenCount)));

	m_gLock.lock();//Acquire();

	if (m_uDispatching != 0)
	{
		ZEventHandler_DelayedEntry entry;

		if (!m_lDelayed)
		{
			if ((m_lDelayed = new ZEventHandler_DelayedList()) == NULL)
			{
				m_gLock.unlock();//Release();
				return false;
			}
		}

		// store the entry for delayed installation
		entry.e_actionType = ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_Install;
		entry.m_eID.m_nType = eType;
		entry.m_pListener = pListener;
		m_lDelayed->push_back(entry);

		m_gLock.unlock();//Release();
		return true;
	}

	if (m_aEventListeners == NULL)
	{
		if ((m_aEventListeners = (struct ZEventHandler_TypeEntry **)malloc(ZEventType::NumTypes * sizeof (struct ZEventHandler_TypeEntry *))) == NULL)
		{
			m_gLock.unlock();//Release();
			return false;
		}

		// prep for usage!
		for (int i = 0; i < ZEventType::NumTypes; i++)
			m_aEventListeners[i] = NULL;
	}

	// make sure there's an entry to handle this type
	if (m_aEventListeners[eType] == NULL)
	{
		m_aEventListeners[eType] = new ZEventHandler_TypeEntry();
		if (!m_aEventListeners[eType])
		{
			m_gLock.unlock();//Release();
			return false;
		}
	}

	struct ZEventHandler_TypeEntry *tentry = m_aEventListeners[eType];

	EXTRACHECKS
	(
		// make sure the iterator isn't already there
		ZEventHandler_EventList::iterator it;
		for (it = tentry->m_lEventListeners.begin(); it != tentry->m_lEventListeners.end(); it++)
		{
			if (*it == pListener)
			{
				// adding an already registered listener
				assert(false);
				m_gLock.unlock();//Release();
				return false;
			}
		}
	)	

	tentry->m_lEventListeners.push_back(pListener);

	m_gLock.unlock();//Release();

	return true;
}

bool ZEventHandler::InstallNamedEventHandler(const ZEventID& eventID, const boost::shared_ptr<ZEventListener>& pListener)
{
	// must be valid input!
	assert(pListener);
	if (!pListener) return false;

	// must be valid input
	assert(eventID.m_nType < ZEventType::NumTypes);

	ZCOUNTLISTENERS(ZTRACE("listeners: %ld\n",InterlockedIncrement(&nListenCount)));

	m_gLock.lock();//Acquire();

	if (m_uDispatching != 0)
	{
		ZEventHandler_DelayedEntry entry;

		if (!m_lDelayed)
		{
			if ((m_lDelayed = new ZEventHandler_DelayedList()) == NULL)
			{
				m_gLock.unlock();//Release();
				return false;
			}
		}

		// store the entry for delayed installation
		entry.e_actionType = ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_InstallNamed;
		entry.m_eID = eventID;
		entry.m_pListener = pListener;
		m_lDelayed->push_back(entry);

		m_gLock.unlock();//Release();
		return true;
	}

	if (m_aEventListeners == NULL)
	{
		if ((m_aEventListeners = (struct ZEventHandler_TypeEntry **)malloc(ZEventType::NumTypes * sizeof (struct ZEventHandler_TypeEntry *))) == NULL)
		{
			m_gLock.unlock();//Release();
			return false;
		}

		// prep for usage!
		for (int i = 0; i < ZEventType::NumTypes; i++)
			m_aEventListeners[i] = NULL;
	}

	// make sure there's an entry to handle this type
	if (m_aEventListeners[eventID.m_nType] == NULL)
	{
		m_aEventListeners[eventID.m_nType] = new ZEventHandler_TypeEntry();
		if (!m_aEventListeners[eventID.m_nType])
		{
			m_gLock.unlock();//Release();
			return false;
		}
	}

	struct ZEventHandler_TypeEntry *tentry = m_aEventListeners[eventID.m_nType];

	// make sure the named vector is larch enough ;)
	if (tentry->m_mNamedListeners.size() < (unsigned)eventID.m_nSubtypeId + 1)
	{
		tentry->m_mNamedListeners.resize(eventID.m_nSubtypeId + 1);

		// confirm resize
		if (tentry->m_mNamedListeners.size() < (unsigned)eventID.m_nSubtypeId + 1)
		{
			// something got very wrong :/
			assert(false);
			m_gLock.unlock();//Release();
			return false;
		}
	}

	EXTRACHECKS
	(
		// make sure the iterator isn't already there
		ZEventHandler_EventList::iterator it;
		for (it = tentry->m_mNamedListeners[eventID.m_nSubtypeId].begin(); it != tentry->m_mNamedListeners[eventID.m_nSubtypeId].end(); it++)
		{
			if (*it == pListener)
			{
				// adding an already registered listener
				assert(false);
				m_gLock.unlock();//Release();
				return false;
			}
		}
	)	

	tentry->m_mNamedListeners[eventID.m_nSubtypeId].push_back(pListener);

	m_gLock.unlock();//Release();

	return true;
}

void ZEventHandler::UninstallEventHandler(ZEventType::EventType eType, const boost::shared_ptr<ZEventListener>& pListener)
{
	// must be valid input!
	assert(pListener);
	if (!pListener) return;
	m_gLock.lock();//Acquire();

	ZCOUNTLISTENERS(InterlockedDecrement(&nListenCount));

	if (m_uDispatching != 0)
	{
		// in case we're dispatching an event, move the listener to a dead listeners list
		// so that it gets disposed after the event finished processing
		ZEventHandler_DelayedEntry entry;

		if (!m_lDelayed)
		{
			if ((m_lDelayed = new ZEventHandler_DelayedList()) == NULL)
			{
				m_gLock.unlock();//Release();
				return;
			}
		}

		// store the entry for delayed installation
		entry.e_actionType = ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_Uninstall;
		entry.m_eID.m_nType = eType;
		entry.m_pListener = pListener;
		m_lDelayed->push_back(entry);
		
		m_gLock.unlock();//Release();
		return;
	}

	struct ZEventHandler_TypeEntry *tentry = m_aEventListeners ? m_aEventListeners[eType] : NULL;

	// no tentry? means it wasn't installed!
	assert(tentry);

	if (tentry != NULL)
	{
#ifdef UNINSTALL_WITH_REVERSE_ITERATORS
		ZEventHandler_EventList::reverse_iterator it;
		for (it = tentry->m_lEventListeners.rbegin(); it != tentry->m_lEventListeners.rend(); it++)
#else
		ZEventHandler_EventList::iterator it;
		for (it = tentry->m_lEventListeners.begin(); it != tentry->m_lEventListeners.end(); it++)
#endif
		{
			if (*it == pListener)
			{
#ifdef UNINSTALL_WITH_REVERSE_ITERATORS
				tentry->m_lEventListeners.erase(--it.base());
#else
				tentry->m_lEventListeners.erase(it);
#endif
				m_gLock.unlock();//Release();
				return;
			}
		}
	
		// the item wasn't installed!
		assert(false);
	}

	m_gLock.unlock();//Release();
}

void ZEventHandler::UninstallNamedEventHandler(const ZEventID& eventID, const boost::shared_ptr<ZEventListener>& pListener)
{
	m_gLock.lock();//Acquire();

	ZCOUNTLISTENERS(InterlockedDecrement(&nListenCount));

	if (m_uDispatching != 0)
	{
		// in case we're dispatching an event, move the listener to a dead listeners list
		// so that it gets disposed after the event finished processing
		ZEventHandler_DelayedEntry entry;

		if (!m_lDelayed)
		{
			if ((m_lDelayed = new ZEventHandler_DelayedList()) == NULL)
			{
				m_gLock.unlock();//Release();
				return;
			}
		}

		// store the entry for delayed installation
		entry.e_actionType = ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_UninstallNamed;
		entry.m_eID = eventID;
		entry.m_pListener = pListener;
		m_lDelayed->push_back(entry);
		
		m_gLock.unlock();//Release();
		
		return;
	}

	struct ZEventHandler_TypeEntry *tentry = m_aEventListeners ? m_aEventListeners[eventID.m_nType] : NULL;

	// no tentry? means it wasn't installed!
 	assert(tentry);

	if (tentry != NULL)
	{
		if (tentry->m_mNamedListeners.size() > (unsigned)eventID.m_nSubtypeId)
		{
#ifdef UNINSTALL_WITH_REVERSE_ITERATORS
			ZEventHandler_EventList::reverse_iterator it;
			for (it = tentry->m_mNamedListeners[eventID.m_nSubtypeId].rbegin(); it != tentry->m_mNamedListeners[eventID.m_nSubtypeId].rend(); it++)
#else
			ZEventHandler_EventList::iterator it;
			for (it = tentry->m_mNamedListeners[eventID.m_nSubtypeId].begin(); it != tentry->m_mNamedListeners[eventID.m_nSubtypeId].end(); it++)
#endif
			{
				if (*it == pListener)
				{
#ifdef UNINSTALL_WITH_REVERSE_ITERATORS
					tentry->m_mNamedListeners[eventID.m_nSubtypeId].erase(--it.base());
#else
					tentry->m_mNamedListeners[eventID.m_nSubtypeId].erase(it);
#endif
					m_gLock.unlock();//Release();
					return;
				}
			}

			// the item wasn't installed!!
			assert(false);
		}
		else
		{
			// the item wasn't installed!!
			assert(false);
		}
	}

	m_gLock.unlock();//Release();
}

void ZEventHandler::PrioritizeEventHandler(ZEventType::EventType eType, const boost::shared_ptr<ZEventListener>& pListener)
{
	// must be valid input!
	assert(pListener);
	if (!pListener) return;

	m_gLock.lock();//Acquire();

	if (m_uDispatching != 0)
	{
		// if we're dispatching, we must not modify the eventlistener list, so cache this instead
		// it will be done after DispatchEvent has finished
		ZEventHandler_DelayedEntry entry;

		if (!m_lDelayed)
		{
			if ((m_lDelayed = new ZEventHandler_DelayedList()) == NULL)
			{
				m_gLock.unlock();//Release();
				return;
			}
		}

		// store the entry for delayed installation
		entry.e_actionType = ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_Prioritize;
		entry.m_eID.m_nType = eType;
		entry.m_pListener = pListener;
		m_lDelayed->push_back(entry);
	}
	else
	{
		struct ZEventHandler_TypeEntry *tentry = m_aEventListeners ? m_aEventListeners[eType] : NULL;

		// no tentry? means it wasn't installed!
		assert(tentry);

		if (tentry != NULL)
		{
			// do named events 1st...
			unsigned nameds = (unsigned)tentry->m_mNamedListeners.size();

			for (unsigned i = 0; i < nameds; i++)
			{
				ZEventHandler_EventList::iterator it;

				for (it = tentry->m_mNamedListeners[i].begin(); it != tentry->m_mNamedListeners[i].end(); it++)
				{
					if (*it == pListener)
					{
						tentry->m_mNamedListeners[i].push_front(*it);
						tentry->m_mNamedListeners[i].erase(it);
						break;
					}
				}
			}

			ZEventHandler_EventList::iterator it;

			for (it = tentry->m_lEventListeners.begin(); it != tentry->m_lEventListeners.end(); it++)
			{
				if (*it == pListener)
				{
					tentry->m_lEventListeners.push_front(*it);
					tentry->m_lEventListeners.erase(it);
					break;
				}
			}
		}
	}

	m_gLock.unlock();//Release();
}

#ifdef _DEBUG
#define PREPENDTABS \
{for (unsigned int tabs = 0; tabs < nCurrentDepth; tabs ++) {TRACE("->\t");};}

void ZEventHandler::DumpListenerChain(unsigned nCurrentDepth, ZEventType::EventType eType)
{
	m_gLock.Acquire();
	PREPENDTABS;
	TRACE("DumpListenerChain of %lx (%s)\n",this,typeid(*this).name());
	assert(eType < ZEventType::NumTypes);
	struct ZEventHandler_TypeEntry *tentry = m_aEventListeners ? m_aEventListeners[eType] : NULL;
	if (tentry != NULL)
	{
		ZEventHandler_EventList::iterator it;
		unsigned int nmit = 0;
		for (nmit = 0; nmit < tentry->m_mNamedListeners.size(); nmit ++)
		{
			for (it = tentry->m_mNamedListeners[nmit].begin(); it != tentry->m_mNamedListeners[nmit].end(); it++)
			{
				PREPENDTABS;
				TRACE("Named Listener %lx (%s)\n",it->GetPtr_Do_Not_Use_This(),typeid(*(it->GetPtr_Do_Not_Use_This())).name());
			}
		}

		for (it = tentry->m_lEventListeners.begin(); it != tentry->m_lEventListeners.end(); it ++)
		{
			ZDECL(pAsHandler,ZEventHandler,*it);

			PREPENDTABS;
			TRACE("Listener %lx (%s)\n",it->GetPtr_Do_Not_Use_This(),typeid(*(it->GetPtr_Do_Not_Use_This())).name());

			if (pAsHandler)
			{
				pAsHandler->DumpListenerChain(nCurrentDepth + 1, eType);
			}
		}
	}
	PREPENDTABS;
	TRACE("EndDumpListenerChain of %lx\n",this);
	m_gLock.Release();
}
#endif

bool ZEventHandler::DispatchEvent(const boost::shared_ptr<ZEvent>& pEvent)
{
	// must be valid input!
	if (!pEvent) return true;

	// this ensures nothing deletes us while inside DispatchEvent
	//boost::shared_ptr<ZEventHandler> self(this);
	bool bRet = true;
	bool bPropagate = true;

	m_gLock.lock();//Acquire();

	m_uDispatching ++;

	if (pEvent)
	{
		ZEventType::EventType eType = pEvent->GetType();
		const int eSubtype = pEvent->GetSubtypeId();
		assert(eType < ZEventType::NumTypes);

		struct ZEventHandler_TypeEntry *tentry = m_aEventListeners ? m_aEventListeners[eType] : NULL;

		if (tentry != NULL)
		{
			// named events take precedence, see if we have any for this subtype
			if (tentry->m_mNamedListeners.size() > (unsigned)eSubtype)
			{

				ZEventHandler_EventList::iterator it;
				for (it = tentry->m_mNamedListeners[eSubtype].begin(); it != tentry->m_mNamedListeners[eSubtype].end(); it++)
				{
					bool bContinue = (*it)->OnEvent(pEvent);
					if (!bContinue && !pEvent->IsAsync())
					{
						//	should stop propagation here
						bRet = false;
						if (pEvent->IsAbortable())
						{
							bPropagate = false;
							break;
						}
					}
				}

			}

			if (bPropagate)
			{
				ZEventHandler_EventList::iterator it;
				for (it = tentry->m_lEventListeners.begin(); it != tentry->m_lEventListeners.end(); it++)
				{
					bool bContinue = (*it)->OnEvent(pEvent);
					if (!bContinue && !pEvent->IsAsync())
					{
						//	should stop propagation here
						bRet = false;
						if (pEvent->IsAbortable()) break;
					}
				}
			}
		}
	}
	else
	{
		// you shouldn't invoke DispatchEvent with a NULL pEvent!
		assert(false);
	}

	m_uDispatching --;

	// probably a bug
	if (m_uDispatching == 0xFFFFFFFF)
		assert(false);

	if (m_uDispatching == 0 && m_lDelayed != NULL)
	{
		// protect ourselves aganist deletion inside of the following code
		boost::shared_ptr<ZEventHandler> pSecure(this);

		ZEventHandler_DelayedList::iterator it;

		for (it = m_lDelayed->begin(); it != m_lDelayed->end(); it++)
		{
			switch (it->e_actionType)
			{
			case ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_Install:
				InstallEventHandler(it->m_eID.m_nType,it->m_pListener);
				break;
			case ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_InstallNamed:
				InstallNamedEventHandler(it->m_eID,it->m_pListener);
				break;
			case ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_Uninstall:
				UninstallEventHandler(it->m_eID.m_nType,it->m_pListener);
				break;
			case ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_UninstallNamed:
				UninstallNamedEventHandler(it->m_eID,it->m_pListener);
				break;
			case ZEventHandler_DelayedEntry::ZEventHandler_DelayedEntry_Type_Prioritize:
				PrioritizeEventHandler(it->m_eID.m_nType,it->m_pListener);
				break;
			}
		}
		m_lDelayed->clear();
	}

	m_gLock.unlock();//Release();
	return bRet;
}

bool ZEventHandler::SynchronizeEvent(const boost::shared_ptr<ZEvent> &pEvent, const boost::shared_ptr<ZEventListener> &pDestination, bool bSyncOnRootThread)
{
	if (m_pSynchronizeHandler != NULL)
	{
		return m_pSynchronizeHandler->SynchronizeEvent(pEvent, pDestination, bSyncOnRootThread);
	}
	else
	{
		assert(false);
	}

	return false;
}

void ZEventHandler::SetSychronizeHandler(ZEventHandler_SynchronizeHandler *pHandler)
{
	m_pSynchronizeHandler = pHandler;

	if (m_pSynchronizeHandler)
	{
		if (!s_pDispatchListener)
			s_pDispatchListener.reset( new ZSyncDispatchListener() );
		if (!s_pMethodListener)
			s_pMethodListener.reset( new ZSyncMethodListener() );
	}
	else
	{
		s_pDispatchListener.reset();// = NULL;
		s_pMethodListener.reset();// = NULL;
	}
}

bool ZEventHandler::IsSychronizeHandlerSet()
{
	return m_pSynchronizeHandler != 0;
}

void ZEventHandler::LockEventHandler(void)
{
	m_gLock.lock();//Acquire();
}

void ZEventHandler::UnlockEventHandler(void)
{
	m_gLock.unlock();//Release();
}

bool ZEventHandler::SynchronizeDispatch(const boost::shared_ptr<ZEventHandler> &pDispatcher, const boost::shared_ptr<ZEvent> &pEvent, bool bSyncOnRootThread)
{
	const boost::shared_ptr<ZEvent> pSyncEvent(new SynchronizedDispatch_Event(pDispatcher, pEvent));
	return SynchronizeEvent(pSyncEvent, s_pDispatchListener, bSyncOnRootThread);
}

