#include "StreamReceivingTask.h"
#include "IDataReceivedObserver.h"
#include "../resiprocate/reflow/ErrorCode.hxx"
#include "../resiprocate/reflow/MediaStream.hxx"
#include "QilexLogger.hxx"

#define CONNECTION_LOST_TIMEOUT 8

StreamReceivingTask::StreamReceivingTask(flowmanager::Flow *pFlow,
										 IDataReceivedObserver *pDataRecvObserver,
										 unsigned int nTimeCallStart,
#ifdef BILLING
										 const boost::shared_ptr<IBillingController>& pBilling,
#endif
										 const srtp_t pSRTPctx)
//: m_csWaitForPacketsEvent(false, false)
{
	assert(pFlow);
	m_pFlow = pFlow;
	m_pDataRecvObserver = pDataRecvObserver;
	m_nTimeCallStart = nTimeCallStart;
#ifdef BILLING
	m_pBilling = pBilling;
#endif
	m_pSRTPctx = pSRTPctx;

	STREAMTRLOGI("StreamReceivingTask::StreamReceivingTask %p %p", this, m_pSRTPctx);
}

StreamReceivingTask::~StreamReceivingTask(void)
{
}

const int StreamReceivingTask::Run()
{
	STREAMTRLOGI("StreamReceivingTask::Run()");
    
    if(!m_pFlow)
        return 3;

	timespec delay;
	delay.tv_sec = 0;
	delay.tv_nsec = 20 * 1000000;

	struct timeval nLastRcvdTime;
	gettimeofday(&nLastRcvdTime, NULL);


	//rec stat
	time_t start;
	start = nLastRcvdTime.tv_sec;
	unsigned long recived = 0;
	//

	bool timeout_notified = false;
	bool isReconnect = false;

	while (true)
	{
		m_threadShut.lock();
		if (isShutdown())
		{
			m_threadShut.unlock();
			STREAMTRLOGI("StreamReceivingTask(%x)::Run() shutdown", this);

			gettimeofday(&nLastRcvdTime, NULL);
			if(m_pFlow->getComponentId() == RTCP_COMPONENT_ID)
				STREAMTRLOGI("TRSTAT StreamReceivingTask %x(RTCP)::rec %d in %d", this, nLastRcvdTime.tv_sec - start, recived);
			else
				STREAMTRLOGI("TRSTAT StreamReceivingTask %x(RTP)::rec %d in %d", this, nLastRcvdTime.tv_sec - start, recived);

			return 1;
		}
		m_threadShut.unlock();
#ifdef BILLING
		if (m_pBilling)
		{
			if (m_pBilling->NotifyCallProgress(GetTickCount() - m_nTimeCallStart) != BillingResults_Success)
				return 2;
		}
#endif
		bool bRTCP = m_pFlow->getComponentId() == RTCP_COMPONENT_ID;

		unsigned nSize = BufferSize;
		boost::system::error_code errCode = m_pFlow->receive((char*)m_pBuffer, nSize, 100);
		if (errCode.value() == flowmanager::Success && nSize > 0)
		{
			bool bOk = true;
			if(isReconnect){
			     QilexStatemachine sm;
			     sm.sendEvent(QilexStatemachine::EVENT_RECONNECTSUCCESS);
			     sm.detachThread();
			     isReconnect = false;
			}
			if (m_pSRTPctx)
			{
				int* bufCpy = new int[(nSize+3)/4];
				memcpy(bufCpy, m_pBuffer, nSize);

				int nSizeOut = nSize;
				if (bRTCP)
				{
					err_status_t stat;
					if ((stat = srtp_unprotect_rtcp(m_pSRTPctx, bufCpy, (int*)&nSizeOut)) == err_status_ok)
					{
						memcpy(m_pBuffer, bufCpy, nSizeOut);
						nSize = nSizeOut;
					}
					else
						SRTPLOGE("StreamReceivingTask::unprotect fail rtcp");
				}
				else
				{
					err_status_t stat;
					if ((stat = srtp_unprotect(m_pSRTPctx, bufCpy, (int*)&nSizeOut)) == err_status_ok)
					{
						memcpy(m_pBuffer, bufCpy, nSizeOut);
						nSize = nSizeOut;
					}
					else
					{
						SRTPLOGE("StreamReceivingTask::unprotect fail, %d", stat);
						bOk = false;
					}
				}

				delete[] bufCpy;
			}

			if (bOk)
			{
				gettimeofday(&nLastRcvdTime, NULL);

				recived += nSize; //rec stat

				if (bRTCP)
				{
					m_pDataRecvObserver->NotifyRtcpDataReceived(m_pBuffer, nSize);
				}
				else
				{
					m_pDataRecvObserver->NotifyDataReceived(m_pBuffer, nSize);
				}
			}
		}
		else if (!bRTCP)
		{
			struct timeval now;
			gettimeofday(&now, NULL);

			if (now.tv_sec - nLastRcvdTime.tv_sec > CONNECTION_LOST_TIMEOUT)
			{
				if(!timeout_notified)
				{
					STREAMTRLOGI("StreamReceivingTask1::EVENT_CALLEND t=%d tlast=%d", now.tv_sec, nLastRcvdTime.tv_sec);
					QilexStatemachine sm;
					sm.sendEvent(QilexStatemachine::EVENT_CALLEND);
					sm.detachThread();
					timeout_notified = true;
				}
			}
			else if (now.tv_sec - nLastRcvdTime.tv_sec > 2 && !isReconnect){
				STREAMTRLOGE("StreamReceivingTask2::EVENT_RECONNECT t=%d tlast=%d", now.tv_sec, nLastRcvdTime.tv_sec);
				QilexStatemachine sm;
			    sm.sendEvent(QilexStatemachine::EVENT_RECONNECT);
			    sm.detachThread();
			    isReconnect = true;
			}
			nanosleep(&delay, NULL);
		}
	}

	return 1;
}

void StreamReceivingTask::thread()
{
	this->Run();
	resip::Lock lock(m_threadShut);
	m_semThreadShutdown.signal();
}

void StreamReceivingTask::Start()
{
	STREAMTRLOGI("StreamReceivingTask::Start()");
	this->run();
}

void StreamReceivingTask::StopThenWait()
{
	resip::Lock lock(m_threadShut);
	this->shutdown();
	m_semThreadShutdown.wait(m_threadShut);
	this->join();
}
