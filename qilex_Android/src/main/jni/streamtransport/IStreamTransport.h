#ifndef _IStreamTransport_h
#define _IStreamTransport_h

#include "StreamTransportResults.h"
#include "ZEventHandler.h"
#include "srtp.h"
#include <string>

class IPAddrContainer
{
public:
	IPAddrContainer(const std::string& strIP="", unsigned short nPort=0) : m_strIP(strIP), m_nPort(nPort) {}

	std::string		m_strIP;
	unsigned short	m_nPort;
};


class IStreamTransport : public virtual ZEventHandler
{
public :
	virtual StreamTransportResults Bind(
		const std::string &strLocalIpAddress,
		unsigned short nLocalPort,
		bool bUseTURN=false,
		bool bEnableRtcp = true,
		const std::string &strTurnIP="",
		unsigned short nTurnPort=0,
		const char* strTurnLogin="",
		const char* strTurnPassword="",
		const std::string& strUserCertPEM="",
		const std::string& strUserKeyPEM=""
		) = 0;
	
	virtual StreamTransportResults SetPeer(
		const std::string &strRemoteIpAddress,
		unsigned short nRemotePort,
		const std::string &strRemoteRtcpIpAddress = "",
		unsigned short nRemoteRtcpPort = 0
		) = 0;

	virtual void SetSRTPctx(const srtp_t pSRTPctx) = 0;

	virtual unsigned long GetSSRC() = 0;

	virtual IPAddrContainer getRelayTuple() = 0;
	virtual IPAddrContainer getReflexiveTuple() = 0;

	virtual StreamTransportResults SendData(
		unsigned char *pData,
		unsigned int nSize
		) = 0;

	virtual StreamTransportResults SendRtcpData(
		unsigned char *pData,
		unsigned int nSize
		) = 0;

	//! Initialize and start receiving and sending threads
	virtual StreamTransportResults StartTransmission() = 0;

	//! Stop and release receiving and sending threads
	virtual StreamTransportResults StopTransmission() = 0;

	virtual StreamTransportResults StopRecv() = 0;
	virtual StreamTransportResults StopSend() = 0;

	//! Release and delete all object associated with connection
	virtual void CloseConnection() = 0;
};

#endif
