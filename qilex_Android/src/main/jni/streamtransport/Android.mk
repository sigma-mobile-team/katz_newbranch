# Copyright (c) 2011 Sirocco Mobile Sp. z o.o.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := streamtransport

LOCAL_CPP_EXTENSION := .cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../boost_1_57_0
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../boost_1_57_0/boost
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../openssl-1.0.2h/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../commonlib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocate/reTurn
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocate/reTurn/client
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocate
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocate/contrib/srtp/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocate/contrib/srtp/crypto/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../sipXportLib/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../resiprocatejni

LOCAL_SRC_FILES := \
	StreamReceivingTask.cpp \
	StreamSendingTask.cpp \
	StreamTransportFactory.cpp \
	StreamTransportEvent.cpp \
	StreamTransport.cpp \

LOCAL_STATIC_LIBRARIES += \
	libreflow \
	libcommonlib

include $(BUILD_STATIC_LIBRARY)
