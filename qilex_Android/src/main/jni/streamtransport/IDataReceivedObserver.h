#ifndef _IDataReceivedObserver_h
#define _IDataReceivedObserver_h

class IDataReceivedObserver
{
public:
	virtual void NotifyDataReceived(
		const unsigned char *pData,
		unsigned int nSize
		) = 0;

	virtual void NotifyRtcpDataReceived(
		const unsigned char *pData,
		unsigned int nSize
		) = 0;
};

#endif
