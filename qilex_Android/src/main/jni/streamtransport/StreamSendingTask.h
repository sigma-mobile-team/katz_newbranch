#ifndef _StreamSendingTask_h
#define _StreamSendingTask_h

#include "../resiprocate/reflow/Flow.hxx"
#include "../resiprocate/rutil/ThreadIf.hxx"
#ifdef BILLING
#include "IBillingController.h"
#endif
#include <list>
#include <string>
#include "DataPacket.h"
#include "../resiprocate/rutil/Condition.hxx"
#include "../resiprocate/rutil/RecursiveMutex.hxx"

class StreamSendingTask : public resip::ThreadIf
{

public:
	StreamSendingTask(flowmanager::Flow *pFlow, const std::string &strRemoteAddress, unsigned short nRemotePort, unsigned int nTimeCallStart,
#ifdef BILLING
			boost::shared_ptr<IBillingController>& pBilling,
#endif
			const srtp_t pSRTPctx);
	~StreamSendingTask();

	//! Add data packet to send
	void AddDataPacket(const DataPacket& dataPacket);

	void Start();
	void StopThenWait();

	void SetSRTPctx(const srtp_t pSRTPctx) { m_pSRTPctx = pSRTPctx; }

private:

	void SendPacket(const DataPacket & dataPacket, bool bRTCP);

	//! packets waiting to be sent
	std::list<DataPacket> m_lPackets;

	unsigned int m_nTimeCallStart;

	resip::RecursiveMutex m_threadShut;
	resip::Condition m_semThreadShutdown;

	resip::RecursiveMutex m_mAccess;
	resip::Condition m_waitForPacketsCondition;
#ifdef BILLING
	boost::shared_ptr<IBillingController> m_pBilling;
#endif
	srtp_t m_pSRTPctx;

	timeval start;
	int sended;

	//! used to send data
	flowmanager::Flow *m_pFlow;

	boost::asio::ip::address ipRemoteAddress;
	unsigned short nRemotePort;

	//! GThreat implementation
	const int Run();
	void PrepareEndThread();

	// helper method to release packets memory and clear list. It is not thread safe!
	void ReleasePackets();

	virtual void thread();

};

#endif
