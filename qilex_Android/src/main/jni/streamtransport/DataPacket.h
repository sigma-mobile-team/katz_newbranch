#ifndef _DataPacket_h
#define _DataPacket_h

/** Simple class used only to group together raw data and its size.
	It is not responsible for deleting it.
*/
class DataPacket
{
public:
	unsigned char*	Data;
	unsigned int	Size;

	DataPacket(
		unsigned char* pData, 
		unsigned int nSize
		)
	{
		this->Data = pData;
		this->Size = nSize;
	}
};

#endif
