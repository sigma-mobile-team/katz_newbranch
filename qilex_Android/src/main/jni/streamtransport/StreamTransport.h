#ifndef _StreamTransport_h
#define _StreamTransport_h

#include "../resiprocate/reflow/MediaStream.hxx"
#include "../resiprocate/reflow/FlowManager.hxx"
#include "../resiprocate/reflow/Flow.hxx"
#include "IStreamTransport.h"
#include "IDataReceivedObserver.h"
#include "StreamReceivingTask.h"
#include "StreamSendingTask.h"
#ifdef BILLING
#include "IBillingController.h"
#endif
#include <boost/shared_ptr.hpp>
#include <pthread.h>


class STLocker
{
public:
	STLocker(pthread_mutex_t& m)
	{
		_m = &m;
		pthread_mutex_lock(_m);
	}

	~STLocker()
	{
		pthread_mutex_unlock(_m);
	}

private:
	pthread_mutex_t* _m;
};



class StreamTransport :
	public IStreamTransport,
	public IDataReceivedObserver,
	public flowmanager::MediaStreamHandler
{
private:
	boost::shared_ptr<StreamReceivingTask>	m_recvTask;
	boost::shared_ptr<StreamSendingTask>	m_sendTask;

	boost::shared_ptr<StreamReceivingTask>	m_RTCPrecvTask;
	boost::shared_ptr<StreamSendingTask>	m_RTCPsendTask;
#ifdef BILLING
	boost::shared_ptr<IBillingController> m_pBilling;
#endif
	flowmanager::FlowManager	*m_pFlowMng;
	flowmanager::MediaStream	*m_pMediaStream;
	flowmanager::Flow			*m_pFlow;
	flowmanager::Flow			*m_pRtcpFlow;

	srtp_t			m_pSRTPctx;
	unsigned long	m_nSSRC;

	std::string		m_strLocalIpAddress;
	unsigned short	m_nLocalPort;
	std::string		m_strRemoteIpAddress;
	unsigned short	m_nRemotePort;
	std::string		m_strRemoteRtcpIpAddress;
	unsigned short	m_nRemoteRtcpPort;
	bool			m_bUseTURN;

	pthread_mutex_t mMutex;

	void onMediaStreamReady(const StunTuple& rtpTuple, const StunTuple& rtcpTuple);
	void onMediaStreamError(unsigned int errorCode);
	unsigned long getmstime();

protected:
	/*
	StreamTransport(
#ifdef BILLING
			const boost::shared_ptr<IBillingController>& pBilling,
#endif
			unsigned long nSSRC);
			*/

public:
	StreamTransport(
#ifdef BILLING
			const boost::shared_ptr<IBillingController>& pBilling,
#endif
			unsigned long nSSRC);

	~StreamTransport(void);

	StreamTransportResults Bind(
		const std::string &strLocalIpAddress,
		unsigned short nLocalPort,
		bool bUseTURN=false,
		bool bEnableRtcp = true,
		const std::string &strTurnIP="",
		unsigned short nTurnPort=0,
		const char* strTurnLogin="",
		const char* strTurnPassword="",
		const std::string& strUserCertPEM="",
		const std::string& strUserKeyPEM=""
		);
	
	StreamTransportResults SetPeer(
		const std::string &strRemoteIpAddress,
		unsigned short nRemotePort,
		const std::string &strRemoteRtcpIpAddress = "",
		unsigned short nRemoteRtcpPort = 0
		);

	void SetSRTPctx(const srtp_t pSRTPctx)
	{
		STLocker lock(mMutex);
		m_pSRTPctx = pSRTPctx;
	}

	unsigned long GetSSRC()
	{
		STLocker lock(mMutex);
		return m_nSSRC;	
	}

	IPAddrContainer getRelayTuple()
	{
		STLocker lock(mMutex);
		if (m_pFlow)
		{
			StunTuple tuple =  m_pFlow->getRelayTuple();
			return IPAddrContainer(tuple.getAddress().to_string(), tuple.getPort());
		}
		return IPAddrContainer();
	}

	IPAddrContainer getReflexiveTuple()
	{
		STLocker lock(mMutex);
		if (m_pFlow)
		{
			StunTuple tuple =  m_pFlow->getReflexiveTuple();
			return IPAddrContainer(tuple.getAddress().to_string(), tuple.getPort());
		}
		return IPAddrContainer();
	}

	StreamTransportResults SendData(
		unsigned char *pData,
		unsigned int nSize
		);

	StreamTransportResults SendRtcpData(
		unsigned char *pData,
		unsigned int nSize
		);

	StreamTransportResults StartTransmission();
	StreamTransportResults StopTransmission();
	StreamTransportResults StopRecv();
	StreamTransportResults StopSend();


	void NotifyDataReceived(
		const unsigned char *pData,
		unsigned int nSize
		);

	void NotifyRtcpDataReceived(
		const unsigned char *pData,
		unsigned int nSize
		);

	void CloseConnection();
};

#endif
