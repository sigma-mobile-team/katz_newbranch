#ifndef _StreamReceivingTask_h
#define _StreamReceivingTask_h

#include "../resiprocate/reflow/Flow.hxx"
#include "../resiprocate/rutil/ThreadIf.hxx"
#ifdef BILLING
#include "IBillingController.h"
#endif
#include "DataPacket.h"

class IDataReceivedObserver;

class StreamReceivingTask : public resip::ThreadIf
{
private:
	static const unsigned BufferSize = 30000;

	unsigned int m_nTimeCallStart;

	//! used to recv data
	flowmanager::Flow *m_pFlow;
	srtp_t m_pSRTPctx;

	resip::RecursiveMutex m_threadShut;
	resip::Condition m_semThreadShutdown;

#ifdef BILLING
	boost::shared_ptr<IBillingController> m_pBilling;
#endif

	/** behind that is some child of IStreamTransport type.
		It is only use to notify about new data from network */
	IDataReceivedObserver *m_pDataRecvObserver;

	const int Run();

	//! buffer for receiving data
	unsigned char m_pBuffer[BufferSize];

protected:


public:
	StreamReceivingTask(flowmanager::Flow *pFlow,
		IDataReceivedObserver *pDataRecvObserver,
		unsigned int nTimeCallStart,
#ifdef BILLING
		const boost::shared_ptr<IBillingController>& pBilling,
#endif
		const srtp_t pSRTPctx);
	~StreamReceivingTask();
	void Start();
	void StopThenWait();
	virtual void thread();
	void SetSRTPctx(const srtp_t pSRTPctx) { m_pSRTPctx = pSRTPctx; }

};

#endif
