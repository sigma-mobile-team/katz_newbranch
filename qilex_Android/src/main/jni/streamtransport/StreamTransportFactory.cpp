#include "StreamTransportFactory.h"
#include "IStreamTransport.h"
#include "StreamTransport.h"

boost::shared_ptr<IStreamTransport> StreamTransportFactory::Create(
#ifdef BILLING
		const boost::shared_ptr<IBillingController>& pBilling,
#endif
		unsigned long nSSRC)
{
	return boost::shared_ptr<IStreamTransport>(new StreamTransport(
#ifdef BILLING
			pBilling,
#endif
			nSSRC));
}
