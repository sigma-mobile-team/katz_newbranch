#ifndef _StreamTransportResults_h
#define _StreamTransportResults_h

enum StreamTransportResults
{
	StreamTransportResults_Success = 0,
	StreamTransportResults_GeneralError
};

#endif
