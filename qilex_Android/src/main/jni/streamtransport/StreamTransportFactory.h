#ifndef _StreamTransportFactory_h
#define _StreamTransportFactory_h

#include <string>
#ifdef BILLING
#include "IBillingController.h"
#endif
#include <boost/shared_ptr.hpp>

class IStreamTransport;

class StreamTransportFactory
{
public:
	static  boost::shared_ptr<IStreamTransport> Create(
#ifdef BILLING
			const boost::shared_ptr<IBillingController>& pBilling,
#endif
			unsigned long nSSRC);
};

#endif
