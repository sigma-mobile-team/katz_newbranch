#include "StreamSendingTask.h"
#include "../resiprocate/reflow/MediaStream.hxx"
#include "../resiprocatejni/QilexLogger.hxx"
#include "QilexLogger.hxx"

StreamSendingTask::StreamSendingTask(flowmanager::Flow *pFlow,
									 const std::string &strRemoteAddress,
									 unsigned short nRemotePort,
									 unsigned int nTimeCallStart,
#ifdef BILLING
									 const boost::shared_ptr<IBillingController>& pBilling,
#endif
									 const srtp_t pSRTPctx)
{
	assert(pFlow);
	m_pFlow = pFlow;
	ipRemoteAddress = boost::asio::ip::address_v4::from_string(strRemoteAddress.c_str());
	this->nRemotePort = nRemotePort;
	m_nTimeCallStart = nTimeCallStart;
#ifdef BILLING
	m_pBilling = pBilling;
#endif
	m_pSRTPctx = pSRTPctx;

	STREAMTRLOGI("StreamSendingTask::StreamSendingTask");
}

StreamSendingTask::~StreamSendingTask(void)
{
}

const int StreamSendingTask::Run()
{
	STREAMTRLOGI("StreamSendingTask::Run");

	gettimeofday(&start, NULL);
	sended = 0;

	resip::Mutex m;
	m_waitForPacketsCondition.wait(m);

	/*
	while (true)
	{
		//STREAMTRLOGI("StreamSendingTask::Run()");

		if (isShutdown())
		{
			STREAMTRLOGI("StreamSendingTask::Run shutdown");
			return 1;
		}
#ifdef BILLING
		if (m_pBilling)
		{
			if (m_pBilling->NotifyCallProgress(GetTickCount() - m_nTimeCallStart) != BillingResults_Success)
				return 2;
		}
#endif

		m_mAccess.lock();
		if (m_lPackets.size() == 0)
		{
			m_mAccess.unlock();
			m_waitForPacketsCondition.wait(m);
			continue;

			//tu cos trzeba bedzie wymyslic ....
			//bo w resip/pthreads nie ma odpowiednika dla ResetEvent
			//win32 m_csWaitForPacketsEvent.Reset();
		}
		else
		{
			STREAMTRLOGI("StreamSendingTask::Run packets send begin");

			bool bRTCP = false;
			if (m_pFlow->getComponentId() == RTCP_COMPONENT_ID)
				bRTCP = true;

			DataPacket dataPacket = m_lPackets.front();
			this->SendPacket(dataPacket, bRTCP);

			STREAMTRLOGI("StreamSendingTask::Run packets to send: %d", m_lPackets.size());

			m_lPackets.pop_front();
			delete [] dataPacket.Data;
		}
		m_mAccess.unlock();
	}
	*/

	timeval nLastRcvdTime;
	gettimeofday(&nLastRcvdTime, NULL);

    /*
	if(m_pFlow->getComponentId() == RTCP_COMPONENT_ID)
		STREAMTRLOGI("TRSTAT StreamSendingTask %x(RTCP)::rec %d in %d", this, nLastRcvdTime.tv_sec - start.tv_sec, sended);
	else
		STREAMTRLOGI("TRSTAT StreamSendingTask %x(RTP)::rec %d in %d", this, nLastRcvdTime.tv_sec - start.tv_sec, sended);
     */

	STREAMTRLOGI("StreamSendingTask::Run/");
	return 1;
}

void StreamSendingTask::PrepareEndThread()
{
	STREAMTRLOGI("StreamSendingTask::PrepareEndThread()");
	m_mAccess.lock();
	ReleasePackets();
	m_mAccess.unlock();
	STREAMTRLOGI("StreamSendingTask::PrepareEndThread() /");
}

void StreamSendingTask::AddDataPacket(const DataPacket& dataPacket)
{
	m_mAccess.lock();
	/*
	if (!isShutdown())
	{

		m_lPackets.push_back(dataPacket);
		m_waitForPacketsCondition.signal();
	}
	*/
    
    if(m_pFlow)
    {

        bool bRTCP = false;
        if (m_pFlow->getComponentId() == RTCP_COMPONENT_ID)
            bRTCP = true;

        this->SendPacket(dataPacket, bRTCP);
        sended += dataPacket.Size;
    }
	m_mAccess.unlock();
}

void StreamSendingTask::thread()
{
	STREAMTRLOGI("StreamSendingTask::thread()");
	this->Run();
	STREAMTRLOGI("StreamSendingTask::thread() 1");
	PrepareEndThread();
	STREAMTRLOGI("StreamSendingTask::thread() 2");

	STREAMTRLOGI("StreamSendingTask::thread() 3");

	resip::Lock lock(m_threadShut);
	STREAMTRLOGI("StreamSendingTask::thread() 4");
	m_semThreadShutdown.signal();
	STREAMTRLOGI("StreamSendingTask::thread()/");
}

void StreamSendingTask::Start()
{
	STREAMTRLOGI("StreamSendingTask::Run()");
	//this->run();
	STREAMTRLOGI("StreamSendingTask::Run()/");
}

void StreamSendingTask::StopThenWait()
{
	return;


	STREAMTRLOGI("StreamSendingTask::StopThenWait()");
	this->shutdown();
	STREAMTRLOGI("StreamSendingTask::StopThenWait() 1");
	m_waitForPacketsCondition.signal();

	resip::Lock lock(m_threadShut);
	STREAMTRLOGI("StreamSendingTask::StopThenWait() 2");
	m_semThreadShutdown.wait(m_threadShut, 200);
	STREAMTRLOGI("StreamSendingTask::StopThenWait()/");
	this->join();
}

void StreamSendingTask::ReleasePackets()
{
	unsigned nSize = m_lPackets.size();
	for (unsigned i = 0; i < nSize; i++)
	{
		DataPacket dataPacket = m_lPackets.front();
		delete [] dataPacket.Data;
		m_lPackets.pop_front();
	}
}

//#include "../commonlib/GTimeWindowCounter.h"

void StreamSendingTask::SendPacket(const DataPacket & dataPacket, bool bRTCP)
{
    if(!m_pFlow)
        return;
    
    int nNewSize = dataPacket.Size;

	if (m_pSRTPctx)
	{
		long* SRTPBuf = new long[(dataPacket.Size + SRTP_MAX_TRAILER_LEN + 4 + 3) / 4]; //must be 32bytes aligned!
		memcpy(SRTPBuf, dataPacket.Data, dataPacket.Size);
		if (bRTCP) {
			err_status_t status;
			if ((status = srtp_protect_rtcp(m_pSRTPctx, SRTPBuf, &nNewSize))== err_status_ok) {
				m_pFlow->rawSendTo(ipRemoteAddress, nRemotePort,
						(char*) SRTPBuf, nNewSize);
			}
		} else {
			err_status_t status;
			if ((status = srtp_protect(m_pSRTPctx, SRTPBuf, &nNewSize))== err_status_ok) {
				m_pFlow->rawSendTo(ipRemoteAddress, nRemotePort,
						(char*) SRTPBuf, nNewSize);
			}
		}
		delete[] SRTPBuf;
	}
	else
		m_pFlow->rawSendTo(ipRemoteAddress, nRemotePort, (char*) dataPacket.Data, dataPacket.Size);

    /*static GTimeWindowCounter counter(1000);
    static GTimeWindowCounter counter2(10000);
    counter.AddSample(nNewSize+46); //46bytes (18kbps) of transport layer headers
    counter2.AddSample(nNewSize+46);
    STREAMTRLOGI("Bitrate:av=%5.2fkbps av10=%5.2fkbps minP=%3dB maxP=%3dB meanP=%3dB avP=%3dB", counter.GetSum()*8/1024.0f, counter2.GetSum()*8/10240.0f, (int)counter.GetMinValue(), (int)counter.GetMaxValue(), (int)counter.GetMeanValue(), (int)counter.GetAveragedValue());
*/
}
