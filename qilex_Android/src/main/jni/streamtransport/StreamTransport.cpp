#include "StreamTransport.h"
#include "StreamTransportEvent.h"
#include "../resiprocate/reflow/MediaStream.hxx"
#include "../resiprocate/reflow/FlowManager.hxx"
#include "../resiprocate/reflow/Flow.hxx"
#include "../resiprocatejni/QilexLogger.hxx"
#include "QilexStatemachine.hxx"
#include "RootCert.h"
#include "QilexLogger.hxx"

#define BINDING_TIMEOUT 11

StreamTransport::StreamTransport(
#ifdef BILLING
		const boost::shared_ptr<IBillingController>& pBilling,
#endif
		unsigned long nSSRC) {
#ifdef BILLING
	m_pBilling = pBilling;
#endif
	m_nSSRC = nSSRC;

	m_pFlowMng = NULL;
	m_pMediaStream = NULL;
	m_pFlow = NULL;
	m_pRtcpFlow = NULL;

	m_pSRTPctx = NULL;

	m_nLocalPort = 0;
	m_nRemotePort = 0;

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
	pthread_mutex_init(&mMutex, &attr);
	pthread_mutexattr_destroy(&attr);
}

StreamTransport::~StreamTransport(void) {
	STLocker lock(mMutex);

	LOGI("StreamTransport::~StreamTransport");
	CloseConnection();

	if (m_pSRTPctx) {
		if (srtp_dealloc(m_pSRTPctx) != err_status_ok)
			assert(0);
	}
	LOGI("/StreamTransport::~StreamTransport");
}

unsigned long StreamTransport::getmstime() {
	struct timeval t;
	gettimeofday(&t, NULL);
	return (t.tv_sec * 1000000) + (t.tv_usec / 1000);
}

void StreamTransport::onMediaStreamReady(const StunTuple& rtpTuple,
		const StunTuple& rtcpTuple) {
	printf("=== stream ready ====\n");
}

void StreamTransport::onMediaStreamError(unsigned int errorCode) {
	printf("=== stream ERROR %d====\n", errorCode);
}

StreamTransportResults StreamTransport::Bind(
		const std::string &strLocalIpAddress, unsigned short nLocalPort,
		bool bUseTURN, bool bEnableRtcp, const std::string &strTurnIP,
		unsigned short nTurnPort, const char* strTurnLogin,
		const char* strTurnPassword, const std::string& strUserCertPEM,
		const std::string& strUserKeyPEM) {
	STLocker lock(mMutex);

	if (bUseTURN)
		LOGI("StreamTransport::Bind turn_addr=%s", strTurnIP.c_str());
	else
		LOGI("StreamTransport::Bind local=%s", strLocalIpAddress.c_str());

	if (bUseTURN || m_strLocalIpAddress != strLocalIpAddress
			|| m_nLocalPort != nLocalPort || m_bUseTURN != bUseTURN) {
		m_strLocalIpAddress = strLocalIpAddress;
		m_nLocalPort = nLocalPort;
		m_bUseTURN = bUseTURN;

		if (m_pMediaStream) {
			delete m_pMediaStream;
			m_pMediaStream = NULL;
		}

		if (m_pFlowMng) {
			delete m_pFlowMng;
			m_pFlowMng = NULL;
		}

		m_pFlowMng =
				new flowmanager::FlowManager(/*ROOT_CERT_QILEX, strUserCertPEM, strUserKeyPEM*/);
		if (m_pFlowMng == NULL)
			return StreamTransportResults_GeneralError;

		m_pMediaStream = m_pFlowMng->createMediaStream(*this,
				StunTuple(StunTuple::UDP,
						boost::asio::ip::address(
								boost::asio::ip::address_v4::from_string(
										strLocalIpAddress.c_str())),
						nLocalPort), bEnableRtcp,
				bUseTURN ?
						flowmanager::MediaStream::TurnAllocation :
						flowmanager::MediaStream::NoNatTraversal,
				strTurnIP.c_str(), nTurnPort, strTurnLogin, strTurnPassword);

		if (m_pMediaStream == NULL)
			return StreamTransportResults_GeneralError;

		m_pFlow = m_pMediaStream->getRtpFlow();
		m_pRtcpFlow = m_pMediaStream->getRtcpFlow();
		timeval nTime;
		gettimeofday(&nTime, NULL);
		time_t nStart = nTime.tv_sec;
		bool bWasConnecting = false;

		if (!m_pFlow) {
			LOGI("StreamTransport::Bind !m_pFlow");
			return StreamTransportResults_GeneralError;
		}

		if (!m_pRtcpFlow)
			LOGI("StreamTransport::Bind !m_pRtcpFlow");

		while (!m_pFlow->isReady()
				|| !(!m_pRtcpFlow || m_pRtcpFlow->isReady()
						|| m_pRtcpFlow->isConnected())) {
			if (!m_pFlow->isUnconnected())
				bWasConnecting = true;

			gettimeofday(&nTime, NULL);

			if ((m_pFlow->isUnconnected() && bWasConnecting)
					|| (nTime.tv_sec - nStart > BINDING_TIMEOUT)) {
				int time = nTime.tv_sec - nStart;
				LOGI(
						"StreamTransport::Bind error .. returning, bWasConnecting = %d, time  = %d",
						bWasConnecting, time);

				if (m_pFlow->isUnconnected() && bWasConnecting)
					LOGI(
							"StreamTransport::Bind error .. m_pFlow->isUnconnected() && bWasConnecting");

				//error
				if (m_pMediaStream) {
					delete m_pMediaStream;
					m_pMediaStream = NULL;
				}
				if (m_pFlowMng) {
					delete m_pFlowMng;
					m_pFlowMng = NULL;
				}
				m_pFlow = NULL;
				m_pRtcpFlow = NULL;
				return StreamTransportResults_GeneralError;
			}

			timespec delay;
			delay.tv_sec = 0;
			delay.tv_nsec = 50 * 1000000;
			nanosleep(&delay, NULL);
		}
	}

//	LOGI("StreamTransport::Bind = StreamTransportResults_Success");
	return StreamTransportResults_Success;
}

StreamTransportResults StreamTransport::SetPeer(
		const std::string &strRemoteIpAddress, unsigned short nRemotePort,
		const std::string &strRemoteRtcpIpAddress,
		unsigned short nRemoteRtcpPort) {
	STLocker lock(mMutex);

	LOGI("StreamTransport::SetPeer");

	if (m_strRemoteIpAddress != strRemoteIpAddress
			|| m_nRemotePort != nRemotePort) {

		LOGI("StreamTransport::SetPeer 1");

		m_strRemoteIpAddress = strRemoteIpAddress;
		m_nRemotePort = nRemotePort;
		m_strRemoteRtcpIpAddress = strRemoteRtcpIpAddress;
		m_nRemoteRtcpPort = nRemoteRtcpPort;

		if (m_pFlow) {
			m_pFlow->setActiveDestination(strRemoteIpAddress.c_str(),
					nRemotePort);
			LOGI("StreamTransport::SetPeer 2");
		}

		if (m_pRtcpFlow)
			m_pRtcpFlow->setActiveDestination(m_strRemoteRtcpIpAddress.c_str(),
					nRemoteRtcpPort);
	}

	return StreamTransportResults_Success;
}

StreamTransportResults StreamTransport::SendData(unsigned char *pData,
		unsigned int nSize) {
	STLocker lock(mMutex);

	//LOGI("StreamTransport::SendData");

	assert(m_sendTask && "first call StartTransmission()");

	if (m_sendTask) {
		unsigned char *_pData = new unsigned char[nSize];
		memcpy(_pData, pData, nSize);
		m_sendTask->AddDataPacket(DataPacket(_pData, nSize));
		//LOGI("StreamTransport::SendData::StreamTransportResults_Success");
		return StreamTransportResults_Success;
	}

	return StreamTransportResults_GeneralError;
}

StreamTransportResults StreamTransport::SendRtcpData(unsigned char *pData,
		unsigned int nSize) {
	STLocker lock(mMutex);

	if (m_RTCPsendTask) {
		unsigned char *_pData = new unsigned char[nSize];
		memcpy(_pData, pData, nSize);

		m_RTCPsendTask->AddDataPacket(DataPacket(_pData, nSize));
		return StreamTransportResults_Success;
	}

	return StreamTransportResults_GeneralError;
}

StreamTransportResults StreamTransport::StartTransmission() {
	STLocker lock(mMutex);

	STREAMTRLOGI("StreamTransport::StartTransmission()");
	StopTransmission();
	STREAMTRLOGI("StreamTransport::StartTransmission() 1");

	unsigned int TimeCallStart = getmstime();

	if (!m_pFlow)
		return StreamTransportResults_GeneralError;

	m_sendTask.reset(
			new StreamSendingTask(m_pFlow, m_strRemoteIpAddress, m_nRemotePort,
					TimeCallStart,
#ifdef BILLING
					m_pBilling,
#endif
					m_pSRTPctx));

	STREAMTRLOGI("StreamTransport::StartTransmission() 2 pFlow=%p", m_pFlow);

	m_recvTask.reset(new StreamReceivingTask(m_pFlow, this, TimeCallStart,
#ifdef BILLING
			m_pBilling,
#endif
			m_pSRTPctx));

	STREAMTRLOGI("StreamTransport::StartTransmission() 3");

	if (m_strRemoteRtcpIpAddress != "0.0.0.0"
			&& !m_strRemoteRtcpIpAddress.empty()) {
		m_RTCPsendTask.reset(
				new StreamSendingTask(m_pRtcpFlow, m_strRemoteRtcpIpAddress,
						m_nRemoteRtcpPort, TimeCallStart,
#ifdef BILLING
						m_pBilling,
#endif
						m_pSRTPctx));
	}

	STREAMTRLOGI("StreamTransport::StartTransmission() 4");
	if(m_pRtcpFlow)
		m_RTCPrecvTask.reset(
				new StreamReceivingTask(m_pRtcpFlow, this, TimeCallStart,
#ifdef BILLING
					m_pBilling,
#endif
					m_pSRTPctx));

	STREAMTRLOGI("StreamTransport::StartTransmission() 5");

	if (!m_sendTask || !m_recvTask)
		return StreamTransportResults_GeneralError;

	STREAMTRLOGI("StreamTransport::StartTransmission() 6");

	m_sendTask->Start();
	m_recvTask->Start();

	STREAMTRLOGI("StreamTransport::StartTransmission() 7");

	if (m_RTCPsendTask)
		m_RTCPsendTask->Start();

	if (m_RTCPrecvTask)
		m_RTCPrecvTask->Start();

	STREAMTRLOGI("StreamTransport::StartTransmission() /");

	return StreamTransportResults_Success;
}

StreamTransportResults StreamTransport::StopRecv() {
	STLocker lock(mMutex);
	if (m_RTCPrecvTask) {
		m_RTCPrecvTask->StopThenWait();
		m_RTCPrecvTask.reset();
	}
	if (m_recvTask) {
		m_recvTask->StopThenWait();
		m_recvTask.reset();
	}
}

StreamTransportResults StreamTransport::StopSend() {
	STLocker lock(mMutex);
	if (m_RTCPsendTask) {
		m_RTCPsendTask->StopThenWait();
		m_RTCPsendTask.reset();
	}
	if (m_sendTask) {
		m_sendTask->StopThenWait();
		m_sendTask.reset();
	}
}

StreamTransportResults StreamTransport::StopTransmission() {
	STLocker lock(mMutex);

	STREAMTRLOGI("StreamTransport::StopTransmission()");

	StopSend();
	StopRecv();

	STREAMTRLOGI("StreamTransport::StopTransmission()/");

	return StreamTransportResults_Success;
}

void StreamTransport::NotifyDataReceived(const unsigned char *pData,
		unsigned int nSize) {
	boost::shared_ptr<StreamTransportEvent> pEvt =
			StreamTransportEvent::CreateDataReceivedEvent(pData, nSize);

	if (pEvt)
		DispatchEvent(pEvt);
}

void StreamTransport::NotifyRtcpDataReceived(const unsigned char *pData,
		unsigned int nSize) {
	const boost::shared_ptr<StreamTransportEvent> pEvt =
			StreamTransportEvent::CreateRtcpDataReceivedEvent(pData, nSize);

	if (pEvt)
		DispatchEvent(pEvt);
}

void StreamTransport::CloseConnection() {
	STLocker lock(mMutex);

	StopTransmission();

	if (m_pMediaStream) {
		delete m_pMediaStream;
		m_pMediaStream = NULL;
	}

	if (m_pFlowMng) {
		delete m_pFlowMng;
		m_pFlowMng = NULL;
	}

	m_pFlow = NULL;
	m_pRtcpFlow = NULL;

	STREAMTRLOGI("/StreamTransport::CloseConnection()");
}
