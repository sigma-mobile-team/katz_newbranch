#ifndef _StreamTransportEvent_h
#define _StreamTransportEvent_h

#include <boost/shared_ptr.hpp>
#include "zevent.h"

class StreamTransportEvent : public virtual ZAsyncEvent
{
protected:
	unsigned char *m_pData;
	unsigned int m_nSize;

	const bool ValidateSubtypeId(const int nSubtypeId) const;

public:
	static const int MINVAL				= 1;
	static const int DataReceived		= 1;
	static const int RtcpDataReceived	= 2;
	static const int ErrorOccured		= 3;
	static const int MAXVAL				= ErrorOccured;

	StreamTransportEvent(int nSubtypeId);
	~StreamTransportEvent(void);

	const ZEventType::EventType GetType(void) const;

	static boost::shared_ptr<StreamTransportEvent> CreateDataReceivedEvent(
		const unsigned char *pData,
		unsigned int nSize
		);

	static boost::shared_ptr<StreamTransportEvent> CreateRtcpDataReceivedEvent(
		const unsigned char *pData,
		unsigned int nSize
		);

	unsigned const char* GetData() const;
	unsigned int GetDataSize() const;
};

#endif
