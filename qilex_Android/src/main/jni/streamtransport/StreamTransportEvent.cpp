#include "StreamTransportEvent.h"

StreamTransportEvent::StreamTransportEvent(int nSubtypeId)
	: ZAsyncEvent(nSubtypeId)
{
	m_pData = NULL;
	m_nSize = NULL;
}

StreamTransportEvent::~StreamTransportEvent(void)
{
	if (m_pData != NULL)
		delete [] m_pData;
}

const bool StreamTransportEvent::ValidateSubtypeId(const int nSubtypeId) const
{
	return nSubtypeId >= MINVAL && nSubtypeId <= MAXVAL;
}

const ZEventType::EventType StreamTransportEvent::GetType(void) const
{
	return ZEventType::StreamTransport;
}

boost::shared_ptr<StreamTransportEvent> StreamTransportEvent::CreateDataReceivedEvent(const unsigned char *pData,
																			  unsigned int nSize)
{
	assert(pData != NULL && nSize > 0 && "wrong pData or nSize parameter");

	boost::shared_ptr<StreamTransportEvent> ret(new StreamTransportEvent(DataReceived));
	if (ret)
	{
		ret->m_pData = new unsigned char[nSize];
		memcpy(ret->m_pData, pData, nSize);
		ret->m_nSize = nSize;
	}
	return ret;
}

boost::shared_ptr<StreamTransportEvent> StreamTransportEvent::CreateRtcpDataReceivedEvent(const unsigned char *pData,
																				unsigned int nSize)
{
	assert(pData != NULL && nSize > 0 && "wrong pData or nSize parameter");

	boost::shared_ptr<StreamTransportEvent> ret(new StreamTransportEvent(RtcpDataReceived) );
	if (ret)
	{
		ret->m_pData = new unsigned char[nSize];
		memcpy(ret->m_pData, pData, nSize);
		ret->m_nSize = nSize;
	}
	return ret;
}

unsigned const char* StreamTransportEvent::GetData() const
{
	return m_pData;
}

unsigned int StreamTransportEvent::GetDataSize() const
{
	return m_nSize;
}
