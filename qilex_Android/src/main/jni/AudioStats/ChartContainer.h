#ifndef CHARTCONTAINER_H_
#define CHARTCONTAINER_H_
#undef min
#undef max
#include <map>
#include "TimeChart.h"
#include <boost/signals2.hpp>

class ChartContainer
{
public:
	ChartContainer();
	virtual ~ChartContainer();
	typedef std::map<std::string, TimeChart::pointer > ChartMap;

	void AddChart(const std::string& name, const TimeChart::pointer& chart);

	void SetTimeOffset(const TimeChart::clock::time_point &offset);
	ChartMap &charts();
	float max_time();
protected:
	TimeChart::clock::time_point _time_offset;
	ChartMap _charts;
	
};

#endif 
