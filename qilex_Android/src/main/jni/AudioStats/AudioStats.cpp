

#include "AudioStats.h"
#include <thread>
#include <fstream>
#include <algorithm>
#include <vector>
#include <boost/bind.hpp>

using namespace std;

AudioStats::AudioStats()
{

}

AudioStats::~AudioStats()
{
//#ifdef DEBUG
	testLogStatic(1.0f);
//#endif
}

ChartContainer& AudioStats::chartContainer()
{
	return _container;
}


void AudioStats::testLog()
{
	std::fstream file;

	file.open ("/mnt/sdcard/HaloTest/textTest.txt", std::ios_base::in | std::ios_base::out | std::ios_base::trunc);
	if (!file.is_open())
		return;

	auto &charts = _container.charts();
	typedef pair<TimeChart::map_type::iterator, TimeChart::map_type::iterator> Pair;
	typedef vector<Pair> IteratorsPair;
	IteratorsPair iterators;

	auto comparer = [](const Pair& old_pair, const Pair& new_pair)
		{
			bool old_end = old_pair.first == old_pair.second;
			if (old_end)
				return true;
			bool new_end = new_pair.first == new_pair.second;
			if (new_end)
				return false;
			return new_pair.first->first < old_pair.first->first;
		};

	file << "Time,";
	for (auto& kv : charts)
	{
		file << kv.first << ",";

		auto p = std::make_pair(kv.second->data().begin(), kv.second->data().end());
		iterators.push_back(p);
	}

	file << std::endl;



	float max_time = _container.max_time();

	float t = 0.0f;
	while (true)
	{
		auto iterator_iterator = std::min_element(iterators.begin(), iterators.end(), comparer);
		if (iterator_iterator->first == iterator_iterator->second)
			break;

		t = iterator_iterator->first->first;
		iterator_iterator->first ++;

		file << t << ",";

		for (auto& kv : charts)
			file << kv.second->valueAt(t) << ",";

		file << std::endl;

	}







	file.close();
}


void AudioStats::testLogStatic(float distance)
{
	std::fstream file;

	file.open ("/mnt/sdcard/HaloTest/textTest.txt", std::ios_base::in | std::ios_base::out | std::ios_base::trunc);
	if (!file.is_open())
		return;

	auto &charts = _container.charts();
	file << "Time,";
	for (auto& kv : charts)
		file << kv.first << ",";
	file << std::endl;



	float max_time = _container.max_time();

	float t = 0.0f;
	for (float t = 0.0f; t < max_time; t += distance)
	{
		file << t << ",";

		for (auto& kv : charts)
			file << kv.second->valueAtRange(t, t + distance) << ",";

		file << std::endl;

	}

	for (auto& kv : charts)
	{
		file << "-------------";
		file << "Time,";
		file << kv.first << ",";
		file << std::endl;

		for (auto &entry : kv.second->data())
			file << entry.first << "," << entry.second << "," << std::endl;

	}

	file.close();

}
