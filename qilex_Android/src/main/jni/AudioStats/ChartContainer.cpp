/*
 * TimeChart.cpp
 *
 *  Created on: 05-06-2013
 *      Author: Mateusz
 */

#include "ChartContainer.h"
#include <thread>
#include <boost/bind.hpp>
#include <algorithm>

ChartContainer::ChartContainer()
{

}

ChartContainer::~ChartContainer()
{

}


void ChartContainer::AddChart(const std::string& name, const TimeChart::pointer& chart)
{
	_charts[name] = chart;
	chart->SetTimeOffset(_time_offset);
}

void ChartContainer::SetTimeOffset(const TimeChart::clock::time_point &offset)
{
	_time_offset = offset;
}

ChartContainer::ChartMap &ChartContainer::charts()
{
	return _charts;
}

float ChartContainer::max_time()
{
	auto it = std::max_element(_charts.begin(), _charts.end(),
			[](ChartMap::value_type &v1, ChartMap::value_type &v2) -> bool
			{
				return v2.second->max_time() > v1.second->max_time();
			});

	if (it == _charts.end())
		return 0.0f;
	return it->second->max_time();
}
