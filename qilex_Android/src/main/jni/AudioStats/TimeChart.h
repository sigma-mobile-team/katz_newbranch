
#ifndef TIMECHART_H_
#define TIMECHART_H_
#undef min
#undef max
#include <map>
#include <memory>
#include <chrono>
#include <boost/shared_ptr.hpp>
#include <thread>
#include "../resiprocate/rutil/RecursiveMutex.hxx"
#include "../resiprocate/rutil/Lock.hxx"

class TimeChart
{
public:
	typedef boost::shared_ptr<TimeChart> pointer;
	typedef std::map<float, float> map_type;
	typedef std::chrono::high_resolution_clock clock;


	typedef resip::RecursiveMutex Mutex;
	typedef resip::Lock Lock;

	TimeChart();
	virtual ~TimeChart();

	virtual float valueAt(float time);
	virtual float valueAtRange(float time1, float time2);

	void AddValue(float time, float value);
	void AddValueNow(float value);

	void erase_older_than(float time);

	map_type& data();

	void SetTimeOffset(const clock::time_point &offset);

	float max_time();
protected:
	Mutex _mutex;
	clock::time_point _time_offset;
	map_type _timeToValue;
};

class MeanTimeChart : public TimeChart
{
public:
	float valueAtRange(float time1, float time2);
};

#endif /* FUNCTORSQUEUE_H_ */
