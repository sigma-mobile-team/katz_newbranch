#ifndef AUDIOSTATS_H_
#define AUDIOSTATS_H_
#undef min
#undef max
#include <map>
#include "TimeChart.h"
#include "Utils.h"
#include "ChartContainer.h"
#include <boost/signals2.hpp>

class AudioStats : public boost::signals2::trackable
{
public:
	AudioStats();
	virtual ~AudioStats();

	ChartContainer& chartContainer();
	void testLog();
	void testLogStatic(float distance = 1.0f);
protected:
	void ResetIndicators();

	float          _duplicatePacketsIndicator;
	float          _bitrateChangeIndicator;

	ChartContainer _container;
};






#endif 
