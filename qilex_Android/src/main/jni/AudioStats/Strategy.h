
#ifndef STRATEGY_H_
#define STRATEGY_H_
#undef min
#undef max
#include <map>
#include <list>
#include <memory>
#include <chrono>
#include <boost/shared_ptr.hpp>
#include <thread>
#include "../resiprocate/rutil/RecursiveMutex.hxx"
#include "../resiprocate/rutil/Lock.hxx"


class Strategy
{
public:
	virtual ~Strategy(){}

	virtual void operator()(){}

	typedef boost::shared_ptr<Strategy> pointer;
};


class StrategyList : public Strategy
{
public:
	void operator()();

	void Add(const Strategy::pointer &strategy);
protected:
	std::list<Strategy::pointer> _strategies;
};

#endif /* FUNCTORSQUEUE_H_ */
