/*
 * TimeChart.cpp
 *
 *  Created on: 05-06-2013
 *      Author: Mateusz
 */

#include "Strategy.h"

#include <boost/bind.hpp>
#include <boost/thread.hpp>




void StrategyList::operator()()
{
	for (auto &strategy : _strategies)
		(*strategy)();
}

void StrategyList::Add(const Strategy::pointer &strategy)
{
	_strategies.push_back(strategy);
}
