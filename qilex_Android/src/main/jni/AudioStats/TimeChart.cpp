/*
 * TimeChart.cpp
 *
 *  Created on: 05-06-2013
 *      Author: Mateusz
 */

#include "TimeChart.h"

#include <boost/bind.hpp>


TimeChart::TimeChart()
{

}

TimeChart::~TimeChart() {

}


float TimeChart::valueAt(float time)
{
	Lock lock(_mutex);

	auto upper = _timeToValue.upper_bound(time);
	if (upper == _timeToValue.begin())
		return 0;
	auto lower = upper --;
	if (lower->first == time || upper == _timeToValue.end())
		return lower->second;

	auto distance_value = upper->second - lower->second;
	auto distance_time = upper->first - lower->first;
	auto value_time = time - upper->first;
	auto percent = value_time / distance_time;
	return upper->second + distance_value * percent;
}


float TimeChart::valueAtRange(float time1, float time2)
{
	Lock lock(_mutex);
	assert(time1 <= time2);
	auto itbegin = _timeToValue.lower_bound(time1);
	auto itend = _timeToValue.upper_bound(time2);

	float sum = 0.0f;
	for (auto it = itbegin; it != itend; it ++)
		sum += it->second;
	return sum;
}

void TimeChart::AddValue(float time, float value)
{
	Lock lock(_mutex);
	_timeToValue[time] = value;
}

void TimeChart::AddValueNow(float value)
{
	auto now = clock::now();
	auto t = std::chrono::duration_cast<std::chrono::milliseconds> (now-_time_offset);
	AddValue(t.count() / 1000.0f, value);
}

float TimeChart::max_time()
{
	Lock lock(_mutex);
	if (_timeToValue.empty())
		return 0.0f;
	return (--_timeToValue.end())->first;
}

void TimeChart::erase_older_than(float time)
{
	Lock lock(_mutex);
	auto from = _timeToValue.begin();
	auto to = _timeToValue.upper_bound(time);
	_timeToValue.erase(from, to);
}

TimeChart::map_type& TimeChart::data()
{
	return _timeToValue;
}


void TimeChart::SetTimeOffset(const clock::time_point &offset)
{
	Lock lock(_mutex);
	_time_offset = offset;
}



float MeanTimeChart::valueAtRange(float time1, float time2)
{
	Lock lock(_mutex);
	assert(time1 <= time2);
	float sum = 0.0f;
	unsigned values = 0;

	auto itbegin = _timeToValue.lower_bound(time1);
	auto itend = _timeToValue.upper_bound(time2);

	if (itend == _timeToValue.begin())
		return 0.0f;

	for (auto it = itbegin; it != itend; it ++, values++)
	{
		sum += it->second;
	}

	if (itbegin == itend && itbegin == _timeToValue.end())
		return _timeToValue.empty() ? 0.0f : (--_timeToValue.end())->second;

	if (itbegin != _timeToValue.end() && itbegin->first != time1)
		sum += valueAt(time1), values ++;

	if (itend != _timeToValue.begin() && (--itend)->first != time2)
		sum += valueAt(time2), values ++;

	return sum / (float)values;
}
