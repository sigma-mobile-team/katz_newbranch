# Copyright (c) 2012 The WebRTC project authors. All Rights Reserved.
#
# Use of this source code is governed by a BSD-style license
# that can be found in the LICENSE file in the root of the source
# tree. An additional intellectual property rights grant can be found
# in the file PATENTS.  All contributing project authors may
# be found in the AUTHORS file in the root of the source tree.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

include $(LOCAL_PATH)/../../../android-webrtc.mk

LOCAL_ARM_MODE := arm
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_MODULE := libwebrtc_system_wrappers
LOCAL_MODULE_TAGS := optional
LOCAL_CPP_EXTENSION := .cc
LOCAL_SRC_FILES := \
    android/cpu-features.c \
    aligned_malloc.cc \
    atomic32_posix.cc \
    condition_variable.cc \
    cpu_features_android.c \
    cpu_features.cc \
    cpu_info.cc \
    critical_section.cc \
    event.cc \
    event_tracer.cc \
    file_impl.cc \
    list_no_stl.cc \
    map.cc \
    rw_lock.cc \
    sleep.cc \
    sort.cc \
    thread.cc \
    tick_util.cc \
    trace_impl.cc \
    condition_variable_posix.cc \
    critical_section_posix.cc \
    event_posix.cc \
    rw_lock_generic.cc \
    thread_posix.cc \
    trace_posix.cc

#    rw_lock_posix.cc

LOCAL_CFLAGS := \
    $(MY_WEBRTC_COMMON_DEFS)

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/../../.. \
    $(LOCAL_PATH)/../interface \
    $(LOCAL_PATH)/spreadsortlib

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libdl
#    libstlport

include $(BUILD_STATIC_LIBRARY)
