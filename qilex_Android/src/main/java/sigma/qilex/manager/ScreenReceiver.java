
package sigma.qilex.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import pl.katz.aero2.MyApplication;

public class ScreenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            ConnectionsManager.getInstance().setAppPaused(true, false);
            MyApplication.isScreenOn = false;
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            ConnectionsManager.getInstance().setAppPaused(false, true);
            MyApplication.isScreenOn = true;
        }
    }
}
