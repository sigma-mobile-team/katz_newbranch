
package sigma.qilex.manager;

import java.util.HashMap;

import android.content.Context;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;

/**
 * Class processing sound.
 */
public class QilexSoundService {

    private static QilexSoundService mInstance;

    public synchronized static QilexSoundService getInstance() {
        if (mInstance == null) {
            mInstance = new QilexSoundService();
        }
        return mInstance;
    }

    private static final String KEY_CALLING_CONNECTING = "KEY_CALLING_CONNECTING";

    private static final String KEY_CALLING_RINGING_BACK = "KEY_CALLING_RINGING_BACK";

    private static final String KEY_CALLING_FAIL = "KEY_CALLING_FAIL";

    private static final String KEY_CALLING_BUSY = "KEY_CALLING_BUSY";

    private static final String KEY_CALL_INCOMING = "KEY_CALL_INCOMING";

    private static final String KEY_MESSAGE_NOTIFICATION = "KEY_MESSAGE_NOTIFICATION";

    /**
     * Hashmap contains all sound id.
     */
    private HashMap<String, Integer> soundIds = new HashMap<>();

    /**
     * A SoundPoolHelper object that handling call signal tone.
     */
    private SoundPoolHelper mSoundPoolHelperCallSignal;

    /**
     * A SoundPoolHelper object that handling call ringing tone.
     */
    private SoundPoolHelper mSoundPoolHelperCallRinging;

    /**
     * A SoundPoolHelper object that handling notification tone.
     */
    private SoundPoolHelper mSoundPoolHelperNotification;

    private QilexSoundService() {
        Context context = MyApplication.getAppContext();
        mSoundPoolHelperCallSignal = new SoundPoolHelper.Builder(context).setType(SoundPoolHelper.TYPE_DIAL_SIGNAL_TONE)
                .build();
        mSoundPoolHelperCallRinging = new SoundPoolHelper.Builder(context).setUseVibrator(true)
                .setType(SoundPoolHelper.TYPE_RING_TONE).build();
        mSoundPoolHelperNotification = new SoundPoolHelper.Builder(context).setUseVibrator(true)
                .setType(SoundPoolHelper.TYPE_NOTIFICATION).build();

        // Initialize signal id
        int connectingSignal = mSoundPoolHelperCallSignal.load(R.raw.call_connect);
        soundIds.put(KEY_CALLING_CONNECTING, connectingSignal);

        int ringingBackSignal = mSoundPoolHelperCallSignal.load(R.raw.dial_tone);
        soundIds.put(KEY_CALLING_RINGING_BACK, ringingBackSignal);

        int callFailSignal = mSoundPoolHelperCallSignal.load(R.raw.call_fail);
        soundIds.put(KEY_CALLING_FAIL, callFailSignal);

        int callBusySignal = mSoundPoolHelperCallSignal.load(R.raw.call_busy);
        soundIds.put(KEY_CALLING_BUSY, callBusySignal);

        int callIncoming = mSoundPoolHelperCallRinging.load(R.raw.call_incoming);
        soundIds.put(KEY_CALL_INCOMING, callIncoming);

        int callMsgNotification = mSoundPoolHelperNotification.load(R.raw.incoming_bg);
        soundIds.put(KEY_MESSAGE_NOTIFICATION, callMsgNotification);
    }

    /**
     * When caller is connecting to SIP server, this sound will start playing.
     */
    public void startCallConnectingSignal() {
        mSoundPoolHelperCallSignal.play(soundIds.get(KEY_CALLING_CONNECTING), -1);
    }

    /**
     * When callee is ringing, this sound will start playing.
     */
    public void startCallRingingBackSignal() {
        mSoundPoolHelperCallSignal.play(soundIds.get(KEY_CALLING_RINGING_BACK), -1);
    }

    /**
     * When caller is fail, this sound will start playing.
     */
    public void playCallFailSignal() {
        mSoundPoolHelperCallSignal.play(soundIds.get(KEY_CALLING_FAIL));
    }

    /**
     * When callee is busy, this sound will start playing.
     */
    public void playCallBusySignal() {
        mSoundPoolHelperCallSignal.play(soundIds.get(KEY_CALLING_BUSY)); // No
                                                                         // loop
    }

    /**
     * When there is incoming call, this sound will start playing.
     */
    public void startIncomingCallRing() {
        mSoundPoolHelperCallRinging.play(soundIds.get(KEY_CALL_INCOMING), -1);
    }

    /**
     * Call to stop incoming call
     */
    public void stopIncomingCallRing() {
        mSoundPoolHelperCallRinging.stopSound(soundIds.get(KEY_CALL_INCOMING));
    }

    /**
     * When notification, this sound will start playing.
     */
    public void playMessageNotificationTone() {
        mSoundPoolHelperNotification.play(soundIds.get(KEY_MESSAGE_NOTIFICATION));
    }

    /**
     * Stop all Signal include: signal tones, ringing tones.
     */
    public void stopAllSignal() {
        mSoundPoolHelperCallSignal.stopSound(soundIds.get(KEY_CALLING_CONNECTING));
        mSoundPoolHelperCallSignal.stopSound(soundIds.get(KEY_CALLING_RINGING_BACK));
        mSoundPoolHelperCallRinging.stopSound(soundIds.get(KEY_CALL_INCOMING));
    }

    /**
     * Release resources sound.
     */
    public void release() {
        mSoundPoolHelperCallSignal.destroy();
        mSoundPoolHelperCallRinging.destroy();
        mInstance = null;
    }
}
