
package sigma.qilex.manager;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import sigma.qilex.sip.resiprocate.Statemachine;
import sigma.qilex.utils.LogUtils;

public class NetworkStateMonitor extends BroadcastReceiver {

    public interface NetworkStateListener {
        public void onNetworkStateChanged(boolean init, int networkType);
    }

    private static String vrcLastIPAddress;

    /**
     * Cannot define Network.
     */
    public static final int NET_STATE_UNKNOWN = -1;

    /** Constant determining the state of the network - no network */
    public static final int NO_NETWORK = 0;

    /**
     * Try defining the state of the network - it is a cellular network, but not
     * enough for Qilex
     */
    public static final int MOBILE_INSUFFICIENT = 1;

    /**
     * Try defining the state of the network - WIFI so we assume that fast
     * enough for Qilex
     */
    public static final int WIFI = 2;

    /**
     * Try defining the state of the network - mobile network fast enough for
     * Qilex'a (3G, HSDPA, LTE perhaps as far as when switching 3G> LTE does not
     * change the IP)
     */
    public static final int MOBILE_SUFFICIENT = 3;

    public static final String DBG_TAG = "NetState";

    private static Vector<NetworkStateListener> mListeners = new Vector<NetworkStateMonitor.NetworkStateListener>();

    private static TelephonyManager mTelephonymanager;

    private static ConnectivityManager mConnectivityManager;

    private static CellularNetworkStateListener mCellNetworkStateListener;

    private static NetworkStatusUpdateThread mNetworkStatusUpdateThread = null;

    /**
     * A field containing one of the constants: NO_NETWORK,
     * MOBILE_QILEX_INSUFFICIENT, WIFI, MOBILE_QILEX_SUFFICIENT or
     * NET_STATE_UNKNOWN if there was no reading of the network
     */
    private static int lastNetworkState = NET_STATE_UNKNOWN;

    private static int lastCellularNetworkState = -1;

    /**
     * Inicjacja monitora stanu sieci, konieczne do wywołania jesli chcemy
     * otrzymywać callbacki
     * 
     * @param context obiekt Context potrzebny do pobrania ConnectivityManager'a
     *            oraz TelephonyManager'a
     */
    public static void init(Context context) {
        mTelephonymanager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        mConnectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mCellNetworkStateListener = new CellularNetworkStateListener();
        mTelephonymanager.listen(mCellNetworkStateListener, PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);
    }

    /** wyłączenie monitora stanu sieci */
    public static void teardown() {
        mTelephonymanager.listen(mCellNetworkStateListener, PhoneStateListener.LISTEN_NONE);
        mCellNetworkStateListener = null;
        mTelephonymanager = null;
        mConnectivityManager = null;
    }

    /**
     * Metoda odpalająca update stanu sieci na żądanie, spowoduje odpalenie
     * listenerów z aktualnym stanem
     */
    public static void updateNetworkState(boolean init, boolean fireCallbacksOnlyIfStateChanged) {
        if (mConnectivityManager == null || mTelephonymanager == null) {
            return; // niezainicjowano
        }
        if (mNetworkStatusUpdateThread == null) {
            mNetworkStatusUpdateThread = new NetworkStatusUpdateThread();
            if (fireCallbacksOnlyIfStateChanged) {
                mNetworkStatusUpdateThread.updateOnlyIfChanged(init);
            } else {
                mNetworkStatusUpdateThread.update(init);
            }
        }
    }

    public static void addListener(NetworkStateListener listener) {
        synchronized (mListeners) {
            if (mListeners.contains(listener) == false) {
                mListeners.addElement(listener);
            }
        }
    }

    public static void removeListener(NetworkStateListener listener) {
        synchronized (mListeners) {
            mListeners.removeElement(listener);
        }

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        updateNetworkState(false, false);
    }

    /**
     * @return one of the constants: NO_NETWORK, MOBILE_QILEX_INSUFFICIENT,
     *         WIFI, MOBILE_QILEX_SUFFICIENT
     */
    public static int getNetworkState() {
        if (mConnectivityManager == null || mTelephonymanager == null)
            return NET_STATE_UNKNOWN;
        NetworkInfo ni = mConnectivityManager.getActiveNetworkInfo();
        if (ni == null) {
            Statemachine.mContext.setConnection(false, false, false, false);
            return NO_NETWORK;
        }
        if (ni.getType() == ConnectivityManager.TYPE_MOBILE) {
            int nMobileSubtype = mTelephonymanager.getNetworkType();
            switch (nMobileSubtype) {
                case TelephonyManager.NETWORK_TYPE_LTE:
                    Statemachine.mContext.setConnection(false, false, false, true);
                    return MOBILE_SUFFICIENT;
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    Statemachine.mContext.setConnection(false, false, true, false);
                    return MOBILE_SUFFICIENT;

                case TelephonyManager.NETWORK_TYPE_GPRS:
                    Statemachine.mContext.setConnection(false, false, false, false);
                    return MOBILE_INSUFFICIENT;
                default:
                    // by default threat it as 3G
                    Statemachine.mContext.setConnection(false, true, false, false);
                    return MOBILE_SUFFICIENT;
            }
        } else if (ni.getType() == ConnectivityManager.TYPE_WIFI) {
            Statemachine.mContext.setConnection(true, false, false, false);
            return WIFI;
        } else {
            Statemachine.mContext.setConnection(false, false, false, false);
            return NO_NETWORK;
        }
    }

    public static boolean isNetworkExist() {
        return !isNoNetwork(getNetworkState());
    }

    private static void fireOnNetworkStateChanged(boolean init, int networkState) {
        synchronized (mListeners) {
            for (Iterator<NetworkStateListener> i = mListeners.iterator(); i.hasNext();) {
                NetworkStateListener l = i.next();
                try {
                    l.onNetworkStateChanged(init, networkState);
                } catch (Throwable t) {
                    LogUtils.e(DBG_TAG, "onNetworkStateChanged in class: " + l.getClass().getCanonicalName(), t);
                }
            }
        }
    }

    private static class NetworkStatusUpdateThread extends Thread {
        public void update(boolean init) {
            mInitition = init;
            mUpdateOnlyIfChanged = false;
            start();
        }

        public void updateOnlyIfChanged(boolean init) {
            mUpdateOnlyIfChanged = true;
            mInitition = init;
            start();
        }

        private boolean mUpdateOnlyIfChanged = false;

        private boolean mInitition = false;

        public void run() {
            try {
                Thread.sleep(300);
            } catch (Exception e) {
            }
            int newState = getNetworkState();

            String vrlLastIPadderess = getIPAddress(true);

            if (isNoNetwork(newState) && vrlLastIPadderess.length() > 0) {
                vrcLastIPAddress = null;
                mNetworkStatusUpdateThread = null;
                return;
            }

            if (lastNetworkState == newState && mUpdateOnlyIfChanged) {
                if (vrlLastIPadderess != null && vrlLastIPadderess.equalsIgnoreCase(vrcLastIPAddress)) {
                    LogUtils.d(DBG_TAG, "IP same. No update.");
                    mNetworkStatusUpdateThread = null;
                    return;
                }
            }
            // Here also check ip
            if ((vrlLastIPadderess != null) && vrlLastIPadderess.equalsIgnoreCase(vrcLastIPAddress)) {
                updateBinding();
                return;
            }

            if (vrlLastIPadderess.length() <= 0) {
                vrlLastIPadderess = null;
            }
            lastNetworkState = newState;
            vrcLastIPAddress = vrlLastIPadderess;
            fireOnNetworkStateChanged(mInitition, lastNetworkState);
            mNetworkStatusUpdateThread = null;
        }
    }

    public static void updateBinding() {
        Statemachine.mContext.updateBinding();
        mNetworkStatusUpdateThread = null;
    }

    private static boolean isNoNetwork(int state) {
        return state == NO_NETWORK || state == MOBILE_INSUFFICIENT || state == NET_STATE_UNKNOWN;
    }

    private static class CellularNetworkStateListener extends PhoneStateListener {
        @Override
        public void onDataConnectionStateChanged(int state, int networkType) {
            boolean stateChanged = false;
            if (lastCellularNetworkState != state)
                stateChanged = true;
            lastCellularNetworkState = state;
            updateNetworkState(false, !stateChanged);
        }
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (Build.VERSION.SDK_INT >= 9 && !intf.isUp())
                    continue;

                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase(Locale.UK);
                        boolean isIPv4 = isValidIp4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port
                                                                // suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        }
        return "";
    }

    public static boolean isValidIp4Address(final String hostName) {
        try {
            return Inet4Address.getByName(hostName) != null;
        } catch (UnknownHostException ex) {
            return false;
        }
    }
}
