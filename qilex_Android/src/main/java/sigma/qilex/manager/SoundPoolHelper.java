
package sigma.qilex.manager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Vibrator;

public class SoundPoolHelper extends SoundPool implements OnLoadCompleteListener {

    public static final int TYPE_DIAL_SIGNAL_TONE = 1;

    public static final int TYPE_RING_TONE = 2;

    public static final int TYPE_MUSIC = 3;

    public static final int TYPE_NOTIFICATION = 4;

    private Set<Integer> mLoaded;

    private HashMap<Integer, Integer> mStreamIds = new HashMap<>();

    private Context mContext;

    private boolean mUseVibrator;

    float volume;

    Vibrator mVibrator;

    AudioManager mAudioManager;

    private SoundPoolHelper(int maxStreams, boolean useVibrator, Context context) {
        this(maxStreams, AudioManager.STREAM_MUSIC, useVibrator, context);
    }

    @SuppressWarnings("deprecation")
    private SoundPoolHelper(int maxStreams, int streamType, boolean userVibrator, Context context) {
        super(maxStreams, streamType, 0);
        mContext = context;
        mLoaded = new HashSet<Integer>();
        mUseVibrator = userVibrator;
        setOnLoadCompleteListener(this);
        mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
        mVibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);

        // Generate volume
        float actualVolume = (float)mAudioManager.getStreamVolume(streamType);
        float maxVolume = (float)mAudioManager.getStreamMaxVolume(streamType);
        volume = actualVolume / maxVolume;
    }

    public int load(int rawId) {
        return load(mContext, rawId, 1);
    }

    public void play(int soundId) {
        play(soundId, 0);
    }

    /**
     * @param soundId
     * @param repeat -1 is loop forever, 0 is no loop
     */
    public void play(int soundId, int repeat) {
        // Is the sound loaded already?
        if (mLoaded.contains(soundId)) {
            int streamId = play(soundId, volume, volume, 1, repeat, 1f);
            mStreamIds.put(soundId, streamId);
        }
        if (mUseVibrator)
            vibrate(repeat);
    }

    /**
     * @param soundId
     */
    public void stopSound(int soundId) {
        if (mStreamIds.containsKey(soundId) == false) {
            return;
        }
        stop(mStreamIds.remove(soundId));
        if (mUseVibrator && mVibrator != null) {
            mVibrator.cancel();
        }
    }

    private void vibrate(int repeat) {
        int repeatTime = repeat < 0 ? 1000 : repeat + 2;
        long[] pattern = new long[repeatTime];
        pattern[0] = 1;
        for (int i = 1; i < repeatTime; i++) {
            pattern[i] = 1000;
        }
        if (isVibrator())
            mVibrator.vibrate(pattern, -1); // 1s
    }

    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        mLoaded.add(sampleId);
    }

    public void destroy() {
        release();
        mAudioManager = null;
        mVibrator = null;
    }

    private boolean isVibrator() {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
        }
        return (mAudioManager != null && (mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE
                || mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) && mVibrator != null);
    }

    public static class Builder {
        private int mMaxStreams = 1;

        private boolean mUseVibrator = false;

        private int mType = TYPE_MUSIC;

        private Context mContext;

        public Builder(Context context) {
            mContext = context;
        }

        public Builder setMaxStreams(int maxStreams) throws IllegalArgumentException {
            if (maxStreams <= 0) {
                throw new IllegalArgumentException(
                        "Strictly positive value required for the maximum number of streams");
            }
            mMaxStreams = maxStreams;
            return this;
        }

        public Builder setUseVibrator(boolean useVibrator) throws IllegalArgumentException {
            mUseVibrator = useVibrator;
            return this;
        }

        public Builder setType(int type) throws IllegalArgumentException {
            mType = type;
            return this;
        }

        public SoundPoolHelper build() {
            int streamType;
            switch (mType) {
                case TYPE_MUSIC:
                    streamType = AudioManager.STREAM_MUSIC;
                    break;
                case TYPE_DIAL_SIGNAL_TONE:
                    streamType = AudioManager.STREAM_VOICE_CALL;
                    break;
                case TYPE_RING_TONE:
                    streamType = AudioManager.STREAM_RING;
                    break;
                case TYPE_NOTIFICATION:
                    streamType = AudioManager.STREAM_NOTIFICATION;
                    break;
                default:
                    streamType = AudioManager.STREAM_MUSIC;
                    break;
            }
            SoundPoolHelper sph = new SoundPoolHelper(mMaxStreams, streamType, mUseVibrator, mContext);
            return sph;
        }
    }
}
