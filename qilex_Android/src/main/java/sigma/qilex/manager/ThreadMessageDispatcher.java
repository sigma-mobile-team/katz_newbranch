
package sigma.qilex.manager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import sigma.qilex.utils.LogUtils;

/**
 * Created by bartek on 23.07.13.
 */
public class ThreadMessageDispatcher extends Handler {

    private static final String tag = "ThreadMessageDispatcher";

    // ------------ POLA STATYCZNE -------------------

    // ------------ POLA KLASY -----------------------

    /**
     * Listener handlera
     */
    private I_ThreadMessageDispatcher vrcListener;

    // ------------ POLA INTERFACEOW -----------------

    // ------------ KONSTRUKTOR ----------------------

    /**
     * Konstruktor z użyciem loopera
     * 
     * @param vrpLooper looper który odpowiada za akcje dispatchera
     * @param vrpListener listener akcji
     */
    public ThreadMessageDispatcher(Looper vrpLooper, I_ThreadMessageDispatcher vrpListener) {
        super(vrpLooper);
        vrcListener = vrpListener;
    }

    /**
     * Konstruktor zwykły. Aby utworzyć na handlera na wątku należy:
     * 
     * <PRE>
     * {@code vrcThread = new Thread( new Runnable()
     * {
     * 
     * &#64;param vrpListener listener akcji, łapany w wyjątku.
     * 
     * &#64;literal @Override public void run() { Looper.prepare(); vrcDispatcher = new ThreadMessageDispatcher(
     * l_listenerDispatcher ); Looper.loop(); } } ); vrcThread.setName( tag ); //nazwa wątku wyświetlana w debugu
     * vrcThread.setPriority( Thread.MAX_PRIORITY ); //zmiana priorytetu loopera - opcjonalna vrcThread.start();
     * //rozpoczęcie loopera }
     * </PRE>
     * 
     * Uwaga! taka akcja nie gwarantuje natychmiastowego dostępu do dispatchera.
     * Zmienna dispatchera może być nullem
     */
    public ThreadMessageDispatcher(I_ThreadMessageDispatcher vrpListener) {
        super();
        vrcListener = vrpListener;
    }

    // ------------ DESTRUKTOR -----------------------

    // ------------ METODY PUBLICZNE -----------------

    /**
     * Wywołuje zakończenie loopera i znisczenie ThreadMessageDispatchera
     */
    public void quit() {
        vrcListener = null;
        post(new QuitLooper());
    }

    // ------------ METODY PROTECTED -----------------

    // ------------ METODY PRIVATE -------------------

    // ------------ METODY INTERFACEOW ---------------

    @Override
    public void handleMessage(Message msg) {
        LogUtils.d("SIGMA", "PHUCPV test Loop");
        try {
            if (vrcListener != null) {
                vrcListener.onHandleMessage(msg);
            }
        } catch (Throwable e) {
            LogUtils.e(tag, "error in handleMessage", e);
        }
        super.handleMessage(msg);
    }

    // ------------ METODY STATYCZNE -----------------

    /**
     * Pomocniczy runnable do wyłączenia loopera
     */
    class QuitLooper implements Runnable {
        public void run() {
            try {
                Looper.myLooper().quit();
            } catch (Throwable e) {
                LogUtils.e(tag, "error", e);
            }
        }
    }

}
