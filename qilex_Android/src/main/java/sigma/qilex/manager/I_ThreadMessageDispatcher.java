
package sigma.qilex.manager;

import android.os.Message;

/**
 * Created by bartek on 23.07.13.
 */
public interface I_ThreadMessageDispatcher {
    /**
     * wywoływane w ThreadMessageDispatcher w handleMessage
     *
     * @param vrpMsg otrzymana wiadomość
     */
    void onHandleMessage(Message vrpMsg);
}
