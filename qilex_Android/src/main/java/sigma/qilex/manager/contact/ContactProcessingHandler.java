
package sigma.qilex.manager.contact;

import sigma.qilex.dataaccess.model.QilexContact;

public interface ContactProcessingHandler {

    public void onStart();

    public void onFinish(boolean result, QilexContact contact);
}
