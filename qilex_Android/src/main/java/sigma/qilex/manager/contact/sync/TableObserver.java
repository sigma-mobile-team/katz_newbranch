
package sigma.qilex.manager.contact.sync;

import android.annotation.TargetApi;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import sigma.qilex.ui.activity.MainTabActivity;

public class TableObserver extends ContentObserver {
    public TableObserver(Handler handler) {
        super(handler);
    }

    /*
     * Define a method that's called when data in the observed content provider
     * changes. This method signature is provided for compatibility with older
     * platforms.
     */
    @Override
    public void onChange(boolean selfChange) {
        change(selfChange, null);
    }

    /*
     * Define a method that's called when data in the observed content provider
     * changes.
     */
    @TargetApi(16)
    @Override
    public void onChange(boolean selfChange, Uri changeUri) {
        change(selfChange, changeUri);
    }
    
    private void change(boolean selfChange, Uri changeUri) {
        MainTabActivity.requestSyncContactWhenCreate = true;
    }
}
