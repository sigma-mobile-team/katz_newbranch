
package sigma.qilex.manager.contact;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.AggregationExceptions;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.model.http.ContactSnapshotModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.UserInfoResponseModel;
import sigma.qilex.dataaccess.network.http.ContactHttpUtils;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest.QilexHttpRequestListener;
import sigma.qilex.dataaccess.network.http.UserActivationUtils;
import sigma.qilex.dataaccess.sqlitedb.ContactContentProviderHelper;
import sigma.qilex.dataaccess.sqlitedb.ContactTempDatabase;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.GetContactSnapshotListener;
import sigma.qilex.manager.contact.sync.TableObserver;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.adapter.ContactDetailPhoneListAdapter.PhoneItem;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;
import sigma.qilex.utils.Zip.DictBody;

/**
 * @author Phuc
 */
public class ContactManager {
    private static final int NUMBER_CONTACT_SYNC_EACH_BLOCK = 500;

    public static final String ACCOUNT_TYPE_SIM_CONTACT = "vnd.sec.contact.sim";

    private static volatile ContactManager mInstance = null;

    public static ContactManager getInstance() {
        ContactManager lInstance = mInstance;
        if (lInstance == null) {
            synchronized (ContactManager.class) {
                lInstance = mInstance;
                if (lInstance == null) {
                    mInstance = lInstance = new ContactManager();
                }
            }
        }
        return lInstance;
    }

    // *******PHONE BOOK MANAGER START**************
    /**
     * List of all QilexContact, contain Katz and non-Katz contact.
     */
    private Vector<QilexContact> mAllContact = new Vector<QilexContact>();

    /**
     * List of all KATZ contacts.
     */
    private Vector<QilexContact> mFrifonContact = new Vector<QilexContact>();

    /**
     * Hashmap contain mapping of phone number and QilexContact.
     */
    private Hashtable<String, ArrayList<QilexContact>> mPhoneBookNumber = new Hashtable<>();

    /**
     * Hashmap contain mapping of contact id and QilexContact.
     */
    private HashMap<Long, QilexContact> mHashMapAllContact = new HashMap<>();

    /**
     * Hashmap contain mapping of server contact id and QilexContact.
     */
    private HashMap<Long, QilexContact> mHashMapServerId = new HashMap<>();

    // *******PHONE BOOK MANAGER END **************

    private Vector<ContactChangeListener> mContactChangeListener;

    private Vector<NotifyCacheContactChangeListener> mFirstSyncListener;

    private Vector<ContactSyncListener> mContactSyncListener;

    private Object mContactSyncObject = new Object();

    Account mCurrentAccount;

    private boolean isContactLoading = false;

    private TableObserver mContentObserver;

    private String lastContactsVersions = "";

    public HashMap<Integer, QilexContact> contactsMap;

    public Vector<ArrayList<QilexContact>> mPerformingWriteContact = new Vector<>();

    private HashMap<String, TempContactModel> mTempContacts = new HashMap<>();

    private ContactTempDatabase mContactTempDb;

    private boolean isGetContactInfoAsyncRunning = false;

    private QilexContact mSupportContact;

    private ContactManager() {
        mContactChangeListener = new Vector<>();
        mFirstSyncListener = new Vector<>();
        mContactSyncListener = new Vector<>();
        mContactTempDb = ContactTempDatabase.getInstance();
        mTempContacts = mContactTempDb.getTempContactToHashMap(null);
        long lastTimeCheckContactWithServer = SharedPrefrerenceFactory.getInstance().getLastTimeSyncWithSnapshot();
        if (lastTimeCheckContactWithServer == 0) {
            SharedPrefrerenceFactory.getInstance().setCurrentToLastTimeAutoSync();
        }

        mSupportContact = new QilexContact();
        mSupportContact.firstName = Utils.isInKatzMode() ? "Katz" : "Frifon";
    }

    public static String ACCOUNT_TYPE() {
        return Utils.isInKatzMode() ? "eu.sigma.qilex" : "eu.sigma.frifon";
    }
    @SuppressWarnings("deprecation")
    public void addNewAccount() {
        Context context = MyApplication.getAppContext();
        AccountManager am = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accounts = am.getAccountsByType(ACCOUNT_TYPE());
        boolean recreateAccount = false;
        if (UserInfo.getInstance().hasPhoneNumber()) {
            if (accounts.length == 1) {
                Account acc = accounts[0];
                if (!acc.name.equals(UserInfo.getInstance().getPhoneNumberFormatted())) {
                    recreateAccount = true;
                } else {
                    mCurrentAccount = acc;
                }
            } else {
                recreateAccount = true;
            }
        } else {
            if (accounts.length > 0) {
                recreateAccount = true;
            }
        }
        if (recreateAccount) {
            if (Utils.hasLlMr1Above()) {
                for (Account c : accounts) {
                    am.removeAccountExplicitly(c);
                }
            } else {
                for (Account c : accounts) {
                    am.removeAccount(c, null, null);
                }
            }
            if (UserInfo.getInstance().hasPhoneNumber()) {
                try {
                    mCurrentAccount = new Account(UserInfo.getInstance().getPhoneNumberFormatted(), ACCOUNT_TYPE());
                    am.addAccountExplicitly(mCurrentAccount, null, null);
                } catch (Exception e) {
                }
            }
        }
        if (Utils.isInMasterMode()) {
            checkForSyncContact();
        }
    }

    boolean mContactIsWriting = false;

    private void removeSyncDataFromContacts(ArrayList<QilexContact> contactList) {
        try {
            if (contactList.isEmpty()) {
                return;
            }
            ContentResolver contentResolver = MyApplication.getAppContext().getContentResolver();
            ArrayList<Long> ids = new ArrayList<>();
            for (QilexContact qilexContact : contactList) {
                ids.add(qilexContact.id);
            }
            String idsSelection = "(" + TextUtils.join(",", ids) + ")";
            StringBuilder selection = new StringBuilder();
            selection.append(RawContacts.CONTACT_ID + " IN " + idsSelection);
            selection.append(" AND " + RawContacts.SYNC3 + "='" + FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'");
            contentResolver.delete(RawContacts.CONTENT_URI, selection.toString(), null);
        } catch (Exception ex) {
        }
    }

    private void applySnapshotToContact(QilexContact contact, ContactSnapshotModel snapshot) {
        contact.serverContactId = snapshot.id;
        // Apply phone number from snapshot to contact
        if (snapshot.frifon_user == true) {
            int phoneNormalSize = contact.phoneNumberNormal.size();
            int phoneFrifonSize = contact.phoneNumberKATZ.size();
            for (int i = phoneNormalSize - 1; i >= 0; i--) {
                PhoneNumberModel phone = contact.phoneNumberNormal.get(i);
                if (QilexPhoneNumberUtils.checkPhoneIsEqual(phone.phoneNumber, snapshot.frifon_msisdn)) {
                    contact.phoneNumberNormal.remove(i);
                }
            }
            boolean hasSnapshotFrifonNumber = false;
            for (int i = phoneFrifonSize - 1; i >= 0; i--) {
                PhoneNumberModel phone = contact.phoneNumberKATZ.get(i);
                if (QilexPhoneNumberUtils.checkPhoneIsEqual(phone.phoneNumber, snapshot.frifon_msisdn)) {
                    hasSnapshotFrifonNumber = true;
                    break;
                }
            }
            if (hasSnapshotFrifonNumber == false) {
                PhoneNumberModel phone = new PhoneNumberModel(snapshot.frifon_msisdn,
                        MyApplication.getAppContext().getString(R.string.phone_type_mobile));
                contact.phoneNumberKATZ.add(phone);
            }
        } else {
            contact.phoneNumberNormal.addAll(contact.phoneNumberKATZ);
            contact.phoneNumberKATZ.clear();
        }
    }

    private void checkListSnapshotWithCurrentContacts(ArrayList<ContactSnapshotModel> listSnapshot) {
        Hashtable<Long, QilexContact> contactMapping = new Hashtable<>(mHashMapServerId);
        ArrayList<ContactSnapshotModel> listSyncSnapshot = new ArrayList<>();
        ArrayList<QilexContact> listSyncContact = new ArrayList<>();
        for (ContactSnapshotModel contactSnapshotModel : listSnapshot) {
            boolean contactIsUpdated = false;
            QilexContact contact = contactMapping.get(contactSnapshotModel.id);
            if (contact == null) {
                continue;
            }
            if (contactSnapshotModel.frifon_user != contact.isKATZ()) {
                contactIsUpdated = true;
            } else if (contactSnapshotModel.frifon_user == true
                    && contact.hasKATZNumber(contactSnapshotModel.frifon_msisdn) == false) {
                contactIsUpdated = true;
            }
            if (contactIsUpdated == true) {
                listSyncContact.add(contact);
                listSyncSnapshot.add(contactSnapshotModel);
            }
        }
        if (listSyncContact.isEmpty()) {
            return;
        }
        performWriteContactsToPhoneBookInternal2(listSyncContact, listSyncSnapshot);
        fireChangeContactListener(null);
        reloadAllContact();
    }

    public void setPhoneNumberKATZToContacts(final String phoneNo, final String avatarUri, final boolean isKATZ) {
        ArrayList<QilexContact> listContacts = getListContactByPhoneNo(phoneNo);
        if (listContacts == null || listContacts.isEmpty()) {
            return;
        }
        ArrayList<ContactSnapshotModel> listSnapshot = new ArrayList<>();
        ArrayList<QilexContact> listSyncContact = new ArrayList<>();
        for (QilexContact qilexContact : listContacts) {
            if (qilexContact.isEditable == false) {
                continue;
            }
            // Case change from non-KATZ to KATZ
            if (isKATZ) {
                if (qilexContact.hasKATZNumber(phoneNo) == false
                        || (avatarUri != null && !avatarUri.equals(qilexContact.photoUri))) {
                    ContactSnapshotModel model = new ContactSnapshotModel();
                    model.avatar_uri = avatarUri;
                    model.frifon_msisdn = phoneNo;
                    model.frifon_user = true;
                    model.id = qilexContact.serverContactId;
                    model.first_name = qilexContact.firstName;
                    model.last_name = qilexContact.lastName;
                    listSyncContact.add(qilexContact);
                    listSnapshot.add(model);
                }
            }
            // Case change from KATZ to NON-KATZ
            else {
                if (qilexContact.hasKATZNumber(phoneNo) == true) {
                    qilexContact.changeKatzPhoneToNormal(phoneNo);
                    ContactSnapshotModel model = new ContactSnapshotModel();
                    model.avatar_uri = avatarUri == null ? qilexContact.photoUri : avatarUri;
                    model.frifon_user = qilexContact.isKATZ();
                    if (model.frifon_user == true) {
                        model.frifon_msisdn = qilexContact.phoneNumberKATZ.get(0).phoneNumber;
                    }
                    model.id = qilexContact.serverContactId;
                    model.first_name = qilexContact.firstName;
                    model.last_name = qilexContact.lastName;
                    listSyncContact.add(qilexContact);
                    listSnapshot.add(model);
                }
            }
        }
        if (listSyncContact.isEmpty()) {
            return;
        }
        performWriteContactsToPhoneBookInternal2(listSyncContact, listSnapshot);
        fireChangeContactListener(null);
        reloadAllContact();
    }

    private void performWriteContactsToPhoneBookInternal2(ArrayList<QilexContact> contactList,
            ArrayList<ContactSnapshotModel> listContactSnapshot) {
        mContactIsWriting = true;
        String freeCall = MyApplication.getAppContext().getString(R.string.contact_free_call);
        String freeMessage = MyApplication.getAppContext().getString(R.string.contact_free_message);
        String katzOutCall = MyApplication.getAppContext().getString(Utils.isInKatzMode() ? R.string.katz_out_call : R.string.katz_out_call_green);
        try {
            ContentResolver contentResolver = MyApplication.getAppContext().getContentResolver();

            HashMap<Long, Object[]> hashMap_id_contactAndSnapshot = new HashMap<>();
            ArrayList<ContentProviderOperation> query = new ArrayList<>();
            StringBuilder selection = new StringBuilder();

            ArrayList<Long> listContactId = new ArrayList<>();
            ArrayList<Long> listSourceRawId = new ArrayList<>();
            HashMap<Long, Long> hashMap_id_rawId = new HashMap<>();

            // Make list all contact
            int size = listContactSnapshot.size();
            ArrayList<QilexContact> outFrifonContact = new ArrayList<>(mFrifonContact);
            ArrayList<Long> listSimRawId = new ArrayList<>();
            for (int contactIndex = 0; contactIndex < size; contactIndex++) {
                ContactSnapshotModel snapshot = listContactSnapshot.get(contactIndex);
                QilexContact contact = contactList.get(contactIndex);
                applySnapshotToContact(contact, snapshot);

                // Apply to cache contact
                QilexContact cacheContact = mHashMapAllContact.get(contact.id);
                if (cacheContact != null) {
                    boolean isKATZBefore = cacheContact.isKATZ();
                    applySnapshotToContact(cacheContact, snapshot);
                    if (isKATZBefore == false && cacheContact.isKATZ()) {
                        outFrifonContact.add(cacheContact);
                    }
                }

                listContactId.add(contact.id);
                listSourceRawId.add(contact.rawContactId);
                if (contact.originAccountType.contains(ACCOUNT_TYPE_SIM_CONTACT)) {
                    listSimRawId.add(contact.rawContactId);
                }
                Object[] contactAndSnapshot = new Object[2];
                contactAndSnapshot[0] = contact;
                contactAndSnapshot[1] = snapshot;
                hashMap_id_contactAndSnapshot.put(contact.id, contactAndSnapshot);
            }
            Collections.sort(outFrifonContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
            mFrifonContact.clear();
            mFrifonContact.addAll(outFrifonContact);
            notifyCacheContactsChanged();

            String sourceRawidsSelection = "(" + TextUtils.join(",", listSourceRawId) + ")";

            // Check to delete old duplicated raw contact
            query = new ArrayList<>();
            ArrayList<Long> removedRawIds = new ArrayList<>();
            selection = new StringBuilder();
            selection.append(RawContacts.SOURCE_ID + " IN " + sourceRawidsSelection);
            selection.append(" AND " + RawContacts.SYNC3 + "='" + FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'");
            Cursor cur = contentResolver.query(RawContacts.CONTENT_URI, projectionRawContact, selection.toString(),
                    null, null);
            if (cur.moveToFirst()) {
                do {
                    long id = cur.getLong(0);
                    long rawId = cur.getLong(1);
                    long sourceRawId = cur.getLong(2);
                    if (listSimRawId.contains(sourceRawId)) {
                        removedRawIds.add(rawId);
                    } else if (hashMap_id_rawId.containsKey(id)) {
                        removedRawIds.add(rawId);
                    } else {
                        hashMap_id_rawId.put(id, rawId);
                        listContactId.remove(Long.valueOf(id));
                    }
                } while (cur.moveToNext());

                String removedRawIdSelection = "(" + TextUtils.join(",", removedRawIds) + ")";
                ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newDelete(ContactsContract.RawContacts.CONTENT_URI);
                builder.withSelection(RawContacts._ID + " IN " + removedRawIdSelection, null);
                query.add(builder.build());
                try {
                    contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                    LogUtils.d("PHUCPV", "PHUCPV00526 delete finish");
                } catch (Exception e) {
                    LogUtils.d("PHUCPV", "PHUCPV00526 delete fail");
                }
            }
            cur.close();

            // Update sync data and server id to existing contact
            ArrayList<Long> listOldId = new ArrayList<>(hashMap_id_rawId.keySet());
            query = new ArrayList<>();
            selection = new StringBuilder();
            for (Long id : listOldId) {
                Object[] contactAndSnapshot = hashMap_id_contactAndSnapshot.get(id);
                if (contactAndSnapshot == null) {
                    continue;
                }
                QilexContact contact = (QilexContact)contactAndSnapshot[0];
                ContentProviderOperation.Builder builderUpdateRaw = ContentProviderOperation
                        .newUpdate(RawContacts.CONTENT_URI)
                        .withSelection(RawContacts._ID + "=" + hashMap_id_rawId.get(id), null)
                        .withValue(RawContacts.SYNC4, contact.serverContactId)
                        .withValue(ContactsContract.RawContacts.SYNC2, contact.toSyncDataString());
                query.add(builderUpdateRaw.build());

                if (query.size() > 400) {
                    try {
                        contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                        LogUtils.d("PHUCPV", "PHUCPV00526 update sync raw continue");
                    } catch (Exception e) {
                        LogUtils.d("PHUCPV", "PHUCPV00526 update sync raw continue error");
                    }
                    query.clear();
                }
            }
            if (query.isEmpty() == false) {
                try {
                    contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                    query.clear();
                    LogUtils.d("PHUCPV", "PHUCPV00526 update sync raw finish");
                } catch (Exception e) {
                    LogUtils.d("PHUCPV", "PHUCPV00526 update sync raw error");
                }
            }

            // Delete existing frifon number from Data table
            ArrayList<Long> listExistingRawIds = new ArrayList<>(hashMap_id_rawId.values());
            String existingIdsSelection = "(" + TextUtils.join(",", listExistingRawIds) + ")";
            ContentProviderOperation.Builder builderDeleteData = ContentProviderOperation.newDelete(Data.CONTENT_URI);
            builderDeleteData.withSelection(Data.RAW_CONTACT_ID + " IN " + existingIdsSelection, null);
            query.add(builderDeleteData.build());
            try {
                contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                LogUtils.d("PHUCPV", "PHUCPV00526 delete old Data finish");
            } catch (Exception e) {
                LogUtils.d("PHUCPV", "PHUCPV00526 delete old Data error");
            }

            // Create copy for non synced contact
            query = new ArrayList<>();
            int newContactSize = listContactId.size();
            ArrayList<Long> listNewRawId = new ArrayList<>();
            for (int i = 0; i < newContactSize; i++) {
                long id = listContactId.get(i);
                Object[] contactAndSnapshot = hashMap_id_contactAndSnapshot.get(id);
                if (contactAndSnapshot == null) {
                    continue;
                }
                QilexContact contact = (QilexContact)contactAndSnapshot[0];
                ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(ContactsContract.RawContacts.CONTENT_URI);
                builder.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, mCurrentAccount.name);
                builder.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, mCurrentAccount.type);
                builder.withValue(RawContacts.CONTACT_ID, id);
                builder.withValue(RawContacts.SOURCE_ID, contact.rawContactId);
                builder.withValue(ContactsContract.RawContacts.SYNC1, contact.rawContactId);
                builder.withValue(ContactsContract.RawContacts.SYNC2, contact.toSyncDataString());// old
                                                                                                  // values
                builder.withValue(RawContacts.SYNC3, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                builder.withValue(RawContacts.SYNC4, contact.serverContactId);
                query.add(builder.build());

                if (query.size() > 400) {
                    try {
                        ContentProviderResult res[] = contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                        for (ContentProviderResult contentProviderResult : res) {
                            String[] resultSplit = contentProviderResult.uri.toString().split("/");
                            String strRawId = resultSplit[resultSplit.length - 1];
                            listNewRawId.add(Long.parseLong(strRawId));
                        }
                        LogUtils.d("PHUCPV", "PHUCPV00526 create copy continuing");
                    } catch (Exception e) {
                        LogUtils.d("PHUCPV", "PHUCPV00526 create copy continuing fail");
                    }
                    query.clear();
                }
            }
            if (query.isEmpty() == false) {
                try {
                    ContentProviderResult res[] = contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                    for (ContentProviderResult contentProviderResult : res) {
                        String[] resultSplit = contentProviderResult.uri.toString().split("/");
                        String strRawId = resultSplit[resultSplit.length - 1];
                        listNewRawId.add(Long.parseLong(strRawId));
                    }
                    LogUtils.d("PHUCPV", "PHUCPV00526 create copy finish");
                } catch (Exception e) {
                    LogUtils.d("PHUCPV", "PHUCPV00526 create copy fail");
                }
            }

            // Create new raw contact's name and aggregation
            query = new ArrayList<>();
            for (int i = 0; i < newContactSize; i++) {
                long id = listContactId.get(i);
                long rawId = listNewRawId.get(i);
                hashMap_id_rawId.put(id, rawId);
                Object[] contactAndSnapshot = hashMap_id_contactAndSnapshot.get(id);
                if (contactAndSnapshot == null) {
                    continue;
                }
                QilexContact contact = (QilexContact)contactAndSnapshot[0];
                ContactSnapshotModel snapshot = (ContactSnapshotModel)contactAndSnapshot[1];

                hashMap_id_rawId.put(id, rawId);

                // Insert data
                ContentProviderOperation.Builder builder = ContentProviderOperation
                        .newInsert(ContactsContract.Data.CONTENT_URI);
                builder.withValue(StructuredName.RAW_CONTACT_ID, rawId);
                builder.withValue(ContactsContract.Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
                builder.withValue(StructuredName.GIVEN_NAME, contact.firstName);
                builder.withValue(StructuredName.FAMILY_NAME, contact.lastName);
                query.add(builder.build());

                query.add(ContentProviderOperation.newUpdate(ContactsContract.AggregationExceptions.CONTENT_URI)
                        .withValue(AggregationExceptions.TYPE, AggregationExceptions.TYPE_KEEP_TOGETHER)
                        .withValue(AggregationExceptions.RAW_CONTACT_ID1, contact.rawContactId)
                        .withValue(AggregationExceptions.RAW_CONTACT_ID2, rawId).build());

                if (query.size() > 400) {
                    try {
                        contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                        LogUtils.d("PHUCPV", "PHUCPV00526 add copy Name continuing");
                    } catch (Exception e) {
                        LogUtils.d("PHUCPV", "PHUCPV00526 add copy Name continuing fail");
                    }
                    query.clear();
                }

            }
            if (query.isEmpty() == false) {
                try {
                    contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                    LogUtils.d("PHUCPV", "PHUCPV00526 add copy Name finish");
                } catch (Exception e) {
                    LogUtils.d("PHUCPV", "PHUCPV00526 add copy Name fail");
                }
            }

            ArrayList<Object[]> listContactAndSnapshot = new ArrayList<>(hashMap_id_contactAndSnapshot.values());
            // Delete old contact Image
            query = new ArrayList<>();
            ArrayList<Long> listRawNewId = new ArrayList<>();
            for (Object[] contactAndSnapshot : listContactAndSnapshot) {
                QilexContact contact = (QilexContact)contactAndSnapshot[0];
                long rawNewId = hashMap_id_rawId.get(contact.id);
                listRawNewId.add(rawNewId);
            }
            String deletedImageAvaSync = "(" + TextUtils.join(",", listRawNewId) + ")";
            contentResolver.delete(Data.CONTENT_URI, Data.RAW_CONTACT_ID + " IN " + deletedImageAvaSync + " AND "
                    + ContactsContract.Data.MIMETYPE + "='" + MIME_TYPE_SERVER_IMAGE() + "'", null);

            // Add phone number
            query = new ArrayList<>();
            for (Object[] contactAndSnapshot : listContactAndSnapshot) {
                QilexContact contact = (QilexContact)contactAndSnapshot[0];
                ContactSnapshotModel snapshot = (ContactSnapshotModel)contactAndSnapshot[1];
                long rawNewId = hashMap_id_rawId.get(contact.id);
                // Phone No
                // Query phone number
                int phoneNormalSize = contact.phoneNumberNormal.size();
                int phoneFrifonSize = contact.phoneNumberKATZ.size();
                ContentProviderOperation.Builder builder;

                if (!Const.IS_FRIFON_LITE) {
                    for (int i = 0; i < phoneNormalSize; i++) {
                        PhoneNumberModel phoneModel = contact.phoneNumberNormal.get(i);
                        if (QilexPhoneNumberUtils.isSupportNumber(phoneModel.phoneNumber)) {
                            continue;
                        }
                        builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawNewId);
                        builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_OUTCALL_QILEX());
                        builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                        builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                        builder.withValue(ContactsContract.Data.DATA3, katzOutCall + " (" + phoneModel.phoneNumber + ")");
                        builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                        query.add(builder.build());
                    }
                }

                for (int i = 0; i < phoneFrifonSize; i++) {
                    PhoneNumberModel phoneModel = contact.phoneNumberKATZ.get(i);
                    if (QilexPhoneNumberUtils.isSupportNumber(phoneModel.phoneNumber)) {
                        continue;
                    }
                    if (!Const.IS_FRIFON_LITE) {
                        builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawNewId);
                        builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_OUTCALL_QILEX());
                        builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                        builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                        builder.withValue(ContactsContract.Data.DATA3, katzOutCall + " (" + phoneModel.phoneNumber + ")");
                        builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                        query.add(builder.build());
                    }

                    builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawNewId);
                    builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_CALL());
                    builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                    builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                    builder.withValue(ContactsContract.Data.DATA3, freeCall + " (" + phoneModel.phoneNumber + ")");
                    builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                    query.add(builder.build());

                    builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawNewId);
                    builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_MESSAGE());
                    builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                    builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                    builder.withValue(ContactsContract.Data.DATA3, freeMessage + " (" + phoneModel.phoneNumber + ")");
                    builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                    query.add(builder.build());
                }

                builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, rawNewId);
                builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_SERVER_IMAGE());
                builder.withValue(ContactsContract.Data.DATA1, snapshot.avatar_uri);
                query.add(builder.build());

                if (query.size() > 400) {
                    try {
                        contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                        LogUtils.d("PHUCPV", "PHUCPV00526 add number continuing");
                    } catch (Exception e) {
                        LogUtils.d("PHUCPV", "PHUCPV00526 add number continuing fail");
                    }
                    query.clear();
                }
            }
            if (query.isEmpty() == false) {
                try {
                    contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                    LogUtils.d("PHUCPV", "PHUCPV00526 add number finish");
                } catch (Exception e) {
                    LogUtils.d("PHUCPV", "PHUCPV00526 add number error");
                }
                query.clear();
            }
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
        }
        mContactIsWriting = false;
    }

    public long addContactToPhoneBook(QilexContact contact, ContactSnapshotModel snapshot) {
        if (mCurrentAccount == null || contact == null) {
            return -1;
        }
        long res = -1;
        String freeCall = MyApplication.getAppContext().getString(R.string.contact_free_call);
        String freeMessage = MyApplication.getAppContext().getString(R.string.contact_free_message);
        String katzOutCall = MyApplication.getAppContext().getString(Utils.isInKatzMode() ? R.string.katz_out_call : R.string.katz_out_call_green);
        // Apply phone number from snapshot to contact
        if (snapshot.frifon_user == true) {
            int phoneNormalSize = contact.phoneNumberNormal.size();
            int phoneFrifonSize = contact.phoneNumberKATZ.size();
            for (int i = phoneNormalSize - 1; i >= 0; i--) {
                PhoneNumberModel phone = contact.phoneNumberNormal.get(i);
                if (QilexPhoneNumberUtils.checkPhoneIsEqual(phone.phoneNumber, snapshot.frifon_msisdn)) {
                    contact.phoneNumberNormal.remove(i);
                }
            }
            boolean hasSnapshotFrifonNumber = false;
            for (int i = phoneFrifonSize - 1; i >= 0; i--) {
                PhoneNumberModel phone = contact.phoneNumberKATZ.get(i);
                if (QilexPhoneNumberUtils.checkPhoneIsEqual(phone.phoneNumber, snapshot.frifon_msisdn)) {
                    hasSnapshotFrifonNumber = true;
                    break;
                }
            }
            if (hasSnapshotFrifonNumber == false) {
                PhoneNumberModel phone = new PhoneNumberModel(snapshot.frifon_msisdn,
                        MyApplication.getAppContext().getString(R.string.phone_type_mobile));
                contact.phoneNumberKATZ.add(phone);
            }
        } else {
            contact.phoneNumberNormal.addAll(contact.phoneNumberKATZ);
            contact.phoneNumberKATZ.clear();
        }

        // START QUERY
        ContentResolver contentResolver = MyApplication.getAppContext().getContentResolver();
        contact.setServerContactId(snapshot.id);
        ArrayList<ContentProviderOperation> query = new ArrayList<>();

        // Check if contact has copy in Raw Contact table
        StringBuilder selection = new StringBuilder();
        selection.append(RawContacts.SOURCE_ID + "=" + contact.rawContactId);
        selection.append(" AND " + RawContacts.SYNC3 + "='" + FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'");
        Cursor cur = contentResolver.query(RawContacts.CONTENT_URI, projectionRawContact, selection.toString(), null,
                null);

        ContentProviderOperation.Builder builder;
        if (cur.getCount() == 1) {
            cur.moveToFirst();
            res = cur.getLong(1);
        } else if (cur.getCount() > 1) {
            // Delete old copies Frifon number in Data
            // We need this code to fix bug when first synchronizing is
            // processing, user update 1 contact. Then Raw contact copy will be
            // duplicated. So this code is used to remove the duplicated Raw
            // contact
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            cur.moveToFirst();
            ArrayList<Long> removedRawIds = new ArrayList<>();
            do {
                long removeRaw = cur.getLong(1);
                removedRawIds.add(removeRaw);
            } while (cur.moveToNext());
            String ids = TextUtils.join(",", removedRawIds);
            builder = ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI);
            builder.withSelection(RawContacts._ID + " IN(" + ids + ")", null);
            ops.add(builder.build());
            try {
                contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (Exception e) {
                LogUtils.e("KUNLQT", e.toString());
            }
        }
        cur.close();

        // Add new copy in Raw Contact
        query = new ArrayList<>();

        if (res == -1) {
            builder = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI);
            builder.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, mCurrentAccount.name);
            builder.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, mCurrentAccount.type);
            builder.withValue(RawContacts.CONTACT_ID, contact.id);
            builder.withValue(RawContacts.SOURCE_ID, contact.rawContactId);
            builder.withValue(ContactsContract.RawContacts.SYNC1, contact.rawContactId);
            builder.withValue(ContactsContract.RawContacts.SYNC2, contact.toSyncDataString());// old
                                                                                              // values
            builder.withValue(RawContacts.SYNC3, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
            builder.withValue(RawContacts.SYNC4, contact.getServerContactId());
            query.add(builder.build());

            builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            builder.withValueBackReference(ContactsContract.CommonDataKinds.StructuredName.RAW_CONTACT_ID, 0);
            builder.withValue(ContactsContract.Data.MIMETYPE,
                    ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, contact.firstName);
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, contact.lastName);
            query.add(builder.build());
            try {
                ContentProviderResult[] result = contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
                res = Long.parseLong(result[0].uri.getLastPathSegment());
            } catch (Exception e) {
                LogUtils.e("KUNLQT", e.toString());
            }
        } else {
            // Delete old copy Frifon number in Data table
            builder = ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI);
            builder.withSelection(Data.RAW_CONTACT_ID + "=" + res, null);
            query.add(builder.build());

            builder = ContentProviderOperation.newUpdate(RawContacts.CONTENT_URI)
                    .withSelection(RawContacts._ID + "=" + res, null)
                    .withValue(RawContacts.SYNC4, contact.getServerContactId())
                    .withValue(ContactsContract.RawContacts.SYNC2, contact.toSyncDataString());
            query.add(builder.build());
            try {
                contentResolver.applyBatch(ContactsContract.AUTHORITY, query);
            } catch (Exception e) {
                LogUtils.e("KUNLQT", e.toString());
            }
        }

        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            ops.add(ContentProviderOperation.newUpdate(ContactsContract.AggregationExceptions.CONTENT_URI)
                    .withValue(AggregationExceptions.TYPE, AggregationExceptions.TYPE_KEEP_TOGETHER)
                    .withValue(AggregationExceptions.RAW_CONTACT_ID1, contact.getRawContactId())
                    .withValue(AggregationExceptions.RAW_CONTACT_ID2, res).build());

            builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, res);
            builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_SERVER_IMAGE());
            builder.withValue(ContactsContract.Data.DATA1, snapshot.avatar_uri);
            ops.add(builder.build());

            // Query phone number
            int phoneNormalSize = contact.phoneNumberNormal.size();
            int phoneFrifonSize = contact.phoneNumberKATZ.size();

            if (!Const.IS_FRIFON_LITE) {
                for (int i = 0; i < phoneNormalSize; i++) {
                    PhoneNumberModel phoneModel = contact.phoneNumberNormal.get(i);
                    if (QilexPhoneNumberUtils.isSupportNumber(phoneModel.phoneNumber)) {
                        continue;
                    }
                    builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, res);
                    builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_OUTCALL_QILEX());
                    builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                    builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                    builder.withValue(ContactsContract.Data.DATA3, katzOutCall + " (" + phoneModel.phoneNumber + ")");
                    builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                    ops.add(builder.build());
                }
            }

            for (int i = 0; i < phoneFrifonSize; i++) {
                PhoneNumberModel phoneModel = contact.phoneNumberKATZ.get(i);
                if (QilexPhoneNumberUtils.isSupportNumber(phoneModel.phoneNumber)) {
                    continue;
                }
                if (!Const.IS_FRIFON_LITE) {
                    builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, res);
                    builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_OUTCALL_QILEX());
                    builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                    builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                    builder.withValue(ContactsContract.Data.DATA3, katzOutCall + " (" + phoneModel.phoneNumber + ")");
                    builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                    ops.add(builder.build());
                }

                builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, res);
                builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_CALL());
                builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                builder.withValue(ContactsContract.Data.DATA3, freeCall + " (" + phoneModel.phoneNumber + ")");
                builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                ops.add(builder.build());

                builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder.withValue(ContactsContract.Data.RAW_CONTACT_ID, res);
                builder.withValue(ContactsContract.Data.MIMETYPE, MIME_TYPE_FRIFON_MESSAGE());
                builder.withValue(ContactsContract.Data.DATA1, phoneModel.phoneNumber);
                builder.withValue(ContactsContract.Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE());
                builder.withValue(ContactsContract.Data.DATA3, freeMessage + " (" + phoneModel.phoneNumber + ")");
                builder.withValue(ContactsContract.Data.SYNC4, contact.rawContactId);
                ops.add(builder.build());
            }
            contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
        }
        return res;
    }

    public static String MIME_TYPE_FRIFON_CALL(){
        return Utils.isInKatzMode() ?
                "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_number_call"
                :"vnd.android.cursor.item/vnd.eu.sigma.voip.frifon_number_call";// CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE;
    }

    public static String MIME_TYPE_FRIFON_MESSAGE() {
        return Utils.isInKatzMode() ?
                "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_message"
                :"vnd.android.cursor.item/vnd.eu.sigma.voip.frifon_message";
    }

    public static String MIME_TYPE_FRIFON_OUTCALL_QILEX() {
        return Utils.isInKatzMode() ?
                "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_out_call_qilex"
                :"vnd.android.cursor.item/vnd.eu.sigma.voip.frifon_out_call_qilex";
    }

    public static final String MIME_TYPE_FRIFON_OUTCALL_NON_QILEX() {
        return Utils.isInKatzMode() ?
                "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_out_call_non_qilex"
                :"vnd.android.cursor.item/vnd.eu.sigma.voip.frifon_out_call_non_qilex";
    }

    public static final String MIME_TYPE_FRIFON_GOOGLE_VOICEMESSAGE = "vnd.android.cursor.item/vnd.eu.sigma.voip.google_voice_message";

    public static final String MIME_TYPE_SERVER_IMAGE() {
        return Utils.isInKatzMode() ?
                "vnd.android.cursor.item/vnd.eu.sigma.voip.server_avatar"
                :"vnd.android.cursor.item/vnd.eu.sigma.frifon.voip.server_avatar";
    }

    public static final String FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() {
        return Utils.isInKatzMode() ? "Qilex" : "Frifon";
    }

    public void reloadAllContact() {
        // Check if contact if reading or writing, does not reload.
        if (isContactLoading == true || mContactIsWriting == true) {
            return;
        }

        isContactLoading = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                ArrayList<QilexContact> outNormalContact = new ArrayList<>();
                ArrayList<QilexContact> outFrifonContact = new ArrayList<>();
                Hashtable<String, ArrayList<QilexContact>> outPhoneBookNumber = new Hashtable<>();
                HashMap<Long, QilexContact> outHashMapServerId = new HashMap<>();
                mHashMapAllContact = readContactsFromPhoneBook(outNormalContact, outFrifonContact, outPhoneBookNumber,
                        outHashMapServerId);
                if (Utils.isInMasterMode() == false) {
                    HashMap<Long, QilexContact> hashmapSlave = readContactsFromSharePreference(outNormalContact,
                            outFrifonContact, outPhoneBookNumber);
                    mHashMapAllContact.putAll(hashmapSlave);
                }

                synchronized (mPhoneBookNumber) {
                    mPhoneBookNumber.clear();
                    mPhoneBookNumber.putAll(outPhoneBookNumber);
                }
                synchronized (mHashMapServerId) {
                    mHashMapServerId.clear();
                    mHashMapServerId.putAll(outHashMapServerId);
                }
                applyTempContactToDb();
                Collections.sort(outFrifonContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
                Collections.sort(outNormalContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
                mAllContact.clear();
                mAllContact.addAll(outNormalContact);
                mFrifonContact.clear();
                mFrifonContact.addAll(outFrifonContact);
                isContactLoading = false;
                notifyCacheContactsChanged();
            }
        }).start();
    }

    /**
     * Get contact from server and
     */
    public void reloadContactFromServer() {
        if (isContactLoading == true) {
            return;
        }
        isContactLoading = true;
        getAllContactEntry(new GetContactSnapshotListener() {
            @Override
            public void onGetContactSnapshotFinish(ErrorModel errors,
                    ArrayList<ContactSnapshotModel> listContactSnapshot) {
                isContactLoading = false;
                reloadAllContact();
            }
        });
    }

    public boolean isContactLoading() {
        return isContactLoading;
    }

    public QilexContact getCacheContactById(long id) {
        return mHashMapAllContact.get(id);
    }

    public boolean isContactWriting() {
        return mContactIsWriting;
    }

    public QilexContact getContactByIdOrLookupKey(String lookupKey, long id) {
        QilexContact result = null;
        if (mContactIsWriting == true || Utils.isInMasterMode() == false || Utils.isStringNullOrEmpty(lookupKey)) {
            result = getCacheContactById(id);
        } else {
            result = getContactByLookupKey(lookupKey);
        }
        if (result == null) {
            result = new QilexContact();
        }
        return result;
    }

    public QilexContact getContactById(long id) {
        Context context = MyApplication.getAppContext();
        ArrayList<String> listFrifonNumber = null;
        try {
            // Create output contact
            QilexContact contact = new QilexContact();
            contact.id = id;

            // Get frifon phone number
            ContentResolver cr = MyApplication.getAppContext().getContentResolver();
            StringBuilder selection = new StringBuilder();
            selection.append(ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + id);
            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projectionPhones,
                    selection.toString(), null, null);
            if (pCur != null) {
                if (pCur.moveToFirst() == true) {
                    do {
                        String number = pCur.getString(1);
                        long rawContactId = pCur.getLong(4);
                        contact.rawContactId = rawContactId;
                        String lookUpKey = pCur.getString(5);
                        String accountType = pCur.getString(6);
                        contact.lookUpKey = lookUpKey;
                        contact.originAccountType = accountType;

                        LogUtils.d("PHUC", "PRINIT getContactById CONTACT ID = " + id);
                        LogUtils.d("PHUC", "PRINIT getContactById LOOKUP ID = " + lookUpKey);

                        // Get phone frifon
                        if (listFrifonNumber == null) {
                            listFrifonNumber = new ArrayList<>();
                            ArrayList<Long> frifonRawId = ContactContentProviderHelper.getRawIdByNameRaw(context,
                                    rawContactId);
                            if (frifonRawId != null) {
                                String ids = TextUtils.join(",", frifonRawId);
                                StringBuilder whereClause = new StringBuilder();
                                whereClause.append(Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_CALL() + "'");
                                whereClause.append(" AND " + Data.RAW_CONTACT_ID + " IN (" + ids + ")");
                                Cursor phonefrifonCur = cr.query(Data.CONTENT_URI, projectionFrifonPhone,
                                        whereClause.toString(), null, null);
                                if (phonefrifonCur.moveToFirst() == true) {
                                    do {
                                        listFrifonNumber.add(phonefrifonCur.getString(1));
                                    } while (phonefrifonCur.moveToNext());
                                }
                                phonefrifonCur.close();
                            }
                        }

                        if (Utils.isStringNullOrEmpty(number)) {
                            continue;
                        }
                        String originalNumber = number;
                        number = QilexPhoneNumberUtils.convertToBasicNumber(number);
                        String plusNumber = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(number);

                        int type = pCur.getInt(2);
                        String strType = Phone.getTypeLabel(context.getResources(), type,
                                context.getString(R.string.phone_type_other)).toString();
                        contact.plusPhones.add(plusNumber);

                        PhoneNumberModel phoneNumberModel = new PhoneNumberModel(plusNumber, strType);
                        phoneNumberModel.phoneOriginal = originalNumber;
                        if (listFrifonNumber.contains(plusNumber)) {
                            contact.phoneNumberKATZ.add(phoneNumberModel);
                        } else {
                            contact.phoneNumberNormal.add(phoneNumberModel);
                        }
                    } while (pCur.moveToNext());
                }
                pCur.close();
            }

            // Get Image
            selection = new StringBuilder();
            selection.append(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + "=" + id);
            // selection.append(" AND " + Data.MIMETYPE + " = '" +
            // Photo.CONTENT_ITEM_TYPE + "'");
            selection.append(" AND ");
            selection.append(" ( " + Data.MIMETYPE + " = '" + Photo.CONTENT_ITEM_TYPE + "'");
            selection.append(" OR " + Data.MIMETYPE + " = '" + MIME_TYPE_SERVER_IMAGE() + "')");
            pCur = cr.query(ContactsContract.Data.CONTENT_URI, projectionImages, selection.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    String mimeType = pCur.getString(3);
                    if (Photo.CONTENT_ITEM_TYPE.equals(mimeType)) {
                        if (Utils.isStringNullOrEmpty(contact.photoUri)) {
                            String photoUri = pCur.getString(1);
                            String photoUriFull = pCur.getString(2);
                            if (photoUri != null) {
                                while (photoUri.endsWith("photo") || photoUri.endsWith("photo/")) {
                                    if (photoUri.endsWith("photo")) {
                                        photoUri = photoUri.substring(0, photoUri.length() - 5);
                                    } else {
                                        photoUri = photoUri.substring(0, photoUri.length() - 6);
                                    }
                                }
                                if (photoUriFull == null) {
                                    photoUriFull = photoUri;
                                }

                                if (Utils.isStringNullOrEmpty(photoUri) == false) {
                                    contact.photoPhoneUri = photoUri;
                                    contact.photoUri = photoUri;
                                }

                                if (Utils.isStringNullOrEmpty(photoUriFull) == false) {
                                    contact.photoUriFull = photoUriFull;
                                }
                            }
                        }
                    } else if (MIME_TYPE_SERVER_IMAGE().equals(mimeType)) {
                        String photoUri = pCur.getString(4);
                        if (!Utils.isStringNullOrEmpty(photoUri)) {
                            contact.photoUri = photoUri;
                            contact.photoUriFull = photoUri;
                        }
                    }
                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get Name
            selection = new StringBuilder();
            selection.append(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + "=" + id);
            selection.append(" AND " + Data.MIMETYPE + " = '" + StructuredName.CONTENT_ITEM_TYPE + "'");
            pCur = cr.query(ContactsContract.Data.CONTENT_URI, projectionNames, selection.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    String fname = pCur.getString(1);
                    String sname = pCur.getString(2);
                    String sname2 = pCur.getString(3);
                    String mname = pCur.getString(4);

                    if (contact != null && contact.firstName.length() == 0 && contact.lastName.length() == 0) {
                        contact.firstName = fname;
                        contact.lastName = sname;
                        if (contact.firstName == null) {
                            contact.firstName = "";
                        }
                        if (mname != null && mname.length() != 0) {
                            if (contact.firstName.length() != 0) {
                                contact.firstName += " " + mname;
                            } else {
                                contact.firstName = mname;
                            }
                        }
                        if (contact.lastName == null) {
                            contact.lastName = "";
                        }
                        if (contact.lastName.length() == 0 && contact.firstName.length() == 0 && sname2 != null
                                && sname2.length() != 0) {
                            contact.firstName = sname2;
                        }
                    }
                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get Sync Data (SYNC4 = serverId)
            selection = new StringBuilder();
            selection.append(RawContacts.CONTACT_ID + " = " + id);
            selection.append(" AND " + RawContacts.SYNC3 + " = '" + FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'");
            pCur = cr.query(RawContacts.CONTENT_URI, projectionRawContact, selection.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    if (contact != null) {
                        try {
                            long sync4 = pCur.getLong(6);
                            contact.serverContactId = sync4;
                        } catch (Exception e) {
                        }
                    }
                } while (pCur.moveToNext());
            }

            pCur.close();
            return contact;
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
            return null;
        }
    }

    public void getContactByLookupKeyAsync(final String lookUpkey, final GetContactInfoListener listener) {
        isGetContactInfoAsyncRunning = true;
        AsyncTask<String, String, QilexContact> task = new AsyncTask<String, String, QilexContact>() {
            @Override
            protected QilexContact doInBackground(String... params) {
                return getContactByLookupKey(lookUpkey);
            }

            @Override
            protected void onPostExecute(QilexContact result) {
                if (isGetContactInfoAsyncRunning == false) {
                    return;
                }
                isGetContactInfoAsyncRunning = false;
                listener.onGetContactInfoFinish(result);
            }
        };
        task.execute();
    }

    public void stopGetContactInfoAsync() {
        isGetContactInfoAsyncRunning = false;
    }

    public QilexContact getContactByLookupKey(String lookUpkey) {
        try {
            // Create output contact
            QilexContact contact;
            ContentResolver cr = MyApplication.getAppContext().getContentResolver();

            // Lookup uri
            Uri lookupUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookUpkey);
            Uri res = ContactsContract.Contacts.lookupContact(cr, lookupUri);

            // Get contact by id
            String strUri = res.toString();
            int lastSlash = strUri.lastIndexOf("/");
            String contactId = strUri.substring(lastSlash).replaceAll("/", Const.STR_EMPTY);
            contact = getContactById(Long.parseLong(contactId));
            return contact;
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
            return null;
        }
    }

    private HashMap<Long, QilexContact> readContactsFromSharePreference(ArrayList<QilexContact> outNormalContact,
            ArrayList<QilexContact> outFrifonContact, Hashtable<String, ArrayList<QilexContact>> outPhoneBookNumber) {
        HashMap<Long, QilexContact> contactsMap = new HashMap<>();
        ArrayList<String> listInfoData = new ArrayList<>();

        String contactJson = SharedPrefrerenceFactory.getInstance().getContactListJsonCache();
        if (Utils.isStringNullOrEmpty(contactJson)) {
            return contactsMap;
        }

        // Convert to list contact
        SharedPrefrerenceFactory.getInstance().setContactListJsonCache(contactJson);
        JSONArray arrayObject;
        try {
            arrayObject = new JSONArray(contactJson);
            int size = arrayObject.length();
            for (int i = 0; i < size; i++) {
                ContactSnapshotModel snapshot = new ContactSnapshotModel(arrayObject.getJSONObject(i));
                QilexContact contact = new QilexContact();
                contact.firstName = snapshot.first_name;
                contact.lastName = snapshot.last_name;
                contact.serverContactId = snapshot.id;
                contact.id = snapshot.id;
                contact.photoUri = snapshot.avatar_uri;
                contact.photoUriFull = snapshot.avatar_uri;
                contact.isEditable = false;
                ArrayList<PhoneNumberModel> listPhoneNo = snapshot.listPhoneNo;

                // Generate Contact PhoneNumber
                for (PhoneNumberModel phoneNumberModel : listPhoneNo) {
                    ArrayList<QilexContact> listContact = outPhoneBookNumber.get(phoneNumberModel.phoneNumber);
                    if (listContact == null) {
                        listContact = new ArrayList<>();
                    }
                    listContact.add(contact);
                    outPhoneBookNumber.put(phoneNumberModel.phoneNumber, listContact);
                }

                // Check frifon number
                if (snapshot.frifon_user == true) {
                    for (PhoneNumberModel phoneNumberModel : listPhoneNo) {
                        if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber,
                                snapshot.frifon_msisdn) == true) {
                            contact.phoneNumberKATZ.add(phoneNumberModel);
                            listPhoneNo.remove(phoneNumberModel);
                            break;
                        }
                    }
                    outFrifonContact.add(contact);
                }
                outNormalContact.add(contact);
                contact.phoneNumberNormal.addAll(listPhoneNo);
                contactsMap.put(contact.serverContactId, contact);

                // Generate Contact PhoneNumber
                for (PhoneNumberModel phoneNumberModel : listPhoneNo) {
                    ArrayList<QilexContact> listContact = outPhoneBookNumber.get(phoneNumberModel.phoneNumber);
                    if (listContact == null) {
                        listContact = new ArrayList<>();
                    }
                    listContact.add(contact);
                    outPhoneBookNumber.put(phoneNumberModel.phoneNumber, listContact);
                }

                // Check contact is exist
                String infoData = contact.toSyncDataString();
                if (listInfoData.contains(infoData)) {
                    outNormalContact.remove(contact);
                    outFrifonContact.remove(contact);
                } else {
                    listInfoData.add(infoData);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contactsMap;
    }

    private HashMap<Long, QilexContact> readContactsFromPhoneBook(ArrayList<QilexContact> outNormalContact,
            ArrayList<QilexContact> outFrifonContact, Hashtable<String, ArrayList<QilexContact>> outPhoneBookNumber,
            HashMap<Long, QilexContact> outHashMapServerId) {
        Context context = MyApplication.getAppContext();
        HashMap<Long, QilexContact> contactsMap = new HashMap<>();
        HashMap<Long, QilexContact> hasMapContactByRawId = new HashMap<>();
        // ArrayList<String> listFrifonNumber = new ArrayList<>();
        HashMap<String, ArrayList<Long>> mapPhone_sourceId = new HashMap<>();

        // HashMap<Long, Long> hasMap_id_rawId = new HashMap<>();
        try {
            ContentResolver cr = MyApplication.getAppContext().getContentResolver();
            StringBuilder selection;
            // Query frifon number
            // Query to get frifon number
            StringBuilder whereClause = new StringBuilder();
            whereClause.append(Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_CALL() + "'");
            Cursor pCur = cr.query(Data.CONTENT_URI, projectionFrifonPhone, whereClause.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    String phoneNo = pCur.getString(1);
                    Long sourceId = pCur.getLong(2);
                    ArrayList<Long> listSourceId = mapPhone_sourceId.get(phoneNo);
                    if (listSourceId == null) {
                        listSourceId = new ArrayList<>();
                    }
                    listSourceId.add(sourceId);
                    mapPhone_sourceId.put(phoneNo, listSourceId);
                    // listFrifonNumber.add(pCur.getString(1));
                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get contact phone no
            HashMap<String, QilexContact> plusContacts = new HashMap<>();
            ArrayList<Long> idsArr = new ArrayList<>();
            pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projectionPhones, null, null, null);
            if (pCur != null) {
                if (pCur.moveToFirst() == true) {
                    do {
                        String number = pCur.getString(1);
                        long rawContactId = pCur.getLong(4);
                        String lookUpKey = pCur.getString(5);
                        Long id = pCur.getLong(0);
                        String accountType = pCur.getString(6);
                        if (number == null || number.length() == 0) {
                            continue;
                        }
                        number = QilexPhoneNumberUtils.convertToBasicNumber(number);
                        String plusNumber = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(number);

                        if (plusContacts.containsKey(plusNumber)) {
                            continue;
                        }

                        if (!idsArr.contains(id)) {
                            idsArr.add(id);
                        }

                        int type = pCur.getInt(2);
                        QilexContact contact = contactsMap.get(id);
                        if (contact == null) {
                            contact = new QilexContact();
                            contact.firstName = "";
                            contact.lastName = "";
                            contact.photoUri = "";
                            contact.photoUriFull = "";
                            contact.rawContactId = rawContactId;
                            contact.id = id;
                            contact.lookUpKey = lookUpKey;
                            contact.serverContactId = -1;
                            contact.phoneNumberKATZ = new ArrayList<>();
                            contact.phoneNumberNormal = new ArrayList<>();
                            contact.originAccountType = accountType == null ? Const.STR_EMPTY : accountType;
                            contactsMap.put(id, contact);
                            if (outNormalContact.contains(contact) == false) {
                                outNormalContact.add(contact);
                            }
                            hasMapContactByRawId.put(rawContactId, contact);

                            // Make phone book by phonenumber
                            ArrayList<QilexContact> listContact = outPhoneBookNumber.get(plusNumber);
                            if (listContact == null) {
                                listContact = new ArrayList<>();
                            }
                            listContact.add(contact);
                            outPhoneBookNumber.put(plusNumber, listContact);
                        }

                        String strType = Phone.getTypeLabel(context.getResources(), type,
                                context.getString(R.string.phone_type_other)).toString();
                        contact.plusPhones.add(plusNumber);

                        ArrayList<Long> listSourceId = mapPhone_sourceId.get(plusNumber);
                        // if (listFrifonNumber.contains(plusNumber)) {
                        if (listSourceId != null && listSourceId.contains(rawContactId)) {
                            contact.phoneNumberKATZ.add(new PhoneNumberModel(plusNumber, strType));
                            if (outFrifonContact.contains(contact) == false) {
                                outFrifonContact.add(contact);
                            }
                        } else {
                            contact.phoneNumberNormal.add(new PhoneNumberModel(plusNumber, strType));
                        }
                    } while (pCur.moveToNext());
                }
                pCur.close();
            }
            String ids = TextUtils.join(",", idsArr);

            // Get Image
            selection = new StringBuilder();
            selection.append(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " IN (" + ids + ")");
            selection.append(" AND ");
            selection.append(" ( " + Data.MIMETYPE + " = '" + Photo.CONTENT_ITEM_TYPE + "'");
            selection.append(" OR " + Data.MIMETYPE + " = '" + MIME_TYPE_SERVER_IMAGE() + "')");
            pCur = cr.query(ContactsContract.Data.CONTENT_URI, projectionImages, selection.toString(), null, null);
            if (pCur != null && pCur.moveToFirst() == true) {
                do {
                    String mimeType = pCur.getString(3);
                    long id = pCur.getLong(0);
                    QilexContact contact = contactsMap.get(id);
                    if (contact == null) {
                        continue;
                    }
                    if (Photo.CONTENT_ITEM_TYPE.equals(mimeType)) {
                        if (Utils.isStringNullOrEmpty(contact.photoUri)) {
                            String photoUri = pCur.getString(1);
                            String photoUriFull = pCur.getString(2);
                            if (photoUri != null) {
                                while (photoUri.endsWith("photo") || photoUri.endsWith("photo/")) {
                                    if (photoUri.endsWith("photo")) {
                                        photoUri = photoUri.substring(0, photoUri.length() - 5);
                                    } else {
                                        photoUri = photoUri.substring(0, photoUri.length() - 6);
                                    }
                                }
                                if (photoUriFull == null) {
                                    photoUriFull = photoUri;
                                }

                                if (Utils.isStringNullOrEmpty(photoUri) == false) {
                                    contact.photoUri = photoUri;
                                    contact.photoPhoneUri = photoUri;
                                }

                                if (Utils.isStringNullOrEmpty(photoUriFull) == false) {
                                    contact.photoUriFull = photoUriFull;
                                }
                            }
                        }
                    } else if (MIME_TYPE_SERVER_IMAGE().equals(mimeType)) {
                        String photoUri = pCur.getString(4);
                        if (!Utils.isStringNullOrEmpty(photoUri)) {
                            contact.photoUri = photoUri;
                            contact.photoUriFull = photoUri;
                        }
                    }

                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get Names
            pCur = cr.query(ContactsContract.Data.CONTENT_URI, projectionNames,
                    ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " IN (" + ids + ") AND "
                            + ContactsContract.Data.MIMETYPE + " = '"
                            + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
                    null, null);
            if (pCur != null && pCur.moveToFirst() == true) {
                do {
                    long id = pCur.getLong(0);
                    String fname = pCur.getString(1);
                    String sname = pCur.getString(2);
                    String sname2 = pCur.getString(3);
                    String mname = pCur.getString(4);
                    long rawId = pCur.getLong(5);

                    QilexContact contact = contactsMap.get(id);
                    if (contact != null && contact.firstName.length() == 0 && contact.lastName.length() == 0) {
                        contact.firstName = fname;
                        contact.lastName = sname;
                        if (contact.firstName == null) {
                            contact.firstName = "";
                        }
                        if (mname != null && mname.length() != 0) {
                            if (contact.firstName.length() != 0) {
                                contact.firstName += " " + mname;
                            } else {
                                contact.firstName = mname;
                            }
                        }
                        if (contact.lastName == null) {
                            contact.lastName = "";
                        }
                        if (contact.lastName.length() == 0 && contact.firstName.length() == 0 && sname2 != null
                                && sname2.length() != 0) {
                            contact.firstName = sname2;
                        }
                    }

                    if (contact != null && contact.rawContactId == -1) {
                        contact.rawContactId = rawId;
                    }
                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get Sync Data (SYNC4 = serverId)
            // SYNC2 = data check sync
            selection = new StringBuilder();
            selection.append(RawContacts.CONTACT_ID + " IN(" + ids + ")");
            selection.append(" AND " + RawContacts.SYNC3 + " = '" + FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'");
            pCur = cr.query(RawContacts.CONTENT_URI, projectionRawContact, selection.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    long id = pCur.getLong(0);

                    QilexContact contact = contactsMap.get(id);
                    if (contact != null) {
                        try {
                            long sync4 = pCur.getLong(6);
                            contact.serverContactId = sync4;
                            outHashMapServerId.put(sync4, contact);
                        } catch (Exception e) {
                        }
                    }
                } while (pCur.moveToNext());
            }

            pCur.close();
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
            contactsMap.clear();
        }
        return contactsMap;
    }

    /**
     * Get un-sync contacts from Contacts database with maximum is
     * NUMBER_CONTACT_SYNC_EACH_BLOCK
     * 
     * @param outNewContact list of new contacts will output here
     * @param outUpdateContact list of updated contact will output here
     * @return true if all contact in db is exist in output lists.
     */
    private boolean readNotSyncContactsFromPhoneBook(ArrayList<QilexContact> outNewContact,
            ArrayList<QilexContact> outUpdateContact) {
        ArrayList<QilexContact> outNewContactTmp = new ArrayList<>();
        ArrayList<QilexContact> outUpdateContactTmp = new ArrayList<>();

        boolean isAll = true;
        Context context = MyApplication.getAppContext();
        HashMap<Long, QilexContact> contactsMap = new HashMap<>();
        HashMap<Long, QilexContact> contactsMapByRawId = new HashMap<>();
        ArrayList<String> listFrifonNumber = new ArrayList<>();

        HashMap<Long, Long> hasMap_id_rawId = new HashMap<>();
        ArrayList<Long> removedRawId = new ArrayList<>();
        try {
            ContentResolver cr = MyApplication.getAppContext().getContentResolver();

            // Query frifon number
            // Query to get frifon number
            StringBuilder whereClause = new StringBuilder();
            whereClause.append(Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_CALL() + "'");
            Cursor pCur = cr.query(Data.CONTENT_URI, projectionFrifonPhone, whereClause.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    listFrifonNumber.add(pCur.getString(1));
                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get contact phone no
            HashMap<String, QilexContact> plusContacts = new HashMap<>();
            ArrayList<Long> idsArr = new ArrayList<>();
            ArrayList<Long> rawidsSourceIds = new ArrayList<>();
            StringBuilder selection = new StringBuilder();
            // selection.append(RawContacts.ACCOUNT_TYPE + " NOT LIKE '" +
            // ACCOUNT_TYPE_SIM_CONTACT + "%'");
            pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projectionPhones, selection.toString(),
                    null, null);
            if (pCur != null) {
                if (pCur.moveToFirst() == true) {
                    do {
                        String number = pCur.getString(1);
                        long rawContactId = pCur.getLong(4);
                        Long id = pCur.getLong(0);
                        String accountType = pCur.getString(6);

                        if (hasMap_id_rawId.containsKey(id)) {
                            if (hasMap_id_rawId.get(id) != rawContactId) {
                                removedRawId.add(rawContactId);
                                continue;
                            }
                        } else {
                            hasMap_id_rawId.put(id, rawContactId);
                        }
                        if (number == null || number.length() == 0) {
                            continue;
                        }
                        number = QilexPhoneNumberUtils.convertToBasicNumber(number);
                        String plusNumber = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(number);

                        if (plusContacts.containsKey(plusNumber)) {
                            continue;
                        }

                        if (!idsArr.contains(id)) {
                            idsArr.add(id);
                        }

                        if (!rawidsSourceIds.contains(rawContactId)) {
                            rawidsSourceIds.add(rawContactId);
                        }

                        int type = pCur.getInt(2);
                        QilexContact contact = contactsMap.get(id);
                        if (contact == null) {
                            contact = new QilexContact();
                            contact.rawContactId = rawContactId;
                            contact.id = id;
                            contact.originAccountType = accountType;
                            contactsMap.put(id, contact);
                            contactsMapByRawId.put(rawContactId, contact);
                            outNewContactTmp.add(contact);
                        }

                        String strType = Phone.getTypeLabel(context.getResources(), type,
                                context.getString(R.string.phone_type_other)).toString();
                        contact.plusPhones.add(plusNumber);

                        if (listFrifonNumber.contains(plusNumber)) {
                            contact.phoneNumberKATZ.add(new PhoneNumberModel(plusNumber, strType));
                        } else {
                            contact.phoneNumberNormal.add(new PhoneNumberModel(plusNumber, strType));
                        }
                    } while (pCur.moveToNext());
                }
                pCur.close();
            }
            String ids = TextUtils.join(",", idsArr);
            String rawSourceIds = TextUtils.join(",", rawidsSourceIds);

            // Delete duplicated raw contact id
            String duplicatedRaw = "(" + TextUtils.join(",", removedRawId) + ")";
            LogUtils.d("PHUCPHUC", "DELETE DUPLICATED RAW " + duplicatedRaw);
            MyApplication.getAppContext().getContentResolver().delete(RawContacts.CONTENT_URI,
                    RawContacts._ID + " IN " + duplicatedRaw, null);

            // Get Names
            pCur = cr.query(ContactsContract.Data.CONTENT_URI, projectionNames,
                    ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " IN (" + ids + ") AND "
                            + ContactsContract.Data.MIMETYPE + " = '"
                            + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
                    null, null);
            if (pCur != null && pCur.moveToFirst() == true) {
                do {
                    long id = pCur.getLong(0);
                    String fname = pCur.getString(1);
                    String sname = pCur.getString(2);
                    String sname2 = pCur.getString(3);
                    String mname = pCur.getString(4);
                    long rawId = pCur.getLong(5);

                    QilexContact contact = contactsMap.get(id);
                    if (contact != null && contact.firstName.length() == 0 && contact.lastName.length() == 0) {
                        contact.firstName = fname;
                        contact.lastName = sname;
                        if (contact.firstName == null) {
                            contact.firstName = "";
                        }
                        if (mname != null && mname.length() != 0) {
                            if (contact.firstName.length() != 0) {
                                contact.firstName += " " + mname;
                            } else {
                                contact.firstName = mname;
                            }
                        }
                        if (contact.lastName == null) {
                            contact.lastName = "";
                        }
                        if (contact.lastName.length() == 0 && contact.firstName.length() == 0 && sname2 != null
                                && sname2.length() != 0) {
                            contact.firstName = sname2;
                        }
                    }

                    if (contact != null && contact.rawContactId == -1) {
                        contact.rawContactId = rawId;
                    }
                } while (pCur.moveToNext());
            }
            pCur.close();

            // Get Sync Data (SYNC4 = serverId)
            // SYNC2 = data check sync
            selection = new StringBuilder();
            selection.append(RawContacts.SOURCE_ID + " IN(" + rawSourceIds + ")");
            selection.append(" AND " + RawContacts.SYNC3 + " = '" + FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'");
            selection.append(" AND " + RawContacts.CONTACT_ID + " > 0");
            pCur = cr.query(RawContacts.CONTENT_URI, projectionRawContact, selection.toString(), null, null);
            if (pCur.moveToFirst() == true) {
                do {
                    long sourceId = pCur.getLong(2);
                    QilexContact contact = contactsMapByRawId.get(sourceId);
                    if (contact != null) {
                        try {
                            long sync4 = pCur.getLong(6);
                            String oldSyncData = pCur.getString(4);
                            String currentData = contact.toSyncDataString();
                            contact.serverContactId = sync4;
                            contact.latestSyncData = oldSyncData;
                            if (contact.serverContactId > 0) {
                                outNewContactTmp.remove(contact);
                                if (contact.latestSyncData.equals(currentData) == false) {
                                    outUpdateContactTmp.add(contact);
                                }
                            }
                        } catch (Exception e) {
                        }
                    }
                } while (pCur.moveToNext());
            }

            pCur.close();
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
            contactsMap.clear();
        }

        // Get 50 contact only
        if (outNewContactTmp.size() > NUMBER_CONTACT_SYNC_EACH_BLOCK) {
            outNewContact.addAll(outNewContactTmp.subList(0, NUMBER_CONTACT_SYNC_EACH_BLOCK));
            isAll = false;
        } else {
            outNewContact.addAll(outNewContactTmp);
        }
        if (outUpdateContactTmp.size() > NUMBER_CONTACT_SYNC_EACH_BLOCK) {
            outUpdateContact.addAll(outUpdateContactTmp.subList(0, NUMBER_CONTACT_SYNC_EACH_BLOCK));
            isAll = false;
        } else {
            outUpdateContact.addAll(outUpdateContactTmp);
        }
        return isAll;
    }

    private boolean isContactPosting = false;

    private boolean isContactRefreshing = false;

    private boolean isContactGetting = false;

    public boolean isContactSynchronizing() {
        return isContactPosting || isContactRefreshing;
    }

    public boolean isContactGetting() {
        return isContactGetting;
    }

    public void checkForSyncContact() {
        if (UserInfo.getInstance().isActive() == false) {
            LogUtils.d("SIGMA-QILEX", "sync contact: user is not active");
            return;
        }
        if (mCurrentAccount == null) {
            LogUtils.d("SIGMA-QILEX", "sync contact: no Account");
            return;
        }
        if (NetworkStateMonitor.getNetworkState() == NetworkStateMonitor.NO_NETWORK) {
            LogUtils.d("SIGMA-QILEX", "sync contact: not network, do not sync");
            return;
        }
        if (isContactSynchronizing() || mPerformingWriteContact.isEmpty() == false) {
            LogUtils.d("SIGMA-QILEX", "sync contact: there is other thread synchronizing");
            return;
        }
        // Check for re-sync with list contact snapshot
        if (Calendar.getInstance().getTimeInMillis() - SharedPrefrerenceFactory.getInstance()
                .getLastTimeSyncWithSnapshot() > Const.Config.TIME_MILLIS_DURATION_RECHECK_CONTACT) {
            isContactPosting = true;
            isContactRefreshing = true;
            isContactGetting = true;
            getAllContactEntry(new GetContactSnapshotListener() {
                @Override
                public void onGetContactSnapshotFinish(ErrorModel errors,
                        final ArrayList<ContactSnapshotModel> listContactSnapshot) {
                    Utils.globalQueue.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (listContactSnapshot != null && listContactSnapshot.isEmpty() == false) {
                                    checkListSnapshotWithCurrentContacts(listContactSnapshot);
                                    fireContactSyncFinish(true);
                                }
                            } catch (Exception ex) {
                            }
                            SharedPrefrerenceFactory.getInstance().setCurrentToLastTimeAutoSync();
                            isContactPosting = false;
                            isContactRefreshing = false;
                            isContactGetting = false;
                        }
                    });
                }
            });
            return;
        }
        // Check for new/updated contacts
        Utils.globalQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    final ArrayList<QilexContact> outNewContact = new ArrayList<>();
                    final ArrayList<QilexContact> outUpdateContact = new ArrayList<>();
                    final boolean isRequestAllContact = readNotSyncContactsFromPhoneBook(outNewContact,
                            outUpdateContact);

                    if (isContactSynchronizing()) {
                        LogUtils.d("SIGMA-QILEX", "sync contact: there is other thread synchronizing");
                        return;
                    }
                    // Post all new contact to server
                    if (outNewContact.isEmpty() == false) {
                        isContactPosting = true;
                        LogUtils.d("SIGMA-QILEX", "SYNC contact: there is " + outNewContact.size() + " new contact to sync"
                                + outNewContact.toString());
                        postAllContactListSnapshot(MyApplication.getAppContext(), outNewContact,
                                new GetContactSnapshotListener() {
                            @Override
                            public void onGetContactSnapshotFinish(final ErrorModel errors,
                                    final ArrayList<ContactSnapshotModel> listContactSnapshot) {
                                Utils.contactQueue.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        LogUtils.d("SYNC CONTACT", "SYNC CONTACT:(add) GET DATA FROM SERVER FINISH");
                                        if (listContactSnapshot == null) {
                                            isContactPosting = false;
                                            if (isContactSynchronizing() == false) {
                                                fireContactSyncFinish(false);
                                            }
                                            LogUtils.d("SYNC CONTACT", "SYNC CONTACT:(add) GET DATA FROM SERVER ERROR");
                                            return;
                                        }

                                        mPerformingWriteContact.add(outNewContact);
                                        performWriteContactsToPhoneBookInternal2(outNewContact, listContactSnapshot);
                                        mPerformingWriteContact.remove(outNewContact);
                                        reloadAllContact();
                                        LogUtils.d("SYNC CONTACT", "SYNC CONTACT:(add) ADDED CONTACT TO PHONEBOOK");

                                        // Set posting contact to false
                                        // Check if there is refreshing
                                        // process
                                        // is not run, fire sync finish
                                        isContactPosting = false;
                                        if (isContactSynchronizing() == false) {
                                            if (isRequestAllContact == true) {
                                                // If finish sync,
                                                // notify to GUI
                                                fireContactSyncFinish(errors == null);
                                            } else {
                                                // If contact is not
                                                // all, do
                                                // next block
                                                checkForSyncContact();
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        LogUtils.d("SIGMA-QILEX", "sync contact: there is no new contact to sync");
                        isContactPosting = false;
                    }

                    // Refresh contact to server
                    if (outUpdateContact.isEmpty() == false) {
                        isContactRefreshing = true;
                        LogUtils.d("SIGMA-QILEX", "SYNC contact: there is " + outUpdateContact.size()
                                + " updated contact to sync" + outUpdateContact.toString());
                        refreshAllContactListSnapshot(MyApplication.getAppContext(), outUpdateContact,
                                new GetContactSnapshotListener() {
                            @Override
                            public void onGetContactSnapshotFinish(final ErrorModel errors,
                                    final ArrayList<ContactSnapshotModel> listContactSnapshot) {
                                Utils.contactQueue.postRunnable(new Runnable() {
                                    @Override
                                    public void run() {
                                        LogUtils.d("SYNC CONTACT", "SYNC CONTACT:(update) GET DATA FROM SERVER FINISH");
                                        if (listContactSnapshot == null) {
                                            // If error is cannot get
                                            // server ID
                                            if (errors.getByErrorCode(131) != null) {
                                                removeSyncDataFromContacts(outUpdateContact);
                                                isContactRefreshing = false;
                                                if (isContactSynchronizing() == false) {
                                                    if (isRequestAllContact == true) {
                                                        // If finish
                                                        // sync,
                                                        // notify to GUI
                                                        fireContactSyncFinish(errors == null);
                                                    } else {
                                                        // If contact is
                                                        // not
                                                        // all, do
                                                        // next block
                                                        checkForSyncContact();
                                                    }
                                                }
                                            } else {
                                                isContactRefreshing = false;
                                                if (isContactSynchronizing() == false) {
                                                    fireContactSyncFinish(false);
                                                }
                                            }
                                            LogUtils.d("SYNC CONTACT", "SYNC CONTACT:(update) GET DATA FROM SERVER ERROR");
                                            return;
                                        }

                                        mPerformingWriteContact.add(outUpdateContact);
                                        performWriteContactsToPhoneBookInternal2(outUpdateContact, listContactSnapshot);
                                        mPerformingWriteContact.remove(outUpdateContact);
                                        reloadAllContact();
                                        LogUtils.d("SYNC CONTACT", "SYNC CONTACT:(update) ADDED CONTACT TO PHONEBOOK");

                                        // Set refreshing to false
                                        // Check if there is posting
                                        // process is
                                        // not run, fire sync finish
                                        isContactRefreshing = false;
                                        // If contact finish sync
                                        if (isContactSynchronizing() == false) {
                                            if (isRequestAllContact == true) {
                                                // If finish sync,
                                                // notify to GUI
                                                fireContactSyncFinish(errors == null);
                                            } else {
                                                // If contact is not
                                                // all, do
                                                // next block
                                                checkForSyncContact();
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        LogUtils.d("SIGMA-QILEX", "sync contact: there is no updated contact to sync");
                        isContactRefreshing = false;
                    }

                    if (isContactSynchronizing()) {
                        fireContactSyncStart(isContactRefreshing, isContactPosting);
                    } else {
                        fireContactSyncFinish(true);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static final String[] projectionFrifonPhone = {
            Data.RAW_CONTACT_ID, // 0
            ContactsContract.CommonDataKinds.Phone.NUMBER, // 1
            Data.SYNC4
            // 2
    };

    private static final String[] projectionPhones = {
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID, // 0
            ContactsContract.CommonDataKinds.Phone.NUMBER, // 1
            ContactsContract.CommonDataKinds.Phone.TYPE, // 2
            ContactsContract.CommonDataKinds.Phone.LABEL, // 3
            ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID, // 4
            ContactsContract.Contacts.LOOKUP_KEY, // 5
            ContactsContract.RawContacts.ACCOUNT_TYPE
            // 6
    };

    private static final String[] projectionNames = {
            ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
            ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, ContactsContract.Data.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME, ContactsContract.Data.RAW_CONTACT_ID
    };

    private static final String[] projectionImages = {
            ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
            ContactsContract.CommonDataKinds.Photo.PHOTO_THUMBNAIL_URI,
            ContactsContract.CommonDataKinds.Photo.PHOTO_URI, ContactsContract.CommonDataKinds.Photo.MIMETYPE,
            ContactsContract.CommonDataKinds.Photo.DATA1
    };

    private static final String[] projectionRawContact = {
            RawContacts.CONTACT_ID, // 0
            RawContacts._ID, // 1
            RawContacts.SOURCE_ID, // 2
            RawContacts.SYNC1, // 3
            RawContacts.SYNC2, // 4
            RawContacts.SYNC3, // 5
            RawContacts.SYNC4, // 6
            RawContacts.ACCOUNT_TYPE
            // 7
            // 6
    };

    public HashMap<Long, QilexContact> getContactsCopy(HashMap<Long, QilexContact> original) {
        HashMap<Long, QilexContact> ret = new HashMap<>();
        for (HashMap.Entry<Long, QilexContact> entry : original.entrySet()) {
            QilexContact copyContact = new QilexContact();
            QilexContact originalContact = entry.getValue();
            copyContact.phoneTypes.addAll(originalContact.phoneTypes);
            copyContact.plusPhones.addAll(originalContact.plusPhones);
            copyContact.firstName = originalContact.firstName;
            copyContact.lastName = originalContact.lastName;
            copyContact.id = originalContact.id;
            ret.put(copyContact.id, copyContact);
        }
        return ret;
    }

    private boolean checkContactsInDeviceChange() {
        boolean reload = false;
        try {
            ContentResolver cr = MyApplication.getAppContext().getContentResolver();
            Cursor pCur = null;
            try {
                pCur = cr.query(ContactsContract.RawContacts.CONTENT_URI, new String[] {
                        ContactsContract.RawContacts.VERSION
                }, null, null, null);
                StringBuilder currentVersion = new StringBuilder();

                while (pCur.moveToNext()) {
                    int col = pCur.getColumnIndex(ContactsContract.RawContacts.VERSION);
                    currentVersion.append(pCur.getString(col));
                }
                String newContactsVersion = currentVersion.toString();
                LogUtils.d("KUNLQT", "newContactsVersion: \n" + newContactsVersion);
                if (lastContactsVersions.length() != 0 && !lastContactsVersions.equals(newContactsVersion)) {
                    reload = true;
                }
                lastContactsVersions = newContactsVersion;
                LogUtils.d("KUNLQT", "lastContactsVersions: \n" + lastContactsVersions);
            } catch (Exception e) {
                LogUtils.e("KUNLQT", e.toString());
            } finally {
                if (pCur != null) {
                    pCur.close();
                }
            }
        } catch (Exception e) {
            LogUtils.e("KUNLQT", e.toString());
        }
        return reload;
    }

    /**
     * Get contact info (Name. image url thumbnail, image url full) by phone
     * number
     * 
     * @param phoneNumber phone number
     * @return String[0] = name, String[1] = thumbnail image uri, String[2] =
     *         full image url
     */
    public String[] getContactNameAndImageByPhoneNo(String phoneNumber) {
        if (QilexPhoneNumberUtils.isSupportNumber(phoneNumber)) {
            String[] stringData = new String[3];
            stringData[0] = mSupportContact.getDisplayName();
            stringData[1] = mSupportContact.photoUri;
            stringData[2] = mSupportContact.photoUri;
            return stringData;
        }
        String name = null;
        String image = null;
        String imageFull = null;
        String phone = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNumber);
        QilexContact contact = getContactByPhoneNo(MyApplication.getAppContext(), phone);
        if (contact != null) {
            name = contact.getDisplayName();
            image = contact.photoUri;
            imageFull = contact.photoUriFull;
        } else {
            TempContactModel tempContact = getTempContact(phone);
            if (tempContact != null) {
                name = tempContact.name;
                image = tempContact.avatarUrl;
                imageFull = tempContact.avatarUrl;
            }
        }
        if (name != null) {
            String[] stringData = new String[3];
            stringData[0] = name;
            stringData[1] = image;
            stringData[2] = imageFull;
            return stringData;
        } else {
            return null;
        }
    }

    /**
     * Get Qilex contact by phone number from synchronized contact list.
     * 
     * @param context Context
     * @param phoneNumber
     * @return the first QilexContact have phoneNumber like param.
     */
    public QilexContact getContactByPhoneNo(Context context, String phoneNumber) {
        if (QilexPhoneNumberUtils.isSupportNumber(phoneNumber)) {
            return mSupportContact;
        }
        String phone = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNumber);
        ArrayList<QilexContact> listContact = mPhoneBookNumber.get(phone);
        if (listContact == null || listContact.isEmpty()) {
            return null;
        } else {
            return listContact.get(0);
        }
    }

    public ArrayList<QilexContact> getListContactByPhoneNo(String phoneNumber) {
        if (QilexPhoneNumberUtils.isSupportNumber(phoneNumber)) {
            ArrayList<QilexContact> listContact = new ArrayList<>();
            listContact.add(mSupportContact);
            return listContact;
        }
        ArrayList<QilexContact> listContact = mPhoneBookNumber.get(phoneNumber);
        if (listContact != null && listContact.isEmpty() == false) {
            return new ArrayList<QilexContact>(listContact);
        } else {
            return new ArrayList<QilexContact>();
        }
    }

    public QilexContact getContactByPhoneNoFromPhoneBook(Context context, String phoneNumber) {
        if (QilexPhoneNumberUtils.isSupportNumber(phoneNumber)) {
            return mSupportContact;
        }

        QilexContact searchContact = null;

        if (mContactIsWriting == true) {
            searchContact = getContactByPhoneNo(context, phoneNumber);
            return searchContact;
        }

        // Search by origin text
        String phone = phoneNumber;
        long contactId = ContactContentProviderHelper.getContactByPhoneNo2(context, phone);
        searchContact = contactId <= 0 ? null : getContactById(contactId);
        if (searchContact != null) {
            return searchContact;
        }

        if (phoneNumber.charAt(0) != '+') {
            // Add current phone code to phone
            phone = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNumber);
            contactId = ContactContentProviderHelper.getContactByPhoneNo2(context, phone);
            searchContact = contactId <= 0 ? null : getContactById(contactId);
            if (searchContact != null) {
                return searchContact;
            }
        } else {
            // Search by phone without country code
            phone = QilexPhoneNumberUtils.removePhoneCode(phoneNumber)[1];
            if (Utils.isStringNullOrEmpty(phone)) {
                return null;
            }
            contactId = ContactContentProviderHelper.getContactByPhoneNo2(context, phone);
            searchContact = contactId <= 0 ? null : getContactById(contactId);
            if (searchContact != null) {
                return searchContact;
            }
            // Search by phone without country code and add "0" at first
            phone = "0" + phone;
            contactId = ContactContentProviderHelper.getContactByPhoneNo2(context, phone);
            searchContact = contactId <= 0 ? null : getContactById(contactId);
            if (searchContact != null) {
                return searchContact;
            }
        }
        return null;
    }

    public Vector<QilexContact> getFrifonContacts() {
        synchronized (mContactSyncObject) {
            if (mFrifonContact.isEmpty()) {
                return new Vector<QilexContact>();
            } else {
                return new Vector<QilexContact>(mFrifonContact);
            }
        }
    }

    public Vector<QilexContact> getAllContact() {
        if (mAllContact.isEmpty()) {
            return new Vector<QilexContact>();
        } else {
            return new Vector<QilexContact>(mAllContact);
        }
    }

    public Bitmap getContactBitmapFromUri(String strUri) {
        return ContactContentProviderHelper.getBitmapContactPhoto(MyApplication.getAppContext(), strUri);
    }

    public boolean isContactExistInSqlite(long id) {
        return true;
    }

    public void addContact(final String firstName, final String lastName, final String phoneNumber, final Bitmap bitmap,
            final boolean isFrifon, final ContactProcessingHandler handler,
            final ContactEntryProcessListener addServerEntryHandler) {
        AsyncTask<Integer, Integer, QilexContact> task = new AsyncTask<Integer, Integer, QilexContact>() {
            @Override
            protected QilexContact doInBackground(Integer... params) {
                handler.onStart();
                return doAddContact(firstName, lastName, phoneNumber, bitmap, isFrifon);
            }

            @Override
            protected void onPostExecute(QilexContact result) {
                if (result != null) {
                    reloadAllContact();
                    fireNewContactListener(result);

                    if (Utils.isInMasterMode() == true) {
                        postContactToServer(result, addServerEntryHandler);
                    } else if (isFrifon == false) {
                        checkPhoneNoIsFrifon(result, phoneNumber);
                    }
                }
                handler.onFinish(result != null, result);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void deleteContact(final Context context, final QilexContact contact,
            final ContactProcessingHandler handler) {
        AsyncTask<Integer, Integer, QilexContact> task = new AsyncTask<Integer, Integer, QilexContact>() {
            @Override
            protected QilexContact doInBackground(Integer... params) {
                handler.onStart();
                return doDeleteContact(context, contact);
            }

            @Override
            protected void onPostExecute(QilexContact result) {
                if (result != null) {
                    deleteContactEntry(result.serverContactId, null);
                    reloadAllContact();
                    fireDeleteContactListener(result);
                }
                handler.onFinish(result != null, result);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Add phone number to contact.
     * 
     * @param activity
     * @param originContact
     * @param number
     * @param handler
     */
    public void addNumberToContact(final Context activity, final QilexContact originContact, final String number,
            final ContactProcessingHandler handler, final ContactEntryProcessListener updateServerEntryHandler) {
        AsyncTask<Integer, Integer, QilexContact> task = new AsyncTask<Integer, Integer, QilexContact>() {
            @Override
            protected QilexContact doInBackground(Integer... params) {
                handler.onStart();
                return doAddNumberToContact(activity, originContact, number);
            }

            @Override
            protected void onPostExecute(QilexContact result) {
                if (result != null) {
                    reloadAllContact();
                    fireChangeContactListener(result);

                    if (Utils.isInMasterMode() == true) {
                        postContactToServer(result, updateServerEntryHandler);
                    } else {
                        checkPhoneNoIsFrifon(result, number);
                    }
                }
                handler.onFinish(result != null, result);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void updateContact(final BaseActivity activity, final String lookupKey, final long contactId,
            final long rawId, final String firstName, final String lastName, final Bitmap bitmap,
            final List<PhoneItem> removedItemsPhone, final List<PhoneItem> addItemsPhone,
            final ContactProcessingHandler handler, final ContactEntryProcessListener updateServerEntryHandler) {
        AsyncTask<Integer, Integer, QilexContact> task = new AsyncTask<Integer, Integer, QilexContact>() {
            @Override
            protected QilexContact doInBackground(Integer... params) {
                handler.onStart();
                return doUpdateContact(activity, contactId, rawId, lookupKey, firstName, lastName, bitmap,
                        removedItemsPhone, addItemsPhone);
            }

            @Override
            protected void onPostExecute(QilexContact result) {
                if (result != null) {
                    reloadAllContact();
                    fireChangeContactListener(result);
                    if (Utils.isInMasterMode() == true) {
                        postContactToServer(result, updateServerEntryHandler);
                    }
                }
                handler.onFinish(result != null, result);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void postContactToServer(QilexContact result, final ContactEntryProcessListener updateServerEntryHandler) {
        long serverContactId = result.serverContactId;
        LogUtils.d("KUNLQT", "postContactToServer serverContactId: " + serverContactId);
        if (serverContactId <= 0) {
            addContactEntry(result, new ContactEntryProcessListener() {
                @Override
                public void onContactSnapshotGet(ErrorModel error, QilexContact contact,
                        ContactSnapshotModel snapshot) {
                    if (updateServerEntryHandler != null) {
                        updateServerEntryHandler.onContactSnapshotGet(error, contact, snapshot);
                    }
                    if (error == null) {
                        if (snapshot != null) {
                            addContactToPhoneBook(contact, snapshot);
                            reloadAllContact();
                            fireChangeContactListener(contact);
                        }
                    }
                }
            });
        } else {
            updateContactEntry(result, new ContactEntryProcessListener() {
                @Override
                public void onContactSnapshotGet(ErrorModel error, QilexContact contact,
                        ContactSnapshotModel snapshot) {
                    if (updateServerEntryHandler != null) {
                        updateServerEntryHandler.onContactSnapshotGet(error, contact, snapshot);
                    }
                    if (error == null && snapshot != null) {
                        addContactToPhoneBook(contact, snapshot);
                        reloadAllContact();
                        fireChangeContactListener(contact);
                    } else if (error != null && error.getByErrorCode(131) != null) {
                        ArrayList<QilexContact> outUpdateContact = new ArrayList<>();
                        outUpdateContact.add(contact);
                        removeSyncDataFromContacts(outUpdateContact);
                    }
                }
            });
        }
    }

    public void checkPhoneNoIsFrifon(final QilexContact result, final String phoneNo) {
        AuthenticationManager.getInstance().requestCheckFrifonNumber(phoneNo,
                new AuthenticationManager.CheckIsFrifonNumberListener() {
                    @Override
                    public void onCheckIsFrifonNumberFinish(int errorCode, final boolean result) {
                        if (errorCode == 0) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    setPhoneNumberKATZToContacts(phoneNo, Const.STR_EMPTY, result);
                                }
                            }).start();
                        }
                    }
                });
    }

    private String convertContactsToJsonArray(ArrayList<QilexContact> ouputAllContact, boolean isAddServerId)
            throws JSONException {
        // loop for each QilexContact in ouputAllContact.
        JSONArray jsonArray = new JSONArray();
        for (QilexContact qilexContact : ouputAllContact) {
            JSONObject jsonObj = new JSONObject();

            // Generate firstname and lastname
            jsonObj.put("first_name", qilexContact.getFirstName());
            jsonObj.put("last_name", qilexContact.getLastName());
            if (isAddServerId == true) {
                if (qilexContact.getServerContactId() > -1) {
                    jsonObj.put("id", qilexContact.getServerContactId());
                } else {
                    long id = ContactContentProviderHelper.getContactServerId(MyApplication.getAppContext(),
                            qilexContact.getRawContactId());
                    jsonObj.put("id", id);
                }
            }

            // Generate phone numbers
            PhoneNumberModel[] normalPhone = qilexContact.getPhoneNumberNormal();
            int phoneNoIndex = 0;
            for (PhoneNumberModel phoneNumberModel : normalPhone) {
                JSONObject phoneJson = new JSONObject();
                phoneJson.put("name", phoneNumberModel.type);
                phoneJson.put("msisdn",
                        QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNumberModel.phoneNumber));
                jsonObj.put("msisdn_" + phoneNoIndex, phoneJson);
                phoneNoIndex++;
            }

            PhoneNumberModel[] frifonPhone = qilexContact.getPhoneNumberKATZ();
            for (PhoneNumberModel phoneNumberModel : frifonPhone) {
                JSONObject phoneJson = new JSONObject();
                phoneJson.put("name", phoneNumberModel.type);
                phoneJson.put("msisdn",
                        QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNumberModel.phoneNumber));
                jsonObj.put("msisdn_" + phoneNoIndex, phoneJson);
                phoneNoIndex++;
            }

            jsonArray.put(jsonObj);
        }
        return jsonArray.toString();
    }

    /**
     * Post list contact to server
     * 
     * @throws JSONException
     */
    public int postAllContactListSnapshot(Context context, ArrayList<QilexContact> ouputAllContact,
            final GetContactSnapshotListener listener) throws JSONException, IOException {
        String strContactData = convertContactsToJsonArray(ouputAllContact, false);
        DictBody body = Zip.compress(strContactData);

        // Call method send to server
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        ArrayList<ContactSnapshotModel> listContactSnapshot = null;
                        ErrorModel errors = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            LogUtils.d("Post Contact", "Post Contact Response: " + message);

                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONArray response = new JSONArray(message);
                                listContactSnapshot = new ArrayList<ContactSnapshotModel>();
                                int size = response.length();
                                for (int i = 0; i < size; i++) {
                                    ContactSnapshotModel model = new ContactSnapshotModel(response.getJSONObject(i));
                                    listContactSnapshot.add(model);
                                }
                            } catch (JSONException e) {
                                errors = new ErrorModel(message);
                            }
                        }
                        listener.onGetContactSnapshotFinish(errors, listContactSnapshot);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        LogUtils.d("Post Contact", "Post Contact Response ERROR " + req.getState());
                        ErrorModel errorsDefault = new ErrorModel();
                        listener.onGetContactSnapshotFinish(errorsDefault, null);
                        break;
                }
            }
        };

        QilexHttpRequest request = ContactHttpUtils.postContactListSnapshot(context,
                UserInfo.getInstance().getPhoneNumberFormatted(), body.data_comp, body.data_checksum,
                httpRequestListener);
        return request.getRequestId();
    }

    /**
     * Post list contact to server
     * 
     * @throws JSONException
     */
    public int refreshAllContactListSnapshot(Context context, ArrayList<QilexContact> ouputAllContact,
            final GetContactSnapshotListener listener) throws JSONException, IOException {
        String strContactData = convertContactsToJsonArray(ouputAllContact, true);
        DictBody body = Zip.compress(strContactData);

        // Call method send to server
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        ArrayList<ContactSnapshotModel> listContactSnapshot = null;
                        ErrorModel errors = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());

                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONArray response = new JSONArray(message);
                                listContactSnapshot = new ArrayList<ContactSnapshotModel>();
                                int size = response.length();
                                for (int i = 0; i < size; i++) {
                                    ContactSnapshotModel model = new ContactSnapshotModel(response.getJSONObject(i));
                                    listContactSnapshot.add(model);
                                }
                            } catch (JSONException e) {
                                errors = new ErrorModel(message);
                            }
                        }
                        listener.onGetContactSnapshotFinish(errors, listContactSnapshot);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errorsDefault = new ErrorModel();
                        listener.onGetContactSnapshotFinish(errorsDefault, null);
                        break;
                }
            }
        };

        QilexHttpRequest request = ContactHttpUtils.refreshContactListSnapshot(context,
                UserInfo.getInstance().getPhoneNumberFormatted(), body.data_comp, body.data_checksum,
                httpRequestListener);
        return request.getRequestId();
    }

    public int getAllContactEntry(final GetContactSnapshotListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        ArrayList<ContactSnapshotModel> listContactSnapshot = null;
                        ErrorModel errors = null;
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                String dataComp = response.getString("data_comp");
                                String unZip = Zip.decompress(dataComp);

                                // Convert to list contact
                                listContactSnapshot = new ArrayList<>();
                                SharedPrefrerenceFactory.getInstance().setContactListJsonCache(unZip);
                                JSONArray arrayObject = new JSONArray(unZip);
                                int size = arrayObject.length();
                                for (int i = 0; i < size; i++) {
                                    ContactSnapshotModel snapshot = new ContactSnapshotModel(
                                            arrayObject.getJSONObject(i));
                                    listContactSnapshot.add(snapshot);
                                }

                                listener.onGetContactSnapshotFinish(null, listContactSnapshot);
                            } catch (Exception e) {
                                errors = new ErrorModel(message);
                                listener.onGetContactSnapshotFinish(errors, null);
                            }
                        } else {
                            ErrorModel errorsDefault = new ErrorModel();
                            listener.onGetContactSnapshotFinish(errorsDefault, null);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errorsDefault = new ErrorModel();
                        listener.onGetContactSnapshotFinish(errorsDefault, null);
                        break;
                }
            }
        };
        QilexHttpRequest request = ContactHttpUtils.getContactList(UserInfo.getInstance().getPhoneNumberFormatted(),
                httpRequestListener);
        return request.getRequestId();
    }

    /**
     * Get contact entry from server
     * 
     * @param serverId
     * @param listener
     * @return
     */
    public int getContactEntry(long serverId, final GetContactSnapshotListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        ArrayList<ContactSnapshotModel> listContactSnapshot = null;
                        ErrorModel errors = null;
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                JSONObject response = new JSONObject(message);
                                ContactSnapshotModel model = new ContactSnapshotModel(response);
                                listContactSnapshot = new ArrayList<>();
                                listContactSnapshot.add(model);
                                listener.onGetContactSnapshotFinish(null, listContactSnapshot);
                            } catch (Exception e) {
                                errors = new ErrorModel(message);
                                listener.onGetContactSnapshotFinish(errors, null);
                            }
                        } else {
                            ErrorModel errorsDefault = new ErrorModel();
                            listener.onGetContactSnapshotFinish(errorsDefault, null);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errorsDefault = new ErrorModel();
                        listener.onGetContactSnapshotFinish(errorsDefault, null);
                        break;
                }
            }
        };
        QilexHttpRequest request = ContactHttpUtils.getContactEntry(MyApplication.getAppContext(),
                UserInfo.getInstance().getPhoneNumberFormatted(), serverId, httpRequestListener);
        return request.getRequestId();
    }

    /**
     * Delete contact entry from server
     * 
     * @param serverId
     * @param listener
     * @return
     */
    public int deleteContactEntry(long serverId, final GetContactSnapshotListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        ArrayList<ContactSnapshotModel> listContactSnapshot = null;
                        ErrorModel errors = null;
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            if (listener != null)
                                listener.onGetContactSnapshotFinish(null, null);
                        } else {
                            ErrorModel errorsDefault = new ErrorModel();
                            if (listener != null)
                                listener.onGetContactSnapshotFinish(errorsDefault, null);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errorsDefault = new ErrorModel();
                        if (listener != null)
                            listener.onGetContactSnapshotFinish(errorsDefault, null);
                        break;
                }
            }
        };
        QilexHttpRequest request = ContactHttpUtils.deleteContactEntry(MyApplication.getAppContext(),
                UserInfo.getInstance().getPhoneNumberFormatted(), serverId, httpRequestListener);
        return request.getRequestId();
    }

    public void updateContactEntry(final QilexContact contact, final ContactEntryProcessListener listener) {
        // Call webservice api
        ContactHttpUtils.updateContactEntry(MyApplication.getAppContext(),
                UserInfo.getInstance().getPhoneNumberFormatted(), contact, new QilexHttpRequestListener() {
                    @Override
                    public void httpRequestStateChanged(QilexHttpRequest req) {
                        switch (req.getState()) {
                            case QilexHttpRequest.STATE_COMPETED:
                                if (req.getResponseData() != null) {
                                    String message = new String(req.getResponseData());
                                    LogUtils.d("CONTACT", "updateContactEntry: " + message);
                                    try {
                                        JSONObject response = new JSONObject(message);
                                        ErrorModel errorModel = null;
                                        ContactSnapshotModel snapshot = new ContactSnapshotModel(response);

                                        // If snashot is = -1 then get from
                                        // server
                                        // fail,
                                        // parse error
                                        if (snapshot.id == -1) {
                                            errorModel = new ErrorModel(response);
                                        }
                                        if (listener != null) {
                                            listener.onContactSnapshotGet(errorModel, contact, snapshot);
                                        }
                                    } catch (JSONException e) {
                                        if (listener != null) {
                                            ErrorModel errorModel = new ErrorModel(message);
                                            listener.onContactSnapshotGet(errorModel, contact, null);
                                        }
                                    }
                                }
                                // TODO: Save contact id to sqlite
                                break;
                            case QilexHttpRequest.STATE_ERROR:
                            case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                            case QilexHttpRequest.STATE_ERROR_DATE:
                                LogUtils.d("CONTACT", "updateContactEntry2: ERROR " + contact.getDisplayName());
                                if (listener != null) {
                                    ErrorModel errorModel = new ErrorModel();
                                    listener.onContactSnapshotGet(errorModel, contact, null);
                                }
                                break;
                        }
                    }
                });
    }

    public void addContactEntry(final QilexContact contact, final ContactEntryProcessListener listener) {
        // Call webservice api
        ContactHttpUtils.createContactEntry(MyApplication.getAppContext(),
                UserInfo.getInstance().getPhoneNumberFormatted(), contact, new QilexHttpRequestListener() {
                    @Override
                    public void httpRequestStateChanged(QilexHttpRequest req) {
                        switch (req.getState()) {
                            case QilexHttpRequest.STATE_COMPETED:
                                if (req.getResponseData() != null) {
                                    String message = new String(req.getResponseData());
                                    LogUtils.d("CONTACT", "createContactEntry: " + message);
                                    try {
                                        JSONObject response = new JSONObject(message);
                                        ErrorModel errorModel = null;
                                        ContactSnapshotModel snapshot = new ContactSnapshotModel(response);

                                        // If snashot is = -1 then get from
                                        // server
                                        // fail,
                                        // parse error
                                        if (snapshot.id == -1) {
                                            errorModel = new ErrorModel(response);
                                        }
                                        if (listener != null) {
                                            listener.onContactSnapshotGet(errorModel, contact, snapshot);
                                        }
                                    } catch (JSONException e) {
                                        if (listener != null) {
                                            ErrorModel errorModel = new ErrorModel(message);
                                            listener.onContactSnapshotGet(errorModel, contact, null);
                                        }
                                    }
                                }
                                // TODO: Save contact id to sqlite
                                break;
                            case QilexHttpRequest.STATE_ERROR:
                            case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                            case QilexHttpRequest.STATE_ERROR_DATE:
                                if (listener != null) {
                                    ErrorModel errorModel = new ErrorModel();
                                    listener.onContactSnapshotGet(errorModel, contact, null);
                                }
                                break;
                        }
                    }
                });
    }

    public synchronized void addContactChangeListener(ContactChangeListener listener) {
        if (mContactChangeListener.contains(listener) == false) {
            mContactChangeListener.addElement(listener);
        }
    }

    public synchronized void removeContactChangeListener(ContactChangeListener listener) {
        mContactChangeListener.removeElement(listener);
    }

    public synchronized void addFirstSyncListener(NotifyCacheContactChangeListener listener) {
        if (mFirstSyncListener.contains(listener) == false) {
            mFirstSyncListener.addElement(listener);
        }
    }

    public synchronized void removeFirstSyncListener(NotifyCacheContactChangeListener listener) {
        mFirstSyncListener.removeElement(listener);
    }

    public synchronized void addContactSyncListener(ContactSyncListener listener) {
        if (mContactSyncListener.contains(listener) == false) {
            mContactSyncListener.addElement(listener);
        }
    }

    public synchronized void removeContactSyncListener(ContactSyncListener listener) {
        mContactSyncListener.removeElement(listener);
    }

    private synchronized void fireContactSyncStart(boolean isUpdate, boolean isAddNew) {
        for (Iterator<ContactSyncListener> i = mContactSyncListener.iterator(); i.hasNext();) {
            ContactSyncListener l = null;
            try {
                l = i.next();
                l.onSyncStart(isUpdate, isAddNew);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireContactSyncFinish(boolean isSuccess) {
        for (Iterator<ContactSyncListener> i = mContactSyncListener.iterator(); i.hasNext();) {
            ContactSyncListener l = null;
            try {
                l = i.next();
                l.onSyncFinish(isSuccess);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireNewContactListener(QilexContact contact) {
        if (mContactChangeListener.isEmpty()) {
            return;
        }
        Vector<ContactChangeListener> v = new Vector<>(mContactChangeListener);
        for (Iterator<ContactChangeListener> i = v.iterator(); i.hasNext();) {
            ContactChangeListener l = null;
            try {
                l = i.next();
                l.onNewContact(contact);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireChangeContactListener(QilexContact contact) {
        if (mContactChangeListener.isEmpty()) {
            return;
        }
        Vector<ContactChangeListener> v = new Vector<>(mContactChangeListener);
        for (Iterator<ContactChangeListener> i = v.iterator(); i.hasNext();) {
            ContactChangeListener l = null;
            try {
                l = i.next();
                l.onUpdateContact(contact);
            } catch (Throwable t) {
                @SuppressWarnings("unused")
                int a = 0;
            }
        }
    }

    private synchronized void fireDeleteContactListener(QilexContact contact) {
        if (mContactChangeListener.isEmpty()) {
            return;
        }
        Vector<ContactChangeListener> v = new Vector<>(mContactChangeListener);
        for (Iterator<ContactChangeListener> i = v.iterator(); i.hasNext();) {
            ContactChangeListener l = null;
            try {
                l = i.next();
                l.onDeleteContact(contact);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void notifyCacheContactsChanged() {
        if (mFirstSyncListener.isEmpty()) {
            return;
        }
        Vector<NotifyCacheContactChangeListener> v = new Vector<>(mFirstSyncListener);
        for (Iterator<NotifyCacheContactChangeListener> i = v.iterator(); i.hasNext();) {
            NotifyCacheContactChangeListener l = null;
            try {
                l = i.next();
                l.onCacheContactChange();
            } catch (Throwable t) {
            }
        }
    }

    /**
     * Add new contact to contact list
     * 
     * @param firstName firstName.
     * @param lastName last Name.
     * @param phoneNumber phone number.
     * @param bitmap bitmap avatar.
     * @return true if add successfully.
     */
    private synchronized QilexContact doAddContact(String firstName, String lastName, String phoneNumber, Bitmap bitmap,
            boolean isFrifon) {
        // Put to Devices Contact
        long rawContactId = ContactContentProviderHelper.addContact(firstName, lastName, phoneNumber, bitmap, -1);

        StringBuilder selection = new StringBuilder();
        selection.append(RawContacts._ID + " = " + rawContactId);
        Cursor pCur = MyApplication.getAppContext().getContentResolver().query(RawContacts.CONTENT_URI,
                projectionRawContact, selection.toString(), null, null);
        long contactId = -1;
        if (pCur.moveToFirst() == true) {
            contactId = pCur.getLong(0);
        }
        pCur.close();

        // If is frifon, add raw contact
        QilexContact newContact = contactId <= 0 ? null : getContactById(contactId);
        if (isFrifon) {
            ContactSnapshotModel snapshot = new ContactSnapshotModel();
            snapshot.frifon_user = true;
            snapshot.id = -1;
            snapshot.frifon_msisdn = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNumber);
            addContactToPhoneBook(newContact, snapshot);
        }
        if (newContact != null) {
            mHashMapAllContact.put(newContact.id, newContact);
            addContactToPhoneBookNumber(phoneNumber, newContact);
        }
        return newContact;
    }

    private void addContactToPhoneBookNumber(String phoneNum, QilexContact contact) {
        synchronized (mPhoneBookNumber) {
            ArrayList<QilexContact> listContact = mPhoneBookNumber.get(phoneNum);
            if (listContact == null) {
                listContact = new ArrayList<>();
            }
            listContact.add(contact);
            mPhoneBookNumber.put(phoneNum, listContact);
        }
    }

    private synchronized QilexContact doDeleteContact(Context context, QilexContact contact) {
        // Delete from phone contact
        ContentResolver cr = context.getContentResolver();
        String where = RawContacts.CONTACT_ID + " = " + contact.id;
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ops.add(ContentProviderOperation.newDelete(RawContacts.CONTENT_URI).withSelection(where, null).build());
        try {
            cr.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }

        // Update Cache data
        mHashMapAllContact.remove(contact.id);
        synchronized (mContactSyncObject) {
            for (int i = 0; i < mAllContact.size(); i++) {
                QilexContact c = mAllContact.get(i);
                if (c.id == contact.id) {
                    mAllContact.removeElement(c);
                    mFrifonContact.removeElement(c);
                    break;
                }
            }
        }
        return contact;
    }

    private QilexContact doAddNumberToContact(Context activity, QilexContact originContact, String number) {
        // Update in Phone Contact
        ContactContentProviderHelper.addNumberToContact(activity, originContact.rawContactId, number);

        // Update Cache Data
        QilexContact updatedContact = null;
        synchronized (mContactSyncObject) {
            for (int i = 0; i < mAllContact.size(); i++) {
                QilexContact c = mAllContact.get(i);
                if (c.id == originContact.id) {
                    mAllContact.removeElement(c);
                    updatedContact = getContactById(originContact.id);
                    mAllContact.addElement(updatedContact);

                    if (mFrifonContact.contains(c)) {
                        mFrifonContact.removeElement(c);
                        if (updatedContact.isKATZ()) {
                            mFrifonContact.addElement(updatedContact);
                        }
                    }
                    break;
                }
            }
            Collections.sort(mAllContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
            Collections.sort(mFrifonContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
        }
        return updatedContact;
    }

    private QilexContact doUpdateContact(BaseActivity activity, final long contactId, long rawContactId,
            final String lookupKey, String firstName, String lastName, Bitmap bitmap, List<PhoneItem> removedItemsPhone,
            List<PhoneItem> addItemsPhone) {
        // Update Phone Contact
        PhoneItem[] item = new PhoneItem[removedItemsPhone.size()];
        item = removedItemsPhone.toArray(item);
        for (PhoneItem phoneItem : item) {
            PhoneItem sipItem2 = new PhoneItem();
            sipItem2.number = QilexPhoneNumberUtils.removePhoneCode(phoneItem.number)[1];
            removedItemsPhone.add(sipItem2);

            PhoneItem sipItem3 = new PhoneItem();
            sipItem3.number = "0" + QilexPhoneNumberUtils.removePhoneCode(phoneItem.number)[1];
            removedItemsPhone.add(sipItem3);

            PhoneItem sipItem4 = new PhoneItem();
            sipItem4.number = phoneItem.originNumber;
            removedItemsPhone.add(sipItem4);
        }

        ContactContentProviderHelper.updateContact(activity, rawContactId, firstName, lastName, bitmap,
                removedItemsPhone, addItemsPhone);
        // Update Cache data
        QilexContact updatedContact = updateCacheDataContact(activity, contactId, lookupKey);

        return updatedContact;
    }

    private QilexContact updateCacheDataContact(Context context, long contactId, String contactLookupKey) {
        synchronized (mContactSyncObject) {
            QilexContact updatedContact = getContactById(contactId);
            if (updatedContact.id == -1) {
                updatedContact = getContactByLookupKey(contactLookupKey);
                if (updatedContact.id == -1) {
                    return null;
                }
            }
            for (int i = 0; i < mAllContact.size(); i++) {
                QilexContact c = mAllContact.get(i);
                if (c.id == contactId || c.lookUpKey.equals(contactLookupKey)) {
                    mAllContact.removeElement(c);
                    if (updatedContact.hasPhoneNumber()) {
                        mAllContact.addElement(updatedContact);
                    }
                    if (mFrifonContact.contains(c)) {
                        mFrifonContact.removeElement(c);
                        if (updatedContact.isKATZ() && updatedContact.hasPhoneNumber()) {
                            mFrifonContact.addElement(updatedContact);
                        }
                    } else if (updatedContact.isKATZ() && updatedContact.hasPhoneNumber()) {
                        mFrifonContact.addElement(updatedContact);
                    }
                    break;
                }
            }
            Collections.sort(mAllContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
            Collections.sort(mFrifonContact, new QilexContact.NameComparator(MyApplication.getAppContext()));
            return updatedContact;
        }
    }

    private void applyTempContactToDb() {
        synchronized (mTempContacts) {
            ArrayList<TempContactModel> models = new ArrayList<>(mTempContacts.values());
            mContactTempDb.putTempContact(models);
        }
    }

    public TempContactModel getTempContact(String phoneNumber) {
        synchronized (mTempContacts) {
            return mTempContacts.get(phoneNumber);
        }
    }

    public int loadContactInfo(Context context, final String phoneNo, final GetContactTempByPhoneListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            LogUtils.d("KUNLQT", "loadContactInfo Response: " + message);

                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);

                                UserInfoResponseModel userInfo = new UserInfoResponseModel(response);
                                final TempContactModel model = new TempContactModel(phoneNo,
                                        userInfo.user_name + " " + userInfo.user_last_name, userInfo.user_avatar_uri);
                                mTempContacts.put(model.phoneNumber, model);
                                mContactTempDb.putTempContact(model);
                                QilexContact newContact = new QilexContact();
                                newContact.firstName = model.name;
                                newContact.photoUri = model.avatarUrl;

                                // Update contact to with phone number to KATZ
                                // user
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setPhoneNumberKATZToContacts(phoneNo, model.avatarUrl, true);
                                    }
                                }).start();

                                // Fire handlers
                                fireNewContactListener(newContact);
                                listener.onGetContactFinish(model, null);
                            } catch (JSONException e) {
                                ErrorModel model = new ErrorModel(message);
                                // If error is no user, or invalid active user
                                if (model.getByErrorCode(ErrorModel.CODE_NO_USER) != null
                                        || model.getByErrorCode(ErrorModel.CODE_INVALID_ACTIVE_USER) != null) {
                                    setPhoneNumberKATZToContacts(phoneNo, Const.STR_EMPTY, false);
                                }
                                listener.onGetContactFinish(null, model);
                            }
                        } else {
                            ErrorModel model = new ErrorModel();
                            listener.onGetContactFinish(null, model);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel model = new ErrorModel();
                        listener.onGetContactFinish(null, model);
                        break;
                }
            }
        };

        QilexHttpRequest request = UserActivationUtils.getAccountInfo(phoneNo, httpRequestListener);
        return request.getRequestId();
    }

    public void registerContentObserver() {
        if (mContentObserver == null) {
            mContentObserver = new TableObserver(null);
            MyApplication.getAppContext().getContentResolver()
                    .registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, mContentObserver);
        }
    }

    public void unregisterContentObserver() {
        if (mContentObserver != null) {
            MyApplication.getAppContext().getContentResolver().unregisterContentObserver(mContentObserver);
            mContentObserver = null;
        }
    }

    public interface ContactChangeListener {
        public void onNewContact(QilexContact newContact);

        public void onUpdateContact(QilexContact newContact);

        public void onDeleteContact(QilexContact newContact);
    }

    public interface GetFrifonPhoneListener {
        public void onGetFrifonPhoneFinish(ArrayList<PhoneNumberModel> result);
    }

    public interface NotifyCacheContactChangeListener {
        public void onCacheContactChange();
    }

    public interface ContactEntryProcessListener {
        public void onContactSnapshotGet(ErrorModel error, QilexContact contact, ContactSnapshotModel snapshot);
    }

    public interface ContactSyncListener {
        public void onSyncStart(boolean isUpdate, boolean isAddNew);

        public void onSyncFinish(boolean isSuccess);

        public void onSyncProgress(int progress);
    }

    public interface GetContactTempByPhoneListener {
        public void onGetContactFinish(TempContactModel model, ErrorModel error);
    }

    public interface GetContactInfoListener {
        public void onGetContactInfoFinish(QilexContact contact);
    }
}
