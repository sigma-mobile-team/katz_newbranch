
package sigma.qilex.manager.contact.sync;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import sigma.qilex.utils.LogUtils;

public class SyncService extends Service {
    private static SyncAdapter sSyncAdapter = null;

    public SyncService() {
        super();
    }

    private static class SyncAdapter extends AbstractThreadedSyncAdapter {
        private Context mContext;

        public SyncAdapter(Context context) {
            super(context, true);
            mContext = context;
        }

        @Override
        public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider,
                SyncResult syncResult) {
            try {
                LogUtils.d("KUNLQT", "ContactsSyncAdapterService.performSync: " + account.toString());
                SyncService.performSync(mContext, account, extras, authority, provider, syncResult);
            } catch (OperationCanceledException e) {
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return getSyncAdapter().getSyncAdapterBinder();
    }

    private SyncAdapter getSyncAdapter() {
        if (sSyncAdapter == null) {
            sSyncAdapter = new SyncAdapter(this);
        }
        return sSyncAdapter;
    }

    private static void performSync(Context context, Account account, Bundle extras, String authority,
            ContentProviderClient provider, SyncResult syncResult) throws OperationCanceledException {
    }
}
