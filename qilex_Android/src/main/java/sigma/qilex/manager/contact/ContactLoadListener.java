
package sigma.qilex.manager.contact;

import java.util.ArrayList;

import sigma.qilex.dataaccess.model.QilexContact;

public interface ContactLoadListener {

    public void onContactLoadStart();

    public void onContactLoadFinish(ArrayList<QilexContact> listAll, ArrayList<QilexContact> listFrifon);
}
