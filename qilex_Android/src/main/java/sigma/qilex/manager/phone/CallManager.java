
package sigma.qilex.manager.phone;

import static android.media.AudioManager.AUDIOFOCUS_GAIN_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
import static android.media.AudioManager.MODE_IN_COMMUNICATION;
import static android.media.AudioManager.MODE_NORMAL;
import static android.media.AudioManager.STREAM_VOICE_CALL;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.OutCallListHistoryModel;
import sigma.qilex.dataaccess.network.http.AccountBalanceUtils;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest.QilexHttpRequestListener;
import sigma.qilex.manager.I_ThreadMessageDispatcher;
import sigma.qilex.manager.QilexSoundService;
import sigma.qilex.manager.ThreadMessageDispatcher;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipCall;
import sigma.qilex.sip.SipCall.SipCallListener;
import sigma.qilex.sip.SipIncomingCallListener;
import sigma.qilex.sip.SipManager;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.IActivity;
import sigma.qilex.ui.activity.InCallActivity;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.SimUtils;
import sigma.qilex.utils.Utils;

/**
 * Class contain methods processing SIP Call functions.
 */
public class CallManager implements SipCallListener, SipIncomingCallListener {

    private static final int MSG_END_PUSH_CALL = 1;

    private static final int MSG_PLAYRINGTONE = 2;

    private static final int PUSH_STATE_INCOMING = 0;

    private static final int PUSH_STATE_CLOSED = 1;

    private static final int PUSH_STATE_ESTABLISH = 2;

    public enum CallActivityEvent {
        CallReject, CallAnswer, CallWithoutTurn, CallEnd, CallDivert, SpeakerOn, SpeakerOff, MuteOn, MuteOff, HeadphonesOn, HeadphonesOff, AudioVolume
    }

    private static CallManager mInstance;

    public static final synchronized CallManager getInstance() {
        if (mInstance == null) {
            mInstance = new CallManager();
        }
        return mInstance;
    }

    private CallLogManager mCallLogManager;

    private QilexSoundService mQilexSoundService;

    private SipManager mSipManager;

    private AudioManager mAudioManager;

    private boolean mAudioFocused;

    private boolean isJustLoginSIP = false;

    private boolean mIsCallInProgress;

    private SipCall mCurrSipCall;

    private CallLogModel mCurrCallLogModel;

    private String mPushPhoneNumber;

    @SuppressWarnings("unused")
    private int mPushPhoneState;

    private Vector<QilexSipCallServiceListener> mQilexSipCallServiceListener = new Vector<CallManager.QilexSipCallServiceListener>();

    private ThreadMessageDispatcher vrcDispatcher;

    private I_ThreadMessageDispatcher mListenerCallTimeOut = new I_ThreadMessageDispatcher() {
        public void onHandleMessage(Message vrpMsg) {
            switch (vrpMsg.what) {
                case MSG_END_PUSH_CALL:
                    QilexService qilexService = BaseActivity.getService();
                    if (qilexService != null) {
                        mQilexSoundService.stopAllSignal();
                        qilexService.sendBroadcast(new Intent(InCallActivity.ACTION_ENDCALL));
                        if (mCurrCallLogModel.getType() == CallLogModel.TYPE_INCOMING) {
                            // This is Miss Call
                            mCallLogManager.updateCallLogType(mCurrCallLogModel.getDbId(), CallLogModel.TYPE_MISSCALL);
                        }
                        mPushPhoneNumber = null;
                        mIsCallInProgress = false;
                    }
                    break;
                case MSG_PLAYRINGTONE:
                    requestAudioFocus("incomingCall");
                    break;
            }
        }
    };

    private CallManager() {
        mSipManager = SipManager.getInstance(MyApplication.getAppContext());
        mAudioManager = (AudioManager)MyApplication.getAppContext().getSystemService(Context.AUDIO_SERVICE);
        mCallLogManager = CallLogManager.getInstance();
        mQilexSoundService = QilexSoundService.getInstance();
        vrcDispatcher = new ThreadMessageDispatcher(mListenerCallTimeOut);
    }

    private AudioManager getAudioManager() {
        if (mAudioManager == null) {
            QilexService qilexService = BaseActivity.getService();
            if (qilexService != null) {
                mAudioManager = QilexService.getAudioManager();
            }
        }

        return mAudioManager;
    }

    public SipCall getCurrCall() {
        return mCurrSipCall;
    }

    private void requestAudioFocus(String lable) {
        if (!mAudioFocused) {
            int res = mAudioManager.requestAudioFocus(null, STREAM_VOICE_CALL, AUDIOFOCUS_GAIN_TRANSIENT);
            if (res == AUDIOFOCUS_REQUEST_GRANTED)
                mAudioFocused = true;
        }
    }

    private void cancelRequestAudio(String lable) {
        if (mAudioFocused) {
            int res = getAudioManager().abandonAudioFocus(null);
            LogUtils.e("NAMND::" + lable,
                    "Audio focus released a bit later: " + (res == AUDIOFOCUS_REQUEST_GRANTED ? "Granted" : "Denied"));
            mAudioFocused = false;
        }
    }

    public void initOutGoingSipCall(IActivity context, String inputString) {
        // TODO: add code after have requirement
        int resultId = CallManager.getInstance().initOutGoingSipCall(inputString);
        if (resultId > 0) {
            context.showAlert(R.string.call_fail, resultId);
        } else if (resultId == -1) {
            String number = "tel:" + inputString;
            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
            context.startActivity(callIntent);
        }
    }

    /**
     * see Method initOutGoingSipCall(String inputString, boolean isOUT)
     * 
     * @param inputString A phone number
     * @return resource string id of error, 0 if success.
     */
    public int initOutGoingSipCall(String inputString) {
        return initOutGoingSipCall(inputString, false);
    }

    /**
     * Method make a freefon call.
     * 
     * @param inputString A phone number
     * @param isOUT for frifonOUT
     * @return resource string id of error, 0 if success.
     */
    public int initOutGoingSipCall(String inputString, boolean isOUT) {
        // Check SIM card changed
        if (!Utils.isInKatzMode() && SimUtils.checkIMSIchangeManual()) {
            return R.string.sim_is_changed;
        }

        // Check network
        if (!QilexUtils.isOnline()) {
            return R.string.msg_call_no_network_title;
        }

        // Check if there is call in progress
        if (mIsCallInProgress == true) {
            return R.string.error_has_current_call;
        }
        // Check if phone number is emergency to gsm
        if (QilexPhoneNumberUtils.checkEmergencyGsm(inputString)) {
            if (Utils.isSupportTelephony())
                return -1;
            else
                return R.string.error_phone_number_not_support;
        }
        // Check if phone number is special number
        if (QilexPhoneNumberUtils.isEmergencyNumber(inputString, true)) {
            return R.string.error_phone_number_not_support;
        }

        final QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
        String sipLoginName = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(inputString);
        // check if peer account is yourself
        if (sipLoginName.equals(localProfile.getLogin())) {
            return R.string.error_call_yourself;
        }

        QilexSipProfile localSipProfile = localProfile.getDefaultSipProfile();

        if (localSipProfile == null) {
            return R.string.error_cannot_connect_to_server;
        }
        // Set call in progress to true
        mIsCallInProgress = true;

        // Start dial tone
        cancelRequestAudio("startSipCall");
        mQilexSoundService.stopAllSignal();
        mQilexSoundService.startCallConnectingSignal();
        requestAudioFocus("startSipCall");
        QilexService qilexService = BaseActivity.getService();
        if (qilexService != null) {
            qilexService.showOutgoingSipCallGUI(sipLoginName, isOUT);
        }

        // Add new call log to db
        if (isOUT) {
            mCurrCallLogModel = mCallLogManager.putNewCallLog(sipLoginName, CallLogModel.TYPE_GLOBAL);
        } else
            mCurrCallLogModel = mCallLogManager.putNewCallLog(sipLoginName, CallLogModel.TYPE_OUTGOING);

        return 0;
    }

    public int startSipCall(String sipLoginName, boolean isOut) {
        QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
        QilexSipProfile localSipProfile = localProfile.getDefaultSipProfile();
        if (localSipProfile == null) {
            return R.string.error_cannot_connect_to_server;
        }
        QilexSipProfile peerProfile = new QilexSipProfile();
        peerProfile.setLogin(sipLoginName);
        peerProfile.setServer(localSipProfile.getServer());
        peerProfile.setIsOUt(isOut);
        // Start calling
        mCurrSipCall = mSipManager.makeCall(localSipProfile, peerProfile, CallManager.this, 15);
        mCurrSipCall.setSpeakerOn(false);
        mCurrSipCall.setMute(false);
        return 0;
    }

    /**
     * Check that there is call in progress.
     * 
     * @return
     */
    public boolean isCallInProgress() {
        return mIsCallInProgress;
    }

    public boolean isPushCallInProgress() {
        return !Utils.isStringNullOrEmpty(mPushPhoneNumber);
    }

    /**
     * Register a UI handler to Call service.
     * 
     * @param listener
     */
    public void addSipCallServiceListener(QilexSipCallServiceListener listener) {
        if (mQilexSipCallServiceListener.contains(listener) == false) {
            mQilexSipCallServiceListener.addElement(listener);
        }
    }

    /**
     * Unregister a UI handler to Call service.
     * 
     * @param listener
     */
    public void removeSipCallServiceListener(QilexSipCallServiceListener listener) {
        mQilexSipCallServiceListener.removeElement(listener);
    }

    /**
     * Finish the current SIP call.
     */
    public void endCurrentSipCall() {
        mPushPhoneNumber = null;
        mIsCallInProgress = false;
        if (mCurrSipCall != null) {
            LogUtils.d("CALL END", "CALL EMD EMD EMD endCall / endCurrentSipCall");
            mCurrSipCall.endCall();
        }
    }

    /**
     * Answer call when have incoming call.
     */
    public void callAnswer() {
        if (Utils.isStringNullOrEmpty(mPushPhoneNumber) == true) {
            mPushPhoneNumber = null;
            answerCall();
        } else {
            answerPushCall();
        }
    }

    private void answerCall() {
        try {
            // Stop all signal
            mQilexSoundService.stopAllSignal();
            // Answer call with timeout is 3 seconds
            mCurrSipCall.answerCall(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void answerPushCall() {
        // Fire handlers
        mPushPhoneState = PUSH_STATE_ESTABLISH;
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callEstablished(null, true);
            } catch (Throwable t) {
            }
        }
    }

    /**
     * Reject call when have incoming call.
     */
    public void callReject() {
        // Check if call is from Push
        if (Utils.isStringNullOrEmpty(mPushPhoneNumber) == false) {
            mPushPhoneNumber = null;
            mPushPhoneState = PUSH_STATE_CLOSED;

            // Fire action call end
            for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
                QilexSipCallServiceListener l = null;
                try {
                    l = i.next();
                    l.callClosed(mCurrSipCall);
                } catch (Throwable t) {
                }
            }
            return;
        }
        if (mCurrSipCall == null) {
            return;
        }
        mCurrSipCall.setStartTimeMillis(Calendar.getInstance().getTimeInMillis());
        mCurrSipCall.endCall();
    }

    private void callReject(SipCall call) {
        mPushPhoneNumber = null;
        call.setStartTimeMillis(Calendar.getInstance().getTimeInMillis());
        call.endCall();
    }

    public void speakerOn() {
        if (mCurrSipCall == null) {
            return;
        }
        mCurrSipCall.setSpeakerOn(true);
        getAudioManager().setSpeakerphoneOn(true);
    }

    public void speakerOff() {
        if (mCurrSipCall == null) {
            return;
        }
        mCurrSipCall.setSpeakerOn(false);
        getAudioManager().setSpeakerphoneOn(false);
    }

    public void muteOn() {
        if (mCurrSipCall != null)
            mCurrSipCall.setMute(true);
    }

    public void muteOff() {
        if (mCurrSipCall != null)
            mCurrSipCall.setMute(false);
    }

    public boolean isMute() {
        if (mCurrSipCall == null) {
            return false;
        }
        return mCurrSipCall.isMuted();
    }

    public boolean isSpeakerOn() {
        return getAudioManager().isSpeakerphoneOn();
    }

    /**
     * Get Audio quality level<br>
     * Level is from 1 to 5.
     * 
     * @return level of audio quality (from 1 to 5)
     */
    public int getAudioQualityLevel() {
        if (mCurrSipCall == null) {
            return 1;
        }
        float quality = mCurrSipCall.audioQuality();
        if (quality > 4) {
            return 5;
        } else if (quality > 3) {
            return 4;
        } else if (quality > 2) {
            return 3;
        } else if (quality > 1) {
            return 2;
        } else {
            return 1;
        }
    }

    // /
    // / IMPLEMENT METHOD OF SipCallListener - START
    // /
    @Override
    public void onCallBusy(SipCall call) {
        if (mCurrSipCall != call) {
            return;
        }
        LogUtils.e("NAMND", "onCallBusy");
        // Setting audio manager and Signal
        cancelRequestAudio("onCallBusy");
        mQilexSoundService.stopAllSignal();
        mQilexSoundService.playCallBusySignal();

        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callBusy(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onCallEnded(SipCall call) {
        mPushPhoneNumber = null;
        if (mCurrSipCall != call) {
            return;
        }
        // Set call in progress to false
        mIsCallInProgress = false;
        getAudioManager().setMode(MODE_NORMAL);
        mQilexSoundService.stopAllSignal();

        // Check duration
        mCurrSipCall.setEndTimerMillis(Calendar.getInstance().getTimeInMillis());
        long startTime = mCurrSipCall.getStartTimeMillis();
        long endTime = mCurrSipCall.getEndTimerMillis();
        if (mCurrCallLogModel != null) {
            if (startTime == 0) {
                if (mCurrCallLogModel.getType() == CallLogModel.TYPE_INCOMING) {
                    // This is Miss Call
                    mCallLogManager.updateCallLogType(mCurrCallLogModel.getDbId(), CallLogModel.TYPE_MISSCALL);
                }
            } else {
                // Update CallLog duration
                mCallLogManager.updateCallLogDuration(mCurrCallLogModel.getDbId(), (int)((endTime - startTime) / 1000));
            }
        }

        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callEnded(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
        canResume = false;
    }

    @Override
    public void onCallEstablished(SipCall call) {
        if (mCurrSipCall != call) {
            return;
        }
        // Stop all signal
        mQilexSoundService.stopAllSignal();
        // Config AudioManager
        if (!android.os.Build.PRODUCT.equals("U9202L")) {
            // If device is not Huawei Ascend P1 (need check because
            // MODE_IN_COMMUNICATION seems buggy on model Huawei Ascend P1).
            // Needed for some newer devices like Xperia Z / Xperia SP
            getAudioManager().setMode(MODE_IN_COMMUNICATION);
        } else {
            getAudioManager().setMode(MODE_NORMAL);
        }
        getAudioManager().setSpeakerphoneOn(call.isSpeakerOn());
        requestAudioFocus("onCallEstablished");
        // Start calling audio
        call.startAudio();
        // Check duration
        mCurrSipCall.setStartTimeMillis(Calendar.getInstance().getTimeInMillis());

        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callEstablished(mCurrSipCall, false);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public long getDuration() {
        return mCurrSipCall.getCurrCallTimeMillis();
    }

    static boolean canResume = false;

    public void pauseCall() {
        if (!canResume && mCurrSipCall != null)
            mCurrSipCall.pauseCall();
        canResume = true;
    }

    public void resumeCall() {
        if (canResume && mCurrSipCall != null) {
            mCurrSipCall.resumeCall();
            canResume = false;
        }
    }

    public static boolean isCanResume() {
        return (canResume && mInstance != null && mInstance.mCurrSipCall != null);
    }

    @Override
    public void onCallCalling(SipCall call) {
        if (mCurrSipCall != call) {
            return;
        }
        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callCalling(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onCallRingingBack(SipCall call) {
        if (mCurrSipCall != call) {
            return;
        }
        // cancelRequestAudio("onCallRingingBack");
        // mSoundService.stopAllSignals();
        // Start call signal
        // boolean signalStarted =
        // mSoundService.genCallingRingingBackSignal();
        // requestAudioFocus("onCallRingingBack");
        // Fire handlers
        mQilexSoundService.stopAllSignal();
        mQilexSoundService.startCallRingingBackSignal();

        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callRingingBack(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onCallReconnect(SipCall call) {
        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callReconnect(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onCallReconnectSuccess(SipCall call) {
        if (mCurrSipCall != call) {
            return;
        }
        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callReconnectSuccess(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onCallError(SipCall call, int errCode, String errMsg) {
        LogUtils.d("SIGMA", "SIGMA-PHUCPV debug function call, onCallError");

        // Delay for 200 milliseconds
        SystemClock.sleep(200);

        if (mCurrSipCall != call) {
            return;
        }
        getAudioManager().setMode(AudioManager.MODE_NORMAL);
        cancelRequestAudio("onCallError");
        mQilexSoundService.stopAllSignal();
        mQilexSoundService.playCallFailSignal();

        // Push notification on Notification Bar
        // TODO: Add notification

        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callError(mCurrSipCall, errCode, errMsg);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onCallClosed(SipCall call) {
        LogUtils.d("SIGMA", "SIGMA-PHUCPV debug function call, onCallClosed");
        QilexService qilexService = BaseActivity.getService();
        if (qilexService != null) {
            qilexService.getNotificationFactory().hideOutgoingNotification();
        }

        if (mCurrSipCall != call) {
            return;
        }

        getAudioManager().setSpeakerphoneOn(false);
        getAudioManager().setMode(AudioManager.MODE_NORMAL);
        cancelRequestAudio("onCallClosed");
        mQilexSoundService.stopAllSignal();

        mIsCallInProgress = false;
        // Fire handlers
        for (Iterator<QilexSipCallServiceListener> i = mQilexSipCallServiceListener.iterator(); i.hasNext();) {
            QilexSipCallServiceListener l = null;
            try {
                l = i.next();
                l.callClosed(mCurrSipCall);
            } catch (Throwable t) {
            }
        }
        // END AND CLOSE BEFORE CALL --> relogin
        if (isJustLoginSIP) {
            if (qilexService != null) {
                qilexService.scheduleLogin(100);// 100ms
            }
        }
    }

    @Override
    public void onTurnNotAvailable(SipCall call) {
        LogUtils.d("SIGMA", "SIGMA-PHUCPV debug function call, onTurnNotAvailable");

        // TODO mCallController.incomingCallPaymentWaring();
    }

    // /
    // / IMPLEMENT METHOD OF SipIncomingCallListener - START
    // /
    @Override
    public void incomingCall(SipCall call) {
        vrcDispatcher.removeMessages(MSG_END_PUSH_CALL);
        TelephonyManager tm = (TelephonyManager)MyApplication.getAppContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        int callState = tm.getCallState();

        // gms: go to busy
        // rejectCall;
        // Check if other call is processing.
        if (mIsCallInProgress == true || (callState == TelephonyManager.CALL_STATE_OFFHOOK)
                || (callState == TelephonyManager.CALL_STATE_RINGING)) {
            // Save this call to history
            String phoneNo = call.getPeerProfile().getLogin();
            mCallLogManager.putNewCallLog(phoneNo, CallLogModel.TYPE_MISSCALL);
            // End this call
            callReject(call);
            return;
        }
        mIsCallInProgress = true;
        // Set mode for Audio Manager
        // getAudioManager().setMode(AudioManager.MODE_RINGTONE);
        requestAudioFocus("incomingCall");
        call.setSpeakerOn(false);
        call.setMute(false);
        call.setListener(this);
        mCurrSipCall = call;

        String phoneNo = call.getPeerProfile().getLogin();

        // Save this call to history
        mCurrCallLogModel = mCallLogManager.putNewCallLog(phoneNo, CallLogModel.TYPE_INCOMING);

        // Show incoming call GUI
        QilexService qilexService = BaseActivity.getService();
        if (qilexService != null) {
            qilexService.showIncomingSipCallGUI(call.getPeerProfile());
        } else {
            Intent intent = new Intent(MyApplication.getAppContext(), QilexService.class);
            intent.putExtra(QilexService.INTENT_PARAM_ACTION, QilexService.ActionEnum.ACTION_OPEN_INCOMING_CALL);
            MyApplication.getAppContext().startService(intent);
        }
        // Ringtone
        vrcDispatcher.removeMessages(MSG_PLAYRINGTONE);
        vrcDispatcher.sendEmptyMessageDelayed(MSG_PLAYRINGTONE, 2000);

        mQilexSoundService.startIncomingCallRing();
    }

    public boolean incomingCallFromPush(String peerNumber) {
        // Is handling other push
        if (mPushPhoneNumber != null) {
            mCallLogManager.putNewCallLog(peerNumber, CallLogModel.TYPE_MISSCALL);
            return false;
        }

        // Has other call in progress
        if (mIsCallInProgress == true) {
            mCallLogManager.putNewCallLog(peerNumber, CallLogModel.TYPE_MISSCALL);
            return false;
        }
        mPushPhoneNumber = peerNumber;
        mPushPhoneState = PUSH_STATE_INCOMING;

        // Save this call to history
        mCurrCallLogModel = mCallLogManager.putNewCallLog(peerNumber, CallLogModel.TYPE_INCOMING);

        // Run sound
        vrcDispatcher.removeMessages(MSG_PLAYRINGTONE);
        vrcDispatcher.sendEmptyMessageDelayed(MSG_PLAYRINGTONE, 2000);

        // Stop after 10s
        vrcDispatcher.removeMessages(MSG_END_PUSH_CALL);
        vrcDispatcher.sendEmptyMessageDelayed(MSG_END_PUSH_CALL, 10000);

        return true;
    }

    public void stopRinging() {
        mQilexSoundService.stopIncomingCallRing();
    }

    public int getKATZOutCallHistory(int page, int onPage, final GetKATZOutCallHistoryListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());

                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                OutCallListHistoryModel model = new OutCallListHistoryModel(response);

                                listener.onGetKATZOutCallFinish(model, null);
                            } catch (JSONException e) {
                                ErrorModel model = new ErrorModel(message);
                                listener.onGetKATZOutCallFinish(null, model);
                            }
                        } else {
                            ErrorModel errorServer = new ErrorModel();
                            listener.onGetKATZOutCallFinish(null, errorServer);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errorNetwork = new ErrorModel();
                        listener.onGetKATZOutCallFinish(null, errorNetwork);
                        break;
                }
            }
        };

        QilexHttpRequest request = AccountBalanceUtils
                .getOutCallHistory(UserInfo.getInstance().getPhoneNumberFormatted(), page, onPage, httpRequestListener);
        return request.getRequestId();
    }

    public void setJustLoginSIP(boolean isRegister) {
        this.isJustLoginSIP = isRegister;
    }

    public interface QilexSipCallServiceListener {
        public void callBusy(SipCall call);

        public void callEnded(SipCall call);

        public void callEstablished(SipCall call, boolean isPushCall);

        public void callCalling(SipCall call);

        public void callRingingBack(SipCall call);

        public void callReconnect(SipCall call);

        public void callReconnectSuccess(SipCall call);

        public void callError(SipCall call, int errCode, String errMsg);

        public void callClosed(SipCall call);

        public void turnNotAvailable(SipCall call);
    }

    public interface GetKATZOutCallHistoryListener {
        public void onGetKATZOutCallFinish(OutCallListHistoryModel result, ErrorModel error);
    }

}
