
package sigma.qilex.manager.phone;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;

import android.content.Context;
import android.os.AsyncTask;
import pl.katz.aero2.MyApplication;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.dataaccess.sqlitedb.CallLogDb;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.utils.SharedPrefrerenceFactory;

public class CallLogManager {
    private static CallLogManager mInstance;

    public static final synchronized CallLogManager getInstance() {
        if (mInstance == null) {
            mInstance = new CallLogManager(MyApplication.getAppContext());
        }
        return mInstance;
    }

    private CallLogDb mCallLogDb;

    private Vector<CallLogChangeListener> listeners = new Vector<CallLogChangeListener>();

    private SharedPrefrerenceFactory mSharedPrefrerenceFactory;

    private CallLogManager(Context context) {
        mCallLogDb = CallLogDb.getInstance();
        mSharedPrefrerenceFactory = SharedPrefrerenceFactory.getInstance();
    }

    public CallLogModel putNewCallLog(String phoneNo, int type) {
        String label = phoneNo;
        long dateMiliSecond = Calendar.getInstance().getTimeInMillis();

        CallLogModel model = new CallLogModel(-1, type, label, dateMiliSecond, 0);
        long id = mCallLogDb.putCallLog(model);
        if (id > -1) {
            if (type == CallLogModel.TYPE_MISSCALL) {
                mSharedPrefrerenceFactory.increaseMissCallNum();

                // Put message
                ChatManager.getInstance().putCallMessage(phoneNo, ChatDatabase.MSG_TYPE_CALLLOG,
                        ChatDatabase.MSG_MINETYPE_MISSCALL);
            }
            fireNewCallLogListener(model);
        }

        return model;
    }

    /**
     * Asynchronous loading all CallLog or miss CallLog.
     * 
     * @param isAll true if load all, false if load misscall only.
     * @param limit number of misscall will be loaded
     * @param listener result will be returned in this handler.
     * @return the AsynTask that execute this method.
     */
    public AsyncTask<Integer, Integer, LoadCallLogResult> loadAllCallLog(final int offset, final int limit,
            final LoadCallLogListener listener) {
        AsyncTask<Integer, Integer, LoadCallLogResult> task = new AsyncTask<Integer, Integer, LoadCallLogResult>() {
            @Override
            protected LoadCallLogResult doInBackground(Integer... params) {
                listener.onLoadCallLogStart();
                LoadCallLogResult result = new LoadCallLogResult();
                result.missCall = getAllMissCallLog(offset, limit);
                result.allCall = getAllCallLog(offset, limit);
                return result;
            }

            @Override
            protected void onPostExecute(LoadCallLogResult result) {
                if (listener != null) {
                    listener.onLoadCallLogFinish(result.allCall, result.missCall);
                }
            }
        };
        task.execute();
        return task;
    }

    public ArrayList<CallLogModel> getAllCallLog(int offset, int limit) {
        return mCallLogDb.findCallLogAll(offset, limit);
    }

    public ArrayList<CallLogModel> getAllMissCallLog(int offset, int limit) {
        return mCallLogDb.findCallLogByType(offset, limit, CallLogModel.TYPE_MISSCALL);
    }

    public ArrayList<String> getMissCallPhoneNumbers(int limit) {
        return mCallLogDb.getMissCallPhoneNumbers(limit);
    }

    public ArrayList<CallLogModel> getCallLogByPhoneNumbers(int limit, String... phoneNums) {
        return mCallLogDb.findCallLogByLabel(limit, phoneNums);
    }

    public void deleteCallLogs(long... ids) {
        mCallLogDb.deleteCallLogByIds(ids);
        fireCallLogDeletedListener(ids);
    }

    public void updateCallLogType(long id, int type) {
        mCallLogDb.updateCallLogType(id, type);
        if (type == CallLogModel.TYPE_MISSCALL) {
            mSharedPrefrerenceFactory.increaseMissCallNum();
            CallLogModel callLog = mCallLogDb.findCallLogById(id);
            // Put message
            ChatManager.getInstance().putCallMessage(callLog.getLabel(), ChatDatabase.MSG_TYPE_CALLLOG,
                    ChatDatabase.MSG_MINETYPE_MISSCALL);
        }
        fireCallLogDurationChangeListener(mCallLogDb.findCallLogById(id));
    }

    /**
     * Update CallLog Duration
     * 
     * @param id sqlite db
     * @param duration duration in second.
     */
    public void updateCallLogDuration(long id, int duration) {
        mCallLogDb.updateCallLogDuration(id, duration);
        fireCallLogDurationChangeListener(mCallLogDb.findCallLogById(id));
    }

    public int countMissCall() {
        return mSharedPrefrerenceFactory.getMissCallNotViewed();
    }

    public int countCallLog(String... phoneNums) {
        return mCallLogDb.countCallLogByLabel(phoneNums);
    }

    public void setMissCallNumToZero() {
        mSharedPrefrerenceFactory.setMissCallNumToZero();
    }

    public void addCallLogChangeListener(CallLogChangeListener listener) {
        if (listeners.contains(listener) == false) {
            listeners.addElement(listener);
        }
    }

    public void removeCallLogChangeListener(CallLogChangeListener listener) {
        listeners.removeElement(listener);
    }

    private void fireNewCallLogListener(CallLogModel model) {
        for (Iterator<CallLogChangeListener> i = listeners.iterator(); i.hasNext();) {
            CallLogChangeListener l = null;
            try {
                l = i.next();
                l.onNewCallLog(model);
            } catch (Throwable t) {
            }
        }
    }

    private void fireCallLogDurationChangeListener(CallLogModel model) {
        for (Iterator<CallLogChangeListener> i = listeners.iterator(); i.hasNext();) {
            CallLogChangeListener l = null;
            try {
                l = i.next();
                l.onCallLogChange(model);
            } catch (Throwable t) {
            }
        }
    }

    private void fireCallLogDeletedListener(long[] callLogIds) {
        for (Iterator<CallLogChangeListener> i = listeners.iterator(); i.hasNext();) {
            CallLogChangeListener l = null;
            try {
                l = i.next();
                l.onCallLogDeleted(callLogIds);
            } catch (Throwable t) {
            }
        }
    }

    public interface LoadCallLogListener {
        public void onLoadCallLogStart();

        public void onLoadCallLogFinish(ArrayList<CallLogModel> listAllCall, ArrayList<CallLogModel> listMissCall);
    }

    public class LoadCallLogResult {
        ArrayList<CallLogModel> missCall;

        ArrayList<CallLogModel> allCall;
    }
}
