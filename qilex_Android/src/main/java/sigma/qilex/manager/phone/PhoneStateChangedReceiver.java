
package sigma.qilex.manager.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import sigma.qilex.ui.activity.InCallActivity;

/**
 * Pause current SIP calls when GSM phone rings or is active.
 * 
 * @author namnd
 */
public class PhoneStateChangedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        final String extraState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        CallManager callManager = CallManager.getInstance();
        if (callManager == null) {
            return;
        }
        if (TelephonyManager.EXTRA_STATE_RINGING.equals(extraState)) {
            callManager.muteOn();
        }
        if (TelephonyManager.EXTRA_STATE_IDLE.equals(extraState)) {
            callManager.muteOff();
        }
        // answer gsm
        if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(extraState)) {
            if (!callManager.isCallInProgress()) {
                return;
            }
            Intent endCall = new Intent(InCallActivity.ACTION_ENDCALL);
            endCall.putExtra(InCallActivity.INTENT_ERROR_CODE, InCallActivity.END_CALL_CODE);
            context.sendBroadcast(endCall);
        }
    }

}
