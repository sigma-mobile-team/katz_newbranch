
package sigma.qilex.manager.phone;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import sigma.qilex.utils.LogUtils;

public class InCallWakeLock {

    public static final int WAKE_STATE_PARTIAL = 0;

    public static final int WAKE_STATE_FULL = 1;

    protected static final int PROXIMITY_SCREEN_OFF_WAKE_LOCK = 32;

    public static final String DBG_TAG = "InCallWakeLock";

    private PowerManager mPm;

    protected WakeLock mWakeLock;

    protected WakeLock mPartialWakeLock;

    protected WakeLock mProximityWakeLock;

    protected boolean mIsProximityWakeLockSupported;

    protected int mWakeState;

    @SuppressWarnings("deprecation")
    public InCallWakeLock(Context context) {
        mPm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = mPm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, DBG_TAG);
        mPartialWakeLock = mPm.newWakeLock(PROXIMITY_SCREEN_OFF_WAKE_LOCK, DBG_TAG);
        try {
            Method method = mPm.getClass().getDeclaredMethod("getSupportedWakeLockFlags");
            int supportedFlags = (Integer)method.invoke(mPm);

            Field f = PowerManager.class.getDeclaredField("PROXIMITY_SCREEN_OFF_WAKE_LOCK");
            int proximityScreenOffWakeLock = (Integer)f.get(null);
            if ((supportedFlags & proximityScreenOffWakeLock) != 0x0) {
                mProximityWakeLock = mPm.newWakeLock(proximityScreenOffWakeLock, DBG_TAG);
                mProximityWakeLock.setReferenceCounted(false);
                mIsProximityWakeLockSupported = true;
            }
        } catch (Throwable t) {
            LogUtils.e(DBG_TAG, "getSupportedWakeLockFlags err");
        }
    }

    public boolean isSupported() {
        return mIsProximityWakeLockSupported;
    }

    public void requestWakeState(int wakeState) {

        synchronized (this) {
            if (mWakeState != wakeState) {
                switch (wakeState) {
                    case WAKE_STATE_PARTIAL:
                        if (mWakeLock.isHeld()) {
                            mWakeLock.release();
                        }
                        if (mPartialWakeLock.isHeld() == false) {
                            mPartialWakeLock.acquire();
                        }

                        break;
                    case WAKE_STATE_FULL:
                        if (mPartialWakeLock.isHeld()) {
                            mPartialWakeLock.release();
                        }
                        if (mIsProximityWakeLockSupported) {
                            if (mProximityWakeLock.isHeld()) {
                                mProximityWakeLock.release();
                            }
                        }
                        if (mWakeLock.isHeld() == false) {
                            mWakeLock.acquire();
                        }
                        break;
                    default:
                        if (mWakeLock.isHeld()) {
                            mWakeLock.release();
                        }
                        if (mPartialWakeLock.isHeld()) {
                            mPartialWakeLock.release();
                        }
                        if (mIsProximityWakeLockSupported) {
                            if (mProximityWakeLock.isHeld() == true) {
                                mProximityWakeLock.release();
                            }
                        }
                        break;
                }
                mWakeState = wakeState;
            }
        }
    }

    public synchronized void release() {
        // WAKE_STATE_FULL when destroy
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire(500);
        }
        if (mPartialWakeLock.isHeld()) {
            mPartialWakeLock.release();
        }
        if (mIsProximityWakeLockSupported) {
            if (mProximityWakeLock.isHeld()) {
                mProximityWakeLock.release();
            }
        }
    }

    @SuppressWarnings("deprecation")
    public synchronized boolean isScreenOn() {
        return mPm.isScreenOn();
    }
}
