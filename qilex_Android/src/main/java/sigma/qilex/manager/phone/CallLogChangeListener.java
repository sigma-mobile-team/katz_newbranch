
package sigma.qilex.manager.phone;

import sigma.qilex.dataaccess.model.CallLogModel;

/**
 * Action handler of event Calllog change.
 */
public interface CallLogChangeListener {

    public void onNewCallLog(CallLogModel callLog);

    public void onCallLogChange(CallLogModel callLog);

    public void onCallLogDeleted(long[] callLogIds);
}
