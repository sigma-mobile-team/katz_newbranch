
package sigma.qilex.manager.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import sigma.qilex.manager.QilexSoundService;
import sigma.qilex.manager.phone.CallManager.CallActivityEvent;
import sigma.qilex.sip.SipCall;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;

public class CallReceiver extends BroadcastReceiver {
    final public static String DBG_TAG = "CallReceiver";

    private QilexService mQilexService;

    private LocalBroadcastManager mLocalBroadcastManager;

    public CallReceiver(QilexService qservice) {
        mQilexService = qservice;
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(mQilexService);
        mLocalBroadcastManager.registerReceiver(this, new IntentFilter("qilex.CallActivity"));
    }

    public void unregisterReceiver() {
        mLocalBroadcastManager.unregisterReceiver(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final Intent i = intent;
        new Thread(new Runnable() {
            @Override
            public void run() {
                CallActivityEvent event = (CallActivityEvent)i.getSerializableExtra("qilex_event");
                CallManager cm = mQilexService.getCallManager();
                SipCall call = cm.getCurrCall();
                if (call == null) {
                    LogUtils.e(DBG_TAG, "onReceive call = null");
                    return;
                }
                LogUtils.e(DBG_TAG, "onReceive event = " + event.name());
                switch (event) {
                    case CallReject:
                    case CallEnd:
                        call.endCall();
                        break;
                    case CallAnswer:
                        try {
                            QilexSoundService.getInstance().stopAllSignal();
                            call.answerCall(3000);
                        } catch (Exception e) {
                            LogUtils.e(DBG_TAG, "CallAnswer: " + e.toString());
                        }
                        break;
                    case CallWithoutTurn:
                        try {
                        } catch (Exception e) {
                            LogUtils.e(DBG_TAG, "CallWithoutTurn: " + e.toString());
                        }
                        break;
                    case CallDivert:
                        // mDivertingCallNumber =
                        // i.getStringExtra("phoneNumber");
                        call.endCall();
                        break;
                    case SpeakerOn:
                        call.setSpeakerOn(true);
                        mQilexService.setAudioManagerSpeakerOn(true);
                        break;
                    case SpeakerOff:
                        call.setSpeakerOn(false);
                        mQilexService.setAudioManagerSpeakerOn(false);
                        break;
                    case HeadphonesOn:
                        LogUtils.e(DBG_TAG, "HeadphonesOn");
                        call.setHeadphonesOn(true);
                        break;
                    case HeadphonesOff:
                        LogUtils.e(DBG_TAG, "HeadphonesOff");
                        call.setHeadphonesOn(false);
                        break;
                    case AudioVolume:
                        float volume = i.getFloatExtra("volume", 1.0f);
                        LogUtils.e(DBG_TAG, "AudioVolume = " + volume);
                        call.setAudioVolume(volume);
                        break;
                    case MuteOn:
                        call.setMute(true);
                        break;
                    case MuteOff:
                        call.setMute(false);
                        break;
                    default:
                        break;
                }
            }
        });
    }
}
