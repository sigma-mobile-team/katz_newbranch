
package sigma.qilex.manager.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import sigma.qilex.manager.phone.CallManager.CallActivityEvent;
import sigma.qilex.utils.LogUtils;

/**
 * BroadcastReceiver checking the condition of headphones
 */
public class HeadphonesConnectedBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_HEADSET_PLUG) == false) {
            return;
        }

        Bundle bundle = intent.getExtras();

        String name = bundle.getString("name");
        int state = bundle.getInt("state", -1);
        int microphone = bundle.getInt("microphone", -1);

        LogUtils.e("HeadphonesConnectedBroadcastReceiver", "name = " + name);
        LogUtils.e("HeadphonesConnectedBroadcastReceiver", "state = " + state);
        LogUtils.e("HeadphonesConnectedBroadcastReceiver", "microphone = " + microphone);

        if (state == 0) {
            notifyHeadPhonesOff(context);
        } else {
            notifyHeadPhonesOn(context);
        }

    }

    private void notifyHeadPhonesOn(Context context) {
        Intent intent = new Intent("qilex.CallActivity");
        intent.putExtra("qilex_event", CallActivityEvent.HeadphonesOn);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void notifyHeadPhonesOff(Context context) {
        Intent intent = new Intent("qilex.CallActivity");
        intent.putExtra("qilex_event", CallActivityEvent.HeadphonesOff);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
