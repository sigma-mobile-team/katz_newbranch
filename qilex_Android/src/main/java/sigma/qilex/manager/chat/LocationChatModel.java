
package sigma.qilex.manager.chat;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

import pl.katz.aero2.Const;
import sigma.qilex.utils.Utils;

public class LocationChatModel {

    public double latitude;

    public double longitude;

    public String thumb_uri = Const.STR_EMPTY;

    public String token = Const.STR_EMPTY;

    public String description = Const.STR_EMPTY;

    public String sub_description = Const.STR_EMPTY;

    public LocationChatModel(double latitude, double longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = address;
    }

    public LocationChatModel(String strData) throws JSONException {
        this(new JSONObject(strData));
    }

    public LocationChatModel(JSONObject jsonData) throws JSONException {
        // Image thumbnail uri
        thumb_uri = jsonData.has("thumb_uri") ? jsonData.getString("thumb_uri") : Const.STR_EMPTY;
        if (Utils.isStringNullOrEmpty(thumb_uri) == false) {
            if (thumb_uri.startsWith("/") == true) {
                thumb_uri = thumb_uri.substring(1);
            }
        }

        // token
        token = jsonData.has("token") ? jsonData.getString("token") : Const.STR_EMPTY;
        description = jsonData.has("title") ? jsonData.getString("title") : Const.STR_EMPTY;
        sub_description = jsonData.has("subtitle") ? jsonData.getString("subtitle") : Const.STR_EMPTY;
        latitude = jsonData.has("latitude") ? jsonData.getDouble("latitude") : 0;
        longitude = jsonData.has("longitude") ? jsonData.getDouble("longitude") : 0;
    }

    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("thumb_uri", "/" + thumb_uri.replace(Const.SERVER_URL, Const.STR_EMPTY));
            obj.put("token", token);
            obj.put("title", description);
            obj.put("subtitle", sub_description);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);
        } catch (JSONException e) {
        }
        return obj;
    }

    public LatLng getLatLng() {
        return new LatLng(latitude, longitude);
    }
}
