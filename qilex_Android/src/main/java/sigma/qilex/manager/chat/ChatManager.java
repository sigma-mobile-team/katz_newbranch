
package sigma.qilex.manager.chat;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TimeZone;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.ChatThreadModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.ImageWriteModel;
import sigma.qilex.dataaccess.network.http.ImageHttpUtils;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest.QilexHttpRequestListener;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipManager;
import sigma.qilex.sip.SipMessage;
import sigma.qilex.sip.SipMessage.GroupChatType;
import sigma.qilex.sip.resiprocate.Events;
import sigma.qilex.sip.SipMessageListener;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ChatManager implements SipMessageListener {

    private static final String DEFAULT_XML_STRING_FORMAT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><isComposing xmlns=\"urn:ietf:params:xml:ns:im-iscomposing\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:ietf:params:xml:ns:im-composing iscomposing.xsd\"> <state>%s</state> <refresh>%s</refresh> </isComposing>";

    private static final String STR_ACTIVE = "active";

    private static final String STR_IDLE = "idle";

    private static final String REFRESH_TIMEOUT_SEC = "60";

    public static final int MSG_ACTIVE_ID = -1000;

    public static final int MSG_IDLE_ID = -1001;

    public static final int MSG_VIEWED_ID = -1100;

    private static ChatManager mInstance;

    public static final synchronized ChatManager getInstance() {
        if (mInstance == null) {
            mInstance = new ChatManager();
        }
        return mInstance;
    }

    private SipManager mSipManager;

    private Vector<SipChatListener> mListSipChatListener;

    private Vector<SipChatComposingListener> mListSipChatComposingListener;

    private Vector<ChatThreadChangeListener> mListChatThreadChangeListener;

    private Vector<ChatMessageChangeListener> mListChatMessageChangeListener;

    private ChatDatabase mDb;

    private HashMap<Long, ChatMessageModel> mUnsendChatMessage = new HashMap<>();

    private Hashtable<Long, ChatThreadModel> mAllChatThread = new Hashtable<>();

    private ChatMessageModel mNewestMessage;

    /**
     * Hashmap store all message downloading image. Key is the token of image
     */
    private Hashtable<String, ChatMessageModel> mDownloadingImageMessage;
    
    private ProcessingStatusMessageThread statusMessageThread;

    private ChatManager() {
        // Initialize singleton object
        mSipManager = SipManager.getInstance(MyApplication.getAppContext());
        mListSipChatListener = new Vector<ChatManager.SipChatListener>();
        mListChatThreadChangeListener = new Vector<ChatManager.ChatThreadChangeListener>();
        mListChatMessageChangeListener = new Vector<ChatManager.ChatMessageChangeListener>();
        mListSipChatComposingListener = new Vector<ChatManager.SipChatComposingListener>();
        mDb = ChatDatabase.getInstance();
        mUnsendChatMessage = new HashMap<Long, ChatMessageModel>();
        mDownloadingImageMessage = new Hashtable<>();
        mDownloadImageListener = new HashMap<>();

        // Set action listener
        mSipManager.setMessageListener(this);
        
        // start Thread check
        if (statusMessageThread != null) {
        	statusMessageThread.stopRunning();
        }
        statusMessageThread = new ProcessingStatusMessageThread();
        statusMessageThread.startRunning();
    }

    /**
     * Send a sip message.
     * 
     * @param sipAddress the SIP address of receive account.
     * @param text message content
     * @param specialData special message like: send location, send Image, ...
     * @return a SipMessage.
     */
    public synchronized SipMessage sendSipMessage(long id, String sipAddress, String text,
            MessageSpecialData specialData, boolean isGroupChat) {
        boolean hasError = false;
        // Get user local profile
        QilexProfile qp = UserInfo.getInstance().getLocalProfile();
        if (qp == null) {
            hasError = true;
        }
        QilexSipProfile userProfile = qp.getDefaultSipProfile();
        if (hasError == false && userProfile == null) {
            hasError = true;
        }
        // Check if peer profile is current user
        if (hasError == false && userProfile.getLogin().equals(sipAddress)) {
            hasError = true;
        }

        if (hasError == false && NetworkStateMonitor.getNetworkState() == NetworkStateMonitor.NO_NETWORK) {
            hasError = true;
        }

        int status = ChatDatabase.MSG_STATUS_SEND_SENDING;
        if (hasError == true) {
            status = ChatDatabase.MSG_STATUS_SEND_FAIL;
        }

        // Create Message on Sqlite
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        ChatMessageModel chatMessage = null;
        ChatThreadModel chatThread;
    	String contactStr = isGroupChat ? Const.STR_EMPTY : sipAddress;
        if (id > -1) {
            chatMessage = mDb.findChatMessageById(id);
        }
        if (chatMessage == null) {
            chatMessage = new ChatMessageModel(-1, -1, contactStr, ChatDatabase.MSG_TYPE_SEND,
                    ChatDatabase.MSG_MINETYPE_TEXT, status, cal.getTimeInMillis(), text, cal.getTimeInMillis());
            if (isGroupChat) {
            	chatMessage.groupUuid = sipAddress;
//            	chatMessage.status = ChatDatabase.MSG_STATUS_SEND_SENT;
            }
            if (specialData != null && specialData.thumbnail != null) {
                chatMessage.imageThumbnail = specialData.thumbnail;
                chatMessage.type = specialData.dataType;
            }
            chatThread = mDb.putChatMsg(chatMessage);
        } else {
            chatThread = mDb.findMsgThreadById(chatMessage.threadId);
//            if (isGroupChat) {
////            	mDb.updateChatMsgStatusAndContent(chatMessage, ChatDatabase.MSG_STATUS_SEND_SENT, text);
//            } else {
            	mDb.updateChatMsgStatusAndContent(chatMessage, ChatDatabase.MSG_STATUS_SEND_SENDING, text);
//            }
        }

        // Create peer profile
        QilexSipProfile peerProfile = new QilexSipProfile();
        peerProfile.setLogin(sipAddress);
        if (userProfile != null) {
            peerProfile.setServer(userProfile.getServer());
        }

        // Create SIP Message
        // Send
        SipMessage msg = new SipMessage(text);
        msg.setTo(peerProfile);
        msg.setTimestamp(chatMessage.dateMillis / 1000);
        msg.setId(chatMessage.id);
        msg.setUuid(chatMessage.uuid);
        if (specialData != null) {
            msg.setType(specialData.toSipType());
        } else {
            msg.setType(SipMessage.TYPE_MESSAGE_TEXT);
        }
        if (isGroupChat) {
        	msg.setGroupChatUuid(sipAddress);
        	msg.setGroupChatMsgType(SipMessage.GroupChatType.MESSAGE);
        }
        if (hasError == false) {
            msg.setFrom(qp.getDefaultSipProfile());
            mSipManager.sendMessage(msg);
        }

        // Update cache data
        if (mAllChatThread != null) {
            mAllChatThread.put(chatThread.id, chatThread);
        }

        // Add to hashmap
        mUnsendChatMessage.put(chatMessage.id, chatMessage);

        // fire event
        fireNewMessageHandler(chatMessage);
        fireThreadChangeHandler(chatThread);

        return msg;
    }

    public synchronized SipMessage sendComposingMessage(String sipAddress, boolean isActive, boolean isGroupChat) {
        Calendar cal = Calendar.getInstance();
        // Get user local profile
        QilexProfile qp = UserInfo.getInstance().getLocalProfile();
        if (qp == null) {
            return null;
        }
        QilexSipProfile userProfile = qp.getDefaultSipProfile();
        if (userProfile == null) {
            return null;
        }
        // Check if peer profile is current user
        if (userProfile.getLogin().equals(sipAddress)) {
            return null;
        }

        // Create peer profile
        QilexSipProfile peerProfile = new QilexSipProfile();
        peerProfile.setLogin(sipAddress);
        peerProfile.setServer(userProfile.getServer());

        // Create SIP Message
        String status = isActive ? STR_ACTIVE : STR_IDLE;
        String content = String.format(DEFAULT_XML_STRING_FORMAT, status, REFRESH_TIMEOUT_SEC);
        SipMessage msg = new SipMessage(content);
        msg.setFrom(qp.getDefaultSipProfile());
        msg.setTo(peerProfile);
        msg.setTimestamp(cal.getTimeInMillis() / 1000);
        msg.setType(SipMessage.TYPE_COMPOSING);
        if (isGroupChat) {
        	msg.setGroupChatMsgType(SipMessage.GroupChatType.MESSAGE);
        	msg.setGroupChatUuid(sipAddress);
        }

        int id = isActive ? MSG_ACTIVE_ID : MSG_IDLE_ID;
        msg.setId(id);

        // Send
        mSipManager.sendMessage(msg);

        return msg;
    }
    
    /**
     * 
     * @param groupChatUuid the group chat Uuid, <code>null</code> means create new group chat
     * @param id
     * @param phoneNumber
     * @return Group Chat UUID
     */
    public synchronized String sendInviteGroupChat(String groupChatUuid, long id, String ...phoneNumber) {
    	boolean hasError = false;
    	// Get user local profile
        QilexProfile qp = UserInfo.getInstance().getLocalProfile();
        if (qp == null) {
            hasError = true;
        }
        QilexSipProfile userProfile = qp.getDefaultSipProfile();
        if (hasError == false && userProfile == null) {
            hasError = true;
        }
        
        boolean isCreateNew = false;
        if (Utils.isStringNullOrEmpty(groupChatUuid)) {
        	groupChatUuid = generateGroupChatUuid();
        	isCreateNew = true;
        }
        // Create peer profile
        QilexSipProfile peerProfile = new QilexSipProfile();
        peerProfile.setLogin(groupChatUuid);
        if (userProfile != null) {
            peerProfile.setServer(userProfile.getServer());
        }
        
        ArrayList<String> listContactExceptYou = new ArrayList<>();
        for (String phone : phoneNumber) {
			if (!UserInfo.getInstance().getPhoneNumberFormatted().equals(phone) && !listContactExceptYou.contains(phone)) {
				listContactExceptYou.add(phone);
			}
		}
        if (listContactExceptYou.size() == 0) {
        	return null;
        }
        
        long timeStamp = Calendar.getInstance().getTimeInMillis();
        String [] arrContactExceptYou = new String[listContactExceptYou.size()];
        arrContactExceptYou = listContactExceptYou.toArray(arrContactExceptYou);
        SipMessage msg = new SipMessage(buildListUriContent(arrContactExceptYou));
        msg.setFrom(qp.getDefaultSipProfile());
        msg.setTo(peerProfile);
        msg.setTimestamp(timeStamp);
        msg.setId(100);
        msg.setType(SipMessage.TYPE_MESSAGE_TEXT);
        msg.setUuid(Long.toString(timeStamp));
        msg.setGroupChatMsgType(GroupChatType.INVITE);
        msg.setGroupChatUuid(groupChatUuid);
        
        // Add new Chat thread
        ChatThreadModel chatThread = mDb.updateChatThread(groupChatUuid, ChatDatabase.MSG_TYPE_GROUP_STATUS,
        		timeStamp, Const.STR_EMPTY, false, ChatDatabase.MSG_MINETYPE_GROUP_INVITE);
        if (isCreateNew) {
        	String administrator = qp.getDefaultSipProfile() == null ? "" : qp.getDefaultSipProfile().getUriString();
        	String participant = "";
        	for (String phoneNo : arrContactExceptYou) {
        		QilexSipProfile peerMember = new QilexSipProfile();
        		peerMember.setLogin(phoneNo);
                participant += peerMember.getUriString() + ",";
			}
        	participant = participant.substring(0, participant.length() - 1);
        	mDb.updateChatThreadGroupData(null, chatThread.id, participant + "\n" + administrator);
        }

        mSipManager.sendMessage(msg);
        
        return groupChatUuid;
    }
    
    /**
     * 
     * @param groupChatUuid the group chat Uuid, <code>null</code> means create new group chat
     * @param id
     * @param phoneNumber
     * @return Group Chat UUID
     */
    public synchronized String sendKickGroupChat(String groupChatUuid, long id, String ...phoneNumber) {
    	boolean hasError = false;
    	// Get user local profile
        QilexProfile qp = UserInfo.getInstance().getLocalProfile();
        if (qp == null) {
            hasError = true;
        }
        QilexSipProfile userProfile = qp.getDefaultSipProfile();
        if (hasError == false && userProfile == null) {
            hasError = true;
        }

        if (hasError == false && NetworkStateMonitor.getNetworkState() == NetworkStateMonitor.NO_NETWORK) {
            hasError = true;
        }
        
        if (hasError) {
        	return null;
        }
        
        // Create peer profile
        QilexSipProfile peerProfile = new QilexSipProfile();
        peerProfile.setLogin(groupChatUuid);
        peerProfile.setServer(userProfile.getServer());
        
        long timeStamp = Calendar.getInstance().getTimeInMillis();
        SipMessage msg = new SipMessage(buildListUriContent(phoneNumber));
        msg.setFrom(qp.getDefaultSipProfile());
        msg.setTo(peerProfile);
        msg.setTimestamp(timeStamp);
        msg.setId(100);
        msg.setType(SipMessage.TYPE_MESSAGE_TEXT);
        msg.setUuid(Long.toString(timeStamp));
        msg.setGroupChatMsgType(GroupChatType.KICK);
        msg.setGroupChatUuid(groupChatUuid);

        mSipManager.sendMessage(msg);
        
        return groupChatUuid;
    }

    public synchronized boolean sendViewedMessage(String sipAddress, String uuid) {
        Calendar cal = Calendar.getInstance();
        // Get user local profile
        QilexProfile qp = UserInfo.getInstance().getLocalProfile();
        if (qp == null) {
            return false;
        }
        QilexSipProfile userProfile = qp.getDefaultSipProfile();
        if (userProfile == null) {
            return false;
        }
        // Check if peer profile is current user
        if (userProfile.getLogin().equals(sipAddress)) {
            return false;
        }

        // Create peer profile
        QilexSipProfile peerProfile = new QilexSipProfile();
        peerProfile.setLogin(sipAddress);
        peerProfile.setServer(userProfile.getServer());

        // Create SIP Message
        String content = uuid;
        SipMessage msg = new SipMessage(content);
        msg.setFrom(qp.getDefaultSipProfile());
        msg.setTo(peerProfile);
        msg.setTimestamp(cal.getTimeInMillis() / 1000);
        msg.setType(SipMessage.TYPE_VIEWED);
        msg.setId(MSG_VIEWED_ID);
        msg.setContent(Const.STR_SPACE);
        msg.setUuid(uuid);

        // Send
        mSipManager.sendMessage(msg);
        mDb.updateChatMsgStatusByUuid(uuid, ChatDatabase.MSG_STATUS_RECEIVE_READ);
        return true;
    }

    public void clearChatHistory() {
        mDb.clearDB();
        if (mAllChatThread != null) {
            mAllChatThread.clear();
        }
    }

    public ChatMessageModel getNewestMessage() {
        return mNewestMessage;
    }

    /**
     * Register a chat handler.
     * 
     * @param sipChatListener
     */
    public void addSipChatListener(SipChatListener sipChatListener) {
        if (mListSipChatListener.contains(sipChatListener) == false) {
            mListSipChatListener.addElement(sipChatListener);
        }
    }

    /**
     * Remove a chat handler.
     * 
     * @param sipChatListener
     */
    public void removeSipChatListener(SipChatListener sipChatListener) {
        mListSipChatListener.removeElement(sipChatListener);
    }

    public void addSipChatComposingListener(SipChatComposingListener listener) {
        if (mListSipChatComposingListener.contains(listener) == false) {
            mListSipChatComposingListener.addElement(listener);
        }
    }

    public void removeSipChatComposingListener(SipChatComposingListener listener) {
    	mListSipChatComposingListener.removeElement(listener);
    }

    public void addChatThreadChangeListener(ChatThreadChangeListener listener) {
        if (mListChatThreadChangeListener.contains(listener) == false) {
            mListChatThreadChangeListener.addElement(listener);
        }
    }

    public void removeChatThreadChangeListener(ChatThreadChangeListener listener) {
        mListChatThreadChangeListener.removeElement(listener);
    }

    public void addChatMessageChangeListener(ChatMessageChangeListener listener) {
        if (mListChatMessageChangeListener.contains(listener) == false) {
            mListChatMessageChangeListener.addElement(listener);
        }
    }

    public void removeChatMessageChangeListener(ChatMessageChangeListener listener) {
        mListChatMessageChangeListener.removeElement(listener);
    }

    /**
     * Load all Chat Thread from Sqlite DB asynchronously, result will be posted
     * into an action listener.
     * 
     * @param listener the listener class that handler result.
     * @return the asyncTask that execute action.
     */
    public AsyncTask<Integer, Integer, ArrayList<ChatThreadModel>> loadAllChatThreadAsynchronous(final int limit,
            final LoadChatThreadListener listener, final boolean isSortByDate) {
        AsyncTask<Integer, Integer, ArrayList<ChatThreadModel>> task = new AsyncTask<Integer, Integer, ArrayList<ChatThreadModel>>() {
            @Override
            protected ArrayList<ChatThreadModel> doInBackground(Integer... params) {
                // Call start listener
                listener.onLoadChatThreadStart();

                ArrayList<ChatThreadModel> result = null;
                // if (mAllChatThread != null) {
                // // If data is loaded before, load cache data from RAM
                // result = new
                // ArrayList<ChatThreadModel>(mAllChatThread.values());
                // } else {
                // // If data is not loaded, load for first time
                result = mDb.findAllMsgThread(limit);
                // }
                // Sort by date if requested
                if (isSortByDate == true) {
                    Collections.sort(result, new ChatThreadModel.DateComparator());
                }
                return result;
            }

            @Override
            protected void onPostExecute(ArrayList<ChatThreadModel> result) {
                listener.onLoadChatThreadFinish(result);

                Hashtable<Long, ChatThreadModel> allChatThread = new Hashtable<Long, ChatThreadModel>();
                for (ChatThreadModel chatThreadModel : result) {
                    allChatThread.put(chatThreadModel.id, chatThreadModel);
                }
                if (mAllChatThread == null) {
                    mAllChatThread = new Hashtable<Long, ChatThreadModel>();
                }
                mAllChatThread.clear();
                mAllChatThread.putAll(allChatThread);
            }
        };
        task.execute();
        return task;
    }

    public int postImageAndThumbnail(Bitmap image, final ImageWriteListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        ErrorModel error = null;
                        ImageWriteModel result = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject jsonData = new JSONObject(message);
                                result = new ImageWriteModel(jsonData);
                            } catch (JSONException e) {
                                error = new ErrorModel(message);
                                result = null;
                            }
                        } else {
                            error = new ErrorModel();
                        }
                        listener.onImageWriteFinish(error, result);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onImageWriteFinish(new ErrorModel(), null);
                        break;
                }
            }
        };

        QilexHttpRequest request = ImageHttpUtils.postImageAndThumbnail(MyApplication.getAppContext(),
                UserInfo.getInstance().getPhoneNumberFormatted(), image, httpRequestListener);
        return request.getRequestId();
    }

    public ChatMessageModel sendImageMessage(final String sipAddress, final String description, final String filePath, final boolean isGroupChat) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        ImageWriteModel imageModel = new ImageWriteModel();
        imageModel.description = description;
        imageModel.image_uri = filePath;
        final ChatMessageModel chatMessage = new ChatMessageModel(-1, -1, sipAddress, ChatDatabase.MSG_TYPE_SEND,
                ChatDatabase.MSG_MINETYPE_IMAGE, ChatDatabase.MSG_STATUS_SEND_PREPARING, cal.getTimeInMillis(),
                imageModel.toJsonObject().toString(), cal.getTimeInMillis());

        final MessageSpecialData imageData = new MessageSpecialData(ChatDatabase.MSG_MINETYPE_IMAGE);
        Bitmap thumb = ImageThumbnailProcessor.generateThumbnail(filePath, -1);
        chatMessage.imageThumbnail = ImageThumbnailProcessor.getBytes(thumb);
        chatMessage.imageUrl = filePath;
        if (isGroupChat) {
        	chatMessage.groupUuid = sipAddress;
        }

        // Add message to DB
        mDb.putChatMsg(chatMessage);

        ImageLoader.getInstance().loadImage("file://" + filePath, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String arg0, View arg1) {
            }

            @Override
            public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
            }

            @Override
            public void onLoadingComplete(String arg0, View arg1, Bitmap bitmap) {
                postImageAndThumbnail(bitmap, new ChatManager.ImageWriteListener() {
                    @Override
                    public void onImageWriteFinish(ErrorModel error, ImageWriteModel imageWriteModel) {
                        if (error == null) {
                            // Send Content
                            imageWriteModel.description = description;
                            JSONObject json = imageWriteModel.toJsonObject();
                            imageData.messageContent = json.toString();
                            sendSipMessage(chatMessage.id, sipAddress, json.toString(), imageData, isGroupChat);
                            ChatMessageModel newChatMsg = mDb.findChatMessageById(chatMessage.id);
                            ArrayList<ChatMessageModel> updates = new ArrayList<>();
                            updates.add(newChatMsg);
                            fireMessageStatusUpdateHandler(updates);
                        } else {
                            updateChatMsgStatus(chatMessage, ChatDatabase.MSG_STATUS_SEND_FAIL);
                            ChatMessageModel newChatMsg = mDb.findChatMessageById(chatMessage.id);
                            ArrayList<ChatMessageModel> updates = new ArrayList<>();
                            updates.add(newChatMsg);
                            fireMessageStatusUpdateHandler(updates);
                        }
                    }
                });
            }

            @Override
            public void onLoadingCancelled(String arg0, View arg1) {
            }
        });

        return chatMessage;
    }

    public ChatMessageModel sendLocationMessage(final String sipAddress, double latitude, double longitude,
            Bitmap snapshot, String address, final boolean isGroupChat) {
        final LocationChatModel locationChat = new LocationChatModel(latitude, longitude, address);
        // Create mesage
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        final ChatMessageModel chatMessage = new ChatMessageModel(-1, -1, sipAddress, ChatDatabase.MSG_TYPE_SEND,
                ChatDatabase.MSG_MINETYPE_LOCATION, ChatDatabase.MSG_STATUS_SEND_PREPARING, cal.getTimeInMillis(),
                locationChat.toJsonObject().toString(), cal.getTimeInMillis());
        chatMessage.imageThumbnail = ImageThumbnailProcessor.getBytes(snapshot);
        if (isGroupChat) {
        	chatMessage.groupUuid = sipAddress;
        }

        // Add message to DB
        mDb.putChatMsg(chatMessage);

        // Upload bitmap
        postImageAndThumbnail(snapshot, new ChatManager.ImageWriteListener() {
            @Override
            public void onImageWriteFinish(ErrorModel error, ImageWriteModel imageWriteModel) {
                if (error == null) {
                    MessageSpecialData imageData = new MessageSpecialData(ChatDatabase.MSG_MINETYPE_LOCATION);
                    locationChat.token = imageWriteModel.token;
                    locationChat.thumb_uri = imageWriteModel.thumb_uri;
                    imageData.messageContent = locationChat.toJsonObject().toString();
                    sendSipMessage(chatMessage.id, sipAddress, imageData.messageContent, imageData, isGroupChat);
                    ChatMessageModel newChatMsg = mDb.findChatMessageById(chatMessage.id);
                    ArrayList<ChatMessageModel> updates = new ArrayList<>();
                    updates.add(newChatMsg);
                    fireMessageStatusUpdateHandler(updates);
                } else {
                    updateChatMsgStatus(chatMessage, ChatDatabase.MSG_STATUS_SEND_FAIL);
                    ChatMessageModel newChatMsg = mDb.findChatMessageById(chatMessage.id);
                    ArrayList<ChatMessageModel> updates = new ArrayList<>();
                    updates.add(newChatMsg);
                    fireMessageStatusUpdateHandler(updates);
                }
            }
        });
        return chatMessage;
    }

    /**
     * Load all chat thread from Sqlite DB synchronously.
     * 
     * @return List of ChatThreadModel.
     */
    public ArrayList<ChatThreadModel> loadAllChatThreadCache() {
        ArrayList<ChatThreadModel> result = new ArrayList<ChatThreadModel>(mAllChatThread.values());
        Collections.sort(result, new ChatThreadModel.DateComparator());
        return result;
    }

    public ArrayList<ChatThreadModel> getAllCurrentChatThread(boolean requestSort) {
        ArrayList<ChatThreadModel> result = new ArrayList<ChatThreadModel>(mAllChatThread.values());
        if (requestSort == true) {
            Collections.sort(result, new ChatThreadModel.DateComparator());
        }
        return result;
    }

    public int countUnreadMessage() {
        return mDb.countUnreadMessage();
    }

    public int countUnreadThread() {
        return mDb.countUnreadThread();
    }

    public int countAllThread() {
        return mDb.countAllThread();
    }

    public void deleteChatThread(long... threadId) {
        for (long id : threadId) {
            mDb.deleteChatThread(id);
            if (mAllChatThread != null) {
                mAllChatThread.remove(id);
            }
        }
        mUnsendChatMessage.clear();
        fireThreadDeleteHandler(threadId);
        fireUnreadMessageChangeHandler(countUnreadThread(), countUnreadMessage());
    }

    public void deleteChatMessage(long... msgIds) {
        // Delete Chat Message
        for (long id : msgIds) {
            mDb.deleteMessage(id);
            if (mAllChatThread != null) {
                mAllChatThread.remove(msgIds);
            }
        }

        // Update cache chat thread
        ArrayList<ChatThreadModel> chatThreads = mDb.findAllMsgThread(mAllChatThread.size());
        mAllChatThread.clear();
        for (ChatThreadModel chatThreadModel : chatThreads) {
            mAllChatThread.put(chatThreadModel.id, chatThreadModel);
        }

        fireThreadChangeHandler(null);
    }

    public void markAllRead(long threadId) {
        if (mDb.countThreadUnreadMessage(threadId) == 0) {
            return;
        }
        mDb.markAllRead(threadId);

        // Update All Chat Thread
        if (mAllChatThread != null) {
            ChatThreadModel model = mAllChatThread.get(threadId);
            if (model != null) {
                mAllChatThread.get(threadId).unreadCount = 0;
            }
        }

        fireUnreadMessageChangeHandler(countUnreadThread(), countUnreadMessage());
        fireThreadChangeHandler(mDb.findMsgThreadById(threadId));
    }
    
    public void markAllMsgStatusRead(long threadId) {
        if (mDb.countThreadUnreadMessage(threadId) == 0) {
            return;
        }
        mDb.markAllMsgStatusRead(threadId);

        // Update All Chat Thread
        if (mAllChatThread != null) {
            ChatThreadModel model = mAllChatThread.get(threadId);
            if (model != null) {
                mAllChatThread.get(threadId).unreadCount = 0;
            }
        }

        fireUnreadMessageChangeHandler(countUnreadThread(), countUnreadMessage());
        fireThreadChangeHandler(mDb.findMsgThreadById(threadId));
    }

    public boolean updateChatMsgStatus(ChatMessageModel msg, int status) {
        boolean isUpdateUnread = msg.status != ChatDatabase.MSG_STATUS_RECEIVE_READ && status == ChatDatabase.MSG_STATUS_RECEIVE_READ;
        boolean result = mDb.updateChatMsgStatus(msg, status);
        if (isUpdateUnread) {
            fireUnreadMessageChangeHandler(countUnreadThread(), countUnreadMessage());
        }
        return result;
    }

    public ArrayList<Long> updateSendingMessageToError(long timeOffsetMillis) {
        ArrayList<Long> hasUpdated = mDb.updateSendingMsgStatusToError(timeOffsetMillis);
        if (hasUpdated.isEmpty() == false) {
            ArrayList<ChatMessageModel> models = new ArrayList<>();
            for (Long id : hasUpdated) {
                ChatMessageModel model = mDb.findChatMessageById(id);
                if (model != null) models.add(model);
            }

            fireMessageStatusUpdateHandler(models);
        }
        return hasUpdated;
    }

    /**
     * Get chat message of Thread.
     * 
     * @param threadId the thread id.
     * @param limit the number of chat message will be loaded (0 if load all).
     * @param listener listener class will handler event when finish.
     * @return The AsyncTask object execute this task.
     */
    @SuppressWarnings("rawtypes")
    public AsyncTask loadChatMessageFromThread(final long threadId, final int offset, final int limit,
            final LoadChatMessageListener listener) {
        AsyncTask<Integer, Integer, ArrayList<ChatMessageModel>> task = new AsyncTask<Integer, Integer, ArrayList<ChatMessageModel>>() {
            @Override
            protected ArrayList<ChatMessageModel> doInBackground(Integer... params) {
                listener.onLoadChatMessageStart();
                return mDb.getChatMessageFromThread(threadId, offset < 0 ? 0 : offset, limit);
            }

            @Override
            protected void onPostExecute(ArrayList<ChatMessageModel> result) {
                listener.onLoadChatMessageFinish(result);
            }
        };
        task.execute();

        return task;
    }

    public ArrayList<ChatMessageModel> getChatMessageImageFromThread(long threadId) {
        return mDb.getChatMessageImageFromThread(threadId);
    }

    public ChatMessageModel getChatMessageById(long msgId) {
        return mDb.findChatMessageById(msgId);
    }

    public ArrayList<ChatMessageModel> getChatMessageById(Long... msgId) {
        return mDb.findChatMessageById(msgId);
    }

    /**
     * Get chat thread by contactStr.
     * 
     * @param contactStr
     * @return
     */
    public ChatThreadModel getChatThreadByContactStr(String contactStr) {
        return mDb.findMsgThreadByContactStr(contactStr);
    }

    private synchronized void fireMessageSentHandler(ChatMessageModel model) {
        for (Iterator<SipChatListener> i = mListSipChatListener.iterator(); i.hasNext();) {
            SipChatListener l = null;
            try {
                l = i.next();
                l.onMessageSent(model);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireMessageConfirmedHandler(ChatMessageModel model) {
        for (Iterator<SipChatListener> i = mListSipChatListener.iterator(); i.hasNext();) {
            SipChatListener l = null;
            try {
                l = i.next();
                l.onMessageConfirmed(model);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireMessageFailureHandler(ChatMessageModel model, int errorCode) {
        for (Iterator<SipChatListener> i = mListSipChatListener.iterator(); i.hasNext();) {
            SipChatListener l = null;
            try {
                l = i.next();
                l.onMessageFailure(model, errorCode);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireMessageIncomingHandler(ChatMessageModel model) {
        for (Iterator<SipChatListener> i = mListSipChatListener.iterator(); i.hasNext();) {
            SipChatListener l = null;
            try {
                l = i.next();
                l.onIncomingMessage(model);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireMessageComposingChangeHandler(String sipAddress, boolean isTyping,
    		boolean isGroupChat, String composingPhoneNumber, String groupUuid) {
        for (Iterator<SipChatComposingListener> i = mListSipChatComposingListener.iterator(); i.hasNext();) {
            SipChatComposingListener l = null;
            try {
                l = i.next();
                l.onSipChatComposing(sipAddress, isTyping, isGroupChat, composingPhoneNumber, groupUuid);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireUnreadMessageChangeHandler(int numberUnreadThread, int totalUnreadMesage) {
        for (Iterator<ChatMessageChangeListener> i = mListChatMessageChangeListener.iterator(); i.hasNext();) {
            ChatMessageChangeListener l = null;
            try {
                l = i.next();
                l.onUnreadMessageChanged(numberUnreadThread, totalUnreadMesage);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireThreadChangeHandler(ChatThreadModel model) {
        for (Iterator<ChatThreadChangeListener> i = mListChatThreadChangeListener.iterator(); i.hasNext();) {
            ChatThreadChangeListener l = null;
            try {
                l = i.next();
                l.onThreadChanged(model);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireThreadDeleteHandler(long[] deletedThreadIds) {
        for (Iterator<ChatThreadChangeListener> i = mListChatThreadChangeListener.iterator(); i.hasNext();) {
            ChatThreadChangeListener l = null;
            try {
                l = i.next();
                l.onThreadDeleted(deletedThreadIds);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireNewMessageHandler(ChatMessageModel... model) {
        for (Iterator<ChatMessageChangeListener> i = mListChatMessageChangeListener.iterator(); i.hasNext();) {
            ChatMessageChangeListener l = null;
            try {
                l = i.next();
                l.onNewMessage(model);
            } catch (Throwable t) {
            }
        }
    }

    private synchronized void fireMessageStatusUpdateHandler(ArrayList<ChatMessageModel> models) {
        for (Iterator<ChatMessageChangeListener> i = mListChatMessageChangeListener.iterator(); i.hasNext();) {
            ChatMessageChangeListener l = null;
            try {
                l = i.next();
                l.onMessageStatusUpdate(models);
            } catch (Throwable t) {
            }
        }
    }

    // //
    // // Override method of SipMessageListener START
    // //
    @Override
    public void onMessageSent(long msgId) {
        if (msgId == MSG_ACTIVE_ID || msgId == MSG_IDLE_ID || msgId == MSG_VIEWED_ID) {
            return;
        }
        ChatMessageModel model = mUnsendChatMessage.get(msgId);
        if (model == null) {
            model = mDb.findChatMessageById(msgId);
            if (model == null) {
                return;
            }
            mUnsendChatMessage.put(msgId, model);
        }
        if (model.isGroupChat()) {
        	return;
        }

        // Update Sqlite
        model.status = ChatDatabase.MSG_STATUS_SEND_SENDING;
        mDb.updateChatMsgStatus(model, ChatDatabase.MSG_STATUS_SEND_SENDING);

        // Fire handler
        fireMessageSentHandler(model);
        ArrayList<ChatMessageModel> models = new ArrayList<>();
        models.add(model);
        fireMessageStatusUpdateHandler(models);
    }

    @Override
    public void onMessageConfirmed(long msgId, int msgStatus) {
        if (msgId == MSG_ACTIVE_ID || msgId == MSG_IDLE_ID || msgId == MSG_VIEWED_ID) {
            return;
        }
        // Update Sqlite
        ChatMessageModel model = mUnsendChatMessage.get(msgId);
        if (model == null) {
            model = mDb.findChatMessageById(msgId);
            if (model == null) {
                return;
            }
            mUnsendChatMessage.put(msgId, model);
        }
        if (msgStatus == 200) {
            model.status = ChatDatabase.MSG_STATUS_SEND_DELIVERED;
            mDb.updateChatMsgStatus(model, ChatDatabase.MSG_STATUS_SEND_DELIVERED);
        } else {
            model.status = ChatDatabase.MSG_STATUS_SEND_SENT;
            mDb.updateChatMsgStatus(model, ChatDatabase.MSG_STATUS_SEND_SENT);
        }

        // Fire handler
        fireMessageConfirmedHandler(model);
        ArrayList<ChatMessageModel> models = new ArrayList<>();
        models.add(model);
        fireMessageStatusUpdateHandler(models);
    }

    @Override
    public void onMessageFailure(long msgId, int errorCode) {
        if (msgId == MSG_ACTIVE_ID || msgId == MSG_IDLE_ID || msgId == MSG_VIEWED_ID) {
            return;
        }
        ChatMessageModel model = mUnsendChatMessage.get(msgId);
        if (model == null) {
            model = mDb.findChatMessageById(msgId);
            if (model == null) {
                return;
            }
            mUnsendChatMessage.put(msgId, model);
        }
        if (model.isGroupChat() && errorCode == 408) {
        	return;
        }

        // Update Sqlite
        model.status = ChatDatabase.MSG_STATUS_SEND_FAIL;
        mDb.updateChatMsgStatus(model, ChatDatabase.MSG_STATUS_SEND_FAIL);

        // Fire handler
        fireMessageFailureHandler(model, errorCode);
        ArrayList<ChatMessageModel> models = new ArrayList<>();
        models.add(model);
        fireMessageStatusUpdateHandler(models);
    }
    
    private void processStatusMessage(SipMessage msg) {
    	if (SipMessage.TYPE_VIEWED.equals(msg.getType())) {
            String uuid = msg.getUuid();
            if (Utils.isStringNullOrEmpty(uuid) == false) {
                // Update database
                boolean updated = mDb.updateChatMsgViewedMember(uuid, msg.getFrom().getLogin());
                if (updated == true) {
                    ArrayList<ChatMessageModel> listModel = new ArrayList<>();
                    ChatMessageModel updatedChatMessage = mDb.findChatMessageByUuid(uuid);
                    listModel.add(updatedChatMessage);
                    fireMessageStatusUpdateHandler(listModel);
                }
            }
        } else if (SipMessage.TYPE_DELIVERED.equals(msg.getType())) {
            String uuid = msg.getUuid();
            if (Utils.isStringNullOrEmpty(uuid) == false) {
                // Update database
            	boolean updated = mDb.updateChatMsgDeliveredMember(uuid, msg.getFrom().getLogin());
            	if (updated == true) {
                    ChatMessageModel updatedChatMessage = mDb.findChatMessageByUuid(uuid);
                    ArrayList<ChatMessageModel> listModel = new ArrayList<>();
                    listModel.add(updatedChatMessage);
                    fireMessageStatusUpdateHandler(listModel);
            	}
            }
        }
    }
    
    public void kickMySelfFromGroup(long threadId) {
    	// Update Group chat data.
    	ChatThreadModel thread = mDb.findMsgThreadById(threadId);
    	String []participants = thread.getListPhoneNumberParticipantExceptYou();
    	String updatedParticipant = TextUtils.join(",", participants);

    	// Add new kick group
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		ArrayList<ChatMessageModel> listNewMsg = new ArrayList<ChatMessageModel>();
    	ChatMessageModel chatMsg = new ChatMessageModel(-1, -1, Const.STR_EMPTY, ChatDatabase.MSG_TYPE_GROUP_STATUS,
                ChatDatabase.MSG_MINETYPE_GROUP_KICK, ChatDatabase.MSG_STATUS_RECEIVE_READ, cal.getTimeInMillis(),
                UserInfo.getInstance().getLocalProfile().getDefaultSipProfile().getUriString(), cal.getTimeInMillis());
		chatMsg.groupUuid = thread.contactStr;
        chatMsg.uuid = Const.STR_EMPTY;
        thread = mDb.putChatMsg(chatMsg);
        listNewMsg.add(chatMsg);

        // Update group chat Data
    	thread.groupData = updatedParticipant + "\n" + thread.getPhoneNumberAdmin();
    	mDb.updateChatThreadGroupData(null, threadId, thread.groupData);
    	
        int unreadMessage = mDb.countUnreadMessage();
        int unreadThread = mDb.countUnreadThread();
    	
    	// Fire event
        fireNewMessageHandler(listNewMsg.toArray(new ChatMessageModel[listNewMsg.size()]));
		fireThreadChangeHandler(thread);
        fireUnreadMessageChangeHandler(unreadThread, unreadMessage);
    }

    Vector<String> groupSelfKickMsgUuid = new Vector<>();
    
    @Override
    public void onIncomingMessage(SipMessage msg) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        // Handling Composing message
        if (SipMessage.TYPE_COMPOSING.equals(msg.getType()) || msg.getId() == MSG_IDLE_ID || msg.getId() == MSG_ACTIVE_ID) {
            // Check content to know is typing or not
            boolean isTyping = true;
            if (msg.getContent().contains(STR_IDLE)) {
                isTyping = false;
            }
            fireMessageComposingChangeHandler(msg.getFrom().getLogin(), isTyping, msg.isGroupChatMessage(),
            		msg.getFrom().getLogin(), msg.getGroupChatUuid());
            return;
        }
        // Handling message Viewed
        else if (SipMessage.TYPE_VIEWED.equals(msg.getType())) {
        	statusMessageThread.addStatusMessageToQueue(msg);
            return;
        } else if (SipMessage.TYPE_DELIVERED.equals(msg.getType())) {
            statusMessageThread.addStatusMessageToQueue(msg);
            return;
        }
        // Handling message Group
        else if (msg.isGroupChatMessage()) {
        	// If message id status
        	if (GroupChatType.STATUS.equals(msg.getGroupChatMsgType())) {
        		String groupUuid = msg.getGroupChatUuid();
        		String participant = msg.getGroupChatParticipants();
        		String administrator = msg.getGroupChatAdministrator();
        		ChatThreadModel chatThread = null;
        		ArrayList<ChatMessageModel> listNewMsg = new ArrayList<ChatMessageModel>();
				boolean isKickedFromGroupChat = false;
        		try {
					JSONObject response = new JSONObject(msg.getContent());
					JSONArray jsonKicked = response.getJSONArray("kicked");
					JSONArray jsonInvited = response.getJSONArray("invited");

					int kickCount = jsonKicked.length();
					for (int i = 0; i < kickCount; i++) {
						String sipUri = jsonKicked.getString(i);
						String phoneNo = QilexPhoneNumberUtils.convertToBasicNumber(sipUri);
						
						// Check if you are kicked from group chat
						if (UserInfo.getInstance().getPhoneNumberFormatted().equals(phoneNo)) {
//							isKickedFromGroupChat = true;
//							chatThread = mDb.findMsgThreadByContactStr(groupUuid);
//							break;
							
							// To check if user if self kick, do not show anything
							if (groupSelfKickMsgUuid.contains(msg.getGroupChatUuid())) {
								break;
							}
						}
						
						ChatMessageModel chatMsg = new ChatMessageModel(-1, -1, Const.STR_EMPTY, ChatDatabase.MSG_TYPE_GROUP_STATUS,
		                        ChatDatabase.MSG_MINETYPE_GROUP_KICK, ChatDatabase.MSG_STATUS_RECEIVE_UNREAD, msg.getTimestamp(),
		                        sipUri, cal.getTimeInMillis());
		        		chatMsg.groupUuid = groupUuid;
		                chatMsg.uuid = msg.getUuid();
		                chatThread = mDb.putChatMsg(chatMsg);
		                listNewMsg.add(chatMsg);
					}
					int inviteCount = jsonInvited.length();
					for (int i = 0; i < inviteCount && isKickedFromGroupChat == false; i++) {
						ChatMessageModel chatMsg = new ChatMessageModel(-1, -1, Const.STR_EMPTY, ChatDatabase.MSG_TYPE_GROUP_STATUS,
		                        ChatDatabase.MSG_MINETYPE_GROUP_INVITE, ChatDatabase.MSG_STATUS_RECEIVE_UNREAD, msg.getTimestamp(),
		                        jsonInvited.getString(i), cal.getTimeInMillis());
		        		chatMsg.groupUuid = groupUuid;
		                chatMsg.uuid = msg.getUuid();
		                chatThread = mDb.putChatMsg(chatMsg);
		                listNewMsg.add(chatMsg);
					}
				} catch (JSONException e) {
				}
        		
        		if (isKickedFromGroupChat) {
//        			if (chatThread != null) {
//        				ChatMessageModel chatMsg = new ChatMessageModel(-1, -1, Const.STR_EMPTY, ChatDatabase.MSG_TYPE_GROUP_STATUS,
//		                        ChatDatabase.MSG_MINETYPE_GROUP_KICK, ChatDatabase.MSG_STATUS_RECEIVE_UNREAD, msg.getTimestamp(),
//		                        UserInfo.getInstance().getPhoneNumberFormatted(), cal.getTimeInMillis());
//		        		chatMsg.groupUuid = groupUuid;
//		                chatMsg.uuid = msg.getUuid();
//        				deleteChatThread(chatThread.id);
//        				fireNewMessageHandler(chatMsg);
//        			}
        		} else {
        			if (chatThread != null) {
            			mDb.updateChatThreadGroupData(null, chatThread.id, participant + "\n" + administrator);
            		}
            		
            		// Get unread message
                    int unreadMessage = mDb.countUnreadMessage();
                    int unreadThread = mDb.countUnreadThread();

                    // Update cache data
                    if (mAllChatThread != null) {
                        mAllChatThread.put(chatThread.id, chatThread);
                    }

                    // Fire handler
                    fireUnreadMessageChangeHandler(unreadThread, unreadMessage);
                    fireThreadChangeHandler(chatThread);
                    if (listNewMsg.isEmpty() == false) {
                        fireMessageIncomingHandler(listNewMsg.get(0));
                        fireNewMessageHandler(listNewMsg.toArray(new ChatMessageModel[listNewMsg.size()]));
                    }

                    // Check QIlexService
                    if (BaseActivity.getService() == null) {
                        Intent intent = new Intent(MyApplication.getAppContext(), QilexService.class);
                        intent.putExtra(QilexService.INTENT_PARAM_ACTION, QilexService.ActionEnum.ACTION_OPEN_INCOMING_MESSAGE);
                        MyApplication.getAppContext().startService(intent);
                    }
        		}
            	return;
        	}
        }

        if (Const.IS_OLD_VERSION) {
            if (SipMessage.TYPE_MESSAGE_IMAGE.equals(msg.getType())
                    || SipMessage.TYPE_MESSAGE_LOCATION.equals(msg.getType())
                    || SipMessage.TYPE_VIEWED.equals(msg.getType())
                    || SipMessage.TYPE_DELIVERED.equals(msg.getType())) {
                return;
            }
        }

        // Insert message to SqliteDB
        String phoneNo = msg.getFrom().getLogin();
        ChatMessageModel chatMsg = new ChatMessageModel(-1, -1, phoneNo, ChatDatabase.MSG_TYPE_RECEIVE,
                ChatDatabase.MSG_MINETYPE_TEXT, ChatDatabase.MSG_STATUS_RECEIVE_UNREAD, msg.getTimestamp(),
                msg.getContent(), cal.getTimeInMillis());
        chatMsg.uuid = msg.getUuid();
        if (msg.isGroupChatMessage()) {
        	chatMsg.groupUuid = msg.getGroupChatUuid();
        	if (mDb.findMsgThreadByContactStr(msg.getGroupChatUuid()) == null) {
        		// Add new Chat thread
                ChatThreadModel chatThread = mDb.updateChatThread(msg.getGroupChatUuid(), ChatDatabase.MSG_TYPE_GROUP_STATUS,
                		msg.getTimestamp(), Const.STR_EMPTY, false, ChatDatabase.MSG_MINETYPE_GROUP_INVITE);
            	String administrator = msg.getGroupChatAdministrator();
            	String participant = msg.getGroupChatParticipants();
            	mDb.updateChatThreadGroupData(null, chatThread.id, participant + "\n" + administrator);
        	}
        }

        // Check message type
        MessageSpecialData specialData = new MessageSpecialData(msg.getType());
        specialData.messageContent = msg.getContent();
        specialData.parseContentToMessageModel(chatMsg);

        // Insert into DB
        ChatThreadModel chatThread = mDb.putChatMsg(chatMsg);

        // Get unread message
        int unreadMessage = mDb.countUnreadMessage();
        int unreadThread = mDb.countUnreadThread();

        // Update cache data
        if (mAllChatThread != null) {
            mAllChatThread.put(chatThread.id, chatThread);
        }

        mNewestMessage = chatMsg;

        // Fire handler
        fireMessageIncomingHandler(chatMsg);
        fireNewMessageHandler(chatMsg);
        fireUnreadMessageChangeHandler(unreadThread, unreadMessage);
        fireThreadChangeHandler(chatThread);

        // Check QIlexService
        if (BaseActivity.getService() == null) {
            Intent intent = new Intent(MyApplication.getAppContext(), QilexService.class);
            intent.putExtra(QilexService.INTENT_PARAM_ACTION, QilexService.ActionEnum.ACTION_OPEN_INCOMING_MESSAGE);
            MyApplication.getAppContext().startService(intent);
        }
    }

    public void putCallMessage(String phoneNo, int chatType, int mineType) {
        long dateMiliSecond = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
        switch (chatType) {
            case ChatDatabase.MSG_TYPE_CALLLOG:
                ChatMessageModel chatModel = new ChatMessageModel(-1, -1, phoneNo, chatType, mineType,
                        ChatDatabase.MSG_STATUS_RECEIVE_READ, dateMiliSecond,
                        MyApplication.getAppResource().getString(R.string.missed_call), dateMiliSecond);
                ChatThreadModel chatThread = mDb.putChatMsg(chatModel);

                // Update cache data
                if (mAllChatThread != null) {
                    mAllChatThread.put(chatThread.id, chatThread);
                }
                // Fire handler
                fireThreadChangeHandler(chatThread);
                fireNewMessageHandler(chatModel);
                break;

            default:
                break;
        }
    }

    public ChatMessageModel resendMessage(long msgId) {
        ChatMessageModel chatMessage = mDb.findChatMessageById(msgId);
        switch (chatMessage.mineType) {
            case ChatDatabase.MSG_MINETYPE_IMAGE:
                ChatMessageModel message = resendImageMessage(chatMessage);
                return message;
            case ChatDatabase.MSG_MINETYPE_TEXT:
                deleteChatMessage(chatMessage.id);
                SipMessage sipMsg = sendSipMessage(-1, chatMessage.getThreadUuid(), chatMessage.textContent, null, chatMessage.isGroupChat());
                return mDb.findChatMessageById(sipMsg.getId());
            case ChatDatabase.MSG_MINETYPE_LOCATION:
                return resendLocationMessage(chatMessage);
            default:
                break;
        }
        return null;
    }

    private ChatMessageModel resendImageMessage(ChatMessageModel chatMessage) {
        String messageContent = chatMessage.textContent;
        // parse content to Object
        try {
            ImageWriteModel imageModel = new ImageWriteModel(new JSONObject(messageContent));

            // If image is not uploaded:
            if (Utils.isStringNullOrEmpty(imageModel.token)) {
                // Remove message and resend
                deleteChatMessage(chatMessage.id);
                return sendImageMessage(chatMessage.getThreadUuid(), imageModel.description, imageModel.image_uri, chatMessage.isGroupChat());
            }
            // If image is uploaded
            else {
                mDb.resetChatMsgStatusToSending(chatMessage.id);
                JSONObject json = imageModel.toJsonObject();
                sendSipMessage(chatMessage.id, chatMessage.getThreadUuid(), json.toString(), null, chatMessage.isGroupChat());
                return chatMessage;
            }
        } catch (JSONException e) {
        }
        return null;
    }

    private ChatMessageModel resendLocationMessage(ChatMessageModel chatMessage) {
        String messageContent = chatMessage.textContent;
        // parse content to Object
        try {
            LocationChatModel locationModel = new LocationChatModel(new JSONObject(messageContent));

            // If image is not uploaded:
            if (Utils.isStringNullOrEmpty(locationModel.token)) {
                // Remove message and resend
                deleteChatMessage(chatMessage.id);
                Bitmap bmp = null;
                if (chatMessage.imageThumbnail != null) {
                    bmp = BitmapFactory.decodeByteArray(chatMessage.imageThumbnail, 0, chatMessage.imageThumbnail.length);
                }
                return sendLocationMessage(chatMessage.getThreadUuid(), locationModel.latitude, locationModel.longitude, bmp, locationModel.description, chatMessage.isGroupChat());
            }
            // If image is uploaded
            else {
                mDb.resetChatMsgStatusToSending(chatMessage.id);
                JSONObject json = locationModel.toJsonObject();
                sendSipMessage(chatMessage.id, chatMessage.getThreadUuid(), json.toString(), null, chatMessage.isGroupChat());
                return chatMessage;
            }
        } catch (JSONException e) {
        }
        return null;
    }

    // //
    // // Override method of SipMessageListener END
    // //
    
    HashMap<String, DownloadImageListener> mDownloadImageListener;

    public int downloadImage(final String url, final String token, final ChatMessageModel chatMessage,
            DownloadImageListener listener, final boolean isThumbnail) {
        // check is downloading or not
        final String key = token + url;
        synchronized (mDownloadImageListener) {
            mDownloadImageListener.put(url, listener);
        }
        if (mDownloadingImageMessage.containsKey(key)) {
            return -1;
        }
        mDownloadingImageMessage.put(key, chatMessage);
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        // Check response data null.
                        mDownloadingImageMessage.remove(key);
                        DownloadImageListener listener = null;
                        synchronized (mDownloadImageListener) {
                            listener = mDownloadImageListener.remove(url);
                        }
                        if (req.getResponseData() != null) {
                            if (isThumbnail) {
                                // Extract thumbnail
                                Bitmap bmp = BitmapFactory.decodeByteArray(req.getResponseData(), 0,
                                        req.getResponseData().length);
                                Bitmap bmpThumb;
                                // Thumbnail size generator
                                if (chatMessage.mineType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                                    bmpThumb = ImageThumbnailProcessor.extractThumbnail_3x2(bmp);
                                } else {
                                    bmpThumb = ImageThumbnailProcessor.extractThumbnail(bmp);
                                }

                                byte[] blob = ImageThumbnailProcessor.getBytes(bmpThumb);
                                mDb.updateChatMsgThumbnail(chatMessage.id, blob);

                                // Upload status
                                ChatManager.getInstance().updateChatMsgStatus(chatMessage,
                                        ChatDatabase.MSG_STATUS_RECEIVE_UNREAD);
                                ChatMessageModel newChatMsg = mDb.findChatMessageById(chatMessage.id);
                                ArrayList<ChatMessageModel> updates = new ArrayList<>();
                                updates.add(newChatMsg);
                                fireMessageStatusUpdateHandler(updates);
                            } else {
                                // Save file
                                String fileName = Calendar.getInstance().getTimeInMillis() + ".jpg";
                                String outFile = ImageThumbnailProcessor.saveImageToKatzLibrary(req.getResponseData(),
                                        fileName);

                                if (listener != null) {
                                    listener.onDownloadFinish(outFile, token, true);
                                }
                                mDb.updateChatMsgImageUrl(chatMessage.id, outFile);
                            }
                        } else {
                            synchronized (mDownloadImageListener) {
                                listener = mDownloadImageListener.remove(url);
                            }
                            if (listener != null) {
                                listener.onDownloadFinish(null, null, false);
                            }
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        mDownloadingImageMessage.remove(key);
                        synchronized (mDownloadImageListener) {
                            listener = mDownloadImageListener.remove(url);
                        }
                        if (listener != null) {
                            listener.onDownloadFinish(null, null, false);
                        }
                        break;
                }
            }
        };

        QilexHttpRequest request = ImageHttpUtils.getImageAndThumbnail(MyApplication.getAppContext(), url, token,
                httpRequestListener);
        return request.getRequestId();
    }

    public void reDownloadThumbnail(ChatMessageModel model, DownloadImageListener listener) {
        switch (model.mineType) {
            case ChatDatabase.MSG_MINETYPE_IMAGE: {
                try {
                    JSONObject json = new JSONObject(model.textContent);
                    ImageWriteModel imageWrite = new ImageWriteModel(json);
                    ChatManager.getInstance().downloadImage(imageWrite.thumb_uri, imageWrite.token,
                            model, listener, true);
                } catch (JSONException e) {
                }
                break;
            }
            case ChatDatabase.MSG_MINETYPE_LOCATION: {
                try {
                    JSONObject json = new JSONObject(model.textContent);
                    LocationChatModel locationModel = new LocationChatModel(json);
                    ChatManager.getInstance().downloadImage(locationModel.thumb_uri, locationModel.token,
                            model, listener, true);
                } catch (JSONException e) {
                }
                break;
            }
            default:
                break;
        }
    }
    
    private String buildListUriContent(String ...phoneNumber) {
    	if (phoneNumber == null || phoneNumber.length == 0) {
    		return Const.STR_EMPTY;
    	}
    	String DEFAULT_URI_FORMAT = "sip:%s@sip.ct.plus.pl";
    	StringBuilder output = new StringBuilder();
    	for (String string : phoneNumber) {
    		output.append(String.format(DEFAULT_URI_FORMAT, string) + ",");
		}
    	String outputString = output.toString();
    	return outputString.substring(0, outputString.length()-1);
    }
    
    private String generateGroupChatUuid() {
    	String phoneNumber = UserInfo.getInstance().getPhoneNumberFormatted();
    	String timeStamp = Long.toString(Calendar.getInstance().getTimeInMillis());
    	String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
    	try {
    		String encode = Zip.encodeBase64(phoneNumber + deviceId + timeStamp);
			return encode.substring(0, encode.length() - 1);
		} catch (UnsupportedEncodingException e) {
			return phoneNumber + deviceId + timeStamp;
		}
    }

    /**
     * Other UI need implement this interface to handler event when have Sip
     * Message.
     */
    public static interface SipChatListener {
        public void onMessageSent(ChatMessageModel msg);

        public void onMessageConfirmed(ChatMessageModel msg);

        public void onMessageFailure(ChatMessageModel msg, int errorCode);

        public void onIncomingMessage(ChatMessageModel msg);
    }

    public static interface SipChatComposingListener {
        public void onSipChatComposing(String sipAddress, boolean isTyping, boolean isGroupChat, String composingPhoneNumber, String groupUuid);
    }

    public static interface ChatThreadChangeListener {
        public void onThreadDeleted(long[] deletedThreadIds);

        public void onThreadChanged(ChatThreadModel model);
    }

    public static interface ChatMessageChangeListener {
        public void onNewMessage(ChatMessageModel... newMsg);

        public void onMessageStatusUpdate(ArrayList<ChatMessageModel> models);

        public void onUnreadMessageChanged(int numberUnreadThread, int totalUnreadMesage);
    }

    public interface LoadChatThreadListener {
        public void onLoadChatThreadStart();

        public void onLoadChatThreadFinish(ArrayList<ChatThreadModel> listResult);
    }

    public interface LoadChatMessageListener {
        public void onLoadChatMessageStart();

        public void onLoadChatMessageFinish(ArrayList<ChatMessageModel> listResult);
    }

    public interface ImageWriteListener {
        public void onImageWriteFinish(ErrorModel error, ImageWriteModel imageWriteModel);
    }

    public interface DownloadImageListener {
        public void onDownloadFinish(String filePath, String token, boolean result);
    }

    public class MessageSpecialData {

        public int dataType; // mine Type

        // Data of SEND IMAGE function - START
        public String imageUrl;

        public String description;

        // Data of SEND IMAGE function - END

        public byte[] thumbnail = null;

        public String messageContent;
        
        // Group Chat
        public boolean isGroupChat = false;

        public MessageSpecialData(int dataType) {
            this.dataType = dataType;
        }

        public MessageSpecialData(String sipType) {
            if (SipMessage.TYPE_MESSAGE_IMAGE.equals(sipType)) {
                dataType = ChatDatabase.MSG_MINETYPE_IMAGE;
            } else if (SipMessage.TYPE_MESSAGE_LOCATION.equals(sipType)) {
                dataType = ChatDatabase.MSG_MINETYPE_LOCATION;
            } else {
                dataType = ChatDatabase.MSG_MINETYPE_TEXT;
            }
        }

        public String toSipType() {
            switch (dataType) {
                case ChatDatabase.MSG_MINETYPE_TEXT:
                    return SipMessage.TYPE_MESSAGE_TEXT;
                case ChatDatabase.MSG_MINETYPE_IMAGE:
                    return SipMessage.TYPE_MESSAGE_IMAGE;
                case ChatDatabase.MSG_MINETYPE_LOCATION:
                    return SipMessage.TYPE_MESSAGE_LOCATION;
                default:
                    return "";
            }
        }

        public void parseContentToMessageModel(final ChatMessageModel chatMessage) {
            switch (dataType) {
                case ChatDatabase.MSG_MINETYPE_IMAGE: {
                    try {
                        JSONObject json = new JSONObject(messageContent);
                        ImageWriteModel imageWrite = new ImageWriteModel(json);
                        chatMessage.mineType = dataType;
                        chatMessage.textContent = messageContent;
                        chatMessage.status = ChatDatabase.MSG_STATUS_RECEIVE_PREPARING;

                        // Download bitmap
                        if (chatMessage.imageThumbnail == null || chatMessage.imageThumbnail.length == 0) {
                            ChatManager.getInstance().downloadImage(imageWrite.thumb_uri, imageWrite.token, chatMessage,
                                    new DownloadImageListener() {
                                        @Override
                                        public void onDownloadFinish(String filePath, String token, boolean result) {

                                        }
                                    }, true);
                        }
                    } catch (JSONException e) {
                    }
                    break;
                }
                case ChatDatabase.MSG_MINETYPE_LOCATION: {
                    try {
                        JSONObject json = new JSONObject(messageContent);
                        LocationChatModel locationChat = new LocationChatModel(json);
                        chatMessage.mineType = dataType;
                        chatMessage.textContent = messageContent;
                        chatMessage.status = ChatDatabase.MSG_STATUS_RECEIVE_PREPARING;
                        // Download bitmap
                        if (chatMessage.imageThumbnail == null || chatMessage.imageThumbnail.length == 0) {
                            ChatManager.getInstance().downloadImage(locationChat.thumb_uri, locationChat.token,
                                    chatMessage, new DownloadImageListener() {
                                        @Override
                                        public void onDownloadFinish(String filePath, String token, boolean result) {

                                        }
                                    }, true);
                        }

                    } catch (JSONException e) {
                    }
                    break;
                }
            }
        }
    }
    
    public class ProcessingStatusMessageThread extends Thread {
    	private LinkedList<SipMessage> waitingQueue = new LinkedList<>();
    	
    	private boolean isRunning = false;
    	
    	public void startRunning() {
    		isRunning = true;
    		start();
    	}
    	
    	public void stopRunning() {
    		isRunning = false;
    	}
    	
    	@Override
    	public void run() {
    		while (isRunning) {
    			SipMessage mess = null;

                while (true) {
                    synchronized (waitingQueue) {
                        mess = waitingQueue.poll();
                    }

                    if (mess != null) {
                        // Processing Message
                        processStatusMessage(mess);
                    } else {
                        break;
                    }
                }

                try {
                    Thread.sleep(Integer.MAX_VALUE);
                } catch (InterruptedException e) {
                }
            }
    	}
    	
    	public void addStatusMessageToQueue(SipMessage mess) {
			synchronized (waitingQueue) {
				waitingQueue.add(mess);
                interrupt();
			}
    	}
    }
    
}
