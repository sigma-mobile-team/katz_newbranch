
package sigma.qilex.manager.chat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import pl.katz.aero2.MyApplication;
import sigma.qilex.utils.Utils;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ImageThumbnailProcessor {

    public static final int DEFAULT_THUMBNAIL_SIZE = 250;

    public static final int DEFAULT_IMAGE_VOLUMN = 10 * 1024 * 1024; // Bytes

    public static String FOLDER_KATZ() {
        return Utils.isInKatzMode() ? "KATZ_Photos" : "Frifon_Photos";
    };

    /**
     * Generate an image thumbnail from an image uri.
     * 
     * @param uri
     * @param imageSize the image size in pixel (-1 will be default)
     * @return
     */
    public static Bitmap generateThumbnail(String imagePath, int imageSize) {
        int size = imageSize > 0 ? imageSize : DEFAULT_THUMBNAIL_SIZE;
        int orientation = getOrientationFromExif(imagePath);
        ImageLoader loader = MyApplication.getImageLoader();

        String uri = "file://" + imagePath;
        Bitmap rawBmp = loader.loadImageSync(uri);
        if (rawBmp == null) {
            return null;
        }
        Bitmap outputBmp = ThumbnailUtils.extractThumbnail(rawBmp, size, size);
        rawBmp.recycle();

        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        outputBmp = Bitmap.createBitmap(outputBmp, 0, 0, outputBmp.getWidth(), outputBmp.getHeight(), matrix, true);

        return outputBmp;
    }

    public static int getOrientationFromExif(String imagePath) {
        int orientation = 0;
        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    orientation = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    orientation = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    orientation = 90;
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                    orientation = 0;
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
        }

        return orientation;
    }

    /**
     * Convert from bitmap to byte array
     * 
     * @param bitmap
     * @return
     */
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Convert from byte array to bitmap
     * 
     * @param image
     * @return
     */
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static Bitmap getMaximumSizeScaleImage(String filePath, long sizeInBytes) {
        if (sizeInBytes < 0) {
            sizeInBytes = DEFAULT_IMAGE_VOLUMN;
        }

        // Calculate File size
        File file = new File(filePath);
        long sizeRawImage = file.length();

        // Load raw image
        ImageLoader loader = MyApplication.getImageLoader();
        Bitmap rawBmp = loader.loadImageSync("file://" + filePath);
        if (rawBmp == null) {
            return null;
        }

        // Calculate rate
        double rate = 1;
        if (sizeRawImage > sizeInBytes) {
            rate = Math.sqrt((double)sizeRawImage / (double)sizeInBytes);
        }
        int imageNewWith = (int)(rawBmp.getWidth() / rate);
        int imageNewHeight = (int)(rawBmp.getHeight() / rate);
        Bitmap outputBmp = ThumbnailUtils.extractThumbnail(rawBmp, imageNewWith, imageNewHeight);
        rawBmp.recycle();
        return outputBmp;
    }

    // Save Image to KATZ Library
    /**
     * save image to KATZ gallery.
     * 
     * @param rawFilePath
     * @param fileName
     * @param sizeInBytes
     * @return absoluted file path
     */
    public static String saveImageToKatzLibrary(String rawFilePath, String fileName, long sizeInBytes) {
        int orientation = getOrientationFromExif(rawFilePath);
        if (sizeInBytes < 0) {
            sizeInBytes = DEFAULT_IMAGE_VOLUMN;
        }

        // Calculate File size
        File file = new File(rawFilePath);
        long sizeRawImage = file.length();

        // Load raw image
        ImageLoader loader = MyApplication.getImageLoader();
        Bitmap rawBmp = loader.loadImageSync("file://" + rawFilePath);
        if (rawBmp == null) {
            return null;
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        rawBmp = Bitmap.createBitmap(rawBmp, 0, 0, rawBmp.getWidth(), rawBmp.getHeight(), matrix, true);

        // Calculate rate
        double rate = 1;
        if (sizeRawImage > sizeInBytes) {
            rate = (double)sizeInBytes / (double)sizeRawImage;
        }

        // Make new file
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/" + FOLDER_KATZ());
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        File savedFile = new File(tempDir.getAbsoluteFile() + "/" + fileName);
        OutputStream fOut = null;
        try {
            fOut = new FileOutputStream(savedFile);
            rawBmp.compress(Bitmap.CompressFormat.JPEG, (int)(rate * 100), fOut);
            fOut.flush();
            fOut.close();
            MyApplication.getAppContext()
                    .sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(savedFile)));
            return savedFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String saveImageToKatzLibrary(byte[] imageByte, String fileName) {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/" + FOLDER_KATZ());
        if (tempDir.exists() == false) {
            tempDir.mkdirs();
        }
        File savedFile = new File(tempDir.getAbsoluteFile() + "/" + fileName);
        try {
            FileOutputStream fos = new FileOutputStream(savedFile);
            fos.write(imageByte);
            fos.flush();
            fos.close();
            // fOut = new FileOutputStream(savedFile);
            // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            MyApplication.getAppContext()
                    .sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(savedFile)));
            return savedFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getAvatarBytesJpg(String filePath) {
        if (Utils.isStringNullOrEmpty(filePath)) {
            return null;
        }
        Bitmap bmp = MyApplication.getImageLoader().loadImageSync("file://" + filePath);
        if (bmp == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static Bitmap extractThumbnail(Bitmap bitmap) {
        int size = bitmap.getWidth() < bitmap.getHeight() ? bitmap.getWidth() : bitmap.getHeight();
        Bitmap resized = ThumbnailUtils.extractThumbnail(bitmap, size, size);
        return resized;
    }

    public static Bitmap extractThumbnail_3x2(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int thumbW = width;
        int thumbH = 2 * width / 3;
        if (thumbH > height) {
            thumbH = height;
            thumbW = 3 * height / 2;
        }
        Bitmap resized = ThumbnailUtils.extractThumbnail(bitmap, thumbW, thumbH);
        return resized;
    }

    public interface GenerateThumbnailListener {
        public void onGeneratingImage(String originUri, Bitmap bitmap);

        public void onGeneratingFinishAll(String[] originUris, Bitmap[] bitmaps);
    }
}
