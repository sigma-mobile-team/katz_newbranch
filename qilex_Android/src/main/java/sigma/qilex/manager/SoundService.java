
package sigma.qilex.manager;

import static android.media.AudioManager.MODE_RINGTONE;
import static android.media.AudioManager.STREAM_VOICE_CALL;

import android.content.Context;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;

/**
 * Class of managing sounds of the application, in particular the firing of
 * ringtones and sounds of dialing / busy.
 */
public class SoundService {
    public static final String DBG_TAG = "SoundService";

    public static final long[] PATERN = {
            0, 1000, 1000
    };

    private class RingtoneSignal extends Signal {
        private Ringtone mRingtone;

        private boolean mIsRinging = false;

        Vibrator mVibrator;

        AudioManager mAudioManager;

        public RingtoneSignal(Ringtone ringtone) {
            super(SIGNAL_RINGTONE);
            mRingtone = ringtone;
            mVibrator = QilexService.getVibratorService();
            mAudioManager = QilexService.getAudioManager();
        }

        @Override
        public synchronized void close() {
            mRingtone = null;
        }

        @Override
        public synchronized boolean startSignal() {
            mIsRinging = true;
            start();
            return false;
        }

        @Override
        public synchronized void stopSignal() {
            if (mRingtone != null) {
                mRingtone.stop();
            }
            if (mVibrator != null) {
                mVibrator.cancel();
            }
            interrupt();
            mIsRinging = false;
        }

        private synchronized boolean isRinging() {
            return mIsRinging;
        }

        @Override
        public void run() {
            try {
                if ((mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE
                        || mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) && mVibrator != null) {
                    mVibrator.vibrate(PATERN, 1);
                }
                while (isRinging()) {
                    if (mRingtone != null)
                        mRingtone.play();
                }
            } catch (Exception e) {
            }
        }

    }

    private abstract class Signal extends Thread {
        private int mType;

        /**
         * Constructor
         * 
         * @param type Type of signal, is one of constant SIGNAL_
         */
        public Signal(int type) {
            mType = type;
        }

        /** Release resource signal. */
        public abstract void close();

        /**
         * @return Type of signal, is one of Constant SIGNAL_
         */
        @SuppressWarnings("unused")
        public int getType() {
            return mType;
        }

        /**
         * Firing Signal
         * 
         * @return true if fired, false otherwise
         */
        public abstract boolean startSignal();

        /** Stop signal */
        public abstract void stopSignal();
    }

    private class ToneSignal extends Signal {
        private int mTone, mDuration;

        private ToneGenerator mToneGenerator;

        /**
         * Constructor.
         * 
         * @param type Type of signal, is one of constant SIGNAL_.
         * @param tone id TONE
         */
        public ToneSignal(int type, int tone) {
            super(type);
            mToneGenerator = new ToneGenerator(STREAM_VOICE_CALL, ToneGenerator.MAX_VOLUME);
            mTone = tone;
            mDuration = -1;
        }

        /**
         * Konstruktor
         * 
         * @param type Type of signal, is one of constant SIGNAL_..
         * @param tone id TONE
         */
        public ToneSignal(int type, int tone, int duration) {
            this(STREAM_VOICE_CALL, tone);
            mDuration = duration;
        }

        @Override
        public void close() {
            if (mDuration < 0) // We stop only if this signal has a manual stop
            {
                mToneGenerator.release();
                mToneGenerator = null;
            }
        }

        @Override
        public void run() {
            try {
                Thread.sleep(mDuration);
            } catch (Exception e) {
            }
            mDuration = -1;
            stopSignal();
            close();
        }

        @Override
        public boolean startSignal() {
            if (mDuration < 0) {
                LogUtils.e("NAMND", "tone duration ==" + mDuration);
                return mToneGenerator.startTone(mTone);
            } else {
                boolean started = mToneGenerator.startTone(mTone);
                LogUtils.e("NAMND", "tone started ==" + started);
                if (started) {
                    start();// running thread
                }
                return started;
            }
        }

        @Override
        public void stopSignal() {
            if (mDuration < 0) // We stop only if this signal has a manual stop
                mToneGenerator.stopTone();
        }

    }

    /** busy signal (or peer rejected) */
    private static final int SIGNAL_CALLING_PEER_BUSY = 3;

    /** signal issued when a peer is unreachable (the timeout) */
    private static final int SIGNAL_CALLING_PEER_UNREACHABLE = 4;

    /** signal calls are answered by peer */
    private static final int SIGNAL_CALLING_RINGING_BACK = 2;

    /** The signal plays a ringtone (you must enter a class object Ringtone) */
    private static final int SIGNAL_RINGTONE = MODE_RINGTONE;

    /** Singleton */
    private static SoundService mInstance = null;

    /**
     * Pobranie singletonu
     * 
     * @param context Kontekst ( zalecany getApplicationContext() )
     * @return singleton SoundService
     */
    public static SoundService getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SoundService(context);
        }
        return mInstance;
    }

    /** Release the SoundService singleton object */
    public static void releaseSingleton() {
        mInstance = null;
    }

    private Context mContext;

    private Signal mCurrSignal;

    private Ringtone mCurrDefaultNotificationRingtone;

    private Uri mCurrDefaultNotificationRingtoneUri;

    /**
     * Create an instant of SoundService object
     * 
     * @param context context
     */
    private SoundService(Context context) {
        mContext = context;
    }

    /**
     * Firing sound busy (or peer rejected the call)
     * 
     * @return true if the signal is fired
     */
    public synchronized boolean genCallingBusySignal() {
        if (mCurrSignal != null)
            return false;
        mCurrSignal = new ToneSignal(SIGNAL_CALLING_PEER_BUSY, ToneGenerator.TONE_CDMA_NETWORK_BUSY, 1500);
        return mCurrSignal.startSignal();
    }

    /**
     * Firing sound wait for a connection - used when you dial the number, and
     * we have a signal from peer (waiting to be picked up by the other party)
     * 
     * @return true jesli sygnał został odpalony, false jeśli nie, w
     *         szeczególności jeśli gra inny dzwięk
     */
    public synchronized boolean genStartCallingSignal() {
        if (mCurrSignal != null)
            return false;
        mCurrSignal = new ToneSignal(SIGNAL_CALLING_RINGING_BACK, ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK);
        return mCurrSignal.startSignal();
    }

    /**
     * Firing sound wait for a connection - used when you dial the number, and
     * we have a signal from peer (waiting to be picked up by the other party)
     * 
     * @return true jesli sygnał został odpalony, false jeśli nie, w
     *         szeczególności jeśli gra inny dzwięk
     */
    public synchronized boolean genCallingRingingBackSignal() {
        if (mCurrSignal != null)
            return false;
        mCurrSignal = new ToneSignal(SIGNAL_CALLING_RINGING_BACK, ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK);
        return mCurrSignal.startSignal();
    }

    /**
     * Firing sound wait for a connection - used when you dial the number, and
     * we have a signal from peer (waiting to be picked up by the other party)
     * 
     * @return true jesli sygnał został odpalony, false jeśli nie, w
     *         szeczególności jeśli gra inny dzwięk
     */
    public synchronized boolean genCallingConnectingSignal() {
        if (mCurrSignal != null)
            return false;
        mCurrSignal = new ToneSignal(SIGNAL_CALLING_RINGING_BACK, ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK);
        return mCurrSignal.startSignal();
    }

    /**
     * Debugowa metoda do odpalania różnych tonów, do wywalenia po dopracowaniu
     * aplikacji
     * 
     * @param tone
     * @return
     */
    public synchronized boolean genCallingRingingBackSignal(int tone) {
        if (mCurrSignal != null)
            return false;
        mCurrSignal = new ToneSignal(0, tone);
        return mCurrSignal.startSignal();
    }

    /**
     * Firing sound signaling that the peer is unreachable or passed a timeout
     * on the connection when dialing
     * 
     * @return true if the signal has been triggered, false if not
     */
    public synchronized boolean genCallingUnreachableSignal() {
        if (mCurrSignal != null)
            return false;
        mCurrSignal = new ToneSignal(SIGNAL_CALLING_PEER_UNREACHABLE, ToneGenerator.TONE_CDMA_ABBR_ALERT, 1500);
        return mCurrSignal.startSignal();
    }

    /**
     * The method of firing ringing tone, expropriates the other signals
     * 
     * @param ringtone object android.media.Ringtone that want to play
     * @return true if fire is managed
     */
    public synchronized boolean genRingtoneSignal(Ringtone ringtone) {
        if (mCurrSignal != null) {
            mCurrSignal.startSignal();
            mCurrSignal.close();
        }
        mCurrSignal = new RingtoneSignal(ringtone);
        return mCurrSignal.startSignal();
    }

    /**
     * Refer to the default phone ring
     * 
     * @return Object type android.media.Ringtone representing the loaded
     *         default ring
     */
    public Ringtone getDefaultRingtone() {
        Uri defRingtoneUri = android.provider.Settings.System.DEFAULT_RINGTONE_URI;
        return RingtoneManager.getRingtone(mContext, defRingtoneUri);
    }

    public Ringtone getDefaultNotificationRingtone() {
        Uri defRingtoneUri = android.provider.Settings.System.DEFAULT_NOTIFICATION_URI;
        if (defRingtoneUri.equals(mCurrDefaultNotificationRingtoneUri) == false) {
            mCurrDefaultNotificationRingtone = RingtoneManager.getRingtone(mContext, defRingtoneUri);
            mCurrDefaultNotificationRingtoneUri = defRingtoneUri;
        }
        return mCurrDefaultNotificationRingtone;
    }

    public Ringtone getRingtoneByUri(Uri ringtoneUri) {
        if (ringtoneUri == null)
            return null;
        return RingtoneManager.getRingtone(mContext, ringtoneUri);
    }

    public synchronized void stopAllSignals() {
        if (mCurrSignal == null)
            return;
        mCurrSignal.stopSignal();
        mCurrSignal.close();
        mCurrSignal = null;
    }

    public synchronized boolean getCallingOtherLine() {
        if (mCurrSignal != null) {
            return false;
        }
        mCurrSignal = new ToneSignal(5, ToneGenerator.TONE_CDMA_NETWORK_CALLWAITING, -1);
        return mCurrSignal.startSignal();
    }
}
