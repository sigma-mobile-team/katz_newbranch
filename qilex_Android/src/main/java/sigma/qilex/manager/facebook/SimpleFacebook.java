
package sigma.qilex.manager.facebook;

import java.util.List;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;

import android.app.Activity;
import android.content.Intent;

public class SimpleFacebook {
    private static SimpleFacebook mInstance = null;

    private static SessionManager mSessionManager = null;

    public static void initialize(Activity activity) {
        if (mInstance == null) {
            FacebookSdk.sdkInitialize(activity.getApplicationContext());
            mInstance = new SimpleFacebook();
            mSessionManager = new SessionManager();
        }
        mSessionManager.setActivity(activity);
    }

    public static SimpleFacebook getInstance(Activity activity) {
        if (mInstance == null) {
            FacebookSdk.sdkInitialize(activity.getApplicationContext());
            mInstance = new SimpleFacebook();
            mSessionManager = new SessionManager();
        }
        mSessionManager.setActivity(activity);
        return mInstance;
    }

    public static SimpleFacebook getInstance() {
        return mInstance;
    }

    public void login(OnLoginListener onLoginListener, List<String> permissions) {
        mSessionManager.login(onLoginListener, permissions);
    }

    public void logout(OnLogoutListener onLogoutListener) {
        mSessionManager.logout(onLogoutListener);
    }

    public boolean isLogin() {
        return mSessionManager.isLogin();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSessionManager.onActivityResult(requestCode, resultCode, data);
    }

    public AccessToken getAccessToken() {
        return mSessionManager.getAccessToken();
    }

}
