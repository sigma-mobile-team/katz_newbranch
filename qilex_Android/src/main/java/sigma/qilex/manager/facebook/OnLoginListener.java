
package sigma.qilex.manager.facebook;

import com.facebook.AccessToken;

/**
 * On login/logout actions listener
 *
 * @author sromku
 */
public interface OnLoginListener extends OnErrorListener {

    void onLogin(AccessToken accessToken);

    void onCancel();

}
