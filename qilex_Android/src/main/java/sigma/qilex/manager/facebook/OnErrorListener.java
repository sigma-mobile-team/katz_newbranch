
package sigma.qilex.manager.facebook;

public interface OnErrorListener {

    void onException(Throwable throwable);

    void onFail(String reason);
}
