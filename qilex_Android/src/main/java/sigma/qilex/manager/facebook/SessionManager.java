
package sigma.qilex.manager.facebook;

import java.lang.ref.WeakReference;
import java.util.List;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import sigma.qilex.utils.LogUtils;

public class SessionManager {
    private final static Class<?> TAG = SessionManager.class;

    private static final String TAG_ = "binhdt";

    private WeakReference<Activity> mActivity;

    private final LoginManager mLoginManager;

    private final LoginCallback mLoginCallback = new LoginCallback();

    private final CallbackManager mCallbackManager = CallbackManager.Factory.create();

    public class LoginCallback implements FacebookCallback<LoginResult> {

        public OnLoginListener loginListener;

        boolean doOnLogin = false;

        @Override
        public void onSuccess(LoginResult loginResult) {
            if (loginListener != null) {
                loginListener.onLogin(loginResult.getAccessToken());

            }
        }

        @Override
        public void onCancel() {
            loginListener.onFail("User canceled the permissions dialog");
        }

        @Override
        public void onError(FacebookException e) {
            loginListener.onException(e);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public SessionManager() {
        mLoginManager = LoginManager.getInstance();
        mLoginManager.registerCallback(mCallbackManager, mLoginCallback);
    }

    public void login(OnLoginListener onLoginListener, List<String> permissions) {
        if (onLoginListener == null) {
            LogUtils.d(TAG_, "OnLoginListener can't be null in -> 'login(OnLoginListener onLoginListener)' method.");
            return;
        }

        if (isLogin()) {
            LogUtils.d(TAG_, "You were already logged in before calling 'login()' method.");
            LoginResult loginResult = createLastLoginResult();
            onLoginListener.onLogin(loginResult.getAccessToken());
            return;
        }

        // just do the login
        loginImpl(onLoginListener, permissions);
    }

    private void loginImpl(OnLoginListener onLoginListener, List<String> permissions) {

        // user hasn't the access token with all read acceptedPermissions we
        // need, thus we ask him to login
        mLoginCallback.loginListener = onLoginListener;

        // login please, with all read permissions
        requestReadPermissions(permissions);
    }

    public void requestReadPermissions(List<String> permissions) {
        mLoginManager.logInWithReadPermissions(mActivity.get(), permissions);
    }

    private LoginResult createLastLoginResult() {
        return new LoginResult(getAccessToken(), getAccessToken().getPermissions(),
                getAccessToken().getDeclinedPermissions());
    }

    public void logout(OnLogoutListener onLogoutListener) {
        if (onLogoutListener == null) {
            LogUtils.d(TAG_, "OnLogoutListener can't be null in -> 'logout(OnLogoutListener onLogoutListener)' method");
            return;
        }

        mLoginManager.logOut();
        onLogoutListener.onLogout();
    }

    public boolean isLogin() {
        AccessToken accessToken = getAccessToken();
        if (accessToken == null) {
            return false;
        }
        return !accessToken.isExpired();
    }

    public AccessToken getAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }

    void setActivity(Activity activity) {
        mActivity = new WeakReference<Activity>(activity);
    }
}
