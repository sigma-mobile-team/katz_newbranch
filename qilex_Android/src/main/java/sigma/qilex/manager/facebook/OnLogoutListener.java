
package sigma.qilex.manager.facebook;

public interface OnLogoutListener {

    void onLogout();
}
