
package sigma.qilex.manager.account;

import sigma.qilex.dataaccess.model.http.ErrorModel;

public interface SendActivationListener {
    public final int ERROR_RESPONSE_NULL = -1;

    public final int ERROR_NETWORK = -2;

    public final int ERROR_TIMEOUT = -3;

    public final int ERROR_WRONG_AUTHEN = -4;

    public final int ERROR_OTHER = -10;

    public void onSendActivationFinish(boolean isSuccess, boolean isFrifon, ErrorModel error);
}
