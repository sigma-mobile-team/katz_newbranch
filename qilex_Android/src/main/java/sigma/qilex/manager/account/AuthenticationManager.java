
package sigma.qilex.manager.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings.Secure;
import pl.katz.aero2.Const;
import pl.katz.aero2.Const.Config;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import pl.katz.aero2.gcm.RegistrationIntentService;
import sigma.qilex.dataaccess.model.PaymentModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.GooglePlayProductListModel;
import sigma.qilex.dataaccess.model.http.LoginResponseModel;
import sigma.qilex.dataaccess.model.http.PurchasePrepaidModel;
import sigma.qilex.dataaccess.model.http.UserInfoResponseModel;
import sigma.qilex.dataaccess.network.http.AccountBalanceUtils;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest.QilexHttpRequestListener;
import sigma.qilex.dataaccess.network.http.UserActivationUtils;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipManager;
import sigma.qilex.sip.SipRegistrationListener;
import sigma.qilex.sip.resiprocate.ResiprocateJni;
import sigma.qilex.sip.resiprocate.SipContext;
import sigma.qilex.sip.resiprocate.Statemachine;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.SimUtils;
import sigma.qilex.utils.Utils;

/**
 * This class has same responsibilities with AccountManager, AccountManager is
 * used for login with old Halo server.
 */
public class AuthenticationManager implements SipRegistrationListener {

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String DBG_TAG = "AccountService";

    private static final String JSON_KEY_MSG = "msg";

    private static final long RE_REGISTER_DELAY = 10000;

    private static AuthenticationManager instance;

    public static AuthenticationManager getInstance() {
        if (instance == null) {
            instance = new AuthenticationManager();
        }
        return instance;
    }

    private boolean isLoggedIn;

    private boolean isRegistSip;

    private Vector<QilexLoginListener> mQilexLoginListeners = new Vector<QilexLoginListener>();

    private Vector<QilexServiceRegistrationListener> mServiceRegistrationListener = new Vector<QilexServiceRegistrationListener>();

    private Vector<GetUserInfoListener> mGetUserInfoListener = new Vector<GetUserInfoListener>();

    private SipManager mSipManager;

    private Timer reRegisterTimer;

    private TimerTask reRegisterTimerTask;

    private HashMap<String, PaymentModel> mListPendingPayment = new HashMap<>();

    private AuthenticationManager() {
        isLoggedIn = false;
        isRegistSip = false;
        mSipManager = SipManager.getInstance(MyApplication.getAppContext());
        reRegisterTimer = new Timer();

        // Load pending payment
        String paymentStr = SharedPrefrerenceFactory.getInstance().getPaymentResend();
        if (Utils.isStringNullOrEmpty(paymentStr) == false) {
            ArrayList<PaymentModel> listPayment = PaymentModel.loadFromJsonArray(paymentStr);
            for (PaymentModel paymentModel : listPayment) {
                mListPendingPayment.put(paymentModel.base64Payment, paymentModel);
            }
        }
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public static String getSyncAccountName() {
        return UserInfo.getInstance().getPhoneNumberFormatted();
    }

    public void addNewAccount(Context activity) {
        // Create Account
        Account newAccount = new Account(getSyncAccountName(), ContactManager.ACCOUNT_TYPE());
        AccountManager accountManager = (AccountManager)activity.getSystemService(Context.ACCOUNT_SERVICE);
        accountManager.addAccountExplicitly(newAccount, null, null);
        ContentResolver.setSyncAutomatically(newAccount, ContactsContract.AUTHORITY, true);

        // Period sync
        ContentResolver.addPeriodicSync(newAccount, "com.android.contacts", Bundle.EMPTY,
                Config.TIME_SECOND_SYNC_CONTACT_PERIOD);
    }

    @SuppressWarnings("deprecation")
    public void removeCurrentAccount(Context context) {
        AccountManager accountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accs = accountManager.getAccountsByType(ContactManager.ACCOUNT_TYPE());
        if (Utils.hasLlMr1Above()) {
            for (Account account : accs) {
                accountManager.removeAccountExplicitly(account);
            }
        } else {
            for (Account account : accs) {
                accountManager.removeAccount(account, null, null);
            }
        }
    }

    /**
     * Method send activation to server.
     * 
     * @param phoneNoWithCountryCode
     * @param listener
     */
    public void sendActivationPhone(final String phoneNoWithCountryCode, boolean isMasterDevice,
            final SendActivationListener listener) {
        // Create Request Listener
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        boolean result = false;
                        boolean isFrifon = false;
                        ErrorModel error = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                if (response.isNull(JSON_KEY_MSG) == false) {
                                    result = true;
                                    error = null;
                                    isFrifon = response.has("has_frifon") ? response.getBoolean("has_frifon") : false;

                                    // Save imsi
                                    if (!Utils.isInKatzMode()) {
                                        SharedPrefrerenceFactory.getInstance().saveLoginPhoneAndImsi(
                                                phoneNoWithCountryCode, SimUtils.getIMSI());
                                    }
                                } else {
                                    error = new ErrorModel(message);
                                    result = false;
                                }
                            } catch (JSONException e) {
                                result = false;
                                error = new ErrorModel();
                            }
                        } else {
                            result = false;
                            error = new ErrorModel();
                        }
                        listener.onSendActivationFinish(result, isFrifon, error);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onSendActivationFinish(false, false, new ErrorModel());
                        break;
                }
            }
        };

        // Send activation to server
        UserActivationUtils.sendActivation(phoneNoWithCountryCode, isMasterDevice, httpRequestListener);
    }

    public void sendActivationCode(String phoneNoWithCountryCode, String activationCode,
            final SendActivationListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        boolean result = false;
                        ErrorModel error = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                if (response.isNull(JSON_KEY_MSG) == false) {
                                    result = true;
                                    error = null;
                                } else {
                                    error = new ErrorModel(message);
                                    result = false;
                                }
                            } catch (JSONException e) {
                                result = false;
                                error = new ErrorModel();
                            }
                        } else {
                            result = false;
                            error = new ErrorModel();
                        }
                        listener.onSendActivationFinish(result, true, error);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onSendActivationFinish(false, false, new ErrorModel());
                        break;
                }
            }
        };

        UserActivationUtils.enterActivation(phoneNoWithCountryCode, activationCode, httpRequestListener);
    }

    /**
     * Method send activation to server.
     * 
     * @param phoneNoWithCountryCode
     * @param listener
     */
    public int sendUnregister(String phoneNoWithCountryCode, final SendActivationListener listener) {
        // Create Request Listener
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        boolean result = false;
                        ErrorModel error = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                if (response.isNull(JSON_KEY_MSG) == false) {
                                    result = true;
                                    error = null;
                                } else {
                                    result = false;
                                    error = new ErrorModel(message);
                                }
                            } catch (JSONException e) {
                                result = false;
                                error = new ErrorModel();
                            }
                        } else {
                            result = false;
                            error = new ErrorModel();
                        }
                        listener.onSendActivationFinish(result, false, error);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onSendActivationFinish(false, false, new ErrorModel());
                        break;
                }
            }
        };

        // Send activation to server
        QilexHttpRequest httpRequest = UserActivationUtils.sendUnregister(phoneNoWithCountryCode, httpRequestListener);
        return httpRequest.getRequestId();
    }

    public void loginWithPhoneNumber(final String phoneNoWithCountryCode) {
        // Create request handler
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_SENDING_REQUEST:
                    case QilexHttpRequest.STATE_GETTING_REPONSE:
                        // start login
                        isLoggedIn = true;
                        break;
                    case QilexHttpRequest.STATE_COMPETED:
                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                LoginResponseModel model = new LoginResponseModel(response);
                                // Update login result to Qilex Profile
                                UserInfo.getInstance().getLocalProfile().mPrivKey = ResiprocateJni.privateKey();
                                UserInfo.getInstance().getLocalProfile().mPubKey = ResiprocateJni.publicKey();
                                UserInfo.getInstance().getLocalProfile().setLogin(phoneNoWithCountryCode);
                                UserInfo.getInstance().getLocalProfile().updateFromLoginResponse(model);
                                UserInfo.getInstance().plusPromotionCalls = model.plus_promotion_calls;
                                SharedPrefrerenceFactory.getInstance().setCacheLogin(message);
                                QilexProfile profile = UserInfo.getInstance().getLocalProfile();
                                SharedPrefrerenceFactory.getInstance().setCachePrivPublicKey(profile.mPrivKey,
                                        profile.mPubKey);

                                // Save imsi
                                if (!Utils.isInKatzMode()) {
                                    SharedPrefrerenceFactory.getInstance().saveLoginPhoneAndImsi(phoneNoWithCountryCode, SimUtils.getIMSI());
                                }

                                // Login success
                                fireQiLexLoginSuccess(model);
                            } catch (JSONException e) {
                                // Compile error code
                                ErrorModel errors = new ErrorModel(message);
                                fireQilexLoginFail(errors.errors[0].code, errors.errors[0].getCurrentErrorMessage());
                            }
                        } else {
                            ErrorModel errors = new ErrorModel();
                            fireQilexLoginFail(errors.errors[0].code, errors.errors[0].getCurrentErrorMessage());
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errors = new ErrorModel();
                        fireQilexLoginFail(errors.errors[0].code, errors.errors[0].getCurrentErrorMessage());
                        break;
                }
            }
        };

        UserActivationUtils.login(phoneNoWithCountryCode, httpRequestListener);
    }

    public void notifyLoginSuccess() {
        isLoggedIn = true;
        // Login success
        String message = SharedPrefrerenceFactory.getInstance().getCacheLoginData();
        if (message == null) {
            return;
        }
        JSONObject response;
        try {
            // Generate public key and private key
            QilexProfile profile = UserInfo.getInstance().getLocalProfile();
            // KeyPair pair;
            // try {
            // pair = Utils.keyGenerator("RSA");
            // profile.setLogin(UserInfo.getInstance().getPhoneNumberFormatted());
            // profile.mPrivKey = Utils.derToPemPrivateKey(pair);
            // profile.mPubKey = Utils.derToPemPublicKey(pair);
            // } catch (Exception e) {
            // e.printStackTrace();
            // }

            String[] keys = SharedPrefrerenceFactory.getInstance().getCachePrivPublicKey();
            profile.mPrivKey = keys[0];
            profile.mPubKey = keys[1];
            response = new JSONObject(message);
            LoginResponseModel model = new LoginResponseModel(response);

            // Update login result to Qilex Profile
            UserInfo.getInstance().getLocalProfile().updateFromLoginResponse(model);
            UserInfo.getInstance().setPushToken(SharedPrefrerenceFactory.getInstance().getRegistrationId());
            LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---START " + message);
            registerSip(UserInfo.getInstance().getLocalProfile(), false);
        } catch (JSONException e) {
            LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---EXCEPTION");
            e.printStackTrace();
        }
    }

    public int setUpUserInfo(String number, String userName, String userLastName, String userAvt, String formatAvt,
            final SetupUserInfoListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        boolean result = false;
                        ErrorModel error = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());

                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                if (response.isNull(JSON_KEY_MSG) == false) {
                                    result = true;
                                    error = null;
                                } else {
                                    result = false;
                                    error = new ErrorModel(response);
                                }
                            } catch (JSONException e) {
                                result = false;
                                error = new ErrorModel();
                            }
                        } else {
                            result = false;
                            error = new ErrorModel();
                        }
                        listener.onSetupUserInfoFinish(result, error);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onSetupUserInfoFinish(false, new ErrorModel());
                        break;
                }
            }
        };

        QilexHttpRequest request = UserActivationUtils.setUpUserInfo(number, userName, userLastName, userAvt, formatAvt,
                httpRequestListener);
        return request.getRequestId();
    }

    public void getUserAccountBalance(String number, final GetAccountBalanceListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                String msisdn = response.getString("msisdn");
                                double balance = response.getDouble("balance");
                                listener.onGetAccountBalanceFinish(msisdn, balance, null);
                            } catch (JSONException e) {
                                ErrorModel error = new ErrorModel(message);
                                listener.onGetAccountBalanceFinish(null, 0, error);
                            }
                        } else {
                            listener.onGetAccountBalanceFinish(null, 0, new ErrorModel());
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onGetAccountBalanceFinish(null, 0, new ErrorModel());
                        break;
                }
            }
        };

        AccountBalanceUtils.getAccountBalance(number, httpRequestListener);
    }

    public int getUserAccountPrepaidHistory(int page, int onPage, final GetListPurchaseListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                PurchasePrepaidModel prepaid = new PurchasePrepaidModel(response);
                                listener.onGetListPurchaseFinish(prepaid, null);
                            } catch (JSONException e) {
                                ErrorModel model = new ErrorModel(message);
                                listener.onGetListPurchaseFinish(null, model);
                            }
                        } else {
                            ErrorModel errorServer = new ErrorModel();
                            errorServer.errors[0].dmsg = MyApplication.getAppContext()
                                    .getString(R.string.connection_error);
                            listener.onGetListPurchaseFinish(null, errorServer);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        ErrorModel errorNetwork = new ErrorModel();
                        errorNetwork.errors[0].dmsg = MyApplication.getAppContext()
                                .getString(R.string.connection_error);
                        listener.onGetListPurchaseFinish(null, errorNetwork);
                        break;
                }
            }
        };

        QilexHttpRequest qhttpHttpRequest = AccountBalanceUtils.getAccountPrepaidHistory(
                UserInfo.getInstance().getPhoneNumberFormatted(), page, onPage, httpRequestListener);
        if (qhttpHttpRequest != null) {
            return qhttpHttpRequest.getRequestId();
        } else {
            return -1;
        }
    }

    public PaymentModel getResendPayment() {
        if (mListPendingPayment.isEmpty()) {
            return null;
        }
        ArrayList<PaymentModel> listPaymentModel = new ArrayList<>(mListPendingPayment.values());
        return listPaymentModel.get(0);
    }

    private void savePendingPayment() {
        synchronized (mListPendingPayment) {
            JSONArray jsonPending = PaymentModel.toJsonArray(new ArrayList<>(mListPendingPayment.values()));
            String saveString = Const.STR_EMPTY;
            if (jsonPending != null) {
                saveString = jsonPending.toString();
            }
            SharedPrefrerenceFactory.getInstance().setPaymentResend(saveString);
        }
    }

    public void postAccountPaymentInfo(Context context, String number, final String base64Payment, String appstoreInfo,
            String userData, String googleProductId, String appStoreProductId, final PostPaymentListener listener) {
        // Save to cache
        synchronized (mListPendingPayment) {
            PaymentModel payment = new PaymentModel(appstoreInfo, userData, base64Payment, googleProductId,
                    appStoreProductId);
            mListPendingPayment.put(base64Payment, payment);
        }
        savePendingPayment();

        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        boolean result = false;
                        ErrorModel error = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                if (response.isNull(JSON_KEY_MSG) == false) {
                                    result = true;
                                    error = null;
                                } else {
                                    result = false;
                                    error = new ErrorModel(response);
                                }
                            } catch (JSONException e) {
                                result = false;
                                error = new ErrorModel();
                            }

                            // Remove from list pending payment
                            synchronized (mListPendingPayment) {
                                mListPendingPayment.remove(base64Payment);
                            }
                            savePendingPayment();
                        } else {
                            result = false;
                            error = new ErrorModel();
                        }
                        listener.onPostPaymentFinish(result, error);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onPostPaymentFinish(false, new ErrorModel());
                        break;
                }
            }
        };

        AccountBalanceUtils.postAccountPaymentInfo(number, base64Payment, appstoreInfo, userData, googleProductId,
                appStoreProductId, httpRequestListener);
    }

    public void getUserInfo(String phoneNo) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);

                                UserInfoResponseModel userInfo = new UserInfoResponseModel(response);

                                fireGetUserInfoFinish(false, userInfo);
                            } catch (JSONException e) {
                                fireGetUserInfoFinish(false, null);
                            }
                        } else {
                            fireGetUserInfoFinish(false, null);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        fireGetUserInfoFinish(true, null);
                        break;
                }
            }
        };

        UserActivationUtils.getAccountInfo(phoneNo, httpRequestListener);
    }

    public int getUserInfo(Context context, String phoneNo, final GetUserInfoListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);

                                UserInfoResponseModel userInfo = new UserInfoResponseModel(response);

                                listener.onGetUserInfoFinish(false, userInfo);
                            } catch (JSONException e) {
                                listener.onGetUserInfoFinish(false, null);
                            }
                        } else {
                            listener.onGetUserInfoFinish(false, null);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onGetUserInfoFinish(true, null);
                        break;
                }
            }
        };

        QilexHttpRequest httpRequest = UserActivationUtils.getAccountInfo(phoneNo, httpRequestListener);
        return httpRequest.getRequestId();
    }

    public int getGooglePlayProductList(Context context, final GetProductListListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONArray response = new JSONArray(message);
                                int size = response.length();
                                ArrayList<GooglePlayProductListModel> listProduct = new ArrayList<GooglePlayProductListModel>();
                                for (int i = 0; i < size; i++) {
                                    JSONObject jsonObj = response.getJSONObject(i);
                                    GooglePlayProductListModel model = new GooglePlayProductListModel(jsonObj);
                                    listProduct.add(model);
                                }

                                listener.onGetProductListFinish(null, listProduct);
                            } catch (JSONException e) {
                                listener.onGetProductListFinish(new ErrorModel(message), null);
                            }
                        } else {
                            listener.onGetProductListFinish(new ErrorModel(), null);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onGetProductListFinish(new ErrorModel(), null);
                        break;
                }
            }
        };

        QilexHttpRequest request = AccountBalanceUtils.getGooglePlayProductList(httpRequestListener);
        return request.getRequestId();
    }

    public int requestCheckFrifonNumber(String phoneNumberWithCountryCode, final CheckIsFrifonNumberListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                boolean isFrifon = response.getBoolean("has_frifon");

                                listener.onCheckIsFrifonNumberFinish(0, isFrifon);
                            } catch (JSONException e) {
                                listener.onCheckIsFrifonNumberFinish(-1, false);
                            }
                        } else {
                            listener.onCheckIsFrifonNumberFinish(-1, false);
                        }
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onCheckIsFrifonNumberFinish(-1, false);
                        break;
                }
            }
        };

        QilexHttpRequest request = UserActivationUtils.checkIsFrifon(phoneNumberWithCountryCode, httpRequestListener);
        return request.getRequestId();
    }

    public int sendAcceptOutRegulation(Context context, boolean regOutAccept, final AcceptRegulationListener listener) {
        return sendAcceptRegulation(context, UserInfo.getInstance().getLocalProfile().isRegulationAccepted(),
                regOutAccept, listener);
    }

    public int sendAcceptRegulation(Context context, boolean regAccept, boolean regOutAccept,
            final AcceptRegulationListener listener) {
        QilexHttpRequestListener httpRequestListener = new QilexHttpRequestListener() {
            @Override
            public void httpRequestStateChanged(QilexHttpRequest req) {
                switch (req.getState()) {
                    case QilexHttpRequest.STATE_COMPETED:
                        boolean result = false;
                        ErrorModel error = null;

                        // Check response data null.
                        if (req.getResponseData() != null) {
                            String message = new String(req.getResponseData());
                            try {
                                // Check response data is success/error or
                                // cannot parse
                                JSONObject response = new JSONObject(message);
                                if (response.isNull(JSON_KEY_MSG) == false) {
                                    result = true;
                                    UserInfo.getInstance().getLocalProfile().updateOutRegulationAcceptedVersion();
                                    error = null;
                                } else {
                                    error = new ErrorModel(message);
                                    result = false;
                                }
                            } catch (JSONException e) {
                                result = false;
                                error = new ErrorModel();
                            }
                        } else {
                            result = false;
                            error = new ErrorModel();
                        }
                        listener.onAcceptRegulationFinish(result, error);
                        break;
                    case QilexHttpRequest.STATE_ERROR:
                    case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    case QilexHttpRequest.STATE_ERROR_DATE:
                        listener.onAcceptRegulationFinish(false, new ErrorModel());
                        break;
                }
            }
        };
        String phoneNo = UserInfo.getInstance().getPhoneNumberFormatted();
        QilexHttpRequest request = UserActivationUtils.sendAcceptRegulation(phoneNo, regAccept, regOutAccept,
                httpRequestListener);
        return request.getRequestId();
    }

    public synchronized boolean registerSip(QilexProfile profile, boolean restartSipStack) {
        // Check network state
        if (QilexService.isReady() && !NetworkStateMonitor.isNetworkExist()) {
            return false;
        }
        // if (SharedPrefrerenceFactory.getInstance().getCacheLoginData() ==
        // null) {
        // return false;
        // }
        if (isLoggedIn == false) {
            return false;
        }
        // Call register SIP
        QilexSipProfile sipProfile = profile.getDefaultSipProfile();
        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---START HE");
        mSipManager.register(sipProfile, profile.getRegistrationExpireTime(), profile.getRepublishFreq(),
                profile.getResubsfreq(), this, restartSipStack);
        return true;
    }

    /**
     * Register notification data to get Push Notification infomation.
     */
    public synchronized void registerGCM() {
        if (Utils.checkPlayServicesExist()) {
            Intent intent = new Intent(MyApplication.getAppContext(), RegistrationIntentService.class);
            MyApplication.getAppContext().startService(intent);
        } else {
            LogUtils.d("KUNLQT", "No valid Google Play Services APK found.");
        }
    }

    public void registerQilexLoginListener(QilexLoginListener listener) {
        if (mQilexLoginListeners.contains(listener) == false) {
            mQilexLoginListeners.addElement(listener);
        }
    }

    public void unregisterQilexLoginListener(QilexLoginListener listener) {
        mQilexLoginListeners.removeElement(listener);
    }

    public void registerGetUserInfoListener(GetUserInfoListener listener) {
        if (mGetUserInfoListener.contains(listener) == false) {
            mGetUserInfoListener.addElement(listener);
        }
    }

    public void unregisterGetUserInfoListener(GetUserInfoListener listener) {
        mGetUserInfoListener.removeElement(listener);
    }

    private void fireGetUserInfoFinish(boolean isNetWorkError, UserInfoResponseModel model) {
        for (Iterator<GetUserInfoListener> i = mGetUserInfoListener.iterator(); i.hasNext();) {
            GetUserInfoListener l = null;
            try {
                l = i.next();
                l.onGetUserInfoFinish(isNetWorkError, model);
            } catch (Throwable t) {
                LogUtils.d("Login Exception", t.toString());
            }
        }
    }

    private void fireQiLexLoginSuccess(LoginResponseModel model) {
        isLoggedIn = true;
        for (Iterator<QilexLoginListener> i = mQilexLoginListeners.iterator(); i.hasNext();) {
            QilexLoginListener l = null;
            try {
                l = i.next();
                l.onLoginSuccess(model);
            } catch (Throwable t) {
                LogUtils.d("Login Exception", t.toString());
            }
        }
    }

    private void fireQilexLoginFail(int errorCode, String errorMessage) {
        isLoggedIn = false;
        for (Iterator<QilexLoginListener> i = mQilexLoginListeners.iterator(); i.hasNext();) {
            QilexLoginListener l = null;
            try {
                l = i.next();
                l.onLoginFail(errorCode, errorMessage);
            } catch (Throwable t) {
                LogUtils.d("Login Exception", t.toString());
            }
        }
    }

    public synchronized void registerServiceRegistrationListener(QilexServiceRegistrationListener listener) {
        if (mServiceRegistrationListener.contains(listener) == false) {
            mServiceRegistrationListener.addElement(listener);
        }
    }

    public synchronized void unregisterServiceRegistrationListener(QilexServiceRegistrationListener listener) {
        mServiceRegistrationListener.removeElement(listener);
    }

    /**
     * Method handler event when registration is started.
     *
     * @param localUserProfile
     */
    private synchronized void fireOnQilexServiceRegitrationStarted(QilexProfile localUserProfile) {
        for (Iterator<QilexServiceRegistrationListener> i = mServiceRegistrationListener.iterator(); i.hasNext();) {
            QilexServiceRegistrationListener l = null;
            try {
                l = i.next();
                l.onQilexServiceRegitrationStarted(localUserProfile);
            } catch (Throwable t) {
                LogUtils.e(DBG_TAG, "onQilexServiceRegitrationStarted in class: " + l.getClass().getCanonicalName(), t);
            }
        }
    }

    /**
     * Method handler when registration finish.
     *
     * @param localUserProfile
     */
    private synchronized void fireOnQilexServiceRegitrationDone(QilexProfile localUserProfile) {
        for (Iterator<QilexServiceRegistrationListener> i = mServiceRegistrationListener.iterator(); i.hasNext();) {
            QilexServiceRegistrationListener l = null;
            try {
                l = i.next();
                l.onQilexServiceRegitrationDone(localUserProfile);
            } catch (Throwable t) {
                LogUtils.e(DBG_TAG, "onQilexServiceRegitrationDone in class: " + l.getClass().getCanonicalName(), t);
            }
        }
    }

    /**
     * Call handler when registration has error.
     *
     * @param localUserProfile
     * @param reason
     * @param msg
     */
    private synchronized void fireOnQilexServiceRegitrationError(QilexProfile localUserProfile, int reason,
            String msg) {
        for (Iterator<QilexServiceRegistrationListener> i = mServiceRegistrationListener.iterator(); i.hasNext();) {
            QilexServiceRegistrationListener l = null;
            try {
                l = i.next();
                l.onQilexServiceRegitrationError(localUserProfile, reason, msg);
            } catch (Throwable t) {
                LogUtils.e(DBG_TAG, "onQilexServiceRegitrationError in class: " + l.getClass().getCanonicalName(), t);
            }
        }
    }

    public void logout() {
        // Set is Login to false
        isLoggedIn = false;

        // Send blank Push Token and register to server
        QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
        UserInfo.getInstance().setPushToken("");
        registerSip(localProfile, false);
        SharedPrefrerenceFactory.getInstance().setCacheLogin(null);

        // Call unregister
        mSipManager.unregister(localProfile.getDefaultSipProfile(), this);

        // Cancel all sync progress
        Context context = MyApplication.getAppContext();
        AccountManager accountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accs = accountManager.getAccountsByType(ContactManager.ACCOUNT_TYPE());
        for (Account account : accs) {
            ContentResolver.cancelSync(account, ContactsContract.AUTHORITY);
            ContentResolver.removePeriodicSync(account, ContactsContract.AUTHORITY, Bundle.EMPTY);
        }
    }

    public void deactivate() {
        // Clear Image Memory cache
        MyApplication.getImageLoader().clearMemoryCache();
        // LoginManager.getInstance().logOut();

        // Remove Account
        removeCurrentAccount(MyApplication.getAppContext());

        // Set first time sync to false
        SharedPrefrerenceFactory.getInstance().setFirstSyncToFalse();
        SharedPrefrerenceFactory.getInstance().saveEmailFb("");
    }

    @Override
    public void onRegistering(QilexSipProfile localProfile) {
        if (isLoggedIn == false) {
            return;
        }
        QilexProfile qilexLocalProfile = UserInfo.getInstance().getLocalProfile();

        fireOnQilexServiceRegitrationStarted(qilexLocalProfile);
    }

    @Override
    public void onRegistrationDone(QilexSipProfile localProfile, long expiryTime) {
        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---DONE");
        // Stop restart register timer
        if (reRegisterTimerTask != null) {
            reRegisterTimerTask.cancel();
            reRegisterTimerTask = null;
            reRegisterTimer.cancel();
            reRegisterTimer = null;
        }
        if (isLoggedIn == false) {
            return;
        }
        isRegistSip = true;

        QilexProfile qilexLocalProfile = UserInfo.getInstance().getLocalProfile();

        // Fire Registration Done
        fireOnQilexServiceRegitrationDone(qilexLocalProfile);
    }

    @Override
    public void onRegistrationFailed(QilexSipProfile localProfile, int errorCode, String errorMessage) {
        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---FAIL " + errorCode);
        if (isLoggedIn == false) {
            return;
        }
        isRegistSip = false;
        QilexProfile qilexLocalProfile = UserInfo.getInstance().getLocalProfile();
        fireOnQilexServiceRegitrationError(qilexLocalProfile, errorCode, errorMessage);
        switch (errorCode) {
            case SipRegistrationListener.ERR_CANCELED:
            case SipRegistrationListener.ERR_UNKNOWN_ERROR:
                // TODO: Handler unknown error
                break;
            case SipRegistrationListener.ERR_SERVICE_FORBIDDEN:
            case SipRegistrationListener.ERR_BAD_ACCOUNT_DATA: {
                // TODO: Handler service forbidden and bad account data
            }
                break;
            case SipRegistrationListener.ERR_TIMEOUT:
                // Restart SIP stack
                if (reRegisterTimerTask != null) {
                    reRegisterTimerTask.cancel();
                    reRegisterTimerTask = null;
                    reRegisterTimer.cancel();
                    reRegisterTimer = null;
                }
                reRegisterTimer = new Timer();
                reRegisterTimerTask = new TimerTask() {
                    @Override
                    public void run() {
                        registerSip(UserInfo.getInstance().getLocalProfile(), true);
                    }
                };
                reRegisterTimer.schedule(reRegisterTimerTask, RE_REGISTER_DELAY);
            case SipRegistrationListener.ERR_NETWORK_DOWN:
                break;

            default:
                throw new AssertionError("QilexService->mSipRegistrationListener->onRegistrationFailed: BAD errorCode");
        }
    }

    public void registerSipDirectly() {
        SipContext mContext = Statemachine.mContext;
        QilexSipProfile sipProfile = UserInfo.getInstance().getLocalProfile().getDefaultSipProfile();
        Statemachine.mContext.mLocalProfile = sipProfile;
        Statemachine.mContext.mRegistrationListener = this;

        Statemachine.mContext.mRegistrationExpiryTime = UserInfo.getInstance().getLocalProfile()
                .getRegistrationExpireTime();
        Statemachine.mContext.mPublishRefreshTime = UserInfo.getInstance().getLocalProfile().getRepublishFreq();
        Statemachine.mContext.mSubscribeRefreshTime = UserInfo.getInstance().getLocalProfile().getResubsfreq();
        Statemachine.mContext.mSipstackRestart = true;

        if (mContext.mLocalProfile != null) {
            Vector<String> aliases = mContext.mLocalProfile.getAliases();
            String aliasesArray[] = null;
            if (aliases != null) {
                aliasesArray = new String[aliases.size()];
                System.arraycopy(aliases.toArray(), 0, aliasesArray, 0, aliases.size());
            }

            mContext.mResiprcocateJni.resetServerIPs();

            for (int i = 0; i < QilexProfile.sipServersCount(); i++) {
                ServerAddr server = QilexProfile.getSipServer(i);
                mContext.mResiprcocateJni.addSipServer(server.getAddr(), server.getPort());
            }

            for (int i = 0; i < QilexProfile.turnServersCount(); i++) {
                ServerAddr server = QilexProfile.getTurnServer(i);
                mContext.mResiprcocateJni.addTurnServer(server.getAddr(), server.getPort());
            }

            // Get push notification device info
            String pushOs = "Android";
            String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
            String pushToken = UserInfo.getInstance().getPushToken();

            // Phuc add for debug - END
            mContext.mResiprcocateJni.register(mContext.mLocalProfile.getLogin(), mContext.mLocalProfile.getPassword(),
                    mContext.mLocalProfile.getInstanceId(), mContext.mLocalProfile.getToken(),
                    mContext.mRegistrationExpiryTime, mContext.mPublishRefreshTime, mContext.mSubscribeRefreshTime,
                    null, aliasesArray, mContext.mLocalProfile.getPrivateKey(), mContext.mLocalProfile.getCertificate(),
                    mContext.mLocalProfile.getPublicKey(), pushOs, deviceId, pushToken);
        }
    }

    public boolean isRegistSip() {
        return isRegistSip;
    }

    @Override
    public void onProfileUnregistered(QilexSipProfile localProfile) {
    }

    public interface GetUserInfoListener {
        public void onGetUserInfoFinish(boolean isNetworkError, UserInfoResponseModel userInfo);
    }

    public interface CheckIsFrifonNumberListener {
        public void onCheckIsFrifonNumberFinish(int errorCode, boolean result);
    }

    public interface PostPaymentListener {
        public void onPostPaymentFinish(boolean isSuccess, ErrorModel error);
    }

    public interface AcceptRegulationListener {
        public void onAcceptRegulationFinish(boolean isSuccess, ErrorModel error);
    }
}
