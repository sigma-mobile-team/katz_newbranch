
package sigma.qilex.manager.account;

import sigma.qilex.dataaccess.model.http.LoginResponseModel;

public interface QilexLoginListener {

    public void onLoginSuccess(LoginResponseModel model);

    public void onLoginFail(int errorCode, String errorMessage);
}
