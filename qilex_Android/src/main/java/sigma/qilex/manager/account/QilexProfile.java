
package sigma.qilex.manager.account;

import java.util.Calendar;
import java.util.Vector;

import android.content.Context;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.LoginResponseModel;
import sigma.qilex.sip.QilexSipProfile;

public class QilexProfile {
    public static final String DBG_TAG = "QilexService";

    private String mLogin;

    private String mPassword;

    private boolean mVerified = false;

    private Vector<QilexSipProfile> mSipProfiles = new Vector<QilexSipProfile>();

    protected long mTimestamp;

    // user_info
    protected String mUserInfoIcokToken;

    protected String mSipToken;

    // CERTIFICATE
    protected String mCertificate;

    // aliases
    protected Vector<String> mAliases = new Vector<String>();

    // sipservers
    protected static Vector<ServerAddr> mSipServers = new Vector<ServerAddr>();

    // turnservers
    protected static Vector<ServerAddr> mTurnServers = new Vector<ServerAddr>();

    // klucze pub/priv
    public String mPrivKey;

    public String mPubKey;

    // parametry odswieżania rejestracji, subskrybcji, publikacji stat.
    protected int mReregfreq = 300;

    protected int mRepublfreq = 300;

    protected int mResubsfreq = 3600;

    protected String mGeoredeUrl;

    protected boolean mGeoredeOfflineHits;

    protected int mGeoredeAccuracy;

    protected double mOutRegulationCurrentVersion;

    protected double mOutRegulationAcceptedVersion;

    protected String mOutRegulationUrl = "http://katz.pl/regulations-katzout";

    protected boolean mRegulationAccepted;

    protected String mRegulationUrl;

    public QilexProfile(Context context) {

    }

    public QilexProfile() {

    }

    public void updateFromLoginResponse(LoginResponseModel model) {
        mTimestamp = Calendar.getInstance().getTimeInMillis();
        // mUserInfoIcokToken = l.mUserInfoIcokToken; // 2012-10-12
        // 15:20:40_C0537802-921E-4A34-9371-F703260F0E51
        mAliases = new Vector<String>(); // blank
        mCertificate = model.cert;
        mReregfreq = 300;
        mRepublfreq = 300;
        mResubsfreq = 3600;
        mGeoredeUrl = "http://georede.redefine.pl/hit";
        mGeoredeOfflineHits = false;
        mGeoredeAccuracy = 4;
        mSipToken = model.sip_token;
        mOutRegulationCurrentVersion = model.out_reg_current_version;
        mOutRegulationAcceptedVersion = model.out_reg_accepted_version;
        mOutRegulationUrl = model.out_reg_url;
        mRegulationAccepted = model.reg_accepted;
        mRegulationUrl = model.reg_url;

        String[] strSipServers = model.sip_servers;
        mSipServers = new Vector<ServerAddr>();
        for (String strServer : strSipServers) {
            String[] splits = strServer.split(":");
            ServerAddr add = new ServerAddr(splits[0], splits[1]);
            mSipServers.addElement(add);
        }

        String[] strTurnServers = model.turn_servers;
        mTurnServers = new Vector<ServerAddr>();
        for (String strServer : strTurnServers) {
            String[] splits = strServer.split(":");
            ServerAddr add = new ServerAddr(splits[0], splits[1]);
            mTurnServers.addElement(add);
        }

        mSipProfiles.clear();
        for (int i = 0; i < mSipServers.size(); ++i) {
            QilexSipProfile sipProfile = new QilexSipProfile();
            sipProfile.setLogin(UserInfo.getInstance().getPhoneNumberFormatted());
            sipProfile.setPassword("");
            sipProfile.setToken(mSipToken);
            sipProfile.setCertificate(mCertificate);
            sipProfile.setPublicKey(mPubKey);
            sipProfile.setPrivateKey(mPrivKey);
            ServerAddr sa = mSipServers.elementAt(i);
            sipProfile.setServer(sa.getAddr());
            sipProfile.setPort(sa.getPort());
            sipProfile.setInstanceId("");// "<urn:uuid:00000000-0000-3-000-0000-000000000000>"
                                         // );

            Vector<String> aliases = new Vector<String>();
            sipProfile.setAliases(aliases);
            mSipProfiles.addElement(sipProfile);
        }
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getLogin() {
        return mLogin;
    }

    public String getPassword() {
        return mPassword;
    }

    public String getUserInfoIcokToken() {
        return mUserInfoIcokToken;
    }

    public static final int sipServersCount() {
        return mSipServers.size();
    }

    public static final ServerAddr getSipServer(int idx) {
        return mSipServers.elementAt(idx);
    }

    public static final int turnServersCount() {
        return mTurnServers.size();
    }

    public static final ServerAddr getTurnServer(int idx) {
        return mTurnServers.elementAt(idx);
    }

    public static String getIcokServiceAddress() {
        // return AccountService.ACCOUNT_SERVER_ICOK_SERVICE_ADDR;
        return null;
    }

    /**
     * Zwraca domyślny SipProfile który powienien być używany w danej chwili.
     * 
     * @return obiekt SipProfile
     */
    public QilexSipProfile getDefaultSipProfile() {
        if (mSipProfiles.size() > 0) {
            return mSipProfiles.firstElement(); // na tą chwilę i tak nie bedzie
                                                // wiecej niż jeden
        } else {
            return null;
        }
    }

    public int getRegistrationExpireTime() {
        return mReregfreq;
    }

    public int getRepublishFreq() {
        return mRepublfreq;
    }

    public int getResubsfreq() {
        return mResubsfreq;
    }

    public boolean verified() {
        return mVerified;
    }

    public double getOutRegulationCurrentVersion() {
        return mOutRegulationCurrentVersion;
    }

    public void setOutRegulationCurrentVersion(double mOutRegulationCurrentVersion) {
        this.mOutRegulationCurrentVersion = mOutRegulationCurrentVersion;
    }

    public double getOutRegulationAcceptedVersion() {
        return mOutRegulationAcceptedVersion;
    }

    public void updateOutRegulationAcceptedVersion() {
        this.mOutRegulationAcceptedVersion = mOutRegulationCurrentVersion;
    }

    public String getOutRegulationUrl() {
        return mOutRegulationUrl;
    }

    public void setOutRegulationUrl(String mOutRegulationUrl) {
        this.mOutRegulationUrl = mOutRegulationUrl;
    }

    public boolean isRegulationAccepted() {
        return mRegulationAccepted;
    }

    public void setRegulationAccepted(boolean mRegulationAccepted) {
        this.mRegulationAccepted = mRegulationAccepted;
    }

    public String getRegulationUrl() {
        return mRegulationUrl;
    }

    public void setRegulationUrl(String mRegulationUrl) {
        this.mRegulationUrl = mRegulationUrl;
    }
}
