
package sigma.qilex.manager.account;

import sigma.qilex.sip.SipRegistrationListener;

public interface QilexServiceRegistrationListener {

    public void onQilexServiceRegitrationStarted(QilexProfile localUserProfile);

    public void onQilexServiceRegitrationDone(QilexProfile localUserProfile);

    public void onQilexServiceRegitrationError(QilexProfile localUserProfile, int reason, String msg);

    /** Registration process is cancelled */
    public static final int ERR_REASON_CANCELED = SipRegistrationListener.ERR_CANCELED;

    /** Registration failed with unknown error (Not recognize) */
    public static final int ERR_REASON_UNKNOWN = SipRegistrationListener.ERR_UNKNOWN_ERROR;

    /** Account data is not OK */
    public static final int ERR_REASON_BAD_ACCOUNT_DATA = SipRegistrationListener.ERR_BAD_ACCOUNT_DATA;

    /** Network error */
    public static final int ERR_REASON_NETWORK_DOWN = SipRegistrationListener.ERR_NETWORK_DOWN;

    /**  */
    public static final int ERR_REASON_SERVICE_FORBIDDEN = SipRegistrationListener.ERR_SERVICE_FORBIDDEN;

    /** Request timeout */
    public static final int ERR_REASON_TIMEOUT = SipRegistrationListener.ERR_TIMEOUT;
}
