
package sigma.qilex.manager.account;

import java.util.ArrayList;

import sigma.qilex.dataaccess.model.http.ContactSnapshotModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;

public interface GetContactSnapshotListener {

    public void onGetContactSnapshotFinish(ErrorModel errors, ArrayList<ContactSnapshotModel> listContactSnapshot);
}
