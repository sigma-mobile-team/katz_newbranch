
package sigma.qilex.manager.account;

public class ServerAddr {
    public ServerAddr(String addr, String port) {
        mAddr = addr;
        mPort = Integer.parseInt(port);
    }

    public ServerAddr(String addr, int port) {
        mAddr = addr;
        mPort = port;
    }

    private String mAddr;

    private int mPort;

    public String getAddr() {
        return mAddr;
    }

    public int getPort() {
        return mPort;
    }

    @Override
    public String toString() {
        return mAddr + ":" + mPort;
    }
}
