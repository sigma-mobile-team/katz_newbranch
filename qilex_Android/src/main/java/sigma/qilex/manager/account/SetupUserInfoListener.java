
package sigma.qilex.manager.account;

import sigma.qilex.dataaccess.model.http.ErrorModel;

public interface SetupUserInfoListener {

    public void onSetupUserInfoFinish(boolean isSuccess, ErrorModel error);
}
