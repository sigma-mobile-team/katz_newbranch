
package sigma.qilex.manager.account;

import sigma.qilex.dataaccess.model.http.ErrorModel;

public interface GetAccountBalanceListener {

    public void onGetAccountBalanceFinish(String msisdn, double balance, ErrorModel error);
}
