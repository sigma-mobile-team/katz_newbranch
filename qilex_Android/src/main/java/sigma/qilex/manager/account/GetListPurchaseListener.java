
package sigma.qilex.manager.account;

import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.PurchasePrepaidModel;

public interface GetListPurchaseListener {

    public void onGetListPurchaseFinish(PurchasePrepaidModel listResult, ErrorModel error);
}
