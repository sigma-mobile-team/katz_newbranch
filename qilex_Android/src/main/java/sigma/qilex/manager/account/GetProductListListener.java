
package sigma.qilex.manager.account;

import java.util.ArrayList;

import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.GooglePlayProductListModel;

public interface GetProductListListener {

    public void onGetProductListFinish(ErrorModel error, ArrayList<GooglePlayProductListModel> listProduct);
}
