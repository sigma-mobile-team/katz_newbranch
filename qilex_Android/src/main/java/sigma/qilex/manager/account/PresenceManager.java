
package sigma.qilex.manager.account;

import java.util.Iterator;
import java.util.Vector;

import android.os.Message;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import sigma.qilex.manager.I_ThreadMessageDispatcher;
import sigma.qilex.manager.ThreadMessageDispatcher;
import sigma.qilex.sip.SipManager;
import sigma.qilex.sip.SipPingStatusListener;
import sigma.qilex.sip.SipPresenceListener;
import sigma.qilex.sip.resiprocate.Statemachine;

public class PresenceManager implements SipPingStatusListener, SipPresenceListener {

    private static final int MSG_REPUBLISH_PRESENCE = 1;

    private static final int MSG_REPUBLISH_PRESENCE_DELAY_MILLIS = 10000;

    private static PresenceManager mInstance;

    public static PresenceManager getInstance() {
        if (mInstance == null) {
            mInstance = new PresenceManager();
        }
        return mInstance;
    }

    private SipManager mSipManager;

    private Vector<PingStatusListener> mPingStatusListener = new Vector<PresenceManager.PingStatusListener>();

    private ThreadMessageDispatcher vrcDispatcher;

    private I_ThreadMessageDispatcher l_listenerDispatcher = new I_ThreadMessageDispatcher() {
        public void onHandleMessage(Message vrpMsg) {
            switch (vrpMsg.what) {
                case MSG_REPUBLISH_PRESENCE:
                    pingPresenceToServer();
                    break;
            }
        }
    };

    /**
     * Constructor.
     */
    private PresenceManager() {
        mSipManager = SipManager.getInstance(MyApplication.getAppContext());
        mSipManager.setPresenceListener(this);
        Statemachine.mContext.mSipPingStatusListener = this;
        vrcDispatcher = new ThreadMessageDispatcher(l_listenerDispatcher);
    }

    public void pingToAdrress(String sipAddress) {
        if (AuthenticationManager.getInstance().isRegistSip() == true) {
            mSipManager.pingToAddress(sipAddress);
        }
    }

    public void startRunPingPresence() {
        // pingPresenceToServer();
    }

    public void pingPresenceToServer() {
        vrcDispatcher.removeMessages(MSG_REPUBLISH_PRESENCE);
        vrcDispatcher.sendEmptyMessageDelayed(MSG_REPUBLISH_PRESENCE, MSG_REPUBLISH_PRESENCE_DELAY_MILLIS);
        mSipManager.publishPresence(Status.AVAILABLE);
    }

    public void registerPingStatusListener(PingStatusListener listener) {
        if (mPingStatusListener.contains(listener) == false) {
            mPingStatusListener.addElement(listener);
        }
    }

    public void unregisterPingStatusListener(PingStatusListener listener) {
        mPingStatusListener.removeElement(listener);
    }

    private void firePingSuccess(String phoneNum) {
        for (Iterator<PingStatusListener> i = mPingStatusListener.iterator(); i.hasNext();) {
            PingStatusListener l = null;
            try {
                l = i.next();
                l.onPingSuccess(phoneNum);
            } catch (Throwable t) {
            }
        }
    }

    private void firePingFail(boolean isTimeOut, String phoneNum, int offlineMinute) {
        for (Iterator<PingStatusListener> i = mPingStatusListener.iterator(); i.hasNext();) {
            PingStatusListener l = null;
            try {
                l = i.next();
                l.onPingFail(isTimeOut, phoneNum, offlineMinute);
            } catch (Throwable t) {
            }
        }
    }

    private void fireReceiveRequest(String phoneNum) {
        for (Iterator<PingStatusListener> i = mPingStatusListener.iterator(); i.hasNext();) {
            PingStatusListener l = null;
            try {
                l = i.next();
                l.onReceiveRequest(phoneNum);
            } catch (Throwable t) {
            }
        }
    }

    @Override
    public void onPingResult(String from, String to, int status, int offlineMinute) {
        String phoneNum = to.replaceFirst("sip:", Const.STR_EMPTY).split("@")[0];
        if (status != STATUS_ONLINE) {
            firePingFail(status == -1, phoneNum, offlineMinute);
        } else {
            firePingSuccess(phoneNum);
        }
    }

    @Override
    public void onReceiveRequest(String from) {
        String phoneNum = from.replaceFirst("sip:", Const.STR_EMPTY).split("@")[0];
        fireReceiveRequest(phoneNum);
    }

    public interface PingStatusListener {
        public void onPingSuccess(String phoneNum);

        public void onPingFail(boolean isTimeOut, String phoneNum, int offlineMinute);

        public void onReceiveRequest(String phoneNum);
    }

    @Override
    public void onStatusUpdate(String uri, Status status) {
    }
}
