
package sigma.qilex.dataaccess.sqlitedb;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import pl.katz.aero2.MyApplication;
import sigma.qilex.dataaccess.model.CallLogModel;

public class CallLogDb extends SQLiteOpenHelper {

    private static final String DB_NAME = "qilexCallLog.db";

    private static final int DB_VERSION = 1;

    public interface CallLogColumn {
        public static final String ID = "_id";

        public static final String LOG_DATE = "LOG_DATE";

        public static final String TYPE = "TYPE";

        public static final String LABEL = "LABEL";

        public static final String DURATION = "DURATION";
    }

    private static final String TABLE_NAME = "CALL_LOG";

    private static final String QUERY_ORDER_BY = "LOG_DATE desc";

    private static CallLogDb instance;

    public static CallLogDb getInstance() {
        if (instance == null) {
            instance = new CallLogDb(MyApplication.getAppContext());
        }
        return instance;
    }

    private CallLogDb(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " ( _id integer primary key asc, LOG_DATE text, "
                + "TYPE integer, LABEL text, DURATION integer );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /**
     * Put calllog item to DB.
     * 
     * @param model
     * @return
     */
    public long putCallLog(CallLogModel model) {
        long id = getWritableDatabase().insert(TABLE_NAME, null, model.parseToContentValue());
        model.setDbId(id);
        return id;
    }

    public ArrayList<CallLogModel> findCallLogByType(int offset, int limit, int type) {
        ArrayList<CallLogModel> listResult = new ArrayList<CallLogModel>();

        Cursor c = getReadableDatabase().query(TABLE_NAME, null, CallLogColumn.TYPE + " = " + type, null, null, null,
                QUERY_ORDER_BY + " LIMIT " + limit + " OFFSET " + offset);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    CallLogModel model = new CallLogModel(c);
                    listResult.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listResult;
    }

    public ArrayList<CallLogModel> findCallLogAll(int offset, int limit) {
        ArrayList<CallLogModel> listResult = new ArrayList<CallLogModel>();

        Cursor c = getReadableDatabase().query(TABLE_NAME, null, null, null, null, null,
                QUERY_ORDER_BY + " LIMIT " + limit + " OFFSET " + offset);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    CallLogModel model = new CallLogModel(c);
                    listResult.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listResult;
    }

    public ArrayList<String> getMissCallPhoneNumbers(int limit) {
        ArrayList<String> listPhoneNo = new ArrayList<String>();

        Cursor c = getReadableDatabase().query(TABLE_NAME, null,
                CallLogColumn.TYPE + " = " + CallLogModel.TYPE_MISSCALL, null, null, null,
                QUERY_ORDER_BY + " LIMIT " + limit);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    CallLogModel model = new CallLogModel(c);
                    if (!listPhoneNo.contains(model.getLabel())) {
                        listPhoneNo.add(model.getLabel());
                    }
                } while (c.moveToNext());
            }
            c.close();
        }
        return listPhoneNo;
    }

    public ArrayList<CallLogModel> findMissCall(int limit) {
        ArrayList<CallLogModel> listResult = new ArrayList<CallLogModel>();

        Cursor c = getReadableDatabase().query(TABLE_NAME, null,
                CallLogColumn.TYPE + " = " + CallLogModel.TYPE_MISSCALL, null, null, null,
                QUERY_ORDER_BY + " LIMIT " + limit);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    CallLogModel model = new CallLogModel(c);
                    listResult.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listResult;
    }

    public CallLogModel findCallLogById(long callLogId) {
        CallLogModel model = null;
        Cursor c = getReadableDatabase().query(TABLE_NAME, null, CallLogColumn.ID + " = " + callLogId, null, null, null,
                null);
        if (c != null) {
            if (c.moveToFirst()) {
                model = new CallLogModel(c);
            }
            c.close();
        }

        return model;
    }

    // TODO: need query by frifon account or another(which is unique value)
    public ArrayList<CallLogModel> findCallLogByLabel(int limit, String... label) {
        ArrayList<CallLogModel> listCallLogs = new ArrayList<CallLogModel>();
        // Search query
        if (label.length == 0) {
            return listCallLogs;
        }

        CallLogModel model = null;

        // Build selection query
        StringBuilder querySelection = new StringBuilder();
        querySelection.append(CallLogColumn.LABEL + "='" + label[0] + "'");
        for (int i = 1; i < label.length; i++) {
            querySelection.append(" OR " + CallLogColumn.LABEL + "='" + label[i] + "'");
        }

        Cursor c = getReadableDatabase().query(TABLE_NAME, null, querySelection.toString(), null, null, null,
                QUERY_ORDER_BY + " LIMIT " + limit);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    model = new CallLogModel(c);
                    listCallLogs.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listCallLogs;
    }

    public int countCallLogByLabel(String... label) {
        // Search query
        int count = 0;
        if (label.length == 0) {
            return 0;
        }

        // Build selection query
        StringBuilder querySelection = new StringBuilder();
        querySelection.append(CallLogColumn.LABEL + "='" + label[0] + "'");
        for (int i = 1; i < label.length; i++) {
            querySelection.append(" OR " + CallLogColumn.LABEL + "='" + label[i] + "'");
        }

        Cursor c = getReadableDatabase().rawQuery("SELECT COUNT(_id) FROM CALL_LOG WHERE " + querySelection, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    count = c.getInt(0);
                } while (c.moveToNext());
            }
            c.close();
        }

        return count;
    }

    /**
     * Count number of miss call.
     * 
     * @param contactStr
     * @return number of unread message.
     */
    public int countMissCall() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(TABLE_NAME);
        query.append(" WHERE ");
        query.append(CallLogColumn.TYPE + " = " + CallLogModel.TYPE_MISSCALL);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    public int updateCallLogType(long id, int type) {
        ContentValues cv = new ContentValues();
        cv.put(CallLogColumn.TYPE, type);
        return getWritableDatabase().update(TABLE_NAME, cv, CallLogColumn.ID + " = " + id, null);
    }

    /**
     * Update CallLog Duration
     * 
     * @param id sqlite db
     * @param duration duration in second.
     */
    public int updateCallLogDuration(long id, int duration) {
        ContentValues cv = new ContentValues();
        cv.put(CallLogColumn.DURATION, duration);
        return getWritableDatabase().update(TABLE_NAME, cv, CallLogColumn.ID + " = " + id, null);
    }

    public int updateCallLog(CallLogModel model) {
        return getWritableDatabase().update(TABLE_NAME, model.parseToContentValue(),
                CallLogColumn.ID + " = " + model.getDbId(), null);
    }

    public void deleteCallLogByIds(long... ids) {
        if (ids == null || ids.length == 0)
            return;
        if (ids.length == 1) {
            deleteCallLogById(ids[0]);
            return;
        }
        String[] args = new String[ids.length];
        StringBuilder placeholders = new StringBuilder();
        for (int i = 0; i < ids.length; i++) {
            args[i] = String.valueOf(ids[i]);
            if (i != 0)
                placeholders.append(", ");
            placeholders.append("?");
        }
        String where = CallLogColumn.ID + " IN (" + placeholders.toString() + ")";
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(TABLE_NAME, where, args);
        } finally {
            db.close();
        }
    }

    public void deleteCallLogById(long id) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(TABLE_NAME, CallLogColumn.ID + " = " + id, null);
        } finally {
            db.close();
        }
    }

    public void clearDB() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(TABLE_NAME, null, null);
        } finally {
            db.close();
        }
    }

    // /**
    // * @return a cursor
    // */
    // public Cursor loadEntries() {
    // SQLiteDatabase db = getWritableDatabase();
    // Cursor c = null;
    // try {
    // c = db.query(TABLE_NAME, null, null, null, null, null, QUERY_ORDER_BY,
    // QUERY_LIMIT);
    // // if you have twice crossed the limit of entries is clean the
    // // excess of
    // // old entries
    // if (c.getCount() > (QUERY_LIMIT_INT + QUERY_LIMIT_INT)) {
    // c.moveToLast();
    // long id = c.getLong(0);
    // db.delete(TABLE_NAME, "_id < " + id, null);
    // }
    // } finally {
    // db.close();
    // }
    // return c;
    // }
    //
    // public Cursor loadTypeEntries(int type) {
    // SQLiteDatabase db = getWritableDatabase();
    // Cursor c = null;
    // try {
    // c = db.query(TABLE_NAME, null, "TYPE = ?", new String[] {
    // String.valueOf(type)
    // }, null, null, QUERY_ORDER_BY, QUERY_LIMIT);
    // // if you have twice crossed the limit of entries is clean the
    // // excess of
    // // old entries
    // if (c.getCount() > (QUERY_LIMIT_INT + QUERY_LIMIT_INT)) {
    // c.moveToLast();
    // long id = c.getLong(0);
    // db.delete(TABLE_NAME, "_id < " + id, null);
    // }
    // } finally {
    // db.close();
    // }
    // return c;
    // }
    //
    // /**
    // * @param item CallLogItem, which will saved
    // */
    // public void putEntry(CallLogModel item) {
    //
    // ContentValues cv = new ContentValues();
    // cv.put(CallLogColumn.LOG_DATE, item.getDateMillis());
    // cv.put(CallLogColumn.TYPE, item.getType());
    // cv.put(CallLogColumn.LABEL, item.getLabel());
    // cv.put(CallLogColumn.DURATION, item.getDuration());
    // SQLiteDatabase db = getWritableDatabase();
    // try {
    // db.insert(TABLE_NAME, null, cv);
    // } finally {
    // db.close();
    // }
    // }
    //
    // /**
    // * clear call log
    // */
    // public void clear() {
    // SQLiteDatabase db = getWritableDatabase();
    // try {
    // db.delete(TABLE_NAME, null, null);
    // } finally {
    // db.close();
    // }
    // }
    //
    // public void removeEntryById(long id) {
    // if (id < 0)
    // return;
    // SQLiteDatabase db = getWritableDatabase();
    // try {
    // db.delete(TABLE_NAME, "_id = " + id, null);
    // } finally {
    // db.close();
    // }
    // }
}
