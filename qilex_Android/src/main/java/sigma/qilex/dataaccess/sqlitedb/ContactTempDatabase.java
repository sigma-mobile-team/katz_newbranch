
package sigma.qilex.dataaccess.sqlitedb;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import pl.katz.aero2.MyApplication;
import sigma.qilex.dataaccess.model.TempContactModel;

public class ContactTempDatabase extends SQLiteOpenHelper {

    private static final String DB_NAME = "qilexContact.db";

    private static final int DB_VERSION = 1;

    public interface ContactTempColumn {
        public static final String PHONE_NUMBER = "phone_number";

        public static final String NAME = "name";

        public static final String AVATAR_URL = "avatar_url";
    }

    private static final String TABLE_NAME = "CONTACT_TEMP";

    private static ContactTempDatabase instance;

    public static ContactTempDatabase getInstance() {
        if (instance == null) {
            instance = new ContactTempDatabase(MyApplication.getAppContext());
        }
        return instance;
    }

    public ContactTempDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder query = new StringBuilder();
        query.append("create table " + TABLE_NAME);
        query.append("(");
        query.append(ContactTempColumn.PHONE_NUMBER + " text,");
        query.append(ContactTempColumn.NAME + " text,");
        query.append(ContactTempColumn.AVATAR_URL + " text");
        query.append(");");
        db.execSQL(query.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }

    public void putTempContact(TempContactModel model) {
        deleteTempContact(model.phoneNumber);
        getWritableDatabase().insert(TABLE_NAME, null, model.parseToContentValue());
    }

    public void putTempContact(ArrayList<TempContactModel> models) {
        ArrayList<String> numbers = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO " + TABLE_NAME + " (");
        query.append(ContactTempColumn.PHONE_NUMBER + ",");
        query.append(ContactTempColumn.NAME + ",");
        query.append(ContactTempColumn.AVATAR_URL + ")");
        query.append(" VALUES ");
        int size = models.size();
        for (int i = 0; i < size; i++) {
            TempContactModel model = models.get(i);
            numbers.add(model.phoneNumber);
            query.append(" ('" + model.phoneNumber + "','" + model.name + "','" + model.avatarUrl + "')");
            if (i < size - 1) {
                query.append(",");
            } else {
                query.append(";");
            }
        }
        deleteTempContact(numbers);
        try {
            getWritableDatabase().execSQL(query.toString());
        } catch (Exception ex) {
        }
    }

    public void deleteAllTempContact() {
        getWritableDatabase().delete(TABLE_NAME, null, null);
    }

    public void deleteTempContact(ArrayList<String> phoneNumbers) {
        String phoneNums = TextUtils.join(",", phoneNumbers);
        String whereClause = ContactTempColumn.PHONE_NUMBER + " IN (" + phoneNums + ")";
        getWritableDatabase().delete(TABLE_NAME, whereClause, null);
    }

    public void deleteTempContact(String phoneNumber) {
        String whereClause = ContactTempColumn.PHONE_NUMBER + " = '" + phoneNumber + "'";
        getWritableDatabase().delete(TABLE_NAME, whereClause, null);
    }

    /**
     * Get temp contact
     * 
     * @param phoneNumbers null to get all
     */
    public HashMap<String, TempContactModel> getTempContactToHashMap(ArrayList<String> phoneNumbers) {
        HashMap<String, TempContactModel> outputHashMap = new HashMap<>();
        String whereClause = null;
        if (phoneNumbers != null && phoneNumbers.isEmpty() == false) {
            String phoneNums = TextUtils.join(",", phoneNumbers);
            whereClause = ContactTempColumn.PHONE_NUMBER + " IN (" + phoneNums + ")";
        }
        Cursor c = getReadableDatabase().query(TABLE_NAME, null, whereClause, null, null, null, null);
        if (c.moveToFirst() == true) {
            do {
                TempContactModel model = new TempContactModel(c);
                outputHashMap.put(model.phoneNumber, model);
            } while (c.moveToNext());
        }
        c.close();
        return outputHashMap;
    }
}
