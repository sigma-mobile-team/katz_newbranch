
package sigma.qilex.dataaccess.sqlitedb;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import pl.katz.aero2.MyApplication;
import sigma.qilex.utils.LogUtils;

public class ImageGalleryProvider {

    public static final String[] GALLERY_PROJECTION = new String[] {
            MediaStore.Images.Media._ID, MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
            MediaStore.Images.Media.MINI_THUMB_MAGIC, MediaStore.Images.Media.DATE_TAKEN
    };

    public static class ImageModel {
        public String url;

        public long lastModified;
    }

    public static class DateCompare implements Comparator<ImageModel> {
        @Override
        public int compare(ImageModel lhs, ImageModel rhs) {
            long compare = rhs.lastModified - lhs.lastModified;
            if (compare < 0) {
                return -1;
            } else if (compare > 0) {
                return 1;
            } else {
                return 0;
            }
        }

    }

    public static ArrayList<String> getFilePaths(String selection) {
        ContentResolver cr = MyApplication.getAppContext().getContentResolver();

        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {
                MediaStore.Images.ImageColumns.DATA
        };
        SortedSet<String> dirList = new TreeSet<String>();
        ArrayList<String> resultIAV = new ArrayList<String>();
        ArrayList<ImageModel> resultFiles = new ArrayList<>();

        String[] directories = null;
        Cursor c = cr.query(uri, projection, selection, null, Media.DATE_ADDED + " DESC");
        if (c.moveToFirst()) {
            do {
                String tempDir = c.getString(0);
                tempDir = tempDir.substring(0, tempDir.lastIndexOf("/"));
                try {
                    dirList.add(tempDir);
                } catch (Exception e) {
                    LogUtils.d("Image Gallery", "Image Gallery Eception");
                }

                // Thumbnail
            } while (c.moveToNext());
            directories = new String[dirList.size()];
            dirList.toArray(directories);
        }
        c.close();

        for (int i = 0; i < dirList.size(); i++) {
            File imageDir = new File(directories[i]);
            File[] imageList = imageDir.listFiles();
            if (imageList == null)
                continue;
            for (File imagePath : imageList) {
                try {
                    if (imagePath.isDirectory()) {
                        imageList = imagePath.listFiles();
                    }
                    if (imagePath.getName().contains(".jpg") || imagePath.getName().contains(".JPG")
                            || imagePath.getName().contains(".jpeg") || imagePath.getName().contains(".JPEG")
                            || imagePath.getName().contains(".png") || imagePath.getName().contains(".PNG")
                            || imagePath.getName().contains(".gif") || imagePath.getName().contains(".GIF")
                            || imagePath.getName().contains(".bmp") || imagePath.getName().contains(".BMP")) {
                        String path = imagePath.getAbsolutePath();

                        ImageModel model = new ImageModel();
                        model.lastModified = imagePath.lastModified();
                        model.url = path;
                        resultFiles.add(model);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        Collections.sort(resultFiles, new DateCompare());
        for (ImageModel image : resultFiles) {
            resultIAV.add(image.url);
        }
        return resultIAV;
    }
}
