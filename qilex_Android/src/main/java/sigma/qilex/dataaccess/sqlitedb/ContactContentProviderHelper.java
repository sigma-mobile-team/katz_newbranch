
package sigma.qilex.dataaccess.sqlitedb;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.http.ContactSnapshotModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.activity.IActivity;
import sigma.qilex.ui.adapter.ContactDetailPhoneListAdapter.PhoneItem;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class ContactContentProviderHelper {

    // public static final String MIME_TYPE_FRIFON =
    // "vnd.android.cursor.item/eu.sigma.qilex.profile";

    public static final String SYNC_TYPE_FRIFON = "Sync_Frifon";

    public static final String MIME_TYPE_FRIFON_CALL = "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_number_call";// CommonDataKinds.SipAddress.CONTENT_ITEM_TYPE;

    public static final String MIME_TYPE_FRIFON_MESSAGE = "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_message";

    public static final String MIME_TYPE_FRIFON_OUTCALL_QILEX = "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_out_call_qilex";

    public static final String MIME_TYPE_FRIFON_OUTCALL_NON_QILEX = "vnd.android.cursor.item/vnd.eu.sigma.voip.qilex_out_call_non_qilex";

    public static final String MIME_TYPE_FRIFON_GOOGLE_VOICEMESSAGE = "vnd.android.cursor.item/vnd.eu.sigma.voip.google_voice_message";

    public static final String FRIFONT_SIP_ADDRESS_CUSTOM_TYPE = "Qilex";

    public static final String COLUMN_SERVER_CONTACT_ID = ContactsContract.Data.DATA13;

    public static final String COLUMN_LAST_TIME_SYNC_MILLIS = ContactsContract.Data.DATA12;

    public static final String COLUMN_CONTACT_AVATAR = ContactsContract.Data.DATA15;

    public static final String FORMAT_FREE_CALL = "Free call (%s)";

    public static long getContactByPhoneNo2(Context context, String phoneNumber) {
        if (!QilexUtils.hasSelfPermission(context, IActivity.CONTACT_PERMISSIONS))
            return -1l;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, phoneNumber);
        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {
                BaseColumns._ID
        }, null, null, null);
        try {
            long contactId = -1;
            if (contactLookup.moveToFirst()) {
                contactId = contactLookup.getLong(contactLookup.getColumnIndex(BaseColumns._ID));
            }
            return contactId;
        } finally {
            contactLookup.close();
        }
    }

    public static String[] queryContactName(long rawContactId) {
        // Get full Name
        String whereName = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.RAW_CONTACT_ID + "= ?";
        String[] whereNameParams = new String[] {
                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, rawContactId + ""
        };
        String[] projection = new String[] {
                StructuredName.GIVEN_NAME, StructuredName.FAMILY_NAME, StructuredName.MIDDLE_NAME
        };
        Cursor nameCur = MyApplication.getAppContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                projection, whereName, whereNameParams, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
        try {
            if (nameCur.moveToFirst() == true) {
                String given = nameCur
                        .getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                String family = nameCur
                        .getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                String middle = nameCur
                        .getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));
                String[] names = new String[3];
                names[0] = given;
                names[1] = family;
                names[2] = middle;
                return names;
            } else {
                return new String[] {
                        Const.STR_EMPTY, Const.STR_EMPTY, Const.STR_EMPTY
                };
            }
        } finally {
            nameCur.close();
        }
    }

    /**
     * Get contact by rawContactId
     * 
     * @param context
     * @param rawContactId
     * @return
     */
    public static QilexContact getContact(Context context, long rawContactId) {
        Uri uri = ContactsContract.Data.CONTENT_URI;
        long nameRawContactId = rawContactId;

        String[] projection = new String[] {
                Data.PHOTO_THUMBNAIL_URI, Data.PHOTO_URI, Data.CONTACT_ID, COLUMN_SERVER_CONTACT_ID
        };
        String selection = ContactsContract.Data.RAW_CONTACT_ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(nameRawContactId)
        };
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder);
        try {
            if (cursor.moveToFirst()) {
                String[] names = queryContactName(nameRawContactId);

                String photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_THUMBNAIL_URI));
                String photoUriFull = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_URI));
                if (photoUri != null) {
                    while (photoUri.endsWith("photo") || photoUri.endsWith("photo/")) {
                        if (photoUri.endsWith("photo")) {
                            photoUri = photoUri.substring(0, photoUri.length() - 5);
                        } else {
                            photoUri = photoUri.substring(0, photoUri.length() - 6);
                        }
                    }
                    if (photoUriFull == null) {
                        photoUriFull = photoUri;
                    }
                } else {
                    String serverAvatarUri = getContactServerAvatar(context, rawContactId);
                    photoUri = serverAvatarUri;
                    photoUriFull = serverAvatarUri;
                }
                QilexContact contact = new QilexContact();
                contact.setPhotoUri(photoUri);
                contact.setPhotoUriFull(photoUriFull);
                contact.setRawContactId(rawContactId);
                contact.setFirstName(names[0]);
                contact.setLastName(names[1]);
                contact.setMiddleName(names[2]);
                long contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                contact.setContentContactId(contactId);
                contact.id = contactId;

                String strServerContactId = cursor.getString(cursor.getColumnIndex(COLUMN_SERVER_CONTACT_ID));
                long serverContactId = -1;
                try {
                    serverContactId = Long.parseLong(strServerContactId);
                } catch (NumberFormatException ex) {
                }
                contact.setServerContactId(serverContactId);

                // Check frifonNumber
                ArrayList<PhoneNumberModel> listPhoneNo = ContactContentProviderHelper.queryContactNumbers(context,
                        nameRawContactId, null);
                setPhoneToContact(listPhoneNo, contact);

                return contact;
            }
        } finally {
            cursor.close();
        }
        return null;
    }

    public static Bitmap getBitmapContactPhoto(Context context, String strUri) {
        Bitmap photo = null;
        Uri uri = Uri.parse(strUri);
        try {
            photo = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
    }

    public static QilexContact getContactByContentId(Context context, long contentId) {
        Uri uri = ContactsContract.Data.CONTENT_URI;

        String[] projection = new String[] {
                Data.RAW_CONTACT_ID, Data.PHOTO_THUMBNAIL_URI, Data.PHOTO_URI, Data.CONTACT_ID, COLUMN_SERVER_CONTACT_ID
        };
        String selection = ContactsContract.Data.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(contentId)
        };
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, sortOrder);
        try {
            if (cursor.moveToFirst()) {
                long rawId = cursor.getLong(cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID));
                String photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_THUMBNAIL_URI));
                String photoUriFull = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_URI));
                if (photoUri != null) {
                    while (photoUri.endsWith("photo") || photoUri.endsWith("photo/")) {
                        if (photoUri.endsWith("photo")) {
                            photoUri = photoUri.substring(0, photoUri.length() - 5);
                        } else {
                            photoUri = photoUri.substring(0, photoUri.length() - 6);
                        }
                    }
                    if (photoUriFull == null) {
                        photoUriFull = photoUri;
                    }
                } else {
                    String serverAvatarUri = getContactServerAvatar(context, rawId);
                    photoUri = serverAvatarUri;
                    photoUriFull = serverAvatarUri;
                }
                String[] names = queryContactName(rawId);
                QilexContact contact = new QilexContact();
                contact.setPhotoUri(photoUri);
                contact.setPhotoUriFull(photoUriFull);
                contact.setRawContactId(rawId);
                contact.setFirstName(names[0]);
                contact.setLastName(names[1]);
                contact.setMiddleName(names[2]);
                contact.setContentContactId(contentId);

                String strServerContactId = cursor.getString(cursor.getColumnIndex(COLUMN_SERVER_CONTACT_ID));
                long serverContactId = -1;
                try {
                    serverContactId = Long.parseLong(strServerContactId);
                } catch (NumberFormatException ex) {
                }
                contact.setServerContactId(serverContactId);
                return contact;
            }
        } finally {
            cursor.close();
        }
        return null;
    }

    public static ArrayList<PhoneNumberModel> queryContactNumbers(Context context, long rawContactId,
            String addedNumber) {
        ArrayList<PhoneNumberModel> numbers = new ArrayList<PhoneNumberModel>();
        long nameRawContactId = getNameRawByRaw(context, rawContactId);
        if (nameRawContactId <= 0) {
            nameRawContactId = rawContactId;
        }

        // Query to get all phone number
        Cursor pCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.RAW_CONTACT_ID + " = ? AND " + Data.MIMETYPE + " = ?",
                new String[] {
                        Long.toString(nameRawContactId), ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
        }, null);

        try {
            while (pCur.moveToNext()) {
                String number = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (Utils.isStringNullOrEmpty(number) == true) {
                    continue;
                }
                String strType = Const.STR_EMPTY;
                int type = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                switch (type) {
                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        strType = context.getString(R.string.phone_type_mobile);
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        strType = context.getString(R.string.phone_type_home);
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        strType = context.getString(R.string.phone_type_work);
                        break;
                    default:
                        strType = context.getString(R.string.phone_type_other);
                        break;
                }
                PhoneNumberModel model = new PhoneNumberModel(number, strType);
                numbers.add(model);
            }
        } finally {
            pCur.close();
        }
        if (addedNumber != null)
            numbers.add(new PhoneNumberModel(addedNumber, context.getString(R.string.phone_type_mobile)));
        return numbers;
    }

    public static boolean updateContact(Context activity, long rawContactId, String firstName, String lastName,
            Bitmap bitmap, List<PhoneItem> itemsPhoneRemoved, List<PhoneItem> itemsPhoneAdd) {
        return updateContactFull(activity, rawContactId, firstName, lastName, null, null, bitmap, itemsPhoneRemoved,
                itemsPhoneAdd);
    }

    private static boolean updateContactFull(Context activity, long nameRawContact, String firstName, String lastName,
            String mobileNumber, String homeNumber, Bitmap bitmap, List<PhoneItem> itemsPhoneRemoved,
            List<PhoneItem> itemsPhoneAdd) {

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        // remove PhoneNumber
        if (itemsPhoneRemoved != null && !itemsPhoneRemoved.isEmpty()) {
            int size = itemsPhoneRemoved.size();
            String[] args = new String[size + 2];
            args[0] = Long.toString(nameRawContact);
            args[1] = Phone.CONTENT_ITEM_TYPE;
            StringBuilder placeholders = new StringBuilder();
            for (int i = 0; i < size; i++) {
                args[2 + i] = itemsPhoneRemoved.get(i).number;
                if (i != 0)
                    placeholders.append(", ");
                placeholders.append("?");
            }
            String where = Phone.NUMBER + " IN (" + placeholders.toString() + ")";
            String selectPhone = Data.RAW_CONTACT_ID + " = ? AND " + "(" + Data.MIMETYPE + " = ?" + " )" + " AND "
                    + where;
            ops.add(ContentProviderOperation.newDelete(Data.CONTENT_URI).withSelection(selectPhone, args).build());
        }
        // Names
        ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                .withSelection("(" + Data.RAW_CONTACT_ID + "= ?)" + " AND " + Data.MIMETYPE + " = ?", new String[] {
                        Long.toString(nameRawContact), ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
        });
        if (firstName != null) {
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, firstName);
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME, Const.STR_EMPTY);
        }
        if (lastName != null) {
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, lastName);
        }
        ops.add(builder.build());

        // Mobile Number
        if (mobileNumber != null) {
            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "=?", new String[] {
                            Long.toString(nameRawContact)
            }).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobileNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }
        if (itemsPhoneAdd != null && itemsPhoneAdd.isEmpty() == false) {
            for (PhoneItem phoneItem : itemsPhoneAdd) {
                addNumberToContact(activity, nameRawContact, phoneItem.number);
            }
        }
        // update avatar
        if (bitmap != null) {
            updateAvatarToContact(activity, nameRawContact, bitmap, ops);
        }

        // Asking the Contact provider to create a new contact
        try {
            activity.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void updateAvatarToContact(Context activity, long rawContactId, Bitmap bitmap,
            ArrayList<ContentProviderOperation> ops) {
        int photoRow = -1;
        String where = ContactsContract.Data.RAW_CONTACT_ID + " = " + rawContactId + " AND "
                + ContactsContract.Data.MIMETYPE + " =='" + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                + "'";
        Cursor cursor = activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, where, null, null);
        String photoUri = null;
        int idIdx = cursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
        if (cursor.moveToFirst()) {
            photoRow = cursor.getInt(idIdx);
            photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.PHOTO_THUMBNAIL_URI));
        }
        cursor.close();
        byte[] b = null;

        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            b = baos.toByteArray();
        }

        if (photoRow >= 0) {
            // update
            ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                    .withSelection(ContactsContract.Data._ID + " = ?", new String[] {
                            Long.toString(photoRow)
            }).withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                    .withValue(ContactsContract.Data.IS_SUPER_PRIMARY, 1)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, b).build());

            if (photoUri != null)
                while (photoUri.endsWith("photo") || photoUri.endsWith("photo/")) {
                    if (photoUri.endsWith("photo")) {
                        photoUri = photoUri.substring(0, photoUri.length() - 5);
                    } else {
                        photoUri = photoUri.substring(0, photoUri.length() - 6);
                    }
                }
            if (photoUri != null) {
                MemoryCacheUtils.removeFromCache(photoUri, ImageLoader.getInstance().getMemoryCache());
                DiskCacheUtils.removeFromCache(photoUri, ImageLoader.getInstance().getDiskCache());
            }
        } else {
            // insert
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, b).build());
        }
    }

    public static boolean addNumberToContact(Context activity, long rawId, String number) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Data.RAW_CONTACT_ID, rawId);
        contentValues.put(Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
        contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, number);
        contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI).withValues(contentValues).build());
        // Asking the Contact provider to create a new contact
        try {
            activity.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get All contact of frifon application.
     * 
     * @param context
     * @param ouputAllContact
     * @param outputFrifonContact
     * @param onGetAllProcessing
     */
    public static void getAllContact(Context context, ArrayList<QilexContact> ouputAllContact,
            ArrayList<QilexContact> outputFrifonContact, OnGetAllContactProcessing onGetAllProcessing) {
        // // Get All in Contacts DB
        // Uri baseUri = Contacts.CONTENT_URI;
        // String[] projection = new String[] {
        // Contacts.NAME_RAW_CONTACT_ID
        // };
        // String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND (" +
        // Contacts.HAS_PHONE_NUMBER + "=1) AND ("
        // + Contacts.DISPLAY_NAME + " != '' ))";
        // Cursor cursor = context.getContentResolver().query(baseUri,
        // projection, select, null,
        // Contacts.DISPLAY_NAME + " ASC");
        // try {
        // if (cursor.moveToFirst()) {
        // do {
        // // Get Data from Cursor
        // long nameRawId = cursor.getLong(cursor
        // .getColumnIndex(ContactsContract.Contacts.NAME_RAW_CONTACT_ID));
        // long rawId = getRawIdByNameRaw(context, nameRawId);
        //
        // QilexContact contact = getContact(context, rawId);
        // if (contact == null) {
        // contact = getContact(context, nameRawId);
        // }
        // if (contact == null) {
        // continue;
        // }
        // contact.setNameRawContactId(rawId);
        //
        // // Notify process loading
        // ouputAllContact.add(contact);
        // if (contact.isFrifon()) {
        // outputFrifonContact.add(contact);
        // }
        //
        // if (onGetAllProcessing != null && ouputAllContact.size() % 40 == 0) {
        // onGetAllProcessing.onLoading(ouputAllContact, outputFrifonContact);
        // }
        // } while (cursor.moveToNext());
        // }
        // } finally {
        // cursor.close();
        // }
    }

    static int testIndex = 0;

    static class GetAllContactUpdate_ContactModel {
        long _id;

        long name_raw_contact_id;
    }

    static class GetAllContactUpdate_RawContactModel {
        long raw_contact_id;

        long source_id;

        long server_id;

        int last_sync_version;
    }

    @SuppressWarnings("unused")
    private static void applyBatchSnapshotToContact(Context context, ArrayList<QilexContact> listContact,
            ArrayList<ContactSnapshotModel> listSnapshot) {
        ArrayList<ContentProviderOperation> addSipFrifonOps = new ArrayList<ContentProviderOperation>();
        int size = listContact.size();
        for (int i = 0; i < size; i++) {
            QilexContact contact = listContact.get(i);
            ContactSnapshotModel snapshot = listSnapshot.get(i);

            // Add frifon address
            PhoneNumberModel[] phone = contact.getPhoneNumberNormal();
            for (PhoneNumberModel phoneNumberModel : phone) {
                if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber, snapshot.frifon_msisdn)) {
                    String phoneWithContryCode = QilexPhoneNumberUtils
                            .addCurrentCountryCodeToPhone(phoneNumberModel.phoneNumber);

                    if (hasFrifonFreeCall(context, contact.getRawContactId(), phoneWithContryCode) == false) {
                        addSipFrifonOps.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                                .withValue(ContactsContract.Data.RAW_CONTACT_ID, contact.getRawContactId())
                                .withValue(Data.MIMETYPE, MIME_TYPE_FRIFON_CALL)
                                .withValue(Data.DATA1, phoneWithContryCode)
                                .withValue(Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE)
                                .withValue(Data.DATA3, "Free Call (" + phoneWithContryCode + ")").build());
                    }
                    if (hasFrifonFreeMessage(context, contact.getRawContactId(), phoneWithContryCode) == false) {
                        addSipFrifonOps.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                                .withValue(ContactsContract.Data.RAW_CONTACT_ID, contact.getRawContactId())
                                .withValue(Data.MIMETYPE, MIME_TYPE_FRIFON_MESSAGE)
                                .withValue(Data.DATA1, phoneWithContryCode)
                                .withValue(Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE)
                                .withValue(Data.DATA3, "Free Message (" + phoneWithContryCode + ")").build());
                    }

                    ContactContentProviderHelper.addSipAddressToContact(MyApplication.getAppContext(),
                            phoneWithContryCode, contact.getRawContactId());
                }
            }
            ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(RawContacts.CONTENT_URI);
        }
    }

    /**
     * Add new contact to contact list
     * 
     * @param firstName firstName.
     * @param lastName last Name.
     * @param phoneNumber phone number.
     * @param bitmap bitmap avatar.
     * @return true if add successfully.
     */
    public static long addContact(String firstName, String lastName, String phoneNumber, Bitmap bitmap,
            long serverContactId) {

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, "")
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, "").build());

        // ------------------------------------------------------ Names

        ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(COLUMN_SERVER_CONTACT_ID, "" + serverContactId);

        boolean insert = false;
        if (firstName != null) {
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, firstName);
            insert = true;
        }
        if (lastName != null) {
            builder.withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, lastName);
            insert = true;
        }
        if (insert)
            ops.add(builder.build());
        // ------------------------------------------------------ Mobile Number
        if (!TextUtils.isEmpty(phoneNumber)) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }

        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Photo.PHOTO, b).build());
        }

        // Asking the Contact provider to create a new contact
        try {
            ContentProviderResult[] res = MyApplication.getAppContext().getContentResolver()
                    .applyBatch(ContactsContract.AUTHORITY, ops);
            Uri myContactUri = res[0].uri;
            int lastSlash = myContactUri.toString().lastIndexOf("/");
            int length = myContactUri.toString().length();
            long nameRawContactID = Integer
                    .parseInt((String)myContactUri.toString().subSequence(lastSlash + 1, length));
            LogUtils.d("SIGMA", "SIGMA-ADD CONTACT " + nameRawContactID);

            return nameRawContactID;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void setPhoneToContact(ArrayList<PhoneNumberModel> listPhoneNo, QilexContact contact) {
        Context context = MyApplication.getAppContext();
        ArrayList<PhoneNumberModel> contactPhoneNormal = new ArrayList<PhoneNumberModel>();
        ArrayList<PhoneNumberModel> contactPhoneFrifon = new ArrayList<PhoneNumberModel>();

        // Frifon contact
        for (PhoneNumberModel phone : listPhoneNo) {
            if (Utils.isStringNullOrEmpty(phone.phoneNumber) == true) {
                continue;
            }

            // Check if existing phone frifon in DB
            String removePhoneCode = QilexPhoneNumberUtils.removePhoneCode(phone.phoneNumber)[1];

            // Query to get frifon number
            StringBuilder whereClause = new StringBuilder();
            whereClause.append("(");
            whereClause.append(Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_CALL + "'");
            whereClause.append(" OR ");
            whereClause.append(Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_MESSAGE + "'");
            whereClause.append(")");
            whereClause.append(" AND ");
            whereClause.append(ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + removePhoneCode + "'");
            Cursor pCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null,
                    whereClause.toString(), null, null);

            if (pCur.moveToFirst() == true) {
                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contactPhoneFrifon.add(new PhoneNumberModel(phoneNo, phone.type));
            } else {
                contactPhoneNormal.add(phone);
            }
            pCur.close();
        }

        // Set phone number
        String[] a = new String[contactPhoneNormal.size()];
        String[] b = new String[contactPhoneFrifon.size()];
        for (int i = 0; i < a.length; i++) {
            a[i] = contactPhoneNormal.get(i).toString();
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = contactPhoneFrifon.get(i).toString();
        }

        contact.setPhoneNumberNormal(a);
        contact.setPhoneNumberKATZ(b);
    }

    public static String[] queryContactNameFull(long rawContactId) {
        // Get full Name
        String whereName = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.RAW_CONTACT_ID + "= ?";
        String[] whereNameParams = new String[] {
                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, rawContactId + ""
        };
        String[] projection = new String[] {
                StructuredName.DISPLAY_NAME, StructuredName.GIVEN_NAME, StructuredName.FAMILY_NAME,
                StructuredName.MIDDLE_NAME, StructuredName.PHONETIC_GIVEN_NAME, StructuredName.PHONETIC_MIDDLE_NAME,
                StructuredName.PHONETIC_FAMILY_NAME, StructuredName.FULL_NAME_STYLE, StructuredName.DISPLAY_NAME_SOURCE
        };
        Cursor nameCur = MyApplication.getAppContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                projection, whereName, whereNameParams, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
        try {
            if (nameCur.moveToFirst() == true) {
                String s1 = nameCur.getString(
                        nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
                String s2 = nameCur
                        .getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                String s3 = nameCur
                        .getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                String s4 = nameCur
                        .getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));
                String s7 = nameCur.getString(
                        nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_GIVEN_NAME));
                String s8 = nameCur.getString(
                        nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_MIDDLE_NAME));
                String s9 = nameCur.getString(
                        nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.PHONETIC_FAMILY_NAME));
                String s10 = nameCur.getString(
                        nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FULL_NAME_STYLE));
                String s100 = nameCur.getString(
                        nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME_SOURCE));
                String[] names = new String[] {
                        s1, s2, s3, s4, s7, s8, s9, s10, s100
                };
                return names;
            } else {
                return new String[] {
                        Const.STR_EMPTY, Const.STR_EMPTY, Const.STR_EMPTY, Const.STR_EMPTY, Const.STR_EMPTY,
                        Const.STR_EMPTY, Const.STR_EMPTY, Const.STR_EMPTY, Const.STR_EMPTY
                };
            }
        } finally {
            nameCur.close();
        }
    }

    public static boolean hasFrifonOutPhone(Context context, long rawId, String phoneNo) {
        String[] projection = new String[] {
                Data._ID
        };
        String selection = Phone.RAW_CONTACT_ID + " = " + rawId + " AND " + Data.MIMETYPE + " = '"
                + MIME_TYPE_FRIFON_OUTCALL_QILEX + "'" + " AND " + Data.DATA1 + " = '" + phoneNo + "'";
        Cursor pCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, projection, selection, null,
                null);
        try {
            return pCur.moveToFirst();
        } finally {
            pCur.close();
        }
    }

    public static boolean hasFrifonFreeCall(Context context, long rawId, String phoneNo) {
        String[] projection = new String[] {
                Data._ID
        };
        String selection = Phone.RAW_CONTACT_ID + " = " + rawId + " AND " + Data.MIMETYPE + " = '"
                + MIME_TYPE_FRIFON_CALL + "'" + " AND " + Data.DATA1 + " = '" + phoneNo + "'";
        Cursor pCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, projection, selection, null,
                null);
        try {
            return pCur.moveToFirst();
        } finally {
            pCur.close();
        }
    }

    public static boolean hasFrifonFreeMessage(Context context, long rawId, String phoneNo) {
        String[] projection = new String[] {
                Data._ID
        };
        String selection = Phone.RAW_CONTACT_ID + " = " + rawId + " AND " + Data.MIMETYPE + " = '"
                + MIME_TYPE_FRIFON_MESSAGE + "'" + " AND " + Data.DATA1 + " = '" + phoneNo + "'";
        Cursor pCur = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, projection, selection, null,
                null);
        try {
            return pCur.moveToFirst();
        } finally {
            pCur.close();
        }
    }

    public static Account getCurrentAccount(Context context) {
        AccountManager accountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accs = accountManager.getAccountsByType("eu.sigma.qilex");
        Account currentAccount = null;
        if (accs.length > 0) {
            currentAccount = accs[0];
        }
        return currentAccount;
    }

    /**
     * Add freefon out call to contact.
     * 
     * @param context
     * @param rawContactID
     */
    public static void addSipFrifonOutToContact(Context context, long rawContactID) {
        ContentResolver contentResolver = context.getContentResolver();

        ArrayList<PhoneNumberModel> listPhoneNo = ContactContentProviderHelper.queryContactNumbers(context,
                rawContactID, null);

        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ArrayList<ContentProviderOperation> opsDelete = new ArrayList<ContentProviderOperation>();

        int size = listPhoneNo.size();

        // Delete non-used frifon address
        String[] inArgs = new String[1];
        inArgs[0] = Long.toString(rawContactID);
        String selectPhone = Data.RAW_CONTACT_ID + " = ? AND " + "(" + Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_CALL
                + "'" + " OR " + Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_MESSAGE + "'" + " OR " + Data.MIMETYPE
                + " = '" + MIME_TYPE_FRIFON_OUTCALL_QILEX + "'" + " OR " + Data.MIMETYPE + " = '"
                + MIME_TYPE_FRIFON_OUTCALL_NON_QILEX + "' )";
        opsDelete.add(ContentProviderOperation.newDelete(Data.CONTENT_URI).withSelection(selectPhone, inArgs).build());
        try {
            contentResolver.applyBatch(ContactsContract.AUTHORITY, opsDelete);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Add phone number to frifon
        for (int i = 0; i < size; i++) {
            String strPhoneNo = listPhoneNo.get(i).phoneNumber;
            strPhoneNo = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(strPhoneNo);

            if (hasFrifonOutPhone(context, rawContactID, strPhoneNo) == false) {
                ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                        .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                        .withValue(Data.MIMETYPE, MIME_TYPE_FRIFON_OUTCALL_QILEX).withValue(Data.DATA1, strPhoneNo)
                        .withValue(Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE)
                        .withValue(Data.DATA3, "Frifont Out Call (" + strPhoneNo + ")").build());
            }
        }

        try {
            contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addSipAddressToContact(Context context, String phoneNumber, long rawContactID) {
        ContentResolver contentResolver = context.getContentResolver();

        // ArrayList<PhoneNumberModel> listPhoneNo =
        // ContactContentProviderHelper.queryContactNumbers(context,
        // rawContactID, null);

        // ArrayList<ContentProviderOperation> opsDelete = new
        // ArrayList<ContentProviderOperation>();
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        if (hasFrifonFreeCall(context, rawContactID, phoneNumber) == false) {
            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(Data.MIMETYPE, MIME_TYPE_FRIFON_CALL).withValue(Data.DATA1, phoneNumber)
                    .withValue(Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE)
                    .withValue(Data.DATA3, "Free Call (" + phoneNumber + ")").build());
        }
        if (hasFrifonFreeMessage(context, rawContactID, phoneNumber) == false) {
            ops.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)
                    .withValue(ContactsContract.Data.RAW_CONTACT_ID, rawContactID)
                    .withValue(Data.MIMETYPE, MIME_TYPE_FRIFON_MESSAGE).withValue(Data.DATA1, phoneNumber)
                    .withValue(Data.DATA2, FRIFONT_SIP_ADDRESS_CUSTOM_TYPE)
                    .withValue(Data.DATA3, "Free Message (" + phoneNumber + ")").build());
        }

        // // Delete non-used frifon address
        // String [] inArgs = new String[1];
        // inArgs[0] = Long.toString(rawContactID);
        // String selectPhone = Data.RAW_CONTACT_ID + " = ? AND "
        // + "(" + Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_CALL + "'"
        // + " OR " + Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_MESSAGE + "'"
        // + " OR " + Data.MIMETYPE + " = '" + MIME_TYPE_FRIFON_OUTCALL_QILEX +
        // "'"
        // + " OR " + Data.MIMETYPE + " = '" +
        // MIME_TYPE_FRIFON_OUTCALL_NON_QILEX + "' )";
        // opsDelete.add(ContentProviderOperation.newDelete(Data.CONTENT_URI).withSelection(selectPhone,
        // inArgs).build());

        try {
            // contentResolver.applyBatch(ContactsContract.AUTHORITY,
            // opsDelete);
            contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long getContactLastUpdateTime(Context context, long rawContactId) {
        Uri baseUri = Contacts.CONTENT_URI;
        String select = ContactsContract.Contacts.NAME_RAW_CONTACT_ID + "=" + rawContactId;
        String[] projection = {
                ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP
        };
        Cursor cursor = context.getContentResolver().query(baseUri, projection, select, null, null);
        try {
            if (cursor.moveToFirst()) {
                long lastUpdate = cursor.getLong(0);
                return lastUpdate;
            } else {
                return -1;
            }
        } finally {
            cursor.close();
        }
    }

    public static boolean updateServerContactId(Context context, long rawContactId, long serverContactId) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(RawContacts.CONTENT_URI)
                .withSelection(RawContacts._ID + "=" + rawContactId, null);
        builder.withValue(RawContacts.SYNC2, serverContactId);
        ops.add(builder.build());
        try {
            MyApplication.getAppContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long getContactServerId(Context context, long rawContactId) {
        Uri uri = RawContacts.CONTENT_URI;
        String[] projection = {
                RawContacts.SYNC2
        };
        String selection = RawContacts._ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(rawContactId)
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
        try {
            if (cursor.moveToFirst()) {
                long sync2 = cursor.getLong(cursor.getColumnIndex(RawContacts.SYNC2));
                return sync2;
            } else {
                return -1;
            }
        } finally {
            cursor.close();
        }
    }

    public static boolean updateServerContactAvatar(Context activity, long rawContactId, String serverContactAvatar) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

        ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
                .withSelection(ContactsContract.Data.RAW_CONTACT_ID + "=?", new String[] {
                        Long.toString(rawContactId)
        }).withValue(COLUMN_CONTACT_AVATAR, serverContactAvatar).build());
        boolean result = true;
        try {
            MyApplication.getAppContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    public static String getContactServerAvatar(Context context, long rawContactId) {
        Uri uri = ContactsContract.Data.CONTENT_URI;

        String[] projection = {
                COLUMN_CONTACT_AVATAR
        };
        String selection = ContactsContract.Data.RAW_CONTACT_ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(rawContactId)
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
        try {
            if (cursor.moveToFirst()) {
                String strServerContactAvatar = cursor.getString(0);
                return strServerContactAvatar;
            } else {
                return null;
            }
        } finally {
            cursor.close();
        }
    }

    public static long getContactDataVersion(Context context, long rawContactId) {
        long nameRawContactId = getNameRawByRaw(context, rawContactId);
        if (nameRawContactId <= 0) {
            nameRawContactId = rawContactId;
        }
        Uri uri = RawContacts.CONTENT_URI;

        String[] projection = {
                RawContacts.VERSION
        };
        String selection = RawContacts._ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(nameRawContactId)
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
        try {
            if (cursor.moveToFirst()) {
                long version = cursor.getLong(cursor.getColumnIndex(RawContacts.VERSION));
                return version;
            } else {
                return -1;
            }
        } finally {
            cursor.close();
        }
    }

    public static long getContactLastSyncVersion(Context context, long rawContactId) {
        Uri uri = RawContacts.CONTENT_URI;
        String[] projection = {
                RawContacts.SYNC4
        };
        String selection = RawContacts._ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(rawContactId)
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
        try {
            if (cursor.moveToFirst()) {
                long sync2 = cursor.getLong(cursor.getColumnIndex(RawContacts.SYNC4));
                return sync2;
            } else {
                return -1;
            }
        } finally {
            cursor.close();
        }
    }

    public static boolean updateContactLastSyncVersion(Context activity, long rawContactId) {
        long nameRawContactId = getNameRawByRaw(activity, rawContactId);
        if (nameRawContactId <= 0) {
            nameRawContactId = rawContactId;
        }
        long curentVersion = getContactDataVersion(activity, nameRawContactId);

        // Update to contact
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ContentProviderOperation.Builder builder = ContentProviderOperation.newUpdate(RawContacts.CONTENT_URI)
                .withSelection(RawContacts._ID + "=" + rawContactId, null);
        builder.withValue(RawContacts.SYNC4, curentVersion);
        ops.add(builder.build());
        try {
            MyApplication.getAppContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get Frifon app's raw id from original contact raw id
     * 
     * @param context
     * @param nameRawContactId
     * @return
     */
    public static ArrayList<Long> getRawIdByNameRaw(Context context, long nameRawContactId) {
        Uri uri = ContactsContract.RawContacts.CONTENT_URI;

        String[] projection = {
                RawContacts._ID
        };
        String selection = RawContacts.SOURCE_ID + " = ? AND " + RawContacts.SYNC3 + " = " + "'"
                + ContactManager.FRIFONT_SIP_ADDRESS_CUSTOM_TYPE() + "'";
        String[] selectionArgs = new String[] {
                Long.toString(nameRawContactId)
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
        try {
            ArrayList<Long> result = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    long rawId = cursor.getLong(0);
                    result.add(rawId);
                } while (cursor.moveToNext());
                return result;
            } else {
                return null;
            }
        } finally {
            cursor.close();
        }
    }

    public static long getNameRawByRaw(Context context, long rawId) {
        Uri uri = ContactsContract.RawContacts.CONTENT_URI;

        String[] projection = {
                RawContacts.SOURCE_ID
        };
        String selection = RawContacts._ID + " = ?";
        String[] selectionArgs = new String[] {
                Long.toString(rawId)
        };
        Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
        try {
            if (cursor.moveToFirst()) {
                long nameRawId = cursor.getLong(0);
                return nameRawId;
            } else {
                return -1;
            }
        } finally {
            cursor.close();
        }
    }

    /**
     * Check if contact is matching with String data or not
     * 
     * @param contact
     * @param data
     * @return
     */
    @SuppressWarnings("unused")
    private static boolean isContactMatchWithData(QilexContact contact, String data) {
        if (Utils.isStringNullOrEmpty(data) == true) {
            return false;
        }

        String[] dataArray = data.split(Const.STR_HTML_SPACE);
        if (dataArray.length < 2) {
            return false;
        }

        // Check name
        if (dataArray[0].equals(contact.getFirstName()) == false) {
            return false;
        }
        if (dataArray[1].equals(contact.getLastName()) == false) {
            return false;
        }

        ArrayList<String> phoneNumbersData = new ArrayList<String>();
        ArrayList<String> phoneNumbersContact = new ArrayList<String>();
        PhoneNumberModel[] phoneFrifon = contact.getPhoneNumberKATZ();
        for (int i = 2; i < dataArray.length; i++) {
            phoneNumbersData.add(dataArray[i]);
        }
        for (PhoneNumberModel phoneNumberModel : phoneFrifon) {
            phoneNumbersContact.add(phoneNumberModel.phoneNumber);
        }
        PhoneNumberModel[] phoneNormal = contact.getPhoneNumberNormal();
        for (PhoneNumberModel phoneNumberModel : phoneNormal) {
            phoneNumbersContact.add(phoneNumberModel.phoneNumber);
        }

        // Compare phone
        if (phoneNumbersData.isEmpty() != phoneNumbersContact.isEmpty()) {
            return false;
        } else {
            for (String phoneData : phoneNumbersData) {
                if (phoneNumbersContact.contains(phoneData) == false) {
                    return false;
                } else {
                    phoneNumbersContact.remove(phoneData);
                }
            }
            if (phoneNumbersContact.isEmpty() == false) {
                return false;
            }
        }

        return true;
    }

    @SuppressWarnings("unused")
    private static Uri addCallerIsSyncAdapterParameter(Uri uri, boolean isSyncOperation) {
        if (isSyncOperation) {
            return uri.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "false").build();
        }
        return uri;
    }

    public static interface OnGetAllContactProcessing {
        public void onLoading(ArrayList<QilexContact> ouputAllContact, ArrayList<QilexContact> outputFrifonContact);
    }
}
