
package sigma.qilex.dataaccess.sqlitedb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.ChatThreadModel;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.Utils;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

public class ChatDatabase extends SQLiteOpenHelper {

    /**
     * Database name.
     */
    private static final String DB_NAME = "qilexMsg.db";

    /**
     * Database version.
     */
    private static final int VERSION = 13;

    // TYPE
    public static final int MSG_TYPE_SEND = 1;

    public static final int MSG_TYPE_RECEIVE = 2;

    // namnd added for calllog message
    public static final int MSG_TYPE_CALLLOG = 3;
    // namnd add mine_type
    public static final int MSG_TYPE_GROUP_STATUS = 4;

    
    // MIME TYPE
    public static final int MSG_MINETYPE_UNDENFINDED = 0;

    public static final int MSG_MINETYPE_TEXT = 1;

    public static final int MSG_MINETYPE_IMAGE = 2;

    public static final int MSG_MINETYPE_LOCATION = 3;

    public static final int MSG_MINETYPE_MISSCALL = 4;

    public static final int MSG_MINETYPE_GROUP_INVITE = 5;
    
    public static final int MSG_MINETYPE_GROUP_STATUS = 6;
    
    public static final int MSG_MINETYPE_GROUP_KICK = 7;
    // /

    /**
     * Define constant of message status. Do not update these values.
     */
    public static final int MSG_STATUS_SEND_PREPARING = 8;

    public static final int MSG_STATUS_SEND_SENDING = 10;

    public static final int MSG_STATUS_SEND_FAIL = 12;

    public static final int MSG_STATUS_SEND_SENT = 14;

    public static final int MSG_STATUS_SEND_DELIVERED = 18;

    public static final int MSG_STATUS_SEND_VIEWED = 22;

    public static final int MSG_STATUS_RECEIVE_PREPARING = 31;

    public static final int MSG_STATUS_RECEIVE_UNREAD = 34;

    public static final int MSG_STATUS_RECEIVE_READ = 38;

    /**
     * Define all Table
     */
    public interface DbTables {
        public static final String THREADS = "THREADS";

        public static final String MSG = "MSG";
    }

    /**
     * Define all Database Column.
     */
    public interface DbColumns {

        public static final String THREADS_ID = "ID";

        public static final String THREADS_CONTACT_STR = "CONTACT_STR";

        public static final String THREADS_TYPE = "TYPE";

        public static final String THREADS_DATE_MILLIS = "DATE_MILLIS";

        public static final String THREADS_SNIPPET = "SNIPPET";

        public static final String THREADS_UNREAD_COUNT = "UNREAD_COUNT";

        public static final String THREADS_MIME_TYPE = "MIME_TYPE";

        public static final String THREADS_GROUP_DATA = "THREADS_GROUP_DATA";
        
        public static final String THREADS_SNIPPET_OWNER = "THREADS_SNIPPET_OWNER";
        
        public static final String[] TABLE_THREADS_COLUMNS = {
                THREADS_ID, // 0
                THREADS_CONTACT_STR, // 1
                THREADS_TYPE, // 2
                THREADS_DATE_MILLIS, // 3
                THREADS_SNIPPET, // 4
                THREADS_UNREAD_COUNT, // 5
                THREADS_MIME_TYPE, // 6
                THREADS_GROUP_DATA, // 7 // Add for group chat
                THREADS_SNIPPET_OWNER // 8 // Add for group chat
        };

        public static final String MSG_ID = "ID";

        public static final String MSG_THREAD_ID = "THREAD_ID";

        public static final String MSG_CONTACT_STR = "CONTACT_STR";

        public static final String MSG_TYPE = "TYPE";

        public static final String MSG_MINE_TYPE = "MINE_TYPE";

        public static final String MSG_STATUS = "STATUS";

        public static final String MSG_DATE_MILLIS = "DATE_MILLIS";

        public static final String MSG_TEXT_CONTENT = "TEXT_CONTENT";

        public static final String MSG_DATE_RECEIVE_MILLIS = "DATE_RECEIVE_MILLIS";

        public static final String MSG_IMAGE_BLOB_THUMBNAIL = "IMAGE_BLOB_THUMBNAIL";

        public static final String MSG_IMAGE_URL = "IMAGE_URL";

        public static final String MSG_UUID = "UUID";

        public static final String MSG_GROUP_UUID = "MSG_GROUP_UUID";

        public static final String MSG_VIEWED_PHONES = "MSG_VIEWED_PHONES";

        public static final String MSG_DELIVERED_PHONES = "MSG_DELIVERED_PHONES";

        public static final String[] TABLE_MSG_COLUMNS = {
                MSG_ID, // 0
                MSG_THREAD_ID, // 1
                MSG_CONTACT_STR, // 2
                MSG_TYPE,//3
                MSG_MINE_TYPE, // 4
                MSG_STATUS, // 5
                MSG_DATE_MILLIS, // 6
                MSG_TEXT_CONTENT, // 7
                MSG_DATE_RECEIVE_MILLIS, // 8
                MSG_IMAGE_BLOB_THUMBNAIL, // 9
                MSG_IMAGE_URL, // 10
                MSG_UUID, // 11
                MSG_GROUP_UUID, // 12
                MSG_VIEWED_PHONES, // 13
                MSG_DELIVERED_PHONES// 14
        };
    }

    private static ChatDatabase instance;

    public static ChatDatabase getInstance() {
        if (instance == null) {
            instance = new ChatDatabase(MyApplication.getAppContext());
        }
        return instance;
    }

    private ChatDatabase(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        LogUtils.d("MESSAGE FUNCTION", "CHAT FUNTION CRETE DB ");
        // Do create table
        StringBuilder sqlCreateTabeThread = new StringBuilder();
        sqlCreateTabeThread.append("create table " + DbTables.THREADS + " ( ");
        sqlCreateTabeThread.append(DbColumns.THREADS_ID + " integer primary key asc, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_CONTACT_STR + " text key, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_TYPE + " integer, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_DATE_MILLIS + " integer , ");
        sqlCreateTabeThread.append(DbColumns.THREADS_SNIPPET + " text, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_UNREAD_COUNT + " integer, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_MIME_TYPE + " integer, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_GROUP_DATA + " text, ");
        sqlCreateTabeThread.append(DbColumns.THREADS_SNIPPET_OWNER + " text ");
        sqlCreateTabeThread.append(" );");
        db.execSQL(sqlCreateTabeThread.toString());

        StringBuilder sqlCreateTabeMsg = new StringBuilder();
        sqlCreateTabeMsg.append("create table " + DbTables.MSG + " ( ");
        sqlCreateTabeMsg.append(DbColumns.MSG_ID + " integer primary key asc, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_THREAD_ID + " integer key, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_CONTACT_STR + " text key, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_TYPE + " integer, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_MINE_TYPE + " integer, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_STATUS + " integer, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_DATE_MILLIS + " integer, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_TEXT_CONTENT + " text, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_DATE_RECEIVE_MILLIS + " integer,");
        sqlCreateTabeMsg.append(DbColumns.MSG_IMAGE_BLOB_THUMBNAIL + " blob, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_IMAGE_URL + " text, ");
        sqlCreateTabeMsg.append(DbColumns.MSG_UUID + " text,");
        sqlCreateTabeMsg.append(DbColumns.MSG_GROUP_UUID + " text,");
        sqlCreateTabeMsg.append(DbColumns.MSG_VIEWED_PHONES + " text,");
        sqlCreateTabeMsg.append(DbColumns.MSG_DELIVERED_PHONES + " text");
        sqlCreateTabeMsg.append(" );");
        db.execSQL(sqlCreateTabeMsg.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Backup data
        ArrayList<ChatThreadModel> allChatThreads = findAllMsgThread(db);
        ArrayList<ChatMessageModel> allChatMessages = getAllChatMessage(db);

        // Drop old table
        db.execSQL("DROP TABLE IF EXISTS THREADS");
        db.execSQL("DROP TABLE IF EXISTS " + DbTables.MSG);

        // Re-Create table
        onCreate(db);

        db.beginTransaction();
        try {
            // Reinsert
            for (ChatThreadModel chatThreadModel : allChatThreads) {
                db.insertWithOnConflict(DbTables.THREADS, null, chatThreadModel.parseToContentValue(),
                        SQLiteDatabase.CONFLICT_REPLACE);
            }
            for (ChatMessageModel chatMessageModel : allChatMessages) {
                db.insertWithOnConflict(DbTables.MSG, null, chatMessageModel.parseToContentValue(),
                        SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Insert new message into DB.</br> Update chat thread.
     * 
     * @param msg
     * @return The chat thread that msg is put.
     */
    public ChatThreadModel putChatMsg(ChatMessageModel msg) {
        // Check maximum message can save
        // TODO: check maximum, delete oldest chat...

        // Update Chat Thread
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        ChatThreadModel thread;

    	thread = updateChatThread(msg.getThreadUuid(), msg.type, cal.getTimeInMillis(), msg.getSnippet(),
                msg.status == MSG_STATUS_RECEIVE_UNREAD, msg.mineType);
        msg.threadId = thread.id;
        LogUtils.d("MESSAGE FUNCTION", "putChatMsg start threadId=" + thread.id);

        // Check if message is Calling Message
        if (msg.type == MSG_TYPE_CALLLOG) {
            ArrayList<ChatMessageModel> previousMess = getChatMessageFromThread(thread.id, 0, 1);
            if (previousMess.isEmpty() == false) {
                ChatMessageModel prevChat = previousMess.get(0);
                if (prevChat.type == msg.type) {
                    // Get number of previous message
                    String number = prevChat.textContent.replaceAll("[^1234567890]", Const.STR_EMPTY);
                    int count = Utils.parseIntZeroBase(number);
                    count++;
                    msg.textContent = Integer.toString(count);
                    deleteMessage(prevChat.id);
                }
            } else {
                msg.textContent = "1";
            }
        } else if (msg.type == MSG_TYPE_RECEIVE) {
        	if (msg.isGroupChat() == false) {
        		updateAllChatMsgToViewed(thread.id);
        	}
//            updateAllChatMsgToViewed(thread.id);
        }

        // Insert message
        if (Utils.isStringNullOrEmpty(msg.uuid)) {
            msg.uuid = String.format(ChatMessageModel.FORMAT_UUID, UserInfo.getInstance().deviceId, msg.dateMillis);
        }
        msg.id = getWritableDatabase().insert(DbTables.MSG, null, msg.parseToContentValue());
        return thread;
    }

    public boolean resetChatMsgStatusToSending(long id) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, MSG_STATUS_SEND_SENDING);

        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_ID + "=" + id);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
        return columnUpdate > 0;
    }

    public boolean resetChatMsgStatusToPreparing(long id) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, MSG_STATUS_SEND_PREPARING);

        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_ID + "=" + id);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
        return columnUpdate > 0;
    }

    public boolean updateChatMsgStatus(ChatMessageModel msg, int status) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, status);

        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_ID + "=" + msg.id);
        selection.append(" AND " + DbColumns.MSG_STATUS + " < " + status);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);

        if (columnUpdate > 0) {
            updateChatThread(msg.getThreadUuid(), msg.type, msg.dateMillis, msg.getSnippet(), false, msg.mineType);
        }
        return columnUpdate > 0;
    }

    public boolean updateChatMsgStatusAndContent(ChatMessageModel msg, int status, String textContent) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, status);
        cv.put(DbColumns.MSG_TEXT_CONTENT, textContent);
        msg.status = status;
        msg.textContent = textContent;

        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_ID + "=" + msg.id);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);

        if (columnUpdate > 0) {
            updateChatThread(msg.getThreadUuid(), msg.type, msg.dateMillis, msg.getSnippet(), false, msg.mineType);
        }
        return columnUpdate > 0;
    }

    public boolean updateChatMsgStatusByUuid(String uuid, int status) {
        if (Utils.isStringNullOrEmpty(uuid)) {
            return false;
        }
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, status);

        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_UUID + "='" + uuid + "'");
        selection.append(" AND " + DbColumns.MSG_STATUS + " < " + status);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
        if (columnUpdate > 0) {
            ChatMessageModel msg = findChatMessageByUuid(uuid);
            if (msg == null) {
                return false;
            }
            updateChatThread(msg.getThreadUuid(), msg.type, msg.dateMillis, msg.getSnippet(), false, msg.mineType);
        }
        return columnUpdate > 0;
    }

    /**
     * 
     * @param uuid
     * @param uuid value at [0] will have format: phone1,phone2,phone3
     * @return
     */
    public boolean updateChatMsgToViewed(String uuid) {
        if (Utils.isStringNullOrEmpty(uuid)) {
            return false;
        }
        ChatMessageModel selectedMessage = findChatMessageByUuid(uuid);
        if (selectedMessage == null) {
            return false;
        }

        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, MSG_STATUS_SEND_VIEWED);
        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_THREAD_ID + "=" + selectedMessage.threadId);
        selection.append(" AND " + DbColumns.MSG_TYPE + " = " + MSG_TYPE_SEND);
        selection.append(" AND (");
        selection.append("(" + DbColumns.MSG_UUID + "='" + uuid + "'");
        selection.append(" AND " + DbColumns.MSG_STATUS + " < " + MSG_STATUS_SEND_VIEWED);
        selection.append(" ) ");
        selection.append(" OR ");
        selection.append("(" + DbColumns.MSG_DATE_RECEIVE_MILLIS + " < " + selectedMessage.dateReceiveMillis);
        selection.append(" AND " + DbColumns.MSG_STATUS + " = " + MSG_STATUS_SEND_DELIVERED + ")");
        selection.append(")");
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
        if (columnUpdate > 0) {
            ChatMessageModel msg = findChatMessageByUuid(uuid);
            if (msg == null) {
                return false;
            }
            updateChatThread(msg.getThreadUuid(), msg.type, msg.dateMillis, msg.getSnippet(), false, msg.mineType);
        }
        return columnUpdate > 0;
    }
    
    public boolean updateChatMsgViewedMember(String messageUuid, String viewedPhoneNo) {
    	ChatMessageModel selectedMessage = findChatMessageByUuid(messageUuid);
        if (selectedMessage == null) {
            return false;
        }
        if (selectedMessage.status == MSG_STATUS_SEND_VIEWED) {
        	return false;
        }

    	ChatThreadModel currentThread = findMsgThreadById(selectedMessage.threadId);
        if (!selectedMessage.isGroupChat()) {
        	// In case single chat
        	return updateChatMsgToViewed(messageUuid);
        } else {
        	// In case group chat
        	if (!selectedMessage.viewedPhones.contains(viewedPhoneNo)) {
            	String []listParticipants = currentThread.getListPhoneNumberParticipantExceptYou();
            	boolean isViewed = true;
            	selectedMessage.viewedPhones += "," + viewedPhoneNo;
            	// Check if all are viewed.
            	for (String participantPhone : listParticipants) {
    				if (!selectedMessage.viewedPhones.contains(participantPhone)) {
    					isViewed = false;
    				}
    			}

            	if (isViewed) {
            		updateChatMsgToViewed(selectedMessage.uuid);
            	} else {
            		// Update viewed
            		ContentValues cv = new ContentValues();
            	    cv.put(DbColumns.MSG_VIEWED_PHONES, selectedMessage.viewedPhones);
        	        StringBuilder selection = new StringBuilder();
        	        selection.append(DbColumns.MSG_THREAD_ID + "=" + selectedMessage.threadId);
        	        getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
            	}
            	return true;
            } else {
            	return false;
            }
        }
    }

    public boolean updateChatMsgDeliveredMember(String messageUuid, String viewedPhoneNo) {
    	ChatMessageModel selectedMessage = findChatMessageByUuid(messageUuid);
        if (selectedMessage == null) {
            return false;
        }
        if (selectedMessage.status == MSG_STATUS_SEND_DELIVERED) {
        	return false;
        }

    	ChatThreadModel currentThread = findMsgThreadById(selectedMessage.threadId);
        if (!selectedMessage.isGroupChat()) {
        	// In case single chat
        	return updateChatMsgStatus(selectedMessage, ChatDatabase.MSG_STATUS_SEND_DELIVERED);
        } else {
        	// In case group chat
        	if (!selectedMessage.deliveredPhones.contains(viewedPhoneNo)) {
            	String []listParticipants = currentThread.getListPhoneNumberParticipantExceptYou();
            	boolean isDelivered = true;
            	selectedMessage.deliveredPhones += "," + viewedPhoneNo;
            	// Check if all are viewed.
            	for (String participantPhone : listParticipants) {
    				if (!selectedMessage.deliveredPhones.contains(participantPhone)) {
    					isDelivered = false;
    				}
    			}

            	if (isDelivered) {
            		updateChatMsgStatus(selectedMessage, ChatDatabase.MSG_STATUS_SEND_DELIVERED);
            	} else {
            		// Update viewed
            		ContentValues cv = new ContentValues();
            	    cv.put(DbColumns.MSG_DELIVERED_PHONES, selectedMessage.deliveredPhones);
        	        StringBuilder selection = new StringBuilder();
        	        selection.append(DbColumns.MSG_THREAD_ID + "=" + selectedMessage.threadId);
        	        getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
            	}
            	return true;
            } else {
            	return false;
            }
        }
    }

    public void updateAllChatMsgToViewed(long threadId) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_STATUS, MSG_STATUS_SEND_VIEWED);
        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_THREAD_ID + " = " + threadId);
        selection.append(" AND " + DbColumns.MSG_TYPE + " = " + MSG_TYPE_SEND);
        selection.append(" AND (" + DbColumns.MSG_STATUS + " = " + MSG_STATUS_SEND_DELIVERED);
        selection.append(" OR " + DbColumns.MSG_STATUS + " = " + MSG_STATUS_SEND_SENT + ")");
        getWritableDatabase().update(DbTables.MSG, cv, selection.toString(), null);
    }

    public boolean updateChatMsgContent(long msgId, String newContent) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_TEXT_CONTENT, newContent);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, DbColumns.MSG_ID + "=" + msgId, null);
        return columnUpdate > 0;
    }

    public boolean updateChatMsgThumbnail(long msgId, byte[] thumbnail) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_IMAGE_BLOB_THUMBNAIL, thumbnail);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, DbColumns.MSG_ID + "=" + msgId, null);
        return columnUpdate > 0;
    }

    public boolean updateChatMsgImageUrl(long msgId, String imageFulPath) {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_IMAGE_URL, imageFulPath);
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, DbColumns.MSG_ID + "=" + msgId, null);
        return columnUpdate > 0;
    }

    public boolean updateImageData(long msgId, String newContent, byte[] thumbnail, String imageFulPath) {
        ContentValues cv = new ContentValues();
        if (newContent != null) {
            cv.put(DbColumns.MSG_TEXT_CONTENT, newContent);
        }
        if (thumbnail != null) {
            cv.put(DbColumns.MSG_IMAGE_BLOB_THUMBNAIL, thumbnail);
        }
        if (imageFulPath != null) {
            cv.put(DbColumns.MSG_IMAGE_URL, imageFulPath);
        }
        int columnUpdate = getWritableDatabase().update(DbTables.MSG, cv, DbColumns.MSG_ID + "=" + msgId, null);
        return columnUpdate > 0;
    }

    public ArrayList<Long> updateSendingMsgStatusToError(long timeOffsetMillis) {
        ArrayList<Long> ids = new ArrayList<>();

        StringBuilder whereQuery = new StringBuilder();
        whereQuery.append("(");
        whereQuery.append(DbColumns.MSG_STATUS + " = " + MSG_STATUS_SEND_SENDING);
        whereQuery.append(" OR ");
        whereQuery.append(DbColumns.MSG_STATUS + " = " + MSG_STATUS_SEND_PREPARING);
        whereQuery.append(")");
        whereQuery.append(" AND " + DbColumns.MSG_DATE_MILLIS + " < " + timeOffsetMillis);

        // Get list updated
        Cursor cur = getReadableDatabase().query(DbTables.MSG, new String[] {
            DbColumns.MSG_ID
        }, whereQuery.toString(), null, null, null, null);
        if (cur.moveToFirst()) {
            do {
                ids.add(cur.getLong(0));
            } while (cur.moveToNext());
        }
        cur.close();

        if (ids.isEmpty() == false) {
            ContentValues cv = new ContentValues();
            cv.put(DbColumns.MSG_STATUS, MSG_STATUS_SEND_FAIL);
            getWritableDatabase().update(DbTables.MSG, cv, whereQuery.toString(), null);
        }
        return ids;
    }
    
    public boolean updateChatThreadGroupData(String contactStr, long threadId, String groupData) {
    	ChatThreadModel chatThread;
    	if (!Utils.isStringNullOrEmpty(contactStr)) {
    		chatThread = findMsgThreadByContactStr(contactStr);
    	} else {
    		chatThread = findMsgThreadById(threadId);
    	}
    	if (chatThread != null) {
    		chatThread.groupData = groupData;
    		getWritableDatabase().update(DbTables.THREADS, chatThread.parseToContentValue(),
                    DbColumns.THREADS_ID + " = " + chatThread.id, null);
    		return true;
    	} else {
    		return false;
    	}
    }

    /**
     * Update Sqlite of Table THREADS, if thread is not exist, insert new.
     * 
     * @param contactStr
     * @param type
     * @param dateMillis
     * @param snippet
     * @return the id of updated/inserted row.
     */
    public ChatThreadModel updateChatThread(String contactStr, int type, long dateMillis, String snippet,
            boolean isUpdateUnread, int mimeType) {
        ChatThreadModel chatThread = findMsgThreadByContactStr(contactStr);

        if (chatThread == null) {
            // If contactStr is not exist in DB, Insert new thread
            chatThread = new ChatThreadModel(-1, contactStr, type, dateMillis, snippet, isUpdateUnread ? 1 : 0);
            chatThread.mimetype = mimeType;
            long generatedId = getWritableDatabase().insert(DbTables.THREADS, null, chatThread.parseToContentValue());
            chatThread.id = generatedId;
        } else {
            // If contactStr is exist in DB, Update existing thread
            chatThread.type = type;
            chatThread.dateMillis = dateMillis;
            chatThread.snippet = snippet;
            chatThread.mimetype = mimeType;

        	if (chatThread.isGroupChat()) {
        		chatThread.unreadCount = countThreadUnreadMessageByGroupUuid(contactStr);
        	} else {
        		chatThread.unreadCount = countThreadUnreadMessage(contactStr);
        	}
            if (isUpdateUnread) {
            	chatThread.unreadCount++;
            }

            getWritableDatabase().update(DbTables.THREADS, chatThread.parseToContentValue(),
                    DbColumns.THREADS_ID + " = " + chatThread.id, null);
        }
        return chatThread;
    }

    public boolean deleteChatThread(long threadId) {
        if (getWritableDatabase().delete(DbTables.THREADS, DbColumns.THREADS_ID + " = " + threadId, null) > 0) {
            getWritableDatabase().delete(DbTables.MSG, DbColumns.MSG_THREAD_ID + " = " + threadId, null);
            return true;
        }
        return false;
    }

    /**
     * Method delete 1 chat message, and update/delete chat thread that contain
     * it.
     * 
     * @param messageId
     * @return
     */
    public boolean deleteMessage(long messageId) {
        // Update thread table
        ChatMessageModel chatMsg = findChatMessageById(messageId);

        // Get last chatMsg with same thread id of deleted chatMsg
        Cursor c = getReadableDatabase().query(DbTables.MSG, null,
                DbColumns.MSG_THREAD_ID + " = " + chatMsg.threadId + " AND " + DbColumns.MSG_ID + " != " + messageId,
                null, null, null, DbColumns.MSG_DATE_RECEIVE_MILLIS + " DESC", "1");
        if (c.moveToFirst()) {
            // Update Thread Snippet
            ChatMessageModel lastMsg = new ChatMessageModel(c);
            ChatThreadModel thread = findMsgThreadById(chatMsg.threadId);
            updateChatThread(thread.contactStr, lastMsg.type, lastMsg.dateMillis, lastMsg.getSnippet(), false,
                    lastMsg.mineType);
        } else if (!chatMsg.isGroupChat()) {
            // Delete Thread
            deleteChatThread(chatMsg.threadId);
        }
        c.close();

        // Delete message id
        return getWritableDatabase().delete(DbTables.MSG, DbColumns.MSG_ID + " = " + messageId, null) > 0;
    }

    /**
     * Get ChatThread info by ID.
     * 
     * @param threadId
     * @return The ChatThreads Object.
     */
    public ChatThreadModel findMsgThreadById(long threadId) {
        Cursor c = getReadableDatabase().query(DbTables.THREADS, null, DbColumns.THREADS_ID + " = " + threadId, null,
                null, null, null);
        ChatThreadModel chatThread = null;
        if (c != null) {
            if (c.moveToFirst()) {
                chatThread = new ChatThreadModel(c);
            }
            c.close();
        }

        return chatThread;
    }

    /**
     * Get ChatThread info by Contact String.
     * 
     * @param contactStr
     * @return The ChatThreads Object.
     */
    public ChatThreadModel findMsgThreadByContactStr(String contactStr) {
        Cursor c = getReadableDatabase().query(DbTables.THREADS, null,
                DbColumns.THREADS_CONTACT_STR + " = '" + contactStr + "'", null, null, null, null);
        ChatThreadModel chatThread = null;
        if (c != null) {
            if (c.moveToFirst()) {
                chatThread = new ChatThreadModel(c);
            }
            c.close();
        }
        return chatThread;
    }

    /**
     * Select Messages from a thread.
     * 
     * @param threadId a thread id.
     * @param limit number of record will be get ( = 0 if get all).
     * @return list of ChatMessageModel.
     */
    public ArrayList<ChatMessageModel> getChatMessageFromThread(long threadId, final int offset, int limit) {
        ArrayList<ChatMessageModel> listResult = new ArrayList<ChatMessageModel>();

        Cursor c = getReadableDatabase().query(DbTables.MSG, null, DbColumns.MSG_THREAD_ID + " = " + threadId, null,
                null, null, DbColumns.MSG_DATE_RECEIVE_MILLIS + " DESC", offset + ", " + limit);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ChatMessageModel model = new ChatMessageModel(c);
                    listResult.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listResult;
    }

    public ArrayList<ChatMessageModel> getAllChatMessage(SQLiteDatabase db) {
        ArrayList<ChatMessageModel> listResult = new ArrayList<ChatMessageModel>();

        Cursor c = db.query(DbTables.MSG, null, null, null, null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ChatMessageModel model = new ChatMessageModel(c);
                    listResult.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listResult;
    }

    public ArrayList<ChatMessageModel> getChatMessageImageFromThread(long threadId) {
        ArrayList<ChatMessageModel> listResult = new ArrayList<ChatMessageModel>();

        StringBuilder selection = new StringBuilder();
        selection.append(DbColumns.MSG_THREAD_ID + " = " + threadId);
        selection.append(" AND (" + DbColumns.MSG_MINE_TYPE + " = " + MSG_MINETYPE_IMAGE + ")");
        Cursor c = getReadableDatabase().query(DbTables.MSG, null, selection.toString(), null, null, null,
                DbColumns.MSG_DATE_RECEIVE_MILLIS + " DESC", null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ChatMessageModel model = new ChatMessageModel(c);
                    listResult.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }

        return listResult;
    }

    /**
     * Find message by message id.
     * 
     * @param msgId
     * @return
     */
    public ChatMessageModel findChatMessageById(long msgId) {
        Cursor c = getReadableDatabase().query(DbTables.MSG, null, DbColumns.MSG_ID + " = " + msgId, null, null, null,
                null);
        ChatMessageModel result = null;
        if (c != null) {
            if (c.moveToFirst()) {
                result = new ChatMessageModel(c);
            }
            c.close();
        }
        return result;
    }

    /**
     * Find message by message uuid.
     * 
     * @param uuid
     * @return
     */
    public ChatMessageModel findChatMessageByUuid(String uuid) {
        Cursor c = getReadableDatabase().query(DbTables.MSG, null, DbColumns.MSG_UUID + " = '" + uuid + "'", null,
                null, null, null);
        ChatMessageModel result = null;
        if (c != null) {
            if (c.moveToFirst()) {
                result = new ChatMessageModel(c);
            }
            c.close();
        }
        return result;
    }
    
    /**
     * Find message by message Group uuid.
     * 
     * @param groupChatUuid
     * @return
     */
    public ChatMessageModel findChatMessageByGroupChatUuid(String groupChatUuid) {
        Cursor c = getReadableDatabase().query(DbTables.MSG, null, DbColumns.MSG_GROUP_UUID + " = '" + groupChatUuid + "'", null,
                null, null, null);
        ChatMessageModel result = null;
        if (c != null) {
            if (c.moveToFirst()) {
                result = new ChatMessageModel(c);
            }
            c.close();
        }
        return result;
    }

    /**
     * Find message by message id.
     * 
     * @param msgId
     * @return
     */
    public ArrayList<ChatMessageModel> findChatMessageById(Long... msgId) {
        String ids = "(" + TextUtils.join(",", msgId) + ")";
        Cursor c = getReadableDatabase().query(DbTables.MSG, null, DbColumns.MSG_ID + " IN " + ids, null, null, null,
                null);
        ArrayList<ChatMessageModel> result = new ArrayList<>();
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ChatMessageModel model = new ChatMessageModel(c);
                    result.add(model);
                } while (c.moveToNext());
            }
            c.close();
        }
        return result;
    }

    /**
     * Get all message thread.
     * 
     * @return list of ChatThreadModel.
     */
    public ArrayList<ChatThreadModel> findAllMsgThread(int limit) {
        ArrayList<ChatThreadModel> listResult = new ArrayList<ChatThreadModel>();
        String sortQuery = DbColumns.THREADS_DATE_MILLIS + " desc";
        if (limit > 0) {
            sortQuery += " LIMIT " + limit;
        }
        Cursor c = getReadableDatabase().query(DbTables.THREADS, null, null, null, null, null, sortQuery);
        ChatThreadModel chatThread = null;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    LogUtils.d("MESSAGE FUNCTION", "putChatMsg findAllMsgThreadc=" + c.getColumnCount());
                    chatThread = new ChatThreadModel(c);
                    listResult.add(chatThread);
                } while (c.moveToNext());
            }
            c.close();
        }
        return listResult;
    }

    /**
     * Count unread message from a message thread.
     *
     * @return number of unread message.
     */
    public int countUnreadMessage() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(DbTables.MSG);
        query.append(" WHERE ");
        query.append(DbColumns.MSG_STATUS + " = " + MSG_STATUS_RECEIVE_UNREAD);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    public int countUnreadThread() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(DbTables.THREADS);
        query.append(" WHERE ");
        query.append(DbColumns.THREADS_UNREAD_COUNT + " > 0 ");
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    public int countAllThread() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(DbTables.THREADS);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    /**
     * Count unread message from a message thread.
     * 
     * @param contactStr
     * @return number of unread message.
     */
    public int countThreadUnreadMessage(String contactStr) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(DbTables.MSG);
        query.append(" WHERE ");
        query.append(DbColumns.MSG_CONTACT_STR + " = '" + contactStr + "'");
        query.append(" AND " + DbColumns.MSG_STATUS + " = " + MSG_STATUS_RECEIVE_UNREAD);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }
    
    public int countThreadUnreadMessageByGroupUuid(String groupUuid) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(DbTables.MSG);
        query.append(" WHERE ");
        query.append(DbColumns.MSG_GROUP_UUID + " = '" + groupUuid + "'");
        query.append(" AND " + DbColumns.MSG_STATUS + " = " + MSG_STATUS_RECEIVE_UNREAD);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    /**
     * Count unread message from a message thread.
     * 
     * @param threadId
     * @return number of unread message.
     */
    public int countThreadUnreadMessage(long threadId) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (*) FROM ");
        query.append(DbTables.MSG);
        query.append(" WHERE ");
        query.append(DbColumns.MSG_THREAD_ID + " = '" + threadId + "'");
        query.append(" AND " + DbColumns.MSG_STATUS + " = " + MSG_STATUS_RECEIVE_UNREAD);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    /**
     * Count all message from a message thread.
     * 
     * @param threadId
     * @return number of unread message.
     */
    public int countAllMessage(long threadId) {
        StringBuilder query = new StringBuilder();
        query.append("SELECT COUNT (ID) FROM ");
        query.append(DbTables.MSG);
        query.append(" WHERE ");
        query.append(DbColumns.MSG_THREAD_ID + " = " + threadId);
        Cursor c = getReadableDatabase().rawQuery(query.toString(), null);
        int count = 0;
        if (null != c) {
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            c.close();
        }
        return count;
    }

    public void markAllRead(long threadId) {
        // Update table THREAD
        ContentValues cvThread = new ContentValues();
        cvThread.put(DbColumns.THREADS_UNREAD_COUNT, 0);
        getWritableDatabase().update(DbTables.THREADS, cvThread, DbColumns.THREADS_ID + " = " + threadId, null);

        // Update Table MSG
        ContentValues cvMsg = new ContentValues();
        cvMsg.put(DbColumns.MSG_STATUS, MSG_STATUS_RECEIVE_READ);
        getWritableDatabase().update(DbTables.MSG, cvMsg,
                DbColumns.MSG_THREAD_ID + " = " + threadId + " AND " + DbColumns.MSG_TYPE + " >= " + MSG_TYPE_RECEIVE,
                null);
    }
    
    public void markAllMsgStatusRead(long threadId) {
        // Update table THREAD
        ContentValues cvThread = new ContentValues();
        cvThread.put(DbColumns.THREADS_UNREAD_COUNT, 0);
        getWritableDatabase().update(DbTables.THREADS, cvThread, DbColumns.THREADS_ID + " = " + threadId, null);

        // Update Table MSG
        ContentValues cvMsg = new ContentValues();
        cvMsg.put(DbColumns.MSG_STATUS, MSG_STATUS_RECEIVE_READ);
        getWritableDatabase().update(DbTables.MSG, cvMsg,
                DbColumns.MSG_THREAD_ID + " = " + threadId
                + " AND " + DbColumns.MSG_TYPE + " = " + MSG_TYPE_GROUP_STATUS,
                null);
    }

    public void clearDB() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.delete(DbTables.MSG, null, null);
            db.delete(DbTables.THREADS, null, null);
        } finally {
            db.close();
        }
    }

    /**
     * Get all message thread.
     * 
     * @return list of ChatThreadModel.
     */
    public ArrayList<ChatThreadModel> findAllMsgThread(SQLiteDatabase db) {
        ArrayList<ChatThreadModel> listResult = new ArrayList<ChatThreadModel>();
        Cursor c = db.query(DbTables.THREADS, null, null, null, null, null, null);
        ChatThreadModel chatThread = null;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    chatThread = new ChatThreadModel(c);
                    listResult.add(chatThread);
                } while (c.moveToNext());
            }
            c.close();
        }
        return listResult;
    }
}
