
package sigma.qilex.dataaccess.model;

public class PhoneCodeModel extends GlobalParcelable {

    public String countryCode;

    public String phoneCode;

    public String countryName;
}
