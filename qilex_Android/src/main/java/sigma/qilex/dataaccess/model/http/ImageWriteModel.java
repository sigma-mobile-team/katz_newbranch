
package sigma.qilex.dataaccess.model.http;

import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;
import sigma.qilex.utils.Utils;

public class ImageWriteModel {

    public String image_uri = Const.STR_EMPTY;

    public String thumb_uri = Const.STR_EMPTY;

    public String token = Const.STR_EMPTY;

    /**
     * This is additional data, not return from AS Api.
     */
    public String description = Const.STR_EMPTY;

    public ImageWriteModel() {

    }

    public ImageWriteModel(JSONObject jsonData) throws JSONException {
        // Image uri
        image_uri = jsonData.getString("image_uri");
        if (Utils.isStringNullOrEmpty(image_uri) == false) {
            if (image_uri.startsWith("/") == true) {
                image_uri = image_uri.substring(1);
            }
        }

        // Image thumbnail uri
        thumb_uri = jsonData.getString("thumb_uri");
        if (Utils.isStringNullOrEmpty(thumb_uri) == false) {
            if (thumb_uri.startsWith("/") == true) {
                thumb_uri = thumb_uri.substring(1);
            }
        }

        // token
        token = jsonData.getString("token");
        description = jsonData.has("description") ? jsonData.getString("description") : Const.STR_EMPTY;
    }

    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("image_uri", "/" + image_uri.replace(Const.SERVER_URL, Const.STR_EMPTY));
            obj.put("thumb_uri", "/" + thumb_uri.replace(Const.SERVER_URL, Const.STR_EMPTY));
            obj.put("token", token);
            obj.put("description", description);
            return obj;
        } catch (JSONException e) {
            e.printStackTrace();
            return obj;
        }
    }
}
