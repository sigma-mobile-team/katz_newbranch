
package sigma.qilex.dataaccess.model;

import android.os.Parcel;

public class CallInfo extends GlobalParcelable {

    String numberPhone;

    String contactName;

    boolean isKATZOut;

    int startTime;

    boolean established;

    public CallInfo() {
        this("", false);
    }

    public CallInfo(String number, boolean katzOut) {
        super();
        this.numberPhone = number;
        this.isKATZOut = katzOut;
        this.contactName = number;
        this.established = false;
        startTime = 0;
    }

    public CallInfo(Parcel in) {
        super(in);
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public boolean isKATZOut() {
        return isKATZOut;
    }

    public void setKATZOut(boolean isKATZOut) {
        this.isKATZOut = isKATZOut;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public boolean isEstablished() {
        return established;
    }

    public void setEstablished(boolean established) {
        this.established = established;
    }
}
