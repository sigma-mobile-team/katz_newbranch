
package sigma.qilex.dataaccess.model;

public class FrifonOutObj extends GlobalParcelable {

    long time;

    String id;

    String price;

    String moneyType;

    String number;

    long duration;

    public FrifonOutObj() {
        super();
    }

    public FrifonOutObj(FrifonOutObj obj) {
        if (obj != null) {
            time = obj.time;
            id = obj.id;
            price = obj.price;
            moneyType = obj.moneyType;
            number = obj.number;
            duration = obj.duration;
        }
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(String moneyType) {
        this.moneyType = moneyType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

}
