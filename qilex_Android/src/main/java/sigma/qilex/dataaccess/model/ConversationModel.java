
package sigma.qilex.dataaccess.model;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * Class represent data of call log.
 */
public class ConversationModel extends GlobalParcelable {

    private String avatarUrl;

    private String name;

    private long dateMillis;

    private String firstContent;
    
    private String katzNumber;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDateMillis() {
        return dateMillis;
    }

    public void setDateMillis(long dateMillis) {
        this.dateMillis = dateMillis;
    }

    public String getFirstContent() {
        return firstContent;
    }

    public void setFirstContent(String firstContent) {
        this.firstContent = firstContent;
    }
    
    public String getKatzNumber() {
		return katzNumber;
	}

	public void setKatzNumber(String katzNumber) {
		this.katzNumber = katzNumber;
	}

	public static class NameComparator implements Comparator<ConversationModel> {
        @Override
        public int compare(ConversationModel c1, ConversationModel c2) {
            Collator compareOperator = Collator.getInstance(new Locale("vi"));
            compareOperator.setStrength(Collator.SECONDARY);
            long date1 = c1.dateMillis;
            long date2 = c2.dateMillis;
            long compare = date1 - date2;
            if (compare < 0) {
                return -1;
            } else if (compare > 0) {
                return 1;
            } else {
                return 0;
            }
        }

    }
}
