
package sigma.qilex.dataaccess.model.http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;

public class OutCallListHistoryModel {

    public int calls_num;

    public String msisdn;

    public int on_page;

    public int page;

    public OutCallModel[] out_calls;

    public OutCallListHistoryModel() {
    }

    public OutCallListHistoryModel(JSONObject jsonData) throws JSONException {
        calls_num = jsonData.has("calls_num") ? jsonData.getInt("calls_num") : 0;
        msisdn = jsonData.has("msisdn") ? jsonData.getString("msisdn") : Const.STR_EMPTY;
        on_page = jsonData.has("on_page") ? jsonData.getInt("on_page") : 0;
        page = jsonData.has("page") ? jsonData.getInt("page") : 0;

        JSONArray outCallArray = jsonData.getJSONArray("out_calls");
        int size = outCallArray.length();
        out_calls = new OutCallModel[size];
        for (int i = 0; i < size; i++) {
            out_calls[i] = new OutCallModel(outCallArray.getJSONObject(i));
        }

        page = jsonData.has("page") ? jsonData.getInt("page") : 0;
    }

    public static class OutCallModel {
        public String called;

        public String caller;

        public String duration;

        public double price;

        public double rate;

        public String start;

        public OutCallModel() {
        }

        public OutCallModel(JSONObject jsonData) {
            try {
                called = jsonData.has("called") ? jsonData.getString("called") : Const.STR_EMPTY;
                caller = jsonData.has("caller") ? jsonData.getString("caller") : Const.STR_EMPTY;
                duration = jsonData.has("duration") ? jsonData.getString("duration") : Const.STR_EMPTY;
                price = jsonData.has("price") ? jsonData.getDouble("price") : 0;
                rate = jsonData.has("rate") ? jsonData.getDouble("rate") : 0;
                start = jsonData.has("start") ? jsonData.getString("start") : Const.STR_EMPTY;
            } catch (JSONException e) {
                called = null;
                caller = null;
                duration = null;
                price = 0;
                rate = 0;
                start = null;
            }
        }
    }
}
