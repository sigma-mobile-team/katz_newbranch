
package sigma.qilex.dataaccess.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;

public class PaymentModel {

    public String appStoreInfo;

    public String userData;

    public String base64Payment;

    public String googlePlayProductId;

    public String appStoreProductId;

    public PaymentModel() {
        appStoreInfo = Const.STR_EMPTY;
        userData = Const.STR_EMPTY;
        base64Payment = Const.STR_EMPTY;
        googlePlayProductId = Const.STR_EMPTY;
        appStoreProductId = Const.STR_EMPTY;
    }

    public PaymentModel(String appStoreInfo, String userData, String base64Payment, String googlePlayProductId,
            String appStoreProductId) {
        this.appStoreInfo = appStoreInfo;
        this.userData = userData;
        this.base64Payment = base64Payment;
        this.googlePlayProductId = googlePlayProductId;
        this.appStoreProductId = appStoreProductId;
    }

    public PaymentModel(JSONObject object) {
        try {
            appStoreInfo = object.getString("appStoreInfo");
            userData = object.getString("userData");
            base64Payment = object.getString("base64Payment");
            googlePlayProductId = object.getString("googlePlayProductId");
            appStoreProductId = object.getString("appStoreProductId");
        } catch (JSONException e) {
            appStoreInfo = Const.STR_EMPTY;
            userData = Const.STR_EMPTY;
            base64Payment = Const.STR_EMPTY;
            googlePlayProductId = Const.STR_EMPTY;
            appStoreProductId = Const.STR_EMPTY;
        }
    }

    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("appStoreInfo", appStoreInfo);
            obj.put("userData", userData);
            obj.put("base64Payment", base64Payment);
            obj.put("googlePlayProductId", googlePlayProductId);
            obj.put("appStoreProductId", appStoreProductId);
        } catch (JSONException e) {
        }
        return obj;
    }

    /**
     * Load payment info from json array data
     * 
     * @param jsonArray
     * @return
     */
    public static ArrayList<PaymentModel> loadFromJsonArray(String strJsonArray) {
        ArrayList<PaymentModel> listResult = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(strJsonArray);
            int size = jsonArray.length();
            for (int i = 0; i < size; i++) {
                PaymentModel paymet = new PaymentModel(jsonArray.getJSONObject(i));
                listResult.add(paymet);
            }
        } catch (JSONException e) {
        }
        return listResult;
    }

    /**
     * Convert listPayment to JSONArray.
     * 
     * @param listPayment
     * @return
     */
    public static JSONArray toJsonArray(ArrayList<PaymentModel> listPayment) {
        if (listPayment == null || listPayment.isEmpty()) {
            return null;
        }
        JSONArray jsonArray = new JSONArray();
        for (PaymentModel paymentModel : listPayment) {
            jsonArray.put(paymentModel.toJsonObject());
        }
        return jsonArray;
    }
}
