
package sigma.qilex.dataaccess.model;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import android.content.Context;
import android.os.Parcel;
import pl.katz.aero2.Const;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;

public class QilexContact extends GlobalParcelable {

    public ArrayList<String> phoneTypes = new ArrayList<>();

    public ArrayList<String> plusPhones = new ArrayList<>();

    long contentId;

    public long serverContactId;

    public long rawContactId;

    public String photoUri;

    public String photoUriFull;

    public String photoPhoneUri;

    public long id;

    public String lookUpKey;

    public String firstName;

    public String lastName;

    String middleName;

    public ArrayList<PhoneNumberModel> phoneNumberNormal;

    public ArrayList<PhoneNumberModel> phoneNumberKATZ;

    String displayName;

    boolean katz;

    private String midName;

    public String latestSyncData;

    public String originAccountType;

    public boolean isEditable;

    public QilexContact() {
        super();
        firstName = "";
        lastName = "";
        photoUri = "";
        photoUriFull = "";
        rawContactId = -1;
        serverContactId = -1;
        id = -1;
        phoneNumberKATZ = new ArrayList<>();
        phoneNumberNormal = new ArrayList<>();
        lookUpKey = "";
        originAccountType = "";
        isEditable = true;
        photoPhoneUri = "";
    }

    public QilexContact(Parcel in) {
        super(in);
    }

    public QilexContact(QilexContact contact) {
        if (contact != null) {
            contentId = contact.contentId;
            serverContactId = contact.serverContactId;
            rawContactId = contact.rawContactId;
            photoUri = contact.photoUri;
            firstName = contact.firstName;
            lastName = contact.lastName;
            phoneNumberNormal = contact.phoneNumberNormal;
            phoneNumberKATZ = contact.phoneNumberKATZ;
            displayName = contact.displayName;
            katz = contact.katz;
        }
    }

    public QilexContact(long contactId, String displayName, String photoUri) {
        super();
        this.contentId = contactId;
        this.displayName = displayName;
        this.photoUri = photoUri;
    }

    public QilexContact(long contactId, String displayName, String number, String photoUri) {
        super();
        this.contentId = contactId;
        this.displayName = displayName;
        this.photoUri = photoUri;
    }

    public QilexContact(String displayName, String number, String photoUri) {
        super();
        this.displayName = displayName;
        this.photoUri = photoUri;
    }

    public QilexContact(String displayName, long rawContactId, String photoUri) {
        this.displayName = displayName;
        this.rawContactId = rawContactId;
        this.photoUri = photoUri;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public String getDisplayName() {
        // FirstName + LastName.
        // If FirstName and LastName are both blank, Get First KATZ Number.
        // If KATZ Number is null, Get first Normal Number.
        // If Normal Number is null, return "Unknown".
        if (Utils.isStringNullOrEmpty(firstName) && Utils.isStringNullOrEmpty(lastName)) {
            if (phoneNumberKATZ != null && phoneNumberKATZ.size() > 0) {
                return phoneNumberKATZ.get(0).phoneNumber;
            } else if (phoneNumberNormal != null && phoneNumberNormal.size() > 0) {
                return phoneNumberNormal.get(0).phoneNumber;
            } else {
                return Const.STR_UNKNOWN;
            }
        } else {
            if (Utils.isStringNullOrEmpty(firstName)) {
                return lastName;
            }
            if (Utils.isStringNullOrEmpty(lastName)) {
                return firstName;
            }
            return firstName + Const.STR_SPACE + lastName;
        }
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public long getContentContactId() {
        return contentId;
    }

    public void setContentContactId(long id) {
        this.contentId = id;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public PhoneNumberModel getFirstKATZNumber() {
        if (isKATZ()) {
            return phoneNumberKATZ.get(0);
        }
        return null;
    }

    public PhoneNumberModel getFirstNormalNumber() {
        if (isKATZ() == false) {
            return phoneNumberNormal.get(0);
        }
        return null;
    }

    public boolean isKATZ() {
        return phoneNumberKATZ != null && phoneNumberKATZ.size() > 0;
    }

    public void setKatz(boolean katz) {
        this.katz = katz;
    }

    public long getRawContactId() {
        return rawContactId;
    }

    public void setRawContactId(long rawContactId) {
        this.rawContactId = rawContactId;
    }

    public long getServerContactId() {
        return serverContactId;
    }

    public void setServerContactId(long dbId) {
        this.serverContactId = dbId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUriFull() {
        return photoUriFull;
    }

    public void setPhotoUriFull(String photoUriFull) {
        this.photoUriFull = photoUriFull;
    }

    public PhoneNumberModel[] getPhoneNumberNormal() {
        if (phoneNumberNormal == null) {
            return null;
        }
        PhoneNumberModel[] phoneNormal = new PhoneNumberModel[phoneNumberNormal.size()];
        phoneNormal = phoneNumberNormal.toArray(phoneNormal);
        return phoneNormal;
    }

    public void setPhoneNumberNormal(String[] phoneNumberNormal) {
        this.phoneNumberNormal = new ArrayList<>();
        for (String string : phoneNumberNormal) {
            this.phoneNumberNormal.add(new PhoneNumberModel(string));
        }
    }

    public PhoneNumberModel[] getPhoneNumberKATZ() {
        if (phoneNumberKATZ == null) {
            return null;
        }
        PhoneNumberModel[] phoneKATZ = new PhoneNumberModel[phoneNumberKATZ.size()];
        phoneKATZ = phoneNumberKATZ.toArray(phoneKATZ);
        return phoneKATZ;
    }

    public void setPhoneNumberKATZ(String[] phoneNumberKATZ) {
        this.phoneNumberKATZ = new ArrayList<>();
        for (String string : phoneNumberKATZ) {
            this.phoneNumberKATZ.add(new PhoneNumberModel(string));
        }
    }

    public boolean isInitPhoneNo() {
        return (phoneNumberNormal != null) || (phoneNumberKATZ != null);
    }

    public boolean hasPhoneNumber(String phoneNum) {
        ArrayList<PhoneNumberModel> phoneList = new ArrayList<>();
        phoneList.addAll(phoneNumberKATZ);
        phoneList.addAll(phoneNumberNormal);

        for (PhoneNumberModel phoneNumberModel : phoneList) {
            if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber, phoneNum) == true) {
                return true;
            }
        }
        return false;
    }

    public boolean hasKATZNumber(String phoneNum) {
        ArrayList<PhoneNumberModel> phoneList = new ArrayList<>();
        phoneList.addAll(phoneNumberKATZ);
        for (PhoneNumberModel phoneNumberModel : phoneList) {
            if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber, phoneNum) == true) {
                return true;
            }
        }
        return false;
    }

    public boolean hasNormalNumber(String phoneNum) {
        ArrayList<PhoneNumberModel> phoneList = new ArrayList<>();
        phoneList.addAll(phoneNumberNormal);
        for (PhoneNumberModel phoneNumberModel : phoneList) {
            if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber, phoneNum) == true) {
                return true;
            }
        }
        return false;
    }

    public void changeKatzPhoneToNormal(String phoneNum) {
        for (PhoneNumberModel phoneNumberModel : phoneNumberKATZ) {
            if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber, phoneNum) == true) {
                phoneNumberKATZ.remove(phoneNumberModel);
                phoneNumberNormal.add(phoneNumberModel);
                break;
            }
        }
    }

    public void changeNormalPhoneToKatz(String phoneNum) {
        for (PhoneNumberModel phoneNumberModel : phoneNumberNormal) {
            if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneNumberModel.phoneNumber, phoneNum) == true) {
                phoneNumberNormal.remove(phoneNumberModel);
                phoneNumberKATZ.add(phoneNumberModel);
                break;
            }
        }
    }

    public boolean hasPhoneNumber() {
        return (phoneNumberKATZ != null && phoneNumberKATZ.size() > 0)
                || (phoneNumberNormal != null && phoneNumberNormal.size() > 0);
    }

    public void addCurrentPhoneCodeToAllPhone() {
        PhoneNumberModel[] katzs = getPhoneNumberKATZ();
        PhoneNumberModel[] normals = getPhoneNumberNormal();
        int index = 0;
        if (katzs.length > 0) {
            String[] phoneNoKATZ = new String[katzs.length];
            for (PhoneNumberModel phoneNumberModel : katzs) {
                phoneNumberModel.phoneNumber = QilexPhoneNumberUtils
                        .addCurrentCountryCodeToPhone(phoneNumberModel.phoneNumber);
                phoneNoKATZ[index] = phoneNumberModel.toString();
                index++;
            }
            setPhoneNumberKATZ(phoneNoKATZ);
        }
        index = 0;
        if (normals.length > 0) {
            String[] phoneNoNormal = new String[normals.length];
            for (PhoneNumberModel phoneNumberModel : normals) {
                phoneNumberModel.phoneNumber = QilexPhoneNumberUtils
                        .addCurrentCountryCodeToPhone(phoneNumberModel.phoneNumber);
                phoneNoNormal[index] = phoneNumberModel.toString();
                index++;
            }
            setPhoneNumberNormal(phoneNoNormal);
        }
    }

    public String toSyncDataString() {
        ArrayList<String> datas = new ArrayList<>();
        datas.add(firstName);
        datas.add(lastName);
        for (PhoneNumberModel phone : phoneNumberNormal) {
            datas.add(phone.phoneNumber);
        }
        for (PhoneNumberModel phone : phoneNumberKATZ) {
            datas.add(phone.phoneNumber);
        }
        Collections.sort(datas);
        String decode = null;
        try {
            decode = Zip.encodeBase64(datas.toString());
        } catch (UnsupportedEncodingException e) {
            decode = datas.toString();
        }
        return decode;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[" + firstName + "," + lastName + "]");
        return result.toString();
    }

    public static class NameComparator implements Comparator<Object> {
        WeakReference<Context> contextRef;

        public NameComparator(Context context) {
            contextRef = new WeakReference<Context>(context);
        }

        @Override
        public int compare(Object c1, Object c2) {
            Locale locale = Locale.US;
            if (contextRef.get() != null) {
                locale = contextRef.get().getResources().getConfiguration().locale;
            }
            Collator compareOperator = Collator.getInstance(locale);
            compareOperator.setStrength(Collator.SECONDARY);
            String name1;
            String name2;
            if (c1 instanceof QilexContact && c2 instanceof QilexContact) {
                name1 = ((QilexContact)c1).getDisplayName().trim();
                name2 = ((QilexContact)c2).getDisplayName().trim();
            } else {// instance of string
                name1 = String.valueOf(c1);
                name2 = String.valueOf(c2);
            }
            // ascending order (descending order would be:
            // name2.compareTo(name1))
            return compareOperator.compare(name1, name2);

        }
    }
}
