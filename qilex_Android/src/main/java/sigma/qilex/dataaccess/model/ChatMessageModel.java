
package sigma.qilex.dataaccess.model;

import java.util.Comparator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.ImageWriteModel;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase.DbColumns;
import sigma.qilex.manager.chat.LocationChatModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

/**
 * Class represent data of Sqlite Table MSG.
 */
public class ChatMessageModel extends GlobalParcelable {

    public static final String FORMAT_UUID = "%s.%d";

    public long id;

    public long threadId;

    public String contactStr;

    public int type;

    public int mineType;

    public int status;

    public long dateMillis;

    /**
     * Data have format in 2 lines:  [Kick contacts]\br[Invite contacts]
     */
    public String textContent;

    public long dateReceiveMillis;

    public byte[] imageThumbnail;

    public String imageUrl;

    public String uuid;
    
    /**
     * 
     */
    public String groupUuid;
    
    public String viewedPhones;
    
    public String deliveredPhones;
    
    public ChatMessageModel() {
        super();
    }

    public ChatMessageModel(Parcel in) {
        super(in);
    }

    public ChatMessageModel(long id, long threadId, String contactStr, int type, int minetype, int status,
            long dateMillis, String textContent, long dateReceiveMillis) {
        this.id = id;
        this.threadId = threadId;
        this.contactStr = contactStr;
        this.type = type;
        this.mineType = minetype;
        this.status = status;
        this.dateMillis = dateMillis;
        this.textContent = textContent;
        this.dateReceiveMillis = dateReceiveMillis;
        this.imageThumbnail = new byte[0];
        this.imageUrl = Const.STR_EMPTY;
        this.uuid = Const.STR_EMPTY;
        this.groupUuid = Const.STR_EMPTY;
        this.viewedPhones = Const.STR_EMPTY;
        this.deliveredPhones = Const.STR_EMPTY;
    }

    public ChatMessageModel(Cursor c) {
        this.id = c.getLong(c.getColumnIndex(DbColumns.MSG_ID));
        this.threadId = c.getLong(c.getColumnIndex(DbColumns.MSG_THREAD_ID));
        this.contactStr = c.getString(c.getColumnIndex(DbColumns.MSG_CONTACT_STR));
        this.type = c.getInt(c.getColumnIndex(DbColumns.MSG_TYPE));

        int mimeTypeIndex = c.getColumnIndex(DbColumns.MSG_MINE_TYPE);
        this.mineType = mimeTypeIndex > -1 ? c.getInt(mimeTypeIndex) : ChatDatabase.MSG_MINETYPE_TEXT;

        this.status = c.getInt(c.getColumnIndex(DbColumns.MSG_STATUS));
        this.dateMillis = c.getLong(c.getColumnIndex(DbColumns.MSG_DATE_MILLIS));
        this.textContent = c.getString(c.getColumnIndex(DbColumns.MSG_TEXT_CONTENT));
        this.dateReceiveMillis = c.getLong(c.getColumnIndex(DbColumns.MSG_DATE_RECEIVE_MILLIS));

        int imageThumbnailIndex = c.getColumnIndex(DbColumns.MSG_IMAGE_BLOB_THUMBNAIL);
        this.imageThumbnail = imageThumbnailIndex > -1 ? c.getBlob(imageThumbnailIndex) : null;

        int imageUrlIndex = c.getColumnIndex(DbColumns.MSG_IMAGE_URL);
        this.imageUrl = imageUrlIndex > -1 ? c.getString(imageUrlIndex) : Const.STR_EMPTY;

        int uuidIndex = c.getColumnIndex(DbColumns.MSG_UUID);
        this.uuid = uuidIndex > -1 ? c.getString(uuidIndex) : Const.STR_EMPTY;

        int threadUuidIndex = c.getColumnIndex(DbColumns.MSG_GROUP_UUID);
        this.groupUuid = threadUuidIndex > -1 ? c.getString(threadUuidIndex) : Const.STR_EMPTY;

        int viewedPhonesIndex = c.getColumnIndex(DbColumns.MSG_VIEWED_PHONES);
        this.viewedPhones = viewedPhonesIndex > -1 ? c.getString(viewedPhonesIndex) : Const.STR_EMPTY;
        if (this.viewedPhones == null) this.viewedPhones = Const.STR_EMPTY;

        int deliveredPhonesIndex = c.getColumnIndex(DbColumns.MSG_DELIVERED_PHONES);
        this.deliveredPhones = deliveredPhonesIndex > -1 ? c.getString(deliveredPhonesIndex) : Const.STR_EMPTY;
        if (this.deliveredPhones == null) this.deliveredPhones = Const.STR_EMPTY;
    }

    public ContentValues parseToContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.MSG_THREAD_ID, threadId);
        cv.put(DbColumns.MSG_CONTACT_STR, contactStr);
        cv.put(DbColumns.MSG_TYPE, type);
        cv.put(DbColumns.MSG_MINE_TYPE, mineType);
        cv.put(DbColumns.MSG_STATUS, status);
        cv.put(DbColumns.MSG_DATE_MILLIS, dateMillis);
        cv.put(DbColumns.MSG_TEXT_CONTENT, textContent);
        cv.put(DbColumns.MSG_DATE_RECEIVE_MILLIS, dateReceiveMillis);
        cv.put(DbColumns.MSG_IMAGE_BLOB_THUMBNAIL, imageThumbnail);
        cv.put(DbColumns.MSG_IMAGE_URL, imageUrl);
        cv.put(DbColumns.MSG_UUID, uuid);
        cv.put(DbColumns.MSG_GROUP_UUID, groupUuid);
        cv.put(DbColumns.MSG_VIEWED_PHONES, viewedPhones);
        cv.put(DbColumns.MSG_DELIVERED_PHONES, deliveredPhones);
        return cv;
    }

    public boolean isGroupChat() {
    	return !Utils.isStringNullOrEmpty(groupUuid);
    }
    
    public boolean isGroupChatStatus() {
    	return isGroupChat()
    			&& (mineType == ChatDatabase.MSG_MINETYPE_GROUP_INVITE
    			|| mineType == ChatDatabase.MSG_MINETYPE_GROUP_KICK);
    }

    public static class DateComparator implements Comparator<ChatMessageModel> {
        @Override
        public int compare(ChatMessageModel c1, ChatMessageModel c2) {
            long date1 = c1.dateReceiveMillis;
            long date2 = c2.dateReceiveMillis;
            long compare = date1 - date2;
            if (c1.type == ChatDatabase.MSG_TYPE_RECEIVE && c2.type == ChatDatabase.MSG_TYPE_RECEIVE
                    && Math.abs(compare) < 2000) {
                compare = c1.dateMillis - c2.dateMillis;
            }

            if (compare < 0) {
                return 1;
            } else if (compare > 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public boolean isStatusPreparing() {
        return status == ChatDatabase.MSG_STATUS_RECEIVE_PREPARING || status == ChatDatabase.MSG_STATUS_SEND_PREPARING;
    }

    public String getSnippet() {
        switch (mineType) {
            case ChatDatabase.MSG_MINETYPE_IMAGE:
                return MyApplication.getAppContext().getString(R.string.image_message);
            case ChatDatabase.MSG_MINETYPE_LOCATION:
                return MyApplication.getAppContext().getString(R.string.location_message);
            case ChatDatabase.MSG_MINETYPE_GROUP_INVITE:
        		return MyApplication.getAppContext().getString(R.string.has_contact_is_join);
            case ChatDatabase.MSG_MINETYPE_GROUP_KICK:
        		return MyApplication.getAppContext().getString(R.string.has_contact_is_kick);
            default:
                return textContent;
        }
    }

    public String getDescription() {
        switch (mineType) {
            case ChatDatabase.MSG_MINETYPE_IMAGE:
                try {
                    ImageWriteModel imageWrite = new ImageWriteModel(new JSONObject(textContent));
                    return imageWrite.description;
                } catch (JSONException e) {
                    return textContent;
                }
            case ChatDatabase.MSG_MINETYPE_LOCATION:
                try {
                    LocationChatModel location = new LocationChatModel(new JSONObject(textContent));
                    return location.description;
                } catch (JSONException e) {
                    return textContent;
                }
            case ChatDatabase.MSG_MINETYPE_MISSCALL:
                // Get number from content:
                return MyApplication.getAppResource().getString(R.string.missed_call_,
                        Utils.parseIntZeroBase(textContent.replaceAll("[^1234567890]", Const.STR_EMPTY)));
            case ChatDatabase.MSG_MINETYPE_GROUP_KICK:
            case ChatDatabase.MSG_MINETYPE_GROUP_INVITE:
            	String actionDescription;
            	if (mineType == ChatDatabase.MSG_MINETYPE_GROUP_KICK) {
            		actionDescription = MyApplication.STR_HAS_LEFT;
            	} else {
                	actionDescription = MyApplication.STR_HAS_JOIN;
            	}
            	String phoneNo = QilexPhoneNumberUtils.convertToBasicNumber(textContent);
            	
            	if (UserInfo.getInstance().getPhoneNumberFormatted().equals(phoneNo)) {
            		if (mineType == ChatDatabase.MSG_MINETYPE_GROUP_KICK) {
            			return MyApplication.STR_YOU_ARE_KICKED;
            		} else {
            			return MyApplication.STR_YOU_ARE_ADDED;
            		}
            		
            	} else {
                	String[] contactDatas = ContactManager.getInstance().getContactNameAndImageByPhoneNo(phoneNo);
                	String contactName;
                	if (contactDatas != null) {
                		contactName = contactDatas[0];
                	} else {
                		contactName = phoneNo;
                	}
                	return contactName + actionDescription;
            	}
            default:
                return textContent;
        }
    }
    
    public String getThreadUuid() {
    	if (!Utils.isStringNullOrEmpty(groupUuid)) {
    		return groupUuid;
    	} else {
    		return contactStr;
    	}
    }
}
