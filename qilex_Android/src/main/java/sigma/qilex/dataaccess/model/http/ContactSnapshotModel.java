
package sigma.qilex.dataaccess.model.http;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.utils.Utils;

public class ContactSnapshotModel {

    private static final String MSISDN_FORMAT = "msisdn_%d";

    public String avatar_uri;

    public String first_name;

    public String frifon_msisdn;

    public boolean frifon_user;

    public long id;

    public String last_name;

    public ArrayList<PhoneNumberModel> listPhoneNo = new ArrayList<>();

    public ContactSnapshotModel() {

    }

    public ContactSnapshotModel(JSONObject jsonData) throws JSONException {
        String avatarWithoutHost = jsonData.has("avatar_uri") ? jsonData.getString("avatar_uri") : Const.STR_EMPTY;
        if (avatarWithoutHost.startsWith("/") == true) {
            avatarWithoutHost = avatarWithoutHost.substring(1);
        }
        if (Utils.isStringNullOrEmpty(avatarWithoutHost) == false) {
            if (avatarWithoutHost.startsWith(UserInfoResponseModel.PREFIX_AVATARS) == false) {
                avatarWithoutHost = UserInfoResponseModel.PREFIX_AVATARS + "/" + avatarWithoutHost;
            }
            avatar_uri = Const.SERVER_URL + avatarWithoutHost;
        } else {
            avatar_uri = Const.STR_EMPTY;
        }

        String firstName = jsonData.has("first_name") ? jsonData.getString("first_name") : null;
        String lastName = jsonData.has("last_name") ? jsonData.getString("last_name") : null;
        frifon_msisdn = jsonData.has("frifon_msisdn") ? jsonData.getString("frifon_msisdn") : null;
        frifon_user = jsonData.has("frifon_user")
                ? (jsonData.isNull("frifon_user") ? false : jsonData.getBoolean("frifon_user")) : false;
        id = jsonData.has("id") ? jsonData.getLong("id") : -1;

        // Convert phone number
        int index = 0;
        do {
            String msisdn = String.format(MSISDN_FORMAT, index);
            if (jsonData.has(msisdn)) {
                JSONObject obj = jsonData.getJSONObject(msisdn);
                String phoneNo = obj.getString("msisdn");
                String name = obj.getString("name");
                if (phoneNo.length() > 0 && phoneNo.charAt(0) != '+') {
                    phoneNo = "+" + phoneNo;
                }
                PhoneNumberModel phoneModel = new PhoneNumberModel(phoneNo, name);
                listPhoneNo.add(phoneModel);
                index++;
            } else {
                index = -1;
            }
        } while (index > -1);
        first_name = firstName == null ? Const.STR_EMPTY : firstName;
        last_name = lastName == null ? Const.STR_EMPTY : lastName;
    }
}
