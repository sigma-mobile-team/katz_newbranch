
package sigma.qilex.dataaccess.model;

import android.content.ContentValues;
import android.database.Cursor;
import sigma.qilex.dataaccess.sqlitedb.ContactTempDatabase.ContactTempColumn;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class TempContactModel extends GlobalParcelable {

    public String phoneNumber;

    public String name;

    public String avatarUrl;

    public TempContactModel(String phoneNumber, String name, String avatarUrl) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.avatarUrl = avatarUrl;
        if (Utils.isStringNullOrEmpty(name)) {
            this.name = QilexPhoneNumberUtils.formatPhoneNoBlock3Number(phoneNumber);
        }
    }

    public TempContactModel(Cursor c) {
        phoneNumber = c.getString(c.getColumnIndex(ContactTempColumn.PHONE_NUMBER));
        name = c.getString(c.getColumnIndex(ContactTempColumn.NAME));
        avatarUrl = c.getString(c.getColumnIndex(ContactTempColumn.AVATAR_URL));
        if (Utils.isStringNullOrEmpty(name)) {
            name = QilexPhoneNumberUtils.formatPhoneNoBlock3Number(phoneNumber);
        }
    }

    public ContentValues parseToContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(ContactTempColumn.PHONE_NUMBER, phoneNumber);
        cv.put(ContactTempColumn.NAME, name);
        cv.put(ContactTempColumn.AVATAR_URL, avatarUrl);
        return cv;
    }
}
