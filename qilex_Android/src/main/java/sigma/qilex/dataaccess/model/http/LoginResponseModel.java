
package sigma.qilex.dataaccess.model.http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;
import sigma.qilex.dataaccess.model.GlobalParcelable;
import sigma.qilex.utils.Utils;

public class LoginResponseModel extends GlobalParcelable {

    public static final String KEY_MSG = "msg";

    public static final String KEY_CERT = "cert";

    public static final String KEY_REFRESH_PERIOD = "refresh_period";

    public static final String KEY_SIP_SERVERS = "sip_servers";

    public static final String KEY_TURN_SERVERS = "turn_servers";

    public static final String KEY_SIP_TOKEN = "sip_token";

    public static final String KEY_USER_AVATAR_URL = "user_avatar_uri";

    public static final String KEY_USER_LAST_NAME = "user_last_name";

    public static final String KEY_USER_NAME = "user_name";

    public static final String KEY_OUT_REG_ACCEPTED_VERSION = "out_reg_accepted_version";

    public static final String KEY_OUT_REG_CURRENT_VERSION = "out_reg_current_version";

    public static final String KEY_OUT_REG_URL = "out_reg_url";

    public static final String KEY_REG_ACCEPTED = "reg_accepted";

    public static final String KEY_REG_URL = "reg_url";

    public String msg;

    public String cert;

    public int refresh_period;

    public String[] sip_servers;

    public String[] turn_servers;

    public String sip_token;

    public String user_avatar_uri;

    public String user_last_name;

    public String user_name;

    public double out_reg_accepted_version;

    public double out_reg_current_version;

    public String out_reg_url;

    public boolean reg_accepted;

    public String reg_url;

    public boolean plus_promotion_calls;

    public LoginResponseModel() {

    }

    public LoginResponseModel(JSONObject jsonObject) throws JSONException {
        msg = jsonObject.getString(KEY_MSG);
        cert = jsonObject.getString(KEY_CERT);
        refresh_period = jsonObject.getInt(KEY_REFRESH_PERIOD); // TODO - use it
                                                                // as a value
                                                                // for
                                                                // ReLoginTasksTimer
                                                                // after a
                                                                // successful
                                                                // login.
                                                                // Instead of
                                                                // hardcoded 24h
        sip_token = jsonObject.getString(KEY_SIP_TOKEN);

        String avatarWithoutHost = jsonObject.getString(KEY_USER_AVATAR_URL);
        if (avatarWithoutHost.startsWith("/") == true) {
            avatarWithoutHost = avatarWithoutHost.substring(1);
        }
        if (Utils.isStringNullOrEmpty(avatarWithoutHost) == false) {
            if (avatarWithoutHost.startsWith(UserInfoResponseModel.PREFIX_AVATARS) == false) {
                avatarWithoutHost = UserInfoResponseModel.PREFIX_AVATARS + "/" + avatarWithoutHost;
            }
            user_avatar_uri = Const.SERVER_URL + avatarWithoutHost;
        } else {
            user_avatar_uri = Const.STR_EMPTY;
        }

        user_last_name = jsonObject.getString(KEY_USER_LAST_NAME);
        user_name = jsonObject.getString(KEY_USER_NAME);
        // Parse sip servers
        JSONArray sipServerArray = jsonObject.getJSONArray(KEY_SIP_SERVERS);
        sip_servers = new String[sipServerArray.length()];
//        sip_servers = new String[]{"10.230.73.4:15060"};
        for (int i = 0; i < sip_servers.length; i++) {
            sip_servers[i] = sipServerArray.get(i).toString();
        }

        // Parse turn servers
        JSONArray turnServerArray = jsonObject.getJSONArray(KEY_TURN_SERVERS);
        turn_servers = new String[turnServerArray.length()];
//        turn_servers = new String[2];
//        turn_servers[0] = "212.2.127.51:3478";
//        turn_servers[1] = "212.2.127.52:3478";
        for (int i = 0; i < turn_servers.length; i++) {
            turn_servers[i] = turnServerArray.get(i).toString();
        }

        // Regulation
        try {
            out_reg_accepted_version = jsonObject.getDouble(KEY_OUT_REG_ACCEPTED_VERSION);
        } catch (Exception ex) {
            out_reg_accepted_version = -1;
        }
        out_reg_current_version = jsonObject.getDouble(KEY_OUT_REG_CURRENT_VERSION);
        out_reg_url = jsonObject.getString(KEY_OUT_REG_URL);
        reg_accepted = jsonObject.getBoolean(KEY_REG_ACCEPTED);
        reg_url = jsonObject.getString(KEY_REG_URL);

        if (!Utils.isInKatzMode() && jsonObject.has("plus_promotion_calls")) {
            plus_promotion_calls = jsonObject.getBoolean("plus_promotion_calls");
        }
    }
}
