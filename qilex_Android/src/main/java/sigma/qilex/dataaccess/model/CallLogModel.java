
package sigma.qilex.dataaccess.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.sqlitedb.CallLogDb.CallLogColumn;

public class CallLogModel extends GlobalParcelable {

    public static final int TYPE_OUTGOING = 1;

    public static final int TYPE_INCOMING = 2;

    public static final int TYPE_MISSCALL = 3;

    public static final int TYPE_GLOBAL = 4;

    public static final int STR_OUTGOING_SRC = R.string.calllog_type_out;

    public static final int STR_INCOMING_SRC = R.string.calllog_type_in;

    public static final int STR_MISSCALL_SRC = R.string.calllog_type_miss;

    public static final int STR_GLOBAL_SRC = R.string.calllog_type_global;

    protected long mDbId;

    /**
     * Type of Item, one of the constants ITEM
     */
    protected int mType;

    /**
     * for SEPARATOR'ow is a string with a date or "Today" or "Yesterday", for
     * others it is a dialed number / caller
     */
    protected String mLabel;

    /**
     * Date represented by item, not applicable separators, dateMillis convert
     * to long (milliseconds) from date
     */
    protected long mDateMilliSecond;

    /**
     * call duration in seconds, or 0 if not applicable (SEPARATOR or MISSED)
     */
    protected int mDurationMillis = 0;

    public CallLogModel() {
        super();
    }

    /** constructor */
    public CallLogModel(long dbId, int type, String label, long dateSecond, int duration) {
        mDbId = dbId;
        mType = type;
        mLabel = label;
        mDateMilliSecond = dateSecond;
        mDurationMillis = duration;
    }

    public CallLogModel(CallLogModel model) {
        if (model != null) {
            mDbId = model.mDbId;
            mType = model.mType;
            mLabel = model.mLabel;
            mDateMilliSecond = model.mDateMilliSecond;
            mDurationMillis = model.mDurationMillis;
        }
    }

    // ** constructor using cursor */
    public CallLogModel(Cursor c) {
        this.mDbId = c.getLong(c.getColumnIndex(CallLogColumn.ID));
        this.mType = c.getInt(c.getColumnIndex(CallLogColumn.TYPE));
        this.mLabel = c.getString(c.getColumnIndex(CallLogColumn.LABEL));
        this.mDateMilliSecond = Long.parseLong(c.getString(c.getColumnIndex(CallLogColumn.LOG_DATE)));
        this.mDurationMillis = c.getInt(c.getColumnIndex(CallLogColumn.DURATION));
    }

    // ** constructor using parcel */
    public CallLogModel(Parcel in) {
        super(in);
    }

    public long getDbId() {
        return mDbId;
    }

    public void setDbId(long dbId) {
        this.mDbId = dbId;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        this.mLabel = label;
    }

    public long getDateMilliSecond() {
        return mDateMilliSecond;
    }

    public void setDateMilliSecond(long dateMillis) {
        this.mDateMilliSecond = dateMillis;
    }

    public int getDuration() {
        return mDurationMillis;
    }

    public void setDuration(int durationMillis) {
        this.mDurationMillis = durationMillis;
    }

    public ContentValues parseToContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(CallLogColumn.LABEL, mLabel);
        cv.put(CallLogColumn.TYPE, mType);
        cv.put(CallLogColumn.LOG_DATE, mDateMilliSecond);
        cv.put(CallLogColumn.DURATION, mDurationMillis);
        return cv;
    }

    public int getStringTypeSrc() {
        switch (mType) {
            case TYPE_OUTGOING:
                return STR_OUTGOING_SRC;
            case TYPE_INCOMING:
                return STR_INCOMING_SRC;
            case TYPE_MISSCALL:
                return STR_MISSCALL_SRC;
            case TYPE_GLOBAL:
                return STR_GLOBAL_SRC;
            default:
                return STR_OUTGOING_SRC;
        }
    }

    public int getIconTypeSrc() {
        switch (mType) {
            case TYPE_OUTGOING:
                return R.drawable.icon_call_outgoing;
            case TYPE_INCOMING:
                return R.drawable.icon_call_incoming;
            case TYPE_MISSCALL:
                return R.drawable.icon_missed;// R.drawable.icon_call_miss;
            case TYPE_GLOBAL:
                return R.drawable.icon_global;// R.drawable.icon_call_kout;
            default:
                return R.drawable.icon_call_incoming;// R.drawable.icon_in;
        }
    }
}
