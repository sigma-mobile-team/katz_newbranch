
package sigma.qilex.dataaccess.model.http;

import org.json.JSONException;
import org.json.JSONObject;

public class GooglePlayProductListModel {

    public double product_value;

    public String product_image_uri;

    public String product_id;

    public String product_description;

    public String product_google_id;

    public String product_name;

    public GooglePlayProductListModel() {
    }

    public GooglePlayProductListModel(JSONObject object) throws JSONException {
        product_value = object.getDouble("product_value");
        product_image_uri = object.getString("product_image_uri");
        product_id = object.getString("product_id");
        product_description = object.getString("product_description");
        product_google_id = object.getString("product_google_id");
        product_name = object.getString("product_name");
    }
}
