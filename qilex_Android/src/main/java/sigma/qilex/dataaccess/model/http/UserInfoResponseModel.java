
package sigma.qilex.dataaccess.model.http;

import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.Const;
import sigma.qilex.utils.Utils;

public class UserInfoResponseModel {

    public static final String PREFIX_AVATARS = "avatars/";
    
    public static final String KEY_USER_NAME = "user_name";

    public static final String KEY_USER_LAST_NAME = "user_last_name";

    public static final String KEY_USER_AVATAR_URI = "user_avatar_uri";

    public String user_name;

    public String user_last_name;

    public String user_avatar_uri;

    public UserInfoResponseModel() {

    }

    public UserInfoResponseModel(JSONObject jsonObject) throws JSONException {
        user_name = jsonObject.getString(KEY_USER_NAME);
        user_last_name = jsonObject.getString(KEY_USER_LAST_NAME);

        String avatarWithoutHost = jsonObject.getString(KEY_USER_AVATAR_URI);
        if (avatarWithoutHost.startsWith("/") == true) {
            avatarWithoutHost = avatarWithoutHost.substring(1);
        }
        if (Utils.isStringNullOrEmpty(avatarWithoutHost) == false) {
            if (avatarWithoutHost.startsWith(PREFIX_AVATARS) == false) {
                avatarWithoutHost = PREFIX_AVATARS + "/" + avatarWithoutHost;
            }
            user_avatar_uri = Const.SERVER_URL + avatarWithoutHost;
        } else {
            user_avatar_uri = Const.STR_EMPTY;
        }
    }
}
