
package sigma.qilex.dataaccess.model.http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PurchasePrepaidModel {

    public static final String KEY_DATE = "date";

    public static final String KEY_OPERATION_ID = "operation_id";

    public static final String KEY_AMOUNT = "amount";

    public String msisdn;

    public int on_page;

    public int page;

    public PurchaseModel[] prepaids = null;

    public PurchasePrepaidModel() {
    }

    public PurchasePrepaidModel(JSONObject jsonData) throws JSONException {
        msisdn = jsonData.getString("msisdn");
        on_page = jsonData.getInt("on_page");
        page = jsonData.getInt("page");
        JSONArray jsonPrepaid = jsonData.getJSONArray("prepaids");
        if (jsonPrepaid != null) {
            int size = jsonPrepaid.length();
            prepaids = new PurchaseModel[size];
            for (int i = 0; i < size; i++) {
                prepaids[i] = new PurchaseModel(jsonPrepaid.getJSONObject(i));
            }
        }
    }

    public static class PurchaseModel {

        public String date;

        public String operation_id;

        public double amount;

        public PurchaseModel() {

        }

        public PurchaseModel(JSONObject jsonObject) throws JSONException {
            date = jsonObject.getString(KEY_DATE);
            operation_id = jsonObject.getString(KEY_OPERATION_ID);
            amount = jsonObject.getDouble(KEY_AMOUNT);
        }

        public JSONObject toJsonObject() {
            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put(KEY_DATE, date);
                jsonObj.put(KEY_OPERATION_ID, operation_id);
                jsonObj.put(KEY_AMOUNT, amount);
            } catch (JSONException e) {
            }
            return jsonObj;
        }
    }
}
