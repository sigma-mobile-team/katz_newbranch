
package sigma.qilex.dataaccess.model.http;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;

/**
 * Class represents error from http response.
 */
public class ErrorModel {

    public static final int CODE_CONTACT_DEFAULT = -1;

    public static final int CODE_CONTACT_NOT_EXIST = 131;

    public static final int CODE_NO_USER = 128;

    public static final int CODE_INVALID_ACTIVE_USER = 108;

    public static final int CODE_INCORRECT_TOKEN = 125;

    public static final int CODE_MISI_CHANGE = 110;

    public HttpError[] errors = null;

    /**
     * Create default error with code = -1 and other content is blank.
     */
    public ErrorModel() {
        errors = new HttpError[1];
        errors[0] = new HttpError();
    }

    public ErrorModel(JSONObject jsonData) {
        try {
            JSONArray jsonErros = jsonData.getJSONArray("errors");
            int size = jsonErros.length();
            errors = new HttpError[size];
            for (int i = 0; i < size; i++) {
                errors[i] = new HttpError(jsonErros.getJSONObject(i));
            }
        } catch (JSONException e) {
            errors = new HttpError[1];
            errors[0] = new HttpError();
        }
    }

    public ErrorModel(String message) {
        try {
            JSONObject jsonData = new JSONObject(message);
            JSONArray jsonErros = jsonData.getJSONArray("errors");
            int size = jsonErros.length();
            errors = new HttpError[size];
            for (int i = 0; i < size; i++) {
                errors[i] = new HttpError(jsonErros.getJSONObject(i));
            }
        } catch (JSONException e) {
            errors = new HttpError[1];
            errors[0] = new HttpError();
        }
    }

    public HttpError getByErrorCode(int code) {
        for (HttpError httpError : errors) {
            if (httpError.code == code) {
                return httpError;
            }
        }
        return null;
    }

    public class HttpError {
        public int code;

        public String dmsg;

        public JSONObject umsg;

        public HashMap<String, String> umsgMap = new HashMap<>();;

        public HttpError() {
            code = CODE_CONTACT_DEFAULT;
            dmsg = MyApplication.getAppContext().getString(R.string.connection_error);
            umsg = null;
        }

        public HttpError(JSONObject jsonData) {
            initByJson(jsonData);
        }

        public HttpError(String message) {
            try {
                JSONObject jsonData = new JSONObject(message);
                initByJson(jsonData);
            } catch (JSONException e) {
                code = -1;
                dmsg = MyApplication.getAppContext().getString(R.string.connection_error);
                umsg = null;
            }
        }

        private void initByJson(JSONObject jsonData) {
            try {
                code = jsonData.has("code") ? jsonData.getInt("code") : -1;
                dmsg = jsonData.has("dmsg") ? jsonData.getString("dmsg")
                        : MyApplication.getAppContext().getString(R.string.connection_error);
                umsg = jsonData.has("umsg") ? jsonData.getJSONObject("umsg") : null;
                if (umsg != null) {
                    Iterator<String> listKeys = umsg.keys();
                    if (listKeys.hasNext()) {
                        String key = listKeys.next();
                        umsgMap.put(key, umsg.getString(key));
                    }
                }
            } catch (JSONException e) {
                code = -1;
                dmsg = MyApplication.getAppContext().getString(R.string.connection_error);
                umsg = null;
            }
        }

        public String getCurrentErrorMessage() {
            if (umsgMap.containsKey(UserInfo.getInstance().getAcceptedLanguage())) {
                return umsgMap.get(UserInfo.getInstance().getAcceptedLanguage());
            } else {
                return dmsg;
            }
        }
    }
}
