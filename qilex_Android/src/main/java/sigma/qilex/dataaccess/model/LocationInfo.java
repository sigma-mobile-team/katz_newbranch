
package sigma.qilex.dataaccess.model;

import com.google.android.gms.maps.model.LatLng;

import android.os.Parcel;

public class LocationInfo extends GlobalParcelable {

    public double latitude;

    public double longitude;

    public String address;

    public LocationInfo() {
        super();
    }

    public LocationInfo(LatLng lat, String addr) {
        this(lat.latitude, lat.longitude, addr);
    }

    public LocationInfo(double lat, double lng, String addr) {
        super();
        latitude = lat;
        longitude = lng;
        address = addr;
    }

    public LocationInfo(Parcel in) {
        super(in);
    }
}
