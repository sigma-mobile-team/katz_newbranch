
package sigma.qilex.dataaccess.model;

import java.util.ArrayList;
import java.util.Comparator;

import pl.katz.aero2.Const;
import pl.katz.aero2.UserInfo;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase.DbColumns;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

/**
 * Class represent Sqlite data of Table THREADS.
 */
public class ChatThreadModel extends GlobalParcelable {

    public long id;

    /**
     * In single chat, contactStr is peerPhoneNumber.
     * In group chat, it is groupUuid
     */
    public String contactStr;

    public int type;

    public long dateMillis;

    public String snippet;

    public int unreadCount;

    public int mimetype;
    
    /**
     * In single chat, groupData is empty
     * In group chat, it is data of participant and administrator.
     * Data have format in 2 lines:  [Participants]\br[Administrator]
     */
    public String groupData;

    /**
     * In single chat, snippetOwner is peerPhoneNumber
     * In group chat, it is name of latest sent name.
     */
    public String snippetOwner;

    public ChatThreadModel() {
        super();
    }

    public ChatThreadModel(Parcel in) {
        super(in);
    }

    public ChatThreadModel(long id, String contactStr, int type, long dateMillis, String snippet, int unreadCount) {
        this.id = id;
        this.contactStr = contactStr;
        this.type = type;
        this.dateMillis = dateMillis;
        this.snippet = snippet;
        this.unreadCount = unreadCount;
        this.mimetype = ChatDatabase.MSG_MINETYPE_UNDENFINDED;
    }

    public ChatThreadModel(Cursor c) {
        id = c.getLong(c.getColumnIndex(DbColumns.THREADS_ID));
        contactStr = c.getString(c.getColumnIndex(DbColumns.THREADS_CONTACT_STR));
        type = c.getInt(c.getColumnIndex(DbColumns.THREADS_TYPE));
        dateMillis = c.getLong(c.getColumnIndex(DbColumns.THREADS_DATE_MILLIS));
        snippet = c.getString(c.getColumnIndex(DbColumns.THREADS_SNIPPET));
        unreadCount = c.getInt(c.getColumnIndex(DbColumns.THREADS_UNREAD_COUNT));

        int mimetypeIndex = c.getColumnIndex(DbColumns.THREADS_MIME_TYPE);
        mimetype = mimetypeIndex > -1 ? c.getInt(mimetypeIndex) : ChatDatabase.MSG_MINETYPE_TEXT;

        int groupDataIndex = c.getColumnIndex(DbColumns.THREADS_GROUP_DATA);
        groupData = groupDataIndex > -1 ? c.getString(groupDataIndex) : Const.STR_EMPTY;

        int snippetOwnerIndex = c.getColumnIndex(DbColumns.THREADS_SNIPPET_OWNER);
        snippetOwner = snippetOwnerIndex > -1 ? c.getString(snippetOwnerIndex) : Const.STR_EMPTY;
    }

    public ContentValues parseToContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(DbColumns.THREADS_CONTACT_STR, contactStr);
        cv.put(DbColumns.THREADS_TYPE, type);
        cv.put(DbColumns.THREADS_DATE_MILLIS, dateMillis);
        cv.put(DbColumns.THREADS_SNIPPET, snippet);
        cv.put(DbColumns.THREADS_UNREAD_COUNT, unreadCount);
        cv.put(DbColumns.THREADS_MIME_TYPE, mimetype);
        cv.put(DbColumns.THREADS_GROUP_DATA, groupData);
        cv.put(DbColumns.THREADS_SNIPPET_OWNER, snippetOwner);
        return cv;
    }

    public boolean isGroupChat() {
    	return !Utils.isStringNullOrEmpty(groupData);
    }

    public String[] getListPhoneNumberParticipants() {
    	if (Utils.isStringNullOrEmpty(groupData)) {
    		return null;
    	}
    	String [] arrayGroupData = groupData.split("\n");
    	if (arrayGroupData.length < 2) {
    		return null;
    	}
    	String []arrayParticipantUri = arrayGroupData[0].split(Const.STR_COMMA);
    	String []output = new String[arrayParticipantUri.length];
    	for (int i = 0; i < arrayParticipantUri.length; i++) {
    		output[i] = QilexPhoneNumberUtils.convertToBasicNumber(arrayParticipantUri[i]);
    	}
    	return output;
    }
    
    public boolean isYouInGroupChat() {
    	String [] arrayGroupData = groupData.split("\n");
    	if (arrayGroupData.length < 2) {
    		return false;
    	}
    	return arrayGroupData[0].contains(UserInfo.getInstance().getPhoneNumberFormatted());
    }
    
    public String[] getListPhoneNumberParticipantExceptYou() {
    	if (Utils.isStringNullOrEmpty(groupData)) {
    		return null;
    	}
    	String [] arrayGroupData = groupData.split("\n");
    	if (arrayGroupData.length < 2) {
    		return null;
    	}
    	String []arrayParticipantUri = arrayGroupData[0].split(Const.STR_COMMA);
    	ArrayList<String> outList = new ArrayList<>();
    	for (int i = 0; i < arrayParticipantUri.length; i++) {
    		String phone = QilexPhoneNumberUtils.convertToBasicNumber(arrayParticipantUri[i]);
    		if (!UserInfo.getInstance().getPhoneNumberFormatted().equals(phone)) {
    			outList.add(phone);
    		}
    	}
    	String []output = new String[outList.size()];
    	return outList.toArray(output);
    }
    
    public String getPhoneNumberAdmin() {
    	if (Utils.isStringNullOrEmpty(groupData)) {
    		return null;
    	}
    	String [] arrayGroupData = groupData.split("\n");
    	if (arrayGroupData.length < 2) {
    		return null;
    	}
    	return QilexPhoneNumberUtils.convertToBasicNumber(arrayGroupData[1]);
    }

    public static class DateComparator implements Comparator<ChatThreadModel> {
        @Override
        public int compare(ChatThreadModel c1, ChatThreadModel c2) {
            long date1 = c1.dateMillis;
            long date2 = c2.dateMillis;
            long compare = date1 - date2;
            if (compare < 0) {
                return 1;
            } else if (compare > 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
