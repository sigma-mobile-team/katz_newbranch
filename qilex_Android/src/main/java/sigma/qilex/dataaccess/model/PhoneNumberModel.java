
package sigma.qilex.dataaccess.model;

import android.os.Parcel;
import pl.katz.aero2.Const;
import sigma.qilex.utils.QilexPhoneNumberUtils;

public class PhoneNumberModel extends GlobalParcelable {

    public static final String PHONE_SPLIT = ";";

    public String phoneNumber = Const.STR_EMPTY;

    public String type = Const.STR_EMPTY;

    public String phoneOriginal = Const.STR_EMPTY;

    public PhoneNumberModel(String number, String type) {
        super();
        this.phoneNumber = QilexPhoneNumberUtils.convertToBasicNumber(number);
        this.phoneOriginal = number;
        this.type = type;
    }

    public PhoneNumberModel(Parcel in) {
        super(in);
    }

    public PhoneNumberModel(String format) {
        super();
        String[] split = format.split(PHONE_SPLIT);
        phoneNumber = QilexPhoneNumberUtils.convertToBasicNumber(split[0]);
        if (split.length > 1) {
            type = split[1];
        }
    }

    public String toString() {
        return phoneNumber + PHONE_SPLIT + type;
    }
}
