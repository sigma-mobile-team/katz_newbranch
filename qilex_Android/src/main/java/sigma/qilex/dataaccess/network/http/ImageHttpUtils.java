
package sigma.qilex.dataaccess.network.http;

import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import pl.katz.aero2.Const;
import pl.katz.aero2.Const.QilexRequestHeader;
import pl.katz.aero2.UserInfo;
import sigma.qilex.manager.chat.ImageThumbnailProcessor;

public class ImageHttpUtils extends UserActivationUtils {

    /**
     * Make request put image to server
     * 
     * @param context
     * @param phonenumber user's phone number
     * @param imageBase64 base64 Image data
     * @param imageSize image size [width]x[height]
     * @param imageFormat jpg
     * @param thumbBase64
     * @param thumbSize
     * @param thumbFormat
     * @param listener
     * @return
     */
    public static QilexHttpRequest postImageAndThumbnail(Context context, String phonenumber, Bitmap image,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            byte[] imageByte = ImageThumbnailProcessor.getBytes(image);
            // Create request
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "img/img_write/" + phonenumber), true,
                    imageByte);

            // Prepare headers
            qHttpRequest.setHeader("Accept", QilexRequestHeader.ACCEPT);
            qHttpRequest.setHeader("Accept-Language", UserInfo.getInstance().getAcceptedLanguage());
            qHttpRequest.setHeader("Accept-Encoding", QilexRequestHeader.ACCEPT_ENCODING);
            qHttpRequest.setHeader("Content-Length", "" + imageByte.length);
            qHttpRequest.setHeader("Content-Type", "image/jpg");
            qHttpRequest.setHeader("User-Agent", "katz_android/1");

            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public static QilexHttpRequest getImageAndThumbnail(Context context, String uri, String token,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "img/" + uri);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);

            // Header
            qHttpRequest.setHeader("Accept", QilexRequestHeader.ACCEPT);
            qHttpRequest.setHeader("Accept-Language", UserInfo.getInstance().getAcceptedLanguage());
            qHttpRequest.setHeader("Accept-Encoding", QilexRequestHeader.ACCEPT_ENCODING);
            qHttpRequest.setHeader("Authorization", token);
            qHttpRequest.execute(listener);

            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
