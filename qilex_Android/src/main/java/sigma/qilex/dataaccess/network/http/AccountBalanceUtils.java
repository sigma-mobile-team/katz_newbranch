
package sigma.qilex.dataaccess.network.http;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import pl.katz.aero2.Const;
import sigma.qilex.utils.Utils;

public class AccountBalanceUtils extends UserActivationUtils {

    public static void getAccountBalance(String phoneNo, QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "account_balance/" + phoneNo);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNo, Const.STR_EMPTY);
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void getAccountBurdenHistory(String phoneNo, int page, int onPage,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "account_burdens_history/" + phoneNo);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNo, Const.STR_EMPTY);
            qHttpRequest.setParam("page", Integer.toString(page));
            qHttpRequest.setParam("on_page", Integer.toString(onPage));
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static QilexHttpRequest getAccountPrepaidHistory(String phoneNo, int page, int onPage,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "account_prepaid_history/" + phoneNo);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNo, Const.STR_EMPTY);
            qHttpRequest.setParam("page", Integer.toString(page));
            qHttpRequest.setParam("on_page", Integer.toString(onPage));
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void postAccountPaymentInfo(String number, String googleInfo, String appstoreInfo, String userData,
            String googleProductId, String appStoreProductId, QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyAccountPaymentInfo(googleInfo, appstoreInfo, userData, googleProductId,
                    appStoreProductId);
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "account_payment_info/" + number), true,
                    body);
            prepareHttpRequestHeader(qHttpRequest, number, body);

            // Call execute
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static String makeBodyAccountPaymentInfo(String googleInfo, String appstoreInfo, String userData,
            String googleProductId, String appStoreProductId) {
        Map<String, String> unsortMap = new HashMap<String, String>();
        unsortMap.put("googleplay_info", googleInfo);
        unsortMap.put("appstore_info", appstoreInfo);
        unsortMap.put("user_data", userData);
        unsortMap.put("googleplay_product_id", googleProductId);
        unsortMap.put("appstore_product_id", appStoreProductId);
        return Utils.convertMapToJsonString(unsortMap);
    }

    public static QilexHttpRequest getGooglePlayProductList(QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "googleplay_product_list");
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, Const.STR_EMPTY, Const.STR_EMPTY);
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public static QilexHttpRequest getOutCallHistory(String phoneNo, int page, int onPage,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "outcall_history/" + phoneNo);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNo, Const.STR_EMPTY);
            qHttpRequest.setParam("page", Integer.toString(page));
            qHttpRequest.setParam("on_page", Integer.toString(onPage));
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            return null;
        }
    }
}
