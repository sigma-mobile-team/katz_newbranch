
package sigma.qilex.dataaccess.network.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import sigma.qilex.utils.LogUtils;

public abstract class QilexHttpRequest {
    public static final String DBG_TAG = "HttpRequest";

    public static final String HEAD_USER_AGENT = "User-Agent";

    /**
     * After the establishment of the state, you can set the parameters and call
     * execute()
     */
    public static final int STATE_READY = 0;

    /**
     * Continues execution of the request - send, in this state, you can not
     * change the parameters.
     */
    public static final int STATE_SENDING_REQUEST = 1;

    /**
     * In this state, is already open stream of responses and response code is
     * known.
     */
    public static final int STATE_GETTING_REPONSE = 2;

    /**
     * Your request has been completes, you can use getResponseCode() and
     * getResponse().
     */
    public static final int STATE_COMPETED = 3;

    public static final int STATE_CANCELED = 4;

    /**
     * Has error during the request is processing.
     */
    public static final int STATE_ERROR = -1;

    /**
     * HttpRequest Timeout.
     */
    public static final int STATE_ERROR_TIMEOUT = -2;

    /**
     * Error date in device's system.
     */
    public static final int STATE_ERROR_DATE = -3;

    private static int mLastIndex = 0;

    private static Hashtable<Integer, QilexHttpRequest> mListHttpRequest = new Hashtable<Integer, QilexHttpRequest>();

    /**
     * Current state of httpRequest object.
     */
    protected int mState = STATE_READY;

    /**
     * The api url.
     */
    protected URL mURL;

    private boolean mUsesClientCert = false;

    /**
     * The request method, can be GET or POST
     */
    protected String mRequestMethod;

    protected boolean mDoOutput;

    /**
     * Default connection timeout.
     */
    protected int mTimeout = 5000;

    /**
     * Handler event when httpRequest change state.
     */
    protected QilexHttpRequestListener mListener;

    /**
     * List of request Parameters.
     */
    protected Hashtable<String, String> mReqParams = new Hashtable<String, String>();

    /**
     * List of request Headers.
     */
    protected Hashtable<String, String> mReqHeaders = new Hashtable<String, String>();

    /**
     * Response code.
     */
    protected int mResponseCode = -1;

    /**
     * Response data.
     */
    protected byte[] mResponseData = null;

    public boolean MultipleSameParamsFix = false;

    protected boolean mExecuted = false;

    protected int mRequestId;

    protected boolean isTextResponse = true;

//    protected AsyncTask<String, Integer, Boolean> mCurrentAsyncTask;
    protected Thread mCurrentAsyncTask;
    
    boolean isCanceled;

    protected QilexHttpRequest(URL url, boolean isCert) {
        mURL = url;
        this.mUsesClientCert = isCert;

        mRequestId = mLastIndex++;
        mListHttpRequest.put(mRequestId, this);
//        mCurrentAsyncTask = new AsyncTask<String, Integer, Boolean>() {
//            @Override
//            protected Boolean doInBackground(String... params) {
//                run();
//                return null;
//            }
//        };
        mCurrentAsyncTask = new Thread(new Runnable() {
            @Override
            public void run() {
                executeSynch(mListener);
            }
        });
    }

    /**
     * Interface that handler httpRequest action.
     */
    public interface QilexHttpRequestListener {
        /**
         * Callback method.
         * 
         * @param req
         */
        public void httpRequestStateChanged(QilexHttpRequest req);
    }

    @SuppressWarnings("rawtypes")
    protected abstract void setParams(HttpURLConnection urlConn, Hashtable params);

    @SuppressWarnings("rawtypes")
    protected abstract void setParamsFix(HttpURLConnection urlConn, Hashtable params);

    private void executeSynch(QilexHttpRequestListener listener) {

        if (mState != STATE_READY)
            throw new IllegalStateException(
                    "HttpsPostRequest.executeSynch(..) wywołane w stanie innym niż STATE_READY");
        try {
            HttpURLConnection urlConn = null;
            System.setProperty("http.keepAlive", "false"); // #94022
                                                           // http://stackoverflow.com/questions/10242942/switching-from-wifi-connection-to-3g-causes-connection-to-hang
            mURL = configureURL();
            if (mUsesClientCert) {
                // inicjalizacja urlConn z użyciem certyfikatu klienta
                setVerifierAndTrustManager(); // ustawienie weryfikacji i
                                              // manager'a zaufanych CA
                urlConn = (HttpsURLConnection)mURL.openConnection();
            } else {
                // normalny init urlConn
                urlConn = (HttpURLConnection)mURL.openConnection();
            }
            urlConn.setRequestMethod(mRequestMethod);
            urlConn.setDoInput(true);
            urlConn.setDoOutput(mDoOutput);
            urlConn.setConnectTimeout(mTimeout);
            // urlConn.setRequestProperty("Content-Type", "text/plain");

            // wpisanie nagłówków
            Enumeration<String> headerKeys = mReqHeaders.keys();
            while (headerKeys.hasMoreElements()) {
                String key = headerKeys.nextElement();
                String value = mReqHeaders.get(key);
                urlConn.setRequestProperty(key, value);
            }

            // mistrzostwo świata !!!!!
            if (MultipleSameParamsFix == true) {
                setParamsFix(urlConn, mReqParams);
                MultipleSameParamsFix = false;
            } else {
                setParams(urlConn, mReqParams);
            }
            byte[] outputBytes = null;
            if (mBody != null) {
                outputBytes = mBody.getBytes("UTF-8");
            } else if (mBodyBytes != null) {
                outputBytes = mBodyBytes;
            }
            if (outputBytes != null) {
                urlConn.setRequestProperty("Content-Length", Integer.toString(outputBytes.length));
                OutputStream os;
                try {
                    os = urlConn.getOutputStream();
                    os.write(outputBytes);
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            // Set parameter
            mState = STATE_SENDING_REQUEST;
            if (isCanceled) {
                mState = STATE_CANCELED;
                return;
            }
            mListener.httpRequestStateChanged(this);
            LogUtils.d(DBG_TAG, "connecting " + urlConn.getURL());
            try {
                urlConn.connect();
            } catch (Throwable tt) {
                LogUtils.e(DBG_TAG, "HttpsPostRequest.connecting urlConn.connect() exception", tt);
                mState = STATE_ERROR_TIMEOUT;
                mListener.httpRequestStateChanged(this);
                return;
            }

            boolean responseInErrorStream = true;
            InputStream is = null;
            // odczyt odpowiedzi
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                mResponseCode = urlConn.getResponseCode();
                is = urlConn.getInputStream();
                responseInErrorStream = false;
                mState = STATE_GETTING_REPONSE;
                if (isCanceled) {
                    mState = STATE_CANCELED;
                    return;
                }
                mListener.httpRequestStateChanged(this);
                byte[] buffer = new byte[1024];
                int r = is.read(buffer, 0, buffer.length);
                while (r >= 0) {
                    baos.write(buffer, 0, r);
                    r = is.read(buffer, 0, buffer.length);
                }
                is.close();
                baos.close();
            } catch (Throwable t) {
                if (responseInErrorStream) {
                    try {
                        is = urlConn.getErrorStream();
                        mState = STATE_GETTING_REPONSE;
                        if (isCanceled) {
                            mState = STATE_CANCELED;
                            return;
                        }
                        mListener.httpRequestStateChanged(this);
                        byte[] buffer = new byte[1024];
                        if (is != null) {
                            int r = is.read(buffer, 0, buffer.length);
                            while (r >= 0) {
                                baos.write(buffer, 0, r);
                                r = is.read(buffer, 0, buffer.length);
                            }
                            is.close();
                        } else {
                            mState = STATE_ERROR;
                        }
                        baos.close();
                    } catch (Throwable tt) {
                        mState = STATE_ERROR;
                        tt.printStackTrace();
                        LogUtils.e(DBG_TAG, "HttpsPostRequest.executeSynch error getting error response:", tt);
                    }
                } else {
                    LogUtils.e(DBG_TAG, "HttpsPostRequest.executeSynch error getting response:", t);
                    mState = STATE_ERROR;
                    LogUtils.e(DBG_TAG, "HttpsPostRequest.executeSynch error:", t);
                }
            } finally {
                try {
                    is.close();
                } catch (Throwable t) {
                }
            }

            urlConn.disconnect();

            mResponseData = baos.toByteArray();
            if (mResponseData != null || isTextResponse) {
                String message = new String(mResponseData);
                LogUtils.d("Http Response", "Http Response: " + mURL + "\n" + message);
            }
            if (mState != STATE_ERROR && mState != STATE_CANCELED) {
                mState = STATE_COMPETED;
            }

            try {
                if (isCanceled) {
                    mState = STATE_CANCELED;
                    return;
                }
                mListener.httpRequestStateChanged(this);
            } catch (Throwable t) {
                LogUtils.e(DBG_TAG, "HttpsPostRequest.executeSynch STATE_COMPETED", t);
            }

        } catch (SocketTimeoutException t) {
            mState = STATE_ERROR_TIMEOUT;
            LogUtils.e(DBG_TAG, "HttpsPostRequest.executeSynch timeout:", t);
        } catch (Throwable t) {
            mState = STATE_ERROR;
            LogUtils.e(DBG_TAG, "HttpsPostRequest.executeSynch error:", t);
        } finally {
            mListHttpRequest.remove(mRequestId);
        }
    }

    @SuppressLint("TrulyRandom")
    private void setVerifierAndTrustManager() {
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[] {};
                    }

                    public void checkClientTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] chain, String authType)
                            throws CertificateException {
                    }
                }
        };
        try {

            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }

            });
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
    }

    /**
     * Start send request to server.
     * 
     * @param listener the callback method handler event state change.
     * @return true if request is sent, false if not.
     */
    public final synchronized boolean execute(QilexHttpRequestListener listener) {
        if (mExecuted) {
            return false;
        }
        setListener(listener);
        mCurrentAsyncTask.start();
//        mCurrentAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return true;
    }

    protected abstract URL configureURL();

    /**
     * Method start execute.
     */
    public final void run() {
        try {
            executeSynch(mListener);
        } catch (Throwable t) {
            LogUtils.e(DBG_TAG, "HttpRequest.run() error", t);
        }
    }

    public final void cancel() {
        isCanceled = true;
//        if (mCurrentAsyncTask != null) {
//            mCurrentAsyncTask.cancel(true);
//        }
    }

    /**
     * Set callback event.
     * 
     * @param mListener
     */
    public void setListener(QilexHttpRequestListener mListener) {
        this.mListener = mListener;
    }

    /**
     * Add request parameter to list of parameters.
     * 
     * @param key parameter's name.
     * @param value parameter's value.
     * @return true if parameter is added, false if not.
     */
    public boolean setParam(String key, String value) {
        if (mExecuted) {
            return false;
        }
        if (value == null) {
            clearParam(key);
            return true;
        }
        mReqParams.put(key, value);
        return true;
    }

    protected String mBody;

    protected byte[] mBodyBytes;

    protected Bitmap mBodyBitmap;

    void setBody(String body) {
        this.mBody = body;
    }

    void setBody(byte[] bodyBytes) {
        this.mBodyBytes = bodyBytes;
    }

    void setBody(Bitmap bitmap) {
        this.mBodyBitmap = bitmap;
    }

    /**
     * Clear list parameter
     * 
     * @param key parameter's key.
     * @return true if parameter is removed.
     */
    public boolean clearParam(String key) {
        if (mExecuted) {
            return false;
        }
        return (mReqParams.remove(key) != null);
    }

    /**
     * Add repquest header to list of header.
     * 
     * @param key header's key.
     * @param value header's value.
     * @return true if header is added, false if not.
     */
    public boolean setHeader(String key, String value) {
        if (mExecuted)
            return false;
        if (value == null) {
            clearHeader(key);
            return true;
        }
        mReqHeaders.put(key, value);
        return true;
    }

    public void setTextResponse(boolean isTextResponse) {
        this.isTextResponse = isTextResponse;
    }

    /**
     * Clear header parameter.
     * 
     * @param key header's key.
     * @return true if a header is removed.
     */
    public boolean clearHeader(String key) {
        if (mExecuted)
            return false;
        return (mReqHeaders.remove(key) != null);
    }

    /**
     * Get HttpRequest's current state.
     * 
     * @return
     */
    public int getState() {
        return mState;
    }

    public int getResponseCode() {
        return mResponseCode;
    }

    public byte[] getResponseData() {
        return mResponseData;
    }

    public int getRequestId() {
        return mRequestId;
    }

    public static synchronized void stopRequest(int requestId) {
        QilexHttpRequest httpRequest = mListHttpRequest.remove(requestId);
        if (httpRequest != null) {
            httpRequest.cancel();
        }
    }
}
