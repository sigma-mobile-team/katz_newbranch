
package sigma.qilex.dataaccess.network.http;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;

import android.graphics.Bitmap;

import sigma.qilex.utils.LogUtils;

public class QilexHttpPostRequest extends QilexHttpRequest {

    public QilexHttpPostRequest(URL url, boolean isCert) {
        super(url, isCert);
        mRequestMethod = "POST";
        mDoOutput = true;
    }

    public QilexHttpPostRequest(URL url, boolean isCert, String body) {
        super(url, isCert);
        mRequestMethod = "POST";
        mDoOutput = true;
        this.setBody(body);
    }

    public QilexHttpPostRequest(URL url, boolean isCert, byte[] body) {
        super(url, isCert);
        mRequestMethod = "POST";
        mDoOutput = true;
        this.setBody(body);
    }

    public QilexHttpPostRequest(URL url, boolean isCert, Bitmap body) {
        super(url, isCert);
        mRequestMethod = "POST";
        mDoOutput = true;
        this.setBody(body);
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void setParams(HttpURLConnection urlConn, Hashtable params) {
        if (mBody != null || mBodyBytes != null) {
            return;
        }
        // enter parameters
        StringBuilder sb = new StringBuilder();
        Enumeration<String> paramKeys = mReqParams.keys();
        while (paramKeys.hasMoreElements()) {
            String key = paramKeys.nextElement();
            String value = mReqParams.get(key);
            if (sb.length() > 0)
                sb.append('&');
            sb.append(key);
            sb.append('=');

            String escapedValue = URLEncoder.encode(value);
            escapedValue = escapedValue.replace("+", "%20");
            sb.append(escapedValue);
        }

        try {
            OutputStream os = urlConn.getOutputStream();
            byte[] paramsBin = sb.toString().getBytes();
            os.write(paramsBin);
            os.close();
        } catch (Exception e) {
            // TODO: handle exception
            LogUtils.e(DBG_TAG, "HTTP POST setParams error", e);
        }
        // execution request
        LogUtils.d(DBG_TAG, "HttpsPostRequest.executeSynch sending request with params:" + sb.toString());

    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void setParamsFix(HttpURLConnection urlConn, Hashtable params) {
        if (mBody != null || mBodyBytes != null) {
            return;
        }
        // enter parameters
        StringBuilder sb = new StringBuilder();
        Enumeration<String> paramKeys = mReqParams.keys();
        while (paramKeys.hasMoreElements()) {
            String key = paramKeys.nextElement();
            String value = mReqParams.get(key);
            if (sb.length() > 0)
                sb.append('&');
            sb.append(key);
            sb.append('=');

            // escapuje not, we used one (sic) place
            // String escapedValue = URLEncoder.encode( value );
            // escapedValue = escapedValue.replace( "+", "%20" );
            sb.append(value);
        }

        try {
            OutputStream os = urlConn.getOutputStream();
            byte[] paramsBin = sb.toString().getBytes();
            os.write(paramsBin);
            os.close();
        } catch (Exception e) {
            // TODO: handle exception
            LogUtils.e(DBG_TAG, "HTTP POST setParams error", e);
        }
        // execution request
        LogUtils.d(DBG_TAG, "HttpsPostRequest.executeSynch sending request with params:" + sb.toString());

    }

    @Override
    protected URL configureURL() {
        return mURL;
    }

}
