
package sigma.qilex.dataaccess.network.http;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;

import sigma.qilex.utils.LogUtils;

public class QilexHttpDeleteRequest extends QilexHttpRequest {

    protected QilexHttpDeleteRequest(URL url, boolean isCert) {
        super(url, isCert);
        mRequestMethod = "DELETE";
        mDoOutput = false;
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void setParams(HttpURLConnection urlConn, Hashtable params) {
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void setParamsFix(HttpURLConnection urlConn, Hashtable params) {
    }

    @Override
    protected URL configureURL() {
        // wpisanie parametr�w
        StringBuilder sb = new StringBuilder();
        Enumeration<String> paramKeys = mReqParams.keys();
        while (paramKeys.hasMoreElements()) {
            String key = paramKeys.nextElement();
            String value = mReqParams.get(key);
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append(key);
            sb.append('=');
            sb.append(URLEncoder.encode(value));
            ;
        }

        try {
            return new URL(mURL.toString() + "?" + sb.toString());
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            LogUtils.e(DBG_TAG, null, e);
            return mURL;
        }
    }

}
