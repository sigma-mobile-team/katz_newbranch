
package sigma.qilex.dataaccess.network.http;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import pl.katz.aero2.Const;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.Utils;

public class ContactHttpUtils extends UserActivationUtils {
    public static void createContactEntry(Context context, String phoneNoWithCountryCode, QilexContact qilexContact,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        QilexHttpRequest qHttpRequest;
        try {
            // Make body
            JSONObject jsonObj = new JSONObject();
            String firstName = qilexContact.getFirstName();
            String lastName = qilexContact.getLastName();
            jsonObj.put("first_name", firstName);
            jsonObj.put("last_name", lastName);
            PhoneNumberModel[] normalPhone = qilexContact.getPhoneNumberNormal();
            int phoneNoIndex = 0;
            for (PhoneNumberModel phoneNumberModel : normalPhone) {
                JSONObject phoneJson = new JSONObject();
                phoneJson.put("name", phoneNumberModel.type);
                phoneJson.put("msisdn", phoneNumberModel.phoneNumber);
                jsonObj.put("msisdn_" + phoneNoIndex, phoneJson);
                phoneNoIndex++;
            }
            PhoneNumberModel[] frifonPhone = qilexContact.getPhoneNumberKATZ();
            for (PhoneNumberModel phoneNumberModel : frifonPhone) {
                JSONObject phoneJson = new JSONObject();
                phoneJson.put("name", phoneNumberModel.type);
                phoneJson.put("msisdn", phoneNumberModel.phoneNumber);
                jsonObj.put("msisdn_" + phoneNoIndex, phoneJson);
                phoneNoIndex++;
            }
            String body = jsonObj.toString();

            qHttpRequest = new QilexHttpPostRequest(
                    new URL(Const.SERVER_URL + "user_one_contact/" + phoneNoWithCountryCode), true, body);
            prepareHttpRequestHeader(qHttpRequest, phoneNoWithCountryCode, body);
            // Call execute
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
        } catch (JSONException e) {
        }
    }

    public static void updateContactEntry(Context context, String phoneNoWithCountryCode, QilexContact qilexContact,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        QilexHttpRequest qHttpRequest;
        try {
            // Make body
            JSONObject jsonObj = new JSONObject();
            String firstName = qilexContact.getFirstName();
            String lastName = qilexContact.getLastName();
            jsonObj.put("first_name", firstName);
            jsonObj.put("last_name", lastName);
            PhoneNumberModel[] normalPhone = qilexContact.getPhoneNumberNormal();
            int phoneNoIndex = 0;
            for (PhoneNumberModel phoneNumberModel : normalPhone) {
                JSONObject phoneJson = new JSONObject();
                String phoneType = phoneNumberModel.type;
                phoneJson.put("name", phoneType);
                phoneJson.put("msisdn", phoneNumberModel.phoneNumber);
                jsonObj.put("msisdn_" + phoneNoIndex, phoneJson);
                phoneNoIndex++;
            }
            PhoneNumberModel[] frifonPhone = qilexContact.getPhoneNumberKATZ();
            for (PhoneNumberModel phoneNumberModel : frifonPhone) {
                JSONObject phoneJson = new JSONObject();
                String phoneType = phoneNumberModel.type;
                phoneJson.put("name", phoneType);
                phoneJson.put("msisdn", phoneNumberModel.phoneNumber);
                jsonObj.put("msisdn_" + phoneNoIndex, phoneJson);
                phoneNoIndex++;
            }
            String body = jsonObj.toString();
            LogUtils.d("HttpPut", "HttpPut: " + jsonObj.toString());

            qHttpRequest = new QilexHttpPutRequest(new URL(Const.SERVER_URL + "user_one_contact/"
                    + phoneNoWithCountryCode + "?id=" + qilexContact.getServerContactId()), true, body);
            prepareHttpRequestHeader(qHttpRequest, phoneNoWithCountryCode, body);
            // Call execute
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
        } catch (JSONException e) {
        }
    }

    public static QilexHttpRequest getContactEntry(Context context, String phoneNoWithCountryCode, long serverId,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "user_one_contact/" + phoneNoWithCountryCode);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNoWithCountryCode, Const.STR_EMPTY);
            qHttpRequest.setParam("id", Long.toString(serverId));
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static QilexHttpRequest deleteContactEntry(Context context, String phoneNoWithCountryCode, long serverId,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "user_one_contact/" + phoneNoWithCountryCode);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpDeleteRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNoWithCountryCode, Const.STR_EMPTY);
            qHttpRequest.setParam("id", Long.toString(serverId));
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static QilexHttpRequest postContactListSnapshot(Context context, String number, String dataComp,
            long dataChecksum, QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyPushContactList(context, dataComp, dataChecksum);
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "user_full_contact_list/" + number),
                    true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public static QilexHttpRequest refreshContactListSnapshot(Context context, String number, String dataComp,
            long dataChecksum, QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyPushContactList(context, dataComp, dataChecksum);
            qHttpRequest = new QilexHttpPutRequest(new URL(Const.SERVER_URL + "user_refresh_contact_list/" + number),
                    true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    private static String makeBodyPushContactList(Context context, String dataComp, long dataChecksum) {
        Map<String, String> unsortMap = new HashMap<String, String>();
        unsortMap.put("data_comp", dataComp);
        unsortMap.put("data_checksum", "" + dataChecksum);
        return Utils.convertMapToJsonString(unsortMap);
    }

    public static QilexHttpRequest getContactList(String phoneNo, QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "user_full_contact_list/" + phoneNo);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNo, Const.STR_EMPTY);
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
