
package sigma.qilex.dataaccess.network.http;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;

public class QilexHttpPutRequest extends QilexHttpRequest {

    public QilexHttpPutRequest(URL url, boolean isCert) {
        super(url, isCert);
        mRequestMethod = "PUT";
        mDoOutput = true;
    }

    public QilexHttpPutRequest(URL url, boolean isCert, String body) {
        super(url, isCert);
        mRequestMethod = "PUT";
        mDoOutput = true;
        this.setBody(body);
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void setParams(HttpURLConnection urlConn, Hashtable params) {

    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void setParamsFix(HttpURLConnection urlConn, Hashtable params) {

    }

    @Override
    protected URL configureURL() {
        return mURL;
    }

}
