
package sigma.qilex.dataaccess.network.http;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.provider.Settings.Secure;
import pl.katz.aero2.Const;
import pl.katz.aero2.Const.QilexRequestHeader;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.sip.resiprocate.ResiprocateJni;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.SimUtils;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;

public class UserActivationUtils {

    public static void sendActivation(String number, boolean isMaster,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyRegister(isMaster);
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "user_register/" + number), true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void enterActivation(String number, String activationCode,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyEnterActivation(activationCode);
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "user_activation/" + number), true,
                    body);
            prepareHttpRequestHeader(qHttpRequest, number, body);

            // Call execute
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static QilexHttpRequest setUpUserInfo(String number, String userName, String userLastName, String userAvt,
            String formatAvt, QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodySetupUserInfo(userName, userLastName, userAvt, formatAvt);
            qHttpRequest = new QilexHttpPutRequest(new URL(Const.SERVER_URL + "user_info/" + number), true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            return null;
        }
    }

    protected static void prepareHttpRequestHeader(QilexHttpRequest qHttpRequest, String number, String body) {
        qHttpRequest.setHeader("Accept", QilexRequestHeader.ACCEPT);
        qHttpRequest.setHeader("Accept-Language", UserInfo.getInstance().getAcceptedLanguage());
        qHttpRequest.setHeader("Accept-Encoding", QilexRequestHeader.ACCEPT_ENCODING);
        qHttpRequest.setHeader("Content-Length", Zip.strLengthOfBytes(body) + "");
        qHttpRequest.setHeader("Content-Type", QilexRequestHeader.CONTENT_TYPE);
        qHttpRequest.setHeader("User-Agent", QilexRequestHeader.USER_AGENT);
//        String requestSign = Utils.buildSecretKey(number, body);
        String requestSign = Utils.prepareHttp(number, body);
        qHttpRequest.setHeader("Authorization", "FRIFON2V02 " + requestSign);
    }

    private static String makeBodyRegister(boolean isMaster) {
        Map<String, String> unsortMap = new HashMap<String, String>();
        String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
        unsortMap.put("dev_id", deviceId);
        unsortMap.put("dev_is_master", Boolean.toString(isMaster));
        unsortMap.put("dev_name", android.os.Build.MODEL);
        unsortMap.put("dev_type", "android");
        String imsi = SimUtils.getIMSI();
        if (Utils.isStringNullOrEmpty(imsi)) {
            imsi = SharedPrefrerenceFactory.getInstance().getLoginImsi();
        }
        if (!Utils.isInKatzMode() && !Utils.isStringNullOrEmpty(imsi)) {
            unsortMap.put("dev_imsi", imsi);
        }
        return Utils.convertMapToJsonString(unsortMap);
    }

    private static String makeBodyEnterActivation(String activationCode) {
        Map<String, String> unsortMap = new HashMap<String, String>();
        String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
        unsortMap.put("dev_id", deviceId);

        unsortMap.put("token", activationCode);
        unsortMap.put("dev_type", "android");
        return Utils.convertMapToJsonString(unsortMap);
    }

    private static String makeBodySetupUserInfo(String userName, String userLastName, String userAvt, String format) {
        Map<String, String> unsortMap = new HashMap<String, String>();
        String encodeUserName = userName;
        String encodeUserLastName = userLastName;
        unsortMap.put("user_name", encodeUserName);
        unsortMap.put("user_last_name", encodeUserLastName);
        unsortMap.put("user_avatar", userAvt);
        unsortMap.put("user_avatar_format", format);
        return Utils.convertMapToJsonString(unsortMap);
    }

    public static void login(String number, QilexHttpRequest.QilexHttpRequestListener listener) {
        String crt = ResiprocateJni.certificatePEM();
        crt = crt.replaceAll("\n", "\\\\n");
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyLogin(crt);
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "user_login/" + number), true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);

            // Call execute
            qHttpRequest.execute(listener);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static String makeBodyLogin(String crt) {
        String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
        Map<String, String> unsortMap = new HashMap<String, String>();
        unsortMap.put("dev_type", "android");
        unsortMap.put("dev_id", deviceId);
        unsortMap.put("dev_crt", crt);
        String imsi = SimUtils.getIMSI();
        if (Utils.isStringNullOrEmpty(imsi)) {
            imsi = SharedPrefrerenceFactory.getInstance().getLoginImsi();
        }
        if (!Utils.isInKatzMode() && !Utils.isStringNullOrEmpty(imsi)) {
            unsortMap.put("dev_imsi", SimUtils.getIMSI());
        }
        return Utils.convertMapToJsonString(unsortMap);
    }

    public static QilexHttpRequest getAccountInfo(String phoneNo, QilexHttpRequest.QilexHttpRequestListener listener) {
        try {
            URL url = new URL(Const.SERVER_URL + "user_info/" + phoneNo);
            QilexHttpRequest qHttpRequest;
            qHttpRequest = new QilexHttpGetRequest(url, true);
            prepareHttpRequestHeader(qHttpRequest, phoneNo, Const.STR_EMPTY);
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static QilexHttpRequest sendUnregister(String number, QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyUnregister();
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "user_unregister/" + number), true,
                    body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    private static String makeBodyUnregister() {
        Map<String, String> unsortMap = new HashMap<String, String>();
        String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
        unsortMap.put("dev_id", deviceId);
        unsortMap.put("dev_name", android.os.Build.MODEL);
        unsortMap.put("dev_type", "android");
        return Utils.convertMapToJsonString(unsortMap);
    }

    public static QilexHttpRequest checkIsFrifon(String number, QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            qHttpRequest = new QilexHttpGetRequest(new URL(Const.SERVER_URL + "has_frifon/" + number), true);
            prepareHttpRequestHeader(qHttpRequest, number, Const.STR_EMPTY);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //
    public static QilexHttpRequest sendIssue(String number, String body,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "user_issue/" + number), true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    private static String makeBodyAcceptRegulation(Boolean reqAccepted, Boolean regOutAccepted) {
        Map<String, String> unsortMap = new HashMap<String, String>();
        unsortMap.put("reg_accepted", reqAccepted.toString());
        unsortMap.put("reg_out_accepted", regOutAccepted.toString());
        return Utils.convertMapToJsonString(unsortMap);
    }

    public static QilexHttpRequest sendAcceptRegulation(String number, boolean reqAccepted, boolean regOutAccepted,
            QilexHttpRequest.QilexHttpRequestListener listener) {
        // Create HttpRequest object
        QilexHttpRequest qHttpRequest;
        try {
            String body = makeBodyAcceptRegulation(reqAccepted, regOutAccepted);
            qHttpRequest = new QilexHttpPostRequest(new URL(Const.SERVER_URL + "accept_reg/" + number), true, body);
            prepareHttpRequestHeader(qHttpRequest, number, body);
            // Call execute
            qHttpRequest.execute(listener);
            return qHttpRequest;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
