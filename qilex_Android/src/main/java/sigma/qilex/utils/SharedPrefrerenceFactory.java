
package sigma.qilex.utils;

import java.util.Calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;

public class SharedPrefrerenceFactory {

    private static final String QILEX_PREFERENCE_NAME = "QILEX_PREFERENCE_NAME";

    private static final String CACHE_PHONE_NO = "CACHE_PHONE_NO";

    private static final String LOGIN_IMSI = "LOGIN_IMSI";

    private static final String LOGIN_PHONE_NO = "LOGIN_PHONE_NO";

    private static final String LOGIN_ACTIVATION_CODE = "LOGIN_ACTIVATION_CODE";

    private static final String USER_INFO_AVATAR_URL = "AVATAR_URL";

    private static final String USER_EMAIL_FB = "USER_EMAIL_FB";

    private static final String MAX_WIDTH_CHAT_ADAPTER = "MAX_WIDTH_CHAT_ADAPTER";

    private static final String IS_FIRST_SYNC_CONTACT = "IS_FIRST_SYNC_CONTACT";

    private static final String MISS_CALL_NUMBER = "MISS_CALL_NUMBER";

    private static final String USER_INFO_FIRSTNAME = "USER_INFO_FIRSTNAME";

    private static final String USER_INFO_LASTNAME = "USER_INFO_LASTNAME";

    private static final String USER_INFO_PHONE_NO = "USER_INFO_PHONE_NO";

    private static final String USER_INFO_COUNTRY_CODE = "USER_INFO_COUNTRY_CODE";

    private static final String USER_INFO_ACTIVATION_CODE = "USER_INFO_ACTIVATION_CODE";

    private static final String PURCHASE_HISTORY_CACHE = "PURCHASE_HISTORY_CACHE";

    private static final String PURCHASE_OUT_CALL_CACHE = "PURCHASE_OUT_CALL_CACHE";

    private static final String MARK_LOGGED_OUT = "MARK_LOGGED_OUT";

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";

    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    public static final String PROPERTY_REG_ID = "registration_id";

    public static final String LOGIN_CACHE = "LOGIN_CACHE";

    public static final String LOGIN_CACHE_PRIV_KEY = "LOGIN_CACHE_PRIV_KEY";

    public static final String LOGIN_CACHE_PUB_KEY = "LOGIN_CACHE_PUB_KEY";

    public static final String LAST_TIME_CHECK_CONTACT = "LAST_TIME_CHECK_CONTACT_%s";

    public static final String CONTACT_LIST_JSON_CACHE = "CONTACT_LIST_JSON_CACHE";

    public static final String PAYMENT_RESEND = "PAYMENT_RESEND";

    public static final String LAST_TIME_AUTO_SYNC_CONTACT = "LAST_TIME_AUTO_SYNC_CONTACT";

    private static SharedPrefrerenceFactory mInstance;

    private SharedPreferences prefs;

    public static SharedPrefrerenceFactory getInstance() {
        if (mInstance == null) {
            mInstance = new SharedPrefrerenceFactory(MyApplication.getAppContext());
        }
        return mInstance;
    }

    private SharedPrefrerenceFactory(Context context) {
        prefs = context.getSharedPreferences(QILEX_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public void putString(String prefKey, String content) {
        prefs.edit().putString(prefKey, content).commit();
    }

    public String getString(String prefKey) {
        return prefs.getString(prefKey, "");
    }

    public void putBoolean(String prefKey, boolean bcontent) {
        prefs.edit().putBoolean(prefKey, bcontent).commit();
    }

    public boolean getBoolean(String prefKey) {
        return prefs.getBoolean(prefKey, false);
    }

    public void remove(String prefKey) {
        prefs.edit().remove(prefKey).commit();
    }

    public String getLoginPhoneNo() {
        return prefs.getString(LOGIN_PHONE_NO, null);
    }

    public String getLoginImsi() {
        return prefs.getString(LOGIN_IMSI, null);
    }

    public void saveLoginPhoneAndImsi(String phoneNo, String imsi) {
        prefs.edit().putString(LOGIN_PHONE_NO, phoneNo).commit();
        prefs.edit().putString(LOGIN_IMSI, imsi).commit();
    }

    public void clearCacheImsi() {
        prefs.edit().putString(LOGIN_IMSI, Const.STR_EMPTY).commit();
    }

    public String getLoginActiveCode() {
        return prefs.getString(LOGIN_ACTIVATION_CODE, null);
    }

    public void saveLoginInfo(String phoneNo, String activationCode) {
        prefs.edit().putString(LOGIN_PHONE_NO, phoneNo).commit();
        prefs.edit().putString(LOGIN_ACTIVATION_CODE, activationCode).commit();
    }

    public String getAvatarUrl() {
        return prefs.getString(USER_INFO_AVATAR_URL, null);
    }

    public void saveAvatarUrl(String avatarUrl) {
        prefs.edit().putString(USER_INFO_AVATAR_URL, avatarUrl).commit();
    }

    public String getEmailFb() {
        return prefs.getString(USER_EMAIL_FB, null);
    }

    public void saveEmailFb(String email) {
        prefs.edit().putString(USER_EMAIL_FB, email).commit();
    }

    public void saveMaxWidthChatAdapter(int maxWidth) {
        prefs.edit().putInt(MAX_WIDTH_CHAT_ADAPTER, maxWidth).commit();
    }

    public int getMaxWidthChatAdapter() {
        return prefs.getInt(MAX_WIDTH_CHAT_ADAPTER, 0);
    }

    public void setFirstSyncToTrue() {
        prefs.edit().putBoolean(IS_FIRST_SYNC_CONTACT, true).commit();
    }

    public void setFirstSyncToFalse() {
        prefs.edit().putBoolean(IS_FIRST_SYNC_CONTACT, false).commit();
    }

    public boolean isFirstSyncContact() {
        return prefs.getBoolean(IS_FIRST_SYNC_CONTACT, false);
    }

    public int getMissCallNotViewed() {
        return prefs.getInt(MISS_CALL_NUMBER, 0);
    }

    public void increaseMissCallNum() {
        int missCallNum = getMissCallNotViewed();
        missCallNum++;
        prefs.edit().putInt(MISS_CALL_NUMBER, missCallNum).commit();
    }

    public void setMissCallNumToZero() {
        prefs.edit().putInt(MISS_CALL_NUMBER, 0).commit();
    }

    public void saveUserInfoFirstName(String firstName) {
        prefs.edit().putString(USER_INFO_FIRSTNAME, firstName).commit();
    }

    public void saveUserInfoLastName(String lastName) {
        prefs.edit().putString(USER_INFO_LASTNAME, lastName).commit();
    }

    public void saveUserInfoActivationCode(String activationCode) {
        prefs.edit().putString(USER_INFO_ACTIVATION_CODE, activationCode).commit();
    }

    public String getUserInfoFirstName() {
        return prefs.getString(USER_INFO_FIRSTNAME, Const.STR_EMPTY);
    }

    public String getUserInfoLastName() {
        return prefs.getString(USER_INFO_LASTNAME, Const.STR_EMPTY);
    }

    public void saveUserInfoPhoneNo(String phoneNoWithoutCountryCode) {
        prefs.edit().putString(USER_INFO_PHONE_NO, phoneNoWithoutCountryCode).commit();
    }

    public void saveUserInfoCountryCode(String countryCode) {
        prefs.edit().putString(USER_INFO_COUNTRY_CODE, countryCode).commit();
    }

    public String getUserInfoPhoneNo() {
        return prefs.getString(USER_INFO_PHONE_NO, Const.STR_EMPTY);
    }

    public String getUserInfoCountryCode() {
        return prefs.getString(USER_INFO_COUNTRY_CODE, Const.STR_EMPTY);
    }

    public String getUserInfoActivationCode() {
        return prefs.getString(USER_INFO_ACTIVATION_CODE, Const.STR_EMPTY);
    }

    public void savePurchaseHistoryCache(String jsonString) {
        prefs.edit().putString(PURCHASE_HISTORY_CACHE, jsonString).commit();
    }

    public String getPurchaseHistoryCache() {
        return prefs.getString(PURCHASE_HISTORY_CACHE, Const.STR_EMPTY);
    }

    public void saveOutHistoryCache(String jsonString) {
        prefs.edit().putString(PURCHASE_OUT_CALL_CACHE, jsonString).commit();
    }

    public String getOutHistoryCache() {
        return prefs.getString(PURCHASE_OUT_CALL_CACHE, Const.STR_EMPTY);
    }

    public void setLogoutStatus(boolean isLoggedOut) {
        prefs.edit().putBoolean(MARK_LOGGED_OUT, isLoggedOut).commit();
    }

    public boolean getLoggoutStatus() {
        return prefs.getBoolean(MARK_LOGGED_OUT, false);
    }

    public void storeRegistrationId(String regId) {
        prefs.edit().putString(PROPERTY_REG_ID, regId).commit();
    }

    public String getRegistrationId() {
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.length() == 0) {
            LogUtils.d("KUNLQT", "Registration not found.");
            return "";
        }
        return registrationId;
    }

    public void setStatusSentTokenToServer(boolean isSent) {
        prefs.edit().putBoolean(SENT_TOKEN_TO_SERVER, isSent).commit();
    }

    public boolean getStatusSentTokenToServer() {
        return prefs.getBoolean(SENT_TOKEN_TO_SERVER, false);
    }

    public void setCacheLogin(String loginData) {
        prefs.edit().putString(LOGIN_CACHE, loginData).commit();
    }

    public String getCacheLoginData() {
        return prefs.getString(LOGIN_CACHE, null);
    }

    public void setContactListJsonCache(String jsonData) {
        prefs.edit().putString(CONTACT_LIST_JSON_CACHE, jsonData).commit();
    }

    public String getContactListJsonCache() {
        return prefs.getString(CONTACT_LIST_JSON_CACHE, null);
    }

    public void setCachePrivPublicKey(String privateKey, String publicKey) {
        prefs.edit().putString(LOGIN_CACHE_PRIV_KEY, privateKey).commit();
        prefs.edit().putString(LOGIN_CACHE_PUB_KEY, publicKey).commit();
    }

    /**
     * @return 0: Private key, 1: Public key
     */
    public String[] getCachePrivPublicKey() {
        String[] output = new String[2];
        output[0] = prefs.getString(LOGIN_CACHE_PRIV_KEY, null);
        output[1] = prefs.getString(LOGIN_CACHE_PUB_KEY, null);
        return output;
    }

    public void setPaymentResend(String jsonArrayData) {
        prefs.edit().putString(PAYMENT_RESEND, jsonArrayData).commit();
    }

    public String getPaymentResend() {
        return prefs.getString(PAYMENT_RESEND, null);
    }

    public void setCurrentToLastTimeAutoSync() {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        prefs.edit().putLong(LAST_TIME_AUTO_SYNC_CONTACT, currentTime).commit();
    }

    public long getLastTimeSyncWithSnapshot() {
        return prefs.getLong(LAST_TIME_AUTO_SYNC_CONTACT, 0);
    }

    public void saveCachePhoneNo(String phoneNoWithoutCountryCode) {
        prefs.edit().putString(CACHE_PHONE_NO, phoneNoWithoutCountryCode).commit();
    }

    public String loadCachePhoneNo() {
        return prefs.getString(CACHE_PHONE_NO, Const.STR_EMPTY);
    }

    public void clearAllData() {
        prefs.edit().clear().commit();
    }
}
