
package sigma.qilex.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import android.content.Context;
import android.telephony.PhoneNumberUtils;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.PhoneCodeModel;

public class QilexPhoneNumberUtils {

    public static final String KATZ_TAG = "sip:";

    public static final String PHONE_FORMAT_CHARACTER = "[^1234567890+*#]";

    public static String listOfPhoneCode;

    private static HashMap<String, PhoneCodeModel> mapCountryCodePhoneCode = null;

    private static ArrayList<String> mListPrefixNumber; // global contry code
                                                        // check beginWith

    private static ArrayList<String> mListPrefixNumberPoland; // check beginWith

    private static ArrayList<String> mListBannedNumber; // check match

    private static ArrayList<String> mListEmergency2Gsm; //

    static {
        Context context = MyApplication.getAppContext();
        initListPrefixNumber(context);
        initListPrefixPolandNumber(context);
        initListBannedNumber(context);
        initListEmergency2Gsm(context);
    }

    private static void initCountryCodePhoneCode(Context context) {
        if (mapCountryCodePhoneCode == null) {
            mapCountryCodePhoneCode = new HashMap<String, PhoneCodeModel>();
            listOfPhoneCode = ";";
            String[] arrayCountryString = context.getResources().getStringArray(R.array.countries);
            for (String string : arrayCountryString) {
                String[] strCountry = string.split(Const.STR_COMMA);
                PhoneCodeModel model = new PhoneCodeModel();
                model.countryCode = strCountry[0];
                model.phoneCode = strCountry[1];
                model.countryName = strCountry[2];
                mapCountryCodePhoneCode.put(model.countryCode, model);
                listOfPhoneCode += model.phoneCode + ";";
            }
        }
    }

    private static void initListPrefixNumber(Context context) {
        if (mListPrefixNumber == null) {
            String[] tmpArr = context.getResources().getStringArray(R.array.prefix_numbers);
            mListPrefixNumber = new ArrayList<>(Arrays.asList(tmpArr));
        }
    }

    private static void initListEmergency2Gsm(Context context) {
        if (mListEmergency2Gsm == null) {
            String[] tmpArr = context.getResources().getStringArray(R.array.emerency_to_gsm);
            mListEmergency2Gsm = new ArrayList<>(Arrays.asList(tmpArr));
        }
    }

    private static void initListPrefixPolandNumber(Context context) {
        if (mListPrefixNumberPoland == null) {
            String[] tmpArr = context.getResources().getStringArray(R.array.prefix_numbers_poland);
            mListPrefixNumberPoland = new ArrayList<>(Arrays.asList(tmpArr));

            // add 48 prefix
            ArrayList<String> listTemp = new ArrayList<>();
            for (int i = 0; i < mListPrefixNumberPoland.size(); i++) {
                listTemp.add("48" + mListPrefixNumberPoland.get(i));
            }
            mListPrefixNumberPoland.addAll(listTemp);
        }
    }

    private static void initListBannedNumber(Context context) {
        if (mListBannedNumber == null) {
            mListBannedNumber = new ArrayList<>();
            ArrayList<String> listZoneNumber = new ArrayList<>(
                    Arrays.asList(context.getResources().getStringArray(R.array.zone_numbers)));
            ArrayList<String> listProceddedNumber = new ArrayList<>(
                    Arrays.asList(context.getResources().getStringArray(R.array.procedded_numbers)));
            mListBannedNumber.addAll(listProceddedNumber);

            for (int i = 0; i < listZoneNumber.size(); i++) {
                for (int j = 0; j < listProceddedNumber.size(); j++) {
                    String phoneNo = listZoneNumber.get(i) + listProceddedNumber.get(j);
                    mListBannedNumber.add(phoneNo);
                }
            }

            // add 48 prefix
            ArrayList<String> listTemp = new ArrayList<>();
            for (int i = 0; i < mListBannedNumber.size(); i++) {
                listTemp.add("48" + mListBannedNumber.get(i));
            }
            mListBannedNumber.addAll(listTemp);
        }
    }

    /**
     * Method generate a formatted phone number (Ex: 0123-456-789 or 0123 456
     * 789) to basic number (0123456789)
     * 
     * @param phoneNo the formatted phone number
     * @return basic phone number.
     */
    public static String convertToBasicNumber(String phoneNo) {
        String output = phoneNo.replaceAll(PHONE_FORMAT_CHARACTER, "");
        return output;
    }

    /**
     * Method format phone number to block 3 character</br>
     * Sample: 0123456789 is formatted to 0 123 456 789
     * 
     * @param phoneNo the phone number
     * @return the formatted phone number.
     */
    public static String formatPhoneNoBlock3Number(String phoneNo) {
        if (phoneNo.charAt(0) == '+') {
            String[] codeAndPhone = QilexPhoneNumberUtils.removePhoneCode(phoneNo);
            return "(+" + codeAndPhone[0] + ")" + Utils.formatDivideString(codeAndPhone[1], 3, Const.STR_SPACE);
        } else {
            return Utils.formatDivideString(phoneNo, 3, Const.STR_SPACE);
        }
    }

    /**
     * Convert from Phone number format: XXXX to frifon address (format:
     * frifon:freefonXXXX@redefine.pl)
     * 
     * @param phoneNo
     * @return
     */
    public static String convertPhoneNo2KATZAddr(String phoneNo) {
        // Add global country code to phone
        String phoneWithCountryCode = addCurrentCountryCodeToPhone(phoneNo);
        return KATZ_TAG + phoneWithCountryCode;
    }

    /**
     * Get phone code base on country code. Example: 48 (is phone code of
     * Poland)
     * 
     * @param countryCode pl, vn, ...
     * @return phone code of country
     */
    public static PhoneCodeModel getCountryPhoneCodeModel(String countryCode) {
        Context context = MyApplication.getAppContext();
        initCountryCodePhoneCode(context);
        PhoneCodeModel model = mapCountryCodePhoneCode.get(countryCode);
        return model;
    }

    /**
     * Get current device's country code, example: pl (is Poland).
     * 
     * @return country code of devices.
     */
    public static PhoneCodeModel getLocalPhoneCodeModel() {
        Context context = MyApplication.getAppContext();
        initCountryCodePhoneCode(context);
        String locale = context.getResources().getConfiguration().locale.getCountry();
        locale = locale.toLowerCase(Locale.getDefault());
        return mapCountryCodePhoneCode.get(locale);
    }

    /**
     * Add current devices country code to phone number. Example: if you are in
     * Poland, and phone number is 0123456789, it will change to +48123456789.
     * 
     * @param phoneNumber
     * @return
     */
    public static String addCurrentCountryCodeToPhone(String phoneNumber) {
        if (checkEmergencyGsm(phoneNumber)) {
            return phoneNumber;
        }
        if (Utils.isStringNullOrEmpty(phoneNumber)) {
            return UserInfo.getInstance().getCountryCode();
        }

        // If phone have country code
        if (phoneNumber.charAt(0) == '+') {
            return phoneNumber;
        }

        // If phone have 00 at first
        if (phoneNumber.length() > 2 && phoneNumber.startsWith("00")) {
            return "+" + phoneNumber.substring(2);
        }

        // Step 1: Remove character "0" at the first if exist
        String output = "";
        if (phoneNumber.charAt(0) == '0') {
            output = phoneNumber.replaceFirst("0", "");
        } else {
            output = phoneNumber;
        }

        // Step 2: Get current phone code
        String phoneCode = UserInfo.getInstance().getCountryCode();
        if (Utils.isStringNullOrEmpty(phoneCode) == false) {
            phoneCode = "+" + phoneCode;
        } else {
            phoneCode = "+" + getLocalPhoneCodeModel().phoneCode;
        }

        // Step 3: Add to phone number
        output = phoneCode + output;

        return output;
    }

    public static String stripExceptNumbers(String str, boolean includePlus) {
        StringBuilder res = new StringBuilder(str);
        String phoneChars = "0123456789";
        if (includePlus) {
            phoneChars += "+";
        }
        for (int i = res.length() - 1; i >= 0; i--) {
            if (!phoneChars.contains(res.substring(i, i + 1))) {
                res.deleteCharAt(i);
            }
        }
        return res.toString();
    }

    /**
     * Remove Phone Code from a phone number. Sample: in Poland, Phone no is
     * +48123456789, method will return 123456789 (remove +48).</br>
     * If phone number begin with "0", remove "0" (0123456789 is replace to
     * 123456789)
     * 
     * @param phoneWithCountryCode
     * @return
     */
    public static String[] removePhoneCode(String phoneWithCountryCode) {
        initCountryCodePhoneCode(MyApplication.getAppContext());
        String[] output = new String[3];

        // Check if phone start with "0", remove "0"
        if (phoneWithCountryCode.charAt(0) == '0') {
            output[0] = "0";
            output[1] = phoneWithCountryCode.replaceFirst("0", "");
            return output;
        }

        // Check if phone is not global number, not start with "+"
        if (phoneWithCountryCode.charAt(0) != '+') {
            output[0] = "";
            output[1] = phoneWithCountryCode;
            return output;
        }

        // If phone is start with + , check it's phone code
        // Check if phone length is 1
        if (phoneWithCountryCode.length() < 3) {
            output[0] = phoneWithCountryCode;
            output[1] = "";
            return output;
        }

        String phone = phoneWithCountryCode.substring(1);
        int subStringStart = 0;
        for (int i = 1; i <= 4 && i <= phone.length(); i++) {
            String canbePhoneCode = phone.substring(0, i);
            if (listOfPhoneCode.contains(";" + canbePhoneCode + ";")) {
                subStringStart = i;
                break;
            }
        }
        output[0] = phone.substring(0, subStringStart);
        output[1] = phone.substring(subStringStart);
        output[2] = phone.substring(subStringStart);
        return output;
    }

    /**
     * Check if 2 phone number is equals, except country code.</br>
     * For example : +48123456789 and 0123456789 is equal.
     * 
     * @param phoneNum1
     * @param phoneNum2
     * @return true is 2 phone number is equal, false if not
     */
    public static boolean checkPhoneIsEqual(String phoneNum1, String phoneNum2) {
        String[] phoneWithCode1 = removePhoneCode(phoneNum1);
        String[] phoneWithCode2 = removePhoneCode(phoneNum2);

        // If phone is not equal, return false
        if (phoneWithCode1[1].equals(phoneWithCode2[1]) == false) {
            return false;
        }

        // If phone is equal, check code
        // Case 1: code is equal
        if (phoneWithCode1[0].equals(phoneWithCode2[0])) {
            return true;
        }

        // Case 2: code1 is 0, code2 is normal
        if (phoneWithCode1[0].equals("0") && phoneWithCode2.length > 0) {
            return true;
        }
        if (phoneWithCode2[0].equals("0") && phoneWithCode1.length > 0) {
            return true;
        }

        // Case 3: code1 and code2 is normal, and equal
        if (phoneWithCode1[1].equals(phoneWithCode2[1])) {
            return true;
        }
        return false;
    }

    /**
     * check global phonenumber (contain '+' and digital)
     * 
     * @param number
     * @return
     */
    public static boolean isGlobalPhoneNumber(String number) {
        return PhoneNumberUtils.isGlobalPhoneNumber(number);
    }

    /**
     * check special phonenumber
     * 
     * @param number
     * @return
     */
    public static boolean isEmergencyNumber(String number, boolean isCheckFobidden) {
        if (isCheckFobidden) {
            return PhoneNumberUtils.isEmergencyNumber(number) || isInValidPhone(number) || !checkWithMoreRule(number);
        } else {
            return PhoneNumberUtils.isEmergencyNumber(number) || isInValidPhone(number);
        }
    }

    private static boolean isInValidPhone(String number) {
        return number.contains("*") || number.contains("#");
    }

    // remove "+", remove "0", remove "+0" if meets
    private static String getPurePhoneNo(String phoneNo) {
        String purePhone = phoneNo;
        if (phoneNo.length() > 1) {
            if (phoneNo.charAt(0) == '+') {
                purePhone = phoneNo.substring(1);
            }
            if (purePhone.charAt(0) == '0') {
                purePhone = purePhone.substring(1);
            }
        } else if (phoneNo.length() == 1) {
            if (phoneNo.charAt(0) == '+' || purePhone.charAt(0) == '0') {
                purePhone = Const.STR_EMPTY;
            }
        }
        return purePhone;
    }

    public static boolean checkEmergencyGsm(String phoneNo) {
        return mListEmergency2Gsm.contains(phoneNo);
    }

    private static boolean checkWithMoreRule(String phoneNo) {
        boolean isValid = true;
        String pureNo = getPurePhoneNo(phoneNo);
        LogUtils.d("binhdt", "In checkWithMoreRule " + pureNo);
        boolean isNotPolandPhoneNo = phoneNo.startsWith("+") && !phoneNo.startsWith("+48");

        if (isNotPolandPhoneNo) {
            for (String prefix : mListPrefixNumber) {
                if (pureNo.startsWith(prefix)) {
                    isValid = false;
                    LogUtils.d("binhdt", "In mListPrefixNumber");
                    break;
                }
            }
        } else {
            for (String prefix : mListBannedNumber) {
                if (pureNo.equals(prefix)) { // check if pureNo is match or not
                    isValid = false;
                    LogUtils.d("binhdt", "In mListBannedNumber");
                    break;
                }
            }

            for (String prefix : mListPrefixNumberPoland) {
                if (pureNo.startsWith(prefix)) {
                    isValid = false;
                    LogUtils.d("binhdt", "In mListPrefixNumberPoland");
                    break;
                }
            }
        }

        return isValid;
    }

    public static final String[] SUPPORT_NUMBER = {
        "+48999999999", "999999999"
    };

    public static boolean isSupportNumber(String phoneNumber) {
        for (String supportNumber : SUPPORT_NUMBER) {
            if (supportNumber.equals(phoneNumber)) {
                return true;
            }
        }
        return false;
    }
}
