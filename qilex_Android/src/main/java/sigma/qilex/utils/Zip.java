
package sigma.qilex.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.CRC32;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class Zip {

    private static int OPTIONS = android.util.Base64.NO_WRAP;

    private static boolean mNoHeader = false;

    public static String encodeBase64(String input) throws UnsupportedEncodingException {
        return android.util.Base64.encodeToString(input.getBytes("UTF-8"), OPTIONS);
    }

    public static String encodeBase64(byte[] bytes) throws UnsupportedEncodingException {
        return android.util.Base64.encodeToString(bytes, OPTIONS);
    }

    public static String decodeBase64(String base64String) {
        byte[] byteArray = android.util.Base64.decode(base64String, OPTIONS);
        return new String(byteArray);
    }

    public static byte[] decodeBase64ToByte(String base64String) {
        return android.util.Base64.decode(base64String, OPTIONS);
    }

    public static DictBody compress(String string) throws IOException {
        CRC32 crc = new CRC32();
        byte[] input = string.getBytes("UTF-8");
        byte[] compressedData = compress(input, Deflater.BEST_COMPRESSION, mNoHeader);
        crc.update(compressedData);
        String data_comp = android.util.Base64.encodeToString(compressedData, OPTIONS);
        DictBody body = new DictBody(data_comp, crc.getValue());
        return body;
    }

    /**/
    private static byte[] compress(byte[] input, int compressionLevel, boolean noHeader) throws IOException {
        Deflater compressor = new Deflater(compressionLevel, noHeader);
        compressor.setInput(input);
        compressor.finish();
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] readBuffer = new byte[65536];
        int readCount = 0;
        while (!compressor.finished()) {
            readCount = compressor.deflate(readBuffer);
            if (readCount > 0) {
                bao.write(readBuffer, 0, readCount);
            }
        }
        compressor.end();
        return bao.toByteArray();
    }

    public static String decompress(String responData) throws IOException, DataFormatException {
        byte[] compressedData = android.util.Base64.decode(responData, OPTIONS);
        byte[] decompressedData = decompress(compressedData, mNoHeader);
        String output = new String(decompressedData, "UTF-8");
        return output;
    }

    private static byte[] decompress(byte[] input, boolean noHeader) throws IOException, DataFormatException {
        Inflater decompressor = new Inflater(noHeader);
        decompressor.setInput(input);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        byte[] readBuffer = new byte[1024];
        int readCount = 0;

        while (!decompressor.finished()) {
            readCount = decompressor.inflate(readBuffer);
            if (readCount > 0) {
                bao.write(readBuffer, 0, readCount);
            }
        }
        decompressor.end();
        return bao.toByteArray();
    }

    public static class DictBody {
        public String data_comp;

        public long data_checksum;

        public DictBody(String data, long checksum) {
            data_comp = data;
            data_checksum = checksum;
        }
    }

    public static int strLengthOfBytes(String input) {
        try {
            byte[] buf = input.getBytes("UTF-8");
            return buf.length;
        } catch (UnsupportedEncodingException e) {
            return input.length();
        }
    }
}
