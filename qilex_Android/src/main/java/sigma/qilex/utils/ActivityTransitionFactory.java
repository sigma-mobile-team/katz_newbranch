
package sigma.qilex.utils;

import pl.frifon.aero2.R;

public class ActivityTransitionFactory {

    public static final int IN_LEFT_OUT_LEFT = 1;

    public static final int IN_RIGHT_OUT_RIGHT = 2;

    public static final int IN_BOTTOM_OUT_BOTTOM = 3;

    public static final int IN_LEFT_OUT_LEFT_2 = 4;

    public static final int IN_RIGHT_OUT_RIGHT_2 = 5;

    private static final int[] TRANSLATION_IN_LEFT_OUT_LEFT = {
            R.anim.activity_in_left, R.anim.activity_out_left, R.anim.activity_stay_out, R.anim.activity_stay_in
    };

    private static final int[] TRANSLATION_IN_RIGHT_OUT_RIGHT = {
            R.anim.activity_in_right, R.anim.activity_out_right, R.anim.activity_stay_out, R.anim.activity_stay_in
    };

    private static final int[] TRANSLATION_IN_BOTTOM_OUT_BOTTOM = {
            R.anim.activity_in_bottom, R.anim.activity_out_bottom, R.anim.activity_stay_out, R.anim.activity_stay_in
    };

    private static final int[] TRANSLATION_IN_LEFT_OUT_LEFT_2 = {
            R.anim.activity_in_left, R.anim.activity_out_left, R.anim.activity_movetohalf_left,
            R.anim.activity_movefromhalf_left
    };

    private static final int[] TRANSLATION_IN_RIGHT_OUT_RIGHT_2 = {
            R.anim.activity_in_right, R.anim.activity_out_right, R.anim.activity_movetohalf_right,
            R.anim.activity_movefromhalf_right
    };

    public static int[] getAnimation(int type) {
        switch (type) {
            case IN_LEFT_OUT_LEFT:
                return TRANSLATION_IN_LEFT_OUT_LEFT;
            case IN_RIGHT_OUT_RIGHT:
                return TRANSLATION_IN_RIGHT_OUT_RIGHT;
            case IN_BOTTOM_OUT_BOTTOM:
                return TRANSLATION_IN_BOTTOM_OUT_BOTTOM;
            case IN_LEFT_OUT_LEFT_2:
                return TRANSLATION_IN_LEFT_OUT_LEFT_2;
            case IN_RIGHT_OUT_RIGHT_2:
                return TRANSLATION_IN_RIGHT_OUT_RIGHT_2;
            default:
                return TRANSLATION_IN_LEFT_OUT_LEFT;
        }
    }
}
