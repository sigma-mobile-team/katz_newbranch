
package sigma.qilex.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;

public class DateTimeUtils {

    private static final SimpleDateFormat FORMAT_YYYYMMDDHHMMSS = new SimpleDateFormat("yyyyMMddHHmmss");

    public static String formatDate(Calendar c) {
        StringBuilder sb = new StringBuilder();
        int i = c.get(Calendar.DAY_OF_MONTH);
        if (i < 10)
            sb.append('0');
        sb.append(i);
        sb.append('.');
        i = c.get(Calendar.MONTH) + 1;// +1 Solid months because they are from
                                      // scratch
        if (i < 10)
            sb.append('0');
        sb.append(i);
        sb.append('.');
        sb.append(c.get(Calendar.YEAR));
        return sb.toString();
    }

    public static String formatDate(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return formatDate(c);
    }

    public static String formatDateToMsgTimestampText(Calendar c, String[] months) {
        StringBuilder sb = new StringBuilder();
        int i = c.get(Calendar.HOUR_OF_DAY);
        sb.append(i);
        sb.append(':');
        i = c.get(Calendar.MINUTE);
        if (i < 10)
            sb.append('0');
        sb.append(i);
        sb.append(',');
        sb.append(' ');
        sb.append(c.get(Calendar.DAY_OF_MONTH));
        sb.append(' ');
        sb.append(months[c.get(Calendar.MONTH)]);
        return sb.toString();
    }

    public static String formatDateToMsgTimestampText(Date d, String[] months) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return formatDateToMsgTimestampText(c, months);
    }

    public static String formatDateWithHour(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return formatDateWithHour(c);
    }

    public static String formatDateWithHour(Calendar c) {
        StringBuilder sb = new StringBuilder();
        int i = c.get(Calendar.DAY_OF_MONTH);
        if (i < 10)
            sb.append('0');
        sb.append(i);
        sb.append('.');
        i = c.get(Calendar.MONTH) + 1;// +1 Solid months because they are from
                                      // scratch
        if (i < 10)
            sb.append('0');
        sb.append(i);
        sb.append('.');
        sb.append(c.get(Calendar.YEAR));
        sb.append(',');
        sb.append(' ');
        i = c.get(Calendar.HOUR_OF_DAY);
        sb.append(i);
        sb.append(':');
        i = c.get(Calendar.MINUTE);
        if (i < 10)
            sb.append('0');
        sb.append(i);
        sb.append(':');
        i = c.get(Calendar.SECOND);
        if (i < 10)
            sb.append('0');
        sb.append(i);
        return sb.toString();
    }

    public static String formatTimeInSeconds(int time) {
        StringBuilder sb = new StringBuilder();
        int i = time / 3600;
        sb.append(i);
        sb.append(':');
        time %= 3600;
        i = time / 60;
        if (i == 0) {
            sb.append('0');
            sb.append('0');
        } else if (i < 10) {
            sb.append('0');
            sb.append(i);
        } else {
            sb.append(i);
        }
        sb.append(':');
        i = time % 60;
        if (i == 0) {
            sb.append('0');
            sb.append('0');
        } else if (i < 10) {
            sb.append('0');
            sb.append(i);
        } else {
            sb.append(i);
        }
        return sb.toString();
    }

    /**
     * Convert time in second to format mm:ss
     * 
     * @param timeSecond time in second.
     * @return time in format mm:ss
     */
    public static String formatTimeMMSS(int timeSecond) {
        StringBuilder sb = new StringBuilder();
        int minute = timeSecond / 60;
        int second = timeSecond % 60;

        if (minute < 10) {
            sb.append(Const.STR_0);
        }
        sb.append(minute);
        sb.append(':');
        if (second < 10) {
            sb.append(Const.STR_0);
        }
        sb.append(second);
        return sb.toString();
    }

    @SuppressLint("DefaultLocale")
    public static String formatTimeMinuteSecond(int timeSecond) {
        Resources res = MyApplication.getAppContext().getResources();
        int minute = timeSecond / 60;
        int second = timeSecond % 60;

        String time = res.getString(R.string.time_format, minute, second);
        return time;
    }

    public static String convertMinuteToReadableTime(int minute) {
        Resources res = MyApplication.getAppContext().getResources();
        String output = "";
        int minutePrint = minute % 60;
        int hour = minute / 60;
        int hourPrint = hour % 24;
        int day = hour / 24;
        int dayPrint = day % 30;
        int month = day / 30;
        int monthPrint = month % 12;
        int year = month / 12;
        if (year > 0) {
            output += res.getString(R.string.years, year);
            if (monthPrint > 0) {
                output += Const.STR_SPACE + res.getString(R.string.months, monthPrint);
            }
            return output;
        }
        if (month > 1) {
            output += res.getString(R.string.months, month);
            return output;
        } else if (month == 1) {
            output += res.getString(R.string.month);
            if (dayPrint > 0) {
                output += Const.STR_SPACE + res.getString(R.string.days, dayPrint);
            }
            return output;
        }
        if (day > 1) {
            return res.getString(R.string.days, day);
        } else if (day == 1) {
            output += res.getString(R.string.day);
            if (hourPrint > 0) {
                output += Const.STR_SPACE + res.getString(R.string.hours, hourPrint);
            }
            return output;
        }
        if (hour > 1) {
            return res.getString(R.string.hours, hour);
        } else if (hour == 1) {
            output += res.getString(R.string.hour);
            if (minutePrint > 0) {
                output += Const.STR_SPACE + res.getString(R.string.minutes, minutePrint);
            }
            return output;
        }
        if (minute == 1)
            return res.getString(R.string.minute);
        return res.getString(R.string.minutes, minute);
    }

    public static String getCurrentTime_yyyyMMddHHmmss() {
        Calendar cal = Calendar.getInstance();
        return FORMAT_YYYYMMDDHHMMSS.format(cal.getTime());
    }
}
