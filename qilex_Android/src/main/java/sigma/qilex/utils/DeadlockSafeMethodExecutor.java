package sigma.qilex.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


/**
 * Runs method in a separate thread, with configurable timeout and retries.
 */
public class DeadlockSafeMethodExecutor
{
	private int mTimeout = 5000;
	private int mRetries = 1;


	private class ExecutorThread extends Thread
	{
		private Result mResult;
		private Class<?> mClass;
		private Object mObject;
		private String mMethodName;
		private Object[] mParams;

		public ExecutorThread(Result result, Class<?> clazz, Object obj, String methodName, Object... params)
		{
			if (!(clazz == null ^ obj == null) || methodName == null)
			{
				throw new RuntimeException();
			}
			mResult = result;
			mClass = clazz;
			mObject = obj;
			mMethodName = methodName;
			mParams = params;
		}

		@Override
		public void run()
		{
			Class<?>[] paramTypes = null;
			if (mParams != null)
			{
				paramTypes = new Class<?>[mParams.length];
				for (int i = 0; i < paramTypes.length; i++)
				{
					paramTypes[i] = mParams[i].getClass();
				}
			}
			Method method = null;
			try
			{
				if (mObject == null)
				{
					method = mClass.getMethod(mMethodName, paramTypes);
				}
				else
				{
					method = mObject.getClass().getMethod(mMethodName, paramTypes);
				}

				mResult.object = method.invoke(mObject, mParams);
			}
			catch (NoSuchMethodException e)
			{
				mResult.exception = e;
			}
			catch (IllegalAccessException e)
			{
				mResult.exception = e;
			}
			catch (IllegalArgumentException e)
			{
				mResult.exception = e;
			}
			catch (InvocationTargetException e)
			{
				mResult.exception = e;
			}
		}
	}

	public Result executeStaticMethod(Class<?> clazz, String methodName, Object... params)
	{
		Result result;
		int retries = 0;
		do
		{
			result = execute(clazz, null, methodName, params);
			retries++;
		}
		while (result.isTimeout && retries <= mRetries);

		return result;
	}

	public Result executeInstanceMethod(Object obj, String methodName, Object... params)
	{
		Result result;
		int retries = 0;
		do
		{
			result = execute(null, obj, methodName, params);
			retries++;
		}
		while (result.isTimeout && retries <= mRetries);

		return result;
	}

	private Result execute(Class<?> clazz, Object obj, String methodName, Object... params)
	{
		Result result = new Result();
		ExecutorThread et = new ExecutorThread(result, clazz, obj, methodName, params);

		et.start();
		try
		{
			et.join(mTimeout);
		}
		catch (InterruptedException e)
		{}

		if (et.isAlive())
		{
			result.isTimeout = true;
			et.interrupt();
		}
		return result;
	}

	public void setTimeout(int secs)
	{
		mTimeout = secs;
	}

	public void setRetries(int n)
	{
		mRetries = n;
	}


	public class Result
	{
		public Object object;
		public Exception exception;
		public boolean isTimeout;
	}
}
