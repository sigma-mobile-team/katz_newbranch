
package sigma.qilex.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.katz.aero2.Const;
import pl.katz.aero2.Const.QilexRequestHeader;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.manager.GDCQueue;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.sip.resiprocate.ResiprocateJni;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.adapter.ChatAdapter.OnMessageChoose;
import sigma.qilex.ui.customview.CustomButton;
import sigma.qilex.ui.customview.MyCustomTextView;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.download.ImageDownloader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.IoUtils;
import com.nostra13.universalimageloader.utils.L;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

public class Utils {

    public final static int PHOTO_REQUEST = 3097;

    public final static int CAMERA_REQUEST = 3099;

    public final static int CROP_REQUEST = 4001;

    public static final int THUMBNAIL_SIZE = 300;

    public static String mAvatarFileName = null;

    private static Dialog tmpDialog = null;

    public static volatile GDCQueue stageQueue = new GDCQueue("stageQueue");

    public static volatile GDCQueue globalQueue = new GDCQueue("globalQueue");

    public static volatile GDCQueue contactQueue = new GDCQueue("contactQueue");

    public static final SimpleDateFormat dateFormatMMDDHHmm = new SimpleDateFormat("MM/dd HH:mm");

    public static boolean checkPlayServices(Activity mAct) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(MyApplication.getAppContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                if (null != mAct)
                    GooglePlayServicesUtil
                            .getErrorDialog(resultCode, mAct, AuthenticationManager.PLAY_SERVICES_RESOLUTION_REQUEST)
                            .show();
            }
            return false;
        }
        return true;
    }

    public static boolean checkPlayServicesExist() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int errorCheck = api.isGooglePlayServicesAvailable(MyApplication.getAppContext());
        if (errorCheck == ConnectionResult.SUCCESS) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check SKD level for call preferencesEditor commit or apply
     * 
     * @param preferencesEditor
     */
    static public void commitOrApplyPreferences(Editor preferencesEditor) {
        if (Build.VERSION.SDK_INT >= 9)
            preferencesEditor.apply();
        else
            preferencesEditor.commit();
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * ICS or later.
     */
    public static boolean hasICSAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * JELLY_BEAN or later.
     */
    public static boolean hasJbAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * JELLY_BEAN_MR1 or later.
     */
    public static boolean hasJbMr1Above() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * JELLY_BEAN_MR2 or later.
     */
    public static boolean hasJbMr2Above() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * KITKAT or later.
     */
    public static boolean hasKkAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * LOLLIPOP or later.
     */
    public static boolean hasLlAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * LOLLIPOP_MR1 or later.
     */
    public static boolean hasLlMr1Above() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    /**
     * Uses static final constants to detect if the device's platform version is
     * Marshmallow or later.
     */
    public static boolean hasMmAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean isValidEmail(String pEmail) {
        Pattern emailPattern;
        if (Build.VERSION.SDK_INT >= 8)
            emailPattern = Patterns.EMAIL_ADDRESS;
        else
            emailPattern = Pattern.compile(
                    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");

        return emailPattern.matcher(pEmail).matches();
    }

    /**
     * decodes image and scales it to reduce memory consumption
     * 
     * @param resolver
     * @param uri
     * @return Bitmap
     */
    public static Bitmap decodeBitmapFromUri(ContentResolver resolver, Uri uri) {
        return decodeBitmapFromUri(resolver, uri, THUMBNAIL_SIZE);
    }

    /**
     * decodes image and scales it to reduce memory consumption
     * 
     * @param resolver
     * @param uri
     * @param size : small size with avatar
     * @return
     */
    public static Bitmap decodeBitmapFromUri(ContentResolver resolver, Uri uri, int size) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        try {
            InputStream iStream = resolver.openInputStream(uri);
            BitmapFactory.decodeStream(iStream, null, o);
            iStream.close();
            // The new size we want to scale to
            int requireSizeWidth = size;
            int requireSizeHeight = size;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= requireSizeWidth && o.outHeight / scale / 2 >= requireSizeHeight)
                scale *= 2;
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            InputStream iStream2 = resolver.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(iStream2, null, o2);
            bitmap = ThumbnailUtils.extractThumbnail(bitmap, size, size);
            Bitmap out = rotateBitmap(getRealPathFromURI(uri), bitmap);
            iStream2.close();
            return out;
        } catch (IOException e) {
            e.printStackTrace();
            LogUtils.d("binhdt", "decodeBitmapFromUri exc " + e.toString());
        }
        return null;
    }

    public static Bitmap decodeBitmapFromFile(BaseActivity activity, String filePath) {
        return decodeBitmapFromFile(activity, filePath, THUMBNAIL_SIZE);
    }

    public static Bitmap decodeBitmapFromFile(BaseActivity activity, String filePath, int size) {
        // Decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, size, size);
        Bitmap out = rotateBitmap(filePath, bitmap);
        return out;
    }

    private static Bitmap rotateBitmap(String file, Bitmap bitmap) {
        try {
            File f = new File(file);
            ExifInterface exif = new ExifInterface(f.getAbsolutePath());
            String exifOrientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            FileInputStream fileStreamResize = new FileInputStream(f);
            fileStreamResize.close();
            if (exifOrientation.equals(String.valueOf(ExifInterface.ORIENTATION_ROTATE_90))) {
                return rotateBitmap(bitmap, 90);
            } else if (exifOrientation.equals(String.valueOf(ExifInterface.ORIENTATION_ROTATE_180))) {
                return rotateBitmap(bitmap, 180);
            } else if (exifOrientation.equals(String.valueOf(ExifInterface.ORIENTATION_ROTATE_270))) {
                return rotateBitmap(bitmap, 270);
            }
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
        return bitmap;
    }

    private static String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {
                    MediaStore.Images.Media.DATA
            };
            cursor = MyApplication.getAppContext().getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Create template file with name is part and it will be stored in
     * /sdcard/KATZ
     * 
     * @param part
     * @return
     * @throws Exception
     */
    public static File createTemporaryFile(String part) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/" + Const.FOLDER_NAME());
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return new File(tempDir.getAbsoluteFile() + "/" + part);
    }

    public static File getTemporaryFile(String part) {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/" + Const.FOLDER_NAME());
        return new File(tempDir.getAbsoluteFile() + "/" + part);
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Save bitmap to image and store it in sdcard
     * 
     * @param bitmap
     * @param name
     * @return
     */
    public static String saveToTempFolder(Bitmap bitmap, String name) {
        try {
            File tempFile = Utils.createTemporaryFile(name);
            if (tempFile.exists()) {
                tempFile.delete();
                MemoryCacheUtils.removeFromCache(Uri.fromFile(tempFile).toString(),
                        ImageLoader.getInstance().getMemoryCache());
                DiskCacheUtils.removeFromCache(Uri.fromFile(tempFile).toString(),
                        ImageLoader.getInstance().getDiskCache());
            }
            FileOutputStream out = new FileOutputStream(tempFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);
            out.flush();
            out.close();
            return tempFile.getAbsolutePath();
        } catch (Exception e) {
            return "";
        }
    }

    public static Bitmap decodeSampledBitmapFromByte(Context context, byte[] bitmapBytes) {

        Display display = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        int reqWidth, reqHeight;
        Point point = new Point();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(point);
            reqWidth = point.x;
            reqHeight = point.y;
        } else {
            reqWidth = display.getWidth();
            reqHeight = display.getHeight();
        }

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inMutable = true;
        options.inBitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false; // If set to true, the decoder will
                                            // return null (no bitmap), but the
                                            // out... fields will still be set,
                                            // allowing the caller to query the
                                            // bitmap without having to allocate
                                            // the memory for its pixels.
        options.inPurgeable = true; // Tell to gc that whether it needs free
                                    // memory, the Bitmap can be cleared
        options.inInputShareable = true; // Which kind of reference will be used
                                         // to recover the Bitmap data after
                                         // being clear, when it will be used in
                                         // the future

        return BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int initialInSampleSize = computeInitialSampleSize(options, reqWidth, reqHeight);

        int roundedInSampleSize;
        if (initialInSampleSize <= 8) {
            roundedInSampleSize = 1;
            while (roundedInSampleSize < initialInSampleSize) {
                // Shift one bit to left
                roundedInSampleSize <<= 1;
            }
        } else {
            roundedInSampleSize = (initialInSampleSize + 7) / 8 * 8;
        }

        return roundedInSampleSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final double height = options.outHeight;
        final double width = options.outWidth;

        final long maxNumOfPixels = reqWidth * reqHeight;
        final int minSideLength = Math.min(reqHeight, reqWidth);

        int lowerBound = (maxNumOfPixels < 0) ? 1 : (int)Math.ceil(Math.sqrt(width * height / maxNumOfPixels));
        int upperBound = (minSideLength < 0) ? 128
                : (int)Math.min(Math.floor(width / minSideLength), Math.floor(height / minSideLength));

        if (upperBound < lowerBound) {
            // return the larger one when there is no overlapping zone.
            return lowerBound;
        }

        if (maxNumOfPixels < 0 && minSideLength < 0) {
            return 1;
        } else if (minSideLength < 0) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }

    public static Bitmap rotatePicture(Context context, int rotation, byte[] data) {
        Bitmap bitmap = decodeSampledBitmapFromByte(context, data);

        if (rotation != 0) {
            Bitmap oldBitmap = bitmap;

            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);

            bitmap = Bitmap.createBitmap(oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, false);

            oldBitmap.recycle();
        }

        return bitmap;
    }

    public static Uri savePicture(Context context, Bitmap bitmap) {
        int cropHeight;
        if (bitmap.getHeight() > bitmap.getWidth())
            cropHeight = bitmap.getWidth();
        else
            cropHeight = bitmap.getHeight();

        bitmap = ThumbnailUtils.extractThumbnail(bitmap, cropHeight, cropHeight, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                context.getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        // Saving the bitmap
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            FileOutputStream stream = new FileOutputStream(mediaFile);
            stream.write(out.toByteArray());
            stream.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }

        // Mediascanner need to scan for the image saved
        Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri fileContentUri = Uri.fromFile(mediaFile);
        mediaScannerIntent.setData(fileContentUri);
        context.sendBroadcast(mediaScannerIntent);

        return fileContentUri;
    }

    /**
     * Save image url to gallery (KATZ folder in sdcard)
     * 
     * @param imageUrl
     * @param fileName
     * @return
     */
    public static String saveToGallery(String imageUrl, String fileName) {
        InputStream sourceStream = null;
        String saved = null;
        try {
            File fileForImage = createTemporaryFile(fileName);

            File cachedImage = ImageLoader.getInstance().getDiskCache().get(imageUrl);
            if (cachedImage != null && cachedImage.exists()) { // if image was
                                                               // cached by UIL
                sourceStream = new FileInputStream(cachedImage);
            } else { // otherwise - download image
                ImageDownloader downloader = new BaseImageDownloader(MyApplication.getAppContext());
                sourceStream = downloader.getStream(imageUrl, null);
            }
            if (sourceStream != null) {
                try {
                    OutputStream targetStream = new FileOutputStream(fileForImage);
                    try {
                        IoUtils.copyStream(sourceStream, targetStream, null);
                        saved = fileForImage.getAbsolutePath();
                    } catch (IOException e) {
                        L.e(e);
                    } finally {
                        targetStream.close();
                    }
                } catch (IOException e) {
                    L.e(e);
                } finally {
                    try {
                        sourceStream.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            L.e(e);
        } catch (Exception e) {
        }

        return saved;
    }

    public static boolean isStringNullOrEmpty(String src) {
        if (src == null) {
            return true;
        }
        if (Const.STR_EMPTY.equals(src.trim())) {
            return true;
        }
        return false;
    }

    public static String formatDivideString(String src, int divideArea, String divideStr) {
        if (src.length() == 0) {
            return src;
        }

        char[] charArray = src.toCharArray();
        String output = Const.STR_EMPTY;

        for (int i = charArray.length - 1; i >= 0; i--) {
            if ((charArray.length - i - 1) % divideArea == 0) {
                output = divideStr + output;
            }
            output = charArray[i] + output;
        }

        if (divideStr.equals(output.substring(output.length() - 1))) {
            output = output.substring(0, output.length() - 1);
        }
        return output;
    }

    /**
     * Calculating MD5, in this form is stored at the moment only password
     * 
     * @param str the String used to calculate.
     * @return string MD5-formatted string
     */
    public static String calculateMD5(String str) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(str.getBytes());
            byte passDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexPass = new StringBuffer();
            for (int i = 0; i < passDigest.length; i++) {
                String h = Integer.toHexString(0xFF & passDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexPass.append(h);
            }
            return hexPass.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static KeyPair keyGenerator(String type)
            throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen;
        keyGen = KeyPairGenerator.getInstance(type);
        keyGen.initialize(new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4), null);
        return keyGen.generateKeyPair();
    }

    public static String convertLoginToDisplayedForm(String login) {
        int atIdx = login.lastIndexOf("_at_");
        int atEndIdx = atIdx + 4;
        if (atIdx > 0) {
            login = login.substring(0, atIdx) + "@" + login.substring(atEndIdx);
        }
        return login;
    }

    /**
     * method onClick user avatar for change
     */
    public static void showChangeAvtDialog(final BaseActivity activity) {
        int[] strResourceIds = {
                R.string.take_photo, R.string.from_gallery
        };

        OnClickListener listeners[] = {
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        activity.startCamera();
                        tmpDialog.cancel();
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        photoPickerIntent.setType("image/*");
                        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                        activity.startActivityForResult(photoPickerIntent, PHOTO_REQUEST);
                        tmpDialog.cancel();
                    }
                }
        };
        tmpDialog = showBasicSelectionDialog(activity, strResourceIds, listeners, true);
    }

    public static void showContextMenu(final Context activity, final String message, final long id, final int position,
            final OnMessageChoose onMessageChooseClick, final boolean isCopy) {
        int[] strResourceIds = {
                R.string.delete, R.string.copy_message
        };
        int[] strResNotCopyIds = {
                R.string.delete
        };
        OnClickListener listeners[] = {
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        onMessageChooseClick.onDelete(id, position);
                        tmpDialog.cancel();
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        onMessageChooseClick.onCopy(message);
                        tmpDialog.cancel();
                    }
                }
        };
        OnClickListener listenerNotCopies[] = {
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        onMessageChooseClick.onDelete(id, position);
                        tmpDialog.cancel();
                    }
                }
        };
        tmpDialog = showBasicSelectionDialog(activity, R.layout.message_longpress_dialog_title, message,
                isCopy ? strResourceIds : strResNotCopyIds, isCopy ? listeners : listenerNotCopies, true);
    }

    /**
     * Show dialog with base input
     * 
     * @param context
     * @param strContents
     * @param listeners
     * @param hasCancelItem
     * @return
     */
    public static Dialog showBasicSelectionDialog(Context context, String[] strContents,
            View.OnClickListener[] listeners, boolean hasCancelItem) {
        return showBasicSelectionDialog(context, -1, null, strContents, listeners, hasCancelItem);
    }

    /**
     * Show dialog with base input
     * 
     * @param context
     * @param strResourceIds
     * @param listeners
     * @param hasCancelItem
     * @return
     */
    public static Dialog showBasicSelectionDialog(Context context, int[] strResourceIds,
            View.OnClickListener[] listeners, boolean hasCancelItem) {
        return showBasicSelectionDialog(context, -1, null, strResourceIds, listeners, hasCancelItem);
    }

    /**
     * Show dialog with base input
     * 
     * @param context
     * @param titleLayoutRes : < 0 if dialog without title
     * @param strResourceIds
     * @param listeners
     * @param hasCancelItem
     * @return
     */
    public static Dialog showBasicSelectionDialog(Context context, int titleLayoutRes, String message,
            int[] strResourceIds, final View.OnClickListener[] listeners, boolean hasCancelItem) {
        if (strResourceIds.length != listeners.length) {
            return null;
        }

        final Dialog dialog = new Dialog(context, R.style.Dialog_No_Border);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater li = LayoutInflater.from(context);
        View viewDialog = li.inflate(R.layout.basic_selection_dialog, null);
        if (titleLayoutRes >= 0) {
            View title = li.inflate(titleLayoutRes, null);
            MyCustomTextView titleContent = (MyCustomTextView)title.findViewById(R.id.title_content);
            titleContent.setText(message);
            ((LinearLayout)viewDialog.findViewById(R.id.rootLayout)).addView(title, 0);
        }
        for (int i = 0; i < strResourceIds.length; i++) {
            final int index = i;
            TextView txtItem = (TextView)li.inflate(R.layout.basic_selection_dialog_item, null);
            txtItem.setText(strResourceIds[i]);
            txtItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listeners[index].onClick(v);
                    dialog.dismiss();
                }
            });
            ((LinearLayout)viewDialog.findViewById(R.id.rootLayout)).addView(txtItem);
        }

        if (hasCancelItem) {
            TextView txtItem = (TextView)li.inflate(R.layout.basic_selection_dialog_item, null);
            txtItem.setText(R.string.cancel);
            txtItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ((LinearLayout)viewDialog.findViewById(R.id.rootLayout)).addView(txtItem);
        }
        dialog.setContentView(viewDialog);
        dialog.show();
        if (titleLayoutRes > 0) {
            Vibrator vibe = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
            vibe.vibrate(10); // 50 is time in ms
        }
        return dialog;
    }

    /**
     * Show dialog with base input
     * 
     * @param context
     * @param titleLayoutRes : < 0 if dialog without title
     * @param message
     * @param listeners
     * @param hasCancelItem
     * @return
     */
    public static Dialog showBasicSelectionDialog(Context context, int titleLayoutRes, String message,
            String[] strContents, final View.OnClickListener[] listeners, boolean hasCancelItem) {
        if (strContents.length != listeners.length) {
            return null;
        }

        final Dialog dialog = new Dialog(context, R.style.Dialog_No_Border);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater li = LayoutInflater.from(context);
        View viewDialog = li.inflate(R.layout.basic_selection_dialog, null);
        if (titleLayoutRes >= 0) {
            View title = li.inflate(titleLayoutRes, null);
            MyCustomTextView titleContent = (MyCustomTextView)title.findViewById(R.id.title_content);
            titleContent.setText(message);
            ((LinearLayout)viewDialog.findViewById(R.id.rootLayout)).addView(title);
        }
        for (int i = 0; i < strContents.length; i++) {
            final int index = i;
            TextView txtItem = (TextView)li.inflate(R.layout.basic_selection_dialog_item, null);
            txtItem.setText(strContents[i]);
            txtItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    listeners[index].onClick(v);
                    dialog.dismiss();
                }
            });
            ((LinearLayout)viewDialog.findViewById(R.id.rootLayout)).addView(txtItem);
        }

        if (hasCancelItem) {
            TextView txtItem = (TextView)li.inflate(R.layout.basic_selection_dialog_item, null);
            txtItem.setText(R.string.cancel);
            txtItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ((LinearLayout)viewDialog.findViewById(R.id.rootLayout)).addView(txtItem);
        }
        dialog.setContentView(viewDialog);
        dialog.show();
        if (titleLayoutRes > 0) {
            Vibrator vibe = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
            vibe.vibrate(10); // 50 is time in ms
        }
        return dialog;
    }

    public static String generateDateToHumanView(long timeInMillis) {
        String result = convertSimpleDayFormat(timeInMillis) + Const.STR_SPACE + Utils.parseHourTime(timeInMillis);
        return result;
    }

    public static String formatTime(long time) {
        return "" + time;
    }

    public static String formatDurationTime(long time) {
        return "Duration\n" + time;
    }

    /**
     * android.permission.READ_PHONE_STATE needed.
     * 
     * @return Line1Number (typically MSISDN) as a String, whether possible to
     *         read. Null otherwise.
     */
    public static String getMSISDN() {
        final TelephonyManager telephonyManager = (TelephonyManager)MyApplication.getAppContext()
                .getSystemService(Context.TELEPHONY_SERVICE);

        String strMSISDN = null;
        try {
            strMSISDN = telephonyManager.getLine1Number();
        } catch (SecurityException e) {
            LogUtils.e("Qilex", "EX getMSISDN() READ_PHONE_STATE permission needed: ", e);
        }

        return strMSISDN;
    }

    public static boolean isInMasterMode() {
        return isSupportTelephony();
    }

    public static boolean isSupportTelephony() {
        PackageManager mgr = MyApplication.getAppContext().getPackageManager();
        boolean hasTelephony = mgr.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        return hasTelephony;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static Calendar clearTimes(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }

    public static String convertSimpleDayFormat(long val) {
        Calendar today = Calendar.getInstance();
        today = clearTimes(today);

        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        yesterday = clearTimes(yesterday);

        Calendar last7days = Calendar.getInstance();
        last7days.add(Calendar.DAY_OF_YEAR, -7);
        last7days = clearTimes(last7days);

        Calendar last30days = Calendar.getInstance();
        last30days.add(Calendar.DAY_OF_YEAR, -30);
        last30days = clearTimes(last30days);
        if (val > today.getTimeInMillis() + 24 * 3600 * 1000) {
            return parseDayTime(val);
        }
        if (val >= today.getTimeInMillis()) {
            return MyApplication.getAppResource().getString(R.string.today);
        } else if (val >= yesterday.getTimeInMillis()) {
            return MyApplication.getAppResource().getString(R.string.yesterday);
        } else {
            return parseDayTime(val);
        }

    }

    /**
     * Time in milisecond
     * 
     * @param time
     * @return
     */
    public static String parseDayTime(long time) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        Date d = new Date(time);
        String result = df.format(d);
        if (result == null) {
            return "";
        } else {
            return result;
        }
    }

    /**
     * Time in milisecond
     * 
     * @param time
     * @return
     */
    public static String parseHourTime(long time) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
        Date d = new Date(time);
        String result = df.format(d);
        if (result == null) {
            return "";
        } else {
            return result;
        }
    }

    public static String convertMapToJsonString(Map<String, String> map) {
        Map<String, String> treeMap = new TreeMap<String, String>(new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }

        });
        treeMap.putAll(map);
        StringBuilder builder = new StringBuilder("{");
        int i = 0;
        for (Map.Entry<String, ?> entry : treeMap.entrySet()) {
            builder.append("\"").append(entry.getKey()).append("\": ");
            builder.append("\"").append(entry.getValue()).append("\"");
            i++;
            if (i < treeMap.entrySet().size()) {
                builder.append(", ");
            }
        }
        builder.append("}");
        return builder.toString();
    }

    public static String prepareHttp(String number, String body) {
        int contentLength = Zip.strLengthOfBytes(body);
        StringBuilder string_to_sign = new StringBuilder();
        string_to_sign.append(QilexRequestHeader.ACCEPT).append("\n").append(QilexRequestHeader.ACCEPT_ENCODING)
                .append("\n").append(contentLength).append("\n").append(QilexRequestHeader.CONTENT_TYPE).append("\n")
                .append(QilexRequestHeader.USER_AGENT).append("\n").append(body).append("\n");
        String message = string_to_sign.toString();
        //byte[] hashData = ResiprocateJni.hmacSHA512(number, message, UserInfo.getInstance().sigKey);
        byte[] hashData = ResiprocateJni.hmacSHA512(number, message, "vqsO+XOZFK4C4tFGOiBbtydXqYg=");
        try {
            return Zip.encodeBase64(hashData);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    // NAMND
    /**
     * @param number
     * @return format katz number (KATZ ID)
     */
    public static String getKATZNumber(String number) {
        return QilexPhoneNumberUtils.convertToBasicNumber(number);
    }

    public static void sendSms(Context context, String callNo) {
        if (Utils.isInMasterMode()) {
            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.setData(Uri.parse("sms:" + callNo));
            context.startActivity(smsIntent);
        }
    }

    public static void sendSmsWithBody(Context context, String callNo, String content) {
        if (Utils.isInMasterMode()) {
            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
            smsIntent.addCategory(Intent.CATEGORY_DEFAULT);
            smsIntent.setType("vnd.android-dir/mms-sms");
            smsIntent.putExtra("sms_body", content);
            smsIntent.setData(Uri.parse("sms:" + callNo));
            context.startActivity(smsIntent);
        }
    }

    public static void showToastInSlaveMode(Context c) {
        Toast.makeText(c, c.getString(R.string.slave_func_not_avaiable), Toast.LENGTH_SHORT).show();
    }

    /**
     * Resize textSize all buttons input for fit button width
     * 
     * @param context
     * @param buttons
     */
    public static void formatTextSizeButton(Context context, final CustomButton... buttons) {
        float density = context.getResources().getDisplayMetrics().density;
        final float totalPadding = (CustomButton.PADDING_HORIZONTAL + 1) * 2 * density;// 1pixel
                                                                                       // from
                                                                                       // 9-patch
        final CustomButton buttonFisrt = buttons[0];
        buttonFisrt.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                Paint paint = new Paint();
                ArrayList<Float> sizes = new ArrayList<Float>();
                for (CustomButton button : buttons) {

                    float size = button.getTextSize();
                    paint.setTextSize(size);
                    paint.setTypeface(button.getTypeface());
                    while (paint.measureText(button.getText().toString()) + totalPadding > button.getWidth()) {
                        size--;
                        paint.setTextSize(size);
                    }
                    paint.setTextSize(size - 1);
                    sizes.add(size);
                }
                float min = Float.MAX_VALUE;
                for (float f : sizes) {
                    if (min > f)
                        min = f;
                }
                for (CustomButton button : buttons) {
                    button.setTextSize(TypedValue.COMPLEX_UNIT_PX, min);
                }
                // this is an important step not to keep receiving
                // callbacks:
                // we should remove this listener
                buttonFisrt.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

    }

    public static void showNetworkErrDialog(Context c) {
        AlertDialog.Builder b = new AlertDialog.Builder(c);
        b.setTitle(c.getString(R.string.dialog_packet_data_title));
        b.setMessage(c.getString(R.string.dialog_packet_data_body));
        b.setNegativeButton(R.string.ok, null);
        b.create().show();
    }

    public static boolean isStringFilterAccepted(String targetString, String filter) {
        String[] filters = filter.toLowerCase(Locale.UK).split(Const.STR_SPACE);
        for (String strFilters : filters) {
            if (Utils.isStringNullOrEmpty(strFilters) == false) {
                if (targetString.indexOf(strFilters) != 0
                        && targetString.contains(Const.STR_SPACE + strFilters) == false) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int parseIntZeroBase(String input) {
        try {
            int output = Integer.parseInt(input);
            return output;
        } catch (NumberFormatException ex) {
            return 1;
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    // NAMND wrote: extract number from string (content sms)
    public static String extractNumber(String contentSMS){
        String strNumber = contentSMS.replaceAll("[^-?0-9]+", "");
        return strNumber;
    }
    public static float dpFromPx(final float px) {
        return px / MyApplication.getAppResource().getDisplayMetrics().density;
    }

    public static float pxFromDp(final float dp) {
        return dp * MyApplication.getAppResource().getDisplayMetrics().density;
    }

    public static String parseIntToString(String value) {
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(value);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }

    public static Drawable getDrawable(Activity activity, int attrResId) {
        int[] attrs = {
                attrResId
        };
        TypedArray ta = activity.getTheme().obtainStyledAttributes(attrs);
        return ta.getDrawable(0);
    }

    public static int getColor(Context activity, int attrResId) {
        int[] attrs = {
                attrResId
        };
        TypedArray ta = activity.getTheme().obtainStyledAttributes(attrs);
        return ta.getColor(0, Color.WHITE);
    }

    public static int getDimens(Context activity, int attrResId) {
        int[] attrs = {
                attrResId
        };
        TypedArray ta = activity.getTheme().obtainStyledAttributes(attrs);
        return ta.getDimensionPixelOffset(0, 0);
    }

    public static String getString(Context activity, int attrResId) {
        int[] attrs = {
                attrResId
        };
        TypedArray ta = activity.getTheme().obtainStyledAttributes(attrs);
        return ta.getString(0);
    }

    public static boolean isInKatzMode() {
        return MyApplication.getAppContext().getApplicationInfo().theme == R.style.KatzAppThemeBlue;
    }
}
