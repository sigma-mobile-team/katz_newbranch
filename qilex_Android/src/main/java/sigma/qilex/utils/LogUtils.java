package sigma.qilex.utils;

import android.util.Log;

/**
 * Created by Phuc on 9/14/2016.
 */
public class LogUtils {
    private static final boolean DEBUG_MODE = false;

    public static void d(String TAG, String MSG) {
        if (DEBUG_MODE) {
            Log.d(TAG, MSG);
        }
    }

    public static void i(String TAG, String MSG) {
        if (DEBUG_MODE) {
            Log.i(TAG, MSG);
        }
    }

    public static void e(String TAG, String MSG) {
        if (DEBUG_MODE) {
            Log.e(TAG, MSG);
        }
    }

    public static void e(String TAG, String MSG, Throwable tr) {
        if (DEBUG_MODE) {
            Log.e(TAG, MSG, tr);
        }
    }
}
