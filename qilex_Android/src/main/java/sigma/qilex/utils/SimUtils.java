package sigma.qilex.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import pl.katz.aero2.MyApplication;

/**
 * Created by Phuc on 6/16/2016.
 */
public class SimUtils {
    public static final String LOG_TAG = "SimUtils";

    /**
     * Method check if imsi number get from SIM is not same as old IMSI saved in Preference.
     * @return false if : oldIMSI or newIMSI is blank or oldIMSI equal newIMSI.
     *         true if : oldIMSI and newIMSI is not blank and not same.
     */
    public static boolean checkIMSIchangeManual() {
        String oldIMSI = SharedPrefrerenceFactory.getInstance().getLoginImsi();
        String newIMSI = getIMSI();
        if (Utils.isStringNullOrEmpty(newIMSI)
                || Utils.isStringNullOrEmpty(oldIMSI)
                || oldIMSI.equals(getIMSI())) {
            return false;
        }
        return true;
    }

    public static String getIMSI() {
//        synchronized (Utils.class) {
//            if (m_strIMSI != null)
//                return m_strIMSI;
//        }

        String strIMSI = null;

        try {
            final TelephonyManager telephonyManager = (TelephonyManager)MyApplication.getAppContext().getSystemService(
                    Context.TELEPHONY_SERVICE);

            try {
                strIMSI = telephonyManager.getSubscriberId();
            } catch (SecurityException e) {
                LogUtils.d(LOG_TAG, "EX MobienceUtils.getIMSI - READ_PHONE_STATE permission needed");
            }

            // for some devices above method may fail (for example only 6 digits
            // are returned)
            // try second method then
            if (strIMSI == null || strIMSI.length() < 14) {
                Process proc = null;
                try {
                    // does not work on all devices
                    proc = safeExecuteRuntimeCommand("getprop ril.IMSI");
                    BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                    strIMSI = br.readLine();
                } catch (java.io.IOException e) {
//                    logToCrashlytics(LOG_TAG, "EX MobienceUtils.getIMSI IOException", e);
                }
                if (proc != null)
                    proc.destroy();
            }
        } catch (Exception e) {
//            logToCrashlytics(LOG_TAG, "EX MobienceUtils.getIMSI unknown exception", e);
        }

        synchronized (Utils.class) {
            m_strIMSI = strIMSI;
            return strIMSI;
        }
    }

    /**
     * execute runtime command on another thread, with timeout - to avoid
     * deadlock caused by Android bug
     */
    public static Process safeExecuteRuntimeCommand(String cmd) {
        LogUtils.i(LOG_TAG, "Executing runtime command: " + cmd);

        DeadlockSafeMethodExecutor executor = new DeadlockSafeMethodExecutor();

        if (MyApplication.getAppContext() == null) {
            // app very start, do not block for too long here
            executor.setRetries(0);
            executor.setTimeout(2000);
        }

        DeadlockSafeMethodExecutor.Result res = executor.executeInstanceMethod(Runtime.getRuntime(), "exec", cmd);

        if (res.exception != null) {
//            logToCrashlytics("safeExecuteRuntimeCommand", "safeExecuteRuntimeCommand exception, command: " + cmd,
//                    res.exception);
            return null;
        }

        if (res.isTimeout) {
//            logToCrashlytics("safeExecuteRuntimeCommand", "safeExecuteRuntimeCommand timeout, command:",
//                    new RuntimeException(cmd));
            return null;
        }

        return (Process)res.object;
    }


    // IMSI of device
    private static volatile String m_strIMSI = null;

    // MAC of device
    private static volatile String m_strMAC = null;

    // UUID
    private static volatile String m_strRandomUUID = null;
}
