
package sigma.qilex.ui.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import pl.katz.aero2.Const.Config;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ConversationModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.PresenceManager;
import sigma.qilex.manager.account.PresenceManager.PingStatusListener;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.manager.account.QilexServiceRegistrationListener;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactChangeListener;
import sigma.qilex.ui.adapter.ListConversationSettingAdapter;
import sigma.qilex.ui.tab.ChatFragment;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.DateTimeUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;

public class ConversationSettingActivity extends BaseActivity
        implements ContactChangeListener, PingStatusListener, QilexServiceRegistrationListener {

    public static final String INTENT_PARAM_GROUP_CHAT_UUID = "INTENT_PARAM_GROUP_CHAT_UUID";
    public static final String INTENT_PARAM_IS_ADMIN = "INTENT_PARAM_IS_ADMIN";
    public static final String INTENT_PARAM_ADMIN_PHONE = "INTENT_PARAM_ADMIN_PHONE";
    public static final String INTENT_PARAM_THREAD_ID = "INTENT_PARAM_THREAD_ID";
    
    private static final int REQUEST_VIEW_CONTACT = 404;

    public static final String RESPONSE_NEW_GROUP_UUID = "RESPONSE_NEW_GROUP_UUID";
    public static final int RESPONSE_CODE_LEAVE_GROUP = 300;

    ListView mList;

    private ContactManager mContactManager;

    private PresenceManager mPresenceManager;

    private AuthenticationManager mAuthenManager;
    
    private ChatManager mChatManager;

    View mHeader;

    String[] mNumbers;

    private ListConversationSettingAdapter adapter;

    private String paramStatus;

    private ArrayList<ConversationModel> mListConversationChatModel;

    private Timer mPingContactTimer;

    private TimerTask mPingContactTimerTask;

    private boolean isReloadWhenResume = true;
    
    private String groupChatUuid;
    
    private boolean isAdmin;
    
    private String adminPhoneNo;
    
    long mThreadId;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContactManager.removeContactChangeListener(this);
        mPresenceManager.unregisterPingStatusListener(this);
        mAuthenManager.unregisterServiceRegistrationListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionbarTitle(R.string.convertion_setting);
        setContentView(R.layout.chat_setting_activity);
        mContactManager = ContactManager.getInstance();
        mPresenceManager = PresenceManager.getInstance();
        mAuthenManager = AuthenticationManager.getInstance();
        mChatManager = ChatManager.getInstance();
        mNumbers = getIntent().getStringArrayExtra("contact_list_numbers");
        if (mNumbers == null) {
        	mNumbers = new String[0];
        }

        // Get Ping status
        paramStatus = getIntent().getExtras().getString(ConversationActivity.INTENT_PARAM_ONLINE_STATUS, null);
        groupChatUuid = getIntent().getExtras().getString(INTENT_PARAM_GROUP_CHAT_UUID, null);
        isAdmin = getIntent().getExtras().getBoolean(INTENT_PARAM_IS_ADMIN, false);
        adminPhoneNo = getIntent().getExtras().getString(INTENT_PARAM_ADMIN_PHONE, null);
        mThreadId = getIntent().getExtras().getLong(INTENT_PARAM_THREAD_ID, 0);

        mList = (ListView)findViewById(android.R.id.list);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mHeader = inflater.inflate(R.layout.chat_setting_header, null);
        mList.addHeaderView(mHeader);
        if (Utils.isStringNullOrEmpty(groupChatUuid)) {
        	findViewById(R.id.btnLeftGroup).setVisibility(View.GONE);
        }
        showBackAction();
        // Create adapter
        adapter = new ListConversationSettingAdapter(this);
        adapter.animateFirstListener = this.animateFirstListener;
        adapter.displayImageOptions = this.circleDisplayImageOptions;
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position < 2) {
                    Intent it = new Intent(ConversationSettingActivity.this, AccountActivity.class);
                    it.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                    it.putExtra("IS_LISTENER_RESULT", true);
                    ConversationSettingActivity.this.startActivityForResult(it, REQUEST_VIEW_CONTACT);
                    return;
                }
                
                if (isAdmin) {
                	showDialogAdmin(position);
                } else {
                    // Open Contact Detail
                	openContactDetail(position);
                }
            }
        });
        mContactManager.addContactChangeListener(this);
        mPresenceManager.registerPingStatusListener(this);
        mAuthenManager.registerServiceRegistrationListener(this);
        isReloadWhenResume = true;
    }
    
    private void showDialogAdmin(final int positionInList) {
    	int[] resourceString = {R.string.view_contact_detail, R.string.remove_participant};
    	OnClickListener []onClicks = new OnClickListener[2];
    	onClicks[0] = new OnClickListener() {
			@Override
			public void onClick(View v) {
				openContactDetail(positionInList);
			}
		};
    	onClicks[1] = new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Kick
				String[] newNumbers = new String[mNumbers.length - 1];
				int index = 0;
				for (int i = 0; i < mNumbers.length; i++) {
					if (i != positionInList - 2) {
						newNumbers[index] = mNumbers[i];
						index++;
					}
				}
				String uuid = mChatManager.sendKickGroupChat(groupChatUuid, -1, mNumbers[positionInList - 2]);
				if (uuid == null) {
					showAlert(R.string.error, R.string.error_connection);
				} else {
					mNumbers = newNumbers;
					bindData();
				}
			}
		};
    	Utils.showBasicSelectionDialog(this, resourceString, onClicks, true);
    }
    
    private void openContactDetail(int positionInList) {
    	QilexContact contact = QilexUtils.hasSelfPermission(ConversationSettingActivity.this,
                CONTACT_PERMISSIONS)
                        ? ContactManager.getInstance().getContactByPhoneNoFromPhoneBook(
                                ConversationSettingActivity.this, mNumbers[positionInList - 2])
                        : null;
        if (contact != null) {
            Intent intent = new Intent(ConversationSettingActivity.this, ContactKATZOutDetailActivity.class);
            contact.setKatz(true);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contact.id);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY, contact.lookUpKey);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                    ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            startActivityForResult(intent, REQUEST_VIEW_CONTACT);
        } else {
            Intent intent = new Intent(ConversationSettingActivity.this, ContactKATZOutDetailActivity.class);
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_PHONE_NUMBER, mNumbers[positionInList - 2]);
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_VIEW_TYPE,
                    ContactKATZOutDetailActivity.VIEW_TYPE_CALLLOG);
            long[] ids = new long[0];
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_LIST_CALLLOG, ids);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                    ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            ConversationSettingActivity.this.startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimerPingContact();
        if (isReloadWhenResume) {
            bindData();
            isReloadWhenResume = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimerPingContact();
    }

    private void startTimerPingContact() {
        mPingContactTimerTask = new TimerTask() {
            @Override
            public void run() {
                pingContactStatus();
            }
        };
        mPingContactTimer = new Timer();
        mPingContactTimer.schedule(mPingContactTimerTask, 1000, Config.TIME_MILLIS_DELAY_PING_CONTACT_STATUS);
    }

    private void stopTimerPingContact() {
        if (mPingContactTimerTask != null) {
            mPingContactTimer.cancel();
            mPingContactTimerTask.cancel();
        }
    }

    private void pingContactStatus() {
        Utils.globalQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
            	for (String number : mNumbers) {
                    mPresenceManager.pingToAdrress(number);
				}
            }
        });
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_add_new_member) {
        	String[] listSelectedPhone = new String[mNumbers.length + 1];
        	for (int i = 0; i < mNumbers.length; i++) {
        		listSelectedPhone[i] = mNumbers[i];
        	}
        	listSelectedPhone[mNumbers.length] = UserInfo.getInstance().getPhoneNumberFormatted();
        	
            Intent intent = new Intent(ConversationSettingActivity.this, ContactListActivity.class);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_BROWSE_CONTACT_MODE, true);
            intent.putExtra("from_chatroom", true);
            intent.putExtra("frifon_exists_numbers", listSelectedPhone);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_BOTTOM_OUT_BOTTOM);
            startActivityForResult(intent, ChatFragment.BROWSER_CONTACT_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ChatFragment.BROWSER_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                String[] numbers = data.getStringArrayExtra("selected_numbers");
                List<String> mNumberListOld = new ArrayList<String>();
                List<String> sendInviteList = new ArrayList<>();
                for (String num : mNumbers) {
                    mNumberListOld.add(num);
                }
                for (int i = 0; i < numbers.length; i++) {
                    if (!mNumberListOld.contains(numbers[i])) {
                        mNumberListOld.add(numbers[i]);
                        if (groupChatUuid != null) {
                    		sendInviteList.add(numbers[i]);
                    	}
                    }
                }
                // If is open from single chat, send invite to all
                if (groupChatUuid == null) {
                	sendInviteList = mNumberListOld;
                }
                mNumbers = mNumberListOld.toArray(new String[mNumberListOld.size()]);
                bindData();
                
                // Send invite
                String groupUuid = mChatManager.sendInviteGroupChat(groupChatUuid, -1, sendInviteList.toArray(new String[sendInviteList.size()]));
            	if (groupUuid == null) {
                    setResult(RESULT_CANCELED);
                    finish();
                    return;
            	}
                Intent responseData = new Intent();
                if (groupChatUuid == null) {
                    responseData.putExtra(RESPONSE_NEW_GROUP_UUID, groupUuid);
                }
                setResult(RESULT_OK, responseData);
                finish();
            }
        }
        if (requestCode == REQUEST_VIEW_CONTACT) {
            if (resultCode == RESULT_OK) {
                bindData();
            }
        }
    }

    public void bindData() {
        adapter.setListItem(getListChat(), adminPhoneNo);
        mList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public ArrayList<ConversationModel> getListChat() {
        dumpData();
        return mListConversationChatModel;
    }

    public boolean isDataBinded() {
        return adapter != null;
    }

    private void dumpData() {
        mListConversationChatModel = new ArrayList<ConversationModel>();

        ConversationModel model = new ConversationModel();
        model.setAvatarUrl(UserInfo.getInstance().getAvatarUrl());
        String name = Utils.isStringNullOrEmpty(UserInfo.getInstance().getDisplayName()) ? getString(R.string.unknown)
                : UserInfo.getInstance().getDisplayName();
        model.setName(String.format(getString(R.string.you) + " (%s)", name));
        mListConversationChatModel.add(model);
        boolean hasPermissionContact = QilexUtils.hasSelfPermission(ConversationSettingActivity.this,
                CONTACT_PERMISSIONS);
        if (mNumbers != null) {
            for (String number : mNumbers) {
                QilexContact contact = hasPermissionContact ? contact = ContactManager.getInstance()
                        .getContactByPhoneNoFromPhoneBook(ConversationSettingActivity.this, number) : null;
                if (contact != null) {
                    model = new ConversationModel();
                    model.setAvatarUrl(contact.getPhotoUri());
                    model.setName(contact.getDisplayName());
                    model.setKatzNumber(number);
                    mListConversationChatModel.add(model);
                } else {
                    model = new ConversationModel();
                    TempContactModel tempContact = mContactManager.getTempContact(number);
                    if (tempContact != null) {
                        model.setName(tempContact.name);
                        model.setAvatarUrl(tempContact.avatarUrl);
                    } else {
                        model.setName(number);
                    }
                    model.setKatzNumber(number);
                    mListConversationChatModel.add(model);
                }
            }
        }
    }
    
    public void onLeftGroupClick(View v) {
    	showConfirmDialog(R.string.left_this_group, R.string.do_you_want_to_left_group,
    			new Runnable() {
			@Override
			public void run() {
				String uuid = mChatManager.sendKickGroupChat(groupChatUuid, -1, UserInfo.getInstance().getPhoneNumberFormatted());
				if (uuid == null) {
					showAlert(R.string.error, R.string.error_connection);
				} else {
					showSimpleProgress();
					postHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							setResult(RESPONSE_CODE_LEAVE_GROUP);
//			        		mChatManager.deleteChatThread(mThreadId);
							mChatManager.kickMySelfFromGroup(mThreadId);
							finish();
						}
					}, 2000);
				}
			}
		});
    }

    @Override
    public void onNewContact(QilexContact newContact) {
        isReloadWhenResume = true;
    }

    @Override
    public void onUpdateContact(QilexContact newContact) {
        isReloadWhenResume = true;
    }

    @Override
    public void onDeleteContact(QilexContact newContact) {
        isReloadWhenResume = true;
    }

    @Override
    public void onPingSuccess(final String phoneNum) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setPhoneNumStatus(phoneNum, getResources().getString(R.string.status_online));
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onPingFail(boolean isTimeOut, final String phoneNum, int offlineMinute) {
    	String status = null;
        if (isTimeOut == false) {
            status = getResources().getString(R.string.status_offline);
            if (offlineMinute > 0) {
                status = getResources().getString(R.string.status_offline_for,
                        DateTimeUtils.convertMinuteToReadableTime(offlineMinute));
            }
        } else {
        	status = getString(R.string.offline_for_a_moment);
        }
        final String newStatus = status;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setPhoneNumStatus(phoneNum, newStatus);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onReceiveRequest(String phoneNum) {
    }

    @Override
    public void onQilexServiceRegitrationStarted(QilexProfile localUserProfile) {
    }

    @Override
    public void onQilexServiceRegitrationDone(QilexProfile localUserProfile) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setItemStatus(0, getString(R.string.status_online));
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onQilexServiceRegitrationError(QilexProfile localUserProfile, int reason, String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setItemStatus(0, getString(R.string.status_offline));
                adapter.notifyDataSetChanged();
            }
        });
    }
}
