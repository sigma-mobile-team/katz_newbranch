
package sigma.qilex.ui.activity;

import java.io.File;
import java.util.Calendar;

import com.bumptech.glide.Glide;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.manager.chat.ImageThumbnailProcessor;

public class ImageInputCommentActivity extends BaseActivity {

    public static final String INTENT_LIST_URLS = "INTENT_LIST_URLS";

    public static final String INTENT_LIST_COMMENT = "INTENT_LIST_COMMENT";

    public static final String INTENT_LIST_THUMBS_BYTES = "INTENT_LIST_THUMBS_BYTES";

    public static final String INTENT_IS_FROM_CAMERA = "INTENT_IS_FROM_CAMERA";
    
    private ViewPager mPager;

    private ImageAdapter mAdapter;

    private static String[] imageUrls;

    private static String[] imageDescription;
    
    private boolean isFromCamera;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_image_description);
        setBackAction(true);
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        actionbarRight3.setText(R.string.send);
        actionbarRight3.setVisibility(View.VISIBLE);

        // Initialize control
        mPager = (ViewPager)findViewById(R.id.pagerImages);

        // Initialize data
        isFromCamera = getIntent().getExtras().getBoolean(INTENT_IS_FROM_CAMERA, false);
        imageUrls = getIntent().getExtras().getStringArray(INTENT_LIST_URLS);
        imageDescription = new String[imageUrls.length];
        for (int i = 0; i < imageUrls.length; i++) {
            imageDescription[i] = Const.STR_EMPTY;
        }
        mAdapter = new ImageAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        initActionListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageUrls = null;
        imageDescription = null;
    }

    private void generateNewImage() {
        AsyncTask<String, String, String[]> createImageTask = new AsyncTask<String, String, String[]>() {
            @Override
            protected String[] doInBackground(String... params) {
                String[] newImageUrls = new String[imageUrls.length];
                for (int i = 0; i < imageUrls.length; i++) {
                    String fileName = Calendar.getInstance().getTimeInMillis() + "_" + i + ".jpg";
                    newImageUrls[i] = ImageThumbnailProcessor.saveImageToKatzLibrary(imageUrls[i], fileName, -1);
                    if (isFromCamera) {
                        // Delete old file
                        File file = new File(imageUrls[i]);
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
                return newImageUrls;
            }

            @Override
            protected void onPostExecute(String[] result) {
                super.onPostExecute(result);
                hideSimpleProgress();
                hideKeyboard();
                Intent data = new Intent();
                data.putExtra(INTENT_LIST_URLS, result);
                data.putExtra(INTENT_LIST_COMMENT, imageDescription);
                setResult(RESULT_OK, data);
                finish();
            }
        };
        createImageTask.execute();
    }

    private void initActionListener() {
        actionbarRight3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSimpleProgress();
                generateNewImage();
            }
        });
    }

    class ImageAdapter extends FragmentStatePagerAdapter {

        public ImageAdapter(FragmentManager fm) {
            super(fm);
            // TODO Auto-generated constructor stub
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public Fragment getItem(int index) {
            // TODO Auto-generated method stub
            return ArrayListFragment.newInstance(index);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return imageUrls.length;
        }

    }

    public static class ArrayListFragment extends Fragment {

        static int mNum;

        public static ArrayListFragment newInstance(int num) {
            ArrayListFragment f = new ArrayListFragment();
            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments() != null ? getArguments().getInt("num") : 1;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.image_input_description, null);

            ImageView image = (ImageView)view.findViewById(R.id.image);
            TextView txtDescription = (TextView)view.findViewById(R.id.txtDescription);

            Glide.with(MyApplication.getAppContext()).load(new File(imageUrls[mNum])).error(R.drawable.gallery_loading)
                    .into(image);
            txtDescription.setText(imageDescription[mNum]);
            txtDescription.addTextChangedListener(new DescriptionTextWatcher(mNum));
            return view;
        }
    }

    public static class DescriptionTextWatcher implements TextWatcher {
        int index;

        public DescriptionTextWatcher(int index) {
            this.index = index;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            imageDescription[index] = s.toString().trim();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
}
