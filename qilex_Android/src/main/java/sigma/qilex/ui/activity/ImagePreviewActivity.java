
package sigma.qilex.ui.activity;

import java.io.File;

import com.bumptech.glide.Glide;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import pl.frifon.aero2.R;

public class ImagePreviewActivity extends BaseActivity implements OnClickListener {
    public static final String INTENT_IMAGE_URL = "INTENT_IMAGE_URL";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.activity_in_alpha, 0);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_preview);
        hideActionBar();

        ImageView imgPreview = (ImageView)findViewById(R.id.imgPreview);
        String url = getIntent().getExtras().getString(INTENT_IMAGE_URL);
        Glide.with(this).load(new File(url)).into(imgPreview);

        findViewById(R.id.btnClose).setOnClickListener(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.activity_out_alpha);
    }

    @Override
    public void onClick(View v) {
        finish();
    }
}
