
package sigma.qilex.ui.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.manager.SoundPoolHelper;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.ui.customview.PhoneKeypadView;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class KeypadActivity extends BaseActionBarActivity implements OnClickListener {

    private final int INSERT_CONTACT_REQUEST = 1;

    private final int ADD_CONTACT_REQUEST = 2;

    private final int EDIT_CONTACT = 4;

    private PhoneKeypadView[] keypads;

    private EditText txtNumber;

    private String inputString = Const.STR_EMPTY;

    private TextView mTxtAddToContactList, mTxtContactName;

    private Dialog dialogContact;

    private ImageView btnDeleteText;

    private ContactManager mContactManager;

    ProgressDialog mProgress;

    Handler sPostHandler = new Handler();

    SoundPoolHelper mSoundPoolHelper;

    private int[] rawSoundIds = new int[] {
            R.raw.dtmf0, R.raw.dtmf1, R.raw.dtmf2, R.raw.dtmf3, R.raw.dtmf4, R.raw.dtmf5, R.raw.dtmf6, R.raw.dtmf7,
            R.raw.dtmf8, R.raw.dtmf9, R.raw.asterix, R.raw.asterix
    };

    private View.OnClickListener onDialogClickCreateNewContact = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String phoneNum = inputString;
            phoneNum = QilexPhoneNumberUtils.convertToBasicNumber(phoneNum);
            phoneNum = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(phoneNum);
            dialogContact.dismiss();
            Intent intent = new Intent(KeypadActivity.this, CreateNewContactActivity.class);
            intent.putExtra("create_new_contact", phoneNum);
            startActivityForResult(intent, ADD_CONTACT_REQUEST);
        }
    };

    private View.OnClickListener onDialogClickAddToExistingContact = new OnClickListener() {
        @Override
        public void onClick(View v) {
            String phoneNum = inputString;
            dialogContact.dismiss();
            Intent intent = new Intent(KeypadActivity.this, ContactListActivity.class);
            intent.putExtra("insert_to_contact_number", phoneNum);
            startActivityForResult(intent, INSERT_CONTACT_REQUEST);
        }
    };

    private View.OnClickListener[] dialogListener = {
            onDialogClickCreateNewContact, onDialogClickAddToExistingContact
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.keypad_activity);
        mContactManager = ContactManager.getInstance();
        mSoundPoolHelper = new SoundPoolHelper.Builder(this).setMaxStreams(12).setType(AudioManager.STREAM_RING).build();

        // Initialize controls
        mTxtAddToContactList = (TextView)findViewById(R.id.txtAddToContactList);
        mTxtContactName = (TextView)findViewById(R.id.txtContactName);
        txtNumber = (EditText)findViewById(R.id.txtNumber);
        txtNumber.addTextChangedListener(onSearchQuery);
        txtNumber.setCustomSelectionActionModeCallback(new Callback() {
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // No implement
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // No implement
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // No implement
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // No implement
                return false;
            }
        });
        btnDeleteText = (ImageView)findViewById(R.id.btnDelete);
        keypads = new PhoneKeypadView[12];
        keypads[0] = (PhoneKeypadView)findViewById(R.id.keypad0).findViewById(R.id.keypad);
        keypads[1] = (PhoneKeypadView)findViewById(R.id.keypad1).findViewById(R.id.keypad);
        keypads[2] = (PhoneKeypadView)findViewById(R.id.keypad2).findViewById(R.id.keypad);
        keypads[3] = (PhoneKeypadView)findViewById(R.id.keypad3).findViewById(R.id.keypad);
        keypads[4] = (PhoneKeypadView)findViewById(R.id.keypad4).findViewById(R.id.keypad);
        keypads[5] = (PhoneKeypadView)findViewById(R.id.keypad5).findViewById(R.id.keypad);
        keypads[6] = (PhoneKeypadView)findViewById(R.id.keypad6).findViewById(R.id.keypad);
        keypads[7] = (PhoneKeypadView)findViewById(R.id.keypad7).findViewById(R.id.keypad);
        keypads[8] = (PhoneKeypadView)findViewById(R.id.keypad8).findViewById(R.id.keypad);
        keypads[9] = (PhoneKeypadView)findViewById(R.id.keypad9).findViewById(R.id.keypad);
        keypads[10] = (PhoneKeypadView)findViewById(R.id.keypadStar).findViewById(R.id.keypad);
        keypads[11] = (PhoneKeypadView)findViewById(R.id.keypadPound).findViewById(R.id.keypad);
        // set SoundID keypad
        for (int i = 0; i < keypads.length; i++) {
            int soundId = mSoundPoolHelper.load(rawSoundIds[i]);
            keypads[i].setDtmfId(soundId);
        }
        mTxtContactName.setVisibility(View.INVISIBLE);
        mTxtAddToContactList.setVisibility(View.INVISIBLE);
        // Initialize action listener
        initActionListener();

        // animation
        overridePendingTransition(R.anim.activity_in_bottom, R.anim.activity_stay_out);
        checkPermission(REQUEST_CALL_CODE);
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        // nothing
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
    }

    protected void onDestroy() {
        super.onDestroy();
        if (mSoundPoolHelper != null) {
            try {
                mSoundPoolHelper.destroy();
                mSoundPoolHelper = null;
            } catch (Exception e) {
            }
        }
    };

    TextWatcher onSearchQuery = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (TextUtils.isEmpty(s) || !QilexService.isReady()) {
                mTxtContactName.setVisibility(View.INVISIBLE);
                mTxtAddToContactList.setVisibility(View.INVISIBLE);
                return;
            }

            // Check if text is paste from clipboard
            String string = s.toString();
            string = QilexPhoneNumberUtils.convertToBasicNumber(string);
            if (string.equals(inputString) == false) {
                String displayString = Utils.formatDivideString(string, 3, Const.STR_SPACE);
                inputString = string;
                txtNumber.setText(displayString);
                return;
            }

            if (Utils.isStringNullOrEmpty(inputString)) {
                mTxtContactName.setVisibility(View.INVISIBLE);
                mTxtAddToContactList.setVisibility(View.INVISIBLE);
            } else {
                String phoneNo = inputString;
                QilexContact contact = mContactManager.getContactByPhoneNo(KeypadActivity.this, phoneNo);
                if (contact == null) {
                    mTxtAddToContactList.setVisibility(View.VISIBLE);
                    mTxtContactName.setVisibility(View.INVISIBLE);
                } else {
                    mTxtAddToContactList.setVisibility(View.INVISIBLE);
                    mTxtContactName.setVisibility(View.VISIBLE);
                    mTxtContactName.setText(contact.getDisplayName());
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void initActionListener() {
        for (int i = 0; i < 12; i++) {
            keypads[i].setOnClickListener(this);
            if (i < 10) {
                keypads[i].setText(Integer.toString(i));
            } else if (i == 10) {
                keypads[i].setText("*");
            } else if (i == 11) {
                keypads[i].setText("#");
            }
        }
        keypads[0].setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PhoneKeypadView keypad = (PhoneKeypadView)v;
                playSound(keypad.getDtmfId());
                int cusorLocation = txtNumber.getSelectionStart();
                if (cusorLocation == 0 && inputString.contains("+") == false) {
                    addTextNum("+");
                } else {
                    addTextNum("0");
                }
                return false;
            }
        });
        keypads[0].setSmallText("+");

        mTxtAddToContactList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int strResourceIds[] = {
                        R.string.create_new_contact, R.string.add_to_existing_contact
                };
                dialogContact = Utils.showBasicSelectionDialog(KeypadActivity.this, strResourceIds, dialogListener,
                        true);
            }
        });

        btnDeleteText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!Utils.isStringNullOrEmpty(inputString)) {
                    deleteTextAtCursor();
                }
            }
        });

        btnDeleteText.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                inputString = Const.STR_EMPTY;
                txtNumber.setText(inputString);
                return false;
            }
        });

        txtNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager)v.getContext()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        txtNumber.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager)v.getContext()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        PhoneKeypadView keypad = (PhoneKeypadView)v;
        playSound(keypad.getDtmfId());
        addTextNum(keypad.getText());
    }

    private void playSound(int soundId) {
        mSoundPoolHelper.play(soundId);
    }

    public void onFreeCallClick(View v) {
        if (TextUtils.isEmpty(txtNumber.getText().toString())) {
            return;
        }
        CallManager.getInstance().initOutGoingSipCall(this, inputString);
    }

    public void onBackClick(View v) {
        // animation
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_stay_in, R.anim.activity_out_bottom);
    }

    public void showProgress() {
        mProgress = ProgressDialog.show(this, null, getString(R.string.waiting), false);
    }

    public void showProgress(int id) {
        mProgress = ProgressDialog.show(this, null, getString(id), false);
    }

    public void showProgressByPostHandler() {
        sPostHandler.post(new Runnable() {
            @Override
            public void run() {
                showProgress();
            }
        });
    }

    public void hideProgressByPostHandler() {
        sPostHandler.post(new Runnable() {
            @Override
            public void run() {
                hideProgress();
            }
        });
    }

    public void hideProgress() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    private void goToEditContact(QilexContact contact) {
        String phoneNumberWithPhoneCode = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(inputString);
        Intent intent = new Intent(this, EditContactActivity.class);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contact.id);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY, contact.lookUpKey);
        intent.putExtra(EditContactActivity.INTENT_PARAM_ADDED_PHONE, phoneNumberWithPhoneCode);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT_2);
        startActivityForResult(intent, EDIT_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INSERT_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                long id = data.getExtras().getLong(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS);
                QilexContact con = mContactManager.getContactById(id);
                goToEditContact(con);
            } else if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == ADD_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                finish();
            } else if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == EDIT_CONTACT) {
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addTextNum(String textAdd) {
        int cusorLocation = txtNumber.getSelectionStart();
        int cursorInputString = generateInputCursor(cusorLocation, txtNumber.getText().toString());

        // if cursor is in last
        if (cursorInputString == inputString.length()) {
            inputString += textAdd;
            cursorInputString = inputString.length();
        } else if (cursorInputString == 0) {
            // If cursor is in first
            inputString = textAdd + inputString;
            cursorInputString++;
        } else {
            // If cursor is inside text
            String subStringFirst = inputString.substring(0, cursorInputString);
            String subStringLast = inputString.substring(cursorInputString);
            inputString = subStringFirst + textAdd + subStringLast;
            cursorInputString++;
        }
        // txtNumber.setText(inputString);
        String displayString = Utils.formatDivideString(inputString, 3, Const.STR_SPACE);
        txtNumber.setText(displayString);
        txtNumber.setSelection(generateDisplayCursor(cursorInputString, inputString, displayString));
    }

    private void deleteTextAtCursor() {
        int cusorLocation = txtNumber.getSelectionStart();
        int cursorInputString = generateInputCursor(cusorLocation, txtNumber.getText().toString());

        // if cursor is in last
        if (cursorInputString == inputString.length()) {
            inputString = inputString.substring(0, inputString.length() - 1);
            cursorInputString--;
        } else if (cursorInputString == 0) {
            // Do nothing
        } else {
            String subStringFirst = inputString.substring(0, cursorInputString - 1);
            String subStringLast = inputString.substring(cursorInputString);
            inputString = subStringFirst + subStringLast;
            cursorInputString--;
        }

        // txtNumber.setText(inputString);
        String displayString = Utils.formatDivideString(inputString, 3, Const.STR_SPACE);
        txtNumber.setText(displayString);
        txtNumber.setSelection(generateDisplayCursor(cursorInputString, inputString, displayString));
    }

    private int generateInputCursor(int displayCursor, String displayString) {
        String displayTextBeforeCursor = displayString.substring(0, displayCursor);
        return displayTextBeforeCursor.replaceAll(Const.STR_SPACE, Const.STR_EMPTY).length();
    }

    private int generateDisplayCursor(int inputCursor, String inputString, String displayString) {
        if (inputCursor == inputString.length()) {
            return displayString.length();
        }

        int totalSpace = (inputString.length() - 1) / 3;
        int lastSpaceNum = (inputString.length() - inputCursor) / 3;
        int firstSpaceNum = totalSpace - lastSpaceNum;
        int length = inputCursor + firstSpaceNum;

        int displayCursor = 0;
        for (int i = 0; i < inputCursor; i++) {
            displayCursor++;
            if (displayString.charAt(i) == ' ') {
                displayCursor++;
            }
        }
        for (int i = inputCursor; i < length; i++) {
            if (displayString.charAt(i) == ' ') {
                displayCursor++;
            }
        }
        return displayCursor;
    }
}
