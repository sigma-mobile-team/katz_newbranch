
package sigma.qilex.ui.activity;

import java.io.IOException;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.LocationInfo;
import sigma.qilex.manager.chat.ImageThumbnailProcessor;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;

public class LocationActivity extends BaseActivity
        implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    public final static String BUNDLE_LOCALTION_INPUT = "start_input_location";

    SupportMapFragment mapFragment;

    LatLng mCurrentLatLng;

    GoogleMap mGoogleMap;

    Geocoder geocoder;

    private LocationRequest mLocationRequest;

    private GoogleApiClient mGoogleApiClient;

    private ImageView mImgMarker;

    LayoutInflater layoutInflater;

    String mCurrentAddress;

    PopupWindow popupWindow;

    ProgressBar progressBar;

    TextView contentAddress;

    private boolean mIsModeView = false;

    private Marker mMarker;

    // use when check GPS to resume activity;
    private boolean forceRequest = false;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.layout_location);
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        setActionBarType3Action(actionCancelLeft, actionSendRight);
        setAtionbarRight3Title(R.string.send);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            checkPermission(REQUEST_LOCATION_CODE);
        }
    }

    private void init() {
        Intent intent = getIntent();
        if (intent != null) {
            mCurrentLatLng = (LatLng)intent.getExtras().get(BUNDLE_LOCALTION_INPUT);
            if (mCurrentLatLng != null) {
                mIsModeView = true;
            }
        }
        geocoder = new Geocoder(this);
        mapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mImgMarker = (ImageView)findViewById(R.id.img_marker);
        layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        mImgMarker.setOnClickListener(mMarkerClick);
        // mapFragment.getMapAsync(this);
        mGoogleMap = mapFragment.getMap();
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setOnCameraChangeListener(mOnCameraListener);
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000) // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        if (mIsModeView) {
            mImgMarker.setVisibility(View.GONE);
            actionbarRight3.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_LOCATION_CODE)
            init();
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_CODE) {
            if (QilexUtils.verifyAllPermissions(grantResults)) {
                init();
                checkGPS();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    private void initPopup(View v, String message) {
        if (popupWindow == null) {
            View popupView = layoutInflater.inflate(R.layout.location_popup, null);
            popupWindow = new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            popupWindow.setOutsideTouchable(true);
            progressBar = (ProgressBar)popupView.findViewById(R.id.progress_bar);
            contentAddress = (TextView)popupView.findViewById(R.id.title);
        } else {
            popupWindow.dismiss();
        }
        if (TextUtils.isEmpty(message)) {
            contentAddress.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            contentAddress.setText("");
        } else {
            contentAddress.setText(message);
            progressBar.setVisibility(View.GONE);
            contentAddress.setVisibility(View.VISIBLE);
        }
        int offsetY = getResources().getInteger(R.integer.pop_marker_padding_bottom) + v.getHeight() / 2;
        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0 - offsetY);
    }

    OnClickListener mMarkerClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            initPopup(v, mCurrentAddress);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (QilexUtils.hasSelfPermission(this, LOCATION_PERMISSIONS)) {
            checkGPS();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (popupWindow != null) {
            popupWindow.dismiss();
            popupWindow = null;
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private void handleNewLocation(LatLng latlng) {
        mCurrentLatLng = latlng;
        mCurrentAddress = getAddress(mCurrentLatLng);
        if (!mIsModeView)
            initPopup(mImgMarker, mCurrentAddress);
        if (mGoogleMap != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLatLng, 16.0f)); // upto_21
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        handleNewLocation(latLng);
    }

    SnapshotReadyCallback callback = new SnapshotReadyCallback() {

        @Override
        public void onSnapshotReady(Bitmap snapshot) {
            LocationInfo info = new LocationInfo(mCurrentLatLng, mCurrentAddress);
            // Generate to square
            snapshot = ImageThumbnailProcessor.extractThumbnail_3x2(snapshot);
            byte[] snaps = null;
            if (snapshot != null) {
                snaps = ImageThumbnailProcessor.getBytes(snapshot);
            }
            resultOK(info, snaps);
        }
    };

    private void resultOK(LocationInfo info, byte[] snapshot) {
        Intent intent = new Intent();
        intent.putExtra("your_location_snapshot", info);
        intent.putExtra("location_snapshot", snapshot);
        setResult(RESULT_OK, intent);
        finish();
    }

    OnClickListener actionSendRight = new OnClickListener() {
        @Override
        public void onClick(View v) {
            showSimpleProgress();
            mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
            mGoogleMap.setMyLocationEnabled(false);
            postHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mGoogleMap.snapshot(callback);
                }
            }, 1000);
        }

    };

    OnClickListener actionCancelLeft = new OnClickListener() {

        @Override
        public void onClick(View v) {
            setResult(RESULT_CANCELED);
            finish();
        }
    };

    OnCameraChangeListener mOnCameraListener = new OnCameraChangeListener() {

        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            if (mIsModeView) {
                mCurrentAddress = getAddress(mCurrentLatLng);
                mMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentLatLng).title(mCurrentAddress)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
                mMarker.showInfoWindow();
            } else {
                mCurrentLatLng = cameraPosition.target;
                mCurrentAddress = getAddress(mCurrentLatLng);
                initPopup(mImgMarker, mCurrentAddress);
            }

        }
    };

    private String getAddress(LatLng target) {
        try {
            List<Address> addresses = geocoder.getFromLocation(mCurrentLatLng.latitude, mCurrentLatLng.longitude, 3);
            if (!addresses.isEmpty()) {
                StringBuilder builder = new StringBuilder(addresses.get(0).getAddressLine(0));
                if (builder.toString().length() < 12)
                    for (int i = 1; i < addresses.size(); i++) {
                        builder.append(", ").append(addresses.get(i).getAddressLine(0));
                    }
                return builder.toString();
            } else {
                return "";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            LogUtils.e("NAMND", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mCurrentLatLng == null || forceRequest) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                onLocationChanged(location);
            }
            forceRequest = false;
        } else {
            handleNewLocation(mCurrentLatLng);
        }
    }

    @Override
    public void onConnectionSuspended(int spend) {
    }

    /**
     * check GPS enable? and request location
     */
    private void checkGPS() {
        LocationManager service = (LocationManager)getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings
        if (!enabled && !mIsModeView) {
            alertbox(R.string.gps_status, R.string.msg_gps_status);
        } else {
            if (mGoogleApiClient != null)
                mGoogleApiClient.connect();
        }
    }

    /*----------Method to create an AlertBox ------------- */
    protected void alertbox(int titleId, int msgId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LocationActivity.this);
        builder.setMessage(msgId).setCancelable(false).setTitle(titleId)
                .setPositiveButton(R.string.gps_on, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        forceRequest = true;
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                        dialog.cancel();
                    }
                }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // cancel the dialog box
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
