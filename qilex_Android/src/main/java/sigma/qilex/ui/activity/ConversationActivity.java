
package sigma.qilex.ui.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import pl.katz.aero2.Const;
import pl.katz.aero2.Const.Config;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.ChatThreadModel;
import sigma.qilex.dataaccess.model.LocationInfo;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.I_ThreadMessageDispatcher;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.ThreadMessageDispatcher;
import sigma.qilex.manager.account.PresenceManager;
import sigma.qilex.manager.account.PresenceManager.PingStatusListener;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.chat.ChatManager.SipChatComposingListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactChangeListener;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.sip.SipMessage;
import sigma.qilex.ui.adapter.ChatAdapter;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.ui.customview.SoftKeyboardRelativeLayout;
import sigma.qilex.ui.customview.SoftKeyboardRelativeLayout.SoftKeyboardListener;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.DateTimeUtils;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import za.co.immedia.pinnedheaderlistview.PinnedHeaderListView;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshPinnedHeaderListView;

public class ConversationActivity extends BaseActivity
        implements OnClickListener, ChatManager.ChatMessageChangeListener, ContactChangeListener, OnTouchListener,
        PingStatusListener, SipChatComposingListener, OnScrollListener, SoftKeyboardListener {

    public static final String PREF_DRAFT_MESSAGE = "prefs_key_draft_message_%d";

    public static final int REQUEST_VIEW = 505;

    public static final int REQUEST_LOCATION = 507;

    public static final int REQUEST_CAMERA = 509;

    public static final int REQUEST_GALLERY = 510;

    public static final int REQUEST_SETTING = 511;

    public static final String TAG = ConversationActivity.class.getSimpleName();

    public static final String INTENT_PARAM_THREAD_ID = "INTENT_PARAM_THREAD_ID";

    public static final String INTENT_PARAM_CONTACT = "INTENT_PARAM_CONTACT";

    public static final String INTENT_PARAM_IS_GROUPCHAT = "INTENT_PARAM_IS_GROUPCHAT";

    public static final String INTENT_PARAM_LAUNCH_OPTIONS = "INTENT_PARAM_LAUNCH_OPTIONS";

    public static final String INTENT_PARAM_BROADCAST_ID = "INTENT_PARAM_BROADCAST_BACK_FROM_CHAT";

    public static final String INTENT_PARAM_ONLINE_STATUS = "INTENT_PARAM_ONLINE_STATUS";

    private static final int MSG_TIME_OUT_COMPOSING = 5000;

    private static final int MSG_TIME_OUT_IDLE = 5001;

    public static final String ACTION_RESEND_MESSAGE = "eu.sigma.qilex.RESEND_MESSAGE";

    private String IS_TYPING;
    
    private String ARE_TYPING;
    
    private MyCustomEditText messageET;

    SoftKeyboardRelativeLayout rootContainer;

    private PullToRefreshPinnedHeaderListView messagesContainer;

    private PinnedHeaderListView mPinnerListViewMessage;

    private ImageView sendBtn;

    private ChatAdapter mAdapter;

    private ChatManager mChatManager;

    private long mThreadId;
    
    private ChatThreadModel currentThreadModel;

    private String mStrContact;

    private CallManager mCallManager;

    private ContactManager mContactManager;

    private PresenceManager mPresenceManager;

    // Actionbar controls
    private TextView txtContact;

    private TextView txtContactStatus;

    private TextView txtComposing;

    private TextView txtNewMessage;

    private View layout_send_image;

    View layoutNewMesage;

    LinearLayout layoutTabTakePhoto;

    LinearLayout layoutTabGallery;

    LinearLayout layoutTabLocation;

    private ImageView imgBtnCall;

    private ImageView imgBtnSetting;

    private ImageView chatExpand;

    private View btnBack;

    private ProgressBar loading;

    private boolean mIsCanPull = true;

    private QilexContact mContact;

    private TempContactModel mTempContact;

    private Timer mPingContactTimer;

    private TimerTask mPingContactTimerTask;

    private boolean isComposing = false;

    private int mNewMessage = 0;

    boolean mIsEndCall = false;

    private ThreadMessageDispatcher vrcDispatcher;

    private static Stack<ConversationActivity> mStackActivity = new Stack<ConversationActivity>();

    private boolean isReloadWhenResume = true;

    private boolean isExpanded = false;
    
    // Group Chat Data - END
    private boolean isGroupChat;
    
    private String[] listParticipantPhone;
    
    private String[] listParticipantPhoneExceptYou;
    
    private String administratorPhone;
    
    private HashMap<String, String[]> mappingName;
    
    private boolean isGroupAdmin = false;
    // Group Chat Data - END

    private I_ThreadMessageDispatcher l_listenerDispatcher = new I_ThreadMessageDispatcher() {
        public void onHandleMessage(Message vrpMsg) {
            switch (vrpMsg.what) {
                case MSG_TIME_OUT_COMPOSING:
                    isComposing = false;
                    synchronized (listShowingComposingNumber) {
                    	listShowingComposingNumber.clear();
					}
                    postHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            hideComposing();
                        }
                    });
                    break;
                case MSG_TIME_OUT_IDLE:
                    sendUncomposing();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IS_TYPING = getResources().getString(R.string.msg_composte);
        ARE_TYPING = getResources().getString(R.string.msg_are_composte);
        putToStack(this);
        setContentView(R.layout.activity_chat);
        mChatManager = ChatManager.getInstance();
        mCallManager = CallManager.getInstance();
        mContactManager = ContactManager.getInstance();
        mPresenceManager = PresenceManager.getInstance();

        txtContactStatus = (TextView)findViewById(R.id.actionbar_sub_title4);
        // Get parameters
        boolean isContinue = getDataFromIntent(getIntent());

        initControls();
        showBackAction();

        // Add Action listener
        mChatManager.addChatMessageChangeListener(this);
        mChatManager.addSipChatComposingListener(this);
        mContactManager.addContactChangeListener(this);
        if (isGroupChat == false) {
            mPresenceManager.registerPingStatusListener(this);
        }
        mContactManager.addContactChangeListener(this);
        vrcDispatcher = new ThreadMessageDispatcher(l_listenerDispatcher);
        IntentFilter intentFilter = new IntentFilter(ACTION_RESEND_MESSAGE);
        registerReceiver(mReSendMessageRecev, intentFilter);

        if (Const.IS_OLD_VERSION) {
            findViewById(R.id.chatExpand).setVisibility(View.GONE);
        }
        QilexService service = getService();
        if (service != null) {
            service.setConversationVisible(true);
        }
        if (!isContinue) {
        	finish();
        	return;
        }
    }

    // case activity is already opened.
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean isContinue = getDataFromIntent(intent);
        if (!isContinue) {
        	finish();
        	return;
        }
        initControls();
    }

    @Override
    protected void onStart() {
        super.onStart();
        QilexService service = getService();
        if (service != null) {
            service.setCurrentChattingPhoneNo(mStrContact);
            service.clearIncomMessageNotification();
        }

        NetworkStateMonitor.addListener(mAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        QilexService service = getService();
        if (service != null) {
            service.setCurrentChattingPhoneNo(Const.STR_EMPTY);
        }
        NetworkStateMonitor.removeListener(mAdapter);
    }

    private boolean getDataFromIntent(Intent intent) {
        Bundle b = intent.getExtras();
        mStrContact = b.getString(INTENT_PARAM_CONTACT);
        mIsEndCall = b.getBoolean("from_imcomming", false);
        currentThreadModel = mChatManager.getChatThreadByContactStr(mStrContact);

        // Check if conversation is group chat and not have thread in SqliteDB.
        if (!mStrContact.startsWith("+") && currentThreadModel == null) {
        	return false;
        }
        
        mThreadId = currentThreadModel == null ? -1 : currentThreadModel.id;
        isGroupChat = currentThreadModel == null ? false : currentThreadModel.isGroupChat();
        if (isGroupChat) {
        	parseGroupChatData();
        }
        
        if (!isGroupChat) {
        	String paramStatus = intent.getExtras().getString(INTENT_PARAM_ONLINE_STATUS, null);
            if (paramStatus != null) {
                txtContactStatus.setVisibility(View.VISIBLE);
                txtContactStatus.setText(paramStatus);
            } else {
                txtContactStatus.setVisibility(View.GONE);
            }

            // Check is support number
            if (QilexPhoneNumberUtils.isSupportNumber(mStrContact)) {
                findViewById(R.id.layout_input_data).setVisibility(View.INVISIBLE);
                findViewById(R.id.img_right_actionbar_call4).setVisibility(View.INVISIBLE);
                findViewById(R.id.img_right_actionbar4).setVisibility(View.INVISIBLE);
            }
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeStack(this);
        mChatManager.removeChatMessageChangeListener(this);
        mChatManager.removeSipChatComposingListener(this);
        if (isGroupChat == false) {
        	mPresenceManager.unregisterPingStatusListener(this);
        }
        mContactManager.removeContactChangeListener(this);
        isComposing = false;
        sendUncomposing();
        if (mReSendMessageRecev != null) {
            unregisterReceiver(mReSendMessageRecev);
        }
        rootContainer.removeSoftKeyboardListener(this);
        QilexService service = getService();
        if (service != null) {
            service.setConversationVisible(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isGroupChat) {
        	stopTimerPingContact();
        }
        hideKeyboard();
        String draftMessage = messageET.getText().toString().trim();
        SharedPrefrerenceFactory.getInstance().putString(String.format(PREF_DRAFT_MESSAGE, mThreadId), draftMessage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isGroupChat) {
            startTimerPingContact();
        }
        String drafMessage = SharedPrefrerenceFactory.getInstance()
                .getString(String.format(PREF_DRAFT_MESSAGE, mThreadId));
        if (!drafMessage.isEmpty()) {
            messageET.setText(drafMessage);
            messageET.setSelection(messageET.getText().length());
        }
        if (mAdapter != null && isReloadWhenResume) {
        	if (isGroupChat) {
            	parseGroupChatData();
        	}
            reloadActionBarContent();
            mAdapter.reGenerateFromModel();
            isReloadWhenResume = false;
        } else {
        	// In group chat mode, sometimes app crash when run to this line
//            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void startTimerPingContact() {
        mPingContactTimerTask = new TimerTask() {
            @Override
            public void run() {
                pingContactStatus();
            }
        };
        mPingContactTimer = new Timer();
        mPingContactTimer.schedule(mPingContactTimerTask, 1000, Config.TIME_MILLIS_DELAY_PING_CONTACT_STATUS);
    }

    private void stopTimerPingContact() {
        if (mPingContactTimerTask != null) {
            mPingContactTimer.cancel();
            mPingContactTimerTask.cancel();
        }
    }

    private void initControls() {
        setActionBarType(ACTIONBAR_TYPE_CHAT_DETAIL);
        rootContainer = (SoftKeyboardRelativeLayout)findViewById(R.id.container);
        messagesContainer = (PullToRefreshPinnedHeaderListView)findViewById(R.id.messagesContainer);
        mPinnerListViewMessage = messagesContainer.getRefreshableView();
        mPinnerListViewMessage.setLongClickable(true);
        messageET = (MyCustomEditText)findViewById(R.id.messageEdit);
        sendBtn = (ImageView)findViewById(R.id.chatSendButton);
        txtComposing = (TextView)findViewById(R.id.txtComposing);
        chatExpand = (ImageView)findViewById(R.id.chatExpand);
        layout_send_image = findViewById(R.id.layout_send_image);

        txtContact = (TextView)findViewById(R.id.actionbar_title4);
        txtContactStatus = (TextView)findViewById(R.id.actionbar_sub_title4);
        imgBtnCall = (ImageView)findViewById(R.id.img_right_actionbar_call4);
        imgBtnSetting = (ImageView)findViewById(R.id.img_right_actionbar4);
        btnBack = findViewById(R.id.layoutMenuLeft4);
        loading = (ProgressBar)findViewById(R.id.loading);
        txtNewMessage = (TextView)findViewById(R.id.txtNewMessage);
        layoutNewMesage = findViewById(R.id.layoutNewMesage);
        layoutTabTakePhoto = (LinearLayout)findViewById(R.id.tab_take_photo);
        layoutTabGallery = (LinearLayout)findViewById(R.id.tab_image_gallery);
        layoutTabLocation = (LinearLayout)findViewById(R.id.tab_location);

        // Init values
        imgBtnCall.setImageResource(R.drawable.bg_btn_chat_call);
        imgBtnSetting.setImageResource(R.drawable.bg_btn_chat_setting);
        boolean empty = Utils.isStringNullOrEmpty(messageET.getText().toString());
        sendBtn.setEnabled(!empty);
        reloadActionBarContent();

        // Set action listener
        sendBtn.setOnClickListener(this);
        imgBtnSetting.setOnClickListener(this);
        imgBtnCall.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        layoutTabTakePhoto.setOnClickListener(this);
        layoutTabGallery.setOnClickListener(this);
        layoutTabLocation.setOnClickListener(this);
        chatExpand.setOnClickListener(this);
        rootContainer.addSoftKeyboardListener(this);
        messagesContainer.setOnTouchListener(this);
        messageET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean empty = Utils.isStringNullOrEmpty(s.toString());
                sendBtn.setEnabled(!empty);

                // Send composing message
                if (empty == false && isComposing == false) {
                    sendComposing();
                } else if (empty == true) {
                    sendUncomposing();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // not setup
            }

            @Override
            public void afterTextChanged(Editable s) {
                // not setup
            }
        });

        mAdapter = new ChatAdapter(this, mChatManager, false);
        messagesContainer.setAdapter(mAdapter);
        loadChatFromSqlite(0);
        if (mThreadId == -1l)
            showKeyboard(messageET);
        // Set a listener to be invoked when the list should be refreshed.
        messagesContainer.setOnRefreshListener(new OnRefreshListener2<PinnedHeaderListView>() {

            @Override
            public void onPullDownToRefresh(PullToRefreshBase<PinnedHeaderListView> refreshView) {
                if (mIsCanPull) {
                    postHandler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            loadChatFromSqlite(mAdapter.getListChatMsg().size());
                        }
                    }, 500);
                } else {
                    loadChatFromSqlite(mAdapter.getListChatMsg().size());
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<PinnedHeaderListView> refreshView) {
                messagesContainer.onRefreshComplete();
            }
        });

        messagesContainer.setOnScrollListener(this);
        layoutNewMesage.setOnClickListener(this);
    }

    private void sendComposing() {
        isComposing = true;
        String sipAddress = mStrContact;
        mChatManager.sendComposingMessage(sipAddress, true, isGroupChat);

        // Send delay uncomposing
        vrcDispatcher.removeMessages(MSG_TIME_OUT_IDLE);
        vrcDispatcher.sendEmptyMessageDelayed(MSG_TIME_OUT_IDLE, Config.TIME_MILLIS_DELAY_SEND_IDLE);
    }

    private void sendUncomposing() {
        isComposing = false;
        String sipAddress = mStrContact;
        mChatManager.sendComposingMessage(sipAddress, false, isGroupChat);
    }

    private void pingContactStatus() {
        Utils.globalQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (mContact == null) {
                    mPresenceManager.pingToAdrress(mStrContact);
                } else {
                    PhoneNumberModel phone = mContact.getFirstKATZNumber();
                    if (phone != null) {
                        mPresenceManager.pingToAdrress(phone.phoneNumber);
                    }
                }
            }
        });
    }

    public void displayMessage(final ChatMessageModel message) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.add(message);
                mAdapter.notifyDataSetChanged();
                mAdapter.isAnimateLastItem = true;
            }
        });
    }

    public void displayMessages(final ArrayList<ChatMessageModel> message) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                for (ChatMessageModel chatMessageModel : message) {
                    mAdapter.add(chatMessageModel);
                }
                mAdapter.notifyDataSetChanged();
                mAdapter.isAnimateLastItem = true;
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hideKeyboard();
        return super.onTouchEvent(event);
    }

    private void scroll() {
        // mPinnerListViewMessage.setSelection(mPinnerListViewMessage.getCount()
        // - 1);
        mPinnerListViewMessage.smoothScrollToPosition(mPinnerListViewMessage.getCount() - 1);
    }

    private void reloadActionBarContent() {
    	if (isGroupChat) {
    		findViewById(R.id.img_right_actionbar_call4).setVisibility(View.GONE);
    		txtContact.setText(R.string.group);

    		if (listParticipantPhone.length > 1) {
    			ArrayList<String> listDisplay = new ArrayList<>();
    			ArrayList<String> listDisplayPhone = new ArrayList<>();
        		for (String phone : listParticipantPhone) {
    				if (!phone.equals(UserInfo.getInstance().getPhoneNumberFormatted())) {
    					String[]contactDatas = mappingName.get(phone);
    					if (contactDatas != null) {
        					listDisplay.add(contactDatas[0]);
    					} else {
    						listDisplay.add(phone);
    					}
    					listDisplayPhone.add(phone);
    				}
    			}
        		listParticipantPhoneExceptYou = new String[listDisplay.size()];
        		String strListContact = TextUtils.join(", ", listDisplay.toArray(new String[listDisplay.size()]));
        		listParticipantPhoneExceptYou = listDisplayPhone.toArray(listParticipantPhoneExceptYou);
        		txtContactStatus.setText(strListContact);
    		} else {
    			txtContactStatus.setText(R.string.none_participant);
    		}
    		
    	} else {
    		findViewById(R.id.img_right_actionbar_call4).setVisibility(View.VISIBLE);
            if (mContact == null) {
                txtContact.setText(mStrContact);
            } else {
                txtContact.setText(mContact.getDisplayName());
            }
            loadContactInfo();
    	}
    }

    private void loadContactInfo() {
        AsyncTask<String, Integer, QilexContact> task = new AsyncTask<String, Integer, QilexContact>() {
            @Override
            protected QilexContact doInBackground(String... params) {
                return mContactManager.getContactByPhoneNoFromPhoneBook(ConversationActivity.this, mStrContact);
            }

            @Override
            protected void onPostExecute(QilexContact result) {
                mContact = result;
                if (mContact == null) {
                    mTempContact = mContactManager.getTempContact(mStrContact);
                    if (mTempContact != null) {
                        txtContact.setText(mTempContact.name);
                    } else {
                        txtContact.setText(mStrContact);
                    }
                } else {
                    txtContact.setText(mContact.getDisplayName());
                }
            }
        };
        task.execute();
    }

    private synchronized void loadChatFromSqlite(int offset) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(View.GONE);
            }
        });
        mChatManager.loadChatMessageFromThread(mThreadId, offset, Const.Config.NUMBER_CHAT_ITEM_LOAD_EACH_SCROLL,
                new ChatManager.LoadChatMessageListener() {

                    @Override
                    public void onLoadChatMessageStart() {
                    }

                    @Override
                    public void onLoadChatMessageFinish(ArrayList<ChatMessageModel> listResult) {
                        messagesContainer.onRefreshComplete();
                        if (listResult == null || listResult.size() == 0) {
                            mIsCanPull = false;
                            return;
                        } else {
                            mIsCanPull = true;
                        }

                        int currentSize = mAdapter.getSectionCount();
                        // Change to read
                        mChatManager.markAllRead(mThreadId);
                        Collections.sort(listResult, new ChatMessageModel.DateComparator());

                        mAdapter.add(listResult);
                        mAdapter.setListChatModel(null);
                        mAdapter.notifyDataSetChanged();
                        int sectionAfterCount = mAdapter.getSectionCount();
                        // Restore the position, add more 2 because it has
                        // section header
                        int pos = listResult.size() + sectionAfterCount - currentSize + 2;
                        mPinnerListViewMessage.setSelectionFromTop(pos, mAdapter.sectionHeight);
                        loading.setVisibility(View.GONE);

                        if (currentThreadModel != null) {
                        	mChatManager.markAllRead(currentThreadModel.id);
                        }
                    }
                });
    }

    @Override
    public void onNewMessage(final ChatMessageModel ...newMsg) {
        // Hide composing
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                hideComposing();
            }
        });
        // Just handler event of receiving message
        postHandler.post(new Runnable() {
            @Override
            public void run() {
            	if (isGroupChat != newMsg[0].isGroupChat()) {
            		return;
            	}
            	
            	boolean hasNewMsg = false;
            	boolean updateTitle = false;
                for (ChatMessageModel chatMessageModel : newMsg) {
                	boolean isMessageOfThisConversation = false;
                	if (isGroupChat && mStrContact.equals(chatMessageModel.groupUuid)) {
                		isMessageOfThisConversation = true;
                	} else if (!isGroupChat && mStrContact.equals(chatMessageModel.contactStr)) {
                		isMessageOfThisConversation = true;
                	}
                	if (chatMessageModel.type == ChatDatabase.MSG_TYPE_GROUP_STATUS) {
                		updateTitle = true;
                	}
            		if ((chatMessageModel.type == ChatDatabase.MSG_TYPE_RECEIVE
            				|| chatMessageModel.type == ChatDatabase.MSG_TYPE_GROUP_STATUS)
                    		&& isMessageOfThisConversation) {
                        mNewMessage++;
                        hasNewMsg = true;
                        mAdapter.add(chatMessageModel);
                        mAdapter.isAnimateLastItem = true;
                    }
        		}
                mAdapter.notifyDataSetChanged();

                // Put new message
                if (hasNewMsg) {
                    layoutNewMesage.setVisibility(View.VISIBLE);
                    txtNewMessage.setText("" + mNewMessage);
                    Animation shake = AnimationUtils.loadAnimation(ConversationActivity.this, R.anim.shake);
                    layoutNewMesage.startAnimation(shake);
                    
                    if (newMsg[0].type == ChatDatabase.MSG_TYPE_GROUP_STATUS) {
                    	mChatManager.markAllMsgStatusRead(mThreadId);
                    }
                }
                if (updateTitle) {
                    currentThreadModel = mChatManager.getChatThreadByContactStr(mStrContact);
                    parseGroupChatData();
                    reloadActionBarContent();
                }
            }
        });
    }

    @Override
    public synchronized void onMessageStatusUpdate(ArrayList<ChatMessageModel> models) {
        for (ChatMessageModel chatMessageModel : models) {
            if (chatMessageModel.status == ChatDatabase.MSG_STATUS_SEND_VIEWED) {
                mAdapter.updateItemStatusToViewed(chatMessageModel.id, chatMessageModel);
            } else {
                mAdapter.updateItem(chatMessageModel.id, chatMessageModel);
            }
        }
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onUnreadMessageChanged(int unreadThread, int unreadMesage) {
    }

    @Override
    public void setBackAction(boolean visible) {
        if (visible) {
            actionbarLeftIcon.setVisibility(View.VISIBLE);
            backLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleBackLogic();
                }
            });
        } else {
            actionbarLeftIcon.setVisibility(View.GONE);
        }
    }

    boolean isOpened = false;

    @Override
    public void onBackPressed() {
        if (isExpanded) {
            toogleChatExpand();
            return;
        }
        handleBackLogic();
    }

    public void handleBackLogic() {
        if (isTaskRoot()) {
            // lauch MainTabActivity with tab chat
            Intent intent = new Intent(ConversationActivity.this, MainTabActivity.class);
            intent.putExtra(ConversationActivity.INTENT_PARAM_LAUNCH_OPTIONS, true);
            ConversationActivity.this.startActivity(intent);
            finish();
        } else {
            // broadcast that we clicked back button
            Intent intent = new Intent();
            intent.setAction(ConversationActivity.INTENT_PARAM_BROADCAST_ID);
            sendBroadcast(intent);

            finish();
        }
    }

    private void eventButtonCallClick() {
        String sipAddress = Utils.getKATZNumber(mStrContact);
        mCallManager.initOutGoingSipCall(this, sipAddress);
    }

    private void eventButtonSettingClick() {
        Intent intent = new Intent(ConversationActivity.this, ConversationSettingActivity.class);
        String[] numbers;
        if (isGroupChat) {
        	numbers = listParticipantPhoneExceptYou;
        } else {
        	numbers = new String[] {
                    mStrContact
            };
        }
        
        intent.putExtra("contact_list_numbers", numbers);
        intent.putExtra(INTENT_PARAM_ONLINE_STATUS, txtContactStatus.getText());
        intent.putExtra(ConversationSettingActivity.INTENT_PARAM_THREAD_ID, mThreadId);
        if (isGroupChat) {
            intent.putExtra(ConversationSettingActivity.INTENT_PARAM_GROUP_CHAT_UUID, currentThreadModel.contactStr);
            intent.putExtra(ConversationSettingActivity.INTENT_PARAM_IS_ADMIN, isGroupAdmin);
            intent.putExtra(ConversationSettingActivity.INTENT_PARAM_ADMIN_PHONE, administratorPhone);
        }
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        startActivityForResult(intent, REQUEST_SETTING);
    }

    private void eventButtonSend() {
        String messageText = messageET.getText().toString();
        if (TextUtils.isEmpty(messageText)) {
            return;
        }

        messageET.setText(Const.STR_EMPTY);
        if (mIsEndCall) {
            CallManager.getInstance().endCurrentSipCall();
        }
        // Send Sip message
        String sipAddress = mStrContact;
        SipMessage sipMesage = mChatManager.sendSipMessage(-1, sipAddress, messageText, null, isGroupChat);
        
        if (sipMesage.getFrom() == null) {
            showAlert(R.string.cannot_send_message, R.string.msg_call_no_network_summary);
        }

        // Display message on screen
        ChatMessageModel chatMessage = mChatManager.getChatMessageById(sipMesage.getId());
        displayMessage(chatMessage);
    }

    BroadcastReceiver mReSendMessageRecev = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long msgId = intent.getExtras().getLong("id", 0);
            ArrayList<Long> listIds = new ArrayList<>();
            listIds.add(msgId);
            mAdapter.removeMessage(listIds);
            ChatMessageModel chatMessage = mChatManager.resendMessage(msgId);
            displayMessage(chatMessage);
        }

    };

    private void showComposing() {
        String userName;
        if (mContact != null) {
            userName = mContact.getDisplayName();
        } else if (mTempContact != null) {
            userName = mTempContact.name;
        } else {
            userName = mStrContact;
        }

        txtComposing.setText(getResources().getString(R.string.msg_composte, userName));
        ObjectAnimator anim = ObjectAnimator.ofFloat(txtComposing, "alpha", 0.7f);
        anim.setDuration(100);
        anim.start();
    }

    private void hideComposing() {
        ObjectAnimator anim = ObjectAnimator.ofFloat(txtComposing, "alpha", 0f);
        anim.setDuration(50);
        anim.start();
    }
    
    ArrayList<String> listShowingComposingNumber = new ArrayList<>();
    
    private synchronized void showGroupChatComposing() {
    	synchronized (listShowingComposingNumber) {
        	if (listShowingComposingNumber.size() > 2) {
        		txtComposing.setText(getResources().getString(
        				R.string.msg_are_composte, Integer.toString(listShowingComposingNumber.size())));
                ObjectAnimator anim = ObjectAnimator.ofFloat(txtComposing, "alpha", 0.7f);
                anim.setDuration(100);
                anim.start();
                vrcDispatcher.removeMessages(MSG_TIME_OUT_COMPOSING);
                vrcDispatcher.sendEmptyMessageDelayed(MSG_TIME_OUT_COMPOSING, 10000);
        	} else if (listShowingComposingNumber.size() == 2) {
        		String []dataName1 = mappingName.get(listShowingComposingNumber.get(0));
        		String []dataName2 = mappingName.get(listShowingComposingNumber.get(1));
        		String name1 = dataName1 == null ? listShowingComposingNumber.get(0) : dataName1[0];
        		String name2 = dataName2 == null ? listShowingComposingNumber.get(1) : dataName2[0];
        		String names = name1 + " " + getResources().getString(
        				R.string.and) + " " + name2;
        		txtComposing.setText(String.format(ARE_TYPING, names));
                vrcDispatcher.removeMessages(MSG_TIME_OUT_COMPOSING);
                vrcDispatcher.sendEmptyMessageDelayed(MSG_TIME_OUT_COMPOSING, 10000);
        	} else if (listShowingComposingNumber.size() == 1) {
        		String []dataName1 = mappingName.get(listShowingComposingNumber.get(0));
        		String name1 = dataName1 == null ? listShowingComposingNumber.get(0) : dataName1[0];
        		txtComposing.setText(getResources().getString(R.string.msg_composte, name1));
                ObjectAnimator anim = ObjectAnimator.ofFloat(txtComposing, "alpha", 0.7f);
                anim.setDuration(100);
                anim.start();
                vrcDispatcher.removeMessages(MSG_TIME_OUT_COMPOSING);
                vrcDispatcher.sendEmptyMessageDelayed(MSG_TIME_OUT_COMPOSING, 10000);
        	} else {
        		hideComposing();
                vrcDispatcher.removeMessages(MSG_TIME_OUT_COMPOSING);
        	}
		}
    }
    
    private void parseGroupChatData() {
		listParticipantPhone = currentThreadModel.getListPhoneNumberParticipants();
		listParticipantPhoneExceptYou = currentThreadModel.getListPhoneNumberParticipantExceptYou();
    	administratorPhone = currentThreadModel.getPhoneNumberAdmin();
    	isGroupAdmin = UserInfo.getInstance().getPhoneNumberFormatted().equals(administratorPhone);
    	
    	mappingName = new HashMap<>();
    	for (String phone : listParticipantPhone) {
			String[] nameAndAvatar = mContactManager.getContactNameAndImageByPhoneNo(phone);
			mappingName.put(phone, nameAndAvatar);
		}

    	// Check if you are in group
    	if (currentThreadModel.isYouInGroupChat()) {
    		findViewById(R.id.messageEdit).setEnabled(true);
    		findViewById(R.id.chatExpand).setEnabled(true);
    		findViewById(R.id.img_right_actionbar4).setEnabled(true);
    	} else {
    		findViewById(R.id.messageEdit).setEnabled(false);
    		findViewById(R.id.chatExpand).setEnabled(false);
    		findViewById(R.id.chatSendButton).setEnabled(false);
    		findViewById(R.id.img_right_actionbar4).setEnabled(false);
    		findViewById(R.id.layout_send_image).setVisibility(View.GONE);
    		findViewById(R.id.chatExpand).setSelected(false);
            isExpanded = false;
    	}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_right_actionbar_call4:
                eventButtonCallClick();
                break;
            case R.id.img_right_actionbar4:
                eventButtonSettingClick();
                break;
            case R.id.chatSendButton:
                eventButtonSend();
                break;
            case R.id.layoutMenuLeft4:
                finish();
                break;
            case R.id.layoutNewMesage:
                scroll();
                break;
            case R.id.tab_take_photo:
                // REQUEST_CAMERA
                Intent intentCamera = new Intent("eu.sigma.qilex.FRIFON_CAMERA");
                intentCamera.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                        ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                startActivityForResult(intentCamera, REQUEST_CAMERA);
                break;
            case R.id.tab_image_gallery:
                // Open gallery
                Intent intent = new Intent(this, ListImageActivity.class);
                intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                startActivityForResult(intent, REQUEST_GALLERY);
                break;
            case R.id.tab_location:
                Intent mapsIntent = new Intent("eu.sigma.qilex.FRIFON_LOCATION");
                mapsIntent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                        ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                startActivityForResult(mapsIntent, REQUEST_LOCATION);
                break;
            case R.id.chatExpand:
                toogleChatExpand();
                break;
            default:
                break;
        }
    }

    @Override
    public void onSoftKeyboardShow() {
        if (isExpanded)
            toogleChatExpand();
    }

    @Override
    public void onSoftKeyboardHide() {
    }

    private void toogleChatExpand() {
        isExpanded = !isExpanded;
        if (isExpanded) {
            hideKeyboard();
            layout_send_image.setVisibility(View.VISIBLE);
            chatExpand.setSelected(true);
        } else {
            layout_send_image.setVisibility(View.GONE);
            chatExpand.setSelected(false);
        }
    }

    @Override
    public void onNewContact(QilexContact newContact) {
        isReloadWhenResume = true;
    }

    @Override
    public void onUpdateContact(QilexContact newContact) {
        isReloadWhenResume = true;
    }

    @Override
    public void onDeleteContact(QilexContact newContact) {
        isReloadWhenResume = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VIEW && resultCode == RESULT_OK) {
            reloadActionBarContent();
            mAdapter.reGenerateFromModel();
        } else if (requestCode == REQUEST_LOCATION && resultCode == RESULT_OK) {
            String sipAddress = mStrContact;
            LocationInfo info = data.getParcelableExtra("your_location_snapshot");
            if (info != null) {
                byte[] snaps = data.getByteArrayExtra("location_snapshot");
                Bitmap bmp = null;
                if (snaps != null) {
                    bmp = BitmapFactory.decodeByteArray(snaps, 0, snaps.length);
                    LogUtils.e("NAMND", "bitmap available, length = " + snaps.length);
                }
                ChatMessageModel chatMessage = mChatManager.sendLocationMessage(sipAddress, info.latitude,
                        info.longitude, bmp, info.address, isGroupChat);
                ArrayList<ChatMessageModel> listNewChat = new ArrayList<>();
                listNewChat.add(chatMessage);
                displayMessages(listNewChat);
            }
        } else if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {
            // Send Image
            String[] imageUrls = data.getExtras().getStringArray(ImageInputCommentActivity.INTENT_LIST_URLS);
            String[] imageDesc = data.getExtras().getStringArray(ImageInputCommentActivity.INTENT_LIST_COMMENT);
            Long[] chatIds = new Long[imageDesc.length];
            String sipAddress = mStrContact;

            ArrayList<ChatMessageModel> listNewChat = new ArrayList<>();
            for (int i = 0; i < imageDesc.length; i++) {
                ChatMessageModel chatMessage = mChatManager.sendImageMessage(sipAddress, imageDesc[i], imageUrls[i], isGroupChat);
                listNewChat.add(chatMessage);
            }
            displayMessages(listNewChat);
        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            // Send Image
            String[] imageUrls = data.getExtras().getStringArray(ImageInputCommentActivity.INTENT_LIST_URLS);
            String[] imageDesc = data.getExtras().getStringArray(ImageInputCommentActivity.INTENT_LIST_COMMENT);
            Long[] chatIds = new Long[imageDesc.length];
            String sipAddress = mStrContact;

            ArrayList<ChatMessageModel> listNewChat = new ArrayList<>();
            for (int i = 0; i < imageDesc.length; i++) {
                ChatMessageModel chatMessage = mChatManager.sendImageMessage(sipAddress, imageDesc[i], imageUrls[i], isGroupChat);
                listNewChat.add(chatMessage);
            }
            displayMessages(listNewChat);
        } else if (requestCode == REQUEST_SETTING) {
        	if (resultCode == RESULT_OK) {
            	if (data != null && data.getExtras() != null) {
                	String newUuid = data.getExtras().getString(ConversationSettingActivity.RESPONSE_NEW_GROUP_UUID, null);
                	if (newUuid != null && !newUuid.equals(mStrContact)) {
                		mStrContact = newUuid;
                        mIsEndCall = false;
                        currentThreadModel = mChatManager.getChatThreadByContactStr(mStrContact);
                        mThreadId = currentThreadModel == null ? -1 : currentThreadModel.id;
                        isGroupChat = currentThreadModel == null ? false : currentThreadModel.isGroupChat();
                        if (isGroupChat) {
                        	parseGroupChatData();
                        }
                        initControls();
                        reloadActionBarContent();
                	}
            	}
        	} else if (resultCode == ConversationSettingActivity.RESPONSE_CODE_LEAVE_GROUP) {
//        		mChatManager.deleteChatThread(mThreadId);
        		finish();
        	}
        }
    }

    @Override
    public void onPingSuccess(String phoneNum) {
        if (phoneNum.equals(mStrContact)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtContactStatus.setText(R.string.status_online);
                    txtContactStatus.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void onPingFail(final boolean isTimeOut, String phoneNum, final int offlineMinute) {
        if (phoneNum.equals(mStrContact)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtContactStatus.setVisibility(View.VISIBLE);
                    if (isTimeOut == false) {
                        String status = getResources().getString(R.string.status_offline);
                        ;
                        if (offlineMinute > 0) {
                            status = getResources().getString(R.string.status_offline_for,
                                    DateTimeUtils.convertMinuteToReadableTime(offlineMinute));
                        }
                        txtContactStatus.setText(status);
                    } else {
                        txtContactStatus.setText(R.string.offline_for_a_moment);
                    }
                }
            });
        }
    }

    @Override
    public void onReceiveRequest(String phoneNum) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSipChatComposing(final String sipAddress, final boolean isTyping,
    		final boolean isGroupChat, final String composingPhoneNumber, String groupUuid) {
        if (isGroupChat == false && mStrContact.equals(sipAddress) == false) {
            return;
        } else if (isGroupChat == true && mStrContact.equals(groupUuid) == false) {
            return;
        }
        postHandler.post(new Runnable() {
            @Override
            public void run() {
            	if (isGroupChat) {
            		if (isTyping) {
            			if (!listShowingComposingNumber.contains(composingPhoneNumber)) {
            				listShowingComposingNumber.add(composingPhoneNumber);
            			}
            		} else {
            			listShowingComposingNumber.remove(composingPhoneNumber);
            		}
            		showGroupChatComposing();
            	} else {
	                if (isTyping) {
	                    showComposing();
	                    vrcDispatcher.removeMessages(MSG_TIME_OUT_COMPOSING);
	                    vrcDispatcher.sendEmptyMessageDelayed(MSG_TIME_OUT_COMPOSING, 10000);
	                } else {
	                    hideComposing();
	                    vrcDispatcher.removeMessages(MSG_TIME_OUT_COMPOSING);
	                }
            	}
            }
        });
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount >= totalItemCount - 1) {
            mNewMessage = 0;
            layoutNewMesage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        
    }

    private static void putToStack(ConversationActivity insertActivity) {
        while (mStackActivity.isEmpty() == false) {
            Activity activity = mStackActivity.remove(0);
            activity.finish();
        }
        mStackActivity.addElement(insertActivity);
    }

    private static void removeStack(ConversationActivity insertActivity) {
        mStackActivity.removeElement(insertActivity);
    }

}
