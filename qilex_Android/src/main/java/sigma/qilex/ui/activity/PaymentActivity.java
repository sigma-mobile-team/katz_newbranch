
package sigma.qilex.ui.activity;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.qilex.frifonout.payment.IPayment;
import com.qilex.frifonout.payment.PaymentImpl;
import com.qilex.frifonout.payment.PaymentListener;
import com.qilex.frifonout.payment.util.Purchase;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.GooglePlayProductListModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.GetProductListListener;
import sigma.qilex.ui.adapter.ProductAdapter;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.Zip;

public class PaymentActivity extends ActionBarActivity
        implements OnItemClickListener, GetProductListListener, PaymentListener {

    public static final int REQUEST_CODE = 401;

    public static final String BUNDLE_PAYMENT = "payment_msg_id";

    ProductAdapter mProductAdapter = null;

    ProgressBar mLyProgress;

    AuthenticationManager mAuthenManager;

    private int currentRequestIdLoadProduct;

    IPayment mPayment;

    MyCustomTextView tvInfo;

    protected ProgressDialog mSimpleProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        mAuthenManager = AuthenticationManager.getInstance();
        mPayment = new PaymentImpl(this);
        mPayment.setPlaymentListener(this);
        ListView lv = (ListView)findViewById(R.id.lv_product);
        mLyProgress = (ProgressBar)findViewById(R.id.pgr_loading);
        tvInfo = (MyCustomTextView)findViewById(R.id.tv_product_error);
        lv.setOnItemClickListener(this);
        mProductAdapter = new ProductAdapter(this);
        mLyProgress.setVisibility(View.VISIBLE);
        tvInfo.setVisibility(View.GONE);
        lv.setAdapter(mProductAdapter);
        currentRequestIdLoadProduct = mAuthenManager.getGooglePlayProductList(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPayment.release();
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
    }

    public void closeButtonClick(View v) {
        QilexHttpRequest.stopRequest(currentRequestIdLoadProduct);
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            showSimpleProgress();
            mPayment.buyManagedConsumable(mProductAdapter.getItem(position).product_google_id);
        } catch (IllegalStateException ie) {
            Toast.makeText(this, R.string.please_sigin_google_play_service, Toast.LENGTH_LONG).show();
        }
    }

    private void addProductList(final ArrayList<GooglePlayProductListModel> listProduct) {
        if (mProductAdapter != null && mLyProgress != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ArrayList<String> skus = getSkus(listProduct);
                    mPayment.setSkus(skus);
                    mProductAdapter.addAll(listProduct);
                    if (!listProduct.isEmpty()) {
                        mLyProgress.setVisibility(View.GONE);
                    }
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mPayment == null)
            return;
        if (!mPayment.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            LogUtils.d("FrifonOutFragment", "onActivityResult handled by IABUtil.");
        }
    }

    public ArrayList<String> getSkus(ArrayList<GooglePlayProductListModel> listProduct) {
        ArrayList<String> skus = new ArrayList<String>();
        for (GooglePlayProductListModel product : listProduct) {
            skus.add(product.product_google_id);
        }
        return skus;
    }

    public ArrayList<String> getSkus(String[] listProduct) {
        ArrayList<String> skus = new ArrayList<String>();
        for (String product : listProduct) {
            skus.add(product);
        }
        return skus;
    }

    @Override
    public void onGetProductListFinish(ErrorModel error, final ArrayList<GooglePlayProductListModel> listProduct) {
        if (listProduct != null) {
            addProductList(listProduct);
            tvInfo.setVisibility(View.GONE);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvInfo.setVisibility(View.VISIBLE);
                    mLyProgress.setVisibility(View.GONE);
                }
            });
        }
    }

    /**
     * Show alert
     * 
     * @param content
     */
    public void showAlert(int titleId, int contentId) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle(titleId);
        // Setting Dialog Message
        alertDialog.setMessage(contentId);
        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onConsumeFinished(Purchase purchase) {
        complain("onConsumeFinished::purchase: " + purchase.toString());
        sendPaymentInfo(purchase);
    }

    Purchase mPurchase;

    private void sendPaymentInfo(Purchase purchase) {
        try {
            mPurchase = purchase;
            // String appStoreInfo = Const.STR_EMPTY;
            // String userData = Const.STR_EMPTY;
            String base64Payment = Zip.encodeBase64(purchase.getOriginalJson().toString());
            String googlePlayProductId = purchase.getSku();
            // String appStoreProductId = Const.STR_EMPTY;

            // Send broadcast
            Intent intent = new Intent(QilexService.BROADCAST_PAYMENT());
            intent.putExtra(QilexService.INTENT_PARAM_PAYMENT_BASE64, base64Payment);
            intent.putExtra(QilexService.INTENT_PARAM_PAYMENT_PRODUCT_ID, googlePlayProductId);
            sendBroadcast(intent);
        } catch (UnsupportedEncodingException e) {
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    private void complain(String s) {
        mPayment.complain("NAMND", s);
    }

    public void showSimpleProgress() {
        // Simple progress bar
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
        mSimpleProgress = new ProgressDialog(this, R.style.QilexProgressBarDialog);
        mSimpleProgress.setCancelable(false);
        mSimpleProgress.show();
        mSimpleProgress.setContentView(R.layout.progress_simple);
    }

    public void hideSimpleProgress() {
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
    }

    @Override
    public void onConsumeFail(Purchase purchase, int errorCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideSimpleProgress();
            }
        });
    }
}
