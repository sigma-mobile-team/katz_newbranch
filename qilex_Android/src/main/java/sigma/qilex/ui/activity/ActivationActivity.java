
package sigma.qilex.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.PhoneCodeModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.LoginResponseModel;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.AuthenticationManager.CheckIsFrifonNumberListener;
import sigma.qilex.manager.account.QilexLoginListener;
import sigma.qilex.manager.account.SendActivationListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.SimUtils;
import sigma.qilex.utils.Utils;

public class ActivationActivity extends BaseActivity
        implements SendActivationListener, QilexLoginListener, CheckIsFrifonNumberListener {

    private String selectedPhoneCode = null;

    public static final int REQUEST_CODE_COUNTRY = 509;

    public static final int REQUEST_CODE_ACCOUNTCONFIG = 600;

    public static final int REQUEST_CODE_ENTERACTIVATION = 601;

    private TextView txtCountryPhoneCode;

    private EditText txtPhoneNo;

    private Button btnContinue;

    String mNumber;

    private AuthenticationManager mAuthenManager;

    private UserInfo mUserInfo;

    private boolean isInputFrifonNumber = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activation_activity);
        setTitle(R.string.activation_title);
        mAuthenManager = AuthenticationManager.getInstance();
        mUserInfo = UserInfo.getInstance();
        // Initialize controls
        txtCountryPhoneCode = (TextView)findViewById(R.id.txtCountryPhoneCode);
        txtPhoneNo = (EditText)findViewById(R.id.txtPhoneNo);
        btnContinue = (Button)findViewById(R.id.btnContinue);
        // Set default selected
        PhoneCodeModel model = QilexPhoneNumberUtils.getCountryPhoneCodeModel("pl");
        selectedPhoneCode = model.phoneCode;
        txtCountryPhoneCode
                .setText(String.format(SelectCountryActivity.FORMAT_COUNTRY, model.countryName, model.phoneCode));

        txtPhoneNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (arg0.toString().trim().length() > 0) {
                    btnContinue.setEnabled(true);
                } else {
                    btnContinue.setEnabled(false);
                }
            }
        });
        showKeyboard(txtPhoneNo);
        ContactManager.getInstance().reloadAllContact();

        if (!Utils.isInKatzMode()) {
            txtPhoneNo.setText(mUserInfo.mCachePhoneNo);
            checkPermission(REQUEST_CALL_CODE);
        }

        // Check SMS Permission
        checkPermission(REQUEST_SMS_CODE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Start QilexService
        bindQilexService();
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_SMS_CODE) {
            if (!QilexUtils.verifyAllPermissions(grantResults)) {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthenManager.unregisterQilexLoginListener(this);
    }

    /**
     * Handler action when click button select country.
     * 
     * @param v
     */
    public void onSelectCountryButtonClick(View v) {
        Intent intentCountry = new Intent(this, SelectCountryActivity.class);
        intentCountry.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        startActivityForResult(intentCountry, REQUEST_CODE_COUNTRY);
    }

    /**
     * Handler action when click button Continue.
     * 
     * @param v
     */
    public void onContinueBtnClick(View v) {
        hideKeyboard();
        // Validate GSM
        if (!Utils.isInKatzMode() && Utils.isStringNullOrEmpty(SimUtils.getIMSI())) {
            showAlert(R.string.error, R.string.need_gsm_connectivity);
            return;
        }

        // Validate input
        String inputPhoneNo = ((MyCustomEditText)txtPhoneNo).getText().toString();
        if (inputPhoneNo.length() < 2 || inputPhoneNo.length() > 15) {
            showAlert(R.string.error, R.string.invalid_phone_number);
            return;
        }
        mNumber = txtPhoneNo.getText().toString();

        mUserInfo.setCountryCode(selectedPhoneCode);
        mUserInfo.setPhoneNumber(mNumber);

        mUserInfo.setCountryCode(selectedPhoneCode);
        mUserInfo.setPhoneNumber(mNumber);
        // mAuthenManager.requestCheckFrifonNumber(mUserInfo.getPhoneNumberFormatted(),
        // this);
        mAuthenManager.sendActivationPhone(mUserInfo.getPhoneNumberFormatted(), Utils.isInMasterMode(), this);
        showSimpleProgress();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 2. Start AccountConfigurationActivity
        if (requestCode == REQUEST_CODE_COUNTRY && resultCode == RESULT_OK) {
            // Back from SelectCountry screen
            String displayCountry = data.getExtras().getString(SelectCountryActivity.INTENT_PARAM_COUNTRY);
            selectedPhoneCode = data.getExtras().getString(SelectCountryActivity.INTENT_PARAM_PHONECODE);

            // Display on view
            txtCountryPhoneCode.setText(displayCountry);
            showKeyboard(txtPhoneNo);
        }
        if (requestCode == REQUEST_CODE_ENTERACTIVATION && resultCode == RESULT_OK) {
            if ("000000".equals(mUserInfo.getActivationCode())) {
                mAuthenManager.registerQilexLoginListener(ActivationActivity.this);
                mAuthenManager.loginWithPhoneNumber(mUserInfo.getPhoneNumberFormatted());
            } else if (isInputFrifonNumber == false) {
                // Back from screen Input Activation Code
                Intent intent = new Intent("sigma.intent.action.FRIFON_ACCOUNTCONFIGURATION");
                startActivityForResult(intent, REQUEST_CODE_ACCOUNTCONFIG);
            } else {
                setResult(RESULT_OK);
                finish();
            }
        }
        if (requestCode == REQUEST_CODE_ACCOUNTCONFIG) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private String extractDigit(String input) {
        return input.replaceAll("\\D+", "");
    }

    @Override
    public void onSendActivationFinish(final boolean isSuccess, final boolean isFrifon, final ErrorModel error) {
        isInputFrifonNumber = isFrifon;
        if (isSuccess) {
            postHandler.post(new Runnable() {
                @Override
                public void run() {
                    hideSimpleProgress();
                    requestEnterActivationCode(false);
                }
            });
        } else if (error != null) {
            // this user has log in
            if (error.getByErrorCode(124) != null) {
                postHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        requestEnterActivationCode(true);
                    }
                });

            } else if (error.getByErrorCode(134) != null) {
                // for slave
                postHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        hideSimpleProgress();
                        Intent intent = new Intent(ActivationActivity.this, ActivationTabletActivity.class);
                        intent.putExtra(ActivationTabletActivity.INTENT_PHONE_NUM_INVALID, mNumber);
                        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                                ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                        startActivity(intent);
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideSimpleProgress();
                        showAlert(R.string.activation_fail, error.errors[0].getCurrentErrorMessage());
                    }
                });
            }
        }
    }

    private void requestEnterActivationCode(boolean ignore) {
        Intent intentActivation = new Intent(ActivationActivity.this, EnterActivationActivity.class);
        intentActivation.putExtra("number_activation",
                extractDigit(txtCountryPhoneCode.getText().toString()) + mNumber);
        intentActivation.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        intentActivation.putExtra("ignore_code", ignore);
        startActivityForResult(intentActivation, REQUEST_CODE_ENTERACTIVATION);
    }

    @Override
    public void onLoginSuccess(LoginResponseModel model) {
        mAuthenManager.unregisterQilexLoginListener(ActivationActivity.this);
        mUserInfo.setActivationCode("000000");
        mUserInfo.setAvatarUrl(model.user_avatar_uri);
        mUserInfo.setFirstName(model.user_name);
        mUserInfo.setLastName(model.user_last_name);
        mUserInfo.saveInfo();
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                hideSimpleProgress();
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public void onLoginFail(int errorCode, String errorMessage) {
        mAuthenManager.unregisterQilexLoginListener(ActivationActivity.this);
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                hideSimpleProgress();
                showAlert(R.string.error, R.string.login_fail);
            }
        });
    }

    @Override
    public void onCheckIsFrifonNumberFinish(int errorCode, boolean result) {
        isInputFrifonNumber = result;
        mAuthenManager.sendActivationPhone(mUserInfo.getPhoneNumberFormatted(), Utils.isInMasterMode(), this);
    }
}
