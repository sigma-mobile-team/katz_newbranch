
package sigma.qilex.ui.activity;

import java.util.Stack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import pl.frifon.aero2.R;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.tab.AccountFragment;

public class AccountActivity extends BaseActivity {

    private Stack<Fragment> mStacks;

    AccountFragment fragment;

    boolean isListenerResult = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list_activity);
        setActionbarTitle(R.string.account);
        showBackAction();
        if (getIntent().getExtras() != null) {
            isListenerResult = getIntent().getExtras().getBoolean("IS_LISTENER_RESULT");
        }
        mStacks = new Stack<Fragment>();
        if (savedInstanceState == null) {
            fragment = new AccountFragment();
            pushFragments(fragment, false, true);
        }
    }

    public void pushFragments(Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    public void popFragments() {
        /*
         * Select the second last fragment in current tab's stack.. which will
         * be shown after the fragment transaction given below
         */
        Fragment fragment = mStacks.elementAt(mStacks.size() - 2);

        /* pop current fragment from stack.. */
        mStacks.pop();

        /*
         * We have the target fragment in hand.. Just show it.. Show a standard
         * navigation animation
         */
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (((BaseFragment)mStacks.lastElement()).onBackPressed() == false) {
            /*
             * top fragment in current tab doesn't handles back press, we can do
             * our thing, which is if current tab has only one fragment in
             * stack, ie first fragment is showing for this tab. finish the
             * activity else pop to previous fragment in stack for the same tab
             */
            if (mStacks.size() == 1) {
                // super.onBackPressed(); // or call finish..
                if (isListenerResult) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    super.onBackPressed();
                }
            } else {
                popFragments();
            }
        } else {
            // do nothing.. fragment already handled back button press.
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, data);
    }
}
