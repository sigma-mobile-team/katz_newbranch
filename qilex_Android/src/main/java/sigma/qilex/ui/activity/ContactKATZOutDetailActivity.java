
package sigma.qilex.ui.activity;

import java.util.ArrayList;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import pl.katz.aero2.Const.Config;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.dataaccess.sqlitedb.CallLogDb;
import sigma.qilex.dataaccess.sqlitedb.ContactContentProviderHelper;
import sigma.qilex.manager.account.PresenceManager;
import sigma.qilex.manager.account.PresenceManager.PingStatusListener;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactChangeListener;
import sigma.qilex.manager.contact.ContactManager.GetContactTempByPhoneListener;
import sigma.qilex.manager.phone.CallLogChangeListener;
import sigma.qilex.manager.phone.CallLogManager;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.ui.adapter.ContactDetailCallListAdapter;
import sigma.qilex.ui.adapter.ContactDetailPhoneListAdapter;
import sigma.qilex.ui.customview.CustomButton;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.DateTimeUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class ContactKATZOutDetailActivity extends BaseActivity
        implements ContactChangeListener, PingStatusListener, CallLogChangeListener, OnClickListener {
    public static final int EDIT_REQUEST = 1;

    private final int ADD_CONTACT_REQUEST = 2;

    public static final String INTENT_PARAM_VIEW_TYPE = "INTENT_PARAM_VIEW_TYPE";

    public static final String INTENT_PARAM_LIST_CALLLOG = "INTENT_PARAM_LIST_CALLLOG";

    public static final String INTENT_PARAM_PHONE_NUMBER = "INTENT_PARAM_PHONE_NUMBER";

    public static final int VIEW_TYPE_DETAIL = 0;

    public static final int VIEW_TYPE_CALLLOG = 1;

    public static final int VIEW_TYPE_IN_CALLING = 2;

    private int mViewType = 0;

    private int limitCallLogGet = 2;

    MyCustomTextView txtContactName;

    ListView mListHistory;

    ListView mListPhoneNumber;

    LinearLayout mViewHistory;

    LinearLayout mViewListPhone;

    View mLoadingContentProgress;

    View mLoadingContentText;

    boolean isContactFrifon = false;

    CustomButton btnInvite;

    CustomButton btnFreeCall;

    CustomButton btnFreeMessage;

    ImageView mImgAvatar;

    QilexContact mContact;

    Button btnAddContact;

    TextView txtAddContact;

    View loadMoreCallLog;

    View layoutLoadMore;

    ImageView collseCallLog;

    Dialog basicDialog;

    private String mNumberIntent;

    int mImgSize;

    String mCallLogPhoneNo;

    private CallLogManager mCallLogManager;

    private ContactManager mContactManager;

    private boolean requestBindContactWhenResume;

    private PresenceManager mPresenceManager;

    private Timer mPingContactTimer;

    private TimerTask mPingContactTimerTask;

    ProgressBar mProgressAvatar;

    private String mNumberCallOUT;

    private int currentRequestIdGetUserInfo;

    private static Stack<ContactKATZOutDetailActivity> mStackActivity = new Stack<ContactKATZOutDetailActivity>();

    OnClickListener actionEdit = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // If current screen is using to add number to contact.
            if (mNumberIntent != null) {
                // Add new number to contact
                ContactContentProviderHelper.addNumberToContact(ContactKATZOutDetailActivity.this,
                        mContact.getRawContactId(), mNumberIntent);
                finish();
                return;
            }

            // If current screen is using to view contact
            Intent intent = new Intent(ContactKATZOutDetailActivity.this, EditContactActivity.class);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, mContact.id);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY, mContact.lookUpKey);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT_2);
            startActivityForResult(intent, EDIT_REQUEST);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        putToStack(this);
        setContentView(R.layout.contact_frifon_out_detail_activity);
        showBackAction();
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        mCallLogManager = CallLogManager.getInstance();
        mContactManager = ContactManager.getInstance();
        mPresenceManager = PresenceManager.getInstance();
        // Initialize controls
        initRes();

        // Get view type
        mViewType = extras.getInt(INTENT_PARAM_VIEW_TYPE, VIEW_TYPE_DETAIL);

        if (mViewType == VIEW_TYPE_DETAIL) {
            initViewContactDetail(extras);
        } else if (mViewType == VIEW_TYPE_CALLLOG) {
            initViewCallLog(extras);
        } else if (mViewType == VIEW_TYPE_IN_CALLING) {
            initViewInCalling(extras);
        }
        if (mContact.isKATZ()) {
            mPresenceManager.registerPingStatusListener(this);
        }
        mContactManager.addContactChangeListener(this);
        mCallLogManager.addCallLogChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeStack(this);
        mPresenceManager.unregisterPingStatusListener(this);
        mContactManager.removeContactChangeListener(this);
        mCallLogManager.removeCallLogChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop ping contact status thread
        stopTimerPingContact();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (requestBindContactWhenResume) {
            requestBindContactWhenResume = false;
            bindContactDataToScreen();
        }
        // Start ping contact status thread
        if (mContact.isKATZ()) {
            startTimerPingContact();
        }
    }

    private void startTimerPingContact() {
        mPingContactTimerTask = new TimerTask() {
            @Override
            public void run() {
                pingContactStatus();
            }
        };
        mPingContactTimer = new Timer();
        mPingContactTimer.schedule(mPingContactTimerTask, 1000, Config.TIME_MILLIS_DELAY_PING_CONTACT_STATUS);
    }

    private void stopTimerPingContact() {
        if (mPingContactTimerTask != null) {
            mPingContactTimer.cancel();
            mPingContactTimerTask.cancel();
        }
    }

    private void pingContactStatus() {
        Utils.globalQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                mPresenceManager.pingToAdrress(mContact.getFirstKATZNumber().phoneNumber);
            }
        });
    }

    private void initViewCallLog(Bundle extras) {
        boolean needLoadContactDataFromServer = false;
        mCallLogPhoneNo = extras.getString(INTENT_PARAM_PHONE_NUMBER);
        mContact = mContactManager.getContactByPhoneNo(this, mCallLogPhoneNo);
        // mContact = mContactManager.getContactByPhoneNoFromPhoneBook(this,
        // mCallLogPhoneNo);
        if (mContact == null) {
            btnAddContact.setVisibility(View.VISIBLE);
            txtAddContact.setVisibility(View.VISIBLE);
            actionbarRight3.setVisibility(View.INVISIBLE);
            mContact = new QilexContact();
            mContact.firstName = "";
            mContact.lastName = "";
            mContact.photoUri = "";
            mContact.photoUriFull = "";
            mContact.rawContactId = -1;
            mContact.id = -1;
            mContact.serverContactId = -1;
            mContact.phoneNumberKATZ = new ArrayList<>();
            mContact.phoneNumberNormal = new ArrayList<>();
            mContact.phoneNumberNormal
                    .add(new PhoneNumberModel(mCallLogPhoneNo, getString(R.string.phone_type_mobile)));
            TempContactModel tempContact = mContactManager.getTempContact(mCallLogPhoneNo);
            if (tempContact == null) {
                needLoadContactDataFromServer = true;
            } else {
                mContact.firstName = tempContact.name;
                mContact.photoUri = tempContact.avatarUrl;
                mContact.phoneNumberKATZ.add(mContact.phoneNumberNormal.get(0));
                mContact.phoneNumberNormal.clear();
            }
        }
        requestBindContactWhenResume = false;

        long[] callLogsIds = extras.getLongArray(INTENT_PARAM_LIST_CALLLOG);
        ArrayList<CallLogModel> callLogs = new ArrayList<CallLogModel>();
        for (long l : callLogsIds) {
            callLogs.add(CallLogDb.getInstance().findCallLogById(l));
        }
        if (callLogs.size() > 0) {
            mViewHistory.setVisibility(View.VISIBLE);
            ContactDetailCallListAdapter callAdapter = new ContactDetailCallListAdapter(this, callLogs);
            mListHistory.setAdapter(callAdapter);
        } else {
            mViewHistory.setVisibility(View.GONE);
        }
        bindContactDataToScreen();

        if (needLoadContactDataFromServer == true) {
            loadContactInfoFromServerForCallLogMode();
        }
    }

    private void initViewContactDetail(Bundle extras) {
        // Get intent param
        String lookupKey = extras.getString(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY);
        long id = extras.getLong(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS);
        mContact = mContactManager.getContactByIdOrLookupKey(lookupKey, id);
        requestBindContactWhenResume = false;
        bindContactDataToScreen();
    }

    private void initViewInCalling(Bundle extras) {
        mContactManager.addContactChangeListener(this);
        // Get intent param
        String lookupKey = extras.getString(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY);
        long id = extras.getLong(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS);
        mContact = mContactManager.getContactByIdOrLookupKey(lookupKey, id);

        bindContactDataToScreen();
    }

    protected android.view.View.OnClickListener onBack = new OnClickListener() {
        @Override
        public void onClick(View v) {
            ContactKATZOutDetailActivity.this.setResult(RESULT_CANCELED);
            onBackPressed();
        }
    };

    private void initRes() {
        txtContactName = (MyCustomTextView)findViewById(R.id.txt_contact_name);
        btnInvite = (CustomButton)findViewById(R.id.btn_invite);
        btnFreeCall = (CustomButton)findViewById(R.id.btn_free_call);
        btnFreeMessage = (CustomButton)findViewById(R.id.btn_free_msg);
        mListHistory = (ListView)findViewById(R.id.list_history);
        mListPhoneNumber = (ListView)findViewById(R.id.list_phone_numbers);
        mViewHistory = (LinearLayout)findViewById(R.id.list_history_calllog);
        mViewListPhone = (LinearLayout)findViewById(R.id.list_phone_container);
        btnAddContact = (Button)findViewById(R.id.btnAddContact);
        txtAddContact = (TextView)findViewById(R.id.txtAddContact);
        loadMoreCallLog = findViewById(R.id.btnLoadMore);
        layoutLoadMore = findViewById(R.id.layoutLoadMore);
        collseCallLog = (ImageView)findViewById(R.id.btnHide);
        mLoadingContentText = findViewById(R.id.loadingContentText);
        mLoadingContentProgress = findViewById(R.id.loadingContentProgress);
        mProgressAvatar = (ProgressBar)findViewById(R.id.progress_avatar);
        loadMoreCallLog.setOnClickListener(this);
    }

    private void initActionBar() {
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        if (isContactFrifon) {
            actionbarSubTitle3.setVisibility(View.VISIBLE);
            actionbarSubTitle3.setText("");
        }
        actionbarTitle3.setText(mContact.getDisplayName());
        if (mNumberIntent != null) {
            actionbarRight3.setText(R.string.done);
        }
        txtContactName.setText(mContact.getDisplayName());
        setActionBarType3Action(onBack, actionEdit);
        if (mContact.hasPhoneNumber(QilexPhoneNumberUtils.SUPPORT_NUMBER[0]) && mContact.phoneNumberNormal.size() == 1) {
            btnInvite.setEnabled(false);
        } else if (mViewType == VIEW_TYPE_IN_CALLING) {
            btnInvite.setVisibility(View.GONE);
            btnFreeCall.setVisibility(View.GONE);
            btnFreeMessage.setVisibility(View.GONE);
        } else {
            if (isContactFrifon) {
                btnInvite.setVisibility(View.GONE);
                btnFreeCall.setVisibility(View.VISIBLE);
                btnFreeMessage.setVisibility(View.VISIBLE);
                Utils.formatTextSizeButton(this, new CustomButton[] {
                        btnFreeCall, btnFreeCall
                });
            } else {
                btnInvite.setVisibility(View.VISIBLE);
                Utils.formatTextSizeButton(this, btnInvite);
                btnFreeCall.setVisibility(View.GONE);
                btnFreeMessage.setVisibility(View.GONE);
            }
        }
        mImgAvatar = (ImageView)findViewById(R.id.user_avatar);
        if (Utils.isStringNullOrEmpty(mContact.getPhotoUri()) == false) {
            MyApplication.getImageLoader().displayImage(mContact.getPhotoUri(), mImgAvatar, circleDisplayImageOptions2,
                    new ProgressLoadingListener(mProgressAvatar));
        }
        actionbarSubTitle3.setVisibility(View.GONE);
        if (Utils.isInMasterMode() == false && mContact.isEditable == false) {
            actionbarRight3.setVisibility(View.INVISIBLE);
        }
    }

    private void bindContactDataToScreen() {
        isContactFrifon = mContact.isKATZ();
        // Initialize actionbar
        initActionBar();

        // Generate phone numbers array
        ArrayList<String> numbers = new ArrayList<String>();
        ArrayList<PhoneNumberModel> numbersWithType = new ArrayList<PhoneNumberModel>();
        numbersWithType.addAll(mContact.phoneNumberKATZ);
        numbersWithType.addAll(mContact.phoneNumberNormal);
        if (mContact.getPhoneNumberKATZ() != null) {
            for (PhoneNumberModel phone : mContact.phoneNumberKATZ) {
                numbers.add(phone.phoneNumber);
            }
        }
        if (mContact.getPhoneNumberNormal() != null) {
            for (PhoneNumberModel phone : mContact.phoneNumberNormal) {
                numbers.add(phone.phoneNumber);
            }
        }
        // if current screen is using to add phone number
        if (mNumberIntent != null) {
            PhoneNumberModel newNumber = new PhoneNumberModel(mNumberIntent,
                    this.getString(R.string.phone_type_mobile));
            numbersWithType.add(newNumber);
        }
        // Show list phone number
        if (numbers.isEmpty()) {
            mViewListPhone.setVisibility(View.GONE);
            mListPhoneNumber.setVisibility(View.GONE);
        } else {
            mViewListPhone.setVisibility(View.VISIBLE);
            ContactDetailPhoneListAdapter phoneAdapter = new ContactDetailPhoneListAdapter(this, numbersWithType);
            phoneAdapter.setIsVisibleButton(mViewType != VIEW_TYPE_IN_CALLING);
            mListPhoneNumber.setAdapter(phoneAdapter);
        }

        if (mViewType == VIEW_TYPE_DETAIL || mViewType == VIEW_TYPE_IN_CALLING) {
            // Show list CallLog
            String[] phoneNumbersArray = new String[numbers.size()];
            ArrayList<CallLogModel> callLogs = mCallLogManager.getCallLogByPhoneNumbers(limitCallLogGet,
                    numbers.toArray(phoneNumbersArray));
            if (callLogs.isEmpty()) {
                mViewHistory.setVisibility(View.GONE);
            } else {
                mViewHistory.setVisibility(View.VISIBLE);
                ContactDetailCallListAdapter callAdapter = new ContactDetailCallListAdapter(this, callLogs);
                mListHistory.setAdapter(callAdapter);
            }
            int countCallLog = mCallLogManager.countCallLog(phoneNumbersArray);
            if (countCallLog > 2) {
                layoutLoadMore.setVisibility(View.VISIBLE);
            }
            if (limitCallLogGet >= countCallLog) {
                loadMoreCallLog.setEnabled(false);
            } else {
                loadMoreCallLog.setEnabled(true);
            }
            if (callLogs.size() > 2) {
                collseCallLog.setEnabled(true);
            } else {
                collseCallLog.setEnabled(false);
            }
        }

        Utils.setListViewHeightBasedOnChildren(mListHistory);
        Utils.setListViewHeightBasedOnChildren(mListPhoneNumber);

        ScrollView scrl = (ScrollView)findViewById(R.id.scrl_root_contact);
        scrl.pageScroll(View.FOCUS_UP);
    }

    public void onInviteClick(View v) {
        if (Utils.isInMasterMode()) {
            if (mContact.getPhoneNumberNormal() != null && mContact.getPhoneNumberNormal().length > 0) {
                String phoneNo = (mContact.getPhoneNumberNormal()[0]).phoneNumber;
                Utils.sendSmsWithBody(this, phoneNo, getString(Utils.isInKatzMode() ? R.string.invite_msg : R.string.invite_msg_green));
            }
        } else {
            Utils.showToastInSlaveMode(ContactKATZOutDetailActivity.this);
        }
    }

    public void onAddContactClick(View v) {
        Intent intent = new Intent(this, CreateNewContactActivity.class);
        intent.putExtra("create_new_contact", mCallLogPhoneNo);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_LEFT_OUT_LEFT_2);
        intent.putExtra(CreateNewContactActivity.INTENT_PARAM_CONTACT, mContact);
        startActivityForResult(intent, ADD_CONTACT_REQUEST);
    }

    public void onLoadMoreCallLog() {
        limitCallLogGet += 5;
        // Generate phone numbers array
        ArrayList<String> numbers = new ArrayList<String>();
        if (mContact.getPhoneNumberKATZ() != null) {
            for (PhoneNumberModel phone : mContact.getPhoneNumberKATZ()) {
                numbers.add(phone.phoneNumber);
            }
        }
        if (mContact.getPhoneNumberNormal() != null) {
            for (PhoneNumberModel phone : mContact.getPhoneNumberNormal()) {
                numbers.add(phone.phoneNumber);
            }
        }

        // Show list CallLog
        String[] phoneNumbersArray = new String[numbers.size()];
        ArrayList<CallLogModel> callLogs = mCallLogManager.getCallLogByPhoneNumbers(limitCallLogGet,
                numbers.toArray(phoneNumbersArray));
        if (callLogs.size() > 0) {
            mViewHistory.setVisibility(View.VISIBLE);
            ContactDetailCallListAdapter callAdapter = new ContactDetailCallListAdapter(this, callLogs);
            mListHistory.setAdapter(callAdapter);
        } else {
            mViewHistory.setVisibility(View.GONE);
        }
        int countCallLog = mCallLogManager.countCallLog(phoneNumbersArray);
        if (callLogs.size() >= countCallLog) {
            loadMoreCallLog.setEnabled(false);
        }
        if (callLogs.size() > 2) {
            collseCallLog.setEnabled(true);
        } else {
            collseCallLog.setEnabled(false);
        }

        Utils.setListViewHeightBasedOnChildren(mListHistory);
    }

    public void onCollapse(View v) {
        limitCallLogGet = 2;
        // Generate phone numbers array
        ArrayList<String> numbers = new ArrayList<String>();
        if (mContact.getPhoneNumberKATZ() != null) {
            for (PhoneNumberModel phone : mContact.getPhoneNumberKATZ()) {
                numbers.add(phone.phoneNumber);
            }
        }
        if (mContact.getPhoneNumberNormal() != null) {
            for (PhoneNumberModel phone : mContact.getPhoneNumberNormal()) {
                numbers.add(phone.phoneNumber);
            }
        }

        // Show list CallLog
        String[] phoneNumbersArray = new String[numbers.size()];
        ArrayList<CallLogModel> callLogs = mCallLogManager.getCallLogByPhoneNumbers(limitCallLogGet,
                numbers.toArray(phoneNumbersArray));
        if (callLogs.size() > 0) {
            mViewHistory.setVisibility(View.VISIBLE);
            ContactDetailCallListAdapter callAdapter = new ContactDetailCallListAdapter(this, callLogs);
            mListHistory.setAdapter(callAdapter);
        } else {
            mViewHistory.setVisibility(View.GONE);
        }

        loadMoreCallLog.setEnabled(true);
        collseCallLog.setEnabled(false);

        Utils.setListViewHeightBasedOnChildren(mListHistory);
    }

    public void onFreeCallClick(View v) {
        PhoneNumberModel[] phones = mContact.getPhoneNumberKATZ();
        if (phones.length == 1) {
            String callid = Utils.getKATZNumber(phones[0].phoneNumber);
            CallManager.getInstance().initOutGoingSipCall(this, callid);

        } else {
            final String[] phoneNos = new String[phones.length];
            OnClickListener[] listeners = new OnClickListener[phones.length];
            for (int i = 0; i < phoneNos.length; i++) {
                final int index = i;
                phoneNos[i] = phones[i].phoneNumber;
                listeners[i] = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String callid = Utils.getKATZNumber(phoneNos[index]);
                        CallManager.getInstance().initOutGoingSipCall(ContactKATZOutDetailActivity.this, callid);
                        basicDialog.dismiss();
                    }
                };
            }
            basicDialog = Utils.showBasicSelectionDialog(this, R.layout.message_longpress_dialog_title, "Make Call",
                    phoneNos, listeners, true);
        }
    }

    public void onFreeMessageClick(View v) {
        PhoneNumberModel[] phones = mContact.getPhoneNumberKATZ();
        if (phones.length == 1) {
            // Check if this contact is yourself
            QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
            if (localProfile.getLogin().equals(Utils.getKATZNumber(phones[0].phoneNumber))) {
                showAlert(R.string.error, R.string.error_chat_yourself);
                return;
            }

            Intent intent = new Intent(this, ConversationActivity.class);
            intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, phones[0].phoneNumber);
            if (actionbarSubTitle3.getVisibility() == View.VISIBLE) {
                intent.putExtra(ConversationActivity.INTENT_PARAM_ONLINE_STATUS, actionbarSubTitle3.getText());
            }
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            startActivity(intent);
        } else {
            final String[] phoneNos = new String[phones.length];
            OnClickListener[] listeners = new OnClickListener[phones.length];
            for (int i = 0; i < phoneNos.length; i++) {
                final int index = i;
                phoneNos[i] = phones[i].phoneNumber;
                listeners[i] = new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
                        if (localProfile.getLogin().equals(Utils.getKATZNumber(phoneNos[index]))) {
                            showAlert(R.string.error, R.string.error_chat_yourself);
                            return;
                        }

                        Intent intent = new Intent(ContactKATZOutDetailActivity.this, ConversationActivity.class);
                        intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, phoneNos[index]);
                        if (actionbarSubTitle3.getVisibility() == View.VISIBLE) {
                            intent.putExtra(ConversationActivity.INTENT_PARAM_ONLINE_STATUS,
                                    actionbarSubTitle3.getText());
                        }
                        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                                ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                        startActivity(intent);
                        basicDialog.dismiss();
                    }
                };
            }
            basicDialog = Utils.showBasicSelectionDialog(this, R.layout.message_longpress_dialog_title,
                    getResources().getString(R.string.message), phoneNos, listeners, true);
        }
    }

    public void onClickFrifonOUT(String number) {
        mNumberCallOUT = number;
        int phoneCode = CallManager.getInstance().initOutGoingSipCall(mNumberCallOUT, true);
        if (phoneCode > 0) {
            showAlert(R.string.call_fail, phoneCode);
        }
    }

    private void loadContactInfoFromServerForCallLogMode() {
        showSimpleProgressByPostHandler(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                QilexHttpRequest.stopRequest(currentRequestIdGetUserInfo);
            }
        });

        currentRequestIdGetUserInfo = mContactManager.loadContactInfo(this, mCallLogPhoneNo,
                new GetContactTempByPhoneListener() {

                    @Override
                    public void onGetContactFinish(TempContactModel model, ErrorModel error) {
                        hideSimpleProgressByPostHandler();
                        if (model != null) {
                            mContact.phoneNumberNormal = new ArrayList<>();
                            mContact.phoneNumberKATZ = new ArrayList<>();
                            mContact.phoneNumberKATZ
                                    .add(new PhoneNumberModel(mCallLogPhoneNo, getString(R.string.phone_type_mobile)));
                            mContact.setFirstName(model.name);
                            mContact.setPhotoUri(model.avatarUrl);
                            postHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    bindContactDataToScreen();
                                }
                            });
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_REQUEST) {
            if (resultCode == RESULT_OK) {
                QilexContact updatedContact = mContactManager.getContactByLookupKey(mContact.lookUpKey);
                if (updatedContact == null || updatedContact.hasPhoneNumber() == false) {
                    finish();
                    return;
                }
                if (updatedContact.id == -1) {
                    updatedContact = mContactManager.getContactById(mContact.id);
                }
                mContact = updatedContact;
                bindContactDataToScreen();
            } else if (resultCode == -5) {
                // Delete contact
                setResult(RESULT_OK);
                finish();
            }
        } else if (requestCode == ADD_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    public void onNewContact(QilexContact newContact) {
    }

    @Override
    public void onUpdateContact(QilexContact newContact) {
        QilexContact updatedContact = mContactManager.getContactByLookupKey(mContact.lookUpKey);
        if (updatedContact.id == -1) {
            updatedContact = mContactManager.getContactById(mContact.id);
        }
        this.mContact = updatedContact;
        if (this.mContact.id != -1) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bindContactDataToScreen();
                }
            });
        }
    }

    @Override
    public void onDeleteContact(QilexContact newContact) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPingSuccess(final String phoneNum) {
        if (phoneNum.equals(mContact.getFirstKATZNumber().phoneNumber)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    actionbarSubTitle3.setVisibility(View.VISIBLE);
                    actionbarSubTitle3.setText(R.string.status_online);
                }
            });
        }
    }

    @Override
    public void onPingFail(final boolean isTimeOut, final String phoneNum, final int offlineMinute) {
        if (phoneNum.equals(mContact.getFirstKATZNumber().phoneNumber)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    actionbarSubTitle3.setVisibility(View.VISIBLE);
                    if (isTimeOut == false) {
                        String status = getResources().getString(R.string.status_offline);
                        if (offlineMinute > 0) {
                            status = getResources().getString(R.string.status_offline_for,
                                    DateTimeUtils.convertMinuteToReadableTime(offlineMinute));
                        }
                        actionbarSubTitle3.setText(status);
                    } else {
                        actionbarSubTitle3.setText(R.string.offline_for_a_moment);
                    }
                }
            });
        }
    }

    @Override
    public void onReceiveRequest(String phoneNum) {

    }

    private static void putToStack(ContactKATZOutDetailActivity insertActivity) {
        while (mStackActivity.isEmpty() == false) {
            Activity activity = mStackActivity.remove(0);
            activity.finish();
        }
        mStackActivity.addElement(insertActivity);
    }

    private static void removeStack(ContactKATZOutDetailActivity insertActivity) {
        mStackActivity.removeElement(insertActivity);
    }

    @Override
    public void onNewCallLog(CallLogModel callLog) {
        if (mContact.hasPhoneNumber(callLog.getLabel())) {
            limitCallLogGet++;
            requestBindContactWhenResume = true;
        }
    }

    @Override
    public void onCallLogChange(CallLogModel callLog) {
    }

    @Override
    public void onCallLogDeleted(long[] callLogIds) {
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnLoadMore) {
            onLoadMoreCallLog();
        }
    }
}
