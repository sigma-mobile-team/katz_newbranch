
package sigma.qilex.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import pl.frifon.aero2.R;
import sigma.qilex.ui.tab.ContactListFragment;

public class ContactListActivity extends BaseActivity {

    ContactListFragment mFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list_activity);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                boolean browserContact = extras.getBoolean("browser_contact_mode", false);
                if (browserContact) {
                    setActionBarType(ACTIONBAR_TYPE_CHAT);
                    boolean isRoom = extras.getBoolean("from_chatroom", false);
                    if (isRoom) {
                        setActionbarTitle(R.string.add_to_chatroom);
                        setAtionbarRight3Title(R.string.done);
                        actionbarRight3.setVisibility(View.INVISIBLE);
                    } else {
                        setActionbarTitle(R.string.select_contact_lable);
                        setAtionbarRight3Title(R.string.start);
                        actionbarRight3.setVisibility(View.INVISIBLE);
                    }
                    // setActionbarTitle3TextSize(13.0f);
                    setActionBarType3Action(actionContactCancel, actionContactStart);
                    showBackAction();
                } else {
                    setActionBarType(ACTIONBAR_TYPE_PHONE_CALL);
                    setActionBarType2Action(actionContactCenterLeft, actionContactCenterRight);
                    resetActionBarCallLogButtons(true, ContactListFragment.VIEW_ALL);
                    hideEditButton();
                    showBackAction();
                }
            }
            mFragment = new ContactListFragment() {
            	@Override
            	public void onSelectedContactChange(int numberOfSelectedContact) {
            		if (numberOfSelectedContact <= 0) {
            			actionbarRight3.setVisibility(View.INVISIBLE);
            		} else {
            			actionbarRight3.setVisibility(View.VISIBLE);
            		}
            	}
            };
            getSupportFragmentManager().beginTransaction().add(R.id.container, mFragment).commit();
        }
    }

    OnClickListener actionContactCenterLeft = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mFragment != null)
                mFragment.onChangeTypeList(ContactListFragment.VIEW_ALL);
        }
    };

    OnClickListener actionContactCenterRight = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mFragment != null)
                mFragment.onChangeTypeList(ContactListFragment.VIEW_FRIFON);
        }
    };

    OnClickListener actionContactCancel = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mFragment.clearSelection();
            setResult(Activity.RESULT_CANCELED);
            ContactListActivity.this.finish();
        }
    };

    OnClickListener actionContactStart = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mFragment != null)
                mFragment.onStartResult();
        }
    };
}
