
package sigma.qilex.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import pl.katz.aero2.AppManager;
import pl.frifon.aero2.R;
import sigma.qilex.utils.QilexUtils;

/**
 * @author namnd @ all activity just extend this class, unless
 *         ImcomingMessageAcitivty
 */
public abstract class IActivity extends ActionBarActivity {
    /**
     * permission for Camera here.
     */
    private static String CAMERA_PERMISSIONS[] = new String[] {
            Manifest.permission.CAMERA
    };

    public final static int REQUEST_CAMERA_CODE = 101;

    /**
     * Read and write permission for contacts listed here.
     */
    public final static String CONTACT_PERMISSIONS[] = new String[] {
            Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS
    };

    public final static int REQUEST_CONTACTS_CODE = 103;

    /**
     * Read and write permission for storage listed here.
     */
    private static String STORAGE_PERMISSIONS[] = new String[] {
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public final static int REQUEST_STORAGE_CODE = 105;

    /**
     * permission for call listed here.
     */
    public final static int REQUEST_CALL_CODE = 107;

    private static String CALL_PERMISSIONS[] = new String[] {
            Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE
    };

    /**
     * permission for call listed here.
     */
    public final static int REQUEST_MICRO_CODE = 109;

    private static String MICRO_PERMISSIONS[] = new String[] {
            Manifest.permission.RECORD_AUDIO
    };

    /**
     * permission for location listed here.
     */
    public final static int REQUEST_LOCATION_CODE = 111;

    public static String LOCATION_PERMISSIONS[] = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };

    public final static int REQUEST_PHONE_STATE_CODE = 112;

    public static String PHONE_PERMISSION[] = new String[] {
            Manifest.permission.READ_PHONE_STATE
    };

    public final static int REQUEST_SMS_CODE = 113;

    public static String SMS_PERMISSION[] = new String[] {
            Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS,
    };

    public static boolean isActive = false;

    public void checkPermission(int requestCode) {
        switch (requestCode) {
            case REQUEST_CAMERA_CODE:
                if (QilexUtils.hasSelfPermission(this, CAMERA_PERMISSIONS))
                    onPreRequestPermission(requestCode);
                else
                    requestPermissions(CAMERA_PERMISSIONS, requestCode);

                break;
            case REQUEST_CONTACTS_CODE:
                if (QilexUtils.hasSelfPermission(this, CONTACT_PERMISSIONS))
                    onPreRequestPermission(requestCode);
                else
                    requestPermissions(CONTACT_PERMISSIONS, requestCode);

                break;
            case REQUEST_STORAGE_CODE:
                if (QilexUtils.hasSelfPermission(this, STORAGE_PERMISSIONS))
                    onPreRequestPermission(requestCode);
                else
                    requestPermissions(STORAGE_PERMISSIONS, requestCode);

                break;
            case REQUEST_CALL_CODE:
                if (QilexUtils.hasSelfPermission(this, CALL_PERMISSIONS))
                    onPreRequestPermission(requestCode);
                else
                    requestPermissions(CALL_PERMISSIONS, requestCode);
                break;
            case REQUEST_MICRO_CODE:
                if (QilexUtils.hasSelfPermission(this, MICRO_PERMISSIONS))
                    onPreRequestPermission(requestCode);
                else
                    requestPermissions(MICRO_PERMISSIONS, requestCode);
                break;
            case REQUEST_LOCATION_CODE:
                if (QilexUtils.hasSelfPermission(this, LOCATION_PERMISSIONS))
                    onPreRequestPermission(requestCode);
                else
                    requestPermissions(LOCATION_PERMISSIONS, requestCode);

                break;
            case REQUEST_SMS_CODE:
                if (QilexUtils.hasSelfPermission(this, SMS_PERMISSION)) {
                    onPreRequestPermission(requestCode);
                } else {
                    requestPermissions(SMS_PERMISSION, requestCode);
                }
            default:
                break;
        }
    }

    public abstract void onPreRequestPermission(int requestCode);

    public abstract void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults);
    
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_CODE:
            case REQUEST_CONTACTS_CODE:
            case REQUEST_STORAGE_CODE:
            case REQUEST_CALL_CODE:
            case REQUEST_MICRO_CODE:
            case REQUEST_LOCATION_CODE:
            case REQUEST_SMS_CODE:
                onPostRequestPermission(requestCode, permissions, grantResults);
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onDestroy() {
        AppManager.getAppManager().removeActivity(this);
        super.onDestroy();
    }

    /**
     * Show alert
     * 
     * @param content
     */
    public void showAlert(int titleId, int contentId) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(titleId);

        // Setting Dialog Message
        alertDialog.setMessage(contentId);

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}
