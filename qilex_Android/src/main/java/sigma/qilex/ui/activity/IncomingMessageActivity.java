
package sigma.qilex.ui.activity;

import pl.katz.aero2.AppManager;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.ChatThreadModel;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.phone.InCallWakeLock;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class IncomingMessageActivity extends ActionBarActivity implements View.OnClickListener {

    public static final String INTENT_RECV_MESSAGE = "sigma.qilex.INCOMING_MESSAGE";

    public static final String INTENT_MSG_TYPE = "sigma.qilex.INTENT_MSG_TYPE";

    public static final String TAG = IncomingMessageActivity.class.getSimpleName();

    MyCustomEditText inputMsg;

    ChatManager mChatManager;

    String name;

    String contactString;
    
    String senderPhoneNo;

    TextView tvName;

    MyCustomTextView tvContent;
    
    ImageView imageContent;

    View relContent;

    int msgMineType;

    private long threadId;

    public static boolean active;

    InCallWakeLock mWakeLock;

    SharedPrefrerenceFactory prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_msg_popup);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mWakeLock = new InCallWakeLock(this);
        Intent intent = getIntent();
        if (intent != null) {
            threadId = getIntent().getLongExtra("qilex_thread_id", -1l);
            String[] args = getIntent().getStringArrayExtra("qilex_args");
            initDialog(args);
        } else {
            finish();
            return;
        }
        active = true;
        prefs = SharedPrefrerenceFactory.getInstance();
        IntentFilter filter = new IntentFilter(INTENT_RECV_MESSAGE);
        registerReceiver(mRecvMessage, filter);
        AppManager.getAppManager().addActivity(this);
    }

    public void initDialog(String... args) {
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        this.getWindow().setAttributes(params);

        params.dimAmount = 0.f;
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        this.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        this.mChatManager = ChatManager.getInstance();
        this.name = args[0];
        this.contactString = args[4];
        this.senderPhoneNo = args[1];

        imageContent = (ImageView)findViewById(R.id.image);
        tvName = (TextView)findViewById(R.id.tvName);
        ImageButton closeBtn = (ImageButton)findViewById(R.id.closeImageButton);
        ImageButton openBtn = (ImageButton)findViewById(R.id.openImageButton);

        tvContent = (MyCustomTextView)findViewById(R.id.tvContent);
        inputMsg = (MyCustomEditText)findViewById(R.id.messageEdit);
        final ImageView sendBtn = (ImageView)findViewById(R.id.chatSendButton);
        sendBtn.setEnabled(false);
        relContent = findViewById(R.id.relContent);

        this.msgMineType = Integer.parseInt(args[3]);
        if (msgMineType == ChatDatabase.MSG_MINETYPE_IMAGE) {
            imageContent.setVisibility(View.VISIBLE);
            imageContent.setImageResource(R.drawable.icon_gallery);
        } else if (msgMineType == ChatDatabase.MSG_MINETYPE_LOCATION) {
            imageContent.setVisibility(View.VISIBLE);
            imageContent.setImageResource(R.drawable.ic_map_marker);
        } else {
            imageContent.setVisibility(View.GONE);
        }

        tvName.setText(name);
        tvContent.setText(args[2]);
        inputMsg.clearFocus();

        inputMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean empty = Utils.isStringNullOrEmpty(s.toString());
                sendBtn.setEnabled(!empty);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // not setup
            }

            @Override
            public void afterTextChanged(Editable s) {
                // not setup
            }
        });
        
        // Check is support number
        if (QilexPhoneNumberUtils.isSupportNumber(contactString)) {
            findViewById(R.id.compose).setVisibility(View.INVISIBLE);
        }

        closeBtn.setOnClickListener(this);
        openBtn.setOnClickListener(this);
        sendBtn.setOnClickListener(this);
        tvContent.setOnClickListener(this);
        relContent.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String draftMessage = prefs.getString(String.format(ConversationActivity.PREF_DRAFT_MESSAGE, threadId));
        boolean isFocus = prefs.getBoolean("focus_msg_popup_" + threadId);
        if (!TextUtils.isEmpty(draftMessage)) {
            inputMsg.setText(draftMessage);
        }
        if (isFocus) {
            inputMsg.requestFocus();
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(inputMsg, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        String draftMsg = inputMsg.getText().toString();
        if (draftMsg != null) {
            prefs.putString(String.format(ConversationActivity.PREF_DRAFT_MESSAGE, threadId), draftMsg);
        }
        prefs.putBoolean("focus_msg_popup_" + threadId, inputMsg.isFocused());
        active = false;
    }

    @Override
    protected void onDestroy() {
        AppManager.getAppManager().removeActivity(this);
        mWakeLock.release();
        if (mRecvMessage != null) {
            unregisterReceiver(mRecvMessage);
        }
        mWakeLock = null;
        prefs.putBoolean("focus_msg_popup_" + threadId, false);
        super.onDestroy();
    }

    BroadcastReceiver mRecvMessage = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(INTENT_RECV_MESSAGE)) {
                long threadId = intent.getLongExtra("qilex_thread_id", -1l);
                setThreadId(threadId);
                String[] args = intent.getStringArrayExtra("qilex_args");
                msgMineType = Integer.parseInt(args[3]);
                if (msgMineType == ChatDatabase.MSG_MINETYPE_IMAGE) {
                    findViewById(R.id.image).setVisibility(View.VISIBLE);
                } else {
                    findViewById(R.id.image).setVisibility(View.GONE);
                }
                setNameAndMsg(args);
                wakeOn();
            }
        }

    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.closeImageButton) {
            finish();
        } else if (v.getId() == R.id.chatSendButton) {
            // Get sip address from phone number
            String sipAddress = contactString;

            // Get chat thread, set all message to READ
            ChatThreadModel threadModel = mChatManager.getChatThreadByContactStr(contactString);
            if (threadModel != null) {
                mChatManager.markAllRead(threadModel.id);
            }

            // Send message
            mChatManager.sendSipMessage(-1, sipAddress, inputMsg.getText().toString(), null, threadModel.isGroupChat());
            sendClearBroadcast();
            inputMsg.setText("");
            finish();
        } else if (v.getId() == R.id.relContent || v.getId() == R.id.openImageButton || v.getId() == R.id.tvContent) {
            // go to chat activity
            Intent intent = new Intent(IncomingMessageActivity.this, ConversationActivity.class);
            intent.setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(ConversationActivity.INTENT_PARAM_THREAD_ID, threadId);
            intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, contactString);
            startActivity(intent);
            sendClearBroadcast();
            finish();
        }
    }

    public void sendClearBroadcast() {
        Intent i = new Intent(QilexService.BROADCAST_NOTIFICATION());
        i.putExtra("notification_action_clear", true);
        sendBroadcast(i);
    }

    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    public synchronized void wakeOn() {
        if (!mWakeLock.isScreenOn()) {
            mWakeLock.requestWakeState(InCallWakeLock.WAKE_STATE_FULL);
        }
    }

    /**
     * Set content to Dialog.
     * 
     * @param args
     * 0. name Contact name or Group Name
     * 1. contactStr contact phone number or Group Chat ID.
     * 2. msg Message content
     */
    public void setNameAndMsg(String... args) {
        this.name = args[0];
        this.contactString = args[1];
        tvName.setText(name);
        tvContent.setText(args[2]);
    }

}
