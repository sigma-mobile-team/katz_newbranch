
package sigma.qilex.ui.activity;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.GetContactTempByPhoneListener;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.manager.phone.CallManager.CallActivityEvent;
import sigma.qilex.manager.phone.CallManager.QilexSipCallServiceListener;
import sigma.qilex.manager.phone.HeadphonesConnectedBroadcastReceiver;
import sigma.qilex.sip.SipCall;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class IncomingCallActivity extends BaseActivity implements QilexSipCallServiceListener {

    public static final String INTENT_PARAM_IS_PUSH = "IncomingCallActivity.INTENT_PARAM_IS_PUSH";

    CallManager mCallManager;

    TextView txtContactName;

    View layoutEndCall, layoutButtons, relAvatar;

    ImageView imgAvatar;

    boolean isCallFinish = false;

    volatile boolean isCallEstablished = false;

    boolean action = false;

    ContactManager mContactManager;

    ChatManager mChatManager;

    QilexContact peerContact;

    String contactName, peerPhoneNumber, smallAvatarUrl = "";

    String[] mDenyMessages;

    Button mTakeCall, mDeny, mDenyMessage;

    boolean hasDisplayingInfo = false;

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.d("HEHE", "onMessageReceived stop call 2");
            mCallManager.callReject();
            notifyFinish();
        }
    };

    GetContactTempByPhoneListener getContactTempListener = new GetContactTempByPhoneListener() {
        @Override
        public void onGetContactFinish(final TempContactModel model, ErrorModel error) {
            if (model == null) {
                return;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (hasDisplayingInfo == false) {
                        contactName = model.name;
                        txtContactName.setText(contactName);
                    }
                    if (Utils.isStringNullOrEmpty(model.avatarUrl) == false) {
                        MyApplication.getImageLoader().displayImage(model.avatarUrl, imgAvatar, imageOption,
                                animateFirstListener);
                    }
                }
            });
        }
    };

    HeadphonesConnectedBroadcastReceiver mHeadphonesConnectedBroadcastReceiver;

    DisplayImageOptions imageOption = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
            .considerExifParams(true).build();

    private AudioManager mAudioManager = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContactManager = ContactManager.getInstance();
        mChatManager = ChatManager.getInstance();
        mCallManager = CallManager.getInstance();
        setContentView(R.layout.incoming_call_activity);
        hideActionBar();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        mDenyMessages = getResources().getStringArray(R.array.deny_messages);
        // Initialize controls
        txtContactName = (TextView)findViewById(R.id.txtContactName);
        layoutButtons = findViewById(R.id.layoutButtons);
        layoutEndCall = findViewById(R.id.btnEndCall);
        imgAvatar = (ImageView)findViewById(R.id.imgAvatar);
        relAvatar = findViewById(R.id.rel_avatar);
        mTakeCall = (Button)findViewById(R.id.btnTakeCall);
        mDeny = (Button)findViewById(R.id.btnDeny);
        mDenyMessage = (Button)findViewById(R.id.btnSendMessage);
        getContactFromPeerAccount();
        isCallFinish = false;
        mCallManager.addSipCallServiceListener(this);

        // Register broadcast receiver
        IntentFilter filter = new IntentFilter(InCallActivity.ACTION_ENDCALL);
        registerReceiver(mReceiver, filter);
        action = false;
        // Register headphone connect receiver
        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        mHeadphonesConnectedBroadcastReceiver = new HeadphonesConnectedBroadcastReceiver();
        registerReceiver(mHeadphonesConnectedBroadcastReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        notifyAudioVolume((float)((float)mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL)
                / (float)mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindQilexService();
        QilexService service = getService();
        if (service != null) {
            service.getNotificationFactory().showIncomingNotification(contactName, smallAvatarUrl);
        }
    }

    private void notifyAudioVolume(float vrpAudioVolume) {
        Intent intent = new Intent("qilex.CallActivity");
        intent.putExtra("qilex_event", CallActivityEvent.AudioVolume);
        intent.putExtra("volume", vrpAudioVolume);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mCallManager.stopRinging();
                return true;
            case KeyEvent.KEYCODE_HEADSETHOOK:
                mCallManager.callAnswer();
                return true;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void getContactFromPeerAccount() {
        String loginName = getIntent().getExtras().getString("NAME");
        peerPhoneNumber = QilexPhoneNumberUtils.convertToBasicNumber(loginName);
        peerContact = mContactManager.getContactByPhoneNo(this, peerPhoneNumber);
        hasDisplayingInfo = true;

        hasDisplayingInfo = true;
        if (peerContact != null) {
            contactName = peerContact.getDisplayName();

            if (Utils.isStringNullOrEmpty(peerContact.getPhotoUriFull()) == false) {
                Bitmap avatar = mContactManager.getContactBitmapFromUri(peerContact.getPhotoUriFull());
                if (avatar != null) {
                    imgAvatar.setImageBitmap(avatar);
                } else {
                    if (Utils.isStringNullOrEmpty(peerContact.getPhotoUri()) == false) {
                        smallAvatarUrl = peerContact.getPhotoUri();
                        MyApplication.getImageLoader().displayImage(peerContact.getPhotoUri(), imgAvatar,
                                normalDisplayImageOptions, animateFirstListener);
                    }
                }
            }
        } else {
            hasDisplayingInfo = false;
            TempContactModel tempContact = mContactManager.getTempContact(peerPhoneNumber);
            if (tempContact == null) {
                contactName = peerPhoneNumber;
                hasDisplayingInfo = false;
            } else {
                contactName = tempContact.name;
                if (Utils.isStringNullOrEmpty(tempContact.avatarUrl) == false) {
                    smallAvatarUrl = tempContact.avatarUrl;
                    MyApplication.getImageLoader().displayImage(tempContact.avatarUrl, imgAvatar,
                            normalDisplayImageOptions, animateFirstListener);
                }
            }
        }
        mContactManager.loadContactInfo(this, peerPhoneNumber, getContactTempListener);
        txtContactName.setText(contactName);
        layoutEndCall.setVisibility(View.GONE);
    }

    public void onButtonTakeCallClick(View v) {
        if (!action) {
            action = true;
            checkPermission(REQUEST_MICRO_CODE);
            startAction(2);
        }
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_MICRO_CODE) {
            callAnswer();
        }
    }

    private void startAction(int position) {
        switch (position) {
            case 0:
                mDenyMessage.setEnabled(false);
                mTakeCall.setEnabled(false);
                break;
            case 1:
                mDeny.setEnabled(false);
                mTakeCall.setEnabled(false);
                break;
            case 2:
                mDeny.setEnabled(false);
                mDenyMessage.setEnabled(false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (QilexUtils.verifyAllPermissions(grantResults)) {
            onPreRequestPermission(requestCode);
        }
    }

    private void callAnswer() {
        mCallManager.callAnswer();
    }

    public void onButtonDenyClick(View v) {
        if (!action) {
            action = true;
            startAction(0);
            Toast.makeText(this, R.string.calling_deny, Toast.LENGTH_SHORT).show();
            mCallManager.callReject();
        }
    }

    public void onButtonSendMessageClick(View v) {
        mCallManager.stopRinging();
        showListDenyMessages(mDenyMessages);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCallManager.stopRinging();
        mCallManager.removeSipCallServiceListener(this);
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
        if (mHeadphonesConnectedBroadcastReceiver != null)
            unregisterReceiver(mHeadphonesConnectedBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        if (isCallFinish == true) {
            super.onBackPressed();
        }
    }

    public void notifyFinish() {
        isCallFinish = true;
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                getService().getNotificationFactory().hideOutgoingNotification();
                finish();
            }
        }, 1500);
    }

    // /
    // / Implement methods of QilexSipCallServiceListener
    // /
    @Override
    public void callBusy(SipCall call) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyFinish();
            }
        });
    }

    @Override
    public void callEnded(SipCall call) {
    }

    @Override
    public void callEstablished(SipCall call, final boolean isPushCall) {
        isCallEstablished = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(IncomingCallActivity.this, InCallActivity.class);
                i.putExtra(InCallActivity.INTENT_NAME_PARTNER, txtContactName.getText().toString());
                i.putExtra(InCallActivity.INTENT_NO_PHONE_PARTNER, peerPhoneNumber);
                i.putExtra(InCallActivity.INTENT_IS_CALL_ESTABLISHED, isCallEstablished);
                i.putExtra(INTENT_PARAM_IS_PUSH, isPushCall);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void callCalling(SipCall call) {

    }

    @Override
    public void callRingingBack(SipCall call) {

    }

    @Override
    public void callReconnect(SipCall call) {
        // TODO Auto-generated method stub
    }

    @Override
    public void callReconnectSuccess(SipCall call) {
        // TODO Auto-generated method stub
    }

    @Override
    public void callError(SipCall call, int errCode, final String errMsg) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                notifyFinish();
            }
        });
    }

    @Override
    public void callClosed(SipCall call) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyFinish();
            }
        });
    }

    @Override
    public void turnNotAvailable(SipCall call) {
    }

    Dialog mMessageDlg;

    public void messageDlgDimmis() {
        if (mMessageDlg != null) {
            mMessageDlg.dismiss();
        }
    }

    public void showListDenyMessages(final String[] listMessages) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final AlertDialog.Builder builder = new AlertDialog.Builder(IncomingCallActivity.this);
                View v = LayoutInflater.from(IncomingCallActivity.this).inflate(R.layout.deny_message_dialog, null);
                builder.setView(v);
                ListView lv = (ListView)v.findViewById(R.id.lv_message);
                builder.setCancelable(true);
                builder.setTitle(R.string.dlg_deny_message_title);
                ArrayAdapter<String> productAdapter = new ArrayAdapter<String>(IncomingCallActivity.this,
                        R.layout.simple_list_item, listMessages);
                lv.setAdapter(productAdapter);
                lv.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position < (listMessages.length - 1)) {
                            String contentMessage = listMessages[position];
                            mChatManager.sendSipMessage(-1, peerPhoneNumber, contentMessage, null, false);
                            Toast.makeText(IncomingCallActivity.this, IncomingCallActivity.this.getResources()
                                    .getString(R.string.toast_sent_message, contentMessage), Toast.LENGTH_SHORT).show();
                            mCallManager.callReject();
                        } else {
                            Intent intent = new Intent(IncomingCallActivity.this, ConversationActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra(ConversationActivity.INTENT_PARAM_THREAD_ID, "");
                            intent.putExtra("from_imcomming", true);
                            intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, peerPhoneNumber);
                            IncomingCallActivity.this.startActivity(intent);
                        }
                        messageDlgDimmis();
                    }
                });
                mMessageDlg = builder.create();
                mMessageDlg.show();
            }
        });
    }
}
