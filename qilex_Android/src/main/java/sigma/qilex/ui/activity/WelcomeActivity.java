
package sigma.qilex.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Toast;

import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class WelcomeActivity extends BaseActivity {

    public static final int REQUEST_CODE_LOGIN = 500;

    public static final int REQUEST_ACCOUNT = 501;

    private MyCustomTextView mTxtPolicyLink;

    private AuthenticationManager mAuthenManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.initApplication();
        super.onCreate(savedInstanceState);
        if (Utils.isInKatzMode()) {
            setActionbarTitle(Utils.getString(this, R.attr.stringAppTitle));
            if (Utils.isInMasterMode()) {
                setContentView(R.layout.welcome_activity);
            } else {
                setContentView(R.layout.welcome_activity_slave);
            }
        } else {
            hideActionBar();
            setContentView(R.layout.welcome_activity_green);
        }

        if (!MyApplication.isLibraryLoaded()) {
            showAlert(R.string.app_name, R.string.app_not_compatible);
            return;
        }
        Utils.checkPlayServices(this);
        mAuthenManager = AuthenticationManager.getInstance();
        mTxtPolicyLink = (MyCustomTextView)findViewById(R.id.policy_link);
        mTxtPolicyLink.setText(Html.fromHtml(getString(
                Utils.isInKatzMode() ? R.string.welcome_policy_link :  R.string.welcome_policy_link_green
        )));
        mTxtPolicyLink.setMovementMethod(LinkMovementMethod.getInstance());
        mTxtPolicyLink.setFocusable(true);
        // Check if user is not logged in, start activity login.
        MyApplication.getAppPreferences().edit().putBoolean("qilex_run_first_time", false).commit();
        UserInfo userInfo = UserInfo.getInstance();
        if (userInfo.hasPhoneNumber() && userInfo.hasActiveCode()) {
            if (!mAuthenManager.isLoggedIn()) {
                mAuthenManager.loginWithPhoneNumber(userInfo.getPhoneNumberFormatted());
            }
            startActivity(new Intent(WelcomeActivity.this, MainTabActivity.class));
            finish();
        }
        if (!Utils.isInKatzMode()) {
            checkPermission(REQUEST_CALL_CODE);
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CALL_CODE) {
            if (!QilexUtils.verifyAllPermissions(grantResults)) {
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void onContinueBtnClick(View v) {
        Intent intentActivation = new Intent("sigma.intent.action.FRIFON_ACTIVATION");
        startActivityForResult(intentActivation, REQUEST_CODE_LOGIN);
    }

    public void onYesBtnClick(View v) {
        Intent intentActivation = new Intent("sigma.intent.action.FRIFON_ACTIVATION");
        startActivityForResult(intentActivation, WelcomeActivity.REQUEST_CODE_LOGIN);
    }

    public void onNoBtnClick(View v) {
        Intent intent = new Intent(this, ActivationTabletActivity.class);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_LOGIN) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(WelcomeActivity.this, MainTabActivity.class);
                intent.putExtra(MainTabActivity.INTENT_PARAM_REQUEST_SHOW_ACCOUNT_CONFIGURATION, true);
                intent.putExtra(MainTabActivity.BUNDLE_TAB_START, MainTabActivity.TAB_INDEX_CONTACTS);
                startActivity(intent);
                finish();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

}
