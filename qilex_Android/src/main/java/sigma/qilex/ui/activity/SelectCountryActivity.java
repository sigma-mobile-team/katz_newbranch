
package sigma.qilex.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import pl.frifon.aero2.R;
import sigma.qilex.ui.adapter.CountryListAdapter;
import sigma.qilex.ui.adapter.CountryListAdapter.CountryItem;
import sigma.qilex.ui.customview.ContactListView;

public class SelectCountryActivity extends BaseActivity implements OnItemClickListener {

    public static final String INTENT_PARAM_COUNTRY = "INTENT_PARAM_COUNTRY";

    public static final String INTENT_PARAM_PHONECODE = "INTENT_PARAM_PHONECODE";

    public static final String FORMAT_COUNTRY = "%s (+%s)";

    CountryListAdapter adapter;

    ContactListView listCountry;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_country_activity);
        setTitle(R.string.country_title);
        showBackAction();
        // Initialize controls
        listCountry = (ContactListView)findViewById(R.id.listCountry);
        // Initialize action listener
        initAction();
        // Initialize data
        adapter = new CountryListAdapter(this);
        listCountry.setAdapter(adapter);
        listCountry.setFastScrollEnabled(true);
        adapter.notifyDataSetChanged();
    }

    private void initAction() {
        listCountry.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> root, View view, int position, long id) {
        CountryItem item = adapter.getItem(position);
        if (!item.isCategory()) {
            String resultString = String.format(FORMAT_COUNTRY, item.getName(), item.getPhoneCode());
            Intent data = new Intent();
            data.putExtra(INTENT_PARAM_COUNTRY, resultString);
            data.putExtra(INTENT_PARAM_PHONECODE, item.getPhoneCode());
            setResult(RESULT_OK, data);
            finish();
        }
    }
}
