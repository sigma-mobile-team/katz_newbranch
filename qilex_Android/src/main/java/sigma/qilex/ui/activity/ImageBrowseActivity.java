
package sigma.qilex.ui.activity;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.http.ImageWriteModel;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.chat.ChatManager.DownloadImageListener;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;
import uk.co.senab.photoview.PhotoView;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ImageBrowseActivity extends BaseActivity {

    public static final String INTENT_LIST_URLS = "INTENT_LIST_URLS";

    public static final String INTENT_LIST_COMMENT = "INTENT_LIST_COMMENT";

    public static final String INTENT_CURRENT_INDEX = "INTENT_CURRENT_INDEX";

    public static final String INTENT_LIST_SELECTED_CHAT = "INTENT_LIST_SELECTED_CHAT";

    private static final String ACTION_RELOAD = "ACTION_RELOAD";

    private ViewPager mPager;

    private ImageAdapter mAdapter;

    private static String[] imageUrls;

    /**
     * May be null or jsonString
     */
    private static String[] imageDescription;

    public static ChatMessageModel[] listSelectedChat;

    private static ImageWriteModel[] imageModels;

    private static DownloadImageListener[] listDownloadImageListener;

    String imageFormat;

    private int currentPage;

    private static boolean isActive;

    private static final String INTENT_LOADED_INDEX = "INTENT_LOADED_INDEX";

    BroadcastReceiver reload = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            mAdapter = new ImageAdapter(getSupportFragmentManager());
//            mPager.setAdapter(mAdapter);
//            mPager.setCurrentItem(currentPage);
//            mAdapter.notifyDataSetChanged();
            try {
                int loadedIndex = intent.getExtras().getInt(INTENT_LOADED_INDEX, 0);
                int childCount = mPager.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View child = mPager.getChildAt(i);
                    int childIndex = (Integer)child.getTag();
                    if (loadedIndex == childIndex) {
                        View view = child.findViewById(R.id.image);
                        PhotoView pv = (PhotoView)view.findViewById(R.id.image);
                        Glide.with(MyApplication.getAppContext()).load(new File(imageUrls[loadedIndex]))
                                .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(pv);
                        break;
                    }
                }
            } catch (Exception ex) {
                LogUtils.d("Phuc", "Phuc Exception when loadimge");
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_image_description);
        setBackAction(false);
        actionbarLeft3.setVisibility(View.INVISIBLE);
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        actionbarRight3.setText(R.string.close);
        actionbarRight3.setVisibility(View.VISIBLE);

        // Initialize control
        mPager = (ViewPager)findViewById(R.id.pagerImages);

        checkPermission(REQUEST_STORAGE_CODE);
        registerReceiver(reload, new IntentFilter(ACTION_RELOAD));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageUrls = null;
        imageDescription = null;
        listSelectedChat = null;
        imageModels = null;
        mAdapter = null;
        listDownloadImageListener = null;
        unregisterReceiver(reload);
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_STORAGE_CODE) {
            // Initialize data
            imageUrls = getIntent().getExtras().getStringArray(INTENT_LIST_URLS);
            imageDescription = getIntent().getExtras().getStringArray(INTENT_LIST_COMMENT);
            listDownloadImageListener = new DownloadImageListener[imageDescription.length];
            currentPage = getIntent().getExtras().getInt(INTENT_CURRENT_INDEX);

            // Image Model
            imageModels = new ImageWriteModel[imageDescription.length];
            for (int i = 0; i < imageDescription.length; i++) {
                if (Utils.isStringNullOrEmpty(imageUrls[i])) {
                    try {
                        imageModels[i] = new ImageWriteModel(new JSONObject(imageDescription[i]));
                    } catch (JSONException e) {
                        imageModels[i] = null;
                    }
                } else {
                    imageModels[i] = null;
                }
            }

            imageFormat = getResources().getString(R.string.image) + " %d/" + imageDescription.length;
            actionbarTitle3.setText(String.format(imageFormat, currentPage + 1));

            mAdapter = new ImageAdapter(getSupportFragmentManager());
            mPager = (ViewPager)findViewById(R.id.pagerImages);
            mPager.setAdapter(mAdapter);
            mPager.setCurrentItem(currentPage);
            mAdapter.notifyDataSetChanged();

            initActionListener();
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_CODE) {
            if (QilexUtils.verifyAllPermissions(grantResults)) {
                onPreRequestPermission(requestCode);
            } else {
                
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false;
    }

    private void initActionListener() {
        actionbarRight3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int page) {
                currentPage = page;
                actionbarTitle3.setText(String.format(imageFormat, page + 1));
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    class ImageAdapter extends FragmentStatePagerAdapter {

        public ImageAdapter(FragmentManager fm) {
            super(fm);
            // TODO Auto-generated constructor stub
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public Fragment getItem(int index) {
            // TODO Auto-generated method stub
            return ArrayListFragment.newInstance(index);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return imageUrls.length;
        }

        @Override
        public Object instantiateItem(ViewGroup arg0, int arg1) {
            // TODO Auto-generated method stub
            return super.instantiateItem(arg0, arg1);
        }
    }

    public static class ArrayListFragment extends Fragment {

        static int mNum;

        public static ArrayListFragment newInstance(int num) {
            ArrayListFragment f = new ArrayListFragment();
            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments() != null ? getArguments().getInt("num") : 0;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.image_browse_fragment, null);
            view.setTag(mNum);

            final PhotoView image = (PhotoView)view.findViewById(R.id.image);
            TextView txtDescription = (TextView)view.findViewById(R.id.txtDescription);
            final ProgressBar progress = (ProgressBar)view.findViewById(R.id.loading);
            final TextView txtError = (TextView)view.findViewById(R.id.txtError);

            final RequestListener<File, GlideDrawable> requestListener2 = new RequestListener<File, GlideDrawable>() {
                @Override
                public boolean onResourceReady(GlideDrawable arg0, File arg1, Target<GlideDrawable> arg2, boolean arg3,
                        boolean arg4) {
                    return false;
                }

                @Override
                public boolean onException(Exception arg0, File arg1, Target<GlideDrawable> arg2, boolean arg3) {
                    progress.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    return false;
                }
            };
            if (Utils.isStringNullOrEmpty(imageUrls[mNum])) {
                // Download image
                if (imageModels[mNum] != null) {
                    if (listDownloadImageListener[mNum] == null) {
                        listDownloadImageListener[mNum] = new DownloadImageListener() {
                            int index = mNum;

                            @Override
                            public void onDownloadFinish(String filePath, String token, boolean result) {
                                if (isActive == false) {
                                    return;
                                }
                                if (result == true) {
                                    imageUrls[index] = filePath;
                                    Intent intentReload = new Intent(ACTION_RELOAD);
                                    intentReload.putExtra(INTENT_LOADED_INDEX, index);
                                    MyApplication.getAppContext().sendBroadcast(intentReload);
                                }
                            }
                        };
                    }
                    ChatManager.getInstance().downloadImage(imageModels[mNum].image_uri, imageModels[mNum].token,
                            listSelectedChat[mNum], listDownloadImageListener[mNum], false);
                } else {
                    progress.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                }
            } else {
                Glide.with(MyApplication.getAppContext()).load(new File(imageUrls[mNum]))
                        .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).listener(requestListener2)
                        .into(image);
            }

            txtDescription.setText(listSelectedChat[mNum].getDescription());
            txtDescription.addTextChangedListener(new DescriptionTextWatcher(mNum));
            return view;
        }
    }

    public static class DescriptionTextWatcher implements TextWatcher {
        int index;

        public DescriptionTextWatcher(int index) {
            this.index = index;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            imageDescription[index] = s.toString().trim();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
}
