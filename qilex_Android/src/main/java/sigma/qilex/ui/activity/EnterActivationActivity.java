
package sigma.qilex.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.LoginResponseModel;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.QilexLoginListener;
import sigma.qilex.manager.account.SendActivationListener;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.Utils;

public class EnterActivationActivity extends BaseActivity implements SendActivationListener, QilexLoginListener {

    public static String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";

    AuthenticationManager mAuthenManager;

    TextView txtTitle;

    EditText txtActivationCode;

    String mNumberActivation = "";

    String mActivationCode;

    private Button btnContinue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_activation_code_activity);
        setTitle(Utils.isInKatzMode() ? R.string.activation_code_title : R.string.activation_code_title_green);
        mAuthenManager = AuthenticationManager.getInstance();
        showBackAction();

        // is slave mode
        if (!Utils.isInMasterMode()) {
            TextView tvGuide = (TextView)findViewById(R.id.tvGuide);
            tvGuide.setText(getString(R.string.activation_code_tip_slave));
        }

        boolean ignore = getIntent().getBooleanExtra("ignore_code", false);
        if (ignore) {
            UserInfo.getInstance().setActivationCode("000000");
            UserInfo.getInstance().saveInfo();
            onEnterActivationSuccess();
            return;
        }

        mNumberActivation = UserInfo.getInstance().getPhoneNumberFormatted();
        // Initialize controls
        txtActivationCode = (EditText)findViewById(R.id.txtActivationCode);
        txtTitle = (TextView)findViewById(R.id.txtTitle);
        int smsTitleId = Utils.isInKatzMode() ? R.string.activation_code_title_obtained_sms
                : R.string.activation_code_title_obtained_sms_green;
        txtTitle.setText(Html.fromHtml(getString(smsTitleId)));

        if (Utils.isInKatzMode()) {
            showKeyboard(txtActivationCode);
        } else {
            txtActivationCode.setEnabled(false);
            postHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideKeyboard();
                }
            }, 200);
        }

        btnContinue = (Button)findViewById(R.id.btnContinue);
        txtActivationCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (cs.length() == 6) {
                    mActivationCode = cs.toString();
                    activation(mActivationCode);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                if (arg0.toString().trim().length() > 0) {
                    btnContinue.setEnabled(true);
                } else {
                    btnContinue.setEnabled(false);
                }
            }
        });
        if (!Utils.isInKatzMode()) {
            btnContinue.setVisibility(View.GONE);
        }

        mAuthenManager.registerQilexLoginListener(this);
        registerReceiver(receiverSms, new IntentFilter(ACTION_SMS));
        if (!Utils.isInKatzMode()) {
            checkPermission(REQUEST_CALL_CODE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Remove Login handler
        mAuthenManager.unregisterQilexLoginListener(this);
        try {
            unregisterReceiver(receiverSms);
        }catch (Exception e){ }
    }

    /**
     * Method handler event button Countinue click.
     * 
     * @param v
     */
    public void onContinueBtnClick(View v) {
        mActivationCode = txtActivationCode.getText().toString();
        activation(mActivationCode);
    }

    private void activation(String code) {
        hideKeyboard();
        if (Utils.isStringNullOrEmpty(mActivationCode) == false) {
            mAuthenManager.sendActivationCode(mNumberActivation, mActivationCode, this);
            showSimpleProgress();
        }
    }

    private void onEnterActivationSuccess() {
        // Input corrected validation code
        // 1. Finish ActivationActivity
        setResult(RESULT_OK);
        finish();
    }
    // //
    // // IMPLEMENT METHOD of LoginListener END
    // //

    @Override
    public void onSendActivationFinish(final boolean isSuccess, boolean isFrifon, final ErrorModel error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isSuccess == true || (error != null && error.getByErrorCode(124) != null)) {
                    // Activation success or error code is 124: user has been
                    // activated
                    UserInfo.getInstance().setActivationCode(mActivationCode);
                    UserInfo.getInstance().saveInfo();
                    LogUtils.e("NAMND", "ENTERACTIVATION::LOGIN");
                    mAuthenManager.loginWithPhoneNumber(mNumberActivation);
                } else {
                    hideSimpleProgress();
                    if (error != null) {
                        if (error.getByErrorCode(ErrorModel.CODE_INCORRECT_TOKEN) != null) {
                            showAlert(getString(R.string.activation_code_dialog_title),
                                    getString(R.string.activation_code_dialog_content));
                        } else {
                            showAlert(getString(R.string.activation_fail), error.errors[0].getCurrentErrorMessage());
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onLoginSuccess(LoginResponseModel model) {
        UserInfo.getInstance().setAvatarUrl(model.user_avatar_uri);
        UserInfo.getInstance().setFirstName(model.user_name);
        UserInfo.getInstance().setLastName(model.user_last_name);
        UserInfo.getInstance().saveInfo();
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                hideSimpleProgress();
                onEnterActivationSuccess();
            }
        });
    }

    @Override
    public void onLoginFail(int errorCode, String errorMessage) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                hideSimpleProgress();
                showAlert(R.string.error, R.string.login_fail);
                hideSimpleProgress();
                showAlert("Login fail", "Login Fail!");
            }
        });
    }

    BroadcastReceiver receiverSms = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            try{
                if (bundle != null){
                    Object[] pdusObj = (Object[])bundle.get("pdus");
                    for (int i =0 ; i<pdusObj.length;i++){
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[])pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                        String contentSms = currentMessage.getDisplayMessageBody();
                        String numberActivate = Utils.extractNumber(contentSms);
                        if(numberActivate.length() == 6){
                            txtActivationCode.setText(numberActivate);
                        }
                    }
                }
            }catch (Exception e){
                LogUtils.e("TAG",e.toString());
            }
        }
    };
}
