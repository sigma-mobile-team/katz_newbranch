
package sigma.qilex.ui.activity;

import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.ui.customview.HttpAuthenticationDialog;
import sigma.qilex.utils.Utils;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PolicyActivity extends BaseActivity {

    public static final int KATZIN = 0;

    public static final int KATZOUT = 1;

    public static final int CENNIK = 2;

    public static final int EULA = 3;

    public static final int PRIVACY = 4;

    int typeShow = KATZIN;

    HttpAuthenticationDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.policy_activity);
        Intent intent = getIntent();
        if (intent != null) {
            String scheme = intent.getScheme();
            if (scheme != null) {
                if (scheme.equalsIgnoreCase(Utils.isInKatzMode() ? "katzout" : "frifon_katzout")) {
                    typeShow = KATZOUT;
                    setTitle(Utils.isInKatzMode() ? R.string.regulamin_katz_out : R.string.regulamin_katz_out_green);
                } else if (scheme.equalsIgnoreCase(Utils.isInKatzMode() ? "katzin" : "frifon_katzin")) {
                    typeShow = KATZIN;
                    setTitle(Utils.isInKatzMode() ? R.string.regulamin_katz : R.string.regulamin_katz_green);
                }
                if (scheme.equalsIgnoreCase(Utils.isInKatzMode() ? "eula" : "frifon_eula")) {
                    typeShow = EULA;
                    setTitle(Utils.isInKatzMode() ? R.string.policy_eula : R.string.policy_eula_green);
                } else if (scheme.equalsIgnoreCase(Utils.isInKatzMode() ? "cennik" : "frifon_cennik")) {
                    typeShow = CENNIK;
                    setTitle(R.string.cennik);
                } else if (scheme.equalsIgnoreCase(Utils.isInKatzMode() ? "privacy" : "frifon_privacy")) {
                    typeShow = PRIVACY;
                    setTitle(R.string.privacy_title);
                }
            }
        }
        showBackAction();
        WebView webContent = (WebView)findViewById(R.id.web_content);
        // Make sure we handle clicked links ourselves
        webContent.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedHttpAuthRequest(WebView view, final HttpAuthHandler handler, String host,
                    String realm) {
                if (typeShow == KATZOUT || typeShow == CENNIK) {
                    dialog = new HttpAuthenticationDialog(PolicyActivity.this, host, realm);
                    dialog.setOkListener(new HttpAuthenticationDialog.OkListener() {
                        public void onOk(String host, String realm, String username, String password) {
                            handler.proceed(username, password);
                        }
                    });

                    dialog.setCancelListener(new HttpAuthenticationDialog.CancelListener() {
                        public void onCancel() {
                            handler.cancel();
                        }
                    });
                    dialog.show();
                }
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (typeShow == KATZOUT || typeShow == CENNIK) {
                    handler.proceed();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // we handle the url ourselves if it's a network url (http /
                // https)
                return !URLUtil.isNetworkUrl(url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                showSimpleProgress(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                    }
                });
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                hideSimpleProgress();
            }
        });
        webContent.getSettings().setJavaScriptEnabled(true);
        webContent.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webContent.getSettings().setSupportMultipleWindows(true);
        String url = "";
        if (typeShow == KATZIN) {
            url = Utils.isInKatzMode() ? "file:///android_asset/policy/katzin.html" : "file:///android_asset/policy/frifon-katzin.html";
        } else if (typeShow == KATZOUT)
            url = UserInfo.getInstance().getLocalProfile().getOutRegulationUrl();
        else if (typeShow == EULA) {
            url = Utils.isInKatzMode() ? "file:///android_asset/policy/eula.html" : "file:///android_asset/policy/frifon-eula.html";
        } else if (typeShow == CENNIK) {
            url = getResources().getString(R.string.cennik_link);
        } else if (typeShow == PRIVACY) {
            url = Utils.isInKatzMode() ? "file:///android_asset/policy/privacy.html" : "file:///android_asset/policy/frifon-privacy.html";
        }
        if (!TextUtils.isEmpty(url))
            webContent.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.stopShow();
        }
    }
}
