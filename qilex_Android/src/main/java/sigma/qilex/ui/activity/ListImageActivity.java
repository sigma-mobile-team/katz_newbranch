
package sigma.qilex.ui.activity;

import java.util.ArrayList;

import com.bumptech.glide.Glide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.sqlitedb.ImageGalleryProvider;
import sigma.qilex.ui.adapter.ListImageGalleryAdapter;
import sigma.qilex.ui.adapter.ListImageGalleryAdapter.ImageItem;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexUtils;

public class ListImageActivity extends BaseActivity implements OnItemClickListener, OnItemLongClickListener {

    public static final int REQUEST_CODE_GET_DESCRIPTION = 800;

    private GridView listViewImage;

    private ProgressBar loading;

    ListImageGalleryAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_from_gallery);
        setActionbarTitle(R.string.choose_from_gallery);

        // Init controls
        listViewImage = (GridView)findViewById(R.id.listViewImage);
        loading = (ProgressBar)findViewById(R.id.loading);
        mAdapter = new ListImageGalleryAdapter(this);
        listViewImage.setAdapter(mAdapter);
        listViewImage.setOnItemClickListener(this);
        listViewImage.setOnItemLongClickListener(this);

        setBackAction(true);
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        actionbarRight3.setText(R.string.next);
        actionbarRight3.setVisibility(View.INVISIBLE);
        initActionListener();
        checkPermission(REQUEST_STORAGE_CODE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(this).resumeRequestsRecursive();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GET_DESCRIPTION) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_STORAGE_CODE)
            bindData();
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (QilexUtils.verifyAllPermissions(grantResults)) {
            onPreRequestPermission(requestCode);
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private void initActionListener() {
        actionbarRight3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ImageItem> selectedItems = mAdapter.getSelectedItems();
                String[] selectedUrls = new String[selectedItems.size()];
                for (int i = 0; i < selectedUrls.length; i++) {
                    selectedUrls[i] = selectedItems.get(i).path;
                }
                Intent intent = new Intent(ListImageActivity.this, ImageInputCommentActivity.class);
                intent.putExtra(ImageInputCommentActivity.INTENT_LIST_URLS, selectedUrls);
                intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                        ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT_2);
                startActivityForResult(intent, REQUEST_CODE_GET_DESCRIPTION);
            }
        });
    }

    private void bindData() {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                ArrayList<String> listImage = ImageGalleryProvider.getFilePaths(null);
                bindDataToScreen(listImage);
            }
        });
    }

    private void bindDataToScreen(final ArrayList<String> listImage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(View.GONE);
                mAdapter.setListFile(listImage);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Glide.with(this).pauseRequestsRecursive();
        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra(ImagePreviewActivity.INTENT_IMAGE_URL, mAdapter.getItem(position).path);
        startActivity(intent);
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
        boolean result = mAdapter.clickCheckItem(position);
        if (result == false) {
            Toast.makeText(this, R.string.select_too_much_image, Toast.LENGTH_SHORT).show();
            return;
        }
        mAdapter.notifyDataSetChanged();
        if (mAdapter.getSelectedItems().isEmpty()) {
            actionbarRight3.setVisibility(View.INVISIBLE);
        } else {
            actionbarRight3.setVisibility(View.VISIBLE);
        }
    }
}
