
package sigma.qilex.ui.activity;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.CallInfo;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.I_ThreadMessageDispatcher;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.QilexSoundService;
import sigma.qilex.manager.ThreadMessageDispatcher;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.AuthenticationManager.AcceptRegulationListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.GetContactTempByPhoneListener;
import sigma.qilex.manager.phone.CallLogManager;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.manager.phone.CallManager.CallActivityEvent;
import sigma.qilex.manager.phone.CallManager.QilexSipCallServiceListener;
import sigma.qilex.manager.phone.HeadphonesConnectedBroadcastReceiver;
import sigma.qilex.manager.phone.InCallWakeLock;
import sigma.qilex.sip.SipCall;
import sigma.qilex.ui.customview.CustomButton;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.ui.tab.ChatFragment;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.DateTimeUtils;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class InCallActivity extends BaseActivity implements QilexSipCallServiceListener {

    public static final String ACTION_ENDCALL = "eu.sigma.qilex.ACTION_END_CALL";

    public static final String INTENT_NAME_PARTNER = "INTENT_NAME_PARTNER";

    public static final String INTENT_NO_PHONE_PARTNER = "INTENT_NO_PHONE_PARTNER";

    public static final String INTENT_CALL_OUT = "INTENT_CALL_OUT";

    public static final String INTENT_IS_CALL_ESTABLISHED = "INTENT_IS_CALL_ESTABLISHED";

    public static final String INTENT_ERROR_CODE = "INTENT_ERROR_CODE";

    public static final String INTENT_CONTACT_NAME = "INTENT_CONTACT_NAME";

    public static final String INTENT_CONTACT_AVATAR = "INTENT_CONTACT_AVATAR";

    public static final int END_CALL_CODE = 301;

    public static final int CALL_STATE_REQUEST = 10901;

    private static final int MSG_CALL_TIMEOUT = 1;

    CallManager mCallManager;

    CallLogManager mCallLogManager;

    AuthenticationManager mAuthenManager;

    ContactManager mContactManager;

    QilexSoundService mQilexSoundService;

    private TextView mTxtTitle, mTxtStatus, mTxtContactName, mTxtQualityIndication;

    private ImageView imgEndCall;

    boolean isCallFinish = false;

    private Timer countTimer;

    private TimerTask countTimerTask;

    private int timeSecondStart = 0;

    private int timeSecondCurrent = 0;

    CallInfo mCallInfo;

    private Runnable mCountTimeRunable, mOutgoingCallRunable;

    private CustomButton mBtnGMS, mBtnContacts;

    private String[] mStatusArrs;

    private QilexContact peerContact = null;

    private String peerNumber = null;

    private boolean isOutgoingCall;

    private int getUserInfoProcess;

    private boolean isOpenFromPush = false;

    BroadcastReceiver mCallStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtils.d("HEHE", "HIHI notifyFinish");
            stopCountTime();
            mCallManager.callReject();
            notifyFinish();
        }
    };

    private ThreadMessageDispatcher vrcDispatcher;

    private I_ThreadMessageDispatcher mListenerCallTimeOut = new I_ThreadMessageDispatcher() {
        public void onHandleMessage(Message vrpMsg) {
            switch (vrpMsg.what) {
                case MSG_CALL_TIMEOUT:
                    mCallManager.endCurrentSipCall();
                    notifyFinish(EndCallStateActivity.CALL_OFFLINE);
                    mQilexSoundService.stopAllSignal();
                    mQilexSoundService.playCallFailSignal();
                    break;
            }
        }
    };

    DialogInterface.OnClickListener mOnOK = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            mCallManager.endCurrentSipCall();
            InCallActivity.this.finish();
        }
    };

    HeadphonesConnectedBroadcastReceiver mHeadphonesConnectedBroadcastReceiver;

    private AudioManager mAudioManager = null;

    private SensorManager mSensorManager;

    private Sensor mProximity;

    protected InCallWakeLock mWakeLock;

    GetContactTempByPhoneListener getContactTempListener = new GetContactTempByPhoneListener() {
        @Override
        public void onGetContactFinish(final TempContactModel model, final ErrorModel error) {
            if (model == null) {
                return;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCallInfo.setContactName(model.name);
                    mTxtContactName.setText(model.name);
                }
            });
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Register broadcast receiver
        IntentFilter filter = new IntentFilter(ACTION_ENDCALL);
        registerReceiver(mCallStateReceiver, filter);

        setContentView(R.layout.outgoing_call_activity);
        hideActionBar();
        mCallManager = CallManager.getInstance();
        mCallLogManager = CallLogManager.getInstance();
        mAuthenManager = AuthenticationManager.getInstance();
        mContactManager = ContactManager.getInstance();
        mQilexSoundService = QilexSoundService.getInstance();
        mStatusArrs = getResources().getStringArray(R.array.network_status);
        // Init call timeout listener
        vrcDispatcher = new ThreadMessageDispatcher(mListenerCallTimeOut);

        // Initialize views
        mTxtTitle = (TextView)findViewById(R.id.title);
        mTxtStatus = (TextView)findViewById(R.id.txtStatus);
        mTxtContactName = (TextView)findViewById(R.id.txtContactName);
        mTxtQualityIndication = (TextView)findViewById(R.id.txtQualityIndication);
        mBtnGMS = (CustomButton)findViewById(R.id.btnGsm);
        mBtnGMS.setEnabled(Utils.isInMasterMode());
        mBtnContacts = (CustomButton)findViewById(R.id.btnContact);
        imgEndCall = (ImageView)findViewById(R.id.imgEndCall);
        // Add sip call listener
        mCallManager.addSipCallServiceListener(this);
        isCallFinish = false;
        if (getIntent().getExtras() != null) {
            Bundle extras = getIntent().getExtras();
            mCallInfo = extras.getParcelable("calling_info_notif");
            isOpenFromPush = getIntent().getExtras().getBoolean(IncomingCallActivity.INTENT_PARAM_IS_PUSH, false);
            if (mCallInfo == null) {
                peerNumber = extras.getString(InCallActivity.INTENT_NO_PHONE_PARTNER);
                boolean isKATZOut = extras.getBoolean(InCallActivity.INTENT_CALL_OUT);
                mCallInfo = new CallInfo(peerNumber, isKATZOut);
                // Get contact
                peerContact = mContactManager.getContactByPhoneNoFromPhoneBook(this, peerNumber);
                String contactName = peerNumber;
                if (peerContact != null) {
                    contactName = peerContact.getDisplayName();
                } else {
                    TempContactModel tempContact = mContactManager.getTempContact(peerNumber);
                    if (tempContact != null) {
                        contactName = tempContact.name;
                    } else {
                        contactName = QilexPhoneNumberUtils.formatPhoneNoBlock3Number(peerNumber);
                    }
                }
                mCallInfo.setContactName(contactName);
                // If is open from incoming call
                boolean isCallEstablished = extras.getBoolean(InCallActivity.INTENT_IS_CALL_ESTABLISHED);
                mCallInfo.setEstablished(isCallEstablished);
            }
            mTxtContactName.setText(mCallInfo.getContactName());
            setMode(mCallInfo.isKATZOut());
        }
        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mWakeLock = new InCallWakeLock(this);
        mHeadphonesConnectedBroadcastReceiver = new HeadphonesConnectedBroadcastReceiver();
        registerReceiver(mHeadphonesConnectedBroadcastReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        notifyAudioVolume((float)((float)mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL)
                / (float)mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL)));
        startTracking();
        checkPermission(REQUEST_CALL_CODE);
    }

    private void init() {
        if (mCallInfo != null) {
            if (mCallInfo.isEstablished()) {
                mTxtStatus.setText(Const.STR_EMPTY);
                imgEndCall.setBackgroundResource(R.drawable.btn_end_call);

                // TODO: Check isOpenFromPush
                if (isOpenFromPush) {
                    mTxtStatus.setText(R.string.connecting);
                } else {
                    startCountTime();
                }
            } else {
                vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
                vrcDispatcher.sendEmptyMessageDelayed(MSG_CALL_TIMEOUT, Const.Config.OUTGOING_CALL_TIMEOUT_MILLIS);
            }
            if (!mCallInfo.isEstablished()) {
                isOutgoingCall = true;
                if (NetworkStateMonitor.getNetworkState() == NetworkStateMonitor.NO_NETWORK) {
                    showAlert(R.string.msg_call_no_network_title, R.string.msg_call_no_network_summary, mOnOK);
                    return;
                }
            } else {
                isOutgoingCall = false;
            }
        }
        if (isOutgoingCall) {
            if (mCallInfo.isKATZOut()) {
                int errorCode = mCallManager.startSipCall(peerNumber, true);
                if (errorCode > 0) {
                    showAlert(getString(R.string.call_fail), getString(errorCode),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getService().getNotificationFactory().hideOutgoingNotification();
                                    finish();
                                }
                            });

                }
            } else {
                checkFrifonContact();
            }
        }
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_CALL_CODE) {
            checkPermission(REQUEST_MICRO_CODE);
        } else if (requestCode == REQUEST_MICRO_CODE) {
            init();
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (QilexUtils.verifyAllPermissions(grantResults)) {
            onPreRequestPermission(requestCode);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Notification
        QilexService service = getService();
        if (service != null)
            service.getNotificationFactory().showOutgoingNotification(mCallInfo,
                    getResources().getString(R.string.calling));
    }

    private void setMode(boolean isOUT) {
        mTxtTitle.setText(isOUT ? Utils.getString(this, R.attr.stringKatzOutCall) : Utils.getString(this, R.attr.stringKatzCall));
        mBtnContacts.setVisibility(isOUT ? View.GONE : View.VISIBLE);
        mBtnGMS.setVisibility(isOUT ? View.GONE : View.VISIBLE);
    }

    private void notifyAudioVolume(float audioVolume) {
        Intent intent = new Intent("qilex.CallActivity");
        intent.putExtra("qilex_event", CallActivityEvent.AudioVolume);
        intent.putExtra("volume", audioVolume);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int audioVolume = -1;
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_VOICE_CALL, AudioManager.ADJUST_RAISE,
                        AudioManager.FLAG_SHOW_UI);
                audioVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
                if (audioVolume != -1) {
                    notifyAudioVolume((float)audioVolume
                            / (float)mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL));
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_VOICE_CALL, AudioManager.ADJUST_LOWER,
                        AudioManager.FLAG_SHOW_UI);
                audioVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
                if (audioVolume != -1) {
                    notifyAudioVolume((float)audioVolume
                            / (float)mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL));

                }
                return true;
            default:
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void onButtonEndCallClick(View v) {
        QilexHttpRequest.stopRequest(getUserInfoProcess);
        QilexSoundService.getInstance().stopAllSignal();
        mCallManager.endCurrentSipCall();
        notifyFinish();
    }

    public void onButtonMuteClick(View v) {
        Button button = (Button)v;
        if (mCallManager.isMute()) {
            mCallManager.muteOff();
            button.setSelected(false);
        } else {
            mCallManager.muteOn();
            button.setSelected(true);
        }
    }

    public void onButtonSpeakerClick(View v) {
        Button button = (Button)v;
        if (mCallManager.isSpeakerOn()) {
            mCallManager.speakerOff();
            button.setSelected(false);
        } else {
            mCallManager.speakerOn();
            button.setSelected(true);
        }
    }

    public void onButtonGsmClick(View v) {
        mCallManager.endCurrentSipCall();
        startCall(mCallInfo.getNumberPhone());
    }

    public void onButtonContactClick(View v) {
        Intent intent = new Intent(this, ContactListActivity.class);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_BROWSE_CONTACT_MODE, false);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_BROWSE_CONTACT_TYPE_VIEW_ONLY, true);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_LEFT_OUT_LEFT);
        startActivityForResult(intent, ChatFragment.BROWSER_CONTACT_REQUEST);
    }

    @Override
    protected void onDestroy() {
        vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
        stopTracking();
        stopCountTime();
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
        if (mOutgoingCallRunable != null) {
            postHandler.removeCallbacks(mOutgoingCallRunable);
        }
        mCallManager.removeSipCallServiceListener(this);
        if (mCallStateReceiver != null)
            unregisterReceiver(mCallStateReceiver);
        if (mHeadphonesConnectedBroadcastReceiver != null)
            unregisterReceiver(mHeadphonesConnectedBroadcastReceiver);
        super.onDestroy();
    }

    private synchronized void startTracking() {
        mSensorManager.registerListener(mListenerSensorEvent, mProximity, SensorManager.SENSOR_DELAY_UI);
    }

    private synchronized void stopTracking() {
        if (mSensorManager != null)
            mSensorManager.unregisterListener(mListenerSensorEvent, mProximity);
    }

    @Override
    public void onBackPressed() {
        // nothing
    }

    public void notifyFinish() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgEndCall.setEnabled(false);
            }
        });
        QilexHttpRequest.stopRequest(getUserInfoProcess);
        vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isCallFinish) {
                    isCallFinish = true;
                    getService().getNotificationFactory().hideOutgoingNotification();
                    finish();
                }
            }
        }, 1500);
    }

    public void notifyFinish(final int mode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgEndCall.setEnabled(false);
            }
        });
        QilexHttpRequest.stopRequest(getUserInfoProcess);
        vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // if (!isCallFinish) {
                isCallFinish = true;
                getService().getNotificationFactory().hideOutgoingNotification();
                switchMode(mode);
                // }
            }
        }, 1700);
    }

    /**
     * Start count time.
     */
    public synchronized void startCountTime() {
        stopCountTime();
        timeSecondStart = mCallInfo.getStartTime();
        if (timeSecondStart == 0) {
            timeSecondStart = (int)Calendar.getInstance().getTimeInMillis() / 1000;
            mCallInfo.setStartTime(timeSecondStart);
        }
        countTimerTask = new TimerTask() {
            @Override
            public void run() {
                timeSecondCurrent = (int)Calendar.getInstance().getTimeInMillis() / 1000;
                postHandler.post(mCountTimeRunable = new Runnable() {
                    @Override
                    public void run() {
                        if (mCallInfo.isEstablished()) {
                            String time = DateTimeUtils.formatTimeMMSS(timeSecondCurrent - timeSecondStart);
                            mTxtStatus.setText(time);
                            // Notification
                            QilexService service = getService();
                            if (service != null) {
                                getService().getNotificationFactory().showOutgoingNotification(mCallInfo, time);
                            }
                            // Audio quality
                            int audioLevel = mCallManager.getAudioQualityLevel();
                            mTxtQualityIndication.setText(getNetworkQuality(audioLevel));
                        }
                    }
                });
            }
        };
        countTimer = new Timer();
        countTimer.schedule(countTimerTask, 0, 1000);
    }

    private String getNetworkQuality(int statusIndicator) {
        if (mStatusArrs != null && statusIndicator < mStatusArrs.length)
            return getString(R.string.network_quality, mStatusArrs[statusIndicator]);
        else
            return getString(R.string.network_quality, " ");
    }

    public synchronized void stopCountTime() {
        if (countTimerTask != null) {
            countTimerTask.cancel();
            countTimer.cancel();
            postHandler.removeCallbacks(mCountTimeRunable);
            mCountTimeRunable = null;
            countTimerTask = null;
            countTimer = null;
        }
    }

    private boolean mOtherCallEnd = false;

    @Override
    public void callBusy(SipCall call) {
        mOtherCallEnd = true;
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                mTxtStatus.setText(R.string.call_line_busy);
                notifyFinish(EndCallStateActivity.CALL_BUSY);
            }
        });
    }

    @Override
    public void callEnded(SipCall call) {
        call.setMute(false);
        call.setSpeakerOn(false);
        stopCountTime();
        if (!mOtherCallEnd) {
            mTxtStatus.post(new Runnable() {
                @Override
                public void run() {
                    mTxtStatus.setText(R.string.call_ended);
                    notifyFinish();
                }
            });
        }
        mOtherCallEnd = false;
    }

    @Override
    public void callEstablished(SipCall call, boolean isPushCall) {
        mCallInfo.setEstablished(true);
        vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
        startCountTime();
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                mTxtStatus.setText("");
                imgEndCall.setBackgroundResource(R.drawable.btn_end_call);
            }
        });
    }

    @Override
    public void callCalling(SipCall call) {
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                mTxtStatus.setText(R.string.call_calling);
            }
        });
    }

    @Override
    public void callRingingBack(SipCall call) {
        vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
        vrcDispatcher.sendEmptyMessageDelayed(MSG_CALL_TIMEOUT, Const.Config.OUTGOING_CALL_TIMEOUT_MILLIS);
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                mTxtStatus.setText(R.string.call_ringing);
            }
        });
    }

    @Override
    public void callReconnect(SipCall call) {
        mCallInfo.setEstablished(false);
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                mTxtStatus.setText(R.string.call_reconnect);
                getService().getNotificationFactory().showOutgoingNotification(mCallInfo,
                        getResources().getString(R.string.call_reconnect));
            }
        });
    }

    @Override
    public void callReconnectSuccess(SipCall call) {
        mCallInfo.setEstablished(true);
    }

    @Override
    public void callError(SipCall call, final int errCode, final String errMsg) {
        mOtherCallEnd = true;
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                if (errCode == SipCall.ERR_CALLINGOUT_PAYMENT) {
                    showConfirmDialog(R.string.top_up_your_account, R.string.amount_of_credits, mBuyClick,
                            mCancelBuyClick);
                } else if (errCode == SipCall.ERR_CALL_PAYMENT) {
                    showConfirmDialog(R.string.top_up_your_account, R.string.in_order_to_place, mBuyClick,
                            mCancelBuyClick);
                } else if (errCode == SipCall.ERR_CALL_INVALID) {
                    notifyFinish(EndCallStateActivity.CALL_INVALID_NUMBER);
                } else {
                    notifyFinish(EndCallStateActivity.CALL_OFFLINE);
                }
            }
        });
    }

    @Override
    public void callClosed(SipCall call) {
        stopCountTime();
    }

    @Override
    public void turnNotAvailable(SipCall call) {

    }

    private void switchMode(int mode) {
        // According to Lukazt and Premek solution, remove error mode busy, just
        // only end call and back to previous screen
        if (mode != EndCallStateActivity.CALL_BUSY) {
            Intent intent = new Intent(InCallActivity.this, EndCallStateActivity.class);
            intent.putExtra(EndCallStateActivity.END_CALL_STATE, mode);
            intent.putExtra(InCallActivity.INTENT_CALL_OUT, mCallInfo.isKATZOut());
            intent.putExtra(InCallActivity.INTENT_NAME_PARTNER, mCallInfo.getContactName());
            intent.putExtra(InCallActivity.INTENT_NO_PHONE_PARTNER, mCallInfo.getNumberPhone());
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_BOTTOM_OUT_BOTTOM);
            startActivity(intent);
        }
        finish();
    }

    private void checkFrifonContact() {
        if (peerNumber == null)
            return;
        getUserInfoProcess = ContactManager.getInstance().loadContactInfo(this, peerNumber,
                new GetContactTempByPhoneListener() {
                    @Override
                    public void onGetContactFinish(final TempContactModel model, final ErrorModel error) {
                        // Cannot find user info, end call and set result to
                        // NOT_FRIFON
                        if (error != null) {
                            if (error.getByErrorCode(ErrorModel.CODE_NO_USER) != null
                                    || error.getByErrorCode(ErrorModel.CODE_INVALID_ACTIVE_USER) != null) {
                                vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
                                mCallManager.endCurrentSipCall();
                                if (!isCallFinish) {
                                    notifyFinish(EndCallStateActivity.CALL_NOT_FRIFON);
                                    mQilexSoundService.stopAllSignal();
                                    mQilexSoundService.playCallFailSignal();
                                } else {
                                    notifyFinish();
                                }
                            } else {
                                postHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        vrcDispatcher.removeMessages(MSG_CALL_TIMEOUT);
                                        isCallFinish = true;
                                        mCallManager.endCurrentSipCall();
                                        mQilexSoundService.stopAllSignal();
                                        mQilexSoundService.playCallFailSignal();
                                        showAlert(getString(R.string.call_fail),
                                                getString(R.string.error_cannot_connect_to_server),
                                                new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                getService().getNotificationFactory().hideOutgoingNotification();
                                                finish();
                                            }
                                        });
                                    }
                                });
                            }
                        } else {
                            // Start send sip call INVITE
                            final int errorCode = mCallManager.startSipCall(peerNumber, false);
                            postHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (peerContact == null) {
                                        mTxtContactName.setText(model.name);
                                    }
                                    if (errorCode > 0) {
                                        showAlert(getString(R.string.call_fail), getString(errorCode),
                                                new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                getService().getNotificationFactory().hideOutgoingNotification();
                                                finish();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
    }

    SensorEventListener mListenerSensorEvent = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {

            if (event.values[0] == 0.0f) {
                // DIM SCREEN
                mWakeLock.requestWakeState(InCallWakeLock.WAKE_STATE_PARTIAL);
            } else {
                // SHOW SCREEN
                mWakeLock.requestWakeState(InCallWakeLock.WAKE_STATE_FULL);
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private void checkRegulation() {
        double outRegulationAcceptedVersion = UserInfo.getInstance().getLocalProfile()
                .getOutRegulationAcceptedVersion();
        double outRegulationCurrentVersion = UserInfo.getInstance().getLocalProfile().getOutRegulationCurrentVersion();
        if (outRegulationAcceptedVersion < 0) {
            showAlertWithLink(R.string.katz_out, R.string.katz_out_regulation, mAcceptListenter, mCancelBuyClick);
        } else if (outRegulationCurrentVersion > outRegulationAcceptedVersion) {
            showAlertWithLink(R.string.katz_out, R.string.katz_out_regulation_update, mAcceptListenter,
                    mCancelBuyClick);
        } else {
            Intent intent = new Intent(InCallActivity.this, PaymentActivity.class);
            startActivityForResult(intent, PaymentActivity.REQUEST_CODE);
        }
    }

    Runnable mBuyClick = new Runnable() {
        @Override
        public void run() {
            checkRegulation();
        }
    };

    Runnable mCancelBuyClick = new Runnable() {
        @Override
        public void run() {
            cancelFinish();
        }
    };

    private DialogInterface.OnClickListener mAcceptListenter = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            final int requestId = mAuthenManager.sendAcceptOutRegulation(InCallActivity.this, true,
                    mAcceptRegulationListener);
            showSimpleProgress(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    QilexHttpRequest.stopRequest(requestId);
                }
            });
            dialog.cancel();
        }
    };

    private void cancelFinish() {
        mTxtStatus.post(new Runnable() {
            @Override
            public void run() {
                mTxtStatus.setText(R.string.call_ended);
                notifyFinish();
            }
        });
        notifyFinish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PaymentActivity.REQUEST_CODE && resultCode == RESULT_CANCELED) {
            cancelFinish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private AcceptRegulationListener mAcceptRegulationListener = new AcceptRegulationListener() {

        @Override
        public void onAcceptRegulationFinish(final boolean isSuccess, ErrorModel error) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideSimpleProgress();
                    if (isSuccess) {
                        Intent intent = new Intent(InCallActivity.this, PaymentActivity.class);
                        startActivityForResult(intent, PaymentActivity.REQUEST_CODE);
                    }
                }
            });

        }
    };
}
