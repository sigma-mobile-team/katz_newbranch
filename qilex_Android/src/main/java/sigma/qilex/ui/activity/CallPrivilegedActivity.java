
package sigma.qilex.ui.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.sqlitedb.ContactContentProviderHelper;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;

public class CallPrivilegedActivity extends BaseActivity {
    private static final String CALL_PRIVILEGED = "android.intent.action.CALL_PRIVILEGED";

    String mNumber = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isFinishNow = true;
        if (QilexService.isReady()) {
            Intent localIntent = getIntent();

            if (localIntent != null) {
                String action = localIntent.getAction();
                if (Intent.ACTION_CALL.equals(action) || CALL_PRIVILEGED.equals(action)
                        || Intent.ACTION_DIAL.equals(action)) {
                    String scheme = localIntent.getScheme();
                    String phoneNumber = localIntent.getData().getSchemeSpecificPart();
                    if ("sip".equals(scheme)) {
                        mNumber = phoneNumber.replaceFirst("sip:", "");
                        checkPermission(REQUEST_CALL_CODE);
                    } else if ("tel".equals(scheme)) {
                        phoneNumber = QilexPhoneNumberUtils.convertToBasicNumber(phoneNumber);
                        phoneNumber = QilexPhoneNumberUtils.convertPhoneNo2KATZAddr(phoneNumber);
                        mNumber = phoneNumber.replaceFirst("sip:", "");
                        checkPermission(REQUEST_CALL_CODE);
                    }
                } else {
                    // Get the URI that points to the selected contact
                    final Uri contactUri = localIntent.getData();
                    // We only need the DATA1 column, because there will be only
                    // one
                    // row in the result
                    String[] projection = {
                            Phone.MIMETYPE, Phone.DATA1
                    };
                    Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
                    if (cursor == null) {
                        finish();
                        return;
                    }
                    cursor.moveToFirst();

                    // Get mime type
                    String mimeType = cursor.getString(cursor.getColumnIndex(Phone.MIMETYPE));

                    // Retrieve the phone number from the DATA1 column
                    String number = cursor.getString(cursor.getColumnIndex(Phone.DATA1));

                    if (ContactContentProviderHelper.MIME_TYPE_FRIFON_CALL.equals(mimeType)) {
                        // Free Call
                        int phoneCode = CallManager.getInstance().initOutGoingSipCall(number);
                        if (phoneCode > 0) {
                            isFinishNow = false;
                            showAlert(R.string.call_fail, phoneCode, new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                        }
                    } else if (ContactContentProviderHelper.MIME_TYPE_FRIFON_OUTCALL_QILEX.equals(mimeType)) {
                        // Frifon out call
                        int phoneCode = CallManager.getInstance().initOutGoingSipCall(number, true);
                        if (phoneCode > 0) {
                            isFinishNow = false;
                            showAlert(R.string.call_fail, phoneCode, new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                        }
                    } else if (ContactContentProviderHelper.MIME_TYPE_FRIFON_MESSAGE.equals(mimeType)) {
                        // Free Chat
                        Intent intent = new Intent(this, ConversationActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, number);
                        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                                ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                        startActivity(intent);
                    }
                }
            }
        }
        if (isFinishNow) {
            finish();
        }

    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_CALL_CODE)
            CallManager.getInstance().initOutGoingSipCall(mNumber, true);
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (QilexUtils.verifyAllPermissions(grantResults)) {
            onPreRequestPermission(requestCode);
        }
    }

}
