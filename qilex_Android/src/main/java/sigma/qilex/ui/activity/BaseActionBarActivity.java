
package sigma.qilex.ui.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import pl.katz.aero2.AppManager;
import pl.frifon.aero2.R;

public abstract class BaseActionBarActivity extends IActivity {

    BroadcastReceiver systemExitReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            boolean isSelfLogout = intent.getExtras().getBoolean(BaseActivity.BROADCAST_INTENT_IS_SELF_LOGOUT);
            int contentTitle = R.string.account_is_deactivated;
            int contentMessage = R.string.account_is_deactivated;
            if (isSelfLogout == false) {
                contentTitle = R.string.app_is_logout;
                contentMessage = R.string.app_is_logout_content;
            }
            showAlert(getString(contentTitle), getString(contentMessage), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    AppManager.getAppManager().appExit(context);
                }
            });
            BaseActivity.FLAG_WAITING_FOR_SELF_LOGOUT = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.registerReceiver(systemExitReceiver, new IntentFilter(BaseActivity.BROADCAST_ACTION_LOGOUT));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(systemExitReceiver);
    }

    /**
     * Show alert
     * 
     * @param content
     */
    public void showAlert(final String title, final String content, final DialogInterface.OnClickListener listener) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActionBarActivity.this);
                // Setting Dialog Title
                alertDialog.setTitle(title);

                // Setting Dialog Message
                alertDialog.setMessage(content);

                // on pressing cancel button
                alertDialog.setNegativeButton(getString(R.string.ok), listener);

                alertDialog.setCancelable(false);

                // Showing Alert Message
                alertDialog.show();
            }
        });
    }
}
