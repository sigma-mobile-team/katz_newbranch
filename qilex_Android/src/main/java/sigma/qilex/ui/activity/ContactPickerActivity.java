
package sigma.qilex.ui.activity;

import java.io.File;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import pl.katz.aero2.MyApplication;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

/**
 * Implement add or change contact avt;, must input mImageAvatar is inside in
 * LinearLayout
 * 
 * @author Mr_Invisible
 */
public class ContactPickerActivity extends BaseActivity {

    public final static String FRIFON_AVT_TEMP = "contact_avt_temp.jpg";

    protected Bitmap mBitmapAvtSave = null;

    protected ImageView mImgAvatar;

    protected int mImgSize;

    String mTempFile;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Utils.PHOTO_REQUEST:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    if (Build.VERSION.SDK_INT < 21 && !QilexUtils.isChineseDevices()) {
                        String[] filePathColumn = {
                                MediaStore.Images.Media.DATA
                        };
                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        // perform Crop on the Image Selected from Gallery
                        mTempFile = picturePath;
                        if (QilexUtils.performCrop(this, picturePath)) {
                            break;
                        }
                    }
                    mBitmapAvtSave = Utils.decodeBitmapFromUri(getContentResolver(), selectedImage);
                    String file = Utils.saveToTempFolder(mBitmapAvtSave, FRIFON_AVT_TEMP);
                    MyApplication.getImageLoader().displayImage("file://" + file, mImgAvatar, circleDisplayImageOptions,
                            animateFirstListener);
                }
                break;
            case Utils.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    File file = null;
                    try {
                        file = Utils.createTemporaryFile(FRIFON_AVT_TEMP);
                    } catch (Exception e) {
                        file = null;
                    }
                    if (file == null)
                        return;
                    mTempFile = file.getAbsolutePath();
                    if (Build.VERSION.SDK_INT < 21 && !QilexUtils.isChineseDevices()) {
                        if (QilexUtils.performCrop(this, file.getAbsolutePath())) {
                            break;
                        }
                    }
                    mBitmapAvtSave = Utils.decodeBitmapFromFile(ContactPickerActivity.this, file.getAbsolutePath());
                    MyApplication.getImageLoader().displayImage("file://" + file, mImgAvatar, circleDisplayImageOptions,
                            animateFirstListener);
                }
                break;
            case Utils.CROP_REQUEST:
                if (resultCode == RESULT_OK) {
                    // Bundle extras = data.getExtras();
                    // if (extras != null) {
                    // mBitmapAvtSave = extras.getParcelable("data");
                    // }
                    File filePath = Utils.getTemporaryFile("/avatar.jpg");
                    if (filePath != null) {
                        mBitmapAvtSave = BitmapFactory.decodeFile(filePath.getAbsolutePath());
                        filePath.delete();
                    }
                }
                if (mBitmapAvtSave == null && !TextUtils.isEmpty(mTempFile)) {
                    mBitmapAvtSave = Utils.decodeBitmapFromFile(ContactPickerActivity.this, mTempFile);
                }
                if (mBitmapAvtSave != null) {
                    String file = Utils.saveToTempFolder(mBitmapAvtSave, FRIFON_AVT_TEMP);
                    MyApplication.getImageLoader().displayImage("file://" + file, mImgAvatar, circleDisplayImageOptions,
                            animateFirstListener);
                }
                break;
        }
    }

    public View.OnClickListener onAvatarClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            onChangeAvatar();
        }
    };

    protected android.view.View.OnClickListener onBack = new OnClickListener() {
        @Override
        public void onClick(View v) {
            ContactPickerActivity.this.setResult(RESULT_CANCELED);
            onBackPressed();
        }
    };

}
