package sigma.qilex.ui.activity;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import pl.katz.aero2.AppManager;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.SendActivationListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.facebook.OnLogoutListener;
import sigma.qilex.manager.facebook.SimpleFacebook;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.ui.service.QilexServiceRestartReceiver;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public abstract class BaseActivity extends IActivity {
    public static final String INTENT_PARAM_ANIMATION_TYPE = "INTENT_PARAM_ANIMATION_TYPE";

    public static final String BROADCAST_ACTION_LOGOUT = "BROADCAST_ACTION_LOGOUT";

    public static final String BROADCAST_INTENT_IS_SELF_LOGOUT = "BROADCAST_INTENT_IS_SELF_LOGOUT";
    public static final String BROADCAST_INTENT_IS_CHANGE_SIM = "BROADCAST_INTENT_IS_CHANGE_SIM";

    static public Typeface sTypefaceRoboto;

    static public Typeface sTypefaceRobotoLight;

    static public Typeface sTypefaceRobotoBold;

    static public Typeface sTypefaceRobotoMedium;

    static public Typeface sTypefaceOpenSans;

    public static final int ACTIONBAR_TYPE_NORMAL = 0;

    public static final int ACTIONBAR_TYPE_PHONE_CALL = 1;

    public static final int ACTIONBAR_TYPE_CHAT = 2;

    public static int ACTIONBAR_TYPE_CHAT_DETAIL = 3;

    public static boolean FLAG_WAITING_FOR_SELF_LOGOUT = false;

    private BroadcastReceiver mBroadCastNeedPlayService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showAlert(R.string.warning, R.string.need_google_play_service);
        }
    };

    static {
        sTypefaceRoboto = Typeface.createFromAsset(MyApplication.getAppContext().getAssets(),
                "fonts/Roboto-Regular.ttf");
        sTypefaceRobotoLight = Typeface.createFromAsset(MyApplication.getAppContext().getAssets(),
                "fonts/Roboto-Light.ttf");
        sTypefaceRobotoBold = Typeface.createFromAsset(MyApplication.getAppContext().getAssets(),
                "fonts/Roboto-Bold.ttf");
        sTypefaceRobotoMedium = Typeface.createFromAsset(MyApplication.getAppContext().getAssets(),
                "fonts/Roboto-Medium.ttf");
        sTypefaceOpenSans = Typeface.createFromAsset(MyApplication.getAppContext().getAssets(),
                "fonts/OpenSansRegular.ttf");
    }

    BroadcastReceiver systemExitReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            boolean isChangeSim = intent.getExtras().getBoolean(BROADCAST_INTENT_IS_CHANGE_SIM, false);
            if (isChangeSim) {
                AppManager.getAppManager().appExit(context);
                LogUtils.d("PHUC", "HIHIHIPHUC1 " + systemExitReceiver);
//                showAlert(getString(R.string.sim_is_changed_title), getString(R.string.sim_is_changed_content),
//                        new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        AppManager.getAppManager().appExit(context);
//                    }
//                });
//                FLAG_WAITING_FOR_SELF_LOGOUT = false;
            } else {
                LogUtils.d("PHUC", "HIHIHIPHUC2 " + systemExitReceiver);
                boolean isSelfLogout = intent.getExtras().getBoolean(BROADCAST_INTENT_IS_SELF_LOGOUT);
                int contentTitle = R.string.account_is_deactivated;
                int contentMessage = R.string.account_is_deactivated;
                if (isSelfLogout == false) {
                    contentTitle = R.string.app_is_logout;
                    contentMessage = R.string.app_is_logout_content;
                }
                showAlert(getString(contentTitle), getString(contentMessage), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AppManager.getAppManager().appExit(context);
                    }
                });
                FLAG_WAITING_FOR_SELF_LOGOUT = false;
            }
        }
    };

    protected Animation mAnimShake;

    // Get from ActivityTransitionFactory
    private int[] inOutAnims;

    protected View LeftLayout3;

    protected ImageView actionbarImgRight2;

    protected ImageView actionbarLeft3;

    protected ImageView actionbarLeft2;

    protected TextView actionbarTitle3;

    protected TextView actionbarSubTitle3;

    protected TextView actionbarRight3;

    protected ImageView actionbarLeftIcon;

    protected ImageView actionbarRightIcon;

    protected TextView actionbarTitle;

    protected LinearLayout backLayout;

    protected LinearLayout backLayout3;

    protected ViewFlipper actionbarFlipper;

    protected TextView actionBarRight2;

    protected TextView actionBarLeft2;

    protected TextView actionBarTextLeft3;

    protected Button actionBarButton2All;

    protected Button actionBarButton2Missing;

    // protected ProgressDialog mProgress;

    protected ProgressDialog mSimpleProgress;

    protected Handler postHandler = new Handler();

    public DisplayImageOptions circleDisplayImageOptions;

    public DisplayImageOptions circleDisplayImageOptions2;

    public DisplayImageOptions normalDisplayImageOptions;

    public ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    private int currentDeactivatingId;

    static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }

    public static class ProgressLoadingListener extends AnimateFirstDisplayListener {
        WeakReference<ProgressBar> ref;

        public ProgressLoadingListener(ProgressBar v) {
            ref = new WeakReference<ProgressBar>(v);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            super.onLoadingComplete(imageUri, view, loadedImage);
            ProgressBar v = ref.get();
            if (v != null) {
                v.setVisibility(View.GONE);
            }
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {
            super.onLoadingCancelled(imageUri, view);
            ProgressBar v = ref.get();
            if (v != null) {
                v.setVisibility(View.GONE);
            }
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            super.onLoadingFailed(imageUri, view, failReason);
            ProgressBar v = ref.get();
            if (v != null) {
                v.setVisibility(View.GONE);
            }
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            super.onLoadingStarted(imageUri, view);
            ProgressBar v = ref.get();
            if (v != null) {
                v.setVisibility(View.VISIBLE);
            }
        }

    }

    public static QilexService getService() {
        if (QilexService.isReady()) {
            return QilexService.getInstance();
        }
        return null;
    }

    public void setTitle(int titleRes) {
        actionbarTitle.setText(titleRes);
    }

    public void setActionBarIconLeft(int resId) {
        actionbarLeftIcon.setImageResource(resId);
        actionbarLeft3.setImageResource(resId);
        actionbarLeft2.setImageResource(resId);
    }

    private void setActionBarIconLeftOnClick(View.OnClickListener listener) {
        backLayout.setOnClickListener(listener);
        actionbarLeft3.setOnClickListener(listener);
        actionbarLeft2.setOnClickListener(listener);
    }

    public void setBackAction(boolean visible) {
        // setActionBarIconLeft(R.drawable.back_icon_);
        if (visible) {
            switch (actionbarFlipper.getDisplayedChild()) {
                case ACTIONBAR_TYPE_NORMAL:
                    actionbarLeftIcon.setVisibility(View.VISIBLE);
                    break;
                case ACTIONBAR_TYPE_PHONE_CALL:
                    actionbarLeft2.setVisibility(View.VISIBLE);
                    break;
                case ACTIONBAR_TYPE_CHAT:
                    actionbarLeft3.setVisibility(View.GONE);
                    actionBarTextLeft3.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
            setActionBarIconLeftOnClick(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (BaseActivity.this instanceof MainTabActivity) {
                        onBackPressed();
                    } else if (BaseActivity.this instanceof AccountActivity) {
                        ((AccountActivity) BaseActivity.this).onBackPressed();
                    } else {
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                }
            });
        } else {
            actionbarLeftIcon.setVisibility(View.GONE);
//            actionbarLeft3.setVisibility(View.GONE);
//            actionBarTextLeft3.setVisibility(View.GONE);
//            actionbarLeft2.setVisibility(View.GONE);
        }
    }

    public void showBackAction() {
        setBackAction(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // Check in/out animation
        Intent intentParam = getIntent();
        Bundle extras = intentParam != null ? intentParam.getExtras() : null;
        int animType = extras != null ? extras.getInt(INTENT_PARAM_ANIMATION_TYPE) : 0;
        if (animType != 0) {
            inOutAnims = ActivityTransitionFactory.getAnimation(animType);
            overridePendingTransition(inOutAnims[0], inOutAnims[2]);

        }
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        View customView = mInflater.inflate(R.layout.actionbar_layout, null);
        backLayout = (LinearLayout) customView.findViewById(R.id.menu_back);
        actionbarLeftIcon = (ImageView) customView.findViewById(R.id.left_icon);
        actionbarLeft2 = (ImageView) customView.findViewById(R.id.left2_icon);
        actionbarLeft3 = (ImageView) customView.findViewById(R.id.imgMenuRight3);
        actionbarRight3 = (TextView) customView.findViewById(R.id.txtRight3);
        actionBarTextLeft3 = (MyCustomTextView) customView.findViewById(R.id.txtLeft3);
        actionbarRightIcon = (ImageView) customView.findViewById(R.id.right_icon);
        actionbarTitle = (MyCustomTextView) customView.findViewById(R.id.actionbar_title);
        actionbarTitle3 = (MyCustomTextView) customView.findViewById(R.id.actionbar_title3);
        actionbarSubTitle3 = (MyCustomTextView) customView.findViewById(R.id.actionbar_sub_title3);
        actionbarFlipper = (ViewFlipper) customView.findViewById(R.id.flipperActionbar);
        actionBarLeft2 = (TextView) customView.findViewById(R.id.txtDelete);
        actionBarRight2 = (TextView) customView.findViewById(R.id.txtEdit);
        actionbarImgRight2 = (ImageView) customView.findViewById(R.id.img_right_actionbar2);
        actionBarButton2All = (Button) customView.findViewById(R.id.btnAll);
        actionBarButton2Missing = (Button) customView.findViewById(R.id.btnMissing);
        actionbarSubTitle3.setVisibility(View.GONE);// default is gone
        actionBarRight2.setVisibility(View.GONE);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(customView);
        actionBar.setDisplayShowCustomEnabled(true);
        // Image display options
        circleDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.avt_default_circle_loading)
                .showImageForEmptyUri(R.drawable.avt_default_circle).showImageOnFail(R.drawable.avt_default_circle)
                .displayer(new RoundedBitmapDisplayer(1000)).cacheInMemory(true).cacheOnDisk(true)
                .considerExifParams(true).build();
        circleDisplayImageOptions2 = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.avt_default_loading).showImageForEmptyUri(R.drawable.avt_default)
                .showImageOnFail(R.drawable.avt_default).displayer(new RoundedBitmapDisplayer(1000)).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true).build();
        normalDisplayImageOptions = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.avt_default_loading)
                .showImageForEmptyUri(R.drawable.avt_default).showImageOnFail(R.drawable.avt_default)
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true).build();
        this.registerReceiver(systemExitReceiver, new IntentFilter(BROADCAST_ACTION_LOGOUT));
    }

    @Override
    protected void onStart() {
        if (Utils.isInMasterMode() == true) {
            if (QilexService.lastTimeIsInBackground && QilexUtils.hasSelfPermission(this, CONTACT_PERMISSIONS)) {
                Utils.stageQueue.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        ContactManager.getInstance().checkForSyncContact();
                    }
                });
            }
        }
        super.onStart();

        this.registerReceiver(mBroadCastNeedPlayService,
                new IntentFilter(MyApplication.BROADCAST_ACTION_NEED_PLAY_SERVICE));
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.mainInterfacePaused = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.mainInterfacePaused = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.stageQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                QilexService.lastTimeIsInBackground = MyApplication.isApplicationInBackground();
            }
        });
        this.unregisterReceiver(mBroadCastNeedPlayService);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSimpleProgress = null;
        this.unregisterReceiver(systemExitReceiver);
    }

    @Override
    public void finish() {
        super.finish();
        hideKeyboard();

        if (inOutAnims != null) {
            overridePendingTransition(inOutAnims[3], inOutAnims[1]);
        }
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
    }

    public void setActionbarTitle(int title) {
        actionbarTitle.setText(title);
        actionbarTitle3.setText(title);
    }

    public void setActionbarTitle(String title) {
        actionbarTitle.setText(title);
        actionbarTitle3.setText(title);
    }

    public void showSimpleProgress() {
        // Simple progress bar
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
        mSimpleProgress = new ProgressDialog(BaseActivity.this, R.style.QilexProgressBarDialog);
        mSimpleProgress.setCancelable(false);
        mSimpleProgress.show();
        mSimpleProgress.setContentView(R.layout.progress_simple);
    }

    public void showSimpleProgress(OnCancelListener onCancelListener) {
        // Simple progress bar
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
        mSimpleProgress = new ProgressDialog(BaseActivity.this, R.style.QilexProgressBarDialog);
        mSimpleProgress.setCancelable(true);
        mSimpleProgress.setOnCancelListener(onCancelListener);
        mSimpleProgress.show();
        mSimpleProgress.setContentView(R.layout.progress_simple);
    }

    public void hideSimpleProgress() {
        if (mSimpleProgress != null) {
            mSimpleProgress.dismiss();
        }
    }

    public void showSimpleProgressByPostHandler() {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                showSimpleProgress();
            }
        });
    }

    public void showSimpleProgressByPostHandler(final OnCancelListener onCancelListener) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                showSimpleProgress(onCancelListener);
            }
        });
    }

    public void hideSimpleProgressByPostHandler() {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                hideSimpleProgress();
            }
        });
    }

    public void notifyToast(int stringId) {
        Toast.makeText(BaseActivity.this, getString(stringId), Toast.LENGTH_SHORT).show();
    }

    public void showConfirmDialog(int title, int content, final Runnable acceptedAction) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(getString(title));

        // Setting Dialog Message
        alertDialog.setMessage(getString(content));

        // on pressing cancel button
        alertDialog.setPositiveButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                acceptedAction.run();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void showConfirmDialog(int title, int content, final Runnable acceptedAction, final Runnable cancelAction) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(getString(title));

        // Setting Dialog Message
        alertDialog.setMessage(getString(content));

        // on pressing cancel button
        alertDialog.setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                cancelAction.run();
            }
        });
        alertDialog.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                acceptedAction.run();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void showConfirmDialogNoYes(int title, int content, final Runnable acceptedAction) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(getString(title));

        // Setting Dialog Message
        alertDialog.setMessage(getString(content));

        // on pressing cancel button
        alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                acceptedAction.run();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    /**
     * Show alert
     *
     * @param content
     */
    public void showAlert(int titleId, String content) {
        String title = getResources().getString(titleId);
        showAlert(title, content);
    }

    /**
     * Show alert
     *
     * @param content
     */
    public void showAlert(String title, String content) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(content);
        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void showAlert(int title, int content, final DialogInterface.OnClickListener onClick) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(getString(title));

        // Setting Dialog Message
        alertDialog.setMessage(getString(content));

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onClick.onClick(dialog, which);
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void showAlertWithLink(int titleId, int contentId, DialogInterface.OnClickListener onClick) {
        showAlertWithLink(titleId, contentId, onClick, null);
    }

    public void showAlertWithLink(int titleId, int contentId, DialogInterface.OnClickListener onClick,
                                  final Runnable cancelRunable) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_simple_with_link, null);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setMovementMethod(LinkMovementMethod.getInstance());
        text.setText(Html.fromHtml(getString(contentId)));
        alertDialog.setView(layout);
        // Setting Dialog Title
        alertDialog.setTitle(getString(titleId));
        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (cancelRunable != null)
                    cancelRunable.run();
                dialog.cancel();
            }
        });
        alertDialog.setPositiveButton(R.string.yes, onClick);
        // Showing Alert Message
        alertDialog.show();
    }

    public void showAlertWarning(int title, int content, DialogInterface.OnClickListener onClick) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(getString(title));

        // Setting Dialog Message
        alertDialog.setMessage(getString(content));

        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setPositiveButton(R.string.continue_, onClick);

        // Showing Alert Message
        alertDialog.show();
    }

    /**
     * Show alert
     *
     * @param content
     */
    public void showAlert(final String title, final String content, final DialogInterface.OnClickListener listener) {
        this.runOnUiThread(new Runnable() {
            public void run() {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle(title);

                // Setting Dialog Message
                alertDialog.setMessage(content);

                // on pressing cancel button
                alertDialog.setNegativeButton(getString(R.string.ok), listener);

                alertDialog.setCancelable(false);

                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    public void bindQilexService() {
        // Intent intent = new Intent(getApplicationContext(),
        // QilexService.class);
        // getApplicationContext().startService(intent);
        sendBroadcast(new Intent(QilexServiceRestartReceiver.ACTION_QILEX_SERVICE_RESTART));
    }

    public void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    public void hideEditButton() {
        actionBarRight2.setVisibility(View.GONE);
    }

    /**
     * Set type of action bar.
     *
     * @param type is one of ACTIONBAR_TYPE_NORMAL, ACTIONBAR_TYPE_PHONE_CALL.
     */
    public void setActionBarType(int type) {
        actionbarFlipper.setDisplayedChild(type);
    }

    /**
     * * Set actionlistener for action bar controls in case type is
     * ACTIONBAR_TYPE_PHONE_CALL.
     *
     * @param actionCenterButtonLeft
     * @param actionCenterButtonRight
     */
    public void setActionBarType2Action(final OnClickListener actionCenterButtonLeft,
                                        final OnClickListener actionCenterButtonRight) {
        setActionBarType2Action(null, actionCenterButtonLeft, actionCenterButtonRight, null);
    }

    /**
     * * Set actionlistener for action bar controls in case type is
     * ACTIONBAR_TYPE_PHONE_CALL.
     *
     * @param actionCenterButtonLeft
     * @param actionCenterButtonRight
     * @param actionRight
     * @param imageRightResource : image resource for image right button
     */
    public void setActionBarType2Action(final OnClickListener actionCenterButtonLeft,
                                        final OnClickListener actionCenterButtonRight, final OnClickListener actionRight, int imageRightResource) {
        setActionBarType2Action(null, actionCenterButtonLeft, actionCenterButtonRight, actionRight, imageRightResource);
    }

    /**
     * Set actionlistener for action bar controls in case type is
     * ACTIONBAR_TYPE_PHONE_CALL.
     *
     * @param actionLeft
     * @param actionCenterButtonLeft
     * @param actionCenterButtonRight
     * @param actionRight
     */
    public void setActionBarType2Action(final OnClickListener actionLeft, final OnClickListener actionCenterButtonLeft,
                                        final OnClickListener actionCenterButtonRight, final OnClickListener actionRight) {
        setActionBarType2Action(actionLeft, actionCenterButtonLeft, actionCenterButtonRight, actionRight, 0);
    }

    /**
     * Set actionlistener for action bar controls in case type is
     * ACTIONBAR_TYPE_PHONE_CALL.
     *
     * @param actionLeft
     * @param actionCenterButtonLeft
     * @param actionCenterButtonRight
     * @param actionRight
     */
    private void setActionBarType2Action(final OnClickListener actionLeft, final OnClickListener actionCenterButtonLeft,
                                         final OnClickListener actionCenterButtonRight, final OnClickListener actionRight, int imageRightResource) {
        if (imageRightResource > 0) {
            actionBarRight2.setVisibility(View.GONE);
            actionBarLeft2.setVisibility(View.GONE);
            actionbarImgRight2.setVisibility(View.VISIBLE);
            actionbarImgRight2.setImageResource(imageRightResource);
            actionbarImgRight2.setOnClickListener(actionRight);
        } else {
            actionBarRight2.setVisibility(View.INVISIBLE);
            actionBarLeft2.setVisibility(View.VISIBLE);
            actionbarImgRight2.setVisibility(View.GONE);
            actionBarLeft2.setOnClickListener(actionLeft);
            actionBarRight2.setOnClickListener(actionRight);
        }
        actionBarButton2All.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Check if btnAll is select, not do anything
                if (actionBarButton2All.getTag() != null && ((Boolean) actionBarButton2All.getTag()) == true) {
                    return;
                }
                // Set selected status
                actionBarButton2All.setTag(true);
                actionBarButton2Missing.setTag(false);
                // Change button background
                actionBarButton2All.setSelected(true);
                actionBarButton2Missing.setSelected(false);

                if (actionCenterButtonLeft != null) {
                    actionCenterButtonLeft.onClick(v);
                }
            }
        });

        actionBarButton2Missing.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Check if btnMissing is select, not do anything
                if (actionBarButton2Missing.getTag() != null && ((Boolean) actionBarButton2Missing.getTag()) == true) {
                    return;
                }

                // Set selected status
                actionBarButton2All.setTag(false);
                actionBarButton2Missing.setTag(true);
                // Change button background
                actionBarButton2All.setSelected(false);
                actionBarButton2Missing.setSelected(true);

                if (actionCenterButtonRight != null) {
                    actionCenterButtonRight.onClick(v);
                }
            }
        });
    }

    public void setActionBarType3Action(final OnClickListener actionLeft, final OnClickListener actionRight) {
        actionbarLeft3.setOnClickListener(actionLeft);
        actionBarTextLeft3.setOnClickListener(actionLeft);
        actionbarRight3.setOnClickListener(actionRight);
    }

    public void setActionbarLeft3Visible(int visible) {
        actionbarLeft3.setVisibility(View.GONE);
        actionBarTextLeft3.setVisibility(visible);
    }

    public void setActionbarRight3Visible(int visible) {
        actionbarRight3.setVisibility(visible);
    }

    public void setActionbarLeft3Title(int titleId) {
        actionbarLeft3.setVisibility(View.GONE);
        actionBarTextLeft3.setVisibility(View.VISIBLE);
        actionBarTextLeft3.setText(titleId);
    }

    public void setAtionbarLeft3Icon(int resId) {
        actionBarTextLeft3.setCompoundDrawablesWithIntrinsicBounds(resId, 0, 0, 0);
        actionBarTextLeft3.setCompoundDrawablePadding(10);
    }

    public void setAtionbarRight3Icon(int resId) {
        actionbarRight3.setText("");
        actionbarRight3.setCompoundDrawablesWithIntrinsicBounds(resId, 0, 0, 0);
    }

    public void setAtionbarRight3Title(int resId) {
        actionbarRight3.setText(resId);
    }

    public void setActionbarTitle3TextSize(float sizeSP) {
        actionbarTitle3.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeSP);
    }

    protected void resetActionBarCallLogButtons(boolean isContact, int type) {
        resetActionBarCallLogButtons(isContact, type == 1);
    }

    protected void resetActionBarCallLogButtons(boolean isContact, boolean modeEdit) {
        actionBarLeft2.setVisibility(View.INVISIBLE);
        if (isContact) {
            actionBarButton2Missing.setText(Utils.getString(this, R.attr.stringAppTitle));
        } else {
            actionBarButton2Missing.setText(R.string.missing);
        }
        if (!modeEdit) {
            // Set selected status:view
            actionBarButton2All.setTag(true);
            actionBarButton2Missing.setTag(false);
            actionBarButton2All.setSelected(true);
            actionBarButton2Missing.setSelected(false);
        } else {
            // Set selected status: edit mode
            actionBarButton2All.setTag(false);
            actionBarButton2Missing.setTag(true);
            actionBarButton2All.setSelected(false);
            actionBarButton2Missing.setSelected(true);
        }

    }

    // binhdt: show / hide keyboard promatically
    public void showKeyboard(final View input) {
        input.requestFocus();
        input.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(input, 0);
            }
        }, 100);
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void confirmLogout() {
        showConfirmDialogNoYes(R.string.logout_account, R.string.confirm_logout_account, new Runnable() {
            @Override
            public void run() {
                userLogout();
            }
        });
    }

    public void confirmDeactivate() {
        showConfirmDialogNoYes(R.string.deactive_account, R.string.confirm_deactive_account, new Runnable() {
            @Override
            public void run() {
                userDeactivate();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == PaymentActivity.REQUEST_CODE) {
            final int msgId = data.getIntExtra(PaymentActivity.BUNDLE_PAYMENT, 0);
            if (msgId > 0)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showAlert(R.string.post_account_payment, msgId);
                    }

                    ;
                });
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void userLogout() {
        // Call Service
        QilexService.getInstance().userLogout();

        // Stop service
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AppManager.getAppManager().appExit(BaseActivity.this);
            }
        }, 1000);

    }

    private void userDeactivate() {
        FLAG_WAITING_FOR_SELF_LOGOUT = true;
        AuthenticationManager authenManager = AuthenticationManager.getInstance();
        showSimpleProgressByPostHandler(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                QilexHttpRequest.stopRequest(currentDeactivatingId);
            }
        });
        currentDeactivatingId = authenManager.sendUnregister(UserInfo.getInstance().getPhoneNumberFormatted(),
                new SendActivationListener() {
                    @Override
                    public void onSendActivationFinish(boolean isSuccess, boolean isFrifon, ErrorModel error) {
                        hideSimpleProgressByPostHandler();
                        if (isSuccess == true || (error != null && error.getByErrorCode(-1) == null)) {
                            SimpleFacebook mSimpleFacebook = SimpleFacebook.getInstance(BaseActivity.this);
                            if (mSimpleFacebook.isLogin()) {
                                mSimpleFacebook.logout(new OnLogoutListener() {
                                    @Override
                                    public void onLogout() {
                                        LogUtils.d("binhdt", "Logout facebook successful");
                                    }
                                });
                            }
                            QilexService.getInstance().userDeactivate(true, true);
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showAlert(R.string.deactivation_fail, R.string.deactivation_fail_message);
                                }
                            });
                        }
                    }
                });
    }

    public void onChangeAvatar() {
        checkPermission(REQUEST_STORAGE_CODE);
    }

    public void startCamera() {
        checkPermission(REQUEST_CAMERA_CODE);
    }

    String mCallNo = null;

    public void startCall(String callNo) {
        mCallNo = callNo;
        startIntentCall();
        // checkPermission(REQUEST_CALL_CODE);
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_STORAGE_CODE)
            Utils.showChangeAvtDialog(BaseActivity.this);
        else if (requestCode == REQUEST_CAMERA_CODE)
            startCameraActivity();
        else if (requestCode == REQUEST_CALL_CODE)
            startIntentCall();

    }

    private void startIntentCall() {
        if (mCallNo != null) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mCallNo));
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(intent);
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (QilexUtils.verifyAllPermissions(grantResults)) {
            onPreRequestPermission(requestCode);
        } else {
            if (requestCode == REQUEST_CALL_CODE)
                mCallNo = null;
        }
    }

    private void startCameraActivity() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            String fileName = "";
            if (this instanceof AccountConfigurationActivity) {
                fileName = Utils.mAvatarFileName;
            } else {
                fileName = ContactPickerActivity.FRIFON_AVT_TEMP;
            }
            File photo = Utils.createTemporaryFile(fileName);
            photo.delete();
            MemoryCacheUtils.removeFromCache(Uri.fromFile(photo).toString(),
                    ImageLoader.getInstance().getMemoryCache());
            DiskCacheUtils.removeFromCache(Uri.fromFile(photo).toString(), ImageLoader.getInstance().getDiskCache());
            Uri imgUri = Uri.fromFile(photo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        } catch (Exception e) {
            return;
        }
        // start camera intent
        startActivityForResult(intent, Utils.CAMERA_REQUEST);
    }
}
