
package sigma.qilex.ui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.chat.ChatManager.ChatMessageChangeListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.phone.CallLogChangeListener;
import sigma.qilex.manager.phone.CallLogManager;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.tab.AccountFragment;
import sigma.qilex.ui.tab.CallLogFragment;
import sigma.qilex.ui.tab.ChatFragment;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class MainTabActivity extends BaseActivity
        implements ChatMessageChangeListener, CallLogChangeListener, OnLayoutChangeListener {

    public static boolean DEBUG = false;

    public static final String BROADCAST_SELECT_TAB = "eu.sigma.qilex.BROADCAST_SELECT_TAB";

    public static String INTENT_PARAM_REQUEST_SHOW_ACCOUNT_CONFIGURATION = "INTENT_PARAM_REQUEST_SHOW_ACCOUNT_CONFIGURATION";

    public static String BUNDLE_TAB_START = "bundle_tab_start";

    public static final int TAB_INDEX_CALLS = 0;

    public static final int TAB_INDEX_CHAT = 1;

    public static final int TAB_INDEX_CONTACTS = 2;

    public static final int TAB_INDEX_KEYPAD = 3;

    public static final int TAB_INDEX_ACCOUNT = 4;

    public static int mStartDefaultTabIndex = TAB_INDEX_CONTACTS;

    public static final String TAB_CALLS = "tab_calls";

    public static final String TAB_CHAT = "tab_chat";

    public static final String TAB_CONTACTS = "tab_contacts";

    public static final String TAB_KEYPAD = "tab_keypad";

    public static final String TAB_ACCOUNT = "tab_account";

    public static boolean requestSyncContactWhenCreate = false;

    private TabHost mTabHost;

    /* A HashMap of stacks, where we use tab identifier as keys.. */
    private HashMap<String, Stack<Fragment>> mStacks;

    /* Save current tabs identifier in this.. */
    private String mCurrentTab;

    private TextView mTxtMissCallNotify;

    private View mTxtMissCallNotifyLayout;

    private TextView mTxtUnreadMsgNotify;

    private View mTxtUnreadMsgNotifyLayout;

    private boolean isChatEditMode;

    private ChatManager mChatService;

    private BroadcastReceiver chatCloseReceiver;

    private boolean isForceSelectChatFragment = false;

    private boolean isForceSelectCallFragment = false;

    private ContactManager mContactManager;

    private CallLogManager mCallLogManager;

    OnClickListener actionCallCenterButtonLeft = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCurrentTab.equals(TAB_CALLS)) {
                getCallLogFragment().changeListMode(CallLogFragment.VIEW_MODE_ALL);
            } else if (mCurrentTab.equals(TAB_CONTACTS)) {
                getContactListFragment().onChangeTypeList(ContactListFragment.VIEW_ALL);
            }
        }
    };

    OnClickListener actionCallCenterButtonRight = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCurrentTab.equals(TAB_CALLS)) {
                getCallLogFragment().changeListMode(CallLogFragment.VIEW_MODE_MISS_CALL);
            } else if (mCurrentTab.equals(TAB_CONTACTS)) {
                getContactListFragment().onChangeTypeList(ContactListFragment.VIEW_FRIFON);
            }
        }
    };

    // Initialize action of Call Tab
    OnClickListener actionCallLeft = new OnClickListener() {
        @Override
        public void onClick(View v) {
            getCallLogFragment().deleteSelected();
        }
    };

    OnClickListener actionCallRight = new OnClickListener() {
        @Override
        public void onClick(View v) {
            CallLogFragment callLogFragment = getCallLogFragment();
            if (callLogFragment.isEditMode()) {
                setCallEditMode(false);
            } else {
                setCallEditMode(true);
            }
        }
    };

    OnClickListener actionContactRight = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainTabActivity.this, AddContactActivity.class);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            startActivity(intent);
        }
    };

    OnClickListener actionChatLeft = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isChatEditMode) {
                setChatEditMode(false);
                onClickChatRow();
            } else {
                Intent intent = new Intent(MainTabActivity.this, ContactListActivity.class);
                intent.putExtra(ContactListFragment.EXTRAS_DATA_BROWSE_CONTACT_MODE, true);
                intent.putExtra("frifon_exists_numbers", new String[]{UserInfo.getInstance().getPhoneNumberFormatted()});
                intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_LEFT_OUT_LEFT);
                startActivityForResult(intent, ChatFragment.BROWSER_CONTACT_REQUEST);
            }
        }
    };

    OnClickListener actionChatRight = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isChatEditMode) {
                final long[] selectedId = getChatFragment().getAdapter().getListSelectedIds();
                if (selectedId.length == 0) {
                    Toast.makeText(MainTabActivity.this, "O item select", Toast.LENGTH_SHORT).show();
                } else {
                    mChatService.deleteChatThread(selectedId);
                    getChatFragment().getAdapter().clearSelectedIds();
                }
            } else {
                setChatEditMode(true);
                onClickChatRow();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tab_activity);
        mContactManager = ContactManager.getInstance();
        mCallLogManager = CallLogManager.getInstance();
        initActivity();
        checkPermission(REQUEST_CONTACTS_CODE);
        int tabIndex = getIntent().getIntExtra(BUNDLE_TAB_START, mStartDefaultTabIndex);
        setCurrentTab(tabIndex);
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_CONTACTS_CODE) {
            if (Utils.isInMasterMode() == true) {
                Utils.stageQueue.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        mContactManager.reloadAllContact();
                        ContactManager.getInstance().checkForSyncContact();
                    }
                });
            }
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (QilexUtils.verifyAllPermissions(grantResults)) {
            onPreRequestPermission(requestCode);
        }
    }

    private void initActivity() {
        mChatService = ChatManager.getInstance();
        postHandler = new Handler();
        /*
         * Navigation stacks for each tab gets created.. tab identifier is used
         * as key to get respective stack for each tab
         */
        mStacks = new HashMap<String, Stack<Fragment>>();
        mStacks.put(TAB_CALLS, new Stack<Fragment>());
        mStacks.put(TAB_CHAT, new Stack<Fragment>());
        mStacks.put(TAB_CONTACTS, new Stack<Fragment>());
        mStacks.put(TAB_KEYPAD, new Stack<Fragment>());
        mStacks.put(TAB_ACCOUNT, new Stack<Fragment>());

        mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setOnTabChangedListener(listener);
        mTabHost.setup();
        mTabHost.getTabWidget().setDividerDrawable(null);
        mTabHost.addOnLayoutChangeListener(this);
        initializeTabs();

        // Initialize action listener
        initActionListeners();

        // binhdt: hide btn Delete when we lauch app in the first time
        resetActionBarCallLogButtons(false, false);

        mChatService.addChatMessageChangeListener(this);
        mCallLogManager.addCallLogChangeListener(this);

        // register receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConversationActivity.INTENT_PARAM_BROADCAST_ID);

        chatCloseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                isForceSelectChatFragment = true;
            }
        };
        registerReceiver(chatCloseReceiver, filter);

        registerReceiver(mNotificationReceiver, new IntentFilter(BROADCAST_SELECT_TAB));

    }

    BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int indexTab = intent.getIntExtra(MainTabActivity.BUNDLE_TAB_START, -1);
            if (indexTab >= 0 && indexTab <= TAB_INDEX_ACCOUNT) {
                setCurrentTab(indexTab);
            }
        }

    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public static boolean active;

    @Override
    protected void onStart() {
        active = true;
        super.onStart();
        bindQilexService();
        mContactManager.reloadAllContact();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMissCallNotifyNumber(mCallLogManager.countMissCall());
        setUnreadMsgNotifyNumber(mChatService.countUnreadThread());
        if (mStartDefaultTabIndex != TAB_INDEX_CONTACTS) {
            setCurrentTab(mStartDefaultTabIndex);
            mStartDefaultTabIndex = TAB_INDEX_CONTACTS;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (isForceSelectChatFragment && !mCurrentTab.equals(TAB_CHAT)) {
            setCurrentTab(TAB_INDEX_CHAT);
            isForceSelectChatFragment = false;
        } else if (isForceSelectCallFragment && !mCurrentTab.equals(TAB_INDEX_CALLS)) {
            setCurrentTab(TAB_INDEX_CALLS);
            isForceSelectCallFragment = false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unregister all listener
        mChatService.removeChatMessageChangeListener(this);
        mCallLogManager.removeCallLogChangeListener(this);
        unregisterReceiver(chatCloseReceiver);
        if (mNotificationReceiver != null) {
            unregisterReceiver(mNotificationReceiver);
        }
        removeAllTabListener();
    }

    private View createTabView(Drawable idImage, int idTitle) {
        View view = LayoutInflater.from(this).inflate(R.layout.tab_item_layout, null);
        ImageView imageView = (ImageView)view.findViewById(R.id.icon_tab);
        imageView.setImageDrawable(idImage);
        MyCustomTextView title = (MyCustomTextView)view.findViewById(R.id.title_tab);
        title.setText(idTitle);
        return view;
    }

    private void initActionListeners() {

        setActionBarType2Action(actionCallLeft, actionCallCenterButtonLeft, actionCallCenterButtonRight,
                actionCallRight);

        // Initialize action of Chat Tab
        setActionBarType3Action(actionChatLeft, actionChatRight);
    }

    public void initializeTabs() {
        initTab(TAB_INDEX_CALLS);
        initTab(TAB_INDEX_CHAT);
        initTab(TAB_INDEX_CONTACTS);
        initTab(TAB_INDEX_KEYPAD);
        initTab(TAB_INDEX_ACCOUNT);
    }

    private void removeAllTabListener() {
        Stack<Fragment> contactFragment = mStacks.get(TAB_CONTACTS);
        for (Fragment fragment : contactFragment) {
            if (fragment instanceof ContactListFragment) {
                mContactManager.removeContactChangeListener((ContactListFragment)fragment);
                mContactManager.removeFirstSyncListener((ContactListFragment)fragment);
            }
        }

        Stack<Fragment> chatFragment = mStacks.get(TAB_CHAT);
        for (Fragment fragment : chatFragment) {
            if (fragment instanceof ChatFragment) {
                ((ChatFragment)fragment).removeAllManagerListener();
            }
        }

        Stack<Fragment> callLogFragment = mStacks.get(TAB_CHAT);
        for (Fragment fragment : callLogFragment) {
            if (fragment instanceof CallLogFragment) {
                ((CallLogFragment)fragment).removeAllManagerListener();
            }
        }
    }

    public CallLogFragment getCallLogFragment() {
        Stack<Fragment> callFragment = mStacks.get(TAB_CALLS);
        Fragment fragment = callFragment.get(callFragment.size() - 1);
        if (fragment == null) {
            return null;
        }
        if (fragment instanceof CallLogFragment) {
            CallLogFragment callLogFragment = (CallLogFragment)fragment;
            return callLogFragment;
        }
        return null;
    }

    public ContactListFragment getContactListFragment() {
        Stack<Fragment> callFragment = mStacks.get(TAB_CONTACTS);
        Fragment fragment = callFragment.get(callFragment.size() - 1);
        if (fragment == null) {
            return null;
        }
        if (fragment instanceof ContactListFragment) {
            ContactListFragment contactListFragment = (ContactListFragment)fragment;
            return contactListFragment;
        }
        return null;
    }

    public ChatFragment getChatFragment() {
        Stack<Fragment> chatFragmentSt = mStacks.get(TAB_CHAT);
        ChatFragment chatFragment = (ChatFragment)chatFragmentSt.get(chatFragmentSt.size() - 1);
        return chatFragment;
    }

    private void initTab(int tabIndex) {
        Drawable idImg = Utils.getDrawable(this, R.attr.iconTabAccount);
        int idTitle = R.string.calls;
        String tabIndicator = TAB_CALLS;
        switch (tabIndex) {
            case TAB_INDEX_CALLS:
                idImg = Utils.getDrawable(this, R.attr.iconTabCall);
                idTitle = R.string.calls;
                tabIndicator = TAB_CALLS;
                break;
            case TAB_INDEX_CHAT:
                idImg = Utils.getDrawable(this, R.attr.iconTabChat);
                idTitle = R.string.chat;
                tabIndicator = TAB_CHAT;
                break;
            case TAB_INDEX_CONTACTS:
                idImg = Utils.getDrawable(this, R.attr.iconTabContact);
                idTitle = R.string.contacts;
                tabIndicator = TAB_CONTACTS;
                break;
            case TAB_INDEX_KEYPAD:
                idImg = getResources().getDrawable(R.drawable.ic_tab_keypad_);
                idTitle = R.string.keypad;
                tabIndicator = TAB_KEYPAD;
                break;
            case TAB_INDEX_ACCOUNT:
                idImg = Utils.getDrawable(this, R.attr.iconTabAccount);
                idTitle = R.string.account;
                tabIndicator = TAB_ACCOUNT;
                break;
            default:
                break;
        }
        TabHost.TabSpec spec = mTabHost.newTabSpec(tabIndicator);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.tab_content);
            }
        });

        View tabView = createTabView(idImg, idTitle);
        spec.setIndicator(tabView);
        mTabHost.addTab(spec);

        // If TAB_INDEX_CALLS or TAB_INDEX_CHAT, initialize notify number view
        if (tabIndex == TAB_INDEX_CALLS) {
            mTxtMissCallNotifyLayout = tabView.findViewById(R.id.notifyIcon);
            mTxtMissCallNotify = (TextView)tabView.findViewById(R.id.txtNotify);
        } else if (tabIndex == TAB_INDEX_CHAT) {
            mTxtUnreadMsgNotifyLayout = tabView.findViewById(R.id.notifyIcon);
            mTxtUnreadMsgNotify = (TextView)tabView.findViewById(R.id.txtNotify);
        }

        // Add listener for Tab Keypad.
        if (tabIndex == TAB_INDEX_KEYPAD) {
            mTabHost.getTabWidget().getChildAt(TAB_INDEX_KEYPAD).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        Intent intent = new Intent(MainTabActivity.this, KeypadActivity.class);
                        startActivity(intent);
                    }
                    return true;
                }
            });
        }
    }

    /* Comes here when user switch tab, or we do programmatically */
    TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
        public void onTabChanged(String tabId) {
            /* Set current tab.. */
            mCurrentTab = tabId;
            if (mStacks.get(tabId).size() == 0) {
                /*
                 * First time this tab is selected. So add first fragment of
                 * that tab. Dont need animation, so that argument is false. We
                 * are adding a new fragment which is not present in stack. So
                 * add to stack is true.
                 */
                if (tabId.equals(TAB_CALLS)) {
                    pushFragments(tabId, new CallLogFragment(), false, true);
                } else if (tabId.equals(TAB_CHAT)) {
                    // Add fragment
                    pushFragments(tabId, new ChatFragment(), false, true);
                } else if (tabId.equals(TAB_CONTACTS)) {
                    ContactListFragment contactListFragment = new ContactListFragment();
                    pushFragments(tabId, contactListFragment, false, true);
                    mContactManager.addContactChangeListener(contactListFragment);
                    mContactManager.addFirstSyncListener(contactListFragment);
                } else if (tabId.equals(TAB_KEYPAD)) {
                    // pushFragments(tabId, new EmptyFragment(tabId,
                    // "tab KEypad"), false, true);
                } else if (tabId.equals(TAB_ACCOUNT)) {
                    pushFragments(tabId, new AccountFragment(), false, true);
                }
            } else {
                /*
                 * We are switching tabs, and target tab is already has atleast
                 * one fragment. No need of animation, no need of stack pushing.
                 * Just show the target fragment
                 */
                pushFragments(tabId, mStacks.get(tabId).lastElement(), false, false);
            }

            if (tabId.equals(TAB_CALLS)) {
                setActionBarType(ACTIONBAR_TYPE_PHONE_CALL);
                setActionBarType2Action(actionCallLeft, actionCallCenterButtonLeft, actionCallCenterButtonRight,
                        actionCallRight);
                if (getCallLogFragment() != null && getCallLogFragment().getAdapter() != null) {
                    resetActionBarCallLogButtons(false, getCallLogFragment().isEditMode());
                    setCallEditMode(getCallLogFragment().isEditMode());
                }
            } else if (tabId.equals(TAB_CHAT)) {
                // Change action bar type
                setActionBarType(ACTIONBAR_TYPE_CHAT);
                setActionBarType3Action(actionChatLeft, actionChatRight);
                // Set action bar title
                setActionbarTitle(isChatEditMode ? R.string.edit_chat_registry : R.string.chat_registry);
                setChatEditMode(getChatFragment().isEditMode());
            } else if (tabId.equals(TAB_CONTACTS)) {
                setActionBarType(ACTIONBAR_TYPE_PHONE_CALL);
                setActionBarType2Action(actionCallCenterButtonLeft, actionCallCenterButtonRight, actionContactRight,
                        R.drawable.ic_add_);
                resetActionBarCallLogButtons(true, getContactListFragment().getViewMode());
                setActionbarTitle(R.string.contacts);
                // setActionBarType(ACTIONBAR_TYPE_NORMAL);
            } else if (tabId.equals(TAB_KEYPAD)) {
                // setActionbarTitle(R.string.keypad);
            } else if (tabId.equals(TAB_ACCOUNT)) {
                setActionbarTitle(R.string.account);
                setActionBarType(ACTIONBAR_TYPE_NORMAL);
            }
        }
    };

    /*
     * Might be useful if we want to switch tab programmatically, from inside
     * any of the fragment.
     */
    public void setCurrentTab(int val) {
        mTabHost.setCurrentTab(val);
    }

    /**
     * Set miss call notify number.
     */
    public void setMissCallNotifyNumber(int missCallNumber) {
        if (missCallNumber > 0) {
            mTxtMissCallNotifyLayout.setVisibility(View.VISIBLE);
            mTxtMissCallNotify.setText(Integer.toString(missCallNumber));
        } else {
            mTxtMissCallNotifyLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Set Unread Message notify number.
     */
    public void setUnreadMsgNotifyNumber(int unreadMsgNumber) {
        if (unreadMsgNumber > 0) {
            mTxtUnreadMsgNotifyLayout.setVisibility(View.VISIBLE);
            mTxtUnreadMsgNotify.setText(Integer.toString(unreadMsgNumber));
        } else {
            mTxtUnreadMsgNotifyLayout.setVisibility(View.GONE);
        }
    }

    /*
     * To add fragment to a tab. tag -> Tab identifier fragment -> Fragment to
     * show, in tab identified by tag shouldAnimate -> should animate
     * transaction. false when we switch tabs, or adding first fragment to a tab
     * true when when we are pushing more fragment into navigation stack.
     * shouldAdd -> Should add to fragment navigation stack (mStacks.get(tag)).
     * false when we are switching tabs (except for the first time) true in all
     * other cases.
     */
    public void pushFragments(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.tab_content, fragment);
        ft.commit();
    }

    public void popFragments() {
        /*
         * Select the second last fragment in current tab's stack.. which will
         * be shown after the fragment transaction given below
         */
        Fragment fragment = mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);

        /* pop current fragment from stack.. */
        mStacks.get(mCurrentTab).pop();

        /*
         * We have the target fragment in hand.. Just show it.. Show a standard
         * navigation animation
         */
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.tab_content, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (mStacks.get(mCurrentTab).isEmpty() == false
                && ((BaseFragment)mStacks.get(mCurrentTab).lastElement()).onBackPressed() == false) {
            /*
             * top fragment in current tab doesn't handles back press, we can do
             * our thing, which is if current tab has only one fragment in
             * stack, ie first fragment is showing for this tab. finish the
             * activity else pop to previous fragment in stack for the same tab
             */
            if (mStacks.get(mCurrentTab).size() == 1) {
                BaseFragment bf = (BaseFragment)mStacks.get(mCurrentTab).lastElement();
                if (bf.isEditMode()) {
                    if (bf instanceof CallLogFragment)
                        setCallEditMode(false);
                    else if (bf instanceof ChatFragment)
                        setChatEditMode(false);
                    else
                        super.onBackPressed();
                } else {
                    super.onBackPressed(); // or call finish..
                }
            } else {
                popFragments();
            }
        } else {
            // do nothing.. fragment already handled back button press.
        }
    }

    /*
     * Imagine if you wanted to get an image selected using ImagePicker intent
     * to the fragment. Ofcourse I could have created a public function in that
     * fragment, and called it from the activity. But couldn't resist myself.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mStacks.get(mCurrentTab).size() == 0) {
            return;
        }

        /* Now current fragment on screen gets onActivityResult callback.. */
        mStacks.get(mCurrentTab).lastElement().onActivityResult(requestCode, resultCode, data);

        if (requestCode == ChatFragment.BROWSER_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                String[] numbers = data.getStringArrayExtra("selected_numbers");
                String groupChatUuid = data.getStringExtra("groupChatUuid");
                if (numbers.length == 0) {
                    Toast.makeText(this, "Must select at least 1 number", Toast.LENGTH_SHORT).show();
                } else {
                    // Start new chat
                    // TODO: Wil be Implement correctly when chat group is
                    // supported, now just get 1st selected contact
                	if (Utils.isStringNullOrEmpty(groupChatUuid)) {
                		if (Utils.getKATZNumber(numbers[0]).equals(UserInfo.getInstance().getPhoneNumberFormatted())) {
                            showAlert(R.string.error, R.string.error_chat_yourself);
                        } else {
                            Intent chatIntent = new Intent(this, ConversationActivity.class);
                            chatIntent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, numbers[0]);
                            startActivity(chatIntent);
                        }
                	} else {
                		Intent chatIntent = new Intent(this, ConversationActivity.class);
                        chatIntent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, groupChatUuid);
                        startActivity(chatIntent);
                	}
                	
                    
                }
            }
        }
    }

    public void setCallEditMode(boolean isEditMode) {
        if (isEditMode) {
            // not visible yet
            // actionBarLeft2.setVisibility(View.VISIBLE);
            actionBarRight2.setText(R.string.cancel);
            getCallLogFragment().changeToEditMode();
        } else {
            actionBarLeft2.setVisibility(View.INVISIBLE);
            actionBarRight2.setText(R.string.edit);
            getCallLogFragment().changeToViewMode();
        }
    }

    public void setCallEditVisible(boolean isVisiable) {
        if (!isVisiable) {
            setCallEditMode(false);
        }
        actionBarRight2.setVisibility(isVisiable ? View.VISIBLE : View.INVISIBLE);
    }

    private void setChatEditMode(boolean isEditMode) {
        isChatEditMode = isEditMode;
        if (isEditMode) {
            actionbarLeft3.setImageResource(R.drawable.ic_back_);
            actionbarTitle3.setText(R.string.edit_chat_registry);
            actionbarRight3.setText(R.string.delete);
            getChatFragment().changeToEditMode();
        } else {
            actionbarLeft3.setImageResource(R.drawable.icon_new_chat_);
            actionbarTitle3.setText(R.string.chat_registry);
            actionbarRight3.setText(R.string.edit);
            getChatFragment().changeToViewMode();
        }
    }

    public void setChatEditVisible(boolean isVisiable) {
        setChatEditMode(false);
        actionbarRight3.setVisibility(isVisiable ? View.VISIBLE : View.INVISIBLE);
    }

    // //
    // // Implement method of SipChatListener START
    // //
    @Override
    public void onNewMessage(ChatMessageModel... newMsg) {
    }

    @Override
    public void onMessageStatusUpdate(ArrayList<ChatMessageModel> models) {
    }

    @Override
    public void onUnreadMessageChanged(final int unreadThread, final int unreadMesage) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                setUnreadMsgNotifyNumber(unreadThread);
            }
        });
    }

    @Override
    public void onNewCallLog(CallLogModel callLog) {
        if (callLog.getType() == CallLogModel.TYPE_MISSCALL) {
            postHandler.post(new Runnable() {
                @Override
                public void run() {
                    setMissCallNotifyNumber(mCallLogManager.countMissCall());
                }
            });
        }
    }

    @Override
    public void onCallLogChange(CallLogModel callLog) {
        if (callLog.getType() == CallLogModel.TYPE_MISSCALL) {
            postHandler.post(new Runnable() {
                @Override
                public void run() {
                    setMissCallNotifyNumber(mCallLogManager.countMissCall());
                }
            });
        }
    }

    @Override
    public void onCallLogDeleted(long[] callLogIds) {
    }

    // //
    // // Implement method of SipChatListener END
    // //

    public void onClickChatRow() {
        if (getString(R.string.delete).equals(actionbarRight3.getText())) {
            long[] selectedId = getChatFragment().getAdapter().getListSelectedIds();
            if (selectedId.length == 0) {
                actionbarRight3.setVisibility(View.INVISIBLE);
            } else {
                actionbarRight3.setVisibility(View.VISIBLE);
            }
        } else {
            int count = getChatFragment().getAdapter().getCount();
            if (count == 0) {
                actionbarRight3.setVisibility(View.INVISIBLE);
            } else {
                actionbarRight3.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setLeftDeleteVisibility(boolean isVivible) {
        if (isVivible) {
            actionBarLeft2.setVisibility(View.VISIBLE);
        } else {
            actionBarLeft2.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight,
            int oldBottom) {
        // if (bottom < oldBottom) {
        // mTabs.setVisibility(View.GONE);
        // } else {
        // mTabs.setVisibility(View.VISIBLE);
        // }
    }
}
