
package sigma.qilex.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.customview.CustomButton;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class AddContactActivity extends BaseActivity implements OnClickListener {

    private final int INSERT_CONTACT_REQUEST = 1;

    private final int ADD_CONTACT_REQUEST = 2;

    private final int VIEW_CONTACT_INFO = 3;

    private final int EDIT_CONTACT = 4;

    EditText edtInputNumber;

    CustomButton btnAddExisting;

    CustomButton btnCreateNew;

    ContactManager mContactManager;

    TextView txtContactName;

    View layoutButtons;

    QilexContact currentSearchContact;

    TextWatcher onSearchQuery = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String inputString = s.toString();
            doSearchWithText(inputString);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact_activity);
        setTitle(R.string.add_contact);
        showBackAction();
        txtContactName = (TextView)findViewById(R.id.txtContactName);
        txtContactName.setVisibility(View.INVISIBLE);
        layoutButtons = findViewById(R.id.layoutButtons);
        btnCreateNew = (CustomButton)findViewById(R.id.create_new_button);
        btnAddExisting = (CustomButton)findViewById(R.id.add_exist_button);
        btnAddExisting.setOnClickListener(this);
        btnCreateNew.setOnClickListener(this);
        txtContactName.setOnClickListener(this);
        edtInputNumber = (EditText)findViewById(R.id.input_number);
        edtInputNumber.addTextChangedListener(onSearchQuery);
        mContactManager = ContactManager.getInstance();
        if (getIntent().getExtras() != null) {
            String numberToAdd = getIntent().getExtras().getString("number_to_add");
            if (numberToAdd != null)
                edtInputNumber.setText(numberToAdd);
        }

        showKeyboard(edtInputNumber);

        Utils.formatTextSizeButton(this, new CustomButton[] {
                btnCreateNew, btnAddExisting
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        String number = edtInputNumber.getEditableText().toString();
        // if (QilexPhoneNumberUtils.isEmergencyNumber(number, false)) {
        // showAlertWarning(R.string.warning,
        // R.string.error_phone_number_not_support,
        // id == R.id.create_new_button ? onContinueCreateNew :
        // onContinueAddExis);
        // return;
        // }
        // Add phone country code to number
        number = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(number);
        switch (id) {
            case R.id.create_new_button:
                createNewContact(number);
                break;
            case R.id.add_exist_button:
                addExistingContact(number);
                break;
            case R.id.txtContactName:
                // Start contact detail
                Intent intent = new Intent(this, ContactKATZOutDetailActivity.class);
                intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, currentSearchContact.id);
                intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY,
                        currentSearchContact.lookUpKey);
                intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                startActivityForResult(intent, VIEW_CONTACT_INFO);
                break;
            default:
                break;
        }
    }

    DialogInterface.OnClickListener onContinueCreateNew = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            String number = edtInputNumber.getEditableText().toString();
            number = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(number);
            createNewContact(number);
        }
    };

    DialogInterface.OnClickListener onContinueAddExis = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            String number = edtInputNumber.getEditableText().toString();
            number = QilexPhoneNumberUtils.addCurrentCountryCodeToPhone(number);
            addExistingContact(number);
        }
    };

    private void createNewContact(String number) {
        Intent intent = new Intent(AddContactActivity.this, CreateNewContactActivity.class);
        intent.putExtra("create_new_contact", number);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_LEFT_OUT_LEFT_2);
        startActivityForResult(intent, ADD_CONTACT_REQUEST);
    }

    private void addExistingContact(String number) {
        Intent intent = new Intent(AddContactActivity.this, ContactListActivity.class);
        intent.putExtra("insert_to_contact_number", number);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT_2);
        startActivityForResult(intent, INSERT_CONTACT_REQUEST);
    }

    private void goToEditContact(QilexContact contact) {
        String phoneNumberWithPhoneCode = QilexPhoneNumberUtils
                .addCurrentCountryCodeToPhone(edtInputNumber.getEditableText().toString());

        Intent intent = new Intent(this, EditContactActivity.class);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contact.id);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY, contact.lookUpKey);
        intent.putExtra(EditContactActivity.INTENT_PARAM_ADDED_PHONE, phoneNumberWithPhoneCode);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT_2);
        startActivityForResult(intent, EDIT_CONTACT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == INSERT_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                long id = data.getExtras().getLong(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS);
                QilexContact con = mContactManager.getContactById(id);
                goToEditContact(con);
            } else if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == ADD_CONTACT_REQUEST) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            } else if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == VIEW_CONTACT_INFO) {
            finish();
            // doSearchWithText(edtInputNumber.getText().toString());
        } else if (requestCode == EDIT_CONTACT) {
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onContactNumberLabelClick(View v) {
        showKeyboard(edtInputNumber);
    }

    private void doSearchWithText(String inputString) {
        if (Utils.isStringNullOrEmpty(inputString)) {
            layoutButtons.setVisibility(View.VISIBLE);
            txtContactName.setVisibility(View.GONE);
            btnCreateNew.setEnabled(false);
            btnAddExisting.setEnabled(false);
        } else {
            currentSearchContact = mContactManager.getContactByPhoneNo(AddContactActivity.this, inputString);
            btnCreateNew.setEnabled(true);
            btnAddExisting.setEnabled(true);
            if (currentSearchContact == null) {
                layoutButtons.setVisibility(View.VISIBLE);
                txtContactName.setVisibility(View.GONE);
            } else {
                layoutButtons.setVisibility(View.GONE);
                txtContactName.setVisibility(View.VISIBLE);
                txtContactName.setText(currentSearchContact.getDisplayName());
            }
        }
    }
}
