
package sigma.qilex.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.http.UserInfoResponseModel;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactProcessingHandler;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class CreateNewContactActivity extends ContactPickerActivity implements OnClickListener {

    public static final String INTENT_PARAM_CONTACT = "INTENT_PARAM_CONTACT";

    MyCustomEditText edtInputFirstName;

    MyCustomEditText edtInputLastName;

    MyCustomTextView txtNumber;

    MyCustomTextView txtType;

    String mNumber;

    ProgressBar mProgressAvatar;

    boolean isFrifon = false;

    private ContactManager mContactManager;

    private AuthenticationManager mAuthenManager;

    ImageView imgFrifonOut, imgMsg, imgCall;

    String mAvatarUrl = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
        }
        mContactManager = ContactManager.getInstance();
        mAuthenManager = AuthenticationManager.getInstance();
        mNumber = extras.getString("create_new_contact");
        setContentView(R.layout.create_new_contact_activity);
        showBackAction();
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        actionbarTitle3.setText(getString(R.string.create_new_contact));
        actionbarRight3.setText(getString(R.string.save));
        edtInputFirstName = (MyCustomEditText)findViewById(R.id.input_first_name);
        edtInputLastName = (MyCustomEditText)findViewById(R.id.input_last_name);
        txtNumber = (MyCustomTextView)findViewById(R.id.number);
        txtType = (MyCustomTextView)findViewById(R.id.type);
        mProgressAvatar = (ProgressBar)findViewById(R.id.progress_avatar);
        txtNumber.setText(mNumber);
        txtType.setText(R.string.phone_type_mobile);
        mImgAvatar = (ImageView)findViewById(R.id.user_avatar);
        mImgAvatar.setOnClickListener(onAvatarClick);
        setActionBarType3Action(onBack, actionSave);

        imgFrifonOut = (ImageView)findViewById(R.id.frifon_out);
        imgMsg = (ImageView)findViewById(R.id.msg);
        imgCall = (ImageView)findViewById(R.id.call);

        imgFrifonOut.setOnClickListener(this);
        imgMsg.setOnClickListener(this);
        imgCall.setOnClickListener(this);

        if (Const.IS_FRIFON_LITE) {
            imgFrifonOut.setVisibility(View.INVISIBLE);
        }

        // Load contact from previous activity
        QilexContact contact = extras.getParcelable(INTENT_PARAM_CONTACT);
        if (contact != null) {
            edtInputFirstName.setText(contact.getFirstName());
            edtInputLastName.setText(contact.getLastName());
            mAvatarUrl = contact.getPhotoUri();
            MyApplication.getImageLoader().displayImage(mAvatarUrl, mImgAvatar, circleDisplayImageOptions2,
                    new ProgressLoadingListener(mProgressAvatar));
        } else {
            // Load contact info from server
            loadContactInfoFromServer();
        }
    }

    OnClickListener actionSave = new OnClickListener() {
        @Override
        public void onClick(View v) {
            checkPermission(REQUEST_CONTACTS_CODE);
        }
    };

    private void save() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                showSimpleProgressByPostHandler();
                doAddContact();
            }
        }).start();
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_CONTACTS_CODE)
            save();
        else
            super.onPreRequestPermission(requestCode);
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CONTACTS_CODE) {
            if (QilexUtils.verifyAllPermissions(grantResults)) {
                onPreRequestPermission(requestCode);
            }
        } else {
            super.onPostRequestPermission(requestCode, permissions, grantResults);
        }
    }

    private void doAddContact() {
        if (mAvatarUrl != null && mBitmapAvtSave == null) {
            mBitmapAvtSave = MyApplication.getImageLoader().loadImageSync(mAvatarUrl);
        }
        String fname = edtInputFirstName.getText().toString();
        String lname = edtInputLastName.getText().toString();
        if (Utils.isStringNullOrEmpty(fname) && Utils.isStringNullOrEmpty(lname)) {
            fname = mNumber;
            lname = Const.STR_EMPTY;
        }
        LogUtils.d("KUNLQT", "doAddContact isFrifon: " + isFrifon);
        mContactManager.addContact(fname, lname, mNumber, mBitmapAvtSave, isFrifon, new ContactProcessingHandler() {
            @Override
            public void onStart() {
            }

            @Override
            public void onFinish(boolean result, QilexContact contact) {
                hideSimpleProgressByPostHandler();
                if (result == true) {
                    setResult(Activity.RESULT_OK);
                    finish();
                } else {
                    setResult(Activity.RESULT_CANCELED);
                    finish();
                }
            }
        }, null);
    }

    private void loadContactInfoFromServer() {
        mAuthenManager.getUserInfo(this, mNumber, new AuthenticationManager.GetUserInfoListener() {
            @Override
            public void onGetUserInfoFinish(boolean isNetworkError, final UserInfoResponseModel userInfo) {
                LogUtils.d("KUNLQT", "onGetUserInfoFinish: " + userInfo);
                if (Utils.isStringNullOrEmpty(edtInputFirstName.getText().toString())) {
                    if (userInfo != null) {
                        isFrifon = true;
                        mAvatarUrl = userInfo.user_avatar_uri;
                        postHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                edtInputFirstName.setText(userInfo.user_name);
                                edtInputLastName.setText(userInfo.user_last_name);
                                LogUtils.d("KUNLQT", "URL 1 " + userInfo.user_avatar_uri);
                                MyApplication.getImageLoader().displayImage(userInfo.user_avatar_uri, mImgAvatar,
                                        circleDisplayImageOptions2, new ProgressLoadingListener(mProgressAvatar));
                                showKeyboard(edtInputFirstName);
                                edtInputFirstName.setSelection(0, userInfo.user_name.length());
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.frifon_out) {
            Toast.makeText(this, "CreateContact frifon out", Toast.LENGTH_LONG).show();
        } else if (v.getId() == R.id.msg) {
            if (mNumber.equals(UserInfo.getInstance().getLocalProfile().getLogin())) {
                showAlert(R.string.error, R.string.error_chat_yourself);
            } else {
                if (Utils.isInMasterMode()) {
                    Utils.sendSms(this, mNumber);

                } else {
                    Utils.showToastInSlaveMode(this);
                }
            }
        } else if (v.getId() == R.id.call) {
            if (mNumber.equals(UserInfo.getInstance().getLocalProfile().getLogin())) {
                Toast.makeText(this, R.string.error_call_yourself, Toast.LENGTH_SHORT).show();
            } else {
                if (Utils.isInMasterMode()) {
                    startCall(mNumber);
                } else {
                    Utils.showToastInSlaveMode(this);
                }
            }
        }
    }
}
