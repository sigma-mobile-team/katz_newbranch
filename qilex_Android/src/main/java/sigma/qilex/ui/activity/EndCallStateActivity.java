
package sigma.qilex.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Toast;
import pl.frifon.aero2.R;
import pl.katz.aero2.Const;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.Utils;

public class EndCallStateActivity extends BaseActionBarActivity {

    public static final String END_CALL_STATE = "end_call_state";

    public static final int CALL_OFFLINE = 1;

    public static final int CALL_BUSY = 3;

    public static final int CALL_INVALID_NUMBER = 5;

    public static final int CALL_NOT_FRIFON = 6;

    private int mCallState = -1;

    private boolean mIsKATZOut = false;

    private String mContactName = "";

    private String mNumPhone = "";

    private MyCustomTextView mTvTitle, mTvContact, mTvStatus, mTvRelStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent == null)
            finish();
        mCallState = intent.getIntExtra(END_CALL_STATE, CALL_OFFLINE);
        mIsKATZOut = intent.getBooleanExtra(InCallActivity.INTENT_CALL_OUT, false);
        if (mCallState == CALL_INVALID_NUMBER) {
            setContentView(R.layout.outgoing_call_invalid_number_activity);
        } else if (mCallState == CALL_NOT_FRIFON) {
            setContentView(R.layout.outgoing_call_not_frifon_activity);
        } else {
            if (mIsKATZOut) {
                setContentView(R.layout.outgoing_call_retry_activity);
            } else {
                setContentView(R.layout.outgoing_call_send_msg_activity);
            }
        }
        if (Const.IS_FRIFON_LITE && findViewById(R.id.btnFrifonOut) != null) {
            findViewById(R.id.btnFrifonOut).setVisibility(View.INVISIBLE);
        }
        mTvTitle = (MyCustomTextView)findViewById(R.id.title);
        mTvContact = (MyCustomTextView)findViewById(R.id.txtContactName);
        mTvStatus = (MyCustomTextView)findViewById(R.id.txtStatus);
        mTvRelStatus = (MyCustomTextView)findViewById(R.id.tvRelStatus);
        // fill data
        mTvTitle.setText(mIsKATZOut ? Utils.getString(this, R.attr.stringKatzOutCall) : Utils.getString(this, R.attr.stringKatzCall));
        mContactName = intent.getStringExtra(InCallActivity.INTENT_NAME_PARTNER);
        mNumPhone = intent.getStringExtra(InCallActivity.INTENT_NO_PHONE_PARTNER);
        mTvContact.setText(mContactName);
        switch (mCallState) {
            case CALL_BUSY:
                mTvStatus.setText(R.string.call_stat_busy);
                mTvRelStatus.setText(R.string.msg_call_stat_busy);
                break;
            case CALL_OFFLINE:
                mTvStatus.setText(R.string.call_stat_no_response);
                mTvRelStatus.setText(R.string.msg_call_stat_offline);
                break;
            case CALL_INVALID_NUMBER:
                mTvStatus.setText(R.string.msg_call_stat_invalid_title);
                mTvRelStatus.setText(R.string.msg_call_stat_invalid_contact);
                break;
            default:
                break;
        }

        // animation
        overridePendingTransition(R.anim.activity_in_bottom, R.anim.activity_stay_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_stay_in, R.anim.activity_out_bottom);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendMsg:
                // go to chat screen
                Intent intent = new Intent(EndCallStateActivity.this, ConversationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(ConversationActivity.INTENT_PARAM_THREAD_ID, "");
                intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, mNumPhone);
                startActivity(intent);
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case R.id.btnRetry:
                int resultId = CallManager.getInstance().initOutGoingSipCall(mNumPhone, mIsKATZOut);
                if (resultId > 0) {
                    Toast.makeText(this, resultId, Toast.LENGTH_SHORT).show();
                } else {
                    delayFinish();
                }
                break;
            case R.id.btnFrifonOut:
                int resultId2 = CallManager.getInstance().initOutGoingSipCall(mNumPhone, true);
                if (resultId2 > 0) {
                    Toast.makeText(this, resultId2, Toast.LENGTH_SHORT).show();
                } else {
                    delayFinish();
                }
                break;
            case R.id.btnOk:
            case R.id.btnCancel:
                finish();
                break;
            default:
                break;
        }
    }

    private void delayFinish() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 500);
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
    }

}
