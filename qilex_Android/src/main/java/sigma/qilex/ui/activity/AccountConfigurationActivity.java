
package sigma.qilex.ui.activity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import pl.katz.aero2.Const;
import pl.katz.aero2.Const.Config;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.SetupUserInfoListener;
import sigma.qilex.manager.facebook.OnLoginListener;
import sigma.qilex.manager.facebook.SimpleFacebook;
import sigma.qilex.ui.customview.CustomButton;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.utils.DateTimeUtils;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;

public class AccountConfigurationActivity extends BaseActivity implements View.OnClickListener, SetupUserInfoListener {
    public static final String USER_AVATAR_NAME_FORMAT = "user_avatar_%s.jpg";

    private ImageView mImgAvatar;

    private CustomButton mBtnStart;

    private LinearLayout mAvatarChange;

    private MyCustomEditText mInputName;

    private int mImgSize;

    private UserInfo mUserInfo;

    private AuthenticationManager mAuthenManager;

    private boolean isChangeInfo = false;

    private boolean isEditName = false;

    private boolean isEditAvatar = false;

    private String initialName;

    private View btnConnectFb;

    private TextView tvFacebookConnect;

    private SimpleFacebook mSimpleFacebook;

    private int mRequestPostUserInfoId;

    private AsyncTask<Void, Void, String> mAsyncTaskPostUserInfo;

    private boolean isClickFacebook = false;

    TextWatcher nameWathcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String newName = s.toString();
            if (newName.equals(initialName)) {
                isEditName = false;
            } else {
                isEditName = true;
            }
            checkToEnableButtonDone(newName);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.acc_configuration_activity);
        setActionbarTitle(R.string.account_configuration);

        if (getIntent().getExtras() != null) {
            isChangeInfo = getIntent().getExtras().getBoolean("IS_CHANGE_INFO");
        }
        mAuthenManager = AuthenticationManager.getInstance();
        mUserInfo = UserInfo.getInstance();

        mImgAvatar = (ImageView)findViewById(R.id.user_avatar);
        mBtnStart = (CustomButton)findViewById(R.id.start_button);
        mInputName = (MyCustomEditText)findViewById(R.id.input_name);

        btnConnectFb = findViewById(R.id.btnConnectFb);
        btnConnectFb.setOnClickListener(this);
        tvFacebookConnect = (TextView)findViewById(R.id.tvFacebookConnect);
        mSimpleFacebook = SimpleFacebook.getInstance(this);

        mAvatarChange = (LinearLayout)findViewById(R.id.layout_user_avatar);
        mAvatarChange.setOnClickListener(this);
        mBtnStart.setOnClickListener(this);
        mInputName.addTextChangedListener(nameWathcher);

        initialName = UserInfo.getInstance().getDisplayName();
        if (isChangeInfo == true) {
            mInputName.setText(initialName);
        }
        mImgAvatar.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                // get current image avatar size height for new reload
                // bitmap
                mImgSize = mImgAvatar.getHeight();
                if (isChangeInfo == true) {
                    if (mUserInfo.hasAvatar()) {
                        MyApplication.getImageLoader().displayImage(mUserInfo.getAvatarUrl(), mImgAvatar,
                                circleDisplayImageOptions2, animateFirstListener);
                    }
                }
                // this is an important step not to keep receiving
                // callbacks:
                // we should remove this listener
                mImgAvatar.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                LogUtils.d("binhdt", "onGlobalLayout mImgSize " + mImgSize);

            }
        });

        if (isChangeInfo) {
            mBtnStart.setVisibility(View.GONE);
            actionbarRight3.setVisibility(View.INVISIBLE);
        }
        updateBtnFb();
    }

    private void saveNewName() {
        String name = mInputName.getText().toString();
        if (!TextUtils.isEmpty(name) && !name.equals(initialName)) {
            UserInfo.getInstance().setFirstName(name);
            UserInfo.getInstance().setLastName(Const.STR_EMPTY);
            isEditName = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        updateBtnFb();
        if (isChangeInfo) {
            setActionBarType(ACTIONBAR_TYPE_CHAT);
            showBackAction();

            actionbarRight3.setText(getString(R.string.done));
            actionBarTextLeft3.setVisibility(View.GONE);
            actionbarLeft3.setVisibility(View.VISIBLE);

            actionbarRight3.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    saveNewName();
                    if (isEditName || isEditAvatar) {
                        setupUserInfo();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (isChangeInfo == true) {
            super.onBackPressed();
        }
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    String mTempFile;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Utils.PHOTO_REQUEST:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    if (Build.VERSION.SDK_INT < 21 && !QilexUtils.isChineseDevices()) {
                        String[] filePathColumn = {
                                MediaStore.Images.Media.DATA
                        };
                        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        // perform Crop on the Image Selected from Gallery
                        mTempFile = picturePath;
                        if (QilexUtils.performCrop(this, picturePath)) {
                            break;
                        }
                    }
                    Bitmap yourSelectedImage = Utils.decodeBitmapFromUri(getContentResolver(), selectedImage);
                    String file = Utils.saveToTempFolder(yourSelectedImage, Utils.mAvatarFileName);
                    String avatarUrl = "file://" + file;
                    MyApplication.getImageLoader().cancelDisplayTask(mImgAvatar);
                    MyApplication.getImageLoader().displayImage(avatarUrl, mImgAvatar, circleDisplayImageOptions2,
                            animateFirstListener);
                    mUserInfo.setAvatarUrl(avatarUrl);
                    isEditAvatar = true;
                    Utils.mAvatarFileName = null;
                }
                break;
            case Utils.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    File file = null;
                    try {
                        file = Utils.createTemporaryFile(Utils.mAvatarFileName);
                    } catch (Exception e) {
                        file = null;
                    }
                    if (file == null)
                        return;
                    mTempFile = file.getAbsolutePath();
                    if (Build.VERSION.SDK_INT < 21 && !android.os.Build.BRAND.equalsIgnoreCase("Huawei")) {
                        if (QilexUtils.performCrop(this, mTempFile)) {
                            break;
                        }
                    }
                    Bitmap mBitmapAvtSave = Utils.decodeBitmapFromFile(AccountConfigurationActivity.this, mTempFile);
                    String fileCam = Utils.saveToTempFolder(mBitmapAvtSave, Utils.mAvatarFileName);
                    String avatarUrl = "file://" + fileCam;
                    MyApplication.getImageLoader().cancelDisplayTask(mImgAvatar);
                    MyApplication.getImageLoader().displayImage(avatarUrl, mImgAvatar, circleDisplayImageOptions2,
                            animateFirstListener);
                    mUserInfo.setAvatarUrl(avatarUrl);
                    isEditAvatar = true;
                    Utils.mAvatarFileName = null;
                }
                break;
            case Utils.CROP_REQUEST:
                Bitmap mBitmapAvtSave = null;
                if (resultCode == RESULT_OK) {
                    // Bundle extras = data.getExtras();
                    // if (extras != null) {
                    // mBitmapAvtSave = extras.getParcelable("data");
                    // LogUtils.d("Duy", "Crop success");
                    // }
                    File filePath = Utils.getTemporaryFile("/avatar.jpg");
                    if (filePath != null) {
                        mBitmapAvtSave = BitmapFactory.decodeFile(filePath.getAbsolutePath());
                        filePath.delete();
                    }
                }
                // if (mBitmapAvtSave == null && !TextUtils.isEmpty(mTempFile))
                // {
                // LogUtils.d("TAG", "Can't get cropped image");
                // mBitmapAvtSave = Utils.decodeBitmapFromFile(
                // AccountConfigurationActivity.this, mTempFile);
                // LogUtils.d("TAG", "Can't get cropped image - done");
                // }
                if (mBitmapAvtSave != null) {
                    String file = Utils.saveToTempFolder(mBitmapAvtSave, Utils.mAvatarFileName);
                    MyApplication.getImageLoader().cancelDisplayTask(mImgAvatar);
                    MyApplication.getImageLoader().displayImage("file://" + file, mImgAvatar, circleDisplayImageOptions,
                            animateFirstListener);
                    String avatarUrl = "file://" + file;
                    mUserInfo.setAvatarUrl(avatarUrl);
                    isEditAvatar = true;
                    Utils.mAvatarFileName = null;
                } else {
                    isEditAvatar = false;
                    Utils.mAvatarFileName = null;
                }
                break;
            default:
                mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
                break;
        }
        checkToEnableButtonDone(mInputName.getText().toString());
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnConnectFb:
                onClickBtnFb();
                break;
            case R.id.layout_user_avatar:
                if (TextUtils.isEmpty(Utils.mAvatarFileName)) {
                    Utils.mAvatarFileName = String.format(USER_AVATAR_NAME_FORMAT,
                            DateTimeUtils.getCurrentTime_yyyyMMddHHmmss());
                }
                onChangeAvatar();
                break;
            case R.id.start_button:
                mUserInfo.setFirstName(mInputName.getText().toString());
                setupUserInfo();
                break;
        }
    }

    private void onClickBtnFb() {
        if (isClickFacebook)
            return;
        isClickFacebook = true;
        if (!mSimpleFacebook.isLogin()) {
            // login now
            loginFb();
        } else {
            restoreFbInfo();
        }
    }

    private void loginFb() {
        mSimpleFacebook.login(new OnLoginListener() {

            @Override
            public void onFail(String reason) {
                LogUtils.d("binhdt", "onFail");
                Toast.makeText(AccountConfigurationActivity.this,
                        AccountConfigurationActivity.this.getResources().getString(R.string.fb_login_failed),
                        Toast.LENGTH_SHORT).show();
                isClickFacebook = false;
            }

            @Override
            public void onException(Throwable throwable) {
                LogUtils.d("binhdt", "onException");
                Toast.makeText(AccountConfigurationActivity.this, AccountConfigurationActivity.this.getResources()
                        .getString(R.string.fb_login_failed, throwable.toString()), Toast.LENGTH_SHORT).show();
                isClickFacebook = false;
            }

            @Override
            public void onLogin(AccessToken accessToken) {
                LogUtils.d("binhdt", "onLogin, accessToken " + accessToken);
                handleLoginSuccess(accessToken);
                isClickFacebook = false;
            }

            @Override
            public void onCancel() {
                LogUtils.d("binhdt", "onCancel");
                isClickFacebook = false;
            }
        }, Arrays.asList("public_profile", "email", "user_friends"));
    }

    protected void handleLoginSuccess(final AccessToken accessToken) {
        showSimpleProgressByPostHandler(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                hideSimpleProgressByPostHandler();
                updateBtnFb();

                try {
                    // map fb data to UI
                    final String name = object.getString("name");
                    if (!TextUtils.isEmpty(name)) {
                        mInputName.setText(name);
                        UserInfo.getInstance().setFirstName(name);
                        UserInfo.getInstance().setLastName(Const.STR_EMPTY);
                        UserInfo.getInstance().saveInfo();
                    }

                    // getAvatar
                    GraphRequest request = GraphRequest.newMeRequest(accessToken,
                            new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Application code
                            try {
                                JSONObject picture = (JSONObject)object.get("picture");
                                JSONObject dta = (JSONObject)picture.get("data");
                                String avatar = dta.getString("url");
                                mUserInfo.setAvatarUrl(avatar);
                                mUserInfo.saveInfo();
                                MyApplication.getImageLoader().displayImage(avatar, mImgAvatar,
                                        circleDisplayImageOptions2, animateFirstListener);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                LogUtils.d("binhdt", "Get avatar exc " + e.toString());
                            }
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "picture.type(large)");
                    request.setParameters(parameters);
                    request.executeAsync();

                    MyApplication.getImageLoader().cancelDisplayTask(mImgAvatar);

                    SharedPrefrerenceFactory.getInstance().saveEmailFb(object.optString("email", null));
                    // save fb avatar to sdcard

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();

    }

    private void restoreFbInfo() {
        if (mSimpleFacebook.isLogin()) {
            handleLoginSuccess(mSimpleFacebook.getAccessToken());
            isEditAvatar = true;
            checkToEnableButtonDone(mInputName.getText().toString());
            isClickFacebook = false;
        }
    }

    private void setupUserInfo() {
        String name = mInputName.getText().toString();
        mUserInfo.setFirstName(name);
        mUserInfo.setLastName(Const.STR_EMPTY);
        showSimpleProgress(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (mAsyncTaskPostUserInfo != null && mAsyncTaskPostUserInfo.isCancelled() == false) {
                    mAsyncTaskPostUserInfo.cancel(true);
                }
                QilexHttpRequest.stopRequest(mRequestPostUserInfoId);
            }
        });
        mAsyncTaskPostUserInfo = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String retVal = Const.STR_EMPTY;
                byte[] avatarByte = mUserInfo.getAvatarBytesJpg();
                if (avatarByte != null) {
                    try {
                        retVal = Zip.encodeBase64(avatarByte);
                    } catch (UnsupportedEncodingException e) {
                    }
                }
                return retVal;
            }

            @Override
            protected void onPostExecute(String base64Avatar) {
                super.onPostExecute(base64Avatar);
                mRequestPostUserInfoId = mAuthenManager.setUpUserInfo(mUserInfo.getPhoneNumberFormatted(),
                        mUserInfo.getFirstName(), mUserInfo.getLastName(), base64Avatar, Config.AVATAR_UPLOAD_FORMAT,
                        AccountConfigurationActivity.this);
            }
        }.execute();
    }

    @Override
    public void onSetupUserInfoFinish(final boolean isSuccess, final ErrorModel error) {
        if (isSuccess) {
            mUserInfo.saveInfo();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideSimpleProgress();
                    setResult(RESULT_OK);
                    finish();
                }
            });
        } else {
            mUserInfo.loadInfo();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideSimpleProgress();
                    showAlert(getString(R.string.error), error.errors[0].getCurrentErrorMessage());
                }
            });
        }
    }

    private void updateBtnFb() {
        if (mSimpleFacebook.isLogin()) {
            tvFacebookConnect.setText(getString(R.string.facebook_connect_use));
        } else {
            tvFacebookConnect.setText(getString(R.string.facebook_connect));
        }
    }

    private void checkToEnableButtonDone(String newName) {
        if (isEditAvatar || isEditName) {
            actionbarRight3.setVisibility(View.VISIBLE);
        } else {
            actionbarRight3.setVisibility(View.INVISIBLE);
        }
    }
}
