
package sigma.qilex.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import pl.frifon.aero2.R;
import sigma.qilex.ui.customview.CustomButton;

public class ActivationTabletActivity extends BaseActivity implements OnClickListener {
    private CustomButton btnBack;

    private TextView tvGuide;

    public static final String INTENT_PHONE_NUM_INVALID = "INTENT_PHONE_NUM_INVALID";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activation_tablet_activity);
        setTitle(R.string.activation_title);
        showBackAction();
        findViews();

        if (getIntent().getExtras() != null) {
            String number = getIntent().getExtras().getString(INTENT_PHONE_NUM_INVALID);
            if (number != null) {
                tvGuide.setText(String.format(getString(R.string.activation_code_error_slave), number));
            } else {
                tvGuide.setText(getString(R.string.activation_code_guide_slave));
            }
        }
    }

    private void findViews() {
        btnBack = (CustomButton)findViewById(R.id.btnBack);
        tvGuide = (TextView)findViewById(R.id.tvGuide);

        btnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnBack) {
            finish();
        }
    }

}
