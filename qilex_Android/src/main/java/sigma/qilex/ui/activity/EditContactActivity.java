
package sigma.qilex.ui.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.http.ContactSnapshotModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactEntryProcessListener;
import sigma.qilex.manager.contact.ContactProcessingHandler;
import sigma.qilex.ui.adapter.ContactDetailPhoneListAdapter.PhoneItem;
import sigma.qilex.ui.adapter.ContactDetailPhoneListUpdateAdapter;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class EditContactActivity extends ContactPickerActivity implements ContactEntryProcessListener {

    public static final String INTENT_PARAM_ADDED_PHONE = "INTENT_PARAM_ADDED_PHONE";

    MyCustomEditText edtInputFirstName;

    MyCustomEditText edtInputLastName;

    QilexContact mContact;

    ListView mListPhone;

    ContactDetailPhoneListUpdateAdapter mAdapter;

    ContactManager mContactManager;

    ProgressBar mProgressAvatar;

    String fName, lName;

    String addedNumber = null;

    public EditContactActivity() {
        mContactManager = ContactManager.getInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
        }
        // long id = extras.getLong(
        // ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS);
        String lookupKey = extras.getString(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY);
        addedNumber = extras.getString(INTENT_PARAM_ADDED_PHONE, null);
        mContact = mContactManager.getContactByLookupKey(lookupKey);

        setContentView(R.layout.edit_contact_activity);
        mImgAvatar = (ImageView)findViewById(R.id.user_avatar);
        mProgressAvatar = (ProgressBar)findViewById(R.id.progress_avatar);
        mImgAvatar.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                // get current image avatar size height for new reload bitmap
                mImgSize = mImgAvatar.getHeight();
                // setAvatarParams();
                MyApplication.getImageLoader().displayImage(mContact.photoPhoneUri, mImgAvatar,
                        circleDisplayImageOptions2, new ProgressLoadingListener(mProgressAvatar));
                // this is an important step not to keep receiving
                // callbacks:
                // we should remove this listener
                mImgAvatar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
        mImgAvatar.setOnClickListener(onAvatarClick);
        showBackAction();
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        // actionbarTitle3.setText()
        actionbarRight3.setText(getString(R.string.done));
        actionbarTitle3.setText(mContact.getDisplayName());
        edtInputFirstName = (MyCustomEditText)findViewById(R.id.input_first_name);
        edtInputLastName = (MyCustomEditText)findViewById(R.id.input_last_name);
        fName = mContact.getFirstName();
        lName = mContact.getLastName();
        edtInputFirstName.setText(fName);
        edtInputLastName.setText(lName);
        mListPhone = (ListView)findViewById(R.id.list_phone_numbers);

        // List PhoneNo
        ArrayList<PhoneNumberModel> numbersWithType = new ArrayList<>();
        ArrayList<PhoneNumberModel> newNumbers = new ArrayList<>();
        numbersWithType.addAll(mContact.phoneNumberKATZ);
        numbersWithType.addAll(mContact.phoneNumberNormal);
        if (addedNumber != null) {
            newNumbers.add(new PhoneNumberModel(addedNumber, getString(R.string.phone_type_mobile)));
        }
        mAdapter = new ContactDetailPhoneListUpdateAdapter(this, numbersWithType, newNumbers, 1);
        mListPhone.setAdapter(mAdapter);

        // Set list view height
        Utils.setListViewHeightBasedOnChildren(mListPhone);
        setActionBarType3Action(onBack, actionDone);
    }

    protected void onPause() {
        super.onPause();
        hideKeyboard();
    };

    OnClickListener actionDone = new OnClickListener() {

        @Override
        public void onClick(View v) {
            checkPermission(REQUEST_CONTACTS_CODE);
        }
    };

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_CONTACTS_CODE)
            update();
        else {
            super.onPreRequestPermission(requestCode);
        }
    };

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CONTACTS_CODE) {
            if (QilexUtils.verifyAllPermissions(grantResults)) {
                onPreRequestPermission(requestCode);
            }
        } else {
            super.onPostRequestPermission(requestCode, permissions, grantResults);
        }
    }

    private void update() {
        hideKeyboard();
        String firstName = edtInputFirstName.getEditableText().toString();
        String lastName = edtInputLastName.getEditableText().toString();

        boolean notEditable = firstName.equals(fName) && lastName.equals(lName) && mBitmapAvtSave == null
                && !mAdapter.isEditable() && addedNumber == null;

        if (notEditable) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        if (Utils.isStringNullOrEmpty(firstName) && Utils.isStringNullOrEmpty(lastName)) {
            if (mContact.isKATZ()) {
                firstName = mContact.getFirstKATZNumber().phoneNumber;
            } else {
                firstName = mContact.getFirstNormalNumber().phoneNumber;
            }

            lastName = Const.STR_EMPTY;
        }

        ArrayList<PhoneItem> outInsert = new ArrayList<PhoneItem>();
        ArrayList<PhoneItem> outDelete = new ArrayList<PhoneItem>();
        mAdapter.getListInsertDeletePhone(outInsert, outDelete);
        mContactManager.updateContact(this, mContact.lookUpKey, mContact.id, mContact.rawContactId, firstName, lastName,
                mBitmapAvtSave, outDelete, outInsert, new ContactProcessingHandler() {
                    @Override
                    public void onStart() {
                        showSimpleProgressByPostHandler();
                    }

                    @Override
                    public void onFinish(boolean result, QilexContact contact) {
                        hideSimpleProgressByPostHandler();
                        if (result == true) {
                            mAdapter.clearSelection();
                            Intent data = new Intent();
                            data.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contact.id);
                            setResult(RESULT_OK, data);
                            finish();
                        }
                    }
                }, EditContactActivity.this);
    }

    public void onDeleteContactClick(View v) {
        showConfirmDialogNoYes(R.string.delete_contact_dialog_title, R.string.delete_contact_dialog_content,
                new Runnable() {
                    @Override
                    public void run() {
                        doDeleteContact();
                    }
                });
    }

    private void doDeleteContact() {
        mContactManager.deleteContact(this, mContact, new ContactProcessingHandler() {
            @Override
            public void onStart() {
                showSimpleProgressByPostHandler();
            }

            @Override
            public void onFinish(boolean result, QilexContact contact) {
                hideSimpleProgressByPostHandler();
                if (result == true) {
                    setResult(-5);
                    finish();
                }
            }
        });
    }

    @Override
    public void onContactSnapshotGet(ErrorModel error, QilexContact contact, ContactSnapshotModel snapshot) {
    }
}
