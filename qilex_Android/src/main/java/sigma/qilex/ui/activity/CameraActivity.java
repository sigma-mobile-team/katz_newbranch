
package sigma.qilex.ui.activity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewAnimator;
import pl.frifon.aero2.R;
import sigma.qilex.manager.chat.ImageThumbnailProcessor;
import sigma.qilex.ui.customview.AnimationFactory;
import sigma.qilex.ui.customview.AnimationFactory.FlipDirection;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

/**
 * @android.hardware.Camera is Deprecated from API 21
 * @check permission for API 23 (Android M)
 * @author namnd
 */
@SuppressWarnings("deprecation")
public class CameraActivity extends BaseActivity implements SurfaceHolder.Callback {
    public static final int REQUEST_CODE_GET_DESCRIPTION = 800;

    public static final String CAMERA_ID_KEY = "camera_id";

    CameraOrientationListener mOrientationListener;

    private static final int PICTURE_SIZE_MAX_WIDTH = 1280;

    private static final int PREVIEW_SIZE_MAX_WIDTH = 640;

    SurfaceView mPreview;

    private int mDisplayOrientation;

    private int mLayoutOrientation;

    Camera mCamera;

    SurfaceHolder mHolder;

    private int mCameraId;

    private static String[] mFlashModes = new String[] {
            Parameters.FLASH_MODE_OFF, Parameters.FLASH_MODE_AUTO, Parameters.FLASH_MODE_ON
    };

    private int[] mFlashModeTitles = new int[] {
            R.string.flash_off, R.string.flash_auto, R.string.flash_on
    };

    private static int flashModeIndex = 1;

    ViewAnimator rootLayout;

    private Button nextBtn;

    private ImageButton shutterBtn;

    private ImageView capturedImageView;

    private String[] imageUrls;

    private SaveImageTask imageTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_layout);
        if (savedInstanceState == null) {
            mCameraId = getBackCameraId();
        } else {
            mCameraId = savedInstanceState.getInt(CAMERA_ID_KEY);
        }
        rootLayout = (ViewAnimator)findViewById(R.id.root_layout);
        nextBtn = (Button)findViewById(R.id.btn_next);
        capturedImageView = (ImageView)findViewById(R.id.surface_captured);
        shutterBtn = (ImageButton)findViewById(R.id.btn_capture);
        mPreview = (SurfaceView)findViewById(R.id.surface_view);
        mHolder = mPreview.getHolder();
        setActionBarType(ACTIONBAR_TYPE_CHAT);
        setActionBarType3Action(actionAutoLeft, actionRefreshRight);
        setActionbarLeft3Title(mFlashModeTitles[flashModeIndex]);
        setAtionbarLeft3Icon(R.drawable.ic_menu_flash);
        setAtionbarRight3Icon(R.drawable.ic_camera_reverse);
        setActionbarRight3Visible((Camera.getNumberOfCameras() > 1) ? View.VISIBLE : View.INVISIBLE);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        // Orientation
        mOrientationListener = new CameraOrientationListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHolder.addCallback(this);
        checkPermission(REQUEST_CAMERA_CODE);
        mOrientationListener.enable();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mOrientationListener.canDetectOrientation() == true) {
            mOrientationListener.enable();
        } else {
            mOrientationListener.disable();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mOrientationListener.disable();
    }

    @Override
    protected void onStop() {
        release();
        mHolder.removeCallback(this);
        mOrientationListener.disable();
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(CAMERA_ID_KEY, mCameraId);
        super.onSaveInstanceState(outState);
    }

    private void openCamera(int cameraId) {
        mCameraId = cameraId;
        try {
            mCamera = Camera.open(mCameraId);
        } catch (Exception e) {
            LogUtils.e("NAMND", "openCamera = " + e.toString());
        }
    }

    private void switchCamera(final int cameraId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                release();
                openCamera(cameraId);
                setParameter(mPreview.getHolder());
            }
        });
    }

    private boolean hasSupportFocus(Camera camera) {
        List<String> supportedFocusModes = camera.getParameters().getSupportedFocusModes();
        boolean hasAutoFocus = supportedFocusModes != null
                && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        return hasAutoFocus;
    }

    private void release() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPreRequestPermission(int requestCode) {
        if (requestCode == REQUEST_CAMERA_CODE) {
            openCamera(mCameraId);
            resetCam();
        } else if (requestCode == REQUEST_STORAGE_CODE) {
            // Button Take Photo action
            showSimpleProgressByPostHandler();
            takePicture();
        }
    }

    @Override
    public void onPostRequestPermission(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_CODE) {
            if (QilexUtils.verifyAllPermissions(grantResults)) {
                openCamera(mCameraId);
                setParameter(mPreview.getHolder());
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        } else if (requestCode == REQUEST_STORAGE_CODE) {
            if (QilexUtils.verifyAllPermissions(grantResults)) {
                // Button Take Photo action
                showSimpleProgressByPostHandler();
                takePicture();
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mCamera == null)
            return;
        setParameter(holder);
    }

    private void setParameter(SurfaceHolder holder) {
        try {
            determineDisplayOrientation();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Camera.Parameters parameters;
        try {
            parameters = mCamera.getParameters();
        } catch (Exception x) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }
        try {
            Size bestPreviewSize = determineBestPreviewSize(parameters);
            Size bestPictureSize = determineBestPictureSize(parameters);

            parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);
            parameters.setPictureSize(bestPictureSize.width, bestPictureSize.height);
            parameters.setPictureFormat(ImageFormat.JPEG);
            List<String> flashModes = parameters.getSupportedFlashModes();
            if (flashModes == null || flashModes.isEmpty()) {
                setActionbarLeft3Visible(View.INVISIBLE);
            } else {
                parameters.setFlashMode(mFlashModes[flashModeIndex]);
                setActionbarLeft3Visible(View.VISIBLE);
            }
            if (hasSupportFocus(mCamera)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            mCamera.setParameters(parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) { }
    }

    private Camera.Size determineBestPreviewSize(Camera.Parameters parameters) {
        return determineBestSize(parameters.getSupportedPreviewSizes(), PREVIEW_SIZE_MAX_WIDTH);
    }

    private Camera.Size determineBestPictureSize(Camera.Parameters parameters) {
        return determineBestSize(parameters.getSupportedPictureSizes(), PICTURE_SIZE_MAX_WIDTH);
    }

    private Size determineBestSize(List<Size> sizes, int widthThreshold) {
        Size bestSize = null;
        Size size;
        int numOfSizes = sizes.size();
        for (int i = 0; i < numOfSizes; i++) {
            size = sizes.get(i);
            boolean isDesireRatio = (size.width / 4) == (size.height / 3);
            boolean isBetterSize = (bestSize == null) || size.width > bestSize.width;

            if (isDesireRatio && isBetterSize) {
                bestSize = size;
            }
        }

        if (bestSize == null) {
            return sizes.get(sizes.size() - 1);
        }

        return bestSize;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera == null)
            return;
        setParameter(holder);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_cancel:
                int visible = shutterBtn.getVisibility();
                if (visible != View.VISIBLE) {
                    if (imageTask != null) {
                        imageTask.cancel(true);
                        imageTask = null;
                    }
                    resetCam();
                    nextBtn.setVisibility(View.GONE);
                    mPreview.setVisibility(View.VISIBLE);
                    capturedImageView.setVisibility(View.GONE);
                    shutterBtn.setVisibility(View.VISIBLE);
                } else {
                    setResult(RESULT_CANCELED);
                    finish();
                }
                break;
            case R.id.btn_capture:
                checkPermission(REQUEST_STORAGE_CODE);
                break;
            case R.id.btn_next:
                imageTask = new SaveImageTask();
                imageTask.execute(outputByte);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GET_DESCRIPTION) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    private void takePicture() {
        mOrientationListener.rememberOrientation();
        mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
    }

    private int getBackCameraId() {
        return CameraInfo.CAMERA_FACING_BACK;
    }

    private void resetCam() {
        setParameter(mPreview.getHolder());
    }

    private void switchCameraWithFlipAnimation(int cameraId) {
        AnimationFactory.flipTransitionOneView(rootLayout, FlipDirection.LEFT_RIGHT);
        switchCamera(cameraId);
    }

    OnClickListener actionRefreshRight = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (Camera.getNumberOfCameras() > 1) {
                if (imageTask != null) {
                    imageTask.cancel(true);
                    imageTask = null;
                }
                nextBtn.setVisibility(View.GONE);
                mPreview.setVisibility(View.VISIBLE);
                capturedImageView.setVisibility(View.GONE);
                shutterBtn.setVisibility(View.VISIBLE);
                imageUrls = null;

                if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    switchCameraWithFlipAnimation(Camera.CameraInfo.CAMERA_FACING_FRONT);
                } else {
                    switchCameraWithFlipAnimation(Camera.CameraInfo.CAMERA_FACING_BACK);
                }
            }
        }

    };

    OnClickListener actionAutoLeft = new OnClickListener() {

        @Override
        public void onClick(View v) {
            flashModeIndex++;
            flashModeIndex %= 3;
            setParameter(mHolder);
            int s = mFlashModeTitles[flashModeIndex];
            setActionbarLeft3Title(s);
            Toast.makeText(CameraActivity.this, s, Toast.LENGTH_SHORT).show();
        }
    };

    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    imageUrls = null;
                    shutterBtn.setVisibility(View.INVISIBLE);
                }
            });
        }
    };

    Camera.PictureCallback rawCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        }
    };

    byte[] outputByte = null;

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            int rotation = (mDisplayOrientation
                    + mOrientationListener.getRememberedNormalOrientation() + mLayoutOrientation) % 360;

            // Display image
            Bitmap rawBmp = Utils.rotatePicture(CameraActivity.this, rotation, data);

            // Rotate bitmap
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            rawBmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            outputByte = stream.toByteArray();
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Display image result
            mPreview.setVisibility(View.INVISIBLE);
            capturedImageView.setVisibility(View.VISIBLE);
            capturedImageView.setImageBitmap(rawBmp);
            nextBtn.setVisibility(View.VISIBLE);
            shutterBtn.setVisibility(View.INVISIBLE);
            hideSimpleProgressByPostHandler();
        }
    };

    private class SaveImageTask extends AsyncTask<byte[], Void, String> {
        @Override
        protected String doInBackground(byte[]... data) {
            try {
                showSimpleProgressByPostHandler();
                if (isCancelled()) {
                    return null;
                }
                String outPath = ImageThumbnailProcessor.saveImageToKatzLibrary(data[0],
                        Calendar.getInstance().getTimeInMillis() + ".jpg");
                imageUrls = new String[1];
                imageUrls[0] = outPath;
                return outPath;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideSimpleProgressByPostHandler();
            if (isCancelled()) {
                return;
            }
            if (result == null) {
                return;
            }
            Intent intent = new Intent(CameraActivity.this, ImageInputCommentActivity.class);
            intent.putExtra(ImageInputCommentActivity.INTENT_IS_FROM_CAMERA, true);
            intent.putExtra(ImageInputCommentActivity.INTENT_LIST_URLS, imageUrls);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            startActivityForResult(intent, REQUEST_CODE_GET_DESCRIPTION);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    shutterBtn.setVisibility(View.VISIBLE);
                    mPreview.setVisibility(View.VISIBLE);
                    capturedImageView.setVisibility(View.GONE);
                    nextBtn.setVisibility(View.GONE);
                }
            }, 1000);
        }
    }

    /**
     * When orientation changes, onOrientationChanged(int) of the listener will
     * be called
     */
    private static class CameraOrientationListener extends OrientationEventListener {

        private int mCurrentNormalizedOrientation;

        private int mRememberedNormalOrientation;

        public CameraOrientationListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            if (orientation != ORIENTATION_UNKNOWN) {
                mCurrentNormalizedOrientation = normalize(orientation);
            }
        }

        private int normalize(int degrees) {
            if (degrees > 315 || degrees <= 45) {
                return 0;
            }

            if (degrees > 45 && degrees <= 135) {
                return 90;
            }

            if (degrees > 135 && degrees <= 225) {
                return 180;
            }

            if (degrees > 225 && degrees <= 315) {
                return 270;
            }

            throw new RuntimeException("The physics as we know them are no more. Watch out for anomalies.");
        }

        public void rememberOrientation() {
            mRememberedNormalOrientation = mCurrentNormalizedOrientation;
        }

        public int getRememberedNormalOrientation() {
            return mRememberedNormalOrientation;
        }
    }

    /**
     * Determine the current display orientation and rotate the camera preview
     * accordingly
     */
    private void determineDisplayOrientation() {
        try {
            CameraInfo cameraInfo = new CameraInfo();
            Camera.getCameraInfo(mCameraId, cameraInfo);

            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            int degrees = 0;

            switch (rotation) {
                case Surface.ROTATION_0: {
                    degrees = 0;
                    break;
                }
                case Surface.ROTATION_90: {
                    degrees = 90;
                    break;
                }
                case Surface.ROTATION_180: {
                    degrees = 180;
                    break;
                }
                case Surface.ROTATION_270: {
                    degrees = 270;
                    break;
                }
            }

            int displayOrientation;

            // Camera direction
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                // Orientation is angle of rotation when facing the camera for
                // the camera image to match the natural orientation of the device
                displayOrientation = (cameraInfo.orientation + degrees) % 360;
                displayOrientation = (360 - displayOrientation) % 360;
            } else {
                displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
            }

            mDisplayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
            mLayoutOrientation = degrees;

            mCamera.setDisplayOrientation(displayOrientation);
        } catch (Exception ex) { }
    }

}
