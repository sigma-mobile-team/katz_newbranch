
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactChangeListener;
import sigma.qilex.manager.contact.ContactManager.NotifyCacheContactChangeListener;
import sigma.qilex.ui.activity.IActivity;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.tab.ClickInfoListener;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class CallLogAdatper extends BaseAdapter implements ContactChangeListener, NotifyCacheContactChangeListener {
    private ArrayList<CallLogModel> mItems;

    private ArrayList<CallLogGroupItem> mGroupItems;

    private HashMap<String, QilexContact> mMapContact = new HashMap<String, QilexContact>();

    LoaderManager mLoaderManager;

    private boolean mIsEditMode;

    private WeakReference<Context> mContextWeak;

    public DisplayImageOptions displayImageOptions;

    public ImageLoadingListener animateFirstListener;

    LayoutInflater mInflater;

    HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    ClickInfoListener clickInfoListener;

    ContactManager mContactManager;

    int viewMode;

    Context context;

    boolean mPermissionContact;

    public CallLogAdatper(Context context, boolean isEditMode, ClickInfoListener clickInfoListener) {
        mContextWeak = new WeakReference<Context>(context);
        this.mIsEditMode = isEditMode;
        this.context = context;
        mContactManager = ContactManager.getInstance();
        mInflater = (LayoutInflater)mContextWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPermissionContact = QilexUtils.hasSelfPermission(mContextWeak.get(), IActivity.CONTACT_PERMISSIONS);
        this.clickInfoListener = clickInfoListener;
    }

    public void setEditMode(boolean isEdit) {
        this.mIsEditMode = isEdit;
        notifyDataSetChanged();
        if (!isEdit) {
            clearSelection();
        }
    }

    public boolean isEditMode() {
        return mIsEditMode;
    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public long[] getCurrentCheckIds() {
        Set<Integer> idSet = mSelection.keySet();
        ArrayList<Long> listIds = new ArrayList<Long>();
        for (Integer pos : idSet) {
            ArrayList<CallLogModel> listCallLog = getItem(pos.intValue()).listItems;
            for (CallLogModel callLogModel : listCallLog) {
                listIds.add(callLogModel.getDbId());
            }
        }
        long[] ids = new long[listIds.size()];
        for (int i = 0; i < listIds.size(); i++) {
            ids[i] = listIds.get(i);
        }
        return ids;
    }

    public synchronized void setData(ArrayList<CallLogModel> items, int viewMode) {
        mItems = items;
        this.viewMode = viewMode;
        generateGroupItem();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mGroupItems != null)
            return mGroupItems.size();
        else
            return -1;
    }

    @Override
    public CallLogGroupItem getItem(int position) {
        if (mGroupItems != null)
            return mGroupItems.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return (result == null) ? false : result.booleanValue();
    }

    public long[] getCallLogsIds(int index) {
        ArrayList<CallLogModel> listCallLog = getItem(index).listItems;
        long[] result = new long[listCallLog.size()];
        for (int i = 0; i < listCallLog.size(); i++) {
            result[i] = listCallLog.get(i).getDbId();
        }
        return result;
    }

    public String getPhoneNo(int index) {
        return getItem(index).label;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final CallLogGroupItem item = getItem(position);
        final ViewHolder holder;
        if (v == null) {
            // create a new view
            holder = new ViewHolder();
            v = mInflater.inflate(R.layout.callog_item_mode_edit, null);
            holder.imMarked = (ImageView)v.findViewById(R.id.imgMarked);
            holder.imAvatar = (ImageView)v.findViewById(R.id.imgAvatar);
            holder.imgInfo = (ImageView)v.findViewById(R.id.imgInfo);
            holder.tvName = (MyCustomTextView)v.findViewById(R.id.txtName);
            holder.tvTime = (MyCustomTextView)v.findViewById(R.id.txtTime);
            holder.dumpView = (View)v.findViewById(R.id.dumpView);
            v.setTag(holder);
        } else {
            holder = (ViewHolder)v.getTag();
        }
        holder.tvTime.setText(Utils.generateDateToHumanView(item.dateMilliSecond));
        if (!mIsEditMode) {
            holder.imMarked.setImageResource(getTypeIconResId(item.type));
        } else {
            if (mSelection.get(position) != null) {
                holder.imMarked.setImageResource(R.drawable.icon_delete_selected);
            } else {
                holder.imMarked.setImageResource(R.drawable.icon_delete_unselected);
            }
        }
        holder.imgInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickInfoListener != null)
                    clickInfoListener.onClickInfo(position);
            }
        });

        // binhdt: insert new dumpView to make clicking on Infomation Icon more
        // easier
        holder.dumpView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.e("CallLogAdatper", "CallLogAdatper imgInfo click");
                if (clickInfoListener != null)
                    clickInfoListener.onClickInfo(position);
            }
        });
        // Set Name and Avatar from contact
        QilexContact searchContact = null;
        searchContact = mContactManager.getContactByPhoneNo(mContextWeak.get(), item.label);
        if (searchContact == null) {
            TempContactModel tempContact = mContactManager.getTempContact(item.label);
            if (tempContact != null) {
                searchContact = new QilexContact();
                searchContact.firstName = tempContact.name;
                searchContact.photoUri = tempContact.avatarUrl;
            }
        }
        mMapContact.put(item.label, searchContact);

        String contactName;
        if (searchContact != null) {
            QilexContact contact = new QilexContact(searchContact);
            contactName = contact.getDisplayName();
            MyApplication.getImageLoader().displayImage(contact.getPhotoUri(), holder.imAvatar, displayImageOptions,
                    animateFirstListener);
        } else {
            holder.imAvatar.setImageResource(R.drawable.avt_default_circle);
            contactName = QilexPhoneNumberUtils.formatPhoneNoBlock3Number(item.label);
        }
        if (item.count > 1) {
            contactName += " (" + item.count + ")";
        }
        holder.tvName.setText(contactName);

        if (item.type == CallLogModel.TYPE_MISSCALL) {
            holder.tvName.setTextColor(context.getResources().getColorStateList(R.drawable.color_red_orange_));
        } else {
            holder.tvName.setTextColor(context.getResources().getColorStateList(R.drawable.color_black_orange_));
        }
        return v;
    }

    private void generateGroupItem() {
        mGroupItems = new ArrayList<CallLogAdatper.CallLogGroupItem>();
        if (mItems.isEmpty()) {
            return;
        }

        int index = 0;
        int size = mItems.size();
        CallLogGroupItem group = new CallLogGroupItem();
        group.index = index;
        index++;
        mGroupItems.add(group);
        group.addItem(mItems.get(0));
        for (int i = 1; i < size; i++) {
            CallLogModel callLog = mItems.get(i);
            if ((callLog != null) && (!group.label.equals(callLog.getLabel()) || group.type != callLog.getType())) {
                group = new CallLogGroupItem();
                group.index = index;
                index++;
                mGroupItems.add(group);
            }
            group.addItem(callLog);
        }
    }

    private static class ViewHolder {
        ImageView imMarked;

        ImageView imAvatar;

        ImageView imgInfo;

        MyCustomTextView tvName;

        MyCustomTextView tvTime;

        View dumpView;
    }

    /** return resourceId of icon corresponding to the type */
    public int getTypeIconResId(int type) {
        switch (type) {
            case CallLogModel.TYPE_INCOMING:
                return R.drawable.icon_call_incoming;// R.drawable.icon_in;
            case CallLogModel.TYPE_OUTGOING:
                return R.drawable.icon_call_outgoing;// R.drawable.icon_out;
            case CallLogModel.TYPE_MISSCALL:
                return R.drawable.icon_missed;
            case CallLogModel.TYPE_GLOBAL:
                return R.drawable.icon_global;
            default:
                return 0;
        }
    }

    class CallLogGroupItem {
        public int index;

        public int type;

        public String label;

        public long dateMilliSecond;

        public int count = 0;

        public ArrayList<CallLogModel> listItems = new ArrayList<CallLogModel>();

        public void addItem(CallLogModel model) {
            listItems.add(model);
            if (count == 0) {
                type = model.getType();
                label = model.getLabel();
                dateMilliSecond = model.getDateMilliSecond();
            }
            count++;
        }
    }

    @Override
    public void onNewContact(QilexContact newContact) {
        mMapContact.clear();
    }

    @Override
    public void onUpdateContact(QilexContact newContact) {
        mMapContact.clear();
    }

    @Override
    public void onDeleteContact(QilexContact newContact) {
        mMapContact.clear();
    }

    @Override
    public void onCacheContactChange() {
        ((Activity)mContextWeak.get()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }
}
