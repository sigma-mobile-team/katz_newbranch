
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.customview.widget.CountrySectionIndexer;

public class CountryListAdapter extends BaseAdapter {

    WeakReference<Context> mContextWeak;

    List<CountryItem> listCountry;

    private CountrySectionIndexer indexer = null;

    private LayoutInflater mInflater;

    public CountryListAdapter(Context context) {
        mContextWeak = new WeakReference<Context>(context);
        initCountryList();
        setIndexer(new CountrySectionIndexer(listCountry));
        mInflater = (LayoutInflater)mContextWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private void initCountryList() {
        listCountry = new ArrayList<CountryItem>();
        String[] arrayCountryString = mContextWeak.get().getResources().getStringArray(R.array.countries);

        for (String string : arrayCountryString) {
            CountryItem item = parseFromResourceString(string);
            listCountry.add(item);
        }
    }

    private CountryItem parseFromResourceString(String resource) {
        CountryItem item = new CountryItem();
        String[] countryProps = resource.split(Const.STR_COMMA);
        item.isCategory = false;
        item.code = countryProps[0];
        item.phoneCode = countryProps[1];
        item.name = countryProps[2];
        return item;
    }

    // get the section textview from row view
    // the section view will only be shown for the first item
    public TextView getSectionTextView(View rowView) {
        TextView sectionTextView = (TextView)rowView.findViewById(R.id.sectionTextView);
        return sectionTextView;
    }

    private void showSectionViewIfFirstItem(View rowView, CountryItem item, int position) {
        TextView sectionTextView = getSectionTextView(rowView);
        View divider = rowView.findViewById(R.id.list_divider);
        // if in search mode then dun show the section header
        // if first item then show the header
        if (indexer.isFirstItemInSection(position)) {
            String sectionTitle = indexer.getSectionTitle(item.getName());
            sectionTextView.setText(sectionTitle);
            sectionTextView.setVisibility(View.VISIBLE);
            divider.setVisibility(View.GONE);
        } else {
            divider.setVisibility(View.VISIBLE);
            sectionTextView.setVisibility(View.GONE);
        }
    }

    // this should be override by subclass if necessary
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        CountryItem item = listCountry.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_country_name, parent, false);
            // default just draw the item only
            holder.name = (MyCustomTextView)convertView.findViewById(R.id.txtCountryName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        // for the very first section item, we will draw a section on top
        showSectionViewIfFirstItem(convertView, item, position);
        holder.name.setText(item.getName());
        return convertView;

    }

    public CountrySectionIndexer getIndexer() {
        return indexer;
    }

    public void setIndexer(CountrySectionIndexer indexer) {
        this.indexer = indexer;
    }

    @Override
    public int getCount() {
        if (listCountry != null)
            return listCountry.size();
        return 0;
    }

    @Override
    public CountryItem getItem(int position) {
        if (listCountry != null && !listCountry.isEmpty())
            return listCountry.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {

        public MyCustomTextView name;

    }

    /**
     * Class represent countries phone code
     */
    public class CountryItem {
        boolean isCategory;

        String code;

        String name;

        String phoneCode;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoneCode() {
            return phoneCode;
        }

        public void setPhoneCode(String phoneCode) {
            this.phoneCode = phoneCode;
        }

        public boolean isCategory() {
            return isCategory;
        }

        public void setCategory(boolean isCategory) {
            this.isCategory = isCategory;
        }

    }
}
