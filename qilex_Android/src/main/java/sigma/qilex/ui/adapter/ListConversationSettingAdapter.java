
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ConversationModel;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.utils.Utils;

public class ListConversationSettingAdapter extends BaseAdapter {

    private WeakReference<Context> mContextWeak;

    private ArrayList<ConversationSettingItem> mListItem = new ArrayList<ListConversationSettingAdapter.ConversationSettingItem>();
    
    private HashMap<String, ConversationSettingItem> itemPhoneNumDictionary = new HashMap<>();

    public DisplayImageOptions displayImageOptions;

    public ImageLoadingListener animateFirstListener;

    private LayoutInflater mInflater;

    public ListConversationSettingAdapter(Context context) {
        mContextWeak = new WeakReference<Context>(context);
        mInflater = (LayoutInflater)mContextWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setItemStatus(int index, String status) {
        mListItem.get(index).status = status;
    }
    
    public void setPhoneNumStatus(String phoneNum, String status) {
    	ConversationSettingItem item = itemPhoneNumDictionary.get(phoneNum);
    	if (item != null) {
        	item.setStatus(status);
    	}
    }

    public ArrayList<ConversationSettingItem> getListItem() {
        return mListItem;
    }

    public void setListItem(ArrayList<ConversationModel> listItem, String adminPhoneNo) {
        mListItem.clear();
        itemPhoneNumDictionary.clear();
        int index = 0;
        for (ConversationModel chatModel : listItem) {
            ConversationSettingItem item = new ConversationSettingItem(chatModel);
            item.generateFromModel(chatModel);
            if (index == 0) {
            	if (UserInfo.getInstance().getPhoneNumberFormatted().equals(adminPhoneNo)) {
            		item.isAdmin = true;
            	}
                if (AuthenticationManager.getInstance().isRegistSip() == true) {
                    item.setStatus(mContextWeak.get().getString(R.string.status_online));
                } else {
                    item.setStatus(mContextWeak.get().getString(R.string.status_offline));
                }
            } else {
                item.setStatus(Const.STR_EMPTY);
            	if (chatModel.getKatzNumber().equals(adminPhoneNo)) {
            		item.isAdmin = true;
            	}
            }
            mListItem.add(item);
            itemPhoneNumDictionary.put(chatModel.getKatzNumber(), item);
            index++;
        }
    }

    @Override
    public int getCount() {
        return mListItem.size();
    }

    @Override
    public ConversationSettingItem getItem(int position) {
        return mListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ConversationSettingItem item = getItem(position);
        ViewHolder holder;
        if (v == null) {
            v = mInflater.inflate(R.layout.conversation_setting_item, null);
            holder = new ViewHolder();
            holder.imgAvatar = (ImageView)v.findViewById(R.id.imgAvatar);
            holder.txtName = (TextView)v.findViewById(R.id.txtName);
            holder.txtStatus = (TextView)v.findViewById(R.id.txtStatus);
            holder.txtAdmin = (TextView)v.findViewById(R.id.txtAdmin);
            v.setTag(holder);
        } else {
            holder = (ViewHolder)v.getTag();
        }
        // Avatar
        MyApplication.getImageLoader().displayImage(item.getAvatarUrl(), holder.imgAvatar, displayImageOptions,
                animateFirstListener);
        // Name
        holder.txtName.setText(item.getName());
        // Time
        if (Utils.isStringNullOrEmpty(item.getStatus())) {
            holder.txtStatus.setVisibility(View.GONE);
        } else {
            holder.txtStatus.setVisibility(View.VISIBLE);
            holder.txtStatus.setText(item.getStatus());
        }
        
        // Admin
        if (item.isAdmin) {
        	holder.txtAdmin.setVisibility(View.VISIBLE);
        } else {
        	holder.txtAdmin.setVisibility(View.GONE);
        }

        return v;
    }

    static class ViewHolder {
        ImageView imgAvatar;

        TextView txtName;

        TextView txtStatus;
        
        TextView txtAdmin;
    }

    private static class ConversationSettingItem {

        private String avatarUrl;

        private String name;

        private String status;
        
        private String katzPhoneNum;
        
        public boolean isAdmin = false;

        public ConversationSettingItem(ConversationModel model) {
            generateFromModel(model);
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public String getName() {
            return name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getKatzPhoneNum() {
			return katzPhoneNum;
		}

		public void setKatzPhoneNum(String katzPhoneNum) {
			this.katzPhoneNum = katzPhoneNum;
		}

		private void generateFromModel(ConversationModel model) {
            this.avatarUrl = model.getAvatarUrl();
            this.name = model.getName();
            this.katzPhoneNum = model.getKatzNumber();
        }

    }

}
