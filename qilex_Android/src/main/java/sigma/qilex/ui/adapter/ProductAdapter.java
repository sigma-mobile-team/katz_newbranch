
package sigma.qilex.ui.adapter;

import java.util.Collection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import pl.katz.aero2.Const.Config;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.http.GooglePlayProductListModel;

public class ProductAdapter extends ArrayAdapter<GooglePlayProductListModel> {

    private LayoutInflater mInflater;

    // Constructors
    public ProductAdapter(Context context) {
        super(context, 0);
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public synchronized void addAll(Collection<? extends GooglePlayProductListModel> collection) {
        super.addAll(collection);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_product, parent, false);
            holder = new ViewHolder();
            holder.tvPrice = (TextView)view.findViewById(R.id.tvPrice);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }

        GooglePlayProductListModel item = getItem(position);
        holder.tvPrice.setText(String.format(Config.MONEY_FORMAT, String.valueOf(item.product_value)));

        return view;
    }

    private static class ViewHolder {

        public TextView tvPrice;

    }
}
