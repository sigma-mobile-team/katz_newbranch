
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.adapter.ContactDetailPhoneListAdapter.PhoneItem;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class ContactDetailPhoneListUpdateAdapter extends BaseAdapter {

    public static final int NORMAL_TYPE = 0;

    public static final int DELETE_TYPE = 1;

    WeakReference<BaseActivity> mActivityWeak;

    DisplayImageOptions options;

    ArrayList<PhoneItem> listNumbers;

    ArrayList<String> listNumberEdit;

    HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    int viewMode = NORMAL_TYPE;

    private boolean mIsEditable = false;

    private LayoutInflater mInflater;

    public ContactDetailPhoneListUpdateAdapter(BaseActivity baseActivity, ArrayList<PhoneNumberModel> listNumbers,
            ArrayList<PhoneNumberModel> newNumbers) {
        mActivityWeak = new WeakReference<BaseActivity>(baseActivity);
        options = baseActivity.circleDisplayImageOptions;
        createListPhoneItem(listNumbers, newNumbers);
        mInflater = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ContactDetailPhoneListUpdateAdapter(BaseActivity baseActivity, ArrayList<PhoneNumberModel> listNumbers,
            ArrayList<PhoneNumberModel> newNumbers, int viewMode) {
        this(baseActivity, listNumbers, newNumbers);
        this.viewMode = viewMode;
    }

    // this should be override by subclass if necessary
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        final PhoneItem item = listNumbers.get(position);
        holder = new ViewHolder();
        convertView = mInflater.inflate(R.layout.contact_edit_number_item, parent, false);
        // default just draw the item only
        holder.number = (MyCustomEditText)convertView.findViewById(R.id.number);
        holder.type = (MyCustomTextView)convertView.findViewById(R.id.type);
        holder.frifonOut = (ImageView)convertView.findViewById(R.id.frifon_out);
        holder.msg = (ImageView)convertView.findViewById(R.id.msg);
        holder.call = (ImageView)convertView.findViewById(R.id.call);
        holder.delete = (ImageView)convertView.findViewById(R.id.delete);
        holder.layoutEditNumner = (LinearLayout)convertView.findViewById(R.id.layoutEditNumner);
        holder.deleteLayout = convertView.findViewById(R.id.deleteLayout);
        convertView.setTag(holder);

        holder.delete.setVisibility(View.VISIBLE);
        if (item.selected) {
            holder.delete.setImageResource(R.drawable.icon_delete_selected);
            holder.number.setEnabled(false);
            holder.number.setText(item.number);
            listNumberEdit.remove(position);
            listNumberEdit.add(position, item.number);
        } else {
            holder.delete.setImageResource(R.drawable.icon_delete_unselected);
            holder.number.setEnabled(true);
            holder.number.setText(listNumberEdit.get(position));
            holder.layoutEditNumner.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.number.setSelection(0, holder.number.getText().toString().length());
                    mActivityWeak.get().showKeyboard(holder.number);
                }
            });
        }
        holder.deleteLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.selected) {
                    removeSelection(position);
                } else {
                    setNewSelection(position, true);
                }
                item.selected = !item.selected;
                mIsEditable = true;
                notifyDataSetChanged();
            }
        });
        holder.number.addTextChangedListener(new PhoneTextWatcher(holder.number, position));

        holder.type.setText(item.type);
        return convertView;
    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public List<PhoneItem> getItemsCheck() {
        Set<Integer> keys = getCurrentCheckedPosition();
        List<PhoneItem> items = new ArrayList<PhoneItem>();
        for (Integer i : keys) {
            items.add(listNumbers.get(i));
        }
        return items;
    }

    public List<PhoneItem> getListPhoneDelete() {
        Set<Integer> keys = getCurrentCheckedPosition();
        List<PhoneItem> items = new ArrayList<PhoneItem>();
        for (Integer i : keys) {
            items.add(listNumbers.get(i));
        }
        return items;
    }

    public boolean isEditable() {
        return mIsEditable;
    }

    public void getListInsertDeletePhone(ArrayList<PhoneItem> outInsert, ArrayList<PhoneItem> outDelete) {
        Set<Integer> keys = getCurrentCheckedPosition();

        int size = listNumbers.size();
        // Check delete numbers
        for (int i = 0; i < size; i++) {
            PhoneItem phoneItem = listNumbers.get(i);
            // If number is marked delete
            boolean isDeleted = true;
            for (int editIndex = 0; editIndex < size; editIndex++) {
                String editNumber = listNumberEdit.get(editIndex);
                if (Utils.isStringNullOrEmpty(editNumber.replace("+", Const.STR_EMPTY))) {
                    continue;
                }
                if (keys.contains(editIndex) == false
                        && QilexPhoneNumberUtils.checkPhoneIsEqual(phoneItem.number, editNumber) == true) {
                    isDeleted = false;
                    break;
                }
            }
            if (isDeleted == true) {
                int outDeleteSize = outDelete.size();
                boolean hasInList = false;
                for (int outDeleteIndex = 0; outDeleteIndex < outDeleteSize; outDeleteIndex++) {
                    if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneItem.number,
                            outDelete.get(outDeleteIndex).number)) {
                        hasInList = true;
                        break;
                    }
                }
                if (hasInList == false) {
                    outDelete.add(phoneItem);
                }
            }
        }

        // Check insert number
        for (int editIndex = 0; editIndex < size; editIndex++) {
            if (keys.contains(editIndex) == true) {
                continue;
            }
            boolean isAdded = true;
            String type = null;
            String editNumber = listNumberEdit.get(editIndex);
            if (Utils.isStringNullOrEmpty(editNumber.replace("+", Const.STR_EMPTY))) {
                continue;
            }
            // Check if edit number is contain in origin number or not
            for (int i = 0; i < size; i++) {
                PhoneItem phoneItem = listNumbers.get(i);
                if (QilexPhoneNumberUtils.checkPhoneIsEqual(phoneItem.number, editNumber) == true
                        && phoneItem.isNew == false) {
                    isAdded = false;
                    type = phoneItem.type;
                    break;
                }
            }
            if (isAdded == true) {
                int outAddSize = outInsert.size();
                boolean hasInList = false;
                for (int outAddIndex = 0; outAddIndex < outAddSize; outAddIndex++) {
                    if (QilexPhoneNumberUtils.checkPhoneIsEqual(editNumber, outInsert.get(outAddIndex).number)) {
                        hasInList = true;
                        break;
                    }
                }
                if (hasInList == false) {
                    PhoneItem item = new PhoneItem();
                    item.number = editNumber;
                    item.type = type;
                    outInsert.add(item);
                }
            }
        }
    }

    static class ViewHolder {

        public MyCustomEditText number;

        public MyCustomTextView type;

        public ImageView call;

        public ImageView msg;

        public ImageView frifonOut;

        public ImageView delete;

        public LinearLayout layoutEditNumner;

        public View deleteLayout;
    }

    @Override
    public int getCount() {
        return listNumbers.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private void createListPhoneItem(ArrayList<PhoneNumberModel> phones, ArrayList<PhoneNumberModel> newNumbers) {
        listNumbers = new ArrayList<PhoneItem>();
        listNumberEdit = new ArrayList<String>();
        for (PhoneNumberModel phone : phones) {
            PhoneItem item = new PhoneItem();
            item.number = phone.phoneNumber;
            item.type = phone.type;
            item.selected = false;
            item.originNumber = phone.phoneOriginal;
            listNumberEdit.add(item.number);
            listNumbers.add(item);
        }
        for (PhoneNumberModel phone : newNumbers) {
            PhoneItem item = new PhoneItem();
            item.number = phone.phoneNumber;
            item.type = phone.type;
            item.selected = false;
            item.isNew = true;
            item.originNumber = phone.phoneOriginal;
            listNumberEdit.add(item.number);
            listNumbers.add(item);
        }
    }

    private class PhoneTextWatcher implements TextWatcher {
        int listIndex;

        View viewContain;

        public PhoneTextWatcher(View viewContain, int listIndex) {
            this.viewContain = viewContain;
            this.listIndex = listIndex;
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String phoneNo = QilexPhoneNumberUtils.convertToBasicNumber(s.toString());
            viewContain.setTag(phoneNo);
            listNumberEdit.remove(listIndex);
            listNumberEdit.add(listIndex, phoneNo);
            mIsEditable = true;
        }

    }
}
