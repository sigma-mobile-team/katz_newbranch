
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.ChatThreadModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactChangeListener;
import sigma.qilex.manager.contact.ContactManager.NotifyCacheContactChangeListener;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.ConversationActivity;
import sigma.qilex.ui.activity.IActivity;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.tab.ClickInfoListener;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class ListConversationChatAdapter extends BaseAdapter
        implements ContactChangeListener, NotifyCacheContactChangeListener {

    private WeakReference<Context> mContextWeak;

    private ArrayList<ConversationChatItem> mListItem;

    private ArrayList<ChatThreadModel> mListChatThreadModel;

    private HashMap<String, QilexContact> mMapContact = new HashMap<String, QilexContact>();

    private boolean isEditMode;

    public DisplayImageOptions displayImageOptions;

    public ImageLoadingListener animateFirstListener;

    public ArrayList<Long> listSelectedThreadId;

    private ContactManager mContactManager;

    LayoutInflater mInflater;

    ClickInfoListener clickInfoListener;

    private int mTotal;

    private OnLoadMoreListener mOnLoadMoreListener;

    boolean mPermissionContact;

    private String HAS_CONTACT_JOINED;

    private String HAS_CONTACT_LEFT;

    private String LOCATION_MESSAGE;

    private String IMAGE_MESSAGE;

    public void setClickInfoListener(ClickInfoListener clickInfoListener) {
        this.clickInfoListener = clickInfoListener;
    }

    public ListConversationChatAdapter(Context context, boolean isEditMode, OnLoadMoreListener onLoadMoreListener) {
    	HAS_CONTACT_JOINED = MyApplication.getAppContext().getString(R.string.has_contact_is_join);
    	HAS_CONTACT_LEFT = MyApplication.getAppContext().getString(R.string.has_contact_is_kick);
    	LOCATION_MESSAGE = MyApplication.getAppContext().getString(R.string.location_message);
    	IMAGE_MESSAGE = MyApplication.getAppContext().getString(R.string.image_message);
        this.mOnLoadMoreListener = onLoadMoreListener;
        this.mContactManager = ContactManager.getInstance();
        mContextWeak = new WeakReference<Context>(context);
        this.isEditMode = isEditMode;
        this.listSelectedThreadId = new ArrayList<Long>();
        this.mInflater = (LayoutInflater)mContextWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPermissionContact = QilexUtils.hasSelfPermission(mContextWeak.get(), IActivity.CONTACT_PERMISSIONS);
    }

    public void setListItem(ArrayList<ChatThreadModel> listItem) {
        this.mListChatThreadModel = listItem;
        // Create list of ConversationChatItem
        mListItem = new ArrayList<ConversationChatItem>();
        for (ChatThreadModel chatModel : mListChatThreadModel) {
            ConversationChatItem item = new ConversationChatItem(chatModel);
            mListItem.add(item);
        }
    }

    public void setListItem(ArrayList<ChatThreadModel> listItem, int total) {
        setListItem(listItem);
        mTotal = total;
    }

    @Override
    public int getCount() {
        int count = mListChatThreadModel == null ? 0 : mListChatThreadModel.size();
        if (mTotal > count) {
            count++;
        }
        return count;
    }

    @Override
    public ChatThreadModel getItem(int position) {
        return mListChatThreadModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (position >= mListItem.size()) {
            return -1;
        }
        return getItem(position).id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        // If item is Load more
        if (position >= mListItem.size()) {
            mOnLoadMoreListener.onLoadMore();
            v = mInflater.inflate(R.layout.item_load_more, null);
            return v;
        }

        // If item is chat item
        final ConversationChatItem item = mListItem.get(position);
        final ViewHolder holder;
        if (v == null || v.getTag() == null) {
            // create a new view
            holder = new ViewHolder();
            v = mInflater.inflate(R.layout.conversation_chat_item, null);
            holder.imgSelected = (ImageView)v.findViewById(R.id.imgMarked);
            holder.imgAvatar = (ImageView)v.findViewById(R.id.imgAvatar);
            holder.txtName = (TextView)v.findViewById(R.id.txtName);
            holder.txtTime = (TextView)v.findViewById(R.id.txtTime);
            holder.txtSnippet = (MyCustomTextView)v.findViewById(R.id.txtContent);
            holder.notifyLayout = v.findViewById(R.id.notifyIcon);
            holder.txtNotify = (TextView)v.findViewById(R.id.txtNotify);
            holder.layoutInfo = v.findViewById(R.id.layoutInfo);
            v.setTag(holder);
        } else {
            holder = (ViewHolder)v.getTag();
        }

        if (isEditMode) {
            holder.imgSelected.setVisibility(View.VISIBLE);
            if (item.isMarkDelete) {
                holder.imgSelected.setImageResource(R.drawable.icon_delete_selected);
            } else {
                holder.imgSelected.setImageResource(R.drawable.icon_delete_unselected);
            }
        } else {
            holder.imgSelected.setVisibility(View.INVISIBLE);
        }

        holder.layoutInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (clickInfoListener != null)
                    clickInfoListener.onClickInfo(position);
            }
        });

        String phoneNumber = item.contactStr;
        String avatarUrl = item.avatarUrl;
        String name = item.name;

        // Avatar
        if (QilexPhoneNumberUtils.isSupportNumber(phoneNumber)) {
            holder.imgAvatar.setImageResource(R.drawable.ic_round);
        } else {
            if (avatarUrl != null) {
                MyApplication.getImageLoader().displayImage(avatarUrl, holder.imgAvatar, displayImageOptions,
                        animateFirstListener);
            } else {
                holder.imgAvatar.setImageResource(R.drawable.avt_default_circle);
            }
        }

        // Name
        holder.txtName.setText(name);

        // Time
        holder.txtTime.setText(item.date);

        // Snippet
        holder.txtSnippet.setText(Html.fromHtml(item.firstChat));

        // Notify Number
        if (item.notifyNum > 0) {
            holder.txtNotify.setText(item.notifyNum > 99 ? "99+" : String.valueOf(item.notifyNum));
            holder.notifyLayout.setVisibility(View.VISIBLE);
        } else {
            holder.notifyLayout.setVisibility(View.GONE);
        }

        if (QilexPhoneNumberUtils.isSupportNumber(item.contactStr) == true) {
            holder.layoutInfo.setVisibility(View.INVISIBLE);
        } else {
            holder.layoutInfo.setVisibility(View.VISIBLE);
        }

        return v;
    }

    public boolean isEditMode() {
        return isEditMode;
    }

    public void setEditMode(boolean isEditMode) {
        if (mListItem == null) {
            return;
        }
        this.isEditMode = isEditMode;

        int size = mListItem.size();
        for (int i = 0; i < size; i++) {
            mListItem.get(i).isMarkDelete = false;
        }
        listSelectedThreadId.clear();
    }

    public long[] getListSelectedIds() {
        int size = listSelectedThreadId.size();
        long[] result = new long[size];
        for (int i = 0; i < size; i++) {
            result[i] = listSelectedThreadId.get(i);
        }
        return result;
    }

    public void clearSelectedIds() {
        listSelectedThreadId.clear();
    }

    public void clickOnItem(int position, View v) {
        // If item is Chat thread
        ConversationChatItem item = mListItem.get(position);
        if (isEditMode) {
            item.isMarkDelete = !item.isMarkDelete;
            ImageView imgSelected = (ImageView)v.findViewById(R.id.imgMarked);
            if (item.isMarkDelete) {
                listSelectedThreadId.add(item.threadId);
                imgSelected.setImageResource(R.drawable.icon_delete_selected);
            } else {
                listSelectedThreadId.remove(item.threadId);
                imgSelected.setImageResource(R.drawable.icon_delete_unselected);
            }
        } else {
            Intent intent = new Intent(mContextWeak.get(), ConversationActivity.class);
            intent.putExtra(ConversationActivity.INTENT_PARAM_THREAD_ID, item.threadId);
            intent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, item.contactStr);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            mContextWeak.get().startActivity(intent);
        }
    }

    private static class ViewHolder {
        public ImageView imgSelected;

        public ImageView imgAvatar;

        public TextView txtName;

        public TextView txtTime;

        public MyCustomTextView txtSnippet;

        public View notifyLayout;

        public TextView txtNotify;

        public View layoutInfo;
    }

    public class ConversationChatItem {
        private boolean isMarkDelete;

        public String avatarUrl;

        public String name;

        public String date;

        public String firstChat;

        public long threadId;

        public String contactStr;

        public int notifyNum;

        public ConversationChatItem(ChatThreadModel model) {
            generateFromModel(model);
        }

        private void generateFromModel(ChatThreadModel model) {
            this.date = Utils.generateDateToHumanView(model.dateMillis);
            this.isMarkDelete = false;

            if (model.type == ChatDatabase.MSG_TYPE_CALLLOG) {
                this.firstChat = String.format("<font color=\"#FF0000\"> %s </font>",
                        mContextWeak.get().getResources().getString(R.string.missed_call));
            } else {
                String msgType = model.type == ChatDatabase.MSG_TYPE_SEND
                        ? String.format("[%s]", mContextWeak.get().getResources().getString(R.string.calllog_type_out))
                        : String.format("[%s]", mContextWeak.get().getResources().getString(R.string.calllog_type_in));
                String snippet = model.snippet;
                if (model.mimetype == ChatDatabase.MSG_MINETYPE_LOCATION) {
                    snippet = LOCATION_MESSAGE;
                    this.firstChat = String.format("%s %s", msgType, snippet);
                } else if (model.mimetype == ChatDatabase.MSG_MINETYPE_IMAGE) {
                    snippet = IMAGE_MESSAGE;
                    this.firstChat = String.format("%s %s", msgType, snippet);
                } else if (model.mimetype == ChatDatabase.MSG_MINETYPE_GROUP_INVITE) {
                	this.firstChat = HAS_CONTACT_JOINED;
                } else if (model.mimetype == ChatDatabase.MSG_MINETYPE_GROUP_KICK) {
                	this.firstChat = HAS_CONTACT_LEFT;
                } else {
                	this.firstChat = String.format("%s %s", msgType, snippet);
                }
            }
            this.threadId = model.id;
            this.contactStr = model.contactStr;
            this.notifyNum = model.unreadCount;

            if (model.isGroupChat()) {
            	this.avatarUrl = Const.STR_EMPTY;
            	ArrayList<String> listName = new ArrayList<>();
            	String []listPhone = model.getListPhoneNumberParticipantExceptYou();
            	for (String phone : listPhone) {
					String[]datas = mContactManager.getContactNameAndImageByPhoneNo(phone);
					if (datas != null) {
						listName.add(datas[0]);
					} else {
						listName.add(phone);
					}
				}
            	
            	if (listPhone.length == 0) {
            		this.name = mContextWeak.get().getResources().getString(R.string.none_participant);
            	} else {
            		this.name = TextUtils.join(", ", listName.toArray(listPhone));
            	}
            } else {
            	QilexContact contact = null;
                String phoneNumber = model.contactStr;
                contact = mContactManager.getContactByPhoneNo(mContextWeak.get(), phoneNumber);
                if (contact == null) {
                    TempContactModel tempContact = mContactManager.getTempContact(phoneNumber);
                    if (tempContact != null) {
                        contact = new QilexContact();
                        contact.firstName = tempContact.name;
                        contact.photoUri = tempContact.avatarUrl;
                    }
                }
                mMapContact.put(phoneNumber, contact);
                if (contact != null) {
                    avatarUrl = contact.getPhotoUri();
                    name = contact.getDisplayName();
                } else {
                    avatarUrl = null;
                    name = QilexPhoneNumberUtils.formatPhoneNoBlock3Number(phoneNumber);
                }
            }
        }
    }

    public enum CallLogType {
        GLOBAL, IN, OUT
    }

    public interface OnLoadMoreListener {
        public void onLoadMore();
    }

    @Override
    public void onNewContact(QilexContact newContact) {
        mMapContact.clear();
    }

    @Override
    public void onUpdateContact(QilexContact newContact) {
        mMapContact.clear();
    }

    @Override
    public void onDeleteContact(QilexContact newContact) {
        mMapContact.clear();
    }

    @Override
    public void onCacheContactChange() {
        ((Activity)mContextWeak.get()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }
}
