/*
 * Copyright 2014 trinea.cn All right reserved. This software is the confidential and proprietary information of
 * trinea.cn ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with trinea.cn.
 */

package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.model.http.OutCallListHistoryModel.OutCallModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.IActivity;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class KATZOutHistoryAdapter extends BaseAdapter implements OnClickListener {

    private HashMap<String, QilexContact> mMapContact = new HashMap<String, QilexContact>();

    private WeakReference<BaseActivity> mActivityWeak;

    private ArrayList<OutCallModel> mArrKATZOut = new ArrayList<OutCallModel>();

    private ArrayList<String> listStartTime = new ArrayList<>();

    LayoutInflater mInflater;

    public DisplayImageOptions displayImageOptions;

    ContactManager mContactManager;

    boolean mPermissionContact;

    private boolean isLoading = false;

    private boolean isLoadEnd = false;

    private Runnable onClickRunnable;

    public KATZOutHistoryAdapter(BaseActivity baseActivity, ArrayList<OutCallModel> arrFrifonOut,
            Runnable onClickRunnable) {
        mContactManager = ContactManager.getInstance();
        mActivityWeak = new WeakReference<BaseActivity>(baseActivity);
        this.mArrKATZOut = arrFrifonOut;
        mInflater = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPermissionContact = QilexUtils.hasSelfPermission(mActivityWeak.get(), IActivity.CONTACT_PERMISSIONS);
        this.onClickRunnable = onClickRunnable;
        generateListStartTime();
    }

    private void generateListStartTime() {
        int size = mArrKATZOut.size();
        for (int i = 0; i < size; i++) {
            listStartTime.add(mArrKATZOut.get(i).start);
        }
    }

    public void addSourceToList(ArrayList<OutCallModel> arrKATZOut) {
        isLoading = false;
        for (OutCallModel callModel : arrKATZOut) {
            if (listStartTime.contains(callModel.start) == false) {
                mArrKATZOut.add(callModel);
                listStartTime.add(callModel.start);
            }
        }
    }

    public ArrayList<OutCallModel> getmCalList() {
        return mArrKATZOut;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void setLoadEnd(boolean isLoadEnd) {
        this.isLoadEnd = isLoadEnd;
    }

    @Override
    public int getCount() {
        if (mArrKATZOut.isEmpty() == true) {
            return 0;
        } else if (isLoadEnd == false) {
            return mArrKATZOut.size() + 1;
        } else {
            return mArrKATZOut.size();
        }
    }

    @Override
    public View getView(final int position, View view, ViewGroup container) {
        if (position >= mArrKATZOut.size()) {
            // mOnLoadMoreListener.onLoadMore();
            View v = mInflater.inflate(R.layout.item_load_more, null);
            if (isLoading == true) {
                v.findViewById(R.id.progress).setVisibility(View.VISIBLE);
            }
            v.setOnClickListener(this);
            return v;
        }

        ViewHolder holder;
        if (view == null || view.getTag() == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.list_frifonout_history_item, container, false);
            holder.time = (MyCustomTextView)view.findViewById(R.id.frifon_out_time);
            holder.name = (MyCustomTextView)view.findViewById(R.id.frifon_out_name);
            holder.duration = (MyCustomTextView)view.findViewById(R.id.frifon_out_duration);
            holder.avatar = (ImageView)view.findViewById(R.id.frifon_out_avt);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }
        OutCallModel model = mArrKATZOut.get(position);
        holder.time.setText(model.start);
        holder.duration.setText(model.duration);

        QilexContact searchContact = null;
        if (mMapContact.containsKey(model.called)) {
            searchContact = mMapContact.get(model.called);
        } else if (mPermissionContact) {
            searchContact = mContactManager.getContactByPhoneNoFromPhoneBook(mActivityWeak.get(), model.called);
        }
        if (searchContact == null) {
            TempContactModel tempContact = mContactManager.getTempContact(model.called);
            if (tempContact != null) {
                searchContact = new QilexContact();
                searchContact.firstName = tempContact.name;
                searchContact.photoUri = tempContact.avatarUrl;
            }
        }
        mMapContact.put(model.called, searchContact);

        if (searchContact != null && Utils.isStringNullOrEmpty(searchContact.getPhotoUri()) == false) {
            holder.name.setText(searchContact.getDisplayName());
            MyApplication.getImageLoader().displayImage(searchContact.getPhotoUri(), holder.avatar, displayImageOptions,
                    mActivityWeak.get().animateFirstListener);
        } else {
            holder.name.setText(QilexPhoneNumberUtils.formatPhoneNoBlock3Number(model.called));
            holder.avatar.setImageResource(R.drawable.avt_default_circle);
        }
        return view;
    }

    private static class ViewHolder {
        MyCustomTextView time;

        MyCustomTextView name;

        MyCustomTextView duration;

        ImageView avatar;
    }

    @Override
    public OutCallModel getItem(int position) {
        int listSize = mArrKATZOut.size();
        if (listSize > 0) {
            if (position >= listSize) {
                return null;
            } else {
                return mArrKATZOut.get(position);
            }
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {
        if (isLoading == false) {
            isLoading = true;
            v.findViewById(R.id.progress).setVisibility(View.VISIBLE);
            onClickRunnable.run();
        }
    }

}
