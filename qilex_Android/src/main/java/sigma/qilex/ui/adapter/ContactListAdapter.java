
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.customview.widget.ContactsSectionIndexer;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class ContactListAdapter extends BaseAdapter implements Filterable {

    private boolean inSearchMode = false;

    WeakReference<BaseActivity> mActivityWeak;

    DisplayImageOptions options;

    List<QilexContact> mItems;
    
    List<QilexContact> mSelectedItems = new ArrayList<QilexContact>();
    
    List<QilexContact> mSelectedItemsCannotChanged = new ArrayList<QilexContact>();

    private ContactsSectionIndexer indexer = null;

    boolean browserContact = false;

    HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    private ValueFilter valueFilter;

    private List<QilexContact> mStringFilterList;

    private List<String> mNumberChecks;

    private LayoutInflater mInflater;

    public ContactListAdapter(BaseActivity baseActivity, List<QilexContact> contacts, boolean browserMode) {
        mActivityWeak = new WeakReference<BaseActivity>(baseActivity);
        options = mActivityWeak.get().circleDisplayImageOptions;
        this.mItems = contacts;
        mStringFilterList = contacts;
        this.browserContact = browserMode;
        setIndexer(new ContactsSectionIndexer(mItems));
        mInflater = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public List<QilexContact> getListContacts() {
        return mItems;
    }

    public void setItems(List<QilexContact> contacts) {
        this.mItems = contacts;
        mStringFilterList = contacts;
        setIndexer(new ContactsSectionIndexer(mItems));
    }

    public void setNewSelection(int position, boolean value) {
        QilexContact ctx = mItems.get(position);
        if (mSelectedItemsCannotChanged.contains(ctx)) {
        	return;
        }

        mSelection.put(position, value);
        if (!mSelectedItems.contains(ctx)) {
        	mSelectedItems.add(mItems.get(position));// Add
        }
        notifyDataSetChanged();
    }

    public void removeSelection(int position) {
    	QilexContact ctx = mItems.get(position);
        if (mSelectedItemsCannotChanged.contains(ctx)) {
        	return;
        }
        mSelection.remove(position);
        mSelectedItems.remove(ctx);// Add
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public boolean isPositionChecked(int position) {
    	if (mSelectedItems.contains(mItems.get(position))) {
    		return true;
    	}
    	return false;
//    	
//        Boolean result = mSelection.get(position);
//        return (result == null) ? false : result.booleanValue();
    }

    public void setCurrentCheckNumbers(List<String> numbers) {
        mNumberChecks = numbers;

        int size = mItems.size();
    	for (int i = 0; i < size; i++) {
    		QilexContact contact = mItems.get(i);
    		if (numbers.contains(contact.getFirstKATZNumber().phoneNumber)) {
    			mSelection.put(i, true);
    			if (!mSelectedItems.contains(contact)) {
    				mSelectedItems.add(contact);// Add
    			}
    		}
    	}
    }
    
    public void setCurrentCheckNumbersCannotModify(List<String> numbers, boolean isRemoveOwnPhone) {
    	mNumberChecks = numbers;
    	int size = mItems.size();
    	for (int i = 0; i < size; i++) {
    		QilexContact contact = mItems.get(i);
    		if (numbers.contains(contact.getFirstKATZNumber().phoneNumber)) {
    			mSelection.put(i, true);
    			if (!mSelectedItems.contains(contact)) {
    				mSelectedItems.add(contact);// Add
    				mSelectedItemsCannotChanged.add(contact);
    			}
    		} else if (isRemoveOwnPhone && contact.getFirstKATZNumber().phoneNumber.equals(UserInfo.getInstance().getPhoneNumberFormatted())) {
    			mSelection.put(i, true);
    			if (!mSelectedItems.contains(contact)) {
    				mSelectedItems.add(contact);// Add
    				mSelectedItemsCannotChanged.add(contact);
    			}
    		}
    	}
    }
    
    public int getNumberOfDisableContact() {
    	return mSelectedItemsCannotChanged.size();
    }
    
    public int getNumberOfDisableContactExceptYou() {
    	ArrayList<QilexContact> contacts = new ArrayList<>();
    	int size = mSelectedItemsCannotChanged.size();
    	for (int i = 0; i < size; i++) {
    		if (!UserInfo.getInstance().getPhoneNumberFormatted().equals(
    				mSelectedItemsCannotChanged.get(i).getFirstKATZNumber().phoneNumber)) {
    			contacts.add(mSelectedItemsCannotChanged.get(i));
    		}
    	}
    	return contacts.size();
    }

    public String[] getCurrentCheckNumbers() {
    	ArrayList<QilexContact> listChecked = new ArrayList<>(mSelectedItems);
    	ArrayList<String> phoneNo = new ArrayList<>();
    	for (QilexContact qilexContact : listChecked) {
    		phoneNo.add(qilexContact.getFirstKATZNumber().phoneNumber);
		}
    	String[] result = new String[phoneNo.size()];
    	return phoneNo.toArray(result);
    }

    // get the section textview from row view
    // the section view will only be shown for the first item
    public TextView getSectionTextView(View rowView) {
        TextView sectionTextView = (TextView)rowView.findViewById(R.id.sectionTextView);
        return sectionTextView;
    }

    public void changeBrowserMode(boolean browserContactMode) {
        browserContact = browserContactMode;
        notifyDataSetChanged();
    }

    private void showSectionViewIfFirstItem(View rowView, QilexContact item, int position) {
        TextView sectionTextView = getSectionTextView(rowView);
        View divider = rowView.findViewById(R.id.list_divider);
        // if in search mode then dun show the section header
        if (inSearchMode) {
            sectionTextView.setVisibility(View.GONE);
        } else {
            // if first item then show the header
            if (indexer.isFirstItemInSection(position)) {
                String sectionTitle = indexer.getSectionTitle(item.getDisplayName().trim());
                sectionTextView.setText(sectionTitle);
                sectionTextView.setVisibility(View.VISIBLE);
                divider.setVisibility(View.GONE);
            } else {
                divider.setVisibility(View.VISIBLE);
                sectionTextView.setVisibility(View.GONE);
            }
        }
    }

    // this should be override by subclass if necessary
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        QilexContact item = mItems.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.contact_list_item, parent, false);
            // default just draw the item only
            holder.name = (MyCustomTextView)convertView.findViewById(R.id.user_name);
            holder.mark = (ImageView)convertView.findViewById(R.id.imgMarked);
            holder.mark.setVisibility(browserContact ? View.VISIBLE : View.INVISIBLE);
            holder.avatar = (ImageView)convertView.findViewById(R.id.user_avatar);
            holder.favorite = (ImageView)convertView.findViewById(R.id.favorite);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        // for the very first section item, we will draw a section on top
        showSectionViewIfFirstItem(convertView, item, position);
        holder.name.setText(item.getDisplayName());
        if (browserContact == true) {
//            if (mSelection.get(position) != null || isNumberCheck(item.getFirstKATZNumber().phoneNumber)) {
            if (mSelectedItems.contains(item)) {
            	if (mSelectedItemsCannotChanged.contains(item)) {
                    holder.mark.setImageResource(R.drawable.icon_check_disable);
            	} else {
                    holder.mark.setImageResource(R.drawable.icon_check);
            	}
                setNewSelection(position, true);
            } else {
                holder.mark.setImageResource(R.drawable.icon_delete_unselected);
            }
        }

        if (item.isKATZ()) {
            holder.favorite.setVisibility(View.VISIBLE);
        } else {
            holder.favorite.setVisibility(View.GONE);
        }
        MyApplication.getImageLoader().displayImage(item.getPhotoUri(), holder.avatar, options,
                mActivityWeak.get().animateFirstListener);
        return convertView;

    }

    public boolean isInSearchMode() {
        return inSearchMode;
    }

    public void setInSearchMode(boolean inSearchMode) {
        this.inSearchMode = inSearchMode;
    }

    public ContactsSectionIndexer getIndexer() {
        return indexer;
    }

    public void setIndexer(ContactsSectionIndexer indexer) {
        this.indexer = indexer;
    }

    @Override
    public int getCount() {
        if (mItems != null)
            return mItems.size();
        return -1;
    }

    @Override
    public QilexContact getItem(int position) {
        if (mItems != null && !mItems.isEmpty())
            return mItems.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {

        // Invoked in a worker thread to filter the data according to the
        // constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (!TextUtils.isEmpty(constraint)) {
                String filter = constraint.toString().toLowerCase();
                List<QilexContact> filterList = new ArrayList<QilexContact>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    QilexContact c = mStringFilterList.get(i);
                    String dName = c.getDisplayName().toLowerCase().trim();

                    // Check phoneNo
                    PhoneNumberModel[] phoneNormal = c.getPhoneNumberNormal();
                    PhoneNumberModel[] phoneFrifon = c.getPhoneNumberKATZ();
                    String phoneFilter = QilexPhoneNumberUtils.removePhoneCode(filter)[1];
                    boolean containPhone = false;
                    if (phoneNormal != null) {
                        for (PhoneNumberModel phone : phoneNormal) {
                            if (phone.phoneNumber.toLowerCase().contains(phoneFilter)) {
                                containPhone = true;
                                break;
                            }
                        }
                    }
                    if (containPhone == false && phoneFrifon != null) {
                        for (PhoneNumberModel phone : phoneFrifon) {
                            if (phone.phoneNumber.toLowerCase().contains(phoneFilter)) {
                                containPhone = true;
                                break;
                            }
                        }
                    }

                    // Filter Name
                    if (containPhone == true || Utils.isStringFilterAccepted(dName, filter)) {
                        filterList.add(c);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;
        }

        // Invoked in the UI thread to publish the filtering results in the user
        // interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mItems = (List<QilexContact>)results.values;
            notifyDataSetChanged();
        }
    }

    @Override
    public void notifyDataSetChanged() {
        setIndexer(new ContactsSectionIndexer(mItems));
        super.notifyDataSetChanged();
    }

    static class ViewHolder {
        public ImageView mark;

        public ImageView avatar;

        public MyCustomTextView name;

        public ImageView favorite;

    }

}
