
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import pl.frifon.aero2.R;
import pl.katz.aero2.Const;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.ui.activity.ContactKATZOutDetailActivity;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.Utils;

public class ContactDetailPhoneListAdapter extends BaseAdapter {

    public static final int NORMAL_TYPE = 0;

    public static final int DELETE_TYPE = 1;

    WeakReference<ContactKATZOutDetailActivity> mActivityWeak;

    DisplayImageOptions options;

    ArrayList<PhoneItem> listNumbers;

    HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    int viewMode = NORMAL_TYPE;

    private boolean mIsVisibleButton = true;

    private LayoutInflater mInflater;

    public ContactDetailPhoneListAdapter(ContactKATZOutDetailActivity baseActivity,
            ArrayList<PhoneNumberModel> listNumbers) {
        mActivityWeak = new WeakReference<ContactKATZOutDetailActivity>(baseActivity);
        options = baseActivity.circleDisplayImageOptions;
        createListPhoneItem(listNumbers);
        mInflater = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ContactDetailPhoneListAdapter(ContactKATZOutDetailActivity baseActivity,
            ArrayList<PhoneNumberModel> listNumbers, int viewMode) {
        this(baseActivity, listNumbers);
        this.viewMode = viewMode;
    }

    public void setIsVisibleButton(boolean isVisibleButton) {
        this.mIsVisibleButton = isVisibleButton;
    }

    // this should be override by subclass if necessary
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final PhoneItem item = listNumbers.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.contact_number_item, parent, false);
            // default just draw the item only
            holder.number = (MyCustomTextView)convertView.findViewById(R.id.number);
            holder.type = (MyCustomTextView)convertView.findViewById(R.id.type);
            holder.frifonOut = (ImageView)convertView.findViewById(R.id.frifon_out);
            if (Const.IS_FRIFON_LITE) {
                holder.frifonOut.setVisibility(View.INVISIBLE);
            }
            holder.msg = (ImageView)convertView.findViewById(R.id.msg);
            holder.call = (ImageView)convertView.findViewById(R.id.call);
            holder.delete = (ImageView)convertView.findViewById(R.id.delete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        if (viewMode == NORMAL_TYPE) {
            holder.delete.setVisibility(View.INVISIBLE);
            // holder.delete.setOnClickListener(null);
            convertView.setOnClickListener(null);
        } else {
            final ImageView tempDel = holder.delete;
            holder.delete.setVisibility(View.VISIBLE);
            /*
             * holder.delete.setOnClickListener(new OnClickListener() {
             * @Override public void onClick(View v) { if (item.selected) {
             * tempDel.setImageResource(R.drawable.icon_delete_unselected);
             * removeSelection(position); } else {
             * tempDel.setImageResource(R.drawable.icon_delete_selected);
             * setNewSelection(position, true); } item.selected =
             * !item.selected; } });
             */

            convertView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (item.selected) {
                        tempDel.setImageResource(R.drawable.icon_delete_unselected);
                        removeSelection(position);
                    } else {
                        tempDel.setImageResource(R.drawable.icon_delete_selected);
                        setNewSelection(position, true);
                    }
                    item.selected = !item.selected;
                }
            });
        }
        holder.number.setText(QilexPhoneNumberUtils.formatPhoneNoBlock3Number(item.number));
        holder.type.setText(item.type);
        final boolean isMasterMode = Utils.isInMasterMode();
        if (mIsVisibleButton == true) {
            if (QilexPhoneNumberUtils.isSupportNumber(item.number)) {
                holder.msg.setVisibility(View.GONE);
                holder.call.setVisibility(View.GONE);
                holder.frifonOut.setVisibility(View.GONE);
            } else if (isMasterMode == false) {
                holder.msg.setVisibility(View.GONE);
                holder.call.setVisibility(View.GONE);
            } else {
                holder.call.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mActivityWeak.get().startCall(item.number);
                    }
                });
                holder.msg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.sendSms(mActivityWeak.get(), item.number);
                    }
                });
            }

            holder.frifonOut.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivityWeak.get().onClickFrifonOUT(item.number);
                }
            });
        } else {
            holder.frifonOut.setVisibility(View.GONE);
            holder.msg.setVisibility(View.GONE);
            holder.call.setVisibility(View.GONE);
        }
        return convertView;

    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public List<PhoneItem> getItemsCheck() {
        Set<Integer> keys = getCurrentCheckedPosition();
        List<PhoneItem> items = new ArrayList<ContactDetailPhoneListAdapter.PhoneItem>();
        for (Integer i : keys) {
            items.add(listNumbers.get(i));
        }
        return items;
    }

    static class ViewHolder {

        public MyCustomTextView number;

        public MyCustomTextView type;

        public ImageView call;

        public ImageView msg;

        public ImageView frifonOut;

        public ImageView delete;
    }

    @Override
    public int getCount() {
        return listNumbers.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class PhoneItem {
        public String number;

        public String type;

        public boolean selected;

        public String originNumber;

        public boolean isNew = false;

        @Override
        public String toString() {
            return "number = " + number + ", type = " + type + ", selected =" + selected;
        }
    }

    private void createListPhoneItem(ArrayList<PhoneNumberModel> phones) {
        listNumbers = new ArrayList<PhoneItem>();
        for (PhoneNumberModel phone : phones) {
            PhoneItem item = new PhoneItem();
            item.number = phone.phoneNumber;
            item.type = phone.type;
            item.selected = false;
            item.originNumber = phone.phoneOriginal;
            listNumbers.add(item);
        }
    }

}
