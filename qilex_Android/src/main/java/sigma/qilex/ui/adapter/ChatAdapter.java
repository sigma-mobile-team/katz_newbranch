
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONException;

import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.NetworkStateMonitor.NetworkStateListener;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.chat.ChatManager.DownloadImageListener;
import sigma.qilex.manager.chat.LocationChatModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.activity.AccountActivity;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.ContactKATZOutDetailActivity;
import sigma.qilex.ui.activity.ConversationActivity;
import sigma.qilex.ui.activity.ImageBrowseActivity;
import sigma.qilex.ui.activity.LocationActivity;
import sigma.qilex.ui.customview.BoundRelativeLayout;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.EmoticUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import za.co.immedia.pinnedheaderlistview.SectionedBaseAdapter;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 */
public class ChatAdapter extends SectionedBaseAdapter implements NetworkStateListener {

    private static String TXT_YOU;

    private ChatManager mChatManager;

    private ArrayList<ChatMessageModel> mListChatModel = new ArrayList<ChatMessageModel>();

    private ArrayList<ChatMessageItem> mChatMessagesItem = new ArrayList<ChatMessageItem>();

    private ArrayList<SectionChat> mListSections = new ArrayList<SectionChat>();

    private WeakReference<Activity> mActivityWeak;

    public DisplayImageOptions displayImageOptions;

    public ImageLoadingListener animateFirstListener;

    private ContactManager mContactManager;

    private boolean isMsgGroup = false;

    ClipboardManager mClipboard;

    private LayoutInflater mInflator;

    private final long DELTA_TIME_GROUP_MESSAGE = 5 * 60 * 1000; // 5 minutes

    OnTouchListener mOnTouchListener;

    public int sectionHeight = 0;

    public boolean isAnimateLastItem = false;

    Animation appearAnimation;

    Set<Integer> setAnimated = new HashSet<Integer>();

    private int widthRow = SharedPrefrerenceFactory.getInstance().getMaxWidthChatAdapter();
    
    public ChatAdapter(ConversationActivity context, ChatManager mChatManager, boolean isGroup) {
        mActivityWeak = new WeakReference<Activity>(context);
        
        this.mChatManager = mChatManager;
        mOnTouchListener = (OnTouchListener)context;
        this.isMsgGroup = isGroup;
        this.mContactManager = ContactManager.getInstance();
        TXT_YOU = mActivityWeak.get().getResources().getString(R.string.you);
        displayImageOptions = ((BaseActivity)mActivityWeak.get()).circleDisplayImageOptions;
        mClipboard = (ClipboardManager)mActivityWeak.get().getSystemService(Context.CLIPBOARD_SERVICE);
        mInflator = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        appearAnimation = AnimationUtils.loadAnimation(context, R.anim.appear_animation);
    }

    public ArrayList<ChatMessageModel> getListChatMsg() {
        return mListChatModel;
    }

    public void setListChatModel(ArrayList<ChatMessageModel> listChats) {
        if (listChats != null) {
            mListChatModel.clear();
            mListChatModel.addAll(listChats);
        }
        reGenerateFromModel();
    }

	public void updateItemStatus(long messageId, int status) {
        synchronized (mChatMessagesItem) {
            for (ChatMessageItem item : mChatMessagesItem) {
                if (item.id == messageId) {
                    item.status = status;
                    break;
                }
            }
            for (ChatMessageModel item : mListChatModel) {
                if (item.id == messageId) {
                    item.status = status;
                    break;
                }
            }
        }
    }

    public void updateItem(long messageId, ChatMessageModel newChatModel) {
        synchronized (mChatMessagesItem) {
            for (ChatMessageItem item : mChatMessagesItem) {
                if (item.id == messageId) {
                    item.generateFromModel(newChatModel);
                    break;
                }
            }
            int size = mListChatModel.size();
            for (int i = 0; i < size; i++) {
                ChatMessageModel item = mListChatModel.get(i);
                if (item.id == newChatModel.id) {
                    mListChatModel.remove(i);
                    mListChatModel.add(i, newChatModel);
                    break;
                }
            }
        }
    }

    public void updateItemStatusToViewed(long messageId, ChatMessageModel newChatModel) {
        synchronized (mChatMessagesItem) {
            for (ChatMessageItem item : mChatMessagesItem) {
                if (item.id == messageId) {
                    item.status = ChatDatabase.MSG_STATUS_SEND_VIEWED;
                } else if (
                		item.status == ChatDatabase.MSG_STATUS_SEND_DELIVERED
                        && item.dateSortMillis < newChatModel.dateReceiveMillis
                        && item.status != ChatDatabase.MSG_STATUS_SEND_FAIL) {
                    item.status = ChatDatabase.MSG_STATUS_SEND_VIEWED;
                }
            }
        }
    }

    public void updateAllItemStatusToViewed() {
        synchronized (mChatMessagesItem) {
            for (ChatMessageItem item : mChatMessagesItem) {
                if (item.type == ChatDatabase.MSG_TYPE_SEND
                        && (item.status == ChatDatabase.MSG_STATUS_SEND_DELIVERED)) {
                    item.status = ChatDatabase.MSG_STATUS_SEND_VIEWED;
                }
            }
        }
    }

    public void reGenerateFromModel() {
        // Generate to List Chat Item.
        mChatMessagesItem.clear();
        mListSections = new ArrayList<SectionChat>();
        int size = mListChatModel.size();
        Calendar pinnerDay = Calendar.getInstance();
        pinnerDay.setTimeInMillis(0);
        ArrayList<ChatMessageItem> messages = new ArrayList<ChatMessageItem>();
        for (int i = 0; i < size; i++) {
            ChatMessageItem item = new ChatMessageItem(mListChatModel.get(mListChatModel.size() - 1 - i));
            mChatMessagesItem.add(item);
            long timeMillis = mListChatModel.get(mListChatModel.size() - 1 - i).dateReceiveMillis;
            if (calculateSections(timeMillis, pinnerDay.getTimeInMillis()) && i < size - 1) {
                messages.add(item);
            } else if (i == size - 1) {
                messages.add(item);
                pinnerDay.setTimeInMillis(timeMillis);
                SectionChat section = new SectionChat();
                section.setMessages(messages);
                section.setHeader(Utils.convertSimpleDayFormat(pinnerDay.getTimeInMillis()));
                mListSections.add(section);
            } else {
                if (i > 0) {
                    SectionChat section = new SectionChat();
                    section.setHeader(Utils.convertSimpleDayFormat(pinnerDay.getTimeInMillis()));
                    section.setMessages(messages);
                    mListSections.add(section);
                }
                pinnerDay.setTimeInMillis(timeMillis);
                Utils.clearTimes(pinnerDay);
                messages = new ArrayList<ChatMessageItem>();
                messages.add(item);
            }
        }
        notifyDataSetChanged();
    }

    private boolean calculateSections(long msgTime, long dayStartTime) {
        return (msgTime >= dayStartTime && msgTime < dayStartTime + 24 * 3600 * 1000);
    }

    @Override
    public View getItemView(int section, final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final ChatMessageItem chatMessage = getItem(section, position);
        if (convertView != null && convertView instanceof RelativeLayout) {
            holder = (ViewHolder)convertView.getTag();
        } else {
            int layoutId = -1;
            if (chatMessage.type == ChatDatabase.MSG_TYPE_SEND) {
                layoutId = R.layout.out_item_chat_media;
            } else if (chatMessage.type == ChatDatabase.MSG_TYPE_RECEIVE) {
                layoutId = R.layout.in_item_chat_media;
            } else if (chatMessage.type == ChatDatabase.MSG_TYPE_GROUP_STATUS) {
            	layoutId = R.layout.status_item_chat_message;
            } else if (chatMessage.type == ChatDatabase.MSG_TYPE_CALLLOG) {
                layoutId = R.layout.calllog_item_chat_message;
            } else {
            	layoutId = R.layout.out_item_chat_media;
            }
            convertView = mInflator.inflate(layoutId, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
        }
        if (chatMessage.subType != ChatDatabase.MSG_MINETYPE_TEXT
                && chatMessage.subType != ChatDatabase.MSG_MINETYPE_UNDENFINDED
                && chatMessage.type != ChatDatabase.MSG_TYPE_CALLLOG) {
            holder.layoutMedia.setVisibility(View.VISIBLE);
            // Set size Image
            LayoutParams param = holder.layoutMedia.getLayoutParams();
            if (chatMessage.subType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                param.height = (int)Utils.pxFromDp(120);
            } else {
                param.height = (int)Utils.pxFromDp(180);
            }
            holder.layoutMedia.setLayoutParams(param);
        } else {
            holder.layoutMedia.setVisibility(View.GONE);
        }

        // TextView width
        LayoutParams paramText = holder.tvMessage.getLayoutParams();
        if (chatMessage.subType == ChatDatabase.MSG_MINETYPE_LOCATION
                || chatMessage.subType == ChatDatabase.MSG_MINETYPE_IMAGE) {
            paramText.width = (int)Utils.pxFromDp(180);
        } else {
            paramText.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        holder.tvMessage.setLayoutParams(paramText);

        if (chatMessage.type == ChatDatabase.MSG_TYPE_SEND) {
            holder.tvStatus.setVisibility(View.VISIBLE);
            if (isMsgGroup) {
                holder.tvUser.setVisibility(View.VISIBLE);
                holder.tvUser.setText(chatMessage.userName);
            } else {
                holder.tvUser.setVisibility(View.GONE);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)holder.imAvatar.getLayoutParams();
                params.topMargin = 0;
                holder.imAvatar.setLayoutParams(params);
            }
            if (needToGroup(section, position)) {
                holder.contentContainer.setBackgroundResource(R.drawable.out_message_non_arrow_);
                holder.imAvatar.setVisibility(View.INVISIBLE);
            } else {
                holder.contentContainer.setBackgroundResource(R.drawable.out_message_);
                holder.imAvatar.setVisibility(View.VISIBLE);
                MyApplication.getImageLoader().displayImage(UserInfo.getInstance().getAvatarUrl(), holder.imAvatar,
                        displayImageOptions, animateFirstListener);
            }
            holder.tvTime.setText(chatMessage.time);
            String text = chatMessage.textMessage;
            Spannable span = EmoticUtils.getSmiledText(mActivityWeak.get(), text);
            holder.tvMessage.setText(span);
//            if (Utils.isStringNullOrEmpty(chatMessage.groupUuid)) {
                holder.tvStatus.setVisibility(View.VISIBLE);
            	if (chatMessage.status == ChatDatabase.MSG_STATUS_SEND_PREPARING) {
                    holder.imgFlag.setVisibility(View.INVISIBLE);
                    holder.tvStatus.setText(R.string.sending);
                    convertView.setAlpha(0.4f);
                    if (holder.progress != null)
                        holder.progress.setVisibility(View.VISIBLE);
                } else if (chatMessage.status == ChatDatabase.MSG_STATUS_SEND_SENDING) {
                    holder.imgFlag.setVisibility(View.INVISIBLE);
                    holder.tvStatus.setText(R.string.sending);
                    convertView.setAlpha(0.4f);
                    if (holder.progress != null)
                        holder.progress.setVisibility(View.GONE);
                } else if (chatMessage.status == ChatDatabase.MSG_STATUS_SEND_FAIL) {
                    holder.imgFlag.setVisibility(View.VISIBLE);
                    holder.tvStatus.setText(R.string.failed);
                    convertView.setAlpha(1f);
                    if (holder.progress != null)
                        holder.progress.setVisibility(View.GONE);
                } else if (chatMessage.status == ChatDatabase.MSG_STATUS_SEND_DELIVERED) {
                    holder.imgFlag.setVisibility(View.INVISIBLE);
                    holder.tvStatus.setText(R.string.delivered);
                    convertView.setAlpha(1f);
                    if (holder.progress != null)
                        holder.progress.setVisibility(View.GONE);
                } else if (chatMessage.status == ChatDatabase.MSG_STATUS_SEND_VIEWED) {
                    holder.imgFlag.setVisibility(View.INVISIBLE);
                    holder.tvStatus.setText(R.string.viewed);
                    convertView.setAlpha(1f);
                    if (holder.progress != null)
                        holder.progress.setVisibility(View.GONE);
                } else {
                    holder.imgFlag.setVisibility(View.INVISIBLE);
                    holder.tvStatus.setText(R.string.sent);
                    convertView.setAlpha(1f);
                    if (holder.progress != null)
                        holder.progress.setVisibility(View.GONE);
                }
//            } else {
//            	if (chatMessage.status == ChatDatabase.MSG_STATUS_SEND_FAIL) {
//                    holder.imgFlag.setVisibility(View.VISIBLE);
//                    holder.tvStatus.setVisibility(View.VISIBLE);
//                    holder.tvStatus.setText(R.string.failed);
//                    convertView.setAlpha(1f);
//                    if (holder.progress != null)
//                        holder.progress.setVisibility(View.GONE);
//                } else {
//                    holder.imgFlag.setVisibility(View.GONE);
//                    holder.tvStatus.setVisibility(View.GONE);
//                }
//            }
            

            // Display image
            if (chatMessage.subType == ChatDatabase.MSG_MINETYPE_IMAGE
                    || chatMessage.subType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                if (chatMessage.thumbnail == null) {
                    holder.imImageContent.setImageResource(R.drawable.icon_location);
                } else {
                    holder.imImageContent.setImageBitmap(chatMessage.thumbnail);
                }
            }
            if (Utils.isStringNullOrEmpty(chatMessage.textMessage)) {
                holder.tvMessage.setVisibility(View.GONE);
            } else {
                holder.tvMessage.setVisibility(View.VISIBLE);
            }
        } else if (chatMessage.type == ChatDatabase.MSG_TYPE_RECEIVE) {
            holder.tvStatus.setVisibility(View.GONE);
            if (isMsgGroup) {
                holder.tvUser.setVisibility(View.VISIBLE);
                holder.tvUser.setText(chatMessage.userName);
            } else {
                holder.tvUser.setVisibility(View.GONE);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)holder.imAvatar.getLayoutParams();
                params.topMargin = 0;
                holder.imAvatar.setLayoutParams(params);
            }
            if (needToGroup(section, position)) {
                holder.contentContainer.setBackgroundResource(R.drawable.in_message_non_arrow_);
                holder.imAvatar.setVisibility(View.INVISIBLE);
                holder.tvUser.setVisibility(View.GONE);
            } else {
                holder.contentContainer.setBackgroundResource(R.drawable.in_message_);
                holder.imAvatar.setVisibility(View.VISIBLE);
                holder.tvUser.setVisibility(View.VISIBLE);
                holder.tvUser.setText(chatMessage.userName);
            }

            if (chatMessage.status == ChatDatabase.MSG_STATUS_RECEIVE_PREPARING) {
                convertView.setAlpha(0.4f);
                if (holder.progress != null)
                    holder.progress.setVisibility(View.VISIBLE);
            } else {
                convertView.setAlpha(1f);
                if (holder.progress != null)
                    holder.progress.setVisibility(View.GONE);
            }
            holder.tvTime.setText(chatMessage.time);
            String text = chatMessage.textMessage;
            Spannable span = EmoticUtils.getSmiledText(mActivityWeak.get(), text);
            holder.tvMessage.setText(span);
            if (QilexPhoneNumberUtils.isSupportNumber(chatMessage.phoneNumber)) {
                holder.imAvatar.setImageResource(R.drawable.ic_round);
            } else {
                MyApplication.getImageLoader().displayImage(chatMessage.avatarUrl, holder.imAvatar, displayImageOptions,
                        animateFirstListener);
            }

            // Send Viewed
            if (chatMessage.status == ChatDatabase.MSG_STATUS_RECEIVE_UNREAD && !Const.IS_OLD_VERSION) {
                boolean sent = mChatManager.sendViewedMessage(chatMessage.contactStr, chatMessage.uuid);
                if (sent == true) {
                    // Update UI or any thing
                    chatMessage.status = ChatDatabase.MSG_STATUS_RECEIVE_READ;
                }
            }

            // Display image
            if (chatMessage.subType == ChatDatabase.MSG_MINETYPE_IMAGE
                    || chatMessage.subType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                if (chatMessage.thumbnail == null) {
                    holder.imImageContent.setImageResource(R.drawable.gallery_loading);
                    // Download image
                    if (chatMessage.status == ChatDatabase.MSG_STATUS_RECEIVE_PREPARING) {
                        // Download bitmap
                        mChatManager.reDownloadThumbnail(chatMessage.itemModel, new DownloadImageListener() {
                            @Override
                            public void onDownloadFinish(String filePath, String token, boolean result) {
                            }
                        });
                    }
                } else {
                    holder.imImageContent.setImageBitmap(chatMessage.thumbnail);
                }
            }
            if (Utils.isStringNullOrEmpty(chatMessage.textMessage)) {
                holder.tvMessage.setVisibility(View.GONE);
            } else {
                holder.tvMessage.setVisibility(View.VISIBLE);
            }
        } else if (chatMessage.type == ChatDatabase.MSG_TYPE_GROUP_STATUS) {
        	holder.tvMessage.setText(chatMessage.textMessage);
        } else {
            holder.tvTime.setText(chatMessage.time);
            holder.tvMessage.setText(chatMessage.textMessage);
        }
        holder.imgFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long msgId = chatMessage.id;
                int pos = mChatMessagesItem.indexOf(chatMessage);
                Intent intent = new Intent(ConversationActivity.ACTION_RESEND_MESSAGE);
                intent.putExtra("id", msgId);
                intent.putExtra("pos", pos);
                intent.putExtra("sip_address", chatMessage.contactStr);
                intent.putExtra("message_text", chatMessage.textMessage);
                // onDeleteMsg(msgId, pos);
                mActivityWeak.get().sendBroadcast(intent);
            }
        });
        if (QilexPhoneNumberUtils.isSupportNumber(chatMessage.phoneNumber) == false) {
            holder.imAvatar.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (chatMessage.type == ChatDatabase.MSG_TYPE_SEND) {
                            Intent it = new Intent(mActivityWeak.get(), AccountActivity.class);
                            it.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE,
                                    ActivityTransitionFactory.IN_LEFT_OUT_LEFT);
                            mActivityWeak.get().startActivityForResult(it, ConversationActivity.REQUEST_VIEW);
                        } else {
                            String noPhone = chatMessage.phoneNumber;
                            gotoDetailContact(noPhone);
                        }
                    }
                    return true;
                }
            });
        }
        holder.contentContainer.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chatMessage.type == ChatDatabase.MSG_TYPE_CALLLOG) {
                    String noPhone = chatMessage.phoneNumber;
                    gotoDetailContact(noPhone);
                } else if (chatMessage.subType == ChatDatabase.MSG_MINETYPE_IMAGE) {
                    gotoImageList(chatMessage.threadId, chatMessage.id);
                } else if (chatMessage.subType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                    gotoLocation(chatMessage.id);
                }
            }
        });
        holder.contentContainer.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                Utils.showContextMenu((BaseActivity)mActivityWeak.get(), chatMessage.textMessage, chatMessage.id,
                        mChatMessagesItem.indexOf(chatMessage), onMessageDelete,
                        chatMessage.subType == ChatDatabase.MSG_MINETYPE_TEXT);
                return false;
            }
        });
        convertView.setOnTouchListener(mOnTouchListener);

        if (isAnimateLastItem && !setAnimated.contains(position) && position == mChatMessagesItem.size() - 1) {
            // animate view
            convertView.startAnimation(appearAnimation);
            setAnimated.add(position);
            isAnimateLastItem = false;
        }

        if (widthRow == 0) {
            final View contentView = convertView;
            final BoundRelativeLayout rlRoot = holder.rlContent;
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

                @SuppressWarnings("deprecation")
                @Override
                public void onGlobalLayout() {
                    contentView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    if (widthRow > 0)
                        return;
                    widthRow = contentView.getMeasuredWidth();
                    if (chatMessage.type == ChatDatabase.MSG_TYPE_SEND) {
                        rlRoot.setMaxWidth((int)(widthRow - Utils.pxFromDp(98)));
                        notifyDataSetChanged();
                    }
                }
            });
        }

        if (widthRow > 0 && chatMessage.type == ChatDatabase.MSG_TYPE_SEND) {
            holder.rlContent.setMaxWidth((int)(widthRow - Utils.pxFromDp(98)));
        }

        return convertView;
    }

    private void gotoDetailContact(String phoneNumber) {
        QilexContact contact = mContactManager.getContactByPhoneNo(mActivityWeak.get(), phoneNumber);
        if (contact != null) {
            // If contact is exist, show detail
            Intent intent = new Intent(mActivityWeak.get(), ContactKATZOutDetailActivity.class);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contact.id);
            intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY, contact.lookUpKey);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            mActivityWeak.get().startActivityForResult(intent, ConversationActivity.REQUEST_VIEW);
        } else if (!TextUtils.isEmpty(phoneNumber)) {
            // If contact is not exist, show add new
            Intent intent = new Intent(mActivityWeak.get(), ContactKATZOutDetailActivity.class);
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_PHONE_NUMBER, phoneNumber);
            long[] ids = new long[0];
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_LIST_CALLLOG, ids);
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_VIEW_TYPE,
                    ContactKATZOutDetailActivity.VIEW_TYPE_CALLLOG);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            mActivityWeak.get().startActivity(intent);
        }
    }

    private void gotoImageList(long threadId, long msgId) {
        ArrayList<ChatMessageModel> listThread = mChatManager.getChatMessageImageFromThread(threadId);
        int size = listThread.size();
        String[] listUrls = new String[size];
        String[] listComments = new String[size];
        int currentIndex = 0;

        ChatMessageModel[] listSelectedChat = new ChatMessageModel[size];
        for (int i = 0; i < size; i++) {
            ChatMessageModel message = listThread.get(i);
            listSelectedChat[i] = message;
            listUrls[i] = message.imageUrl;
            listComments[i] = message.textContent;
            if (msgId == message.id) {
                currentIndex = i;
            }
        }
        Intent intent = new Intent(mActivityWeak.get(), ImageBrowseActivity.class);
        intent.putExtra(ImageBrowseActivity.INTENT_LIST_URLS, listUrls);
        intent.putExtra(ImageBrowseActivity.INTENT_LIST_COMMENT, listComments);
        intent.putExtra(ImageBrowseActivity.INTENT_CURRENT_INDEX, currentIndex);
        ImageBrowseActivity.listSelectedChat = listSelectedChat;
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        mActivityWeak.get().startActivity(intent);
    }

    private void gotoLocation(long msgId) {
        ChatMessageModel cm = mChatManager.getChatMessageById(msgId);
        try {
            LocationChatModel lcm = new LocationChatModel(cm.textContent);
            Intent intent = new Intent(mActivityWeak.get(), LocationActivity.class);
            intent.putExtra(LocationActivity.BUNDLE_LOCALTION_INPUT, lcm.getLatLng());
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            mActivityWeak.get().startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean needToGroup(int section, int pos) {
        try {
            SectionChat sec = mListSections.get(section);
            ArrayList<ChatMessageItem> message = sec.getMessages();
            if (pos <= 0) {
                return false;
            } else {
                ChatMessageItem currentPos = message.get(pos);
                ChatMessageItem previous = message.get(pos - 1);
                if (currentPos.userName.equals(previous.userName)
                        && currentPos.dateSortMillis - previous.dateSortMillis <= DELTA_TIME_GROUP_MESSAGE
                        && currentPos.type == previous.type) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public synchronized void add(ChatMessageModel message) {
        long previousMsgDay = 0;
        if (mListChatModel.size() > 0) {
            ChatMessageModel oldItem = mListChatModel.get(0);
            previousMsgDay = oldItem.dateReceiveMillis;
        }

        Calendar pinnerDay = Calendar.getInstance();
        pinnerDay.setTimeInMillis(previousMsgDay);
        Utils.clearTimes(pinnerDay);
        mListChatModel.add(0, message);
        ChatMessageItem item = new ChatMessageItem(message);
        mChatMessagesItem.add(item);
        if (calculateSections(message.dateReceiveMillis, pinnerDay.getTimeInMillis())) {
            SectionChat sec = mListSections.get(mListSections.size() - 1);
            sec.getMessages().add(item);
        } else {
            SectionChat sec = new SectionChat();
            ArrayList<ChatMessageItem> messages = new ArrayList<ChatMessageItem>();
            sec.setHeader(Utils.convertSimpleDayFormat(message.dateReceiveMillis));
            messages.add(item);
            sec.setMessages(messages);
            if (mListSections == null) {
                mListSections = new ArrayList<SectionChat>();
            }
            mListSections.add(sec);
            // update old headers
            for (SectionChat section : mListSections) {
                if (section.getMessages() == null || section.getMessages().size() == 0) {
                    // TODO: for later delete message feature need update
                } else {
                    section.setHeader(Utils.convertSimpleDayFormat(section.getMessages().get(0).dateSortMillis));
                }
            }
        }
    }

    public void add(List<ChatMessageModel> messages) {
        if (mListChatModel == null) {
            mListChatModel = new ArrayList<ChatMessageModel>();
        }
        mListChatModel.addAll(messages);
        // Generate to List Chat Item.
        int size = messages.size();
        for (int i = 0; i < size; i++) {
            ChatMessageItem item = new ChatMessageItem(messages.get(i));
            mChatMessagesItem.add(item);
        }
    }

    public void removeMessage(List<Long> ids) {
        synchronized (mListChatModel) {
            int size = mListChatModel.size();
            for (int i = size - 1; i >= 0; i--) {
                long id = mListChatModel.get(i).id;
                if (ids.contains(id)) {
                    mListChatModel.remove(i);
                }
            }
            reGenerateFromModel();
        }
    }

    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.contentContainer = (LinearLayout)v.findViewById(R.id.contentWithBackground);
        holder.tvUser = (MyCustomTextView)v.findViewById(R.id.tv_user);
        holder.tvTime = (MyCustomTextView)v.findViewById(R.id.tv_time);
        holder.tvMessage = (MyCustomTextView)v.findViewById(R.id.txtMessage);
        holder.imAvatar = (ImageView)v.findViewById(R.id.im_avatar);
        holder.imgFlag = (ImageView)v.findViewById(R.id.img_flag);
        holder.tvStatus = (MyCustomTextView)v.findViewById(R.id.tv_status);
        holder.rlContent = (BoundRelativeLayout)v.findViewById(R.id.content);
        holder.imImageContent = (ImageView)v.findViewById(R.id.image);
        holder.progress = (ProgressBar)v.findViewById(R.id.progress);
        holder.layoutMedia = (RelativeLayout)v.findViewById(R.id.layout_media);
        return holder;
    }

    /**
     * copy content message to clipboard
     * 
     * @param text
     */
    public void copyToClipboard(String text) {
        ClipData clip = ClipData.newPlainText("content_message", text);
        mClipboard.setPrimaryClip(clip);
    }

    private static class ViewHolder {

        public ImageView imgFlag;

        public LinearLayout contentContainer;

        public ImageView imAvatar;

        public MyCustomTextView tvUser;

        public MyCustomTextView tvMessage;

        public MyCustomTextView tvTime;

        public MyCustomTextView tvStatus;

        public BoundRelativeLayout rlContent;

        public ImageView imImageContent;

        public ProgressBar progress;

        public RelativeLayout layoutMedia;
    }

    /**
     * Class represent data of Chat.
     */
    private class ChatMessageItem {

        public long id;

        public String avatarUrl;

        public String userName;

        public String textMessage;

        public String time;

        public int type;

        public int subType;

        public String phoneNumber;

        public String contactStr;

        public int status;

        public long dateSortMillis;

        public Bitmap thumbnail;

        public long threadId;

        public String uuid;
        
        public String groupUuid;

        public ChatMessageModel itemModel;

        public ChatMessageItem(ChatMessageModel model) {
            generateFromModel(model);
        }

        public void generateFromModel(ChatMessageModel model) {
            itemModel = model;
            textMessage = model.getDescription();
            dateSortMillis = model.dateReceiveMillis;
            contactStr = model.contactStr;
            id = model.id;
            uuid = model.uuid;
            time = Utils.parseHourTime(model.dateMillis);
            threadId = model.threadId;
            // Parse type
            type = model.type;
            subType = model.mineType;
            status = model.status;
            groupUuid = model.groupUuid;
            
            if (!Utils.isStringNullOrEmpty(contactStr)) {
                if (subType == ChatDatabase.MSG_MINETYPE_IMAGE) {
                    thumbnail = BitmapFactory.decodeByteArray(model.imageThumbnail, 0, model.imageThumbnail.length);
                } else if (subType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                    thumbnail = BitmapFactory.decodeByteArray(model.imageThumbnail, 0, model.imageThumbnail.length);
                }
                if (type == ChatDatabase.MSG_TYPE_SEND) {
                    avatarUrl = UserInfo.getInstance().getAvatarUrl();
                    userName = TXT_YOU;
                    phoneNumber = UserInfo.getInstance().getPhoneNumberFormatted();
                } else {
                    phoneNumber = model.contactStr;
                    QilexContact contact = mContactManager.getContactByPhoneNo(mActivityWeak.get(), phoneNumber);
                    if (contact != null) {
                        this.avatarUrl = contact.getPhotoUri();
                        this.userName = contact.getDisplayName();
                    } else {
                        TempContactModel tempContact = mContactManager.getTempContact(phoneNumber);
                        if (tempContact != null) {
                            this.avatarUrl = tempContact.avatarUrl;
                            this.userName = tempContact.name;
                        } else {
                            this.avatarUrl = Const.STR_EMPTY;
                            this.userName = QilexPhoneNumberUtils.formatPhoneNoBlock3Number(phoneNumber);
                        }
                    }
                }
            }
        }
    }

    @Override
    public ChatMessageItem getItem(int section, int position) {
        return mListSections.get(section).getMessages().get(position);
    }

    @Override
    public long getItemId(int section, int position) {
        return position;
    }

    @Override
    public int getSectionCount() {
        return mListSections.size();
    }

    @Override
    public int getCountForSection(int section) {
        return mListSections.get(section).getMessages().size();
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        LinearLayout layout = null;
        if (convertView != null && convertView instanceof LinearLayout) {
            layout = (LinearLayout)convertView;

        } else {
            layout = (LinearLayout)mInflator.inflate(R.layout.header_item, null);
        }
        ((TextView)layout.findViewById(R.id.textItem)).setText(mListSections.get(section).getHeader());
        sectionHeight = layout.getMeasuredHeight();
        return layout;
    }

    @Override
    public int getItemViewTypeCount() {
        return 4;
    }

    // calllog(miss or ) or chat (me or not me)
    @Override
    public int getItemViewType(int section, int position) {
        ChatMessageItem chatMessage = getItem(section, position);
        return chatMessage.type;
    }

    class SectionChat {
        ArrayList<ChatMessageItem> messages = new ArrayList<ChatMessageItem>();

        String header;

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }

        public ArrayList<ChatMessageItem> getMessages() {
            return messages;
        }

        public void setMessages(ArrayList<ChatMessageItem> messages) {
            this.messages = messages;
        }
    }

    OnMessageChoose onMessageDelete = new OnMessageChoose() {

        @Override
        public void onDelete(long id, int position) {
            onDeleteMsg(id, position);
        }

        @Override
        public void onCopy(String message) {
            if (!TextUtils.isEmpty(message)) {
                copyToClipboard(message);
            }
        }
    };

    public void onDeleteMsg(long id, int position) {
        int deletePosition = mListChatModel.size() - 1 - position;
        if (deletePosition < 0 || deletePosition > mListChatModel.size() - 1)
            return;
        mListChatModel.remove(deletePosition);
        mChatManager.deleteChatMessage(id);
        reGenerateFromModel();
        notifyDataSetChanged();
    }

    public interface OnMessageChoose {
        public void onDelete(long id, int position);

        public void onCopy(String message);
    }

    @Override
    public void onNetworkStateChanged(boolean init, int networkType) {
        // Re
        switch (networkType) {
            case NetworkStateMonitor.WIFI:
            case NetworkStateMonitor.MOBILE_SUFFICIENT:
                ArrayList<ChatMessageModel> listChatMsg = new ArrayList<>(mListChatModel);
                for (ChatMessageModel chatMessageModel : listChatMsg) {
                    if (chatMessageModel.mineType == ChatDatabase.MSG_MINETYPE_IMAGE
                            || chatMessageModel.mineType == ChatDatabase.MSG_MINETYPE_LOCATION) {
                        if (chatMessageModel.imageThumbnail == null || chatMessageModel.imageThumbnail.length == 0) {
                            // Download image
                            if (chatMessageModel.status == ChatDatabase.MSG_STATUS_RECEIVE_PREPARING) {
                                // Download bitmap
                                mChatManager.reDownloadThumbnail(chatMessageModel, new DownloadImageListener() {
                                    @Override
                                    public void onDownloadFinish(String filePath, String token, boolean result) {
                                        mActivityWeak.get().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                notifyDataSetChanged();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
                break;
        }
    }
}
