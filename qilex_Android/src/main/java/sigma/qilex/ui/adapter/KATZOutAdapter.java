/*
 * Copyright 2014 trinea.cn All right reserved. This software is the confidential and proprietary information of
 * trinea.cn ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with trinea.cn.
 */

package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import pl.katz.aero2.Const.Config;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.http.PurchasePrepaidModel;
import sigma.qilex.dataaccess.model.http.PurchasePrepaidModel.PurchaseModel;
import sigma.qilex.ui.customview.MyCustomTextView;

public class KATZOutAdapter extends BaseAdapter implements OnClickListener {

    private WeakReference<Context> mContextWeak;

    private ArrayList<PurchasePrepaidModel.PurchaseModel> mArrKATZOut = new ArrayList<PurchasePrepaidModel.PurchaseModel>();

    private ArrayList<String> listIds = new ArrayList<>();

    private LayoutInflater mInflater;

    private boolean isLoading = false;

    private boolean isLoadEnd = false;

    private Runnable onClickRunnable;

    public KATZOutAdapter(Context context, ArrayList<PurchasePrepaidModel.PurchaseModel> arrKATZOut,
            Runnable onClickRunnable) {
        mContextWeak = new WeakReference<Context>(context);
        mInflater = (LayoutInflater)mContextWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.onClickRunnable = onClickRunnable;
        this.mArrKATZOut = arrKATZOut;
        generateListId();
    }

    public void setListSource(ArrayList<PurchasePrepaidModel.PurchaseModel> arrKATZOut) {
        isLoading = false;
        mArrKATZOut.clear();
        mArrKATZOut.addAll(arrKATZOut);
        generateListId();
    }

    public void addSourceToList(ArrayList<PurchaseModel> arrKATZOut) {
        isLoading = false;
        for (PurchaseModel purchaseModel : arrKATZOut) {
            if (listIds.contains(purchaseModel.operation_id) == false) {
                mArrKATZOut.add(purchaseModel);
                listIds.add(purchaseModel.operation_id);
            }
        }
    }

    public ArrayList<PurchaseModel> getmArrKATZOut() {
        return mArrKATZOut;
    }

    private void generateListId() {
        int size = mArrKATZOut.size();
        for (int i = 0; i < size; i++) {
            listIds.add(mArrKATZOut.get(i).operation_id);
        }
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void setLoadEnd(boolean isLoadEnd) {
        this.isLoadEnd = isLoadEnd;
    }

    @Override
    public int getCount() {
        if (mArrKATZOut.isEmpty() == true) {
            return 0;
        } else if (isLoadEnd == false) {
            return mArrKATZOut.size() + 1;
        } else {
            return mArrKATZOut.size();
        }
    }

    @Override
    public View getView(final int position, View view, ViewGroup container) {
        if (position >= mArrKATZOut.size()) {
            // mOnLoadMoreListener.onLoadMore();
            View v = mInflater.inflate(R.layout.item_load_more, null);
            if (isLoading == true) {
                v.findViewById(R.id.progress).setVisibility(View.VISIBLE);
            }
            v.setOnClickListener(this);
            return v;
        }

        ViewHolder holder;
        if (view == null || view.getTag() == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.list_frifonout_item, container, false);
            holder.time = (MyCustomTextView)view.findViewById(R.id.time);
            holder.id = (MyCustomTextView)view.findViewById(R.id.frifon_out_id);
            holder.price = (MyCustomTextView)view.findViewById(R.id.frifon_out_price);
            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();
        }
        PurchasePrepaidModel.PurchaseModel katz = getItem(position);

        String timeSubString = katz.date.substring(0, katz.date.length() - 7);
        holder.time.setText(timeSubString);
        holder.id.setText("ID: " + katz.operation_id);
        holder.price.setText(String.format(Config.MONEY_FORMAT, katz.amount));
        return view;
    }

    private static class ViewHolder {
        MyCustomTextView time;

        MyCustomTextView id;

        MyCustomTextView price;
    }

    @Override
    public PurchasePrepaidModel.PurchaseModel getItem(int position) {
        int listSize = mArrKATZOut.size();
        if (listSize > 0) {
            if (position >= listSize) {
                return null;
            } else {
                return mArrKATZOut.get(position);
            }
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {
        if (isLoading == false) {
            isLoading = true;
            v.findViewById(R.id.progress).setVisibility(View.VISIBLE);
            onClickRunnable.run();
        }
    }
}
