
package sigma.qilex.ui.adapter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import sigma.qilex.ui.activity.BaseActivity;

public class ListImageGalleryAdapter extends BaseAdapter {

    public static final int MAXIMUM_SELECTED = 10;

    WeakReference<BaseActivity> mActivityWeak;

    ArrayList<ImageItem> listItem = new ArrayList<>();

    LayoutInflater mInflater;

    DisplayImageOptions displayImageOptions;

    ImageLoader loader = MyApplication.getImageLoader();

    private ArrayList<ImageItem> selectedItems;

    public ListImageGalleryAdapter(BaseActivity baseActivity) {
        mActivityWeak = new WeakReference<BaseActivity>(baseActivity);
        mInflater = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedItems = new ArrayList<>();
    }

    public void setListFile(ArrayList<String> listFileUrl) {
        selectedItems.clear();
        listItem.clear();
        int size = listFileUrl.size();
        for (int i = 0; i < size; i++) {
            String url = listFileUrl.get(i);
            ImageItem item = new ImageItem();
            item.path = url;
            item.isSelected = false;
            listItem.add(item);
        }
    }

    public boolean clickCheckItem(int position) {
        ImageItem clickeditem = listItem.get(position);
        boolean itemSelection = !clickeditem.isSelected;

        // // Clear other selected, simgle select only
        // selectedItems.clear();
        // int size = listItem.size();
        // for (int i = 0; i < size; i++) {
        // ImageItem item = listItem.get(i);
        // item.isSelected = false;
        // }
        if (itemSelection) {
            if (selectedItems.size() < MAXIMUM_SELECTED) {
                clickeditem.isSelected = itemSelection;
                selectedItems.add(clickeditem);
                return true;
            } else {
                return false;
            }
        } else {
            clickeditem.isSelected = itemSelection;
            selectedItems.remove(clickeditem);
            return true;
        }
    }

    public ArrayList<ImageItem> getSelectedItems() {
        return selectedItems;
    }

    @Override
    public int getCount() {
        return listItem.size();
    }

    @Override
    public ImageItem getItem(int position) {
        return listItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        ImageItem item = listItem.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_image_item, parent, false);
            holder.image = (ImageView)convertView.findViewById(R.id.image);
            holder.imgChecked = (ImageView)convertView.findViewById(R.id.imgChecked);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        Glide.with(mActivityWeak.get()).load(new File(item.path)).centerCrop().thumbnail(0.1f)
                .placeholder(R.drawable.gallery_loading).error(R.drawable.gallery_loading).into(holder.image);

        if (item.isSelected) {
            holder.imgChecked.setImageResource(R.drawable.icon_gallery_checked);
        } else {
            holder.imgChecked.setImageResource(R.drawable.icon_gallery_unchecked);
        }
        return convertView;
    }

    public class ImageItem {
        public String path;

        public boolean isSelected;
    }

    class ViewHolder {
        public ImageView image;

        public ImageView imgChecked;
    }
}
