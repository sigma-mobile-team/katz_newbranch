
package sigma.qilex.ui.adapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.DateTimeUtils;
import sigma.qilex.utils.Utils;

public class ContactDetailCallListAdapter extends BaseAdapter {

    WeakReference<BaseActivity> mActivityWeak;

    DisplayImageOptions options;

    ArrayList<CallLogModel> listItems;

    LayoutInflater mInflater;

    int viewMode;

    public ContactDetailCallListAdapter(BaseActivity baseActivity, ArrayList<CallLogModel> listPhoneCalls) {
        mActivityWeak = new WeakReference<BaseActivity>(baseActivity);
        mInflater = (LayoutInflater)mActivityWeak.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        options = mActivityWeak.get().circleDisplayImageOptions;
        listItems = listPhoneCalls;
    }

    // get the section textview from row view
    // the section view will only be shown for the first item
    public TextView getSectionTextView(View rowView) {
        TextView sectionTextView = (TextView)rowView.findViewById(R.id.sectionTextView);
        return sectionTextView;
    }

    // this should be override by subclass if necessary
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        CallLogModel item = new CallLogModel(listItems.get(position));
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.phone_call_history_item, parent, false);
            // default just draw the item only
            holder.day = (MyCustomTextView)convertView.findViewById(R.id.day_of_time);
            holder.hour = (MyCustomTextView)convertView.findViewById(R.id.hour_of_time);
            holder.duration = (MyCustomTextView)convertView.findViewById(R.id.duration);
            holder.type = (ImageView)convertView.findViewById(R.id.type);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.day.setText(Utils.convertSimpleDayFormat(item.getDateMilliSecond()));
        holder.hour.setText(Utils.parseHourTime(item.getDateMilliSecond()));
        String duration = item.getDuration() == 0 ? mActivityWeak.get().getResources().getString(R.string.cancel)
                : DateTimeUtils.formatTimeMinuteSecond(item.getDuration());
        holder.duration.setText(duration);
        holder.type.setImageResource(item.getIconTypeSrc());
        return convertView;

    }

    class ViewHolder {

        public MyCustomTextView day;

        public ImageView type;

        public MyCustomTextView hour;

        public MyCustomTextView duration;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public CallLogModel getItem(int position) {
        if (!listItems.isEmpty())
            return listItems.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
