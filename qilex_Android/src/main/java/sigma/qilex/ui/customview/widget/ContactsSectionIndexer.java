
package sigma.qilex.ui.customview.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.widget.SectionIndexer;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.QilexContact.NameComparator;

public class ContactsSectionIndexer implements SectionIndexer {

    private static String OTHER = "#";

    private static String[] mSections = {
            OTHER, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z"
    };

    private static int OTHER_INDEX = 0; // index of other in the mSections array

    private int[] mPositions; // store the list of starting position index for
                              // each section
                              // e.g. A start at index 0, B start at index 20, C
                              // start at index 41 and so on

    private int mCount; // this is the count for total number of contacts

    private boolean isFullSection = false;// set if you want full section
                                          // default, if not, this will only
                                          // show which sections available

    // Assumption: the contacts array has been sorted
    public ContactsSectionIndexer(List<QilexContact> contacts) {
        if (contacts == null) {
            mCount = 0;
            initSectionsList(new ArrayList<QilexContact>());
        } else {
            mCount = contacts.size();
            if (!isFullSection) {
                initSectionsList(contacts);
            } else
                initPositions(contacts);
        }
    }

    @SuppressLint("DefaultLocale")
    private void initSectionsList(List<QilexContact> contacts) {
        int position = 0;
        ArrayList<Integer> positions = new ArrayList<Integer>();
        ArrayList<String> sections = new ArrayList<String>();
        NameComparator comparator = new NameComparator(MyApplication.getAppContext());
        for (QilexContact contact : contacts) {

            String indexableItem = contact.getDisplayName().trim();
            if (indexableItem.length() == 0) {
                if (!sections.contains(OTHER)) {
                    sections.add(OTHER);
                    positions.add(position);
                }
            }
            String sub = indexableItem.substring(0, 1).toUpperCase();
            if (sub.compareTo("A") >= 0 && sub.compareTo("Z") <= 0) {
                if (!sections.contains(sub)) {
                    sections.add(sub);
                    positions.add(position);
                }
            } else {
                String newSub = OTHER;
                for (char c = 'A'; c < 'Z' - 1; c++) {
                    if (comparator.compare(String.valueOf(c), sub)
                            * comparator.compare(String.valueOf((char)(c + 1)), sub) < 0) {
                        newSub = String.valueOf(c);
                        break;
                    }
                }
                if (!sections.contains(newSub)) {
                    sections.add(newSub);
                    positions.add(position);
                }

            }
            position++;
        }
        mSections = new String[sections.size()];
        mSections = sections.toArray(mSections);
        mPositions = new int[positions.size()];
        int count = 0;
        for (int i : positions) {
            mPositions[count++] = i;
        }
    }

    public String getSectionTitle(String indexableItem) {
        int sectionIndex = getSectionIndex(indexableItem);
        if (mSections.length == 0) {
            return Const.STR_EMPTY;
        }
        return mSections[sectionIndex];
    }

    // return which section this item belong to
    @SuppressLint("DefaultLocale")
    public int getSectionIndex(String indexableItem) {

        if (indexableItem == null) {
            return OTHER_INDEX;
        }

        indexableItem = indexableItem.trim();
        String firstLetter = OTHER;

        if (indexableItem.length() == 0) {
            return OTHER_INDEX;
        } else {
            // get the first letter
            firstLetter = String.valueOf(indexableItem.charAt(0)).toUpperCase();
        }
        NameComparator comparator = new NameComparator(MyApplication.getAppContext());
        int sectionCount = mSections.length;
        for (int i = 0; i < sectionCount; i++) {
            if (firstLetter.equals(mSections[i])) {
                return i;
            }
            if (i < sectionCount - 1)
                if (comparator.compare(mSections[i], firstLetter)
                        * comparator.compare(mSections[i + 1], firstLetter) < 0) {
                    return i;
                }
        }

        return OTHER_INDEX;

    }

    // initialize the position index
    public void initPositions(List<QilexContact> contacts) {

        int sectionCount = mSections.length;
        mPositions = new int[sectionCount];

        Arrays.fill(mPositions, -1); // initialize everything to -1

        // Assumption: list of items have already been sorted by the prefer
        // names
        int itemIndex = 0;

        for (QilexContact contact : contacts) {

            String indexableItem = contact.getDisplayName().trim();
            int sectionIndex = getSectionIndex(indexableItem); // find out which
                                                               // section this
                                                               // item belong to

            if (mPositions[sectionIndex] == -1) // if not set before, then do
                                                // this, otherwise just ignore
                mPositions[sectionIndex] = itemIndex;

            itemIndex++;

        }

        int lastPos = -1;

        // now loop through, for all the ones not found, set position to the one
        // before them
        // this is to make sure the array is sorted for binary search to work
        for (int i = 0; i < sectionCount; i++) {
            if (mPositions[i] == -1)
                mPositions[i] = lastPos;

            lastPos = mPositions[i];

        }

    }

    @Override
    public int getPositionForSection(int section) {
        if (section < 0 || section >= mSections.length) {
            return -1;
        }

        return mPositions[section];
    }

    @Override
    public int getSectionForPosition(int position) {
        if (position < 0 || position >= mCount) {
            return -1;
        }

        int index = Arrays.binarySearch(mPositions, position);

        /*
         * Consider this example: section positions are 0, 3, 5; the supplied
         * position is 4. The section corresponding to position 4 starts at
         * position 3, so the expected return value is 1. Binary search will not
         * find 4 in the array and thus will return -insertPosition-1, i.e. -3.
         * To get from that number to the expected value of 1 we need to negate
         * and subtract 2.
         */
        return index >= 0 ? index : -index - 2;
    }

    @Override
    public Object[] getSections() {
        return mSections;
    }

    // if first item in section, then return the section
    // otherwise return -1
    public boolean isFirstItemInSection(int position) { // check whether this
                                                        // item is the first
                                                        // item in section
        int section = Arrays.binarySearch(mPositions, position);
        return (section > -1);

    }

}
