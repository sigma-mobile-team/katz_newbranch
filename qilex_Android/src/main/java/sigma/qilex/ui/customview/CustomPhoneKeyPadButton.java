
package sigma.qilex.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.ui.activity.BaseActivity;

/**
 * Extends View.Button: Custom font in Button class
 */
public class CustomPhoneKeyPadButton extends Button {

    public CustomPhoneKeyPadButton(Context context) {
        super(context);
    }

    public CustomPhoneKeyPadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(this, context, attrs);
    }

    public CustomPhoneKeyPadButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(this, context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    /**
     * Sets a font on a textview based on the custom :font attribute If the
     * custom font attribute isn't found in the attributes nothing happens
     * 
     * @param textview
     * @param context
     * @param attrs
     */
    private void setCustomFont(TextView textview, Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoStyle);
        String font = a.getString(R.styleable.RobotoStyle_font);
        if (font == null) {
            this.setTypeface(BaseActivity.sTypefaceRobotoLight);
        } else if (font.equals(Const.BOLD)) {
            this.setTypeface(BaseActivity.sTypefaceRobotoBold);
        } else if (font.equals(Const.LIGHT)) {
            this.setTypeface(BaseActivity.sTypefaceRobotoLight);
        } else { // normal or null
            this.setTypeface(BaseActivity.sTypefaceRoboto);
        }
        a.recycle();
    }

}
