
package sigma.qilex.ui.customview;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SoftKeyboardRelativeLayout extends RelativeLayout {
    // Callback
    public interface SoftKeyboardListener {
        public void onSoftKeyboardShow();

        public void onSoftKeyboardHide();
    }

    private boolean isKeyboardShown = false;

    private List<SoftKeyboardListener> lsners = new ArrayList<SoftKeyboardListener>();

    private float layoutMaxH = 0f; // max measured height is considered layout
                                   // normal size

    private static final float DETECT_ON_SIZE_PERCENT = 0.8f;

    public SoftKeyboardRelativeLayout(Context context) {
        super(context);
    }

    public SoftKeyboardRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @SuppressLint("NewApi")
    public SoftKeyboardRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int newH = MeasureSpec.getSize(heightMeasureSpec);
        if (newH > layoutMaxH) {
            layoutMaxH = newH;
        }
        if (layoutMaxH != 0f) {
            final float sizePercent = newH / layoutMaxH;
            if (!isKeyboardShown && sizePercent <= DETECT_ON_SIZE_PERCENT) {
                isKeyboardShown = true;
                for (final SoftKeyboardListener lsner : lsners) {
                    lsner.onSoftKeyboardShow();
                }
            } else if (isKeyboardShown && sizePercent > DETECT_ON_SIZE_PERCENT) {
                isKeyboardShown = false;
                for (final SoftKeyboardListener lsner : lsners) {
                    lsner.onSoftKeyboardHide();
                }
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void addSoftKeyboardListener(SoftKeyboardListener lsner) {
        lsners.add(lsner);
    }

    public void removeSoftKeyboardListener(SoftKeyboardListener lsner) {
        lsners.remove(lsner);
    }

}
