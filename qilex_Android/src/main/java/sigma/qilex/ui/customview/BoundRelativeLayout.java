
package sigma.qilex.ui.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class BoundRelativeLayout extends RelativeLayout {

    public BoundRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BoundRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public BoundRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public BoundRelativeLayout(Context context) {
        super(context);
    }

    private int mMaxWidth = Integer.MAX_VALUE;

    public void setMaxWidth(int mMaxWidth) {
        this.mMaxWidth = mMaxWidth;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getMeasuredWidth() > mMaxWidth) {
            setMeasuredDimension(mMaxWidth, getMeasuredHeight());
        }
    }
}
