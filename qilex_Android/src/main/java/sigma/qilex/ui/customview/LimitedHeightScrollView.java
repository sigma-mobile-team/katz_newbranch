
package sigma.qilex.ui.customview;

import sigma.qilex.utils.Utils;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class LimitedHeightScrollView extends ScrollView {

    public LimitedHeightScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public LimitedHeightScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LimitedHeightScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LimitedHeightScrollView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = MeasureSpec.makeMeasureSpec((int)Utils.pxFromDp(150), MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
