
package sigma.qilex.ui.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.utils.Utils;

/**
 * Extends View.EditText: Custom font for EditText
 */
public class MyCustomEditText extends EditText {

    public MyCustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    @TargetApi(21)
    public MyCustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);

    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoStyle);

            final int N = a.getIndexCount();
            for (int i = 0; i < N; ++i) {
                int attr = a.getIndex(i);
                switch (attr) {
                    case R.styleable.RobotoStyle_font:
                        String font = a.getString(attr);
                        if (font == null) {
                            this.setTypeface(BaseActivity.sTypefaceRobotoLight);
                        } else if (font.equals(Const.BOLD)) {
                            this.setTypeface(BaseActivity.sTypefaceRobotoBold);
                        } else if (font.equals(Const.LIGHT)) {
                            this.setTypeface(BaseActivity.sTypefaceRobotoLight);
                        } else if (font.equals(Const.MEDIUM)) {
                            this.setTypeface(BaseActivity.sTypefaceRobotoMedium);
                        } else { // normal or null
                            this.setTypeface(BaseActivity.sTypefaceRoboto);
                        }
                        break;
                }
            }
            a.recycle();
        }
    }

    public MyCustomEditText(Context context) {
        super(context);
        this.setTypeface(BaseActivity.sTypefaceRoboto);
    }

    public boolean checkMandatory() {
        String text = this.getText().toString();
        if (Utils.isStringNullOrEmpty(text)) {
            Animation shake = AnimationUtils.loadAnimation(this.getContext(), R.anim.shake);
            this.startAnimation(shake);
            return false;
        }
        return true;
    }
}
