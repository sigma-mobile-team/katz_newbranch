
package sigma.qilex.ui.customview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;
import sigma.qilex.ui.customview.widget.IndexScroller;

public class ContactListView extends ListView {

    protected boolean mIsFastScrollEnabled = false;

    protected IndexScroller mScroller = null;

    protected GestureDetector mGestureDetector = null;

    // additional customization
    protected boolean inSearchMode = false; // whether is in search mode

    protected boolean autoHide = false; // alway show the scroller

    private boolean isNeedDraw = false;

    public ContactListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(21)
    public ContactListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public IndexScroller getScroller() {
        return mScroller;
    }

    @Override
    public boolean isFastScrollEnabled() {
        return mIsFastScrollEnabled;
    }

    public void setNeedReDrawScroller() {
        isNeedDraw = true;
    }

    // override this if necessary for custom scroller
    public void createScroller() {
        mScroller = new IndexScroller(getContext(), this);
        mScroller.setAutoHide(autoHide);
        mScroller.setShowIndexContainer(false);
        mScroller.setIndexPaintColor(Color.argb(255, 0, 122, 255));

        if (autoHide)
            mScroller.hide();
        else
            mScroller.show();

        // style 1

        // style 2
        // mScroller.setShowIndexContainer(true);
        // mScroller.setIndexPaintColor(Color.WHITE);

    }

    @Override
    public void setFastScrollEnabled(boolean enabled) {
        mIsFastScrollEnabled = enabled;
        if (mIsFastScrollEnabled) {
            if (mScroller == null) {
                createScroller();
            }
        } else {
            if (mScroller != null) {
                mScroller.hide();
                mScroller = null;
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        // Overlay index bar
        if (!inSearchMode) // dun draw the scroller if not in search mode
        {
            if (mScroller != null || isNeedDraw) {
                mScroller.draw(canvas, isNeedDraw);
                isNeedDraw = false;
            }
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        // disable when in search mode
        if (inSearchMode) {
            return super.onTouchEvent(ev);
        }
        // Intercept ListView's touch event
        if (mScroller != null && mScroller.onTouchEvent(ev))
            return true;

        if (mGestureDetector == null) {
            mGestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    // If fling happens, index bar shows
                    mScroller.show();
                    return super.onFling(e1, e2, velocityX, velocityY);
                }

            });
        }
        mGestureDetector.onTouchEvent(ev);

        return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mScroller.contains(ev.getX(), ev.getY()))
            return true;

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
        if (mScroller != null)
            mScroller.setAdapter(adapter);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mScroller != null)
            mScroller.onSizeChanged(w, h, oldw, oldh);
    }

    public boolean isInSearchMode() {
        return inSearchMode;
    }

    public void setInSearchMode(boolean inSearchMode) {
        this.inSearchMode = inSearchMode;
    }

}
