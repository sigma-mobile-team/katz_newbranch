
package sigma.qilex.ui.customview;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import pl.frifon.aero2.R;
import sigma.qilex.manager.I_ThreadMessageDispatcher;
import sigma.qilex.manager.ThreadMessageDispatcher;

public class PhoneKeypadView extends FrameLayout implements View.OnClickListener, View.OnTouchListener {

    private static final int MSG_LONG_CLICK = 1;

    private static final int LONG_CLICK_DELAY = 600;

    private View imgTouchAnim;

    private Button btnKeypad;

    private TextView txtSecondContent;

    private View.OnClickListener listener = null;

    private View.OnLongClickListener longClickListener = null;

    private ThreadMessageDispatcher vrcDispatcher;

    private boolean mIsActionLongClick = false;

    private int dtmfId;

    private I_ThreadMessageDispatcher l_listenerDispatcher = new I_ThreadMessageDispatcher() {
        public void onHandleMessage(Message vrpMsg) {
            switch (vrpMsg.what) {
                case MSG_LONG_CLICK:
                    if (longClickListener != null) {
                        longClickListener.onLongClick(PhoneKeypadView.this);
                        mIsActionLongClick = true;
                    }
                    break;
            }
        }
    };

    @TargetApi(21)
    public PhoneKeypadView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    public PhoneKeypadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public PhoneKeypadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public PhoneKeypadView(Context context) {
        super(context);
        initView(context);
    }

    @SuppressLint("InflateParams")
    private void initView(Context context) {
        vrcDispatcher = new ThreadMessageDispatcher(l_listenerDispatcher);

        // Inflate layout
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.phone_keypad, null);
        this.addView(v);

        imgTouchAnim = v.findViewById(R.id.imgTouchAnim);
        btnKeypad = (Button)v.findViewById(R.id.btnKeypad);
        txtSecondContent = (TextView)v.findViewById(R.id.txtSecondContent);

        // Add action listener
        // btnKeypad.setOnClickListener(this);
        btnKeypad.setOnTouchListener(this);
    }

    public void setText(String text) {
        btnKeypad.setText(text);
    }

    public void setSmallText(String text) {
        if (text == null) {
            txtSecondContent.setVisibility(View.GONE);
        } else {
            txtSecondContent.setText(text);
            txtSecondContent.setVisibility(View.VISIBLE);
        }
    }

    public String getText() {
        return btnKeypad.getText().toString();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setOnLongClickListener(View.OnLongClickListener listener) {
        this.longClickListener = listener;
    }

    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        // ObjectAnimator anim = ObjectAnimator.ofFloat(imgTouchAnim, "alpha",
        // 1, 0);
        // anim.setDuration(200);
        // anim.start();

        if (this.listener != null) {
            this.listener.onClick(this);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            imgTouchAnim.setAlpha(0.6f);
            vrcDispatcher.removeMessages(MSG_LONG_CLICK);
            vrcDispatcher.sendEmptyMessageDelayed(MSG_LONG_CLICK, LONG_CLICK_DELAY);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            vrcDispatcher.removeMessages(MSG_LONG_CLICK);
            ObjectAnimator anim = ObjectAnimator.ofFloat(imgTouchAnim, "alpha", 0.6f, 0);
            anim.setDuration(200);
            anim.start();

            if (mIsActionLongClick == true) {
                mIsActionLongClick = false;
                return false;
            }
            if (this.listener != null) {
                this.listener.onClick(this);
            }
        }

        return false;
    }

    public int getDtmfId() {
        return dtmfId;
    }

    public void setDtmfId(int dtmfId) {
        this.dtmfId = dtmfId;
    }
}
