
package sigma.qilex.ui.customview;

import android.app.Activity;
import android.app.Service;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import sigma.qilex.ui.activity.BaseActivity;

/**
 * BaseFragment only for the ExtendActivity from BaseActivity
 */
public class BaseFragment extends Fragment {

    protected BaseActivity mBaseActivity;

    private boolean isActive;

    protected SoftKeyboard softKeyboard;

    private boolean mIsEditMode;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mBaseActivity = (BaseActivity)activity;
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (softKeyboard != null) {
            softKeyboard.unRegisterSoftKeyboardCallback();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isActive = false;
    }

    @Override
    public void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mBaseActivity.hideKeyboard();
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mBaseActivity = null;
    }

    public void addKeyBoardListener(ViewGroup viewGroup, SoftKeyboard.SoftKeyboardChanged softKeyboarChange) {
        InputMethodManager im = (InputMethodManager)mBaseActivity.getSystemService(Service.INPUT_METHOD_SERVICE);
        softKeyboard = new SoftKeyboard(viewGroup, im);
        softKeyboard.setSoftKeyboardCallback(softKeyboarChange);
    }

    public boolean isEditMode() {
        return mIsEditMode;
    }

    public void setEditMode(boolean isEditMode) {
        this.mIsEditMode = isEditMode;
    }
}
