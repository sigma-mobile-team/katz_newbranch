
package sigma.qilex.ui.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.ui.activity.BaseActivity;

/**
 * Extends View.Button: Custom font in Button class
 */
public class CustomButton extends Button {
    // padding in dp
    public static int PADDING_HORIZONTAL = 4;

    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(this, context, attrs);
    }

    @TargetApi(21)
    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(this, context, attrs);
    }

    /**
     * Sets a font on a textview based on the custom :font attribute If the
     * custom font attribute isn't found in the attributes nothing happens
     * 
     * @param textview
     * @param context
     * @param attrs
     */
    private void setCustomFont(TextView textview, Context context, AttributeSet attrs) {
        int padding = (int)(context.getResources().getDisplayMetrics().density * PADDING_HORIZONTAL);
        setPadding(padding, 0, padding, 0);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RobotoStyle);
        String font = a.getString(R.styleable.RobotoStyle_font);
        if (font == null) {
            this.setTypeface(BaseActivity.sTypefaceRobotoLight);
        } else if (font.equals(Const.BOLD)) {
            this.setTypeface(BaseActivity.sTypefaceRobotoBold);
        } else if (font.equals(Const.LIGHT)) {
            this.setTypeface(BaseActivity.sTypefaceRobotoLight);
        } else if (font.equals(Const.MEDIUM)) {
            this.setTypeface(BaseActivity.sTypefaceRobotoMedium);
        } else if (font.equals(Const.OPEN_SANS_REGULAR)) {
            this.setTypeface(BaseActivity.sTypefaceOpenSans);
        } else { // normal or null
            this.setTypeface(BaseActivity.sTypefaceRoboto);
        }
        a.recycle();
    }

}
