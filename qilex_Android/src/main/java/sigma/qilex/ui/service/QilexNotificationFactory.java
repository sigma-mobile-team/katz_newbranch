
package sigma.qilex.ui.service;

import java.util.ArrayList;
import java.util.Calendar;

import com.nostra13.universalimageloader.core.assist.ImageSize;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.support.v4.app.NotificationCompat;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.CallInfo;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.ContactKATZOutDetailActivity;
import sigma.qilex.ui.activity.ConversationActivity;
import sigma.qilex.ui.activity.InCallActivity;
import sigma.qilex.ui.activity.IncomingCallActivity;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.activity.WelcomeActivity;
import sigma.qilex.ui.tab.ContactListFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.Utils;

public class QilexNotificationFactory {

    public static final int MAXIMUM_NEW_KATZ_NOTIFY = 5;

    public static final int NOTIFY_ID_SYNC_CONTACT = 1000;

    public static final int NOTIFY_ID_OUTGOING_CALL = 1001;

    public static final int NOTIFY_ID_MISSCALL = 1002;

    public static final int NOTIFY_ID_INCOM_MESSAGE = 1003;

    public static final int NOTIFY_ID_APP_LOGOUT = 1004;

    public static final int NOTIFY_ID_NEW_KATZ_CONTACT = 1500;

    public static final int NOTIFY_ID_PAYMENT_FINISH = 1006;

    public static final int NOTIFY_ID_NONE = 999;

    private int notifyIdNewKatz;

    NotificationManager mNotifyManager;

    private QilexService mService;

    private PendingIntent mLaunchMainComponentPI;

    private PendingIntent mLaunchMainComponentPIWithTabInfo;

    private Notification mActiveNotification;

    private Notification mInactiveNetworkDownNotification;

    private NotificationCompat.Builder syncContactBuilder;

    private NotificationCompat.Builder ongoingCallBuilder;

    private NotificationCompat.Builder missCallBuilder;

    private PendingIntent ongoingCallPendingIntent;

    private PendingIntent incomingCallPendingIntent;

    private PendingIntent incomMessagePendingIntent;

    Bitmap mLargeIcon, mLargeIconInactive;

    int mIconInactive;

    int mIconActive;

    int notifyIconSize;

    ImageSize iconSize;

    Bitmap defaultAvatar;

    public QilexNotificationFactory(QilexService service) {
        mService = service;
        notifyIdNewKatz = 0;
        mNotifyManager = (NotificationManager)service.getSystemService(Context.NOTIFICATION_SERVICE);

        notifyIconSize = (int)QilexUtils.convertDpToPixel(60, mService);
        iconSize = new ImageSize(notifyIconSize, notifyIconSize);
        Options options = new Options();
        options.outHeight = notifyIconSize;
        options.outWidth = notifyIconSize;
        defaultAvatar = BitmapFactory.decodeResource(mService.getResources(), R.drawable.avt_default_circle, options);

        // Init pending Intent
        Intent showMainComponentIntent = new Intent(QilexService.BROADCAST_NOTIFICATION());
        mLaunchMainComponentPI = PendingIntent.getBroadcast(mService, 0, showMainComponentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mLargeIcon = BitmapFactory.decodeResource(service.getResources(), MyApplication.getAppIconId());
        mLargeIconInactive = BitmapFactory.decodeResource(service.getResources(), R.drawable.ic_launcher_inactive);
        if (Utils.hasLlAbove()) {
            mIconInactive = R.drawable.ic_notification_lollipop_off;
            mIconActive = R.drawable.ic_notification_lollipop;
        } else {
            mIconInactive = R.drawable.ic_launcher_inactive;
            mIconActive = MyApplication.getAppIconId();
        }

        // Init notification Active
        mActiveNotification = new NotificationCompat.Builder(service.getApplicationContext()).setSmallIcon(mIconActive)
                .setLargeIcon(mLargeIcon).setContentTitle("KATZ (Account Name)").setContentText("Service Active")
                .setContentIntent(mLaunchMainComponentPI).build();

        // Init notification Inactive network down
        mInactiveNetworkDownNotification = new NotificationCompat.Builder(service.getApplicationContext())
                .setSmallIcon(mIconInactive).setLargeIcon(mLargeIconInactive).setContentTitle("KATZ (Account Name)")
                .setContentText(getString(R.string.msg_call_no_network_title)).setContentIntent(mLaunchMainComponentPI)
                .build();

        // Init ongoing call notification builder
        ongoingCallBuilder = new NotificationCompat.Builder(mService);
        ongoingCallBuilder.setSmallIcon(R.drawable.icon_end_call);

        //
        Intent intentClose = new Intent(InCallActivity.ACTION_ENDCALL);
        PendingIntent pi = PendingIntent.getBroadcast(mService, 4, intentClose, PendingIntent.FLAG_UPDATE_CURRENT);
        ongoingCallBuilder.setDeleteIntent(pi);

        // Init misscall notification builder
        missCallBuilder = new NotificationCompat.Builder(mService);
        missCallBuilder.setSmallIcon(R.drawable.icon_miss_call);
        Intent showMainComponentIntentWithTabInfo = new Intent(QilexService.BROADCAST_NOTIFICATION());
        // switch to tab 0 - Call
        showMainComponentIntentWithTabInfo.putExtra(MainTabActivity.BUNDLE_TAB_START, MainTabActivity.TAB_INDEX_CALLS);
        mLaunchMainComponentPIWithTabInfo = PendingIntent.getBroadcast(mService, 0, showMainComponentIntentWithTabInfo,
                PendingIntent.FLAG_UPDATE_CURRENT);

        missCallBuilder.setContentIntent(mLaunchMainComponentPIWithTabInfo);
        missCallBuilder.setAutoCancel(true);

        // Init sync contact notification builder
        syncContactBuilder = new NotificationCompat.Builder(mService.getApplicationContext());
        syncContactBuilder.setSmallIcon(mIconActive).setLargeIcon(mLargeIcon).setContentTitle("Synch All Contact");
    }

    public void destroy() {
        mNotifyManager.cancelAll();
    }

    public Notification createServiceActive() {
        mActiveNotification = new NotificationCompat.Builder(mService.getApplicationContext()).setSmallIcon(mIconActive)
                .setLargeIcon(mLargeIcon).setContentTitle(getNotificationTitle()).setContentText("Service Running")
                .setContentIntent(mLaunchMainComponentPI).build();
        return mActiveNotification;
    }

    public Notification createServiceInactiveNetworkDown() {
        mInactiveNetworkDownNotification = new NotificationCompat.Builder(mService.getApplicationContext())
                .setSmallIcon(mIconInactive).setLargeIcon(mLargeIconInactive).setContentTitle(getNotificationTitle())
                .setContentText(getString(R.string.msg_call_no_network_title)).setContentIntent(mLaunchMainComponentPI)
                .build();
        return mInactiveNetworkDownNotification;
    }

    public void showOutgoingNotification(CallInfo callInfo, String timerCount) {
        Intent showOutgoingCallIntent = new Intent(mService, InCallActivity.class);
        showOutgoingCallIntent.putExtra("calling_info_notif", callInfo);
        ongoingCallPendingIntent = PendingIntent.getActivity(mService, 1, showOutgoingCallIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        ongoingCallBuilder.setContentIntent(ongoingCallPendingIntent);
        ongoingCallBuilder.setContentTitle(getString(R.string.calling_notif, callInfo.getContactName()));
        ongoingCallBuilder.setContentText(timerCount);
        Notification notify = ongoingCallBuilder.build();
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_OUTGOING_CALL, notify);
    }

    public void showPaymentSuccess() {
        NotificationCompat.Builder paymentBuilder = new NotificationCompat.Builder(mService);
        paymentBuilder.setSmallIcon(MyApplication.getAppIconId());
        paymentBuilder.setContentTitle(mService.getString(R.string.payment_successfully));
        paymentBuilder.setContentText(mService.getString(R.string.payment_successfully));
        paymentBuilder.setAutoCancel(true);
        Notification notify = paymentBuilder.build();
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_PAYMENT_FINISH, notify);
    }

    public void showNotifyText(String title, String content) {
        NotificationCompat.Builder paymentBuilder = new NotificationCompat.Builder(mService);
        paymentBuilder.setSmallIcon(MyApplication.getAppIconId());
        paymentBuilder.setContentTitle(title);
        paymentBuilder.setContentText(content);
        paymentBuilder.setAutoCancel(true);
        Notification notify = paymentBuilder.build();
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_NONE, notify);
    }

    public void showNewKatzNotification(String name, String avatarUrl, String contactLookUpKey, long contactId) {
        notifyIdNewKatz++;
        if (notifyIdNewKatz >= MAXIMUM_NEW_KATZ_NOTIFY) {
            notifyIdNewKatz = 0;
        }

        Intent intent = new Intent(mService, ContactKATZOutDetailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY, contactLookUpKey);
        intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contactId);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_BOTTOM_OUT_BOTTOM);
        NotificationCompat.Builder newKatzBuilder = new NotificationCompat.Builder(mService);
        newKatzBuilder.setSmallIcon(MyApplication.getAppIconId());

        if (Utils.isInKatzMode()) {
            newKatzBuilder.setContentTitle(getString(R.string.new_katz_contact_inform));
            newKatzBuilder.setContentText(name + " " + getString(R.string.new_katz_contact));
        } else {
            newKatzBuilder.setContentTitle(getString(R.string.new_katz_contact_inform_green));
            newKatzBuilder.setContentText(name + " " + getString(R.string.new_katz_contact_green));
        }
        newKatzBuilder.setAutoCancel(true);

        PendingIntent startActivityIntent = PendingIntent.getActivity(mService, NOTIFY_ID_NEW_KATZ_CONTACT, intent,
                Intent.FLAG_ACTIVITY_NEW_TASK);
        newKatzBuilder.setContentIntent(startActivityIntent);

        if (Utils.isStringNullOrEmpty(avatarUrl) == false) {
            Bitmap avatar = MyApplication.getImageLoader().loadImageSync(avatarUrl, iconSize);
            if (avatar != null) {
                newKatzBuilder.setLargeIcon(avatar);
            } else {
                newKatzBuilder.setLargeIcon(defaultAvatar);
            }
        } else {
            newKatzBuilder.setLargeIcon(defaultAvatar);
        }
        Notification notify = newKatzBuilder.build();
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_NEW_KATZ_CONTACT + notifyIdNewKatz, notify);
    }

    public void hideOutgoingNotification() {
        mNotifyManager.cancel(NOTIFY_ID_OUTGOING_CALL);
    }

    public void clearIncomMessageNotification() {
        mNotifyManager.cancel(NOTIFY_ID_INCOM_MESSAGE);
    }
    
    public void showGroupChatStatusNotification(String groupUuid, String content) {
        // message
        NotificationCompat.Builder incomMessageBuilder = new NotificationCompat.Builder(mService);
        incomMessageBuilder.setSmallIcon(MyApplication.getAppIconId());
        
        Intent showIncomMessageIntent = new Intent(mService, ConversationActivity.class);
        showIncomMessageIntent.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        showIncomMessageIntent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, groupUuid);
        incomMessagePendingIntent = PendingIntent.getActivity(mService, 3, showIncomMessageIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        incomMessageBuilder.setContentIntent(incomMessagePendingIntent);
        incomMessageBuilder.setContentTitle(getString(R.string.group));
        incomMessageBuilder.setContentText(content);
        incomMessageBuilder.setLargeIcon(defaultAvatar);
        Notification notify = incomMessageBuilder.build();
        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_INCOM_MESSAGE, notify);
    }
    
    public void showGroupChatSelfKickedNotification(String content) {
        // message
        NotificationCompat.Builder incomMessageBuilder = new NotificationCompat.Builder(mService);
        incomMessageBuilder.setSmallIcon(MyApplication.getAppIconId());
        incomMessageBuilder.setContentIntent(incomMessagePendingIntent);
        incomMessageBuilder.setContentTitle(getString(R.string.group));
        incomMessageBuilder.setContentText(content);
        incomMessageBuilder.setLargeIcon(defaultAvatar);
        Notification notify = incomMessageBuilder.build();
        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_INCOM_MESSAGE, notify);
    }

    public void showIncomMessageNotification2(long threadId, String content, String... phoneNumbers) {
        // message
        NotificationCompat.Builder incomMessageBuilder = new NotificationCompat.Builder(mService);
        incomMessageBuilder.setSmallIcon(MyApplication.getAppIconId());

        // Get data from phone numner
        ContactManager contactManager = ContactManager.getInstance();
        String name = Const.STR_EMPTY;
        String avatarUrl = Const.STR_EMPTY;
        String firstPhone = phoneNumbers[0];
        QilexContact contact = contactManager.getContactByPhoneNo(MyApplication.getAppContext(), firstPhone);
        if (contact == null) {
            TempContactModel tempContact = contactManager.getTempContact(firstPhone);
            if (tempContact != null) {
                name = tempContact.name;
                avatarUrl = tempContact.avatarUrl;
            } else {
                name = firstPhone;
            }
        } else {
            name = contact.getDisplayName();
            avatarUrl = contact.photoUri;
        }

        Bitmap avatar = MyApplication.getImageLoader().loadImageSync(avatarUrl, iconSize);

        Intent showIncomMessageIntent = new Intent(mService, ConversationActivity.class);
        showIncomMessageIntent.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        showIncomMessageIntent.putExtra(ConversationActivity.INTENT_PARAM_THREAD_ID, threadId);
        showIncomMessageIntent.putExtra(ConversationActivity.INTENT_PARAM_CONTACT, firstPhone);
        incomMessagePendingIntent = PendingIntent.getActivity(mService, 3, showIncomMessageIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        incomMessageBuilder.setContentIntent(incomMessagePendingIntent);
        incomMessageBuilder.setContentTitle(getString(R.string.new_message_from));
        incomMessageBuilder.setContentText(name + ": " + content);
        if (avatar != null) {
            incomMessageBuilder.setLargeIcon(avatar);
        } else {
            incomMessageBuilder.setLargeIcon(defaultAvatar);
        }
        Notification notify = incomMessageBuilder.build();
        notify.flags |= Notification.FLAG_AUTO_CANCEL;
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_INCOM_MESSAGE, notify);
    }

    public void showIncomingNotification(String phoneNumber, String avatarUrl) {
        Intent showIncomingCallIntent = new Intent(mService, IncomingCallActivity.class);

        // Get data from phone numner
        ContactManager contactManager = ContactManager.getInstance();
        String name = Const.STR_EMPTY;
        QilexContact contact = contactManager.getContactByPhoneNo(MyApplication.getAppContext(), phoneNumber);
        if (contact == null) {
            TempContactModel tempContact = contactManager.getTempContact(phoneNumber);
            if (tempContact != null) {
                name = tempContact.name;
                avatarUrl = tempContact.avatarUrl;
            } else {
                name = phoneNumber;
            }
        } else {
            name = contact.getDisplayName();
            avatarUrl = contact.photoUri;
        }

        showIncomingCallIntent.putExtra("NAME", phoneNumber);
        incomingCallPendingIntent = PendingIntent.getActivity(mService, 2, showIncomingCallIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        ongoingCallBuilder.setContentIntent(incomingCallPendingIntent);
        ongoingCallBuilder.setContentTitle(getString(
                Utils.isInKatzMode() ? R.string.katz_call : R.string.katz_call_green));
        ongoingCallBuilder.setContentText(getString(
                Utils.isInKatzMode() ? R.string.calling_via_katz_notif : R.string.calling_via_katz_notif_green,
                name));

        Notification notify = ongoingCallBuilder.build();
        notify.when = Calendar.getInstance().getTimeInMillis();
        mNotifyManager.notify(NOTIFY_ID_OUTGOING_CALL, notify);
    }

    public void showMissCallNotification(int totalMissCall, ArrayList<String> listPhoneNo) {
        ContactManager contactManager = ContactManager.getInstance();
        String avatarUrl = Const.STR_EMPTY;
        String strFrom = mService.getString(R.string.from) + Const.STR_SPACE;
        String strAnd = Const.STR_SPACE + mService.getString(R.string.and) + Const.STR_SPACE;
        String strOthers = Const.STR_SPACE + mService.getString(R.string.others);
        String contentText = strFrom;

        String[] name = new String[listPhoneNo.size()];
        int index = 0;
        for (String phoneNo : listPhoneNo) {
            String[] contactInfo = contactManager.getContactNameAndImageByPhoneNo(phoneNo);
            if (contactInfo != null && Utils.isStringNullOrEmpty(contactInfo[1]) == false) {
                avatarUrl = contactInfo[1];
            }
            if (contactInfo != null) {
                name[index] = contactInfo[0];
            } else {
                name[index] = phoneNo;
            }
            index++;
        }

        if (listPhoneNo.size() <= 2) {
            contentText += name[0];
            if (name.length == 2) {
                contentText += strAnd + name[1];
            }
        } else {
            contentText += name[0] + strAnd + (name.length - 1) + strOthers;
        }
        missCallBuilder.setContentText(contentText);
        missCallBuilder.setContentTitle(totalMissCall + Const.STR_SPACE + mService.getString(R.string.missed_call));
        Bitmap avatar = MyApplication.getImageLoader().loadImageSync(avatarUrl, iconSize);
        if (avatar != null) {
            missCallBuilder.setLargeIcon(avatar);
        } else {
            missCallBuilder.setLargeIcon(defaultAvatar);
        }
        mNotifyManager.notify(NOTIFY_ID_MISSCALL, missCallBuilder.build());
    }

    public void showAppLogoutNotification() {
        Intent intent = new Intent(mService, WelcomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(mService,
                0 /*
                   * Request code
                   */, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mService)
                .setSmallIcon(MyApplication.getAppIconId()).setContentTitle(Utils.isInKatzMode()
                        ? getString(R.string.app_name) : getString(R.string.app_name_green))
                .setContentText(mService.getString(R.string.app_is_logout)).setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)mService
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFY_ID_APP_LOGOUT, notificationBuilder.build());
    }

    // NAMND ADD get Title Notification
    private String getNotificationTitle() {
        UserInfo info = UserInfo.getInstance();
        return info.getFirstName() + "(" + info.getPhoneNumberFormatted() + ")";
    }

    private String getString(int resId) {
        return mService.getResources().getString(resId);
    }

    private String getString(int resId, Object... formatArgs) {
        return mService.getResources().getString(resId, formatArgs);
    }
}
