
package sigma.qilex.ui.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class QilexServiceRestartReceiver extends WakefulBroadcastReceiver {

    public static final String ACTION_QILEX_SERVICE_RESTART = "sigma.qilex.ui.service.QilexServiceRestartReceiver.ACTION_QILEX_SERVICE_RESTART";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        // Toast.makeText(context, "HAHA HIHI", Toast.LENGTH_LONG).show();
        Intent service = new Intent(context, QilexService.class);
        context.startService(service);
        // startWakefulService(context, service);
    }
}
