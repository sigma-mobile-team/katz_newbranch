
package sigma.qilex.ui.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.telephony.SmsMessage;
import android.view.WindowManager;
import android.widget.Toast;

import pl.frifon.aero2.R;

import pl.katz.aero2.AppManager;
import pl.katz.aero2.Const;
import pl.katz.aero2.MyApplication;
import pl.katz.aero2.UserInfo;
import pl.katz.aero2.gcm.RegistrationIntentService;
import pl.katz.aero2.gcm.RegistrationIntentService.RegisterGcmListener;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.PaymentModel;
import sigma.qilex.dataaccess.model.PhoneNumberModel;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.dataaccess.model.TempContactModel;
import sigma.qilex.dataaccess.model.http.ContactSnapshotModel;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.LoginResponseModel;
import sigma.qilex.dataaccess.sqlitedb.CallLogDb;
import sigma.qilex.dataaccess.sqlitedb.ChatDatabase;
import sigma.qilex.manager.NetworkStateMonitor;
import sigma.qilex.manager.NetworkStateMonitor.NetworkStateListener;
import sigma.qilex.manager.QilexSoundService;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.AuthenticationManager.PostPaymentListener;
import sigma.qilex.manager.account.GetContactSnapshotListener;
import sigma.qilex.manager.account.QilexLoginListener;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.manager.account.QilexServiceRegistrationListener;
import sigma.qilex.manager.account.SendActivationListener;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.chat.ChatManager.SipChatListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.GetContactTempByPhoneListener;
import sigma.qilex.manager.contact.ContactManager.NotifyCacheContactChangeListener;
import sigma.qilex.manager.phone.CallLogChangeListener;
import sigma.qilex.manager.phone.CallLogManager;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.manager.phone.CallReceiver;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipManager;
import sigma.qilex.sip.SipPresenceListener;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.EnterActivationActivity;
import sigma.qilex.ui.activity.IActivity;
import sigma.qilex.ui.activity.InCallActivity;
import sigma.qilex.ui.activity.IncomingCallActivity;
import sigma.qilex.ui.activity.IncomingMessageActivity;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.SimUtils;
import sigma.qilex.utils.Utils;

public class QilexService extends Service implements QilexServiceRegistrationListener, SipPresenceListener,
        NetworkStateListener, SipChatListener, CallLogChangeListener, QilexLoginListener {
    private static final String TAG = "QilexService";

    public static final String GCM_ONEOFF_TAG = "oneoff|[0,0]";

    public static final String GCM_REPEAT_TAG = "repeat|[7200,1800]";

    public static final String INTENT_PARAM_ACTION = "INTENT_PARAM_ACTION";

    public static final String INTENT_PARAM_PEER_NUMBER = "INTENT_PARAM_PEER_NUMBER";

    public static final String INTENT_PARAM_SERVER_ID = "INTENT_PARAM_SERVER_ID";

    private static boolean DEBUG = Const.DEBUG_MODE;

    public static String BROADCAST_NOTIFICATION() {
        return Utils.isInKatzMode() ? "eu.sigma.qilex.NOTIFICATIONRECEIVER" : "eu.sigma.qilex.FRIFON_NOTIFICATIONRECEIVER";
    }

    public static String BROADCAST_PAYMENT() {
        return Utils.isInKatzMode() ? "eu.sigma.qilex.BROADCAST_PAYMENT" : "eu.sigma.qilex.FRIFON_BROADCAST_PAYMENT";
    }

    public static final String INTENT_PARAM_PAYMENT_BASE64 = "INTENT_PARAM_PAYMENT_base64Payment";

    public static final String INTENT_PARAM_PAYMENT_PRODUCT_ID = "INTENT_PARAM_PAYMENT_googlePlayProductId";

    public static final int FIRST_SYNC_NOT = 1;

    public static final int FIRST_SYNC_PROCESSING = 2;

    public static final int FIRST_SYNC_FINISH = 3;

    public static boolean lastTimeIsInBackground = true;

    private Handler postHandler;

    private static QilexService mInstance = null;

    private static Vibrator mVibrationService;

    private static AudioManager mAudioManager;

    private static SipManager mSipManager;

    private CallManager mCallManager;

    private CallReceiver mCallReceiver;

    private ChatManager mChatManager;

    private ContactManager mContactManager;

    private CallLogManager mCallLogManager;

    private AuthenticationManager mAuthenManager;

    private QilexNotificationFactory mNotificationFactory;

    private boolean isRegisteredSIP = false;

    private Timer mCheckSendingMessageTimer = new Timer();

    private TimerTask mCheckSendingMessageTimerTask = new CheckFailMessageTask();

    private QilexSoundService mQilexSoundService;

    private WifiLock wifiLock;

    private WakeLock wakeLock;

    // *** PAYMENT CACHE ***

    private RegisterGcmListener mRegisterGcmListener = new RegisterGcmListener() {

        @Override
        public void onGcmRegistered(String registrationId) {
            UserInfo.getInstance().setPushToken(registrationId);
            UserInfo.getInstance().saveInfo();
            // Start register to SIP server
            mAuthenManager.registerSip(UserInfo.getInstance().getLocalProfile(), true);
        }

        @Override
        public void onGcmRegisteredFail() {
            // Start register to SIP server
            mAuthenManager.registerSip(UserInfo.getInstance().getLocalProfile(), true);
        }
    };

    BroadcastReceiver mScreenOnOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                if (mCallManager.isCallInProgress()) {
                    mQilexSoundService.stopIncomingCallRing();
                }
                if (QilexUtils.isChineseDevices()) {
                    startForeground(9000, mNotificationFactory.createServiceActive());
                }
            } else if (Intent.ACTION_SCREEN_ON.equals(action)) {
                if (QilexUtils.isChineseDevices()) {
                    stopForeground(true);
                }
            }
        }
    };

    BroadcastReceiver mPostPaymentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Listener
            PostPaymentListener paymentListener = new PostPaymentListener() {
                @Override
                public void onPostPaymentFinish(boolean isSuccess, ErrorModel error) {
                    if (isSuccess == true) {
                        postHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mNotificationFactory.showPaymentSuccess();
                            }
                        });
                    }
                }
            };

            // Post payment
            String base64Payment = intent.getExtras().getString(INTENT_PARAM_PAYMENT_BASE64);
            String googlePlayProductId = intent.getExtras().getString(INTENT_PARAM_PAYMENT_PRODUCT_ID);
            String userData = Const.STR_EMPTY;
            mAuthenManager.postAccountPaymentInfo(context, UserInfo.getInstance().getPhoneNumberFormatted(),
                    base64Payment, Const.STR_EMPTY, userData, googlePlayProductId, Const.STR_EMPTY, paymentListener);
        }
    };

    BroadcastReceiver mSimCardChangeRegister = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!Utils.isInKatzMode() && SimUtils.checkIMSIchangeManual()) {
                reActivationPhone();
            }
        }
    };

    public static final String BROADCAST_ACTION_RELOGIN = "BROADCAST_ACTION_RELOGIN";
    BroadcastReceiver mReloginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mAuthenManager.loginWithPhoneNumber(UserInfo.getInstance().getPhoneNumberFormatted());
        }
    };

    public class LocalBinder extends Binder {
        public QilexService getService() {
            return QilexService.this;
        }
    }

    private final IBinder mBinder = new LocalBinder();

    private String mCurrentChattingPhoneNo = "";

    private boolean isConversationVisible = false;

    private boolean mTestDelayElapsed = true; // no timer

    public static boolean isReady() {
        return mInstance != null && mInstance.mTestDelayElapsed;
    }

    public static QilexService getInstance() {
        if (isReady())
            return mInstance;
        throw new RuntimeException("QilexService not instantiated yet");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtils.e("NAMND", "QilexService is onCreate");
        mInstance = this; // instance is ready once manager has been
        // created
        // Initialize singleton objects
        mVibrationService = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        mSipManager = SipManager.getInstance(this);
        mSipManager.setIncomingCallListener(CallManager.getInstance());
        mContactManager = ContactManager.getInstance();
        mCallManager = CallManager.getInstance();
        mNotificationFactory = new QilexNotificationFactory(this);
        mChatManager = ChatManager.getInstance();
        mCallLogManager = CallLogManager.getInstance();
        mAuthenManager = AuthenticationManager.getInstance();
        mQilexSoundService = QilexSoundService.getInstance();

        postHandler = new Handler();
        NetworkStateMonitor.init(this);
        // go to networkchange
        NetworkStateMonitor.updateNetworkState(true, false);

        // Add listener
        NetworkStateMonitor.addListener(this);
        mChatManager.addSipChatListener(this);
        mCallLogManager.addCallLogChangeListener(this);
        mCallReceiver = new CallReceiver(this);
        mAuthenManager.registerQilexLoginListener(this);
        mAuthenManager.registerServiceRegistrationListener(this);
        registerReceiver(mNotificationReceiver, new IntentFilter(BROADCAST_NOTIFICATION()));
        registerReceiver(mPostPaymentReceiver, new IntentFilter(BROADCAST_PAYMENT()));
        // Register on/off screen
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        registerReceiver(mScreenOnOffReceiver, filter);
        RegistrationIntentService.mRegisterGcmListener = this.mRegisterGcmListener;

        if (DEBUG) {
            startForeground(1, mNotificationFactory.createServiceInactiveNetworkDown());
        }
        // dumpDeviceInformation();
        setupCheckSendingMessageTask(1000);
        lockCPU();

        // On Sim card change
        if (!Utils.isInKatzMode()) {
            registerReceiver(mSimCardChangeRegister, new IntentFilter("android.intent.action.SIM_STATE_CHANGED"));
        }
        registerReceiver(mReloginReceiver, new IntentFilter(BROADCAST_ACTION_RELOGIN));
    }

    // @Override
    // public IBinder onBind(Intent intent) {
    // return mBinder;
    // }

    public CallManager getCallManager() {
        return mCallManager;
    }

    boolean registerWithoutlogin = false;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.d("", "onMessageReceived onStartCommand");
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int startAction = extras.getInt(INTENT_PARAM_ACTION, -1);
                switch (startAction) {
                    case ActionEnum.ACTION_OPEN_INCOMING_CALL:
                        QilexSipProfile peerProfile = mCallManager.getCurrCall().getPeerProfile();
                        showIncomingSipCallGUI(peerProfile);
                        break;
                    case ActionEnum.ACTION_OPEN_INCOMING_MESSAGE:
                        onIncomingMessage(mChatManager.getNewestMessage());
                        break;
                    case ActionEnum.ACTION_OPEN_INCOMING_CALL_FROM_PUSH:
                        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---StartSTART");
                        // mAuthenManager.notifyLoginSuccess();
                        registerWithoutlogin = true;
                        break;
                    case ActionEnum.ACTION_GET_CONTACT_INFO_FROM_SERVER:
                        String serverContactId = extras.getString(INTENT_PARAM_SERVER_ID, "-1");
                        LogUtils.d("", "onMessageReceived QilexService::SERVER ID=" + serverContactId);
                        addPushRefreshContact(serverContactId);
                        break;

                    default:
                        break;
                }
            }
        }
        return START_STICKY;
    }

    @Override
    public synchronized void onDestroy() {
        LogUtils.d("", "onMessageReceived onDestroy 1");
        if (!Utils.isInKatzMode()) {
            unregisterReceiver(mSimCardChangeRegister);
        }
        unregisterReceiver(mReloginReceiver);

        // Restart service
        if (SharedPrefrerenceFactory.getInstance().getLoggoutStatus() == false) {
            sendBroadcast(new Intent(QilexServiceRestartReceiver.ACTION_QILEX_SERVICE_RESTART));
        }
        super.onDestroy();
        // Set instant
        mInstance = null;
        // Unregister all signed listeners
        mAuthenManager.unregisterQilexLoginListener(this);
        mAuthenManager.unregisterServiceRegistrationListener(this);
        // mPresenceService.removeSipPresenceListener(this);
        mCallLogManager.removeCallLogChangeListener(this);
        NetworkStateMonitor.removeListener(this);
        mChatManager.removeSipChatListener(this);
        if (mNotificationReceiver != null) {
            unregisterReceiver(mNotificationReceiver);
        }
        if (mScreenOnOffReceiver != null) {
            unregisterReceiver(mScreenOnOffReceiver);
        }

        mCallReceiver.unregisterReceiver();
        unregisterReceiver(mPostPaymentReceiver);
        mNotificationFactory.destroy();
        cancelReLoginTasks();
        mContactManager.unregisterContentObserver();
        cancelCheckSendingMessageTask();

        unlockCPU();
        LogUtils.e("NAMND", "Stop Service");
        LogUtils.d("", "onMessageReceived onDestroy");
    }

    public static Vibrator getVibratorService() {
        return mVibrationService;
    }

    public static AudioManager getAudioManager() {
        return mAudioManager;
    }

    public QilexNotificationFactory getNotificationFactory() {
        return this.mNotificationFactory;
    }

    public void setAudioManagerSpeakerOn(boolean flag) {
        mAudioManager.setSpeakerphoneOn(flag);
    }

    public boolean isAudioManagerSpeakerOn() {
        return mAudioManager.isSpeakerphoneOn();
    }

    /**
     * Show calling activity.
     */
    public void showOutgoingSipCallGUI(String realNumber, boolean out) {
        // TODO: Show calling activity
        Intent i = new Intent(this, InCallActivity.class);
        i.putExtra(InCallActivity.INTENT_NO_PHONE_PARTNER, realNumber);
        i.putExtra(InCallActivity.INTENT_CALL_OUT, out);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void notifyOutgoingCallEnd(int errorCode) {
        Intent intent = new Intent(InCallActivity.ACTION_ENDCALL);
        intent.putExtra(InCallActivity.INTENT_ERROR_CODE, errorCode);
        sendBroadcast(intent);
    }

    /**
     * Show incoming call activity.
     */
    public void showIncomingSipCallGUI(QilexSipProfile peerProfile) {
        // TODO: Show incoming call activity
        Intent i = new Intent(this, IncomingCallActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("NAME", peerProfile.getLogin());
        startActivity(i);
    }

    public void showIncomingSipCallGUIFromPush(String peerNumber) {
        Intent i = new Intent(this, IncomingCallActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("NAME", peerNumber);
        i.putExtra(IncomingCallActivity.INTENT_PARAM_IS_PUSH, true);
        startActivity(i);
    }

    public void userLogout() {
        // Unregister on SIP server
        mAuthenManager.logout();
        SharedPrefrerenceFactory.getInstance().setLogoutStatus(true);
        UserInfo.getInstance().setPushToken("");
        // Stop Foreground
        cancelReLoginTasks();
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopSelf();
            }
        }, 500);
    }

    public void userDeactivate(boolean isSelfDeactivate, boolean clearCachePhoneNo) {
        // Clear login cache data
        if (clearCachePhoneNo) {
            UserInfo.getInstance().mCachePhoneNo = Const.STR_EMPTY;
        }
        UserInfo.getInstance().clearInfoAndSave();
        mAuthenManager.logout();

        // Clear CallLog, Chat Log
        CallLogDb.getInstance().clearDB();
        ChatManager.getInstance().clearChatHistory();
        SharedPrefrerenceFactory.getInstance().setMissCallNumToZero();
        mNotificationFactory.destroy();

        // Unregister on SIP server
        mAuthenManager.deactivate();

        // Stop Foreground
        cancelReLoginTasks();

        // Send Broadcast to opening Dialog Activity
        if (clearCachePhoneNo == true) {
            Intent intentBroadcast = new Intent(BaseActivity.BROADCAST_ACTION_LOGOUT);
            intentBroadcast.putExtra(BaseActivity.BROADCAST_INTENT_IS_SELF_LOGOUT, isSelfDeactivate);
            intentBroadcast.putExtra(BaseActivity.BROADCAST_INTENT_IS_CHANGE_SIM, false);
            sendBroadcast(intentBroadcast);
        }

        // Stop service
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPrefrerenceFactory.getInstance().setLogoutStatus(true);
                stopSelf();
            }
        }, 500);
    }

    /**
     * Show alert
     * 
     * @param content
     */
    public void showLogoutAlert(int title, int content) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle(title).setMessage(content).create();

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                System.exit(0);
            }
        });
        alertDialog.show();
    }

    public void setCurrentChattingPhoneNo(String phoneNo) {
        mCurrentChattingPhoneNo = phoneNo;
    }

    public void setConversationVisible(boolean isConversationVisible) {
        this.isConversationVisible = isConversationVisible;
    }

    private void lockWifi() {
        WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        if (wifiLock != null) {
            if (!wifiLock.isHeld()) {
                wifiLock.acquire();
                LogUtils.d("sigma", "Wifi Locked");
            }
        } else {
            wifiLock = wifiManager.createWifiLock("Katz");
            wifiLock.acquire();
            LogUtils.d("sigma", "Wifi Locked");
        }
    }

    private void unlockWifi() {
        if (wifiLock != null && wifiLock.isHeld()) {
            wifiLock.release();
            LogUtils.d("sigma", "Wifi unlocked");
        }
    }

    private void lockCPU() {
        if (wakeLock != null) {
            if (!wakeLock.isHeld()) {
                wakeLock.acquire();
            }
        } else {
            PowerManager powerManager = (PowerManager)getSystemService(POWER_SERVICE);
            wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Katz");
            wakeLock.acquire();
        }
    }

    private void unlockCPU() {
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }
    }

    // /
    // / Implement methods of QilexServiceRegistrationListener START
    // /
    @Override
    public void onQilexServiceRegitrationStarted(QilexProfile localUserProfile) {
        logE("QilexService::onQilexServiceRegitration----Start");
        isRegisteredSIP = true;
    }

    @Override
    public void onQilexServiceRegitrationDone(QilexProfile localUserProfile) {
        logE("onMessageReceived QilexService::onQilexServiceRegitration---DONE");
        isRegisteredSIP = true;
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                if (DEBUG)
                    startForeground(1, mNotificationFactory.createServiceActive());
            }
        });
    }

    @Override
    public void onQilexServiceRegitrationError(QilexProfile localUserProfile, final int reason, final String msg) {
        logE("QilexService::onQilexServiceRegitrationError");
        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---ERROR");
        isRegisteredSIP = false;
        if (registerWithoutlogin == true) {
            registerWithoutlogin = false;
            scheduleLogin(100);
        }
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                if (DEBUG)
                    startForeground(1, mNotificationFactory.createServiceInactiveNetworkDown());
            }
        });
    }

    // /
    // / Implement methods of QilexServiceRegistrationListener END
    // /

    // /
    // / Implement methods of SipPresenceListener START
    // /
    @Override
    public void onStatusUpdate(String uri, Status status) {
    }

    public void checkForUnsuccessPayment() {
        PaymentModel payment = mAuthenManager.getResendPayment();
        if (payment == null) {
            LogUtils.d("PAYMENT", "PAYMENT CHECK NONE");
            return;
        }

        // Listener
        PostPaymentListener paymentListener = new PostPaymentListener() {
            @Override
            public void onPostPaymentFinish(boolean isSuccess, ErrorModel error) {
                if (isSuccess == true) {
                    postHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mNotificationFactory.showPaymentSuccess();
                            checkForUnsuccessPayment();
                        }
                    });
                }
            }
        };

        // Post payment
        String base64Payment = payment.base64Payment;
        String googlePlayProductId = payment.googlePlayProductId;
        String userData = payment.userData;
        mAuthenManager.postAccountPaymentInfo(this, UserInfo.getInstance().getPhoneNumberFormatted(), base64Payment,
                Const.STR_EMPTY, userData, googlePlayProductId, Const.STR_EMPTY, paymentListener);
    }

    @Override
    public void onNetworkStateChanged(boolean init, int networkType) {
        QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
        if (localProfile != null) {
            // TODO: Check login/logout like Qilex Code
        }
        if (networkType == NetworkStateMonitor.WIFI) {
            lockWifi();
        } else {
            unlockWifi();
        }
        switch (networkType) {
            case NetworkStateMonitor.NET_STATE_UNKNOWN:
            case NetworkStateMonitor.NO_NETWORK:
            case NetworkStateMonitor.MOBILE_INSUFFICIENT:
                logE("onNetworkStateChanged = NO_NETWORK");
                break;
            case NetworkStateMonitor.WIFI:
            case NetworkStateMonitor.MOBILE_SUFFICIENT:
                if (init) {
                    checkForUnsuccessPayment();
                    if (registerWithoutlogin == true) {
                        mAuthenManager.notifyLoginSuccess();
                    } else {
                        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---Start Login");
                        scheduleLogin(100);
                    }
                } else {
                    if (mCallManager.isCallInProgress()) {
                        mCallManager.setJustLoginSIP(true);
                    } else {
                        LogUtils.d("", "onMessageReceived QilexService::onQilexServiceRegitration---Start Login");
                        scheduleLogin(100);// after 100ms
                    }
                }
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("unused")
    private void restartSIPStack() {
        if (mAuthenManager.isLoggedIn()) {
            if (!isRegisteredSIP) {
                mAuthenManager.registerSip(UserInfo.getInstance().getLocalProfile(), true);
            }
        } else {
            scheduleLogin(100);// after 100ms
        }
        mCallManager.setJustLoginSIP(false);
    }

    BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean clear = intent.getBooleanExtra("notification_action_clear", false);
            if (clear) {
                if (mNotificationFactory != null)
                    mNotificationFactory.clearIncomMessageNotification();
            } else if (!MainTabActivity.active && mAuthenManager.isLoggedIn()) {
                intent.setClass(context.getApplicationContext(), MainTabActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                int tabIndex = intent.getIntExtra(MainTabActivity.BUNDLE_TAB_START,
                        MainTabActivity.mStartDefaultTabIndex);
                MainTabActivity.mStartDefaultTabIndex = tabIndex;
                context.startActivity(intent);
            } else {
                intent.setAction(MainTabActivity.BROADCAST_SELECT_TAB);
                context.sendBroadcast(intent);
            }
        }

    };

    // //
    // // Implement methods of SipChatListener - START
    // //
    @Override
    public void onMessageSent(ChatMessageModel msg) {

    }

    @Override
    public void onMessageConfirmed(ChatMessageModel msg) {

    }

    @Override
    public void onMessageFailure(ChatMessageModel msg, int errorCode) {

    }

    @Override
    public void onIncomingMessage(final ChatMessageModel msg) {
        // If there is chat screen showing, don't show incoming dialog and don't
        // notify
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                QilexContact contact = mContactManager.getContactByPhoneNo(QilexService.this, msg.contactStr);
                String contactName = msg.contactStr;

                // Update contact phone number to katz
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mContactManager.setPhoneNumberKATZToContacts(msg.contactStr, null, true);
                    }
                }).start();
                // Change contact to SIP:
                if (contact != null) {
                    contactName = contact.getDisplayName();
                } else {
                    TempContactModel tempContact = mContactManager.getTempContact(msg.contactStr);
                    if (tempContact != null) {
                        contactName = tempContact.name;
                    } else {
                        mContactManager.loadContactInfo(QilexService.this, msg.contactStr,
                                new GetContactTempByPhoneListener() {
                            @Override
                            public void onGetContactFinish(final TempContactModel model, final ErrorModel error) {
                            }
                        });
                    }
                }

                // Show Notification
                Bundle bundle = new Bundle();
                bundle.putLong("qilex_thread_id", msg.threadId);
                String[] args = new String[] {
                        contactName, msg.contactStr, msg.getDescription(), Integer.toString(msg.mineType), msg.getThreadUuid()
                };
                bundle.putStringArray("qilex_args", args);
                showIncomingNotify2(msg, bundle, msg.threadId, msg.getDescription(), msg.contactStr);
            }
        });
    }

    private void showIncomingNotify2(ChatMessageModel msg, Bundle bundle, long threadId, String content,
            String... phoneNumbers) {
        boolean isShowNotification = false;
        boolean isShowDialog = false;

        // If Conversation Screen is not on top, show notification
        if (isConversationVisible == false) {
            isShowNotification = true;
            // If mCurrentChattingPhoneNo is blank so Conversation Screen is not
            // CREATED
            if (Utils.isStringNullOrEmpty(mCurrentChattingPhoneNo) == true) {
                isShowDialog = true;
            } else if (mCurrentChattingPhoneNo.equals(msg.getThreadUuid()) == false) {
                isShowDialog = true;
            } else {
                isShowDialog = false;
            }
        } else {
            isShowDialog = false;
            if (mCurrentChattingPhoneNo.equals(msg.getThreadUuid()) == true) {
                isShowNotification = false;
            } else {
                isShowNotification = true;
            }
        }

        if (isShowNotification) {
        	if (msg.isGroupChat()) {
                mNotificationFactory.showGroupChatStatusNotification(msg.groupUuid, content);
                isShowDialog = false;
        	} else {
                mNotificationFactory.showIncomMessageNotification2(threadId, content, phoneNumbers);
        	}
        }

        if (isShowDialog) {
            showInCommingMessage(bundle);
        }

        if (isShowNotification || isShowDialog) {
            mQilexSoundService.playMessageNotificationTone();
        }
    }

    // // Implement methods of SipChatListener - END

    /**
     * showInCommingMessage
     * 
     * @param bundle
     */
    public void showInCommingMessage(Bundle bundle) {
        if (!IncomingMessageActivity.active) {
            Intent it = new Intent(getApplicationContext(), IncomingMessageActivity.class);
            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (!IActivity.isActive) {
                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
            it.putExtras(bundle);
            startActivity(it);
        } else {
            Intent irecv = new Intent(IncomingMessageActivity.INTENT_RECV_MESSAGE);
            irecv.putExtras(bundle);
            sendBroadcast(irecv);
        }
    }

    public void clearIncomMessageNotification() {
        mNotificationFactory.clearIncomMessageNotification();
    }

    /**
     * Show Toast from a post handler.
     * 
     * @param message display content.
     */
    public void showToastOnPostHandler(final String message) {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(QilexService.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onNewCallLog(CallLogModel callLog) {
        // Miss call notification
        if (callLog.getType() == CallLogModel.TYPE_MISSCALL) {
            showMissCallNotification();
        }
    }

    @Override
    public void onCallLogChange(CallLogModel callLog) {
        // Miss call notification
        if (callLog.getType() == CallLogModel.TYPE_MISSCALL) {
            showMissCallNotification();
        }
    }

    @Override
    public void onCallLogDeleted(long[] callLogIds) {
    }

    private void showMissCallNotification() {
        ArrayList<String> listPhoneNo = mCallLogManager.getMissCallPhoneNumbers(mCallLogManager.countMissCall());
        mNotificationFactory.showMissCallNotification(mCallLogManager.countMissCall(), listPhoneNo);
    }

    @Override
    public void onLoginSuccess(LoginResponseModel model) {
        isRegisteredSIP = false;
        SharedPrefrerenceFactory.getInstance().setLogoutStatus(false);

        // Update user info
        UserInfo userInfo = UserInfo.getInstance();
        userInfo.setAvatarUrl(model.user_avatar_uri);
        userInfo.setFirstName(model.user_name);
        userInfo.setLastName(model.user_last_name);
        userInfo.saveInfo();

        // Start register
        // showToastOnPostHandler("LOGIN SUCCESS");
        startRegister();
        scheduleLogin(model.refresh_period * 1000); // 1 day

        // Get contact list if is Slave mode
        if (Utils.isInMasterMode() == false) {
            mContactManager.reloadContactFromServer();
        }
    }

    @Override
    public void onLoginFail(int errorCode, String errorMessage) {
        if (errorCode == 126 || errorCode == 127) {
            getNotificationFactory().showAppLogoutNotification();
            userDeactivate(true, true);
        } else if (!Utils.isInKatzMode() && errorCode == ErrorModel.CODE_MISI_CHANGE) {
            userLogout();
        } else {
            scheduleLogin(10000); // 10s
        }
    }

    public void scheduleLogin(int delay) {
        mCallManager.setJustLoginSIP(false);
        cancelReLoginTasks();
        setupReLoginTask(delay);
    }

    private synchronized void setupReLoginTask(int delay) {
        QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
        if (localProfile == null) {
            return;
        }
        // bookmark
        mCurrReLoginTask = new ReLoginAttemptTask();
        mReLoginTasksTimer.schedule(mCurrReLoginTask, delay);
    }

    private synchronized void cancelReLoginTasks() {
        QilexProfile localProfile = UserInfo.getInstance().getLocalProfile();
        if (localProfile != null && mCurrReLoginTask != null) {
            mCurrReLoginTask.cancel();
            mCurrReLoginTask = null;
            mReLoginTasksTimer.purge();
        }
    }

    private synchronized void setupCheckSendingMessageTask(int delay) {
        mCheckSendingMessageTimerTask = new CheckFailMessageTask();
        mCheckSendingMessageTimer.schedule(mCheckSendingMessageTimerTask, delay,
                Const.Config.TIME_MILLIS_MESSAGE_TIMEOUT);
    }

    private synchronized void cancelCheckSendingMessageTask() {
        if (mCheckSendingMessageTimerTask != null) {
            mCheckSendingMessageTimerTask.cancel();
            mCheckSendingMessageTimerTask = null;
            mCheckSendingMessageTimer.purge();
        }
    }

    private void logE(String message) {
        if (DEBUG)
            LogUtils.e("NAMND", message);
    }

    private void startRegister() {
        logE("start Register");
        // Save UserInfo
        UserInfo.getInstance().saveInfo();
        // Create Sync account
        mContactManager.addNewAccount();

        UserInfo.getInstance().setPushToken(SharedPrefrerenceFactory.getInstance().getRegistrationId());
        mAuthenManager.registerSip(UserInfo.getInstance().getLocalProfile(), false);
    }

    private void reActivationPhone() {
        if (UserInfo.getInstance().isActive()) {
            // Call deactivate account
            userDeactivate(false, false);
            SharedPrefrerenceFactory.getInstance().clearCacheImsi();

            // Show system dialog
            AlertDialog alertDialog = new AlertDialog.Builder(QilexService.this)
                    .setTitle(getString(R.string.sim_is_changed_title))
                    .setMessage(getString(R.string.sim_is_changed_content))
                    .setIcon(R.drawable.ic_launcher_green)
                    .setCancelable(false)
                    .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AppManager.getAppManager().appExit(QilexService.this);
                        }
                    })
                    .create();
            alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            alertDialog.show();
        }
    }

    BroadcastReceiver receiverSms = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            try{
                if (bundle != null){
                    Object[] pdusObj = (Object[])bundle.get("pdus");
                    for (int i =0 ; i < pdusObj.length;i++){
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[])pdusObj[i]);
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                        String contentSms = currentMessage.getDisplayMessageBody();
                        String numberActivate = Utils.extractNumber(contentSms);
                        unregisterReceiver(receiverSms);
                        mAuthenManager.sendActivationCode(
                                UserInfo.getInstance().getPhoneNumberFormatted(),
                                numberActivate,
                                new SendActivationListener() {
                                    @Override
                                    public void onSendActivationFinish(boolean isSuccess, boolean isFrifon, ErrorModel error) {
                                        if (isSuccess) {

                                        } else if (error.getByErrorCode(ErrorModel.CODE_INCORRECT_TOKEN) != null) {
                                            reActivationPhone();
                                        }
                                    }
                                });
                    }
                }
            }catch (Exception e){
                LogUtils.e("TAG",e.toString());
            }
        }
    };

    private Timer mReLoginTasksTimer = new Timer();

    private TimerTask mCurrReLoginTask;

    private class ReLoginAttemptTask extends TimerTask {
        @Override
        public void run() {
            try {
                if (UserInfo.getInstance().hasActiveCode()) {
                    if (!Utils.isInKatzMode() && SimUtils.checkIMSIchangeManual()) {
                        reActivationPhone();
                        return;
                    }
                    mAuthenManager.loginWithPhoneNumber(UserInfo.getInstance().getPhoneNumberFormatted());
                }
            } catch (Exception e) {
                e.printStackTrace();
                // TODO - call login failed handler.
                // Better to catch all exceptions here. Unhandled exception will
                // stop Timer process silently, so automatic relogin will stop
                // working.
            }
        }

    }

    private class CheckFailMessageTask extends TimerTask {
        @Override
        public void run() {
            long currentTimeMillis = Calendar.getInstance().getTimeInMillis();
            long timeOutCheck = currentTimeMillis - Const.Config.TIME_MILLIS_MESSAGE_TIMEOUT;
            mChatManager.updateSendingMessageToError(timeOutCheck);
        }
    }

    public static interface ActionEnum {
        public int ACTION_OPEN_INCOMING_CALL = 1;

        public int ACTION_OPEN_INCOMING_MESSAGE = 2;

        public int ACTION_OPEN_INCOMING_CALL_FROM_PUSH = 3;

        public int ACTION_OPEN_INCOMING_MESSAGE_FROM_PUSH = 4;

        public int ACTION_GET_CONTACT_INFO_FROM_SERVER = 5;

        public int ACTION_RESTART = -10;
    }

    public interface LogoutListener {
        public void onLogoutFinish();
    }

    // **** Check contact push from server
    private LinkedList<UpdateContactEntryListener> mIncommingRefreshContactEvents = new LinkedList<>();

    UpdateContactEntryListener processingPushingContactEntry;

    private synchronized void addPushRefreshContact(String serverId) {
        if (serverId != null) {
            synchronized (mIncommingRefreshContactEvents) {
                mIncommingRefreshContactEvents.add(new UpdateContactEntryListener(Long.parseLong(serverId)));
            }
        }
        if (processingPushingContactEntry == null && mIncommingRefreshContactEvents.isEmpty() == false) {
            synchronized (mIncommingRefreshContactEvents) {
                processingPushingContactEntry = mIncommingRefreshContactEvents.poll();
            }
            mContactManager.addFirstSyncListener(processingPushingContactEntry);
            mContactManager.reloadAllContact();
        }
    }

    private class UpdateContactEntryListener implements NotifyCacheContactChangeListener {
        long serverContactId;

        public UpdateContactEntryListener(long serverContactId) {
            this.serverContactId = serverContactId;
            LogUtils.d("contactEntryUpdate", "contactEntryUpdate update Init");
        }

        @Override
        public void onCacheContactChange() {
            mContactManager.removeFirstSyncListener(this);
            LogUtils.d("contactEntryUpdate", "contactEntryUpdate update onCacheContactChange");
            updateContactByEntry(serverContactId);
        }

        private void updateContactByEntry(long serverContactId) {
            mContactManager.getContactEntry(serverContactId, new GetContactSnapshotListener() {
                @Override
                public void onGetContactSnapshotFinish(ErrorModel errors,
                        ArrayList<ContactSnapshotModel> listContactSnapshot) {
                    LogUtils.d("contactEntryUpdate", "contactEntryUpdate update updateContactByEntry FINISH");
                    if (errors == null && listContactSnapshot != null && listContactSnapshot.isEmpty() == false) {
                        final ContactSnapshotModel snapshot = listContactSnapshot.get(0);

                        if (snapshot.frifon_user == true) {
                            // Update phone number
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    mContactManager.setPhoneNumberKATZToContacts(snapshot.frifon_msisdn,
                                            snapshot.avatar_uri, true);
                                    LogUtils.d("contactEntryUpdate",
                                            "contactEntryUpdate update updateContactByEntry FINISH 2");
                                    processingPushingContactEntry = null;
                                    addPushRefreshContact(null);
                                }
                            }).start();
                            // Show notification
                            final String contactLookUpKey;
                            final String avatarUrl;
                            final String name = snapshot.first_name + " " + snapshot.last_name;
                            final long contactId;
                            QilexContact contact = mContactManager.getContactByPhoneNo(MyApplication.getAppContext(),
                                    snapshot.frifon_msisdn);
                            if (contact != null) {
                                contactLookUpKey = contact.lookUpKey;
                                contactId = contact.id;
                                if (Utils.isStringNullOrEmpty(snapshot.avatar_uri)) {
                                    avatarUrl = contact.photoUri;
                                } else {
                                    avatarUrl = snapshot.avatar_uri;
                                }
                                // Show Notification
                                postHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mNotificationFactory.showNewKatzNotification(name, avatarUrl, contactLookUpKey, contactId);
                                    }
                                });
                            }
                        } else {
                            final ArrayList<PhoneNumberModel> listPhone = snapshot.listPhoneNo;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    for (PhoneNumberModel phoneNumberModel : listPhone) {
                                        mContactManager.setPhoneNumberKATZToContacts(phoneNumberModel.phoneNumber,
                                                Const.STR_EMPTY, false);
                                    }
                                    LogUtils.d("contactEntryUpdate",
                                            "contactEntryUpdate update updateContactByEntry FINISH 3");
                                    processingPushingContactEntry = null;
                                    addPushRefreshContact(null);
                                }
                            }).start();
                        }
                    } else {
                        LogUtils.d("contactEntryUpdate", "contactEntryUpdate update updateContactByEntry FINISH 4");
                        processingPushingContactEntry = null;
                        addPushRefreshContact(null);
                    }
                }
            });
        }
    }
    // **** Check contact push from server - END

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }
}
