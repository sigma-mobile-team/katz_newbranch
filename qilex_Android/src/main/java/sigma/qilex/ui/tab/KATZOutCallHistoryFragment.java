
package sigma.qilex.ui.tab;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.OutCallListHistoryModel;
import sigma.qilex.dataaccess.model.http.OutCallListHistoryModel.OutCallModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.manager.phone.CallManager.GetKATZOutCallHistoryListener;
import sigma.qilex.ui.adapter.KATZOutHistoryAdapter;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.Utils;

/**
 * Fragment for test
 */
public class KATZOutCallHistoryFragment extends BaseFragment implements GetKATZOutCallHistoryListener {

    private static final int ITEM_PER_PAGE = 10;

    ListView mListFrifon;

    ProgressBar mProgressBar;

    MyCustomTextView tvNoItem;

    ArrayList<OutCallModel> mArrFrifon;

    KATZOutHistoryAdapter mFrifonAdapter;

    CallManager mCallManager;

    private int currentPage = 1;

    private int loadingId = -1;

    private Runnable loadMoreRunnable = new Runnable() {
        @Override
        public void run() {
            getHistory();
        }
    };

    public KATZOutCallHistoryFragment() {
        mCallManager = CallManager.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frifon_out_list_fragment, container, false);
        mListFrifon = (ListView)rootView.findViewById(R.id.list_frifon_out);
        mProgressBar = (ProgressBar)rootView.findViewById(R.id.loading);
        tvNoItem = (MyCustomTextView)rootView.findViewById(R.id.txtNoItem);
        mProgressBar.setVisibility(View.VISIBLE);
        tvNoItem.setVisibility(View.GONE);

        if (mFrifonAdapter != null) {
            mListFrifon.setAdapter(mFrifonAdapter);
        } else {
            mFrifonAdapter = new KATZOutHistoryAdapter(mBaseActivity, new ArrayList<OutCallModel>(), loadMoreRunnable);
            mListFrifon.setAdapter(mFrifonAdapter);
        }

        // currentPage = 1;
        getHistory();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBaseActivity.setActionbarTitle(Utils.getString(getActivity(), R.attr.stringKatzOutCallHistoryTitle));
        mBaseActivity.showBackAction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        QilexHttpRequest.stopRequest(loadingId);
    }

    public void getHistory() {
        loadingId = mCallManager.getKATZOutCallHistory(currentPage, ITEM_PER_PAGE, this);
    }

    @Override
    public void onGetKATZOutCallFinish(final OutCallListHistoryModel result, ErrorModel error) {
        if (error == null) {
            mBaseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(View.GONE);

                    if (result.out_calls == null) {
                        return;
                    }

                    // Create list items
                    mArrFrifon = new ArrayList<OutCallModel>();
                    for (OutCallModel outCallModel : result.out_calls) {
                        mArrFrifon.add(outCallModel);
                    }

                    // If result is empty
                    if (mArrFrifon.isEmpty()) {
                        // Show icon NO ITEM if there is no item
                        if (currentPage == 1) {
                            tvNoItem.setVisibility(View.VISIBLE);
                        }
                        // If list blank, set adapter to load end
                        if (mFrifonAdapter != null) {
                            mFrifonAdapter.setLoadEnd(true);
                            mFrifonAdapter.notifyDataSetChanged();
                        }
                        return;
                    } else {
                        // if item is not 10, set adapter to load end
                        tvNoItem.setVisibility(View.GONE);
                    }

                    // Bind data to screen
                    if (mFrifonAdapter == null) {
                        mFrifonAdapter = new KATZOutHistoryAdapter(mBaseActivity, mArrFrifon, loadMoreRunnable);
                        // saveAdapterListCache();
                        if (mArrFrifon.size() < ITEM_PER_PAGE) {
                            mFrifonAdapter.setLoadEnd(true);
                        }
                        mListFrifon.setAdapter(mFrifonAdapter);
                        currentPage++;
                    } else {
                        mFrifonAdapter.addSourceToList(mArrFrifon);
                        if (mArrFrifon.size() < ITEM_PER_PAGE) {
                            mFrifonAdapter.setLoadEnd(true);
                        }
                        currentPage++;
                    }
                    mFrifonAdapter.notifyDataSetChanged();
                }
            });
        } else {
            mBaseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(View.GONE);
                    mBaseActivity.showAlert(R.string.error, R.string.getting_list_katz_out_fail);
                    if (mFrifonAdapter == null || mFrifonAdapter.isEmpty())
                        tvNoItem.setVisibility(View.VISIBLE);
                    else {
                        tvNoItem.setVisibility(View.GONE);
                    }
                }
            });
        }
    }
}
