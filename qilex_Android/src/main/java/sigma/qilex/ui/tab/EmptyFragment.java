
package sigma.qilex.ui.tab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import pl.frifon.aero2.R;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.customview.BaseFragment;

/**
 * Fragment for test
 * 
 * @author Mr_Invisible
 */
public class EmptyFragment extends BaseFragment {
    TextView content;

    String tabID = "empty";

    String contentTxt = "empty";

    public EmptyFragment(String tabId, String contentTxt) {
        this.contentTxt = contentTxt;
        this.tabID = tabId;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBaseActivity.setBackAction(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.empty_fragment, container, false);
        content = (TextView)rootView.findViewById(R.id.text_empt);
        content.setText(contentTxt);
        content.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                enterNextFragment();
            }
        });
        return rootView;
    }

    private void enterNextFragment() {
        int i = 0;
        String contents = contentTxt;
        if (contentTxt.contains("-")) {
            String[] s = contentTxt.split("-");
            int a = Integer.parseInt(s[1].trim());
            i = a + 1;
            contents = s[0] + "- " + i;
            if (i >= 10) {
                Toast.makeText(getActivity(), "Max stack, can not add over 10 fragments!", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            contents = contentTxt + " - 0";
        }
        if (mBaseActivity instanceof MainTabActivity)
            ((MainTabActivity)mBaseActivity).pushFragments(tabID, new EmptyFragment(tabID, contents), true, true);
    }
}
