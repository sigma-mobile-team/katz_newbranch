
package sigma.qilex.ui.tab;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.util.Util;

import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.dataaccess.network.http.UserActivationUtils;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.CustomButton;
import sigma.qilex.ui.customview.MyCustomEditText;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;

public class SupportFragment extends BaseFragment implements OnClickListener {

    private LinearLayout llProblemType;

    private MyCustomEditText tvInputContent;

    private MyCustomEditText tvInputEmail;

    private CustomButton btnSend;

    private TextView tvType;

    private String subjectMail;

    String[] strProblemTypes;

    int mSendingHttpRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.support_fragment, container, false);
        strProblemTypes = mBaseActivity.getResources().getStringArray(
                Utils.isInKatzMode() ? R.array.support_types : R.array.support_types_green);
        findViews(rootView);
        bindData();
        tvInputContent.addTextChangedListener(mTextWatcher);
        return rootView;
    }

    TextWatcher mTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            checkSend();
        }
    };

    private void bindData() {
        String email = SharedPrefrerenceFactory.getInstance().getEmailFb();
        if (email != null) {
            tvInputEmail.setText(email);
        }
        checkSend();
        tvInputEmail.addTextChangedListener(mTextWatcher);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBaseActivity.setActionbarTitle(R.string.support);
        mBaseActivity.showBackAction();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void findViews(View rootView) {
        llProblemType = (LinearLayout)rootView.findViewById(R.id.ll_problem_type);
        tvInputContent = (MyCustomEditText)rootView.findViewById(R.id.tv_input_content);
        tvInputEmail = (MyCustomEditText)rootView.findViewById(R.id.tv_input_email);
        tvType = (TextView)rootView.findViewById(R.id.tvType);
        btnSend = (CustomButton)rootView.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        llProblemType.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnSend) {
            String email = tvInputEmail.getText().toString();
            if (!Utils.isEmailValid(email)) {
                mBaseActivity.showAlert(R.string.failed, R.string.invalid_email);
                return;
            }
            String content = tvInputContent.getEditableText().toString();
            Issue issue = new Issue(email, content, subjectMail);
            String body = issue.toJSON();
            mSendingHttpRequest = UserActivationUtils
                    .sendIssue(UserInfo.getInstance().getPhoneNumberFormatted(), body, listener).getRequestId();
            mBaseActivity.showSimpleProgressByPostHandler(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    QilexHttpRequest.stopRequest(mSendingHttpRequest);
                }
            });
        } else if (v == llProblemType) {
            if (Utils.isInKatzMode()) {
                showTypeDl();
            } else {
                showTypeDlGreen();
            }
        }
    }

    private void checkSend() {
        String content = tvInputContent.getEditableText().toString();
        String email = tvInputEmail.getText().toString();
        boolean hasNotSend = TextUtils.isEmpty(content) || TextUtils.isEmpty(subjectMail) || TextUtils.isEmpty(email);
        btnSend.setEnabled(!hasNotSend);
    }

    private void sendEmailDone(final int messageId) {
        mBaseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBaseActivity.hideSimpleProgressByPostHandler();
                Toast.makeText(mBaseActivity, messageId, Toast.LENGTH_SHORT).show();
                ((MainTabActivity)mBaseActivity).popFragments();
            }
        });
    }

    QilexHttpRequest.QilexHttpRequestListener listener = new QilexHttpRequest.QilexHttpRequestListener() {
        @Override
        public void httpRequestStateChanged(QilexHttpRequest req) {
            switch (req.getState()) {
                case QilexHttpRequest.STATE_COMPETED:
                    sendEmailDone(R.string.send_support_done);
                    break;
                case QilexHttpRequest.STATE_ERROR_DATE:
                case QilexHttpRequest.STATE_ERROR:
                case QilexHttpRequest.STATE_ERROR_TIMEOUT:
                    mBaseActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBaseActivity.showAlert(R.string.error, R.string.send_support_fail);
                        }
                    });
                    break;
            }
        }
    };

    private void setTextType(int index) {
        subjectMail = strProblemTypes[index];
        tvType.setText(subjectMail);
        checkSend();
    }

    private void showTypeDl() {
        OnClickListener listeners[] = {
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // regis
                        setTextType(0);
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // call
                        setTextType(1);
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // chat
                        setTextType(2);
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // payment
                        setTextType(3);
                    }
                }, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // contact
                        setTextType(4);
                    }
                }
        };

        Utils.showBasicSelectionDialog(getActivity(), strProblemTypes, listeners, true);
    }

    private void showTypeDlGreen() {
        OnClickListener listeners[] = {
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // regis
                        setTextType(0);
                    }
                }, new OnClickListener() {

            @Override
            public void onClick(View v) {
                // call
                setTextType(1);
            }
        }, new OnClickListener() {

            @Override
            public void onClick(View v) {
                // chat
                setTextType(2);
            }
        }, new OnClickListener() {

            @Override
            public void onClick(View v) {
                // payment
                setTextType(3);
            }
        }
        };

        Utils.showBasicSelectionDialog(getActivity(), strProblemTypes, listeners, true);
    }

    /**
     * issue_info: information from user about the problem. issue_type: issue
     * type, valid types: Register, Connection, Messsage, Payment, Contacts in
     * Polish (Rejestracja, Połączenia, Wiadomość, Płatność, Kontakty)
     */
    class Issue {
        public String email;

        public String info;

        public String type;

        public Issue(String email, String info, String type) {
            this.email = email;
            this.info = info;
            this.type = type;
        }

        public String toJSON() {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_email", email);
                jsonObject.put("issue_info", info);
                jsonObject.put("issue_type", type);
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
                return "";
            }

        }
    }
}
