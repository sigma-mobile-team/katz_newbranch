
package sigma.qilex.ui.tab;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import pl.frifon.aero2.R;
import pl.katz.aero2.Const;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.Utils;

/**
 * Fragment for test
 * 
 * @author Mr_Invisible
 */
public class AccountSettingsFragment extends BaseFragment implements OnClickListener {

    LinearLayout mDeactiveLayout;

    LinearLayout mRegulaminKATZOut;

    LinearLayout mCennik;

    LinearLayout mRegulaminKATZ;

    LinearLayout mEULA;

    LinearLayout mPrivacy;

    public AccountSettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.account_settings_fragment, container, false);
        mDeactiveLayout = (LinearLayout)rootView.findViewById(R.id.layout_deactive);
        mRegulaminKATZOut = (LinearLayout)rootView.findViewById(R.id.layout_regulamin_katz_out);
        mCennik = (LinearLayout)rootView.findViewById(R.id.layout_cennik);
        mRegulaminKATZ = (LinearLayout)rootView.findViewById(R.id.layout_regulamin_katz);
        mEULA = (LinearLayout)rootView.findViewById(R.id.layout_eula);
        mPrivacy = (LinearLayout)rootView.findViewById(R.id.layout_privacy);
        mDeactiveLayout.setOnClickListener(this);
        mRegulaminKATZOut.setOnClickListener(this);
        mRegulaminKATZ.setOnClickListener(this);
        mEULA.setOnClickListener(this);
        mPrivacy.setOnClickListener(this);
        mCennik.setOnClickListener(this);
        if (Const.IS_FRIFON_LITE) {
            rootView.findViewById(R.id.layout_cennik_divider).setVisibility(View.GONE);
            rootView.findViewById(R.id.layout_regulamin_katz_out_divider).setVisibility(View.GONE);
            mRegulaminKATZOut.setVisibility(View.GONE);
            mCennik.setVisibility(View.GONE);
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBaseActivity.setActionbarTitle(R.string.settings);
        mBaseActivity.showBackAction();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.layout_regulamin_katz_out:
                goIntentView(Utils.isInKatzMode() ? "katzout" : "frifon_katzout");
                break;
            case R.id.layout_regulamin_katz:
                goIntentView(Utils.isInKatzMode() ? "katzin" : "frifon_katzin");
                break;
            case R.id.layout_cennik:
                goIntentView(Utils.isInKatzMode() ? "cennik" : "frifon_cennik");
                break;
            case R.id.layout_eula:
                goIntentView(Utils.isInKatzMode() ? "eula" : "frifon_eula");
                break;
            case R.id.layout_privacy:
                goIntentView(Utils.isInKatzMode() ? "privacy" : "frifon_privacy");
                break;
            case R.id.layout_deactive:
                mBaseActivity.confirmDeactivate();
                break;
            default:
                break;
        }
    }

    private void goIntentView(String schema) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(schema);
        builder.authority(Intent.ACTION_VIEW);
        Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        startActivity(intent);
    }
}
