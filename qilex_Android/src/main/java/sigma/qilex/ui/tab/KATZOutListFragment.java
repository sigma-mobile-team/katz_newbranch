
package sigma.qilex.ui.tab;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.model.http.PurchasePrepaidModel;
import sigma.qilex.dataaccess.model.http.PurchasePrepaidModel.PurchaseModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.GetListPurchaseListener;
import sigma.qilex.ui.adapter.KATZOutAdapter;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;

/**
 * Fragment for test
 * 
 * @author Mr_Invisible
 */
public class KATZOutListFragment extends BaseFragment implements GetListPurchaseListener {

    private static final int ITEM_PER_PAGE = 10;

    private ProgressBar loadingBar;

    MyCustomTextView tvNoItem;

    ListView mListFrifon;

    KATZOutAdapter mFrifonAdapter;

    KATZOutAdapter mFrifonAdapterCache;

    private int currentPage = 1;

    private AuthenticationManager mAuthenManager;

    private int loadingId = -1;

    private SharedPrefrerenceFactory mSharePreference;

    private Runnable loadMoreRunnable = new Runnable() {
        @Override
        public void run() {
            loadingId = mAuthenManager.getUserAccountPrepaidHistory(currentPage, ITEM_PER_PAGE,
                    KATZOutListFragment.this);
        }
    };

    public KATZOutListFragment() {
        mAuthenManager = AuthenticationManager.getInstance();
        mSharePreference = SharedPrefrerenceFactory.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frifon_out_list_fragment, container, false);
        mListFrifon = (ListView)rootView.findViewById(R.id.list_frifon_out);
        loadingBar = (ProgressBar)rootView.findViewById(R.id.loading);
        tvNoItem = (MyCustomTextView)rootView.findViewById(R.id.txtNoItem);
        if (mFrifonAdapter != null) {
            mListFrifon.setAdapter(mFrifonAdapter);
        } else {
            // load cache
            ArrayList<PurchaseModel> datas = loadAdapterListCache();
            if (datas != null) {
                mFrifonAdapterCache = new KATZOutAdapter(mBaseActivity, datas, null);
                mFrifonAdapterCache.setLoadEnd(true);
                mListFrifon.setAdapter(mFrifonAdapterCache);
                mFrifonAdapterCache.notifyDataSetChanged();
            }

        }
        loadingId = mAuthenManager.getUserAccountPrepaidHistory(currentPage, ITEM_PER_PAGE, this);
        loadingBar.setVisibility(View.VISIBLE);
        tvNoItem.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_purchase_history, 0, 0);
        tvNoItem.setText(R.string.no_purchase_log);
        tvNoItem.setVisibility(View.GONE);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mBaseActivity.setActionbarTitle(R.string.katz_out_purchase_history);
        mBaseActivity.showBackAction();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        QilexHttpRequest.stopRequest(loadingId);
    }

    @Override
    public void onGetListPurchaseFinish(final PurchasePrepaidModel resultPrepaid, final ErrorModel error) {
        if (resultPrepaid != null) {
            mBaseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingBar.setVisibility(View.GONE);
                    if (resultPrepaid.prepaids == null) {
                        return;
                    }

                    // Create list items
                    ArrayList<PurchaseModel> listResult = new ArrayList<PurchaseModel>();
                    for (PurchaseModel purchaseModel : resultPrepaid.prepaids) {
                        listResult.add(purchaseModel);
                    }

                    // If result is empty
                    if (listResult.isEmpty()) {
                        // Show icon NO ITEM if there is no item
                        if (currentPage == 1) {
                            tvNoItem.setVisibility(View.VISIBLE);
                        }
                        // If list blank, set adapter to load end
                        if (mFrifonAdapter != null) {
                            mFrifonAdapter.setLoadEnd(true);
                            mFrifonAdapter.notifyDataSetChanged();
                        }
                        return;
                    } else {
                        // if item is not 10, set adapter to load end
                        tvNoItem.setVisibility(View.GONE);
                    }

                    // Bind data to screen
                    if (mFrifonAdapter == null) {
                        mFrifonAdapter = new KATZOutAdapter(mBaseActivity, listResult, loadMoreRunnable);
                        saveAdapterListCache();
                        if (listResult.size() < ITEM_PER_PAGE) {
                            mFrifonAdapter.setLoadEnd(true);
                        }
                        mListFrifon.setAdapter(mFrifonAdapter);
                        currentPage++;
                    } else {
                        mFrifonAdapter.addSourceToList(listResult);
                        if (listResult.size() < ITEM_PER_PAGE) {
                            mFrifonAdapter.setLoadEnd(true);
                        }
                        currentPage++;
                    }
                    mFrifonAdapter.notifyDataSetChanged();
                }
            });
        } else {
            mBaseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingBar.setVisibility(View.GONE);
                    mBaseActivity.showAlert(R.string.error, error.errors[0].getCurrentErrorMessage());
                    if (mFrifonAdapter == null || mFrifonAdapter.isEmpty()) {
                        tvNoItem.setVisibility(View.VISIBLE);
                    } else {
                        tvNoItem.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    private void saveAdapterListCache() {
        JSONArray jsonArray = new JSONArray();
        ArrayList<PurchaseModel> datas = mFrifonAdapter.getmArrKATZOut();
        for (PurchaseModel purchaseModel : datas) {
            jsonArray.put(purchaseModel.toJsonObject());
        }
        mSharePreference.savePurchaseHistoryCache(jsonArray.toString());
    }

    private ArrayList<PurchaseModel> loadAdapterListCache() {
        try {
            String jsonArrStr = mSharePreference.getPurchaseHistoryCache();
            if (Utils.isStringNullOrEmpty(jsonArrStr) == false) {
                JSONArray jsonArray = new JSONArray(jsonArrStr);
                int size = jsonArray.length();
                if (size == 0) {
                    return null;
                }
                ArrayList<PurchaseModel> datas = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    PurchaseModel purchaseModel = new PurchaseModel(jsonArray.getJSONObject(i));
                    datas.add(purchaseModel);
                }
                return datas;
            } else {
                return null;
            }
        } catch (JSONException e) {
            return null;
        }
    }
}
