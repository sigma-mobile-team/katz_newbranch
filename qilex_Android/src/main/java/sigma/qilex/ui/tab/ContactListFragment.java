
package sigma.qilex.ui.tab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.QilexContact;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.contact.ContactManager.ContactChangeListener;
import sigma.qilex.manager.contact.ContactManager.ContactSyncListener;
import sigma.qilex.manager.contact.ContactManager.NotifyCacheContactChangeListener;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.ContactKATZOutDetailActivity;
import sigma.qilex.ui.adapter.ContactListAdapter;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.ContactListView;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.ui.customview.SoftKeyboard;
import sigma.qilex.utils.ActivityTransitionFactory;

/**
 * Fragment ContactList
 */
public class ContactListFragment extends BaseFragment
        implements OnItemClickListener, ContactChangeListener, NotifyCacheContactChangeListener, ContactSyncListener {

    public static final String EXTRAS_DATA_SELECTED_CONTACTS = "selected_contacts";

    public static final String EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY = "EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY";

    public static final String EXTRAS_DATA_BROWSE_CONTACT_MODE = "browser_contact_mode";

    public static final String EXTRAS_DATA_BROWSE_CONTACT_TYPE_VIEW_ONLY = "browser_contact_type_view_only";

    // The loader's unique id. Loader ids are specific to the Activity or
    // Fragment in which they reside.
    public static final int VIEW_ALL = 0;

    public static final int VIEW_FRIFON = 1;

    ContactListView mListView;

    SmoothProgressBar mProgressSync;

    ContactListAdapter mContactAdapter;

    MyCustomTextView txtMyNumber;

    int translationMyNumber = 0;

    int mViewMode = VIEW_ALL;

    EditText edtSearch;

    boolean mBrowserContact = false;

    boolean mBrowserViewOnly;

    ProgressBar progressLoading;

    View mSearchContainer;

    String mNumberIntent;

    String[] mNumberExists;

    List<QilexContact> mContacts = new ArrayList<QilexContact>();

    List<QilexContact> mKATZContacts = new ArrayList<QilexContact>();;

    String cursorFilter;

    boolean onSearching = false;

    ContactManager mContactManager;

    private boolean requestReloadWhenResume = true;

    private SoftKeyboard softKeyboard;
    
    private int oldSelectionNumber;

    TextWatcher onSearchQuery = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mContactAdapter != null) {
                mContactAdapter.getFilter().filter(s);
                cursorFilter = s.toString();
                if (TextUtils.isEmpty(cursorFilter)) {
                    onSearching = false;
                    showMyNumberText();
                } else {
                    onSearching = true;
                    hideMyNumberText();
                }
                mListView.setInSearchMode(onSearching);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public ContactListFragment() {
        mContactManager = ContactManager.getInstance();
        requestReloadWhenResume = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = mBaseActivity.getIntent().getExtras();
        if (extras != null) {
            mNumberIntent = extras.getString("insert_to_contact_number");
            mBrowserContact = extras.getBoolean(EXTRAS_DATA_BROWSE_CONTACT_MODE, false);
            mBrowserViewOnly = extras.getBoolean(EXTRAS_DATA_BROWSE_CONTACT_TYPE_VIEW_ONLY, false);
            mNumberExists = extras.getStringArray("frifon_exists_numbers");
        }
        mContactManager.addContactSyncListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.contact_list_fragment, container, false);
        edtSearch = (EditText)rootView.findViewById(R.id.input_search_query);

        mContactManager.reloadAllContact();

        // Initialize controls
        mSearchContainer = rootView.findViewById(R.id.search_container);
        progressLoading = (ProgressBar)rootView.findViewById(R.id.loading);
        txtMyNumber = (MyCustomTextView)rootView.findViewById(R.id.my_number);
        txtMyNumber.setText(
                String.format(getString(R.string.format_my_number), UserInfo.getInstance().getPhoneNumberFormatted()));
        mListView = (ContactListView)rootView.findViewById(R.id.contact_list);
        mListView.setFastScrollEnabled(true);
        mListView.setChoiceMode(mBrowserContact ? ListView.CHOICE_MODE_MULTIPLE : ListView.CHOICE_MODE_SINGLE);
        mProgressSync = (SmoothProgressBar)rootView.findViewById(R.id.progressSync);

        // Initialize listview data
        mListView.setVisibility(View.VISIBLE);
        if (mBrowserContact) {
            mKATZContacts = new Vector<QilexContact>();
            mKATZContacts.addAll(mContactManager.getFrifonContacts());
            mContactAdapter = new ContactListAdapter(mBaseActivity, mKATZContacts, mBrowserContact);
            if (mNumberExists != null) {
                mContactAdapter.setCurrentCheckNumbersCannotModify(Arrays.asList(mNumberExists), false);
                oldSelectionNumber = mContactAdapter.getCurrentCheckNumbers().length;
            }
            mViewMode = VIEW_FRIFON;
        }
        if (mContactAdapter != null) {
            mListView.setAdapter(mContactAdapter);
        }

        // Initialize action listener
        initActionListener();
        translationMyNumber = txtMyNumber.getTop();

        if (softKeyboard != null) {
            softKeyboard.unRegisterSoftKeyboardCallback();
        }
        
//        oldSelectionNumber = mContactAdapter.getCurrentCheckNumbers().length;
        return rootView;
    }

    private void initActionListener() {
        mListView.setOnItemClickListener(this);
        edtSearch.addTextChangedListener(onSearchQuery);
        mListView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mBaseActivity.hideKeyboard();
                return mBaseActivity.onTouchEvent(event);
            }
        });

        mSearchContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBaseActivity.showKeyboard(edtSearch);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mContactManager.removeContactSyncListener(this);
    }

    public void onStartResult() {
        if (mContactAdapter != null) {
        	String[] currentCheckNumber = mContactAdapter.getCurrentCheckNumbers();
        	
        	// Remove you
        	ArrayList<String> listCurrentCheckNumberExceptYou = new ArrayList<String>();
        	for (int i = 0; i < currentCheckNumber.length; i++) {
        		if (!UserInfo.getInstance().getPhoneNumberFormatted().equals(currentCheckNumber[i])) {
        			listCurrentCheckNumberExceptYou.add(currentCheckNumber[i]);
        		}
        	}
        	String []currentCheckNumberExceptYou = new String[listCurrentCheckNumberExceptYou.size()];
        	currentCheckNumberExceptYou = listCurrentCheckNumberExceptYou.toArray(currentCheckNumberExceptYou);
        	
            Intent intent = new Intent();
            intent.putExtra("selected_numbers", currentCheckNumberExceptYou);
            
            // Send invite
            if (mContactAdapter.getNumberOfDisableContactExceptYou() == 0 && currentCheckNumberExceptYou.length > 1) {
            	String groupChatUuid = ChatManager.getInstance().sendInviteGroupChat(null, -1, currentCheckNumberExceptYou);
            	
            	if (groupChatUuid == null) {
                    mBaseActivity.setResult(Activity.RESULT_CANCELED);
                    mBaseActivity.finish();
                    return;
            	}
                intent.putExtra("groupChatUuid", groupChatUuid);
            }
            mBaseActivity.setResult(Activity.RESULT_OK, intent);
        } else {
            mBaseActivity.setResult(Activity.RESULT_CANCELED);
        }
        mBaseActivity.finish();
    }

    public void clearSelection() {
        if (mContactAdapter != null) {
            mContactAdapter.clearSelection();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (requestReloadWhenResume == true || mBrowserViewOnly) {
            // Check if this fragment is Contact picker.
            loadListData();
            doSearch();
        }
        if (mContactManager.isContactSynchronizing() == true && mContactManager.isContactGetting() == false) {
            mProgressSync.setVisibility(View.VISIBLE);
        } else {
            mProgressSync.setVisibility(View.GONE);
        }

    }

    private void doSearch() {
        if (mContactAdapter != null && edtSearch != null && edtSearch.getText() != null
                && edtSearch.getText().toString().length() > 0) {
            mContactAdapter.getFilter().filter(edtSearch.getText().toString());
            cursorFilter = edtSearch.getText().toString().toString();
            if (TextUtils.isEmpty(cursorFilter)) {
                onSearching = false;
                showMyNumberText();
            } else {
                onSearching = true;
                hideMyNumberText();
            }
            mListView.setInSearchMode(onSearching);
        }
    }

    public void onChangeTypeList(int type) {
        if (mViewMode == type) {
            return;
        }
        mViewMode = type;
        if (mContactAdapter != null) {
            mContactAdapter.setItems((mViewMode == ContactListFragment.VIEW_ALL) ? mContacts : mKATZContacts);
            mContactAdapter.getFilter().filter(edtSearch.getText().toString());
            mContactAdapter.notifyDataSetChanged();
            mListView.getScroller().setAdapter(mContactAdapter);
        }
    }

    public int getViewMode() {
        return mViewMode;
    }

    private void loadListData() {
        if (mBrowserViewOnly == false && mContactManager.isContactLoading() == true
                && (mContactAdapter == null || mContactAdapter.isEmpty())) {
            progressLoading.setVisibility(View.VISIBLE);
        } else {
            progressLoading.setVisibility(View.GONE);
        }
        // Get All Contact
        mContacts = mContactManager.getAllContact();

        // Get Frifon Contact
        mKATZContacts = mContactManager.getFrifonContacts();
        requestReloadWhenResume = false;
        if (mContactAdapter == null || onSearching) {
            mContactAdapter = new ContactListAdapter(mBaseActivity, (mViewMode == VIEW_ALL) ? mContacts : mKATZContacts,
                    mBrowserContact);
            mListView.setAdapter(mContactAdapter);
            progressLoading.setVisibility(View.GONE);
        } else {
            int idx = mListView.getFirstVisiblePosition();
            View vfirst = mListView.getChildAt(0);
            int pos = 0;
            if (vfirst != null)
                pos = vfirst.getTop();
            mContactAdapter.setItems((mViewMode == ContactListFragment.VIEW_ALL) ? mContacts : mKATZContacts);
            mListView.setSelectionFromTop(idx, pos);
            mContactAdapter.getFilter().filter(edtSearch.getText().toString());
        }
        mContactAdapter.notifyDataSetChanged();
        mListView.setNeedReDrawScroller();
    }

    private void hideMyNumberText() {
        txtMyNumber.setVisibility(View.GONE);
    }

    private void showMyNumberText() {
        txtMyNumber.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        List<QilexContact> contacts = mContactAdapter.getListContacts();
        QilexContact contactFromList = contacts.get(position);

        // If contact is exist, do next step
        if (!mBrowserContact) {
            if (mNumberIntent != null) {
                // If contact is open from Add Number To Contact, check if this
                // device is slave mode
                // Check if contact is not editable, show error.
                if (contactFromList.isEditable == false) {
                    Toast.makeText(mBaseActivity, R.string.contact_is_not_editable, Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent();
                intent.putExtra(EXTRAS_DATA_SELECTED_CONTACTS, contactFromList.id);
                mBaseActivity.setResult(Activity.RESULT_OK, intent);
                mBaseActivity.finish();
            } else {
                Intent intent = new Intent(mBaseActivity, ContactKATZOutDetailActivity.class);
                intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS, contactFromList.id);
                intent.putExtra(ContactListFragment.EXTRAS_DATA_SELECTED_CONTACTS_LOOKUP_KEY,
                        contactFromList.lookUpKey);
                intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_LEFT_OUT_LEFT);
                if (mBrowserViewOnly == true) {
                    intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_VIEW_TYPE,
                            ContactKATZOutDetailActivity.VIEW_TYPE_IN_CALLING);
                }
                mBaseActivity.startActivity(intent);
            }
        } else {
            boolean check = !mContactAdapter.isPositionChecked(position);
            if (check)
                mContactAdapter.setNewSelection(position, check);
            else
                mContactAdapter.removeSelection(position);
            onSelectedContactChange(mContactAdapter.getCurrentCheckNumbers().length - oldSelectionNumber);
        }
    }

    @Override
    public void onNewContact(final QilexContact newContact) {
        requestReloadWhenResume = true;
    }

    @Override
    public void onUpdateContact(QilexContact newContact) {
        requestReloadWhenResume = true;
    }

    @Override
    public void onDeleteContact(QilexContact newContact) {
        requestReloadWhenResume = true;
    }

    @Override
    public void onCacheContactChange() {
        mBaseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mContactAdapter == null) {
                    loadListData();
                } else {
                    mContacts.clear();
                    mKATZContacts.clear();
                    mContacts.addAll(mContactManager.getAllContact());
                    mKATZContacts.addAll(mContactManager.getFrifonContacts());
                    mContactAdapter.notifyDataSetChanged();
                    mListView.setNeedReDrawScroller();
                    if (mBrowserViewOnly == false && mContactManager.isContactLoading() == true
                            && (mContactAdapter == null || mContactAdapter.isEmpty())) {
                        progressLoading.setVisibility(View.VISIBLE);
                    } else {
                        progressLoading.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    @Override
    public void onSyncStart(boolean isUpdate, boolean isAddNew) {
        mBaseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressSync.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onSyncFinish(final boolean isSuccess) {
        mBaseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressSync.setVisibility(View.GONE);
                if (mBrowserContact) {
                    mKATZContacts = new Vector<QilexContact>();
                    mKATZContacts.addAll(mContactManager.getFrifonContacts());
                    mContactAdapter = new ContactListAdapter(mBaseActivity, mKATZContacts, mBrowserContact);
                    if (mNumberExists != null) {
                        mContactAdapter.setCurrentCheckNumbersCannotModify(Arrays.asList(mNumberExists), false);
                    }
                    mViewMode = VIEW_FRIFON;
                    mListView.setAdapter(mContactAdapter);
                    progressLoading.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onSyncProgress(int progress) {
        // TODO Auto-generated method stub
    }
    
    public void onSelectedContactChange(int numberOfSelectedContact) {
    }
}
