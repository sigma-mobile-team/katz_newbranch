
package sigma.qilex.ui.tab;

import java.util.ArrayList;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.ChatMessageModel;
import sigma.qilex.dataaccess.model.ChatThreadModel;
import sigma.qilex.manager.chat.ChatManager;
import sigma.qilex.manager.chat.ChatManager.ChatMessageChangeListener;
import sigma.qilex.manager.chat.ChatManager.ChatThreadChangeListener;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.ContactKATZOutDetailActivity;
import sigma.qilex.ui.activity.ConversationActivity;
import sigma.qilex.ui.activity.ConversationSettingActivity;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.adapter.ListConversationChatAdapter;
import sigma.qilex.ui.adapter.ListConversationChatAdapter.OnLoadMoreListener;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.SharedPrefrerenceFactory;

public class ChatFragment extends BaseFragment
        implements ChatThreadChangeListener, ClickInfoListener, OnItemClickListener, OnLoadMoreListener, ChatMessageChangeListener {

    public static final int BROWSER_CONTACT_REQUEST = 1001;

    private TextView mTxtNoChat;

    private ListView mListChat;

    private ListConversationChatAdapter adapter;

    private ChatManager mChatManager;

    private ContactManager mContactManager;

    private Handler postHandler = new Handler();

    private boolean requestReloadWhenResume = true;

    private AsyncTask<Integer, Integer, ArrayList<ChatThreadModel>> loadingChatThreadTask;

    private ProgressBar progressLoading;

    ArrayList<ChatThreadModel> listChatThread = new ArrayList<ChatThreadModel>();

    public ChatFragment() {
        setEditMode(false);
        mChatManager = ChatManager.getInstance();
        mContactManager = ContactManager.getInstance();
        requestReloadWhenResume = true;
        mChatManager.addChatThreadChangeListener(this);
        mChatManager.addChatMessageChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.tab_chat_fragment, container, false);

        // Initialize controls
        mListChat = (ListView)rootView.findViewById(R.id.listChat);
        progressLoading = (ProgressBar)rootView.findViewById(R.id.loading);
        mTxtNoChat = (TextView)rootView.findViewById(R.id.txtNoChat);
        progressLoading.setVisibility(View.GONE);

        // Create adapter
        if (adapter == null) {
            adapter = new ListConversationChatAdapter(mBaseActivity, false, this);
            adapter.setClickInfoListener(this);
            adapter.animateFirstListener = mBaseActivity.animateFirstListener;
            adapter.displayImageOptions = mBaseActivity.circleDisplayImageOptions;
            mContactManager.addContactChangeListener(adapter);
        }
        mListChat.setAdapter(adapter);
        mListChat.setOnItemClickListener(this);
        mContactManager.addFirstSyncListener(adapter);
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mContactManager.removeFirstSyncListener(adapter);
        if (loadingChatThreadTask != null) {
            loadingChatThreadTask.cancel(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (requestReloadWhenResume == true) {
            getChatThread(false);
            requestReloadWhenResume = false;
        } else {
            notifyDataSetChange();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void removeAllManagerListener() {
        mChatManager.removeChatThreadChangeListener(this);
        mContactManager.removeContactChangeListener(adapter);
        mChatManager.removeChatMessageChangeListener(this);
    }

    public void getChatThread(boolean isLoadMore) {
        // Load async
        if (loadingChatThreadTask != null) {
            loadingChatThreadTask.cancel(true);
        }
        int count = adapter.getCount();
        if (isLoadMore == true) {
            count += 5;
            if (count < 10) {
                count = 10;
            }
        }
        loadingChatThreadTask = mChatManager.loadAllChatThreadAsynchronous(count,
                new ChatManager.LoadChatThreadListener() {
                    @Override
                    public void onLoadChatThreadStart() {
                        postHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressLoading.setVisibility(View.VISIBLE);
                                mTxtNoChat.setVisibility(View.GONE);
                            }
                        });
                    }

                    @Override
                    public void onLoadChatThreadFinish(ArrayList<ChatThreadModel> listResult) {
                        postHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressLoading.setVisibility(View.GONE);
                            }
                        });
                        if (listResult.size() == 0) {
                            mTxtNoChat.setVisibility(View.VISIBLE);
                            mListChat.setVisibility(View.GONE);
                        } else {
                            mTxtNoChat.setVisibility(View.GONE);
                            mListChat.setVisibility(View.VISIBLE);
                        }
                        adapter.setListItem(listResult, mChatManager.countAllThread());
                        notifyDataSetChange();
                    }
                }, true);
    }

    public void changeToEditMode() {
        if (adapter == null) {
            return;
        }
        setEditMode(true);
        adapter.setEditMode(isEditMode());
        adapter.notifyDataSetChanged();
    }

    public void changeToViewMode() {
        if (adapter == null) {
            return;
        }
        setEditMode(false);
        adapter.setEditMode(isEditMode());
        adapter.notifyDataSetChanged();
    }

    public boolean isDataBinded() {
        return adapter != null;
    }

    public ListConversationChatAdapter getAdapter() {
        return adapter;
    }

    private void notifyDataSetChange() {
        if (adapter.getCount() == 0) {
            mListChat.setVisibility(View.INVISIBLE);
            mTxtNoChat.setVisibility(View.VISIBLE);
            if (mBaseActivity instanceof MainTabActivity && isActive()) {
                ((MainTabActivity)mBaseActivity).setChatEditVisible(false);
            }
        } else {
            mListChat.setVisibility(View.VISIBLE);
            mTxtNoChat.setVisibility(View.INVISIBLE);
            if (mBaseActivity instanceof MainTabActivity && isActive()) {
                ((MainTabActivity)mBaseActivity).setChatEditVisible(true);
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void notifyDataSetChangeByPostHandler() {
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChange();
            }
        });
    }

    @Override
    public void onThreadDeleted(final long[] deletedThreadIds) {
        requestReloadWhenResume = true;
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                for (long threadId : deletedThreadIds) {
                    SharedPrefrerenceFactory.getInstance()
                            .remove(String.format(ConversationActivity.PREF_DRAFT_MESSAGE, threadId));
                }
                if (isActive()) {
                    getChatThread(false);
                } else {
                    listChatThread = mChatManager.getAllCurrentChatThread(true);
                    adapter.setListItem(mChatManager.getAllCurrentChatThread(true), mChatManager.countAllThread());
                    notifyDataSetChangeByPostHandler();
                }
            }
        });
    }

    @Override
    public void onThreadChanged(ChatThreadModel model) {
        requestReloadWhenResume = true;
        postHandler.post(new Runnable() {
            @Override
            public void run() {
                if (isActive()) {
                    getChatThread(false);
                } else {
                    listChatThread = mChatManager.getAllCurrentChatThread(true);
                    adapter.setListItem(mChatManager.getAllCurrentChatThread(true), mChatManager.countAllThread());
                    notifyDataSetChangeByPostHandler();
                }
            }
        });
    }

    @Override
    public void onClickInfo(int position) {
        ChatThreadModel chatThread = adapter.getItem(position);
        if (chatThread.isGroupChat()) {
        	if (chatThread.isYouInGroupChat()) {
        		Intent intent = new Intent(mBaseActivity, ConversationSettingActivity.class);
            	String[] numbers = chatThread.getListPhoneNumberParticipantExceptYou();
            	intent.putExtra("contact_list_numbers", numbers);
                intent.putExtra(ConversationSettingActivity.INTENT_PARAM_GROUP_CHAT_UUID, chatThread.contactStr);
                intent.putExtra(ConversationSettingActivity.INTENT_PARAM_IS_ADMIN, UserInfo.getInstance().getPhoneNumberFormatted().equals(
                		chatThread.getPhoneNumberAdmin()));
                intent.putExtra(ConversationSettingActivity.INTENT_PARAM_ADMIN_PHONE, chatThread.getPhoneNumberAdmin());
                intent.putExtra(ConversationSettingActivity.INTENT_PARAM_THREAD_ID, chatThread.id);
                intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                startActivity(intent);
        	}
        } else {
        	String realNumber = chatThread.contactStr;

            Intent intent = new Intent(getActivity(), ContactKATZOutDetailActivity.class);
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_VIEW_TYPE,
                    ContactKATZOutDetailActivity.VIEW_TYPE_CALLLOG);
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_PHONE_NUMBER, realNumber);
            long[] ids = new long[0];
            intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_LIST_CALLLOG, ids);
            intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
            mBaseActivity.startActivity(intent);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (id == -1) {
            getChatThread(true);
        } else {
            adapter.clickOnItem(position, view);
            // notify to activity
            if (mBaseActivity instanceof MainTabActivity) {
                MainTabActivity acti = (MainTabActivity)mBaseActivity;
                if (acti != null) {
                    acti.onClickChatRow();
                }
            }
        }
    }

    @Override
    public void onLoadMore() {
        getChatThread(true);
    }

    @Override
    public void onNewMessage(ChatMessageModel ...newMsg) {
    }

    @Override
    public void onMessageStatusUpdate(ArrayList<ChatMessageModel> models) {
    }

    @Override
    public void onUnreadMessageChanged(int numberUnreadThread, int totalUnreadMesage) {
        requestReloadWhenResume = true;
    }
}
