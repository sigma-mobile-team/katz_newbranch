
package sigma.qilex.ui.tab;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import pl.katz.aero2.Const;
import pl.katz.aero2.Const.Config;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.dataaccess.network.http.QilexHttpRequest;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.AuthenticationManager.AcceptRegulationListener;
import sigma.qilex.manager.account.GetAccountBalanceListener;
import sigma.qilex.ui.activity.AccountActivity;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.activity.PaymentActivity;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.Utils;

public class KATZOutFragment extends BaseFragment implements OnClickListener, GetAccountBalanceListener {

    MyCustomTextView mTxtFrifonPrice;

    AuthenticationManager mAuthenManager;

    ProgressBar loadingBalance;

    ImageButton btnRefresh;

    UserInfo mUserInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuthenManager = AuthenticationManager.getInstance();
        mUserInfo = UserInfo.getInstance();
    }

    private void checkRegulation(Intent intent) {
        double outRegulationAcceptedVersion = mUserInfo.getLocalProfile().getOutRegulationAcceptedVersion();
        double outRegulationCurrentVersion = mUserInfo.getLocalProfile().getOutRegulationCurrentVersion();
        int titleDialog = Utils.isInKatzMode() ? R.string.regulamin_katz_out : R.string.regulamin_katz_out_green;
        if (outRegulationAcceptedVersion < 0) {
            mBaseActivity.showAlertWithLink(titleDialog, R.string.katz_out_regulation,
                    mAcceptListenter);
        } else if (outRegulationCurrentVersion > outRegulationAcceptedVersion) {
            mBaseActivity.showAlertWithLink(titleDialog, R.string.katz_out_regulation_update,
                    mAcceptListenter);
        } else if (intent != null) {
            startActivityForResult(intent, PaymentActivity.REQUEST_CODE);
        }
    }

    private DialogInterface.OnClickListener mAcceptListenter = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            final int requestId = mAuthenManager.sendAcceptOutRegulation(mBaseActivity, true,
                    mAcceptRegulationListener);
            mBaseActivity.showSimpleProgress(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    QilexHttpRequest.stopRequest(requestId);
                }
            });
            dialog.cancel();
        }
    };

    private AcceptRegulationListener mAcceptRegulationListener = new AcceptRegulationListener() {

        @Override
        public void onAcceptRegulationFinish(final boolean isSuccess, ErrorModel error) {
            mBaseActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBaseActivity.hideSimpleProgress();
                    if (isSuccess) {
                        Intent intent = new Intent(mBaseActivity, PaymentActivity.class);
                        startActivityForResult(intent, PaymentActivity.REQUEST_CODE);
                    }
                }
            });

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frifon_out_fragment, container, false);
        LinearLayout katzOut = (LinearLayout)rootView.findViewById(R.id.katz_out);
        LinearLayout settings = (LinearLayout)rootView.findViewById(R.id.settings);
        mTxtFrifonPrice = (MyCustomTextView)rootView.findViewById(R.id.txtAccountBalance);
        btnRefresh = (ImageButton)rootView.findViewById(R.id.btnRefresh);
        if (mUserInfo.accountBalance > -1) {
            mTxtFrifonPrice.setText(String.format(Config.MONEY_FORMAT, mUserInfo.accountBalance));
        }
        mTxtFrifonPrice.setEnabled(false);

        loadingBalance = (ProgressBar)rootView.findViewById(R.id.loadingBalance);

        loadBalance();

        // Set listener
        katzOut.setOnClickListener(this);
        settings.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);
        mTxtFrifonPrice.setOnClickListener(this);
        rootView.findViewById(R.id.btnAdd).setOnClickListener(this);
        return rootView;
    }

    private void loadBalance() {
        // Load Account Balance
        mAuthenManager.getUserAccountBalance(UserInfo.getInstance().getPhoneNumberFormatted(), this);
        loadingBalance.setVisibility(View.VISIBLE);
        mTxtFrifonPrice.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBaseActivity.setActionbarTitle(Utils.getString(getActivity(), R.attr.stringKatzOutTitle));
        mBaseActivity.showBackAction();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.katz_out:
                if (mBaseActivity instanceof MainTabActivity)
                    ((MainTabActivity)mBaseActivity).pushFragments(MainTabActivity.TAB_ACCOUNT,
                            new KATZOutCallHistoryFragment(), true, true);
                else if (mBaseActivity instanceof AccountActivity)
                    ((AccountActivity)mBaseActivity).pushFragments(new KATZOutCallHistoryFragment(), true, true);

                break;
            case R.id.settings:
                if (mBaseActivity instanceof MainTabActivity)
                    ((MainTabActivity)mBaseActivity).pushFragments(MainTabActivity.TAB_ACCOUNT,
                            new KATZOutListFragment(), true, true);
                else if (mBaseActivity instanceof AccountActivity)
                    ((AccountActivity)mBaseActivity).pushFragments(new KATZOutListFragment(), true, true);
                break;
            case R.id.btnAdd:
                Intent intent = new Intent(mBaseActivity, PaymentActivity.class);
                checkRegulation(intent);
                break;
            case R.id.btnRefresh:
            case R.id.txtAccountBalance:
                btnRefresh.setVisibility(View.GONE);
                loadBalance();
                break;
            default:
                break;
        }
    }

    @Override
    public void onGetAccountBalanceFinish(String msisdn, final double balance, final ErrorModel error) {
        mBaseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTxtFrifonPrice.setEnabled(true);
                loadingBalance.setVisibility(View.GONE);
                if (error == null) {
                    mUserInfo.accountBalance = balance;
                    mTxtFrifonPrice.setText(String.format(Config.MONEY_FORMAT, balance));
                    mTxtFrifonPrice.setEnabled(true);
                } else {
                    btnRefresh.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
