
package sigma.qilex.ui.tab;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import pl.katz.aero2.Const;
import pl.katz.aero2.Const.Config;
import pl.katz.aero2.MyApplication;
import pl.frifon.aero2.R;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.http.ErrorModel;
import sigma.qilex.manager.account.AuthenticationManager;
import sigma.qilex.manager.account.SetupUserInfoListener;
import sigma.qilex.manager.facebook.OnLoginListener;
import sigma.qilex.manager.facebook.SimpleFacebook;
import sigma.qilex.ui.activity.AccountActivity;
import sigma.qilex.ui.activity.AccountConfigurationActivity;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.BaseActivity.ProgressLoadingListener;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.ui.customview.MyCustomTextView;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;

public class AccountFragment extends BaseFragment implements OnClickListener, SetupUserInfoListener {
    final int SELECT_PHOTO = 0;

    final int CAMERA_REQUEST = 1;

    ImageView mImgAvatar;

    int mImgSize;

    View mViewDividerFb;

    MyCustomTextView mUserName;

    UserInfo mUserInfo;

    MyCustomTextView myNumber;

    EditText txtInputName;

    LinearLayout editName;

    View editingName;

    public boolean isEditName = false;

    public boolean isEditAvatar = false;

    private View btnConnectFb;

    private TextView tvFacebookConnect;

    private SimpleFacebook mSimpleFacebook;

    ProgressBar mProgressAvatar;

    boolean isActive = false;

    boolean isRequestingFacebook = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserInfo = UserInfo.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(mBaseActivity);
        View rootView = inflater.inflate(R.layout.tab_account_fragment, container, false);
        txtInputName = (EditText)rootView.findViewById(R.id.txtInputName);
        editingName = rootView.findViewById(R.id.editing_name);
        mImgAvatar = (ImageView)rootView.findViewById(R.id.user_avatar);
        mProgressAvatar = (ProgressBar)rootView.findViewById(R.id.progress_avatar);
        btnConnectFb = rootView.findViewById(R.id.btnConnectFb);
        btnConnectFb.setOnClickListener(this);
        tvFacebookConnect = (TextView)rootView.findViewById(R.id.tvFacebookConnect);
        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());

        mImgAvatar.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                // get current image avatar size height for new reload bitmap
                mImgSize = mImgAvatar.getHeight();
                // this is an important step not to keep receiving
                // callbacks:
                // we should remove this listener
                mImgAvatar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        // setImage to avatar
        if (mUserInfo.hasAvatar()) {
            String avatarUrl = mUserInfo.getAvatarUrl();
            MyApplication.getImageLoader().displayImage(avatarUrl, mImgAvatar, mBaseActivity.circleDisplayImageOptions2,
                    mBaseActivity.animateFirstListener);
        }

        mUserName = (MyCustomTextView)rootView.findViewById(R.id.user_name);

        updateInfo();

        LinearLayout frifonOut = (LinearLayout)rootView.findViewById(R.id.frifon_out);
        frifonOut.setOnClickListener(this);
        if (Const.IS_FRIFON_LITE) {
            rootView.findViewById(R.id.frifon_out_divider).setVisibility(View.GONE);
            frifonOut.setVisibility(View.GONE);
        }
        LinearLayout settings = (LinearLayout)rootView.findViewById(R.id.settings);
        settings.setOnClickListener(this);
        LinearLayout support = (LinearLayout)rootView.findViewById(R.id.support);
        support.setOnClickListener(this);

        rootView.findViewById(R.id.logout).setOnClickListener(this);
        // if (isLoggedIn()) {
        // authFacebookButton.setVisibility(View.GONE);
        // }
        mViewDividerFb = rootView.findViewById(R.id.view_divider_fb);

        myNumber = (MyCustomTextView)rootView.findViewById(R.id.my_number);
        myNumber.setText(String.format(getString(R.string.format_my_number), mUserInfo.getPhoneNumberFormatted()));

        editName = (LinearLayout)rootView.findViewById(R.id.edit_name);
        editName.setOnClickListener(this);

        // Load avatar
        if (mUserInfo.hasAvatar()) {
            mImgAvatar.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                @SuppressWarnings("deprecation")
                @Override
                public void onGlobalLayout() {
                    try {
                        // get current image avatar size height for new reload
                        // bitmap
                        mImgSize = mImgAvatar.getHeight();
                        setAvatarParams();
                        MyApplication.getImageLoader().displayImage(mUserInfo.getAvatarUrl(), mImgAvatar,
                                mBaseActivity.circleDisplayImageOptions2, mBaseActivity.animateFirstListener);
                        // this is an important step not to keep receiving
                        // callbacks:
                        // we should remove this listener
                        mImgAvatar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } catch (Exception e) {
                    }
                }
            });
        }

        updateBtnFb();
        return rootView;
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isActive = true;
        mUserInfo.loadInfo();
        mBaseActivity.hideKeyboard();
        mBaseActivity.setActionbarTitle(R.string.account);
        if (mBaseActivity instanceof MainTabActivity)
            mBaseActivity.setBackAction(false);
        updateInfo();
        mSimpleFacebook = SimpleFacebook.getInstance(getActivity());
        updateBtnFb();
    }

    private void updateInfo() {
        // setImage to avatar
        if (mUserInfo.hasAvatar()) {
            String avatarUrl = mUserInfo.getAvatarUrl();
            MyApplication.getImageLoader().displayImage(avatarUrl, mImgAvatar, mBaseActivity.circleDisplayImageOptions2,
                    new ProgressLoadingListener(mProgressAvatar));
        }
        String name = mUserInfo.getDisplayName();
        if (Utils.isStringNullOrEmpty(name)) {
            mUserName.setText(R.string.unknown);
            mUserName.setSelected(true);
            mUserName.setTextColor(getResources().getColor(R.color.gray));
        } else {
            mUserName.setText(mUserInfo.getDisplayName());
            mUserName.setSelected(false);
            mUserName.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        // TODO ???
        /*
         * if (id == R.id.btnConnectFb) { editingName.setVisibility(View.GONE);
         * editName.setVisibility(View.VISIBLE); } else { saveNewName(); }
         */
        switch (id) {
            case R.id.btnConnectFb:
                onClickBtnFb();
                break;
            case R.id.edit_name:
                Intent i = new Intent(mBaseActivity, AccountConfigurationActivity.class);
                i.putExtra("IS_CHANGE_INFO", true);
                i.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
                startActivity(i);
                break;
            case R.id.frifon_out:
                if (mBaseActivity instanceof MainTabActivity)
                    ((MainTabActivity)mBaseActivity).pushFragments(MainTabActivity.TAB_ACCOUNT, new KATZOutFragment(),
                            true, true);
                else if (mBaseActivity instanceof AccountActivity)
                    ((AccountActivity)mBaseActivity).pushFragments(new KATZOutFragment(), true, true);
                break;
            case R.id.settings:
                if (mBaseActivity instanceof MainTabActivity)
                    ((MainTabActivity)mBaseActivity).pushFragments(MainTabActivity.TAB_ACCOUNT,
                            new AccountSettingsFragment(), true, true);
                else if (mBaseActivity instanceof AccountActivity)
                    ((AccountActivity)mBaseActivity).pushFragments(new AccountSettingsFragment(), true, true);
                break;
            case R.id.support:
                clickSupport();
                break;
            case R.id.user_avatar:
                mBaseActivity.onChangeAvatar();
                break;
            case R.id.logout:
                mBaseActivity.confirmLogout();
                break;
            default:
                break;
        }

    }

    private void clickSupport() {
        if (mBaseActivity instanceof MainTabActivity)
            ((MainTabActivity)mBaseActivity).pushFragments(MainTabActivity.TAB_ACCOUNT, new SupportFragment(), true,
                    true);
        else if (mBaseActivity instanceof AccountActivity)
            ((AccountActivity)mBaseActivity).pushFragments(new SupportFragment(), true, true);
    }

    private void onClickBtnFb() {
        if (!mSimpleFacebook.isLogin()) {
            // login now
            loginFb();
        } else {
            restoreFbInfo();
        }
    }

    private void loginFb() {
        mSimpleFacebook.login(new OnLoginListener() {

            @Override
            public void onFail(String reason) {
                LogUtils.d("binhdt", "onFail");
                Toast.makeText(mBaseActivity, mBaseActivity.getResources().getString(R.string.fb_login_failed),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onException(Throwable throwable) {
                LogUtils.d("binhdt", "onException");
                Toast.makeText(mBaseActivity, mBaseActivity.getResources().getString(R.string.fb_login_failed),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLogin(AccessToken accessToken) {
                LogUtils.d("binhdt", "onLogin, accessToken " + accessToken);
                handleLoginSuccess(accessToken);
            }

            @Override
            public void onCancel() {
                LogUtils.d("binhdt", "onCancel");
            }
        }, Arrays.asList("public_profile", "email", "user_friends"));
    }

    private void restoreFbInfo() {
        if (mSimpleFacebook.isLogin()) {
            handleLoginSuccess(mSimpleFacebook.getAccessToken());
            isEditAvatar = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * reset layout param of user avatar with real width and height in px
     */
    private void setAvatarParams() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mImgSize, mImgSize);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        mImgAvatar.setLayoutParams(params);
    }

    private void updateBtnFb() {
        if (mSimpleFacebook.isLogin()) {
            tvFacebookConnect.setText(getString(R.string.facebook_connect_use));
        } else {
            tvFacebookConnect.setText(getString(R.string.facebook_connect));
        }
    }

    protected void handleLoginSuccess(final AccessToken accessToken) {
        isRequestingFacebook = true;
        mBaseActivity.showSimpleProgressByPostHandler(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                isRequestingFacebook = false;
            }
        });
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (isRequestingFacebook == false) {
                    return;
                }
                mBaseActivity.hideSimpleProgressByPostHandler();
                try {
                    // map fb data to UI
                    final String name = object.getString("name");
                    mUserName.setText(name);
                    mUserName.setTextColor(getResources().getColor(R.color.black));
                    UserInfo.getInstance().setFirstName(name);
                    UserInfo.getInstance().setLastName(Const.STR_EMPTY);
                    UserInfo.getInstance().saveInfo();

                    SharedPrefrerenceFactory.getInstance().saveEmailFb(object.optString("email", null));

                    GraphRequest request = GraphRequest.newMeRequest(accessToken,
                            new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Application code
                            try {
                                JSONObject picture = (JSONObject)object.get("picture");
                                JSONObject dta = (JSONObject)picture.get("data");
                                String avatar = dta.getString("url");

                                mUserInfo.setAvatarUrl(avatar);
                                LogUtils.d("binhdt", "URL " + avatar);
                                isEditAvatar = true;
                                mUserInfo.saveInfo();

                                if (isActive) {
                                    updateInfo();
                                    updateBtnFb();
                                }

                                setupUserInfo();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                LogUtils.d("binhdt", "Get avatar exc " + e.toString());
                            }
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "picture.type(large)");
                    request.setParameters(parameters);
                    request.executeAsync();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void setupUserInfo() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String retVal = Const.STR_EMPTY;
                byte[] avatarByte = mUserInfo.getAvatarBytesJpg();
                if (avatarByte != null) {
                    try {
                        retVal = Zip.encodeBase64(avatarByte);
                    } catch (UnsupportedEncodingException e) {
                    }
                }
                return retVal;
            }

            @Override
            protected void onPostExecute(String base64Avatar) {
                super.onPostExecute(base64Avatar);
                AuthenticationManager.getInstance().setUpUserInfo(mUserInfo.getPhoneNumberFormatted(),
                        mUserInfo.getFirstName(), mUserInfo.getLastName(), base64Avatar, Config.AVATAR_UPLOAD_FORMAT,
                        AccountFragment.this);
            }
        }.execute();
    }

    @Override
    public void onSetupUserInfoFinish(boolean isSuccess, ErrorModel error) {

    }
}
