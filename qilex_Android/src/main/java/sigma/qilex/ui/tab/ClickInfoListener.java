
package sigma.qilex.ui.tab;

public interface ClickInfoListener {
    public void onClickInfo(int position);
}
