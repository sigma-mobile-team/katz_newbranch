
package sigma.qilex.ui.tab;

import java.util.ArrayList;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import pl.frifon.aero2.R;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.manager.phone.CallLogChangeListener;
import sigma.qilex.manager.phone.CallLogManager;
import sigma.qilex.manager.phone.CallLogManager.LoadCallLogResult;
import sigma.qilex.manager.phone.CallManager;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.activity.ContactKATZOutDetailActivity;
import sigma.qilex.ui.activity.MainTabActivity;
import sigma.qilex.ui.adapter.CallLogAdatper;
import sigma.qilex.ui.customview.BaseFragment;
import sigma.qilex.utils.ActivityTransitionFactory;
import sigma.qilex.utils.Utils;

public class CallLogFragment extends BaseFragment
        implements OnItemClickListener, CallLogChangeListener, ClickInfoListener {

    public static final int VIEW_MODE_ALL = 0;

    public static final int VIEW_MODE_MISS_CALL = 1;

    private ListView mListCallLog;

    private CallLogAdatper mAdapter;

    private TextView mTxtNoCallLog;

    private boolean mReloadCallLogOnResume;

    private int viewMode = VIEW_MODE_ALL;

    ArrayList<CallLogModel> listAll;

    ArrayList<CallLogModel> listMissCall;

    CallLogManager mCallLogManager;

    CallManager mCallManager;

    ContactManager mContactManager;

    private ProgressBar progressLoading;

    private Handler postHandler = new Handler();

    AsyncTask<Integer, Integer, LoadCallLogResult> taskLoading;

    public CallLogFragment() {
        setEditMode(false);
        mReloadCallLogOnResume = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_call_log_fragment, container, false);
        mCallLogManager = CallLogManager.getInstance();
        mCallManager = CallManager.getInstance();
        mContactManager = ContactManager.getInstance();

        // Initialize list call log
        initListCallLog();

        // Initialize controls
        mListCallLog = (ListView)rootView.findViewById(R.id.listCallLog);
        progressLoading = (ProgressBar)rootView.findViewById(R.id.loading);
        mTxtNoCallLog = (TextView)rootView.findViewById(R.id.txtNoCallLog);
        progressLoading.setVisibility(View.GONE);

        // Create adapter
        if (mAdapter == null) {
            mAdapter = new CallLogAdatper(mBaseActivity, false, this);
            mAdapter.animateFirstListener = mBaseActivity.animateFirstListener;
            mAdapter.displayImageOptions = mBaseActivity.circleDisplayImageOptions;
            mContactManager.addContactChangeListener(mAdapter);
        }
        mListCallLog.setAdapter(mAdapter);
        mListCallLog.setOnItemClickListener(this);

        // Default view mode is view all.
        viewMode = VIEW_MODE_ALL;

        // Add handler of Call log change
        mCallLogManager.addCallLogChangeListener(this);
        mContactManager.addFirstSyncListener(mAdapter);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCallLogManager.removeCallLogChangeListener(this);
        mContactManager.removeFirstSyncListener(mAdapter);
        if (taskLoading != null) {
            taskLoading.cancel(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mReloadCallLogOnResume == true) {
            if (taskLoading != null) {
                taskLoading.cancel(true);
            }
            taskLoading = mCallLogManager.loadAllCallLog(0, 100, new CallLogManager.LoadCallLogListener() {
                @Override
                public void onLoadCallLogStart() {
                    postHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressLoading.setVisibility(View.VISIBLE);
                        }
                    });
                }

                @Override
                public void onLoadCallLogFinish(ArrayList<CallLogModel> listAllCall,
                        ArrayList<CallLogModel> listMissCall) {
                    progressLoading.setVisibility(View.GONE);
                    CallLogFragment.this.listAll = listAllCall;
                    CallLogFragment.this.listMissCall = listMissCall;
                    changeListMode(viewMode);
                }
            });

            mReloadCallLogOnResume = false;
        } else {
            changeListMode(viewMode);
        }
        if (mAdapter == null) {
            mAdapter.notifyDataSetChanged();
        }

        // Set miss call number to zero
        postHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setMissCallNotifyNumber(0);
            }
        }, 2000);
    }

    private void notifyDataChanged() {
        if (mAdapter.getCount() == 0) {
            mListCallLog.setVisibility(View.INVISIBLE);
            mTxtNoCallLog.setVisibility(View.VISIBLE);
            if (mBaseActivity instanceof MainTabActivity) {
                ((MainTabActivity)mBaseActivity).setCallEditVisible(false);
            }
        } else {
            mListCallLog.setVisibility(View.VISIBLE);
            mTxtNoCallLog.setVisibility(View.INVISIBLE);
            if (mBaseActivity instanceof MainTabActivity) {
                ((MainTabActivity)mBaseActivity).setCallEditVisible(true);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void initListCallLog() {
        listAll = mCallLogManager.getAllCallLog(0, 100);
        listMissCall = mCallLogManager.getAllMissCallLog(0, 100);
    }

    public void changeListMode(int mode) {
        viewMode = mode;
        if (viewMode == VIEW_MODE_ALL) {
            mAdapter.setData(listAll, mode);
        } else if (viewMode == VIEW_MODE_MISS_CALL) {
            mAdapter.setData(listMissCall, mode);
        }
        notifyDataChanged();
    }

    public void changeToEditMode() {
        setEditMode(true);
        mAdapter.setEditMode(isEditMode());
        changeListMode(viewMode);
    }

    public void changeToViewMode() {
        // Create adapter
        setEditMode(false);
        mAdapter.setEditMode(isEditMode());
        mAdapter.notifyDataSetChanged();
    }

    public CallLogAdatper getAdapter() {
        return mAdapter;
    }

    public void deleteSelected() {
        long[] ids = mAdapter.getCurrentCheckIds();
        if (ids.length == 0) {
            Toast.makeText(mBaseActivity, R.string.no_item_select, Toast.LENGTH_SHORT).show();
        } else {
            mCallLogManager.deleteCallLogs(mAdapter.getCurrentCheckIds());
            mAdapter.clearSelection();
        }
    }

    public void removeAllManagerListener() {
        mContactManager.removeContactChangeListener(mAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
        if (mAdapter != null) {
            if (mAdapter.isEditMode()) {
                boolean check = !mAdapter.isPositionChecked(position);
                if (check)
                    mAdapter.setNewSelection(position, check);
                else
                    mAdapter.removeSelection(position);

                // binhdt : setVisible Delete or not
                MainTabActivity acti = (MainTabActivity)getActivity();
                if (acti != null) {
                    long[] ids = mAdapter.getCurrentCheckIds();
                    acti.setLeftDeleteVisibility(ids.length > 0);
                }
            } else {
                String phoneNo = mAdapter.getPhoneNo(position);
                String sipAddress = Utils.getKATZNumber(phoneNo);
                mCallManager.initOutGoingSipCall(mBaseActivity, sipAddress);
            }
        }
    }

    @Override
    public void onNewCallLog(CallLogModel callLog) {
        synchronized (listAll) {
            listAll.add(0, callLog);
        }
    }

    @Override
    public void onCallLogChange(CallLogModel callLog) {
        // Update list misscall
        if (callLog.getType() == CallLogModel.TYPE_MISSCALL) {
            synchronized (listMissCall) {
                synchronized (listMissCall) {
                    listMissCall.add(0, callLog);
                }
            }
        }
        // Update list all
        synchronized (listAll) {
            int sizeMissCall = listAll.size();
            for (int i = 0; i < sizeMissCall; i++) {
                CallLogModel model = listAll.get(i);
                if (callLog.getDbId() == model.getDbId()) {
                    model.setDateMilliSecond(callLog.getDateMilliSecond());
                    model.setType(callLog.getType());
                    break;
                }
            }
        }
    }

    @Override
    public void onCallLogDeleted(long[] callLogIds) {
        initListCallLog();
        changeListMode(viewMode);
        mBaseActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataChanged();
            }
        });
        mReloadCallLogOnResume = true;
    }

    @Override
    public void onClickInfo(int position) {
        Intent intent = new Intent(getActivity(), ContactKATZOutDetailActivity.class);
        intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_VIEW_TYPE,
                ContactKATZOutDetailActivity.VIEW_TYPE_CALLLOG);
        intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_PHONE_NUMBER, mAdapter.getPhoneNo(position));
        long[] ids = mAdapter.getCallLogsIds(position);
        intent.putExtra(ContactKATZOutDetailActivity.INTENT_PARAM_LIST_CALLLOG, ids);
        intent.putExtra(BaseActivity.INTENT_PARAM_ANIMATION_TYPE, ActivityTransitionFactory.IN_RIGHT_OUT_RIGHT);
        mBaseActivity.startActivity(intent);
    }

    private void setMissCallNotifyNumber(int num) {
        if (mBaseActivity instanceof MainTabActivity) {
            ((MainTabActivity)mBaseActivity).setMissCallNotifyNumber(num);
            mCallLogManager.setMissCallNumToZero();
        }
    }
}
