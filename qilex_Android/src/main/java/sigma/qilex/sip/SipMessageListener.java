
package sigma.qilex.sip;

public interface SipMessageListener {
    /**
     * Woadomosc zostala wyslana
     */
    public void onMessageSent(long msgId);

    /**
     * Wiadomosc dotarla do nadawcy
     */
    public void onMessageConfirmed(long msgId, int msgStatus);

    /**
     * Blad podczas wysylania wiadomosci
     * 
     * @param msg
     */
    public void onMessageFailure(long msgId, int errorCode);

    /**
     * Otrzymano nowa wiadomosc
     */
    public void onIncomingMessage(SipMessage msg);

}
