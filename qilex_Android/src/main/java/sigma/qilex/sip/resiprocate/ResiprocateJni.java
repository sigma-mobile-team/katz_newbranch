
package sigma.qilex.sip.resiprocate;

public class ResiprocateJni {

    public static native void initialize();

    public static native void shutdown();

    public native void register(String userName, String password, String profileInstanceId, String token,
            int expiryTime, int republishfreq, int resubscribefreq, String ipAddress, String[] aliases, String privkey,
            String cert, String pubkey, String pushOs, String deviceId, String pushToken);

    public native void pingToAddress(String sipAddress);

    public native void unregister();

    public native void makeCall(String userName, String password, String profileInstanceId, String ipAddress,
            String peerUri, int expirationTime, boolean showNoTurnWaring, boolean isOUT);

    public native void resetServerIPs();

    public native void addSipServer(String ip, int port);

    public native void addTurnServer(String ip, int port);

    public native void answerCall();

    public native void pauseCall();

    public native void resumeCall();

    public native void rejectCall();

    public native void startAudio();

    public native float audioQuality();

    public native void stackRestart();

    public native void updateAudioConfig(boolean speakerOn, boolean headphonesOn, boolean mute);

    public native void sendMessage(String from, String to, String id, String timestamp, String content, String type,
            String uuid);
    
    public native void sendGroupChatMessageWithParticipant(String from, String to, String id, String timestamp, String content, String type,
            String uuid, String groupChatMsgType, String groupChatUuid, String groupChatPaticipants, String groupChatAdministrator);

    public native void sendGroupChatMessage(String from, String to, String id, String timestamp, String content, String type,
            String uuid, String groupChatMsgType, String groupChatUuid);
    
    public native void sendComposeMessage(String from, String to, String id, String timestamp, String content);
    
    public native void sendComposeGroupMessage(String from, String to, String id, String timestamp, String content
    		, String groupChatMsgType, String groupChatUuid);

    public native void publishPresence(int status);

    public native void subscribePresence(String pee);

    public native void unsubscribePresence(String peer);

    public native void setConnection(boolean wifi, boolean mobile3g, boolean mobile2g, boolean lte);

    public native void setPhonePerformance(float performance, boolean lowLatencyAudio, int androidVersion);

    public native void updateAudioVolume(float audioVolume);

    public native void updateBinding();

    public static native byte[] hmacSHA512(String number, String msg,String hashKeySign);
    
    public static native String certificatePEM();

    public static native String publicKey();

    public static native String privateKey();
}
