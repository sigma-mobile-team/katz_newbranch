
package sigma.qilex.sip.resiprocate;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Vector;

import android.net.Uri;
import android.provider.Settings.Secure;
import pl.katz.aero2.MyApplication;
import pl.katz.aero2.UserInfo;
import sigma.qilex.dataaccess.model.CallLogModel;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.manager.account.ServerAddr;
import sigma.qilex.manager.phone.CallLogManager;
import sigma.qilex.sip.SipCall;
import sigma.qilex.sip.SipMessage;
import sigma.qilex.sip.SipRegistrationListener;
import sigma.qilex.sip.resiprocate.SipContext.ConfirmMessage;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.QilexPhoneNumberUtils;
import sigma.qilex.utils.SimUtils;

/** Class state machine */
public class Statemachine extends Thread {
    public static final String DBG_TAG = new String("Statemachine");

    /** Context state machine */
    public static SipContext mContext;

    /**
     * Przesyla event do maszyny stanow
     * 
     * @param eventID
     */
    public void sendEvent(int eventID) {
        sendEvent(Events.values()[eventID]);
    }

    /** Przesyla event do maszyny stanow */
    public void sendEvent(Events event) {
        if (event == Events.EVENT_REGISTER_SIPSTACKRESTARTED)
            mRestarting = false;

        if (mRestarting)
            return;

        if (mRegistrationInstance != null) {
            mRegistrationInstance.privateSendEvent(event);
        }

        if (mCallInstance != null) {
            mCallInstance.privateSendEvent(event);
        }

        if (mMsgInstance != null) {
            mMsgInstance.privateSendEvent(event);
        }

        if (mPresenceInstance != null) {
            mPresenceInstance.privateSendEvent(event);
        }
        mInstance.privateSendEvent(event);

    }

    // Sekcja prywatna
    /** Obsluga rejestracji */
    private static boolean mRestarting = false;

    /** Obsluga rejestracji */
    private static Statemachine mRegistrationInstance = null;

    /** Obsluga rozmowy */
    private static Statemachine mCallInstance = null;

    /** Obsluga wiadomosci */
    private static Statemachine mMsgInstance = null;

    /** Obsluga presence */
    private static Statemachine mPresenceInstance = null;

    /** Instancja singletonu maszyny stanow */
    private static Statemachine mInstance = null;

    /** Kolejka zdarzen */
    private LinkedList<Events> mIncommingEvents;

    /** Aktualny stan */
    private State mState;

    /** Nazwa maszyny stanow */
    private String mName;

    /** Flaga zakonczenia pracy maszyny stanow */
    private boolean mShuttingDown;

    /** Lista stanow maszyny stanow */
    abstract class State {

        private String mStateName;

        public State(String name) {
            mStateName = name;
        }

        public String toString() {
            return mStateName;
        }

        public abstract void onEntry();

        public abstract void processEvent(Events event);

        public abstract void onExit();

    }

    /**
     * Wyslanie wiadomosci do konkretnej kolejki
     * 
     * @param event
     */
    private void privateSendEvent(Events event) {
        LogUtils.i(DBG_TAG, mName + " Statemachine.sendEvent(" + event.toString() + ")");
        synchronized (mIncommingEvents) {
            mIncommingEvents.add(event);
            // dumpQueue();
        }

        if (event == Events.EVENT_SHUTDOWN) {
            synchronized (mIncommingEvents) {
                mIncommingEvents.clear();
            }
            mShuttingDown = true;
        }
        interrupt();
    }

    /** Glowna metoda watku */
    @Override
    public void run() {
        while (!mShuttingDown) {
            Events event = null;
            try {
                while (true) {
                    synchronized (mIncommingEvents) {
                        event = mIncommingEvents.poll();
                    }
                    if (event == null)
                        break;
                    processSMEvent(event);
                }
            } catch (NoSuchElementException nsee) {
                LogUtils.e(DBG_TAG, mName + " Statemachine. consume event(" + event + ") " + nsee);
            }

            try {
                sleep(Integer.MAX_VALUE);
            } catch (InterruptedException e) {
            }
        }
        LogUtils.i(DBG_TAG, mName + " shutting down");
    }

    /**
     * Prywatny konstruktor
     * 
     * @param initialState nazwa pierwszego stanu
     * @param name nazwa stanu wspolbieznego
     */
    private Statemachine(String initialState, String name) {
        super(name);
        mIncommingEvents = new LinkedList<Events>();
        mName = name;
        mShuttingDown = false;
        mState = null;
        try {
            State firstTransitionTo = (State)this.getClass().getDeclaredField(initialState).get(this);
            transition(firstTransitionTo);
        } catch (Exception e) {
            LogUtils.e(DBG_TAG, mName + e.toString());
        }
    }

    /**
     * Funkcja zwraca instancje (singleton) maszyny stanow
     * 
     * @return
     */
    public static Statemachine instance() {
        if (mInstance == null) {
            mContext = new SipContext();
            mInstance = new Statemachine("INITIAL", "SM[MAIN]");
            mInstance.start();
        }
        return mInstance;
    }

    /**
     * Realizacja przejścia do stanu docelowego
     * 
     * @param targetState stan docelowy
     */
    private void transition(State targetState) {
        if (mState != null) {
            // LogUtils.i( DBG_TAG, mName +" " + mState.toString()+ ".onExit()" );
            mState.onExit();
            LogUtils.i(DBG_TAG, mName + " " + mState.toString() + " -> " + targetState.toString());
        } else {
            String logMessage = mName + " " + "null" + " -> " + targetState.toString();
            LogUtils.i(DBG_TAG, logMessage);
            mContext.logMessage(logMessage);
        }
        mState = targetState;
        if (mState != null) {
            // LogUtils.i( DBG_TAG, mName +" " + mState.toString()+".onEntry()" );
            mState.onEntry();
        }
    }

    /**
     * Glowna metoda przetwarzania eventow
     * 
     * @param event
     */
    private void processSMEvent(Events event) {
        if (event == null)
            return;

        String logMessage = mName + " " + mState.toString() + ".processEvent(" + event.name() + ")";
        LogUtils.i(DBG_TAG, logMessage);
        mContext.logMessage(logMessage);
        mState.processEvent(event);
    }

    // Lista stanow maszyny stanow

    /** Stan poczatkowy dla watku glownego */
    private State INITIAL = new State("INITIAL") {

        @Override
        public void onEntry() {
            mRegistrationInstance = new Statemachine("REGISTRATION_MAIN", "SM[REG]");
            mCallInstance = new Statemachine("CALL_MAIN", "SM[CALL]");
            mMsgInstance = new Statemachine("MESSAGE_MAIN", "SM[MSG]");
            mPresenceInstance = new Statemachine("PRESENCE_MAIN", "SM[PRES]");

            mRegistrationInstance.start();
            mCallInstance.start();
            mMsgInstance.start();
            mPresenceInstance.start();
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }
    };

    /** Stan glowny dla rejestracji */
    private State REGISTRATION_MAIN = new State("REGISTRATION_MAIN") {
        @Override
        public void onEntry() {
            transition(REGISTER_UNREGISTERED);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }
    };

    /** Stan glowny dla rozmow */
    private State CALL_MAIN = new State("CALL_MAIN") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_MAIN/onEntry");
            // Phuc add for debug - END
            transition(CALL_NOTREADY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }
    };

    /** Stan glowny dla przesylania wiadomosci */
    private State MESSAGE_MAIN = new State("MESSAGE_MAIN") {

        @Override
        public void onEntry() {
            transition(MESSAGE_NOTREADY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }
    };

    /** Stan glowny dla presence */
    private State PRESENCE_MAIN = new State("PRESENCE_MAIN") {
        @Override
        public void onEntry() {
            transition(PRESENCE_NOTREADY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }

    };

    /** Stan ERROR */
    private State ERROR = new State("ERROR") {

        @Override
        public void onEntry() {
            LogUtils.e(DBG_TAG, "stateERROR");
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_CALLEND:
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    if (mContext.mCallListener != null) {
                        mContext.mCallListener.onCallEnded(mContext.mCall);
                        mContext.mCallListener.onCallClosed(mContext.mCall);
                    }
                    transition(REGISTER_REGISTERED);
                    break;
                default:
                    break;
            }
        }
    };

    private State REGISTER_UNREGISTERED = new State("REGISTER_UNREGISTERED") {

        @Override
        public void onEntry() {
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {

                case EVENT_REGISTER:
                    mContext.mUnregisterExplicitlyCalled = false;
                    transition(REGISTER_REQUESTED);
                    break;
                case EVENT_REGISTER_SIPSTACKRESTART:
                    transition(REGISTER_SIPSTACKRESTARTING);
                    break;
                case EVENT_REGISTERINGUNKNOWNHOST:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            mContext.mRegistrationErrorCode, mContext.mRegistrationErrorMessage);
                    break;
                case EVENT_REGISTERED:
                    transition(REGISTER_REGISTERED);
                    break;

                case EVENT_REGISTERALLBINDINGSREMOVED:
                    // TODO: now its uncommented, related to
                    // https://redmine.redefine.pl/issues/72538
                    // Until explicitly called request registration
                    if (!mContext.mUnregisterExplicitlyCalled) {
                        transition(REGISTER_REQUESTED);
                    }
                    break;

                default:
                    break;

            }
        }
    };

    /** Restart stosu sip */
    private State REGISTER_SIPSTACKRESTARTING = new State("REGISTER_SIPSTACKRESTARTING") {
        @Override
        public void onEntry() {
            mContext.mResiprcocateJni.stackRestart();
            mRestarting = true;
        }

        @Override
        public void onExit() {
            mRestarting = false;
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_REGISTER_SIPSTACKRESTARTED:
                    transition(REGISTER_SIPSTACKRESTARTED);
                    break;
                default:
                    break;
            }
        }
    };

    /** Restart stosu sip */
    private State REGISTER_SIPSTACKRESTARTED = new State("REGISTER_SIPSTACKRESTARTED") {
        @Override
        public void onEntry() {
            mContext.mSipstackRestart = false;
            transition(REGISTER_REQUESTED);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_REGISTER:
                    transition(REGISTER_REQUESTED);
                    break;
                case EVENT_REGISTERALLBINDINGSREMOVED:
                    transition(REGISTER_REQUESTED);
                default:
                    break;
            }
        }
    };

    /** Wyslano zadanie rejestracji do biblioteki SIP */
    private State REGISTER_REQUESTED = new State("REGISTER_REQUESTED") {
        @Override
        public void onEntry() {
            if (mContext.mLocalProfile != null) {
                Vector<String> aliases = mContext.mLocalProfile.getAliases();
                String aliasesArray[] = null;
                if (aliases != null) {
                    aliasesArray = new String[aliases.size()];
                    System.arraycopy(aliases.toArray(), 0, aliasesArray, 0, aliases.size());
                }

                mContext.mResiprcocateJni.resetServerIPs();

                for (int i = 0; i < QilexProfile.sipServersCount(); i++) {
                    ServerAddr server = QilexProfile.getSipServer(i);
                    mContext.mResiprcocateJni.addSipServer(server.getAddr(), server.getPort());
                }

                for (int i = 0; i < QilexProfile.turnServersCount(); i++) {
                    ServerAddr server = QilexProfile.getTurnServer(i);
                    mContext.mResiprcocateJni.addTurnServer(server.getAddr(), server.getPort());
                }

                // Get push notification device info
                String pushOs = "Android";
                String deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(),
                        Secure.ANDROID_ID);
                String pushToken = UserInfo.getInstance().getPushToken();

                // Phuc add for debug - END
                LogUtils.d("SIGMA", "SIGMA-PHUCPV userName= " + mContext.mLocalProfile.getLogin());
                LogUtils.d("SIGMA", "SIGMA-PHUCPV password= " + mContext.mLocalProfile.getPassword());
                LogUtils.d("SIGMA", "SIGMA-PHUCPV profileInstanceId= " + mContext.mLocalProfile.getInstanceId());
                LogUtils.d("SIGMA", "SIGMA-PHUCPV token= " + mContext.mLocalProfile.getToken());
                LogUtils.d("SIGMA", "SIGMA-PHUCPV expiryTime= " + mContext.mRegistrationExpiryTime);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV republishfreq= " + mContext.mPublishRefreshTime);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV resubscribefreq= " + mContext.mSubscribeRefreshTime);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV ipAddress= " + null);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV aliases= " + aliasesArray);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV privkey= " + mContext.mLocalProfile.getPrivateKey());
                LogUtils.d("SIGMA", "SIGMA-PHUCPV cert= " + mContext.mLocalProfile.getCertificate());
                LogUtils.d("SIGMA", "SIGMA-PHUCPV pubkey= " + Uri.encode(mContext.mLocalProfile.getPublicKey()));
                LogUtils.d("SIGMA", "SIGMA-PHUCPV pushOs= " + pushOs);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV deviceId= " + deviceId);
                LogUtils.d("SIGMA", "SIGMA-PHUCPV pushToken= " + pushToken);
                mContext.mResiprcocateJni.register(mContext.mLocalProfile.getLogin(),
                        mContext.mLocalProfile.getPassword(), mContext.mLocalProfile.getInstanceId(),
                        mContext.mLocalProfile.getToken(), mContext.mRegistrationExpiryTime,
                        mContext.mPublishRefreshTime, mContext.mSubscribeRefreshTime, null, aliasesArray,
                        mContext.mLocalProfile.getPrivateKey(), mContext.mLocalProfile.getCertificate(),
                        mContext.mLocalProfile.getPublicKey(), pushOs, deviceId, pushToken);
                // }
            } else {

            }

        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function LOGIN: StateMachine.State REGISTER_REQUESTED - processEvent:"
                    + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_REGISTERING:
                    transition(REGISTER_REGISTERING);
                    break;

                case EVENT_REGISTER_SIPSTACKRESTART:
                    transition(REGISTER_SIPSTACKRESTARTING);
                    break;

                case EVENT_REGISTRATIONTIMEOUT:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            SipRegistrationListener.ERR_TIMEOUT, mContext.mRegistrationErrorMessage);
                    transition(REGISTER_UNREGISTERED);
                    break;

                case EVENT_REGISTERFAILED:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            mContext.mRegistrationErrorCode, mContext.mRegistrationErrorMessage);
                    transition(REGISTER_UNREGISTERED);
                    break;

                default:
                    break;
            }
        }
    };

    /** Otrzymano potwierdzenie o rozpoczeciu procesu rejestracji (logowania) */
    private State REGISTER_REGISTERING = new State("REGISTER_REGISTERING") {
        @Override
        public void onEntry() {
            mContext.mRegistrationListener.onRegistering(mContext.mLocalProfile);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_REGISTERED:
                    transition(REGISTER_REGISTERED);
                    break;

                case EVENT_REGISTERFAILED:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            mContext.mRegistrationErrorCode, mContext.mRegistrationErrorMessage);
                    transition(REGISTER_UNREGISTERED);
                    break;

                case EVENT_REGISTRATIONTIMEOUT:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            SipRegistrationListener.ERR_TIMEOUT, mContext.mRegistrationErrorMessage);
                    transition(REGISTER_UNREGISTERED);
                    break;

                default:

                    break;
            }
        }

    };

    /**
     * Otrzymano wiadomosc o pomyslnym zarejestrowaniu (zalogowaniu) uzytkownika
     * na serwerze
     */
    private State REGISTER_REGISTERED = new State("REGISTER_REGISTERED") {
        @Override
        public void onEntry() {
            mContext.mRegistrationListener.onRegistrationDone(mContext.mLocalProfile, mContext.mRegistrationExpiryTime);

        }

        @Override
        public void onExit() {
            mContext.mResiprcocateJni.unregister();
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_REGISTER:
                    transition(REGISTER_REQUESTED);
                    break;

                case EVENT_REGISTER_SIPSTACKRESTART:
                    transition(REGISTER_SIPSTACKRESTARTING);
                    break;

                case EVENT_REGISTRATIONTIMEOUT:
                case EVENT_REGISTERFAILED:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            SipRegistrationListener.ERR_TIMEOUT, mContext.mRegistrationErrorMessage);
                    transition(REGISTER_UNREGISTERED);
                    break;

                case EVENT_REGISTERUNREGISTER:
                    mContext.mUnregisterExplicitlyCalled = true;
                    transition(REGISTER_UNREGISTERED);
                    break;

                case EVENT_REGISTERALLBINDINGSREMOVED:
                    transition(REGISTER_REQUESTED);
                    break;

                default:
                    break;
            }
        }
    };

    private State CALL_NOTREADY = new State("CALL_NOTREADY") {

        @Override
        public void onEntry() {
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_NOTREADY/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_REGISTERED:
                    transition(CALL_READY);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                case EVENT_CALLEND:
                    if (mContext.mCall != null) {
                        mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    }
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    break;

                default:
                    break;
            }
        }
    };

    private State CALL_READY = new State("CALL_READY") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_READY/onEntry");
            // Phuc add for debug - END
            mContext.mResiprcocateJni.rejectCall();
            mContext.mWithoutTurnAcceptation = false;
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_READY/processEvent - event="
                    + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_REGISTER:
                case EVENT_REGISTERFAILED:
                case EVENT_REGISTERUNREGISTER:
                case EVENT_REGISTRATIONTIMEOUT:
                    transition(CALL_NOTREADY);
                    break;

                case EVENT_CALLMAKE:
                    transition(CALL_INITMAKECALL);
                    break;

                case EVENT_CALLNEWINCOMING:
                    transition(CALL_INCOMINGCALL);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                case EVENT_CALLFAILED:
                case EVENT_CALLOUTOFPAYMENT:
                case EVENT_CALLEND:
                case EVENT_CALLANSWERED:
                case EVENT_CALLNOTEXIST:
                    if (mContext.mCall != null) {
                        mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    }
                    if (mContext.mCallListener != null) {
                        mContext.mCallListener.onCallEnded(mContext.mCall);
                        mContext.mCallListener.onCallClosed(mContext.mCall);
                    }
                    break;

                case EVENT_CALLNEWSESSION:
                case EVENT_CALLRINGING:
                    mContext.mResiprcocateJni.rejectCall();
                    break;

                default:
                    break;
            }
        }
    };

    private boolean isNeedShowNoTurnWaring() {
        // zmiana zalozen - komunikat niezaleznie od isp return
        // QilexProfile.getIsp()!=null &&
        // QilexProfile.getIsp().equalsIgnoreCase("CP") &&
        // !mContext.mWithoutTurnAcceptation;
        return false;
        // return !mContext.mWithoutTurnAcceptation;
    }

    /** Inicjalizacja nawiazania polaczenia */
    private State CALL_INITMAKECALL = new State("CALL_INITMAKECALL") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_INITMAKECALL/onEntry");
            // Phuc add for debug - END
            String peerURI = mContext.mPeerProfile.getURI();
            mContext.mResiprcocateJni.makeCall(mContext.mLocalProfile.getLogin(), SimUtils.getIMSI(),//mContext.mLocalProfile.getPassword()
                    mContext.mLocalProfile.getInstanceId(), null, peerURI, mContext.mCallTimeoutMiliseconds,
                    isNeedShowNoTurnWaring(), mContext.mPeerProfile.isOUT());

            mContext.mCall.setCallState(SipCall.STATE_CALLING);
            mContext.mCallListener.onCallCalling(mContext.mCall);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_INITMAKECALL/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLFAILED:
                    mContext.mCall.setCallState(SipCall.ERR_UNKNOWN);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_UNKNOWN, "call failed");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                case EVENT_CALLOUTOFPAYMENT:
                    mContext.mCall.setCallState(SipCall.ERR_CALL_PAYMENT);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_CALL_PAYMENT, "callOUT failed");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                case EVENT_CALLINVALID:
                    mContext.mCall.setCallState(SipCall.ERR_CALL_INVALID);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_CALL_INVALID, "callOUT invalid");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                case EVENT_CALLNOTEXIST:
                    mContext.mCall.setCallState(SipCall.ERR_NOTCONNECTED);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_NOTCONNECTED, "call not exist");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLEND:
                    mContext.mResiprcocateJni.rejectCall();
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLTIMEOUT:
                    mContext.mCall.setCallState(SipCall.ERR_CALLING_TIMEOUT);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_PEER_NOT_FOUND, "timeout");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLREMOTEEND:
                    mContext.mCall.setCallState(SipCall.STATE_BUSY);
                    mContext.mCallListener.onCallBusy(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLUNKNOWNHOST:
                    mContext.mRegistrationListener.onRegistrationFailed(mContext.mLocalProfile,
                            mContext.mRegistrationErrorCode, mContext.mRegistrationErrorMessage);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLNEWSESSION:
                    transition(CALL_NEWSESSION);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                case EVENT_CALLTURNNOTAVAILABLE:
                    if (!mContext.mWithoutTurnAcceptation) {
                        transition(CALL_WITHOUTTURN);
                    }
                    break;
                default:
                    break;
            }
        }

    };

    /** Rozmowa bez dostepnosci serwera TURN */
    private State CALL_WITHOUTTURN = new State("CALL_WITHOUTTURN") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_WITHOUTTURN/onEntry");
            // Phuc add for debug - END
            mContext.mCallListener.onTurnNotAvailable(mContext.mCall);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_WITHOUTTURN/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLEND:
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLWITHOUTTURNACCEPTED:
                    mContext.mWithoutTurnAcceptation = true;
                    transition(CALL_INITMAKECALL);
                    break;

                default:
                    break;
            }
        }
    };

    private State CALL_NEWSESSION = new State("CALL_NEWSESSION") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_NEWSESSION/onEntry");
            // Phuc add for debug - END
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_NEWSESSION/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLFAILED:
                    mContext.mCall.setCallState(SipCall.ERR_UNKNOWN);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_UNKNOWN, "call failed");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                case EVENT_CALLOUTOFPAYMENT:
                    mContext.mCall.setCallState(SipCall.ERR_CALL_PAYMENT);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_CALL_PAYMENT, "callOUT failed");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                case EVENT_CALLNOTEXIST:
                    mContext.mCall.setCallState(SipCall.ERR_NOTCONNECTED);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_NOTCONNECTED, "call failed");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLRINGING:
                    mContext.mCall.setCallState(SipCall.STATE_CALLING_RINGING_BACK);
                    mContext.mCallListener.onCallRingingBack(mContext.mCall);
                    break;

                case EVENT_CALLREMOTEEND:
                    mContext.mResiprcocateJni.rejectCall();
                    mContext.mCall.setCallState(SipCall.STATE_BUSY);
                    mContext.mCallListener.onCallBusy(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                case EVENT_CALLANSWERED:
                    transition(CALL_ESTABLISHED);
                    break;

                case EVENT_CALLEND:
                    mContext.mResiprcocateJni.rejectCall();
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                default:
                    break;

            }

        }

    };

    private State CALL_INCOMINGCALL = new State("CALL_INCOMINGCALL") {

        @Override
        public void onEntry() {

            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_INCOMINGCALL/onEntry");
            // Phuc add for debug - END

            mContext.mCall = new SipCallImpl(mContext.mLocalProfile, mContext.mPeerProfile, mContext.mCallListener);
            mContext.mCall.setCallState(SipCall.STATE_INCOMING_CALL_RINGING);
            mContext.mIncomingCallListener.incomingCall(mContext.mCall);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_INCOMINGCALL/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLEND:
                    mContext.mResiprcocateJni.rejectCall();
                    transition(CALL_CALLENDCONFIRM);
                    break;

                case EVENT_CALLANSWER:
                    mContext.mResiprcocateJni.answerCall();
                    break;

                case EVENT_CALLANSWERED:
                    transition(CALL_ESTABLISHED);
                    break;

                case EVENT_CALLREMOTEEND:
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                default:
                    break;
            }
        }
    };

    private State CALL_CALLENDCONFIRM = new State("CALL_CALLENDCONFIRM") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_CALLENDCONFIRM/onEntry");
            // Phuc add for debug - END
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_CALLENDCONFIRM/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLREMOTEEND:
                case EVENT_CALLEND:
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    if (mContext.mCallListener != null) {
                        mContext.mCallListener.onCallEnded(mContext.mCall);
                        mContext.mCallListener.onCallClosed(mContext.mCall);
                    }
                    transition(CALL_READY);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                default:
                    break;
            }
        }

    };

    private State CALL_ESTABLISHED = new State("CALL_ESTABLISHED") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_ESTABLISHED/onEntry");
            // Phuc add for debug - END
            mContext.mCall.setCallState(SipCall.STATE_CALL_ESTABLISHED);
            mContext.mCallListener.onCallEstablished(mContext.mCall);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_ESTABLISHED/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLEND:
                    mContext.mResiprcocateJni.rejectCall();
                    transition(CALL_CALLENDCONFIRM);

                    break;

                case EVENT_CALLREMOTEEND:
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLSTARTAUDIO:
                    mContext.mResiprcocateJni.startAudio();
                    break;

                case EVENT_CALLAUDIOSTARTED:
                    transition(CALL_AUDIOSTARTED);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                default:
            }
        }

    };

    private State CALL_AUDIOSTARTED = new State("CALL_AUDIOSTARTED") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_AUDIOSTARTED/onEntry");
            // Phuc add for debug - END
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function CALL: StateMachine.java.State CALL_AUDIOSTARTED/processEvent - event="
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_CALLEND:
                    mContext.mResiprcocateJni.rejectCall();
                    transition(CALL_CALLENDCONFIRM);
                    break;
                case EVENT_RECONNECT:
                    mContext.mCallListener.onCallReconnect(mContext.mCall);
                    break;
                case EVENT_RECONNECTSUCCESS:
                    mContext.mCallListener.onCallReconnectSuccess(mContext.mCall);
                    break;
                case EVENT_CALLOUTOFPAYMENT:
                    mContext.mCall.setCallState(SipCall.ERR_CALLINGOUT_PAYMENT);
                    mContext.mCallListener.onCallError(mContext.mCall, SipCall.ERR_CALLINGOUT_PAYMENT,
                            "callOUT request payment");
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;
                case EVENT_CALLREMOTEEND:
                    mContext.mCall.setCallState(SipCall.STATE_ENDED);
                    mContext.mCallListener.onCallEnded(mContext.mCall);
                    mContext.mCallListener.onCallClosed(mContext.mCall);
                    transition(CALL_READY);
                    break;

                case EVENT_CALLAUDIOCONFIGUPDATE:
                    mContext.mResiprcocateJni.updateAudioConfig(mContext.mIsSpeakerOn, mContext.mIsHeadphonesOn,
                            mContext.mIsMuted);
                    mContext.mResiprcocateJni.updateAudioVolume(mContext.mAudioVolume);
                    break;

                default:

            }

        }

    };

    private State MESSAGE_NOTREADY = new State("MESSAGE_NOTREADY") {

        @Override
        public void onEntry() {
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_REGISTERED:
                    transition(MESSAGE_READY);
                    break;

                default:
                    break;
            }
        }
    };

    private State MESSAGE_READY = new State("MESSAGE_READY") {

        @Override
        public void onEntry() {
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            switch (event) {
                case EVENT_REGISTER:
                case EVENT_REGISTERFAILED:
                case EVENT_REGISTERUNREGISTER:
                case EVENT_REGISTRATIONTIMEOUT:
                    transition(MESSAGE_NOTREADY);
                    break;

                case EVENT_MESSAGESENDNEW:
                    transition(MESSAGE_SENDING);
                    break;

                case EVENT_MESSAGENEWINCOMING:
                    transition(MESSAGE_INCOMING);
                    break;

                case EVENT_MESSAGESENDFAILURE:
                    while (mContext.mFailedMessages.size() > 0 && mContext.mMessageListener != null) {
                        Long id = mContext.mFailedMessages.remove(0);
                        Integer errCode = mContext.mFailedErrorCodes.remove(0);
                        mContext.mMessageListener.onMessageFailure(id.longValue(), errCode.intValue());
                    }
                    break;

                case EVENT_MESSAGESENT:
                    while (mContext.mSentMessages.size() > 0 && mContext.mMessageListener != null) {
                        Long id = mContext.mSentMessages.remove(0);
                        mContext.mMessageListener.onMessageSent(id.longValue());
                    }
                    break;

                case EVENT_MESSAGESENTCONFIRMED:
                    while (mContext.mConfirmedMessages.size() > 0 && mContext.mMessageListener != null) {
                        ConfirmMessage cm = mContext.mConfirmedMessages.remove(0);
                        mContext.mMessageListener.onMessageConfirmed(cm.msgId, cm.msgStatus);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    private State MESSAGE_SENDING = new State("MESSAGE_SENDING") {

        @Override
        public void onEntry() {
            if (mContext.mOutgoingMessages.size() > 0) {
                SipMessage msg = mContext.mOutgoingMessages.remove(0);
                if (msg.isGroupChatMessage()) {
                	if (SipMessage.TYPE_COMPOSING.equals(msg.getType()) == false) {
                    	mContext.mResiprcocateJni.sendGroupChatMessage(msg.getFrom().getUriString(), msg.getTo().getUriString(),
    	                        Long.valueOf(msg.getId()).toString(), Long.valueOf(msg.getTimestamp()).toString(),
    	                        msg.getContent(), msg.getType(), msg.getUuid(),
    	                        msg.getGroupChatMsgType(), msg.getGroupChatUuid());
                	} else {
                        mContext.mResiprcocateJni.sendComposeGroupMessage(msg.getFrom().getUriString(),
                                msg.getTo().getUriString(), Long.valueOf(msg.getId()).toString(),
                                Long.valueOf(msg.getTimestamp()).toString(), msg.getContent(),
                                msg.getGroupChatMsgType(), msg.getGroupChatUuid());
                	}
                } else {
                	if (SipMessage.TYPE_COMPOSING.equals(msg.getType()) == false) {
                        mContext.mResiprcocateJni.sendMessage(msg.getFrom().getUriString(), msg.getTo().getUriString(),
                                Long.valueOf(msg.getId()).toString(), Long.valueOf(msg.getTimestamp()).toString(),
                                msg.getContent(), msg.getType(), msg.getUuid());
                    } else {
                        mContext.mResiprcocateJni.sendComposeMessage(msg.getFrom().getUriString(),
                                msg.getTo().getUriString(), Long.valueOf(msg.getId()).toString(),
                                Long.valueOf(msg.getTimestamp()).toString(), msg.getContent());
                    }
                }
            }
            transition(MESSAGE_READY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }
    };

    private State MESSAGE_INCOMING = new State("MESSAGE_INCOMING") {
        @Override
        public void onEntry() {
            while (mContext.mIncomingMessages.size() > 0) {
                SipMessage msg = mContext.mIncomingMessages.remove(0);
                mContext.mMessageListener.onIncomingMessage(msg);
            }
            transition(MESSAGE_READY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
        }
    };

    private State PRESENCE_NOTREADY = new State("PRESENCE_NOTREADY") {
        @Override
        public void onEntry() {
            // mContext.mPresenceStatus = Status.NOT_AVAILABLE;
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_NOTREADY/onEntry");
            // Phuc add for debug - END
        }

        @Override
        public void onExit() {
            // mContext.mResiprcocateJni.publishPresence(
            // mContext.mPresenceStatus.ordinal() );
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_NOTREADY/processEvent "
                            + event.toString());
            // Phuc add for debug - END
            switch (event) {
                case EVENT_REGISTERED:
                    mContext.mPresenceSubscriptionPool.renewAllSubscriptions();
                    transition(PRESENCE_READY);
                    break;

                default:
                    break;
            }
        }
    };

    private State PRESENCE_READY = new State("PRESENCE_READY") {
        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_READY/onEntry");
            // Phuc add for debug - END
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_READY/processEvent "
                    + event.toString());
            // Phuc add for debug - END
            switch (event) {
                // case EVENT_REGISTER:
                case EVENT_REGISTERFAILED:
                case EVENT_REGISTERUNREGISTER:
                case EVENT_REGISTRATIONTIMEOUT:
                    transition(PRESENCE_NOTREADY);
                    break;

                case EVENT_PRESENCEPUBLISH:
                    mContext.mResiprcocateJni.publishPresence(mContext.mPresenceStatus.ordinal());
                    break;

                case EVENT_PRESENCEPUBLISHED: {
                    LogUtils.i(DBG_TAG, "EVENT_PRESENCEPUBLISHED");
                    break;
                }

                case EVENT_PRESENCESUBSCRIBE:
                    transition(PRESENCE_SUBSCRIBE);
                    break;

                case EVENT_PRESENCESUBSCRIBED:
                    transition(PRESENCE_SUBSCRIBED);
                    break;

                case EVENT_PRESENCESUBSCRIPTIONUPDATE:
                    transition(PRESENCE_SUBSCRIPTIONUPDATE);
                    break;

                case EVENT_PRESENCEUNSUBSCRIBE:
                    transition(PRESENCE_UNSUBSCRIBE);
                    break;

                case EVENT_PRESENCESUBSCRIPTIONTERMINATED:
                    transition(PRESENCE_SUBSCRITIONTERMINATED);
                    break;

                default:
                    break;
            }
        }

    };

    private State PRESENCE_SUBSCRIBE = new State("PRESENCE_SUBSCRIBE") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRIBE/onEntry");
            // Phuc add for debug - END
            // immediate
            Vector<String> toBeRequestedImmediate = mContext.mPresenceSubscriptionPool
                    .getSubscriptionRequestedImmediate();
            Iterator<String> iterator = toBeRequestedImmediate.iterator();
            while (iterator.hasNext()) {
                String contact = iterator.next();
                // String aor = mContext.convertToAddressOfRequest(contact);
                LogUtils.d("subscriptionjni", "sub imm" + contact);
                mContext.mResiprcocateJni.subscribePresence(contact);
                mContext.mPresenceSubscriptionPool.setSubscriptinRequestDispatched(contact);
            }

            // non-immediate
            Vector<String> toBeRequested = mContext.mPresenceSubscriptionPool.getSubscriptionRequested();
            iterator = toBeRequested.iterator();
            while (iterator.hasNext()) {
                String contact = iterator.next();
                // String aor = mContext.convertToAddressOfRequest(contact);
                LogUtils.d("subscriptionjni", "sub laz" + contact);
                mContext.mResiprcocateJni.subscribePresence(contact);
                mContext.mPresenceSubscriptionPool.setSubscriptinRequestDispatched(contact);
            }
            transition(PRESENCE_READY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRIBE/processEvent "
                            + event.toString());
            // Phuc add for debug - END
        }

    };

    private State PRESENCE_SUBSCRIBED = new State("PRESENCE_SUBSCRIBED") {
        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA", "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRIBED/onEntry");
            // Phuc add for debug - END
            transition(PRESENCE_READY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRIBED/processEvent "
                            + event.toString());
            // Phuc add for debug - END
        }

    };

    private State PRESENCE_UNSUBSCRIBE = new State("PRESENCE_UNSUBSCRIBE") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_UNSUBSCRIBE/onEntry");
            // Phuc add for debug - END
            Vector<String> toBeRequested = mContext.mPresenceSubscriptionPool.getUnsubscribeRequested();
            Iterator<String> iterator = toBeRequested.iterator();
            while (iterator.hasNext()) {
                String contact = iterator.next();
                // String aor = mContext.convertToAddressOfRequest(contact);
                LogUtils.d("subscription", "unsub " + contact);
                mContext.mResiprcocateJni.unsubscribePresence(contact);
                mContext.mPresenceSubscriptionPool.setUnsubscriptionRequestDispatched(contact);
            }
            transition(PRESENCE_READY);

        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_UNSUBSCRIBE/processEvent "
                            + event.toString());
            // Phuc add for debug - END
        }

    };

    private State PRESENCE_SUBSCRITIONTERMINATED = new State("PRESENCE_SUBSCRITIONTERMINATED") {
        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRITIONTERMINATED/onEntry");
            // Phuc add for debug - END
            transition(PRESENCE_READY);
        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRITIONTERMINATED/processEvent "
                            + event.toString());
            // Phuc add for debug - END
        }

    };

    private State PRESENCE_SUBSCRIPTIONUPDATE = new State("PRESENCE_SUBSCRIPTIONUPDATE") {

        @Override
        public void onEntry() {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRIPTIONUPDATE/onEntry");
            // Phuc add for debug - END
            mContext.mPresenceSubscriptionPool.notifyListener();
            transition(PRESENCE_READY);

        }

        @Override
        public void onExit() {
        }

        @Override
        public void processEvent(Events event) {
            // Phuc add for debug - START
            LogUtils.d("SIGMA",
                    "SIGMA-PHUCPV DEBUG function PRESENCE: StateMachine.java.State PRESENCE_SUBSCRIPTIONUPDATE/processEvent "
                            + event.toString());
            // Phuc add for debug - END
        }

    };

    public void handlerPingResponse(final String status, final String from, final String to,
            final String offlineMinute) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int minute = 0;
                if (offlineMinute != null) {
                    minute = Integer.parseInt(offlineMinute) / 60 + 1;
                }
                mContext.mSipPingStatusListener.onPingResult(new String(from), new String(to), Integer.parseInt(status),
                        minute);
            }
        }).start();
    }

    public void handlerPingRequest(final String from, String to) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mContext.mSipPingStatusListener.onReceiveRequest(new String(from));
            }
        }).start();
    }

    public void addMissCall(final String from) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String phoneNumber = QilexPhoneNumberUtils.convertToBasicNumber(new String(from));
                CallLogManager.getInstance().putNewCallLog(phoneNumber, CallLogModel.TYPE_MISSCALL);
            }
        }).start();
    }
}
