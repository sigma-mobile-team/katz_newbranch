
package sigma.qilex.sip.resiprocate;

import java.io.UnsupportedEncodingException;
import java.util.Vector;

import android.util.Log;
import android.util.Pair;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipCall.SipCallListener;
import sigma.qilex.sip.SipDebugLogListener;
import sigma.qilex.sip.SipIncomingCallListener;
import sigma.qilex.sip.SipMessage;
import sigma.qilex.sip.SipMessageListener;
import sigma.qilex.sip.SipPingStatusListener;
import sigma.qilex.sip.SipPresenceListener;
import sigma.qilex.sip.SipRegistrationListener;
import sigma.qilex.utils.LogUtils;

/**
 * Kontekst dla maszyny stanow i pozostalych klas
 */
public class SipContext {
    public static final String DBG_TAG = "SipContext";

    // profiles
    public QilexSipProfile mLocalProfile = new QilexSipProfile();

    public QilexSipProfile mPeerProfile = new QilexSipProfile();

    public String mCallId = null;

    // manager
    public SipManagerImpl mManager = null;

    public boolean mShuttingDown = false;

    // native interface
    public ResiprocateJni mResiprcocateJni = new ResiprocateJni();

    // registration
    public SipRegistrationListener mRegistrationListener = null;

    public SipPingStatusListener mSipPingStatusListener = null;

    public int mRegistrationExpiryTime = 0;

    public int mPublishRefreshTime = 0;

    public int mSubscribeRefreshTime = 0;

    public int mRegistrationErrorCode = SipRegistrationListener.ERR_NETWORK_DOWN;

    public String mRegistrationErrorMessage = null;

    public boolean mSipstackRestart = false;

    public boolean mUnregisterExplicitlyCalled = false;

    // call
    public SipCallImpl mCall = null;

    public SipCallListener mCallListener = null;

    public SipIncomingCallListener mIncomingCallListener = null;

    public int mCallTimeoutMiliseconds = 0;

    public boolean mIsMuted = false;

    public boolean mIsSpeakerOn = false;

    public boolean mWithoutTurnAcceptation = false;

    public boolean mIsHeadphonesOn = false;

    public float mAudioVolume = -1.0f;

    // messages
    public SipMessageListener mMessageListener;

    public Vector<SipMessage> mOutgoingMessages = new Vector<SipMessage>();

    public Vector<SipMessage> mIncomingMessages = new Vector<SipMessage>();

    public Vector<Long> mSentMessages = new Vector<Long>();

    public Vector<ConfirmMessage> mConfirmedMessages = new Vector<ConfirmMessage>();

    public Vector<Long> mFailedMessages = new Vector<Long>();

    public Vector<Integer> mFailedErrorCodes = new Vector<Integer>();

    // presence
    public SipPresenceListener.Status mPresenceStatus;

    public SipPresenceListener mPresenceListener;

    public Vector<Pair<String, Pair<SipPresenceListener.Status, String>>> mPresenceUpdates = new Vector<Pair<String, Pair<SipPresenceListener.Status, String>>>();

    public PresenceSubscriptionPool mPresenceSubscriptionPool = null;

    public int re;

    public int republishfreq;

    public int resubscribefreq;

    // debug log listener
    public SipDebugLogListener mDebugLogListener = null;

    public void setPeerUri(String peerUri) {
        try {
            mPeerProfile = new QilexSipProfile(peerUri);
        } catch (Exception e) {
            LogUtils.e(Statemachine.DBG_TAG, e.toString());
        }
    }

    public void setCallId(String callId) {
        mCallId = callId;
    }

    public void addIncomingMessage(String from, String to, String id, String timestamp, String content, String type,
            String uuid) {
        SipMessage msg = new SipMessage(from, to, id, timestamp, content, type);
        msg.setUuid(uuid);
        mIncomingMessages.add(msg);
    }
    
    public void addIncomingMessage(String from, String to, String id, String timestamp, String content, String type,
            String uuid, String groupChatMsgType, String groupChatUuid, String groupChatParticipants, String groupChatAdministrator) {
        SipMessage msg = new SipMessage(from, to, id, timestamp, content, type);
        msg.setUuid(uuid);
        msg.setGroupChatAdministrator(groupChatAdministrator);
        msg.setGroupChatMsgType(groupChatMsgType);
        msg.setGroupChatParticipants(groupChatParticipants);
        msg.setGroupChatUuid(groupChatUuid);
        mIncomingMessages.add(msg);
    }
    
    public void addIncomingMessage(String from, String to, String id, String timestamp, byte[] content, String type,
            String uuid) {
    	String strcontent = "";
    	try {
			strcontent = new String(content, "UTF-8");
		} catch (UnsupportedEncodingException e) {
	    	strcontent = "";
		}
    	
        SipMessage msg = new SipMessage(from, to, id, timestamp, strcontent, type);
        msg.setUuid(uuid);
        mIncomingMessages.add(msg);
    }
    
    public void addIncomingMessage(String from, String to, String id, String timestamp, byte[] content, String type,
            String uuid, String groupChatMsgType, String groupChatUuid, String groupChatParticipants, String groupChatAdministrator) {
    	String strcontent = "";
    	try {
			strcontent = new String(content, "UTF-8");
		} catch (UnsupportedEncodingException e) {
	    	strcontent = "";
		}
    	SipMessage msg = new SipMessage(from, to, id, timestamp, strcontent, type);
        msg.setUuid(uuid);
        msg.setGroupChatAdministrator(groupChatAdministrator);
        msg.setGroupChatMsgType(groupChatMsgType);
        msg.setGroupChatParticipants(groupChatParticipants);
        msg.setGroupChatUuid(groupChatUuid);
        mIncomingMessages.add(msg);
    }

    public void addSentMessage(String msgId) {
        mSentMessages.add(Long.valueOf(msgId));
    }

    public void addConfirmedMessage(String msgId, String confirmCode) {
        // if code ==200 OK
        ConfirmMessage cm = new ConfirmMessage(Long.valueOf(msgId));
        cm.msgStatus = Integer.valueOf(confirmCode);
        mConfirmedMessages.add(cm);
    }

    public void addFailedMessage(String msgId, String strErr) {
        mFailedMessages.add(Long.valueOf(msgId));
        mFailedErrorCodes.add(Integer.valueOf(strErr));
    }

    public void addPresenceSubscribed(String peer) {
        // update peer's domain with user profile domain (or ip address)
        String[] split = peer.split("@");

        if (split.length == 2) {
            String subscribedPeer = split[0] + "@" + mLocalProfile.getServer();
            mPresenceSubscriptionPool.setSubscriptionRequestConfirmed(subscribedPeer);
        } else {
            LogUtils.e(DBG_TAG, "addPresenceSubscribed error");
        }
    }

    public void addPresenceUnsubscribed(String peer) {
        String[] split = peer.split("@");
        if (split.length == 2) {
            String subscribedPeer = split[0] + "@" + mLocalProfile.getServer();
            mPresenceSubscriptionPool.setUnsubscriptionRequestConfirmed(subscribedPeer);
        }
    }

    public void addPresenceUpdate(String peer, int status, String statusMsg) {
        String[] split = peer.split("@");
        String subscribedPeer = null;
        if (split.length == 2) {
            subscribedPeer = split[0] + "@" + mLocalProfile.getServer();
        }

        mPresenceSubscriptionPool.onStatusUpdate(subscribedPeer, SipPresenceListener.Status.values()[status]);
    }

    public void publishKeyValue(String key, String value) {
        if (mDebugLogListener != null) {
            mDebugLogListener.onKeyValueUpdate(key, value);
        }
    }

    public void logMessage(String logMessage) {
        if (mDebugLogListener != null) {
            mDebugLogListener.onLogMessage(logMessage);
        }
    }

    public void updateBinding() {
        mResiprcocateJni.updateBinding();
    }

    public void setConnection(boolean wifi, boolean mobile3g, boolean mobile2g, boolean lte) {
        mResiprcocateJni.setConnection(wifi, mobile3g, mobile2g, lte);
    }

    public class ConfirmMessage {
        public long msgId;

        public int msgStatus;

        public ConfirmMessage(long id) {
            msgId = id;
        }
    }

}
