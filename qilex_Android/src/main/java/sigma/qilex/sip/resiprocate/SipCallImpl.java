
package sigma.qilex.sip.resiprocate;

import android.os.Build;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipCall;

public class SipCallImpl extends SipCall {

    private long mCallStartTime;

    public SipCallImpl(QilexSipProfile localProfile, QilexSipProfile peerProfile, SipCallListener callListener) {
        super(localProfile, peerProfile);
        setListener(callListener);
        Statemachine.mContext.mResiprcocateJni.setPhonePerformance(1.0f, false, Build.VERSION.SDK_INT);
        mCallStartTime = 0;
    }

    @Override
    public void answerCall(int timeout) {
        Statemachine.instance().sendEvent(Events.EVENT_CALLANSWER);

    }

    @Override
    public void pauseCall() {
        Statemachine.mContext.mResiprcocateJni.pauseCall();
    }

    @Override
    public void resumeCall() {
        Statemachine.mContext.mResiprcocateJni.resumeCall();
    }

    @Override
    public void endCall() {
        Statemachine.instance().sendEvent(Events.EVENT_CALLEND);
    }

    @Override
    public boolean isMuted() {
        return Statemachine.mContext.mIsMuted;
    }

    @Override
    public boolean isSpeakerOn() {
        return Statemachine.mContext.mIsSpeakerOn;
    }

    @Override
    public void setMute(boolean muted) {
        Statemachine.mContext.mIsMuted = muted;
        Statemachine.instance().sendEvent(Events.EVENT_CALLAUDIOCONFIGUPDATE);
    }

    @Override
    public void setSpeakerOn(boolean speakerOn) {
        Statemachine.mContext.mIsSpeakerOn = speakerOn;
        Statemachine.instance().sendEvent(Events.EVENT_CALLAUDIOCONFIGUPDATE);
    }

    @Override
    public void startAudio() {
        mCallStartTime = System.currentTimeMillis();
        Statemachine.instance().sendEvent(Events.EVENT_CALLSTARTAUDIO);
    }

    /**
     * Obtain real-time quality rating of the call Based on local RTP statistics
     * and RTCP feedback, a quality rating is computed and updated during all
     * the duration of the call. This function returns its value at the time of
     * the function call. It is expected that the rating is updated at least
     * every 5 seconds or so. The rating is a floating point number comprised
     * between 0 and 5. 4-5 = good quality <br>
     * 3-4 = average quality <br>
     * 2-3 = poor quality <br>
     * 1-2 = very poor quality <br>
     * 0-1 = can't be worse, mostly unusable <br>
     *
     * @return The function returns -1 if no quality measurement is available,
     *         for example if no active audio stream exist. Otherwise it returns
     *         the quality rating.
     **/
    @Override
    public float audioQuality() {
        return Statemachine.mContext.mResiprcocateJni.audioQuality() * 5.0f;
    }

    @Override
    public long getCurrCallTimeMillis() {
        if (mCallStartTime == 0) {
            return 0;
        }
        return System.currentTimeMillis() - mCallStartTime;
    }

    @Override
    public void setListener(SipCallListener listener) {
        super.setListener(listener);
        Statemachine.mContext.mCallListener = listener;
    }

    public void setCallState(int state) {
        super.setState(state);
    }

    @Override
    public void setHeadphonesOn(boolean headphoneOn) {
        Statemachine.mContext.mIsHeadphonesOn = headphoneOn;
        Statemachine.instance().sendEvent(Events.EVENT_CALLAUDIOCONFIGUPDATE);

    }

    @Override
    public void setAudioVolume(float volume) {
        Statemachine.mContext.mAudioVolume = volume;
        Statemachine.instance().sendEvent(Events.EVENT_CALLAUDIOCONFIGUPDATE);
    }
}
