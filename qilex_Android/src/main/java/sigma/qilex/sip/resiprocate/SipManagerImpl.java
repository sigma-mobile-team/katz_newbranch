
package sigma.qilex.sip.resiprocate;

import java.util.Vector;

import android.content.Context;
import sigma.qilex.sip.QilexSipProfile;
import sigma.qilex.sip.SipCall;
import sigma.qilex.sip.SipCall.SipCallListener;
import sigma.qilex.sip.SipDebugLogListener;
import sigma.qilex.sip.SipManager;
import sigma.qilex.sip.SipMessage;
import sigma.qilex.sip.SipMessageListener;
import sigma.qilex.sip.SipPresenceListener;
import sigma.qilex.sip.SipPresenceListener.Status;
import sigma.qilex.sip.SipRegistrationListener;
import sigma.qilex.utils.LogUtils;

public class SipManagerImpl extends SipManager {
    private static final String DBG_TAG = new String("SipManagerImpl");

    private Statemachine mStatemachine;

    private PresenceSubscriptionPool mPresenceSubscriptionPool;

    /**
     * Constructor
     * 
     * @param context
     */
    public SipManagerImpl(Context context) {
        mStatemachine = Statemachine.instance();
        Statemachine.mContext.mManager = this;
        mPresenceSubscriptionPool = new PresenceSubscriptionPool(mStatemachine);
        Statemachine.mContext.mPresenceSubscriptionPool = mPresenceSubscriptionPool;
        Statemachine.mContext.mPresenceListener = mPresenceSubscriptionPool;
        if (!nativeLibraryInit(context)) {
            // TODO: error escalation
        } else {
            ResiprocateJni.initialize();
        }
    }

    private boolean nativeLibraryInit(Context context) {

        boolean libraryLoaded = false;
        try {
            System.loadLibrary("freefonjni");
            libraryLoaded = true;
            LogUtils.d(DBG_TAG, "freefonjni loaded");
        } catch (UnsatisfiedLinkError e) {
            LogUtils.d(DBG_TAG, "native library can't be loaded");
        } catch (SecurityException e) {
            LogUtils.d(DBG_TAG, "native library load - SecurityException");
        }

        return libraryLoaded;
    }

    @Override
    public void register(QilexSipProfile profile, int registrationExpiryTime, int publishRefreshTime,
            int subscribeRefreshTime, SipRegistrationListener registrationListener, boolean withSipstackRestart) {
        Statemachine.mContext.mLocalProfile = profile;
        Statemachine.mContext.mRegistrationListener = registrationListener;
        Statemachine.mContext.mRegistrationExpiryTime = registrationExpiryTime;
        Statemachine.mContext.mPublishRefreshTime = publishRefreshTime;
        Statemachine.mContext.mSubscribeRefreshTime = subscribeRefreshTime;
        Statemachine.mContext.mIncomingCallListener = mIncomingCallListener;
        Statemachine.mContext.mSipstackRestart = withSipstackRestart;

        if (withSipstackRestart)
            mStatemachine.sendEvent(Events.EVENT_REGISTER_SIPSTACKRESTART);
        else
            mStatemachine.sendEvent(Events.EVENT_REGISTER);
    }

    @Override
    public void unregister(QilexSipProfile localProfile, SipRegistrationListener listener) {
        mPresenceSubscriptionPool.unsubscribeAllPresences();
        mStatemachine.sendEvent(Events.EVENT_REGISTERUNREGISTER);
    }

    @Override
    public void sendMessage(SipMessage msg) {
        // wysyłamy wiadomości które jakimś cudem są na liście
        int vrlSize = Statemachine.mContext.mOutgoingMessages.size();
        for (int i = 0; i < vrlSize; ++i) {
            LogUtils.d("sendMessage", "wysyłamy niewysłane");
            Statemachine.instance().sendEvent(Events.EVENT_MESSAGESENDNEW);
        }

        if (msg != null) {
            Statemachine.mContext.mOutgoingMessages.add(msg);
            Statemachine.instance().sendEvent(Events.EVENT_MESSAGESENDNEW);
        }
    }

    @Override
    public void setMessageListener(SipMessageListener messageListener) {
        mMessageListener = messageListener;
        Statemachine.mContext.mMessageListener = mMessageListener;
    }

    @Override
    public void makeCallWithoutTurn() {
        mStatemachine.sendEvent(Events.EVENT_CALLWITHOUTTURNACCEPTED);
    }

    @Override
    public SipCall makeCall(QilexSipProfile localProfile, QilexSipProfile peerProfile, SipCallListener callListener,
            int timeoutSeconds) {
        // Phuc add for debug - START
        LogUtils.d("KUNLQT", "SIGMA-PHUCPV DEBUG function CALL: SipManagerImpl.java/makeCall - peerProfile uriString="
                + peerProfile.getURI());
        // Phuc add for debug - END
        Statemachine.mContext.mLocalProfile = localProfile;
        Statemachine.mContext.mPeerProfile = peerProfile;
        Statemachine.mContext.mCallListener = callListener;
        Statemachine.mContext.mCallTimeoutMiliseconds = timeoutSeconds * 1000;

        Statemachine.mContext.mCall = new SipCallImpl(Statemachine.mContext.mLocalProfile,
                Statemachine.mContext.mPeerProfile, callListener);

        mStatemachine.sendEvent(Events.EVENT_CALLMAKE);

        return Statemachine.mContext.mCall;
    }

    @Override
    public void teardown() {
        mStatemachine.sendEvent(Events.EVENT_SHUTDOWN);
        ResiprocateJni.shutdown();
    }

    @Override
    public void publishPresence(Status status) {
        LogUtils.d(DBG_TAG, "publishPresence " + status);

        Statemachine.mContext.mPresenceStatus = status;
        mStatemachine.sendEvent(Events.EVENT_PRESENCEPUBLISH);
    }

    @Override
    public void subscribePresence(Vector<String> contactList, boolean immediate) {
        mPresenceSubscriptionPool.subscribePresence(contactList, immediate);
    }

    @Override
    public void unsubscribePresence(Vector<String> contactList) {
        mPresenceSubscriptionPool.unsubscribePresence(contactList);
    }

    @Override
    public void setPresenceListener(SipPresenceListener presenceListener) {
        mPresenceSubscriptionPool.setPresenceListener(presenceListener);
    }

    /* ========== Debug Log Listener ========== */

    @Override
    public void setDebugLogListener(SipDebugLogListener listener) {
        Statemachine.mContext.mDebugLogListener = listener;
    }

    public void resetPresence() {
        mPresenceSubscriptionPool = null;
        mPresenceSubscriptionPool = new PresenceSubscriptionPool(mStatemachine);
        Statemachine.mContext.mPresenceSubscriptionPool = mPresenceSubscriptionPool;
        Statemachine.mContext.mPresenceListener = mPresenceSubscriptionPool;
    }

    @Override
    public void pingToAddress(String sipAddress) {
        Statemachine.mContext.mResiprcocateJni.pingToAddress(sipAddress);
    }

}
