
package sigma.qilex.sip.resiprocate;

import sigma.qilex.sip.SipPresenceListener.Status;

/**
 * Przechowuje dane zwiazane z subskrypcja pojedynczego kontaktu oraz stanem
 * zapytania
 */
public class PresenceSubscriptionItem {

    public enum RequestStatus {
        UNSUBSCRIBED, SUBSCRIBE, SUBSCRIBE_DISPATCHED, SUBSCRIBED, UNSUBSCRIBE, UNSUBSCRIBE_DISPATCHED,
    }

    private long mSubscriptionTime;

    private long mLatestStatusTime;

    private Status mPresenceStatus;

    private RequestStatus mRequestStatus;

    private boolean mImmediateSubscription;

    public PresenceSubscriptionItem() {
        mSubscriptionTime = System.currentTimeMillis();
        mLatestStatusTime = 0;
        mPresenceStatus = Status.NOT_AVAILABLE;
        mRequestStatus = RequestStatus.UNSUBSCRIBED;
        mImmediateSubscription = false;
    }

    public void setPresenceStatus(Status status) {
        mPresenceStatus = status;
        mLatestStatusTime = System.currentTimeMillis();
    }

    public Status getPresenceStatus() {
        return mPresenceStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        mRequestStatus = requestStatus;
    }

    public boolean isReadyForUnsubscription() {
        return false;
    }

    public void subscribe() {
        mRequestStatus = RequestStatus.SUBSCRIBE;
    }

    public void unsubscribe() {
        mRequestStatus = RequestStatus.UNSUBSCRIBE;
    }

    public boolean isSubscribed() {
        boolean returnValue;

        switch (mRequestStatus) {
            case SUBSCRIBED:
            case UNSUBSCRIBE:
            case UNSUBSCRIBE_DISPATCHED:
                returnValue = true;
                break;
            case SUBSCRIBE:
            case SUBSCRIBE_DISPATCHED:
            case UNSUBSCRIBED:
            default:
                returnValue = false;
                break;
        }
        return returnValue;
    }

    public RequestStatus getRequestStatus() {
        return mRequestStatus;
    }

    public void setImmediate(boolean immediateSubscriptionFlag) {
        mImmediateSubscription = immediateSubscriptionFlag;
    }

    public boolean isImmediate() {
        return mImmediateSubscription;
    }

    public String toString() {
        return mRequestStatus.toString() + " " + mPresenceStatus.toString();
    }
}
