
package sigma.qilex.sip.resiprocate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;

import sigma.qilex.sip.SipPresenceListener;
import sigma.qilex.sip.resiprocate.PresenceSubscriptionItem.RequestStatus;
import sigma.qilex.utils.LogUtils;

/**
 * Utrzymuje subskrypcje statusow w celu ograniczenia ilosci operacji
 * subskrypcja - zakonczenie subskrypcji. Cykl subskrypcji: +---> SUBSCRIBE
 * ----> SUBSCRIBE_DISPATCHED ----> SUBSCRIBED UNSUBSCRIBED <---
 * UNSUBSCRIBE_DISPATCHED <------------ UNSUBSCRIBE
 */
public class PresenceSubscriptionPool implements SipPresenceListener {
    public static final String DBG_TAG = "PresenceSubscriptionPool";

    private SipPresenceListener mPresenceListener;

    private Statemachine mStatemachine;

    private HashMap<String, PresenceSubscriptionItem> mSubscriptions;

    // private Thread mUnsubscribeThread;
    private Thread mLazySubscription;

    private ConcurrentLinkedQueue<String> mLazySubscriptions;

    /**
     * Konstruktor
     * 
     * @param statemachine
     */
    public PresenceSubscriptionPool(Statemachine statemachine) {
        mStatemachine = statemachine;
        mSubscriptions = new HashMap<String, PresenceSubscriptionItem>();
        mLazySubscription = null;
        mLazySubscriptions = new ConcurrentLinkedQueue<String>();

    }

    /**
     * Subskrypcja kontaktow
     * 
     * @param contactList
     */
    public void subscribePresence(Vector<String> contactList, boolean immediateFlag) {
        // if (immediateFlag)
        // {
        // Iterator<String> i = contactList.iterator();
        // boolean requestSubscription = false;
        // while (i.hasNext())
        // {
        // String contact = i.next();
        // synchronized (mSubscriptions) {
        // PresenceSubscriptionItem item = mSubscriptions.get(contact);
        //
        // if (item == null) {
        // item = new PresenceSubscriptionItem();
        // item.setImmediate(true);
        // mSubscriptions.put(contact, item);
        // }
        // if (item.isSubscribed())
        // {
        // onStatusUpdate(contact, item.getPresenceStatus());
        // }
        // else
        // {
        // item.setImmediate(true);
        // item.subscribe();
        // LogUtils.i(DBG_TAG, "immediate subscription " + contact);
        // requestSubscription = true;
        // }
        // }
        // }
        // if (requestSubscription)
        // {
        // mStatemachine.sendEvent(Events.EVENT_PRESENCESUBSCRIBE);
        // }
        // }
        // else
        // {
        LogUtils.d(DBG_TAG, "subscribePresence");
        lazySubscription(contactList);
        // }

        //
    }

    private long vrcLastLazySubscriptionInvokedTime = 0;

    private void lazySubscription(Vector<String> contactList) {
        if (contactList.isEmpty())
            return;

        // vvv To rozwiazanie psuje dzialanie statusow! Gubimy subskrypcje
        // //dajemy ograniczenie na 5 sekund. bo to potem przerasta możlwiości
        // telefonu
        // if (System.currentTimeMillis() - vrcLastLazySubscriptionInvokedTime <
        // 5000)
        // {
        // return;
        // }

        LogUtils.d(DBG_TAG, "Lazy subscription :");
        mLazySubscriptions.addAll(contactList);

        if (mLazySubscription == null) {
            // vrcLastLazySubscriptionInvokedTime = System.currentTimeMillis();
            mLazySubscription = new Thread(new Runnable() {
                public void run() {
                    try {
                        String contact = mLazySubscriptions.poll();
                        while (contact != null) {
                            synchronized (mSubscriptions) {
                                PresenceSubscriptionItem item = mSubscriptions.get(contact);
                                if (item == null) {
                                    item = new PresenceSubscriptionItem();
                                    item.setImmediate(false);
                                    item.subscribe();
                                    mSubscriptions.put(contact, item);
                                    mStatemachine.sendEvent(Events.EVENT_PRESENCESUBSCRIBE);
                                    LogUtils.d(DBG_TAG, "subscribed " + contact);
                                    // try {
                                    // Thread.sleep(50);
                                    // } catch (InterruptedException e) {
                                    // LogUtils.e(DBG_TAG,"",e);
                                    // }
                                } else {
                                    // LogUtils.d(DBG_TAG,"mPresenceListener.onStatusUpdate");
                                    // mPresenceListener.onStatusUpdate(
                                    // contact, item.getPresenceStatus() );
                                }

                                contact = mLazySubscriptions.poll();
                            }
                        }
                    } catch (Throwable e) {
                    }
                    mLazySubscription = null;
                }

            });
            mLazySubscription.start();
        }

    }

    /**
     * Zakonczenie wybranych subskrypcji
     * 
     * @param contactList
     */
    public void unsubscribePresence(Vector<String> contactList) {
        Iterator<String> i = contactList.iterator();
        while (i.hasNext()) {
            String contact = i.next();

            synchronized (mSubscriptions) {
                PresenceSubscriptionItem item = mSubscriptions.get(contact);

                if (item != null) {
                    item.unsubscribe();
                } else {
                    LogUtils.e(DBG_TAG, "unsubscribePresence error");
                }
            }
        }
        mStatemachine.sendEvent(Events.EVENT_PRESENCEUNSUBSCRIBE);
    }

    /**
     * 
     */
    public void unsubscribeAllPresences() {
        unsubscribePresence(getSubscribed());
        boolean allSubscriptionsRemoved = false;

        while (!allSubscriptionsRemoved) {
            try {
                Thread.sleep(1000);
                allSubscriptionsRemoved = getSubscribed().isEmpty();

                // LogUtils.d( DBG_TAG, "subscriptions left : " +
                // getSubscribed().size() );
                LogUtils.d(DBG_TAG, "subscriptions total       :  " + mSubscriptions.size());
                Iterator<String> i = mSubscriptions.keySet().iterator();
                while (i.hasNext()) {
                    String s = i.next();

                    LogUtils.d(DBG_TAG, s + mSubscriptions.get(s).toString());
                }

            } catch (InterruptedException ie) {

            }
        }
    }

    /**
     * Ustawienie listenera
     * 
     * @param presenceListener
     */
    public void setPresenceListener(SipPresenceListener presenceListener) {
        mPresenceListener = presenceListener;
    }

    /**
     * Metoda pomocnicza zwracajaca subskrypcje majace ten sam stan zapytania do
     * serwera
     * 
     * @param status
     * @param immediateFlag jesli true to zwracaj tylko te z ustawiona flaga
     *            immediate
     * @return
     */
    private Vector<String> getRequests(PresenceSubscriptionItem.RequestStatus status, boolean immediateFlag) {
        LogUtils.d(DBG_TAG, "getRequests " + status + " " + immediateFlag);

        Vector<String> returnValue = new Vector<String>();
        synchronized (mSubscriptions) {
            for (Map.Entry<String, PresenceSubscriptionItem> entry : mSubscriptions.entrySet()) {
                PresenceSubscriptionItem value = entry.getValue();
                String key = entry.getKey();
                LogUtils.d(DBG_TAG, "getRequests " + value + " " + key + " " + immediateFlag);
                if (immediateFlag) {
                    if (value.isImmediate()) {
                        returnValue.add(key);
                    }
                } else if (value != null && key != null && value.getRequestStatus() == status) {
                    returnValue.add(key);
                }
            }
        }
        return returnValue;
    }

    /**
     * Zwraca liste kontaktow ktore nie maja subskrypcji
     * 
     * @return
     */
    public Vector<String> getUnsubscribed() {
        return getRequests(RequestStatus.UNSUBSCRIBED, false);

    }

    /**
     * Zwraca liste kontaktow ktore maja byc subskrybowane
     * 
     * @return
     */
    public Vector<String> getSubscriptionRequested() {
        return getRequests(RequestStatus.SUBSCRIBE, false);

    }

    /**
     * Zwraca liste kontaktow ktore maja byc subskrybowane
     * 
     * @return
     */
    public Vector<String> getSubscriptionRequestedImmediate() {
        return getRequests(RequestStatus.SUBSCRIBE, true);

    }

    /**
     * Zwraca liste kontaktow ktorych zaptyanie o subskrypcje zostalo wyslane
     * 
     * @return
     */
    public Vector<String> getSubscriptionRequestDispatched() {
        return getRequests(RequestStatus.SUBSCRIBE_DISPATCHED, false);

    }

    /**
     * Zwraca liste kontaktow ktore sa subskrybowane
     * 
     * @return
     */
    public Vector<String> getSubscribed() {
        return getRequests(RequestStatus.SUBSCRIBED, false);

    }

    /**
     * Zwraca liste kontaktow ktorych subskrypcja ma byc usunieta
     * 
     * @return
     */
    public Vector<String> getUnsubscribeRequested() {
        return getRequests(RequestStatus.UNSUBSCRIBE, false);

    }

    /**
     * Zwraca liste kontaktow ktorych anulowanie subskrypcji zostalo wyslane
     * 
     * @return
     */
    public Vector<String> getUnsubscriptionRequestDistpatched() {
        return getRequests(RequestStatus.UNSUBSCRIBE_DISPATCHED, false);
    }

    /** Wywolywane w chwili wyslania subskrypcji */
    public void setSubscriptinRequestDispatched(String contact) {
        LogUtils.d("subscriptionjni", "sub requested " + contact);
        synchronized (mSubscriptions) {
            PresenceSubscriptionItem item = mSubscriptions.get(contact);
            if (item != null) {
                item.setRequestStatus(RequestStatus.SUBSCRIBE_DISPATCHED);
                item.setImmediate(false);
            } else {
                LogUtils.e(DBG_TAG, "setSubscriptionRequestDispatched error");
            }
        }
    }

    /** Wywolywane w chwili potwierdzenia subskrypcji */
    public void setSubscriptionRequestConfirmed(String contact) {

        LogUtils.d("subscriptionjni", "subscribed " + contact);
        synchronized (mSubscriptions) {
            // PresenceSubscriptionItem item = mSubscriptions.get(QilexService
            // .convertPublicAddressToDisplayedForm(new StringBuffer(contact)));
            // if (item != null) {
            // item.setRequestStatus(RequestStatus.SUBSCRIBED);
            // } else {
            // LogUtils.e(DBG_TAG, "setSubscriptionRequestDispatched error");
            // }
        }
    }

    /** Wywolywane w chwili wyslania anulowania subskrypcji */
    public void setUnsubscriptionRequestDispatched(String contact) {
        LogUtils.d("subscriptionjni", "unsub requested " + contact);
        synchronized (mSubscriptions) {
            PresenceSubscriptionItem item = mSubscriptions.get(contact);
            if (item != null) {
                item.setRequestStatus(RequestStatus.UNSUBSCRIBE_DISPATCHED);
            } else {
                LogUtils.e(DBG_TAG, "setUnsubscriptionRequested error");
            }
        }

    }

    /** Wywolywane w chwili zakonczenia subskrypcji */
    public void setUnsubscriptionRequestConfirmed(String contact) {
        LogUtils.d("subscriptionjni", "UNsubscribed " + contact);
        LogUtils.d(DBG_TAG, "subscriptionjni UNsubscribed " + contact);
        // PresenceSubscriptionItem item = mSubscriptions.get(QilexService
        // .convertPublicAddressToDisplayedForm(new StringBuffer(contact)));
        // if (item != null) {
        // if (item.getRequestStatus() == RequestStatus.UNSUBSCRIBE_DISPATCHED)
        // {
        // item.setRequestStatus(RequestStatus.UNSUBSCRIBED);
        // } else {
        // Vector<String> contactList = new Vector<String>();
        // contactList.add(QilexService.convertPublicAddressToDisplayedForm(new
        // StringBuffer(contact)));
        // lazySubscription(contactList);
        // }
        //
        // } else {
        // LogUtils.e(DBG_TAG, "setUnsubscriptionRequested error");
        // }
    }

    /** @see SipPresenceListener */
    public void onStatusUpdate(String contact, Status status) {
        try {
            LogUtils.d(DBG_TAG, "onStatusUpdate contact " + contact + " status " + status);
            PresenceSubscriptionItem item;
            // String contactVisibleForm =
            // QilexService.convertPublicAddressToDisplayedForm(new
            // StringBuffer(contact));
            // synchronized (mSubscriptions) {
            // item = mSubscriptions.get(contactVisibleForm);
            // if (item != null) {
            // item.setPresenceStatus(status);
            // }
            // }
            //
            // mPresenceListener.onStatusUpdate(contactVisibleForm, status);
        } catch (Throwable e) {
            LogUtils.e(DBG_TAG, "catch error", e);
        }
    }

    // TODO: to be removed
    public void notifyListener() {

        // for (Map.Entry<String, PresenceSubscriptionItem> entry :
        // mSubscriptions
        // .entrySet()) {
        // String key = entry.getKey();
        // PresenceSubscriptionItem value = entry.getValue();
        // if (key != null && value != null && mPresenceListener != null) {
        // mPresenceListener.onStatusUpdate(key, value.getPresenceStatus());
        // }
        // }
    }

    // /**
    // * Wysyla stan Presence dla wszystkich kontaktow
    // */
    // public void updateAllStatuses()
    // {
    // if (mPresenceListener!=null)
    // {
    // for (Map.Entry<String, PresenceSubscriptionItem> entry :
    // mSubscriptions.entrySet())
    // {
    // PresenceSubscriptionItem value = entry.getValue();
    // String key = entry.getKey();
    // mPresenceListener.onStatusUpdate(key, value.getPresenceStatus());
    // }
    // }
    //
    // }

    // /**
    // * unsubscribe thread
    // */
    // public void run()
    // {
    //
    // try {
    // while(!mSubscriptions.isEmpty())
    // {
    // Thread.sleep(1000);
    // long currentTime = System.currentTimeMillis();
    // long subscriptionTime = 100 * mSubscriptions.size();
    //
    // Iterator<String> iterator = mSubscriptions.keySet().iterator();
    // while(iterator.hasNext())
    // {
    // synchronized (mSubscriptions)
    // {
    // String key = iterator.next();
    // PresenceSubscriptionItem si = mSubscriptions.get(key);
    //
    // if (si.isReadyForUnsubscription())
    // {
    // mStatemachine.sendEvent( Events.EVENT_PRESENCEUNSUBSCRIBE );
    // }
    // }
    // }
    //
    //
    //
    // }
    //
    // } catch (InterruptedException e)
    // {
    // return;
    // }
    // }

    protected void dumpSubscriptions() {

        for (Map.Entry<String, PresenceSubscriptionItem> entry : mSubscriptions.entrySet()) {
            PresenceSubscriptionItem value = entry.getValue();
            String key = entry.getKey();

            LogUtils.d(DBG_TAG, key + " " + value.getRequestStatus() + " " + value.getPresenceStatus());

        }

    }

    public void renewAllSubscriptions() {
        LogUtils.d(DBG_TAG, "renewAllSubscriptions");
        unsubscribePresence(new Vector<String>(mSubscriptions.keySet()));
        lazySubscription(getUnsubscribed());
    }

}
