
package sigma.qilex.sip;

import android.util.Log;

import sigma.qilex.utils.LogUtils;

public abstract class SipCall {
    public interface SipCallListener {
        /**
         * Callback wywoływany gdy druga strona jest "Zajęta", odrzuca
         * połączenie, lub po prostu nie odebrała połączenia
         * 
         * @param call
         */
        public void onCallBusy(SipCall call);

        /**
         * Połączenie zakończone - wywoływane gdy połączenie zakończy się bez
         * błędów, możliwe sytuacje: - jedna ze stron kończy nawiązane
         * połączenie ( normalnie wciskając zakończ )
         * 
         * @param call
         */
        public void onCallEnded(SipCall call);

        /**
         * Połączenie zostalo nawiązane, można rozpocząć nadawanie i odbiór
         * audio
         * 
         * @param call
         */
        public void onCallEstablished(SipCall call);

        /**
         * @return duration current sipcall
         */
        public long getDuration();

        /**
         * Callback wywołany tuż po wysłaniu INVITE, nastepny może byc od razu
         * onCallBusy albo onCallRingingBack
         * 
         * @param call
         */
        public void onCallCalling(SipCall call);

        /**
         * Callback wywołany gdy dostaniemy odpowiedź "RINING" na INVITE. Czyli
         * u drugiego klienta dzwoni potem albo on odrzuci i bedzie onCallBusy
         * albo odbierze i będzie onCallEstablished albo po prostu nie odbierze
         * i dostaniemy po jakimś czasie błąd timeout nawiązania połączenia
         * 
         * @param call
         */
        public void onCallRingingBack(SipCall call);

        /**
         * @param call
         */
        public void onCallReconnect(SipCall call);

        /**
         * @param call
         */
        public void onCallReconnectSuccess(SipCall call);

        /**
         * Callback wywołany gdy wystapi jakiś błąd, kody błędów to stałe klasy
         * SipCall zaczynające się od ERR_..
         * 
         * @param call
         * @param errCode
         * @param errMsg
         */
        public void onCallError(SipCall call, int errCode, String errMsg);

        /**
         * Obiekt SipCall zakończyl życie, wywołane ZAWSZE po onCallEnded,
         * onCallBusy oraz onCallError
         * 
         * @param call
         */
        public void onCallClosed(SipCall call);

        /**
         * @param call
         */
        public void onTurnNotAvailable(SipCall call);

    }

    /**
     * Nieokreslony błąd, czyli każda sytuacja dla której nie mamy
     * zdefiniowanego specyficznego błędu
     */
    public static final int ERR_UNKNOWN = -1;

    /** Upłynął timeout na otrzymanie RINGING w odpowiedzi na INVITE */
    public static final int ERR_CALLING_TIMEOUT = -2;

    /**
     * Upłynął timeout na nawiązenie odbieranego połączenia, ściślej czas od
     * wywołania answerCall( TIMEOUT ), gdzie w podanym czasie TIMEOUT nie udało
     * się ustalić połączenia.
     */
    public static final int ERR_INCOMING_CALL_TIMEOUT = -3;

    /** Połączenie zostało utracone w czasie rozmowy */
    public static final int ERR_CONNECTION_LOST = -4;

    /** Połączenie niemożliwe z powodu nie znalezienia peer'a w sieci SIP */
    public static final int ERR_PEER_NOT_FOUND = -5;

    /** Kontakt nie istnieje lub jest Niedostępny */
    public static final int ERR_NOTCONNECTED = -6;

    public static final int ERR_CALLINGOUT_PAYMENT = -7;

    public static final int ERR_CALL_PAYMENT = -8;

    public static final int ERR_CALL_INVALID = -9;

    /** Stan na początku, powinien szybko zmienić sie w inny */
    public static final int STATE_INIT = 0;

    /**
     * Połaczenie przychodzące dzwoni, jest to stan początkowy dla połączenia
     * przychodzącego zwróconego przez listener połączeń przychodzących
     */
    public static final int STATE_INCOMING_CALL_RINGING = 1;

    /**
     * Dzwonimy - stan początkowy obiektu zwróconego przez SipManager.makeCall
     */
    public static final int STATE_CALLING = 2;

    /**
     * Dzwonimy i jest już odpowiedź od peer'a że dostał INVITE, w tym stanie
     * jeszcze nie wiadomo czy peer odbierze połączenie
     */
    public static final int STATE_CALLING_RINGING_BACK = 3;

    /** Połączenie nawiązane */
    public static final int STATE_CALL_ESTABLISHED = 4;

    /**
     * Otrzymano odpowiedź o zajętości peer'a ( odrzucił, lub miał rozmowe czyli
     * defacto tę nasza też odrzucił )
     */
    public static final int STATE_BUSY = 5;

    /** Połączenie zakończone */
    public static final int STATE_ENDED = 6;

    protected QilexSipProfile mLocalProfile, mPeerProfile;

    protected SipCallListener mListener;

    private int mState, mPrevState;

    private long mStartTimeMillis;

    private long mEndTimerMillis;

    private boolean isOutgoing;

    public SipCall(QilexSipProfile localProfile, QilexSipProfile peerProfile) {
        mLocalProfile = localProfile;
        mPeerProfile = peerProfile;
    }

    public long getStartTimeMillis() {
        return mStartTimeMillis;
    }

    public void setStartTimeMillis(long startTimeMillis) {
        this.mStartTimeMillis = startTimeMillis;
    }

    public long getEndTimerMillis() {
        return mEndTimerMillis;
    }

    public void setEndTimerMillis(long endTimerMillis) {
        this.mEndTimerMillis = endTimerMillis;
    }

    public boolean isOutgoing() {
        return isOutgoing;
    }

    public void setOutgoing(boolean isOutgoing) {
        this.isOutgoing = isOutgoing;
    }

    /**
     * @return stan obiektu czyli jedna ze stałych STATE_.. lub ERR_..
     */
    public final synchronized int getState() {
        return mState;
    }

    /**
     * @return poprzedni stan obiektu, czyli jedna ze stałych STATE_.. lub ERR_
     */
    public final synchronized int getPrevState() {
        return mPrevState;
    }

    /**
     * Metoda do ustawiania aktualnego stanu, zapisuje obecny stan jako
     * prevState i ustawia nowy
     * 
     * @param state
     */
    protected final synchronized void setState(int state) {

        LogUtils.d("TESTOWY", "mPrevState=" + String.valueOf(mState) + " new mState=" + String.valueOf(state));

        mPrevState = mState;
        mState = state;
    }

    /**
     * Odebranie połączenia przychodzącego, w czasie < timeout powinien wywołać
     * się callback onCallEstablished, jeśli nie to onCallError z kodem błędu
     * ERR_INCOMING_CALL_TIMEOUT
     * 
     * @param timeout czas w sekundach jaki dajemy na nawiązanie połączenia
     * @throws Exception jesli odebranie rozmowy nie powiedzie sie
     */
    public abstract void answerCall(int timeout) throws Exception;

    public abstract void pauseCall();

    public abstract void resumeCall();

    /**
     * Zakończenie połączenia w każdej z możliwych sytuacji, czyli oznacza: 1)
     * przerwanie nawiązanego połączenia 2) przerwanie dzwonienia jesli to my
     * dzwonimy 3) odrzucenie połączenia jesli to do nas dzownią
     */
    public abstract void endCall();

    /**
     * Wywoływane aby rozpocząć nadawanie i odbiór audio gdy mamy już nawiązane
     * połączenie SIP ( sesje ). W pierwszej wersji jest wywoływane w
     * onCallEstablished, jednak w poźniejszych gdy dojdzie "Zawieś połączenie"
     * będzie używane do wznowienia audio.
     */
    public abstract void startAudio();

    /**
     * @return true jeśli mikrofon jest wyciszony
     */
    public abstract boolean isMuted();

    /**
     * Wyłączenie/włączenie mikrofonu
     * 
     * @param muted jesli true to włączamy mikrofon, dla false wyłączamy
     */
    public abstract void setMute(boolean muted);

    /**
     * @return true jeśli rozmowa jest w trybie głośnomówiącym, false gdy na
     *         słuchawce.
     */
    public abstract boolean isSpeakerOn();

    /**
     * Przełączenie pomiędzy trybem głóśnomówiącym a normalnym słuchawkowym
     * 
     * @param speakerOn true jesli włączyć rozmowę na głośnik, false jeśli
     *            przełączyć na słuchawkę
     */
    public abstract void setSpeakerOn(boolean speakerOn);

    /**
     * Przełączenie pomiędzy trybem głóśnomówiącym a normalnym słuchawkowym
     * 
     * @param headphoneOn true jesli podpiete są słuchwaki
     */
    public abstract void setHeadphonesOn(boolean headphoneOn);

    /**
     * Przełączenie pomiędzy trybem głóśnomówiącym a normalnym słuchawkowym
     * 
     * @param headphoneOn true jesli podpiete są słuchwaki
     */
    public abstract void setAudioVolume(float volume);

    public abstract float audioQuality();

    /**
     * @return lokalny profil Sip dla tego połączenia
     */
    public QilexSipProfile getLocalProfile() {
        return mLocalProfile;
    }

    /**
     * @return profil peer'a dla tego połączenia ( do kogo dzwonimy lub kto
     *         dzwoni )
     */
    public QilexSipProfile getPeerProfile() {
        return mPeerProfile;
    }

    public void setListener(SipCallListener listener) {
        mListener = listener;
    }

    /**
     * Metoda zwracająca czas połączenia, czas mierzony jest od nawiązania
     * połączenia ( sesji )
     * 
     * @return czas połączenia w milisekundach lub -1 jeśli połączenie nie
     *         zostalo jeszcze nawiązane
     */
    public abstract long getCurrCallTimeMillis();
}
