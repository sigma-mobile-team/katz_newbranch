
package sigma.qilex.sip;

import java.util.Vector;

import android.content.Context;
import sigma.qilex.sip.resiprocate.SipManagerImpl;

/** Manager SIP */
public abstract class SipManager {
    private static SipManager mInstance;

    protected SipIncomingCallListener mIncomingCallListener;

    protected SipMessageListener mMessageListener;

    /** @return instancje SipManager */
    public static SipManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SipManagerImpl(context);
            // mInstance = new com.siroccomobile.sip.androsip.SipManagerImpl(
            // context );
        }
        return mInstance;
    }

    /**
     * Zarejestruj profil uzytkownika w SIP
     * 
     * @param profile
     * @param expiryTime
     * @param listener
     */
    public abstract void register(QilexSipProfile profile, int registrationExpiryTime, int publishRefreshTime,
            int subscribeRefreshTime, SipRegistrationListener listener, boolean withSipstackRestart);

    /**
     * Wyrejestruj profil uzytkownika z uslugi SIP
     * 
     * @param localProfile
     * @param listener
     */
    public abstract void unregister(QilexSipProfile localProfile, SipRegistrationListener listener);

    public abstract void makeCallWithoutTurn();

    /**
     * @param localProfile
     * @param peerProfile
     * @param callListener
     * @param timeoutSeconds
     * @return
     */
    public abstract SipCall makeCall(QilexSipProfile localProfile, QilexSipProfile peerProfile,
            SipCall.SipCallListener callListener, int timeoutSeconds);

    /**
     * Metoda ustawiająca Listenera dla połączeń przychodzacych.
     * 
     * @param listener
     */
    public void setIncomingCallListener(SipIncomingCallListener listener) {
        mIncomingCallListener = listener;
    }

    /**
     * Wyslanie wiadomosci
     * 
     * @param msg
     */
    public abstract void sendMessage(SipMessage msg);

    /**
     * Ustawienie Listenera dla wiadomosci
     * 
     * @param messageListener
     */
    public void setMessageListener(SipMessageListener messageListener) {
        mMessageListener = messageListener;
    }

    /**
     * Publikuje status uzytkownika
     * 
     * @param status
     */
    public abstract void publishPresence(SipPresenceListener.Status status);

    /**
     * Subskrybuje liste kontaktow w celu otrzymywania statusu presence
     * 
     * @param contactList
     * @param immediate flaga
     */
    public abstract void subscribePresence(Vector<String> contactList, boolean immediate);

    /**
     * Anluluje subskrypcje presence
     * 
     * @param contactList
     */
    public abstract void unsubscribePresence(Vector<String> contactList);

    /**
     * Ustawienie Listenera dla presence
     * 
     * @param presenceListener
     */
    public abstract void setPresenceListener(SipPresenceListener presenceListener);

    /**
     * Aktualizacja listy serverow TURN
     * 
     * @param servers - lista asdresow serverow TURN
     */
    public void updateTurnServers(Vector<String> servers) {
    }

    /**
     * Posprzątanie w bibliotece Sip, będzie wywoływane przy zakańczaniu
     * działania QilexService
     */
    public abstract void teardown();

    /**
     * Ustawienie listenera do debuglog
     * 
     * @param listener
     */
    public void setDebugLogListener(SipDebugLogListener listener) {
    }

    /**
     * Ustawienie listenera do debuglog
     * 
     * @param listener
     */
    public void resetPresence() {
    }

    public abstract void pingToAddress(String sipAddress);
}
