
package sigma.qilex.sip;

import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.Utils;
import android.util.Log;

public class SipMessage {
    public final static String DBG_TAG = "SipMessage";

    public static final String TYPE_MESSAGE_TEXT = "1001";

    public static final String TYPE_MESSAGE_IMAGE = "1002";

    public static final String TYPE_MESSAGE_LOCATION = "1003";

    public static final String TYPE_DELIVERED = "2001";

    public static final String TYPE_VIEWED = "2002";

    public static final String TYPE_COMPOSING = "2003";

    /**
     * @return the mId
     */
    public long getId() {
        return mId;
    }

    /**
     * @param mId the mId to set
     */
    public void setId(long mId) {
        this.mId = mId;
    }

    /**
     * @return the mFrom
     */
    public QilexSipProfile getFrom() {
        return mFrom;
    }

    /**
     * @param mFrom the mFrom to set
     */
    public void setFrom(QilexSipProfile mFrom) {
        this.mFrom = mFrom;
    }

    /**
     * @return the mTo
     */
    public QilexSipProfile getTo() {
        return mTo;
    }

    /**
     * @param mTo the mTo to set
     */
    public void setTo(QilexSipProfile mTo) {
        this.mTo = mTo;
    }

    /**
     * Konstruktor
     * 
     * @param mContent tresc wiadomosci
     */
    public SipMessage(String content) {
        mContent = content;
    }

    /**
     * Konstruktor
     * 
     * @param from nadawca
     * @param to odbiorca
     * @param id identyfikator wiadomosci
     * @param timestamp czas wyslania wiadomosci
     * @param content tresc wiadomosci
     */
    public SipMessage(String from, String to, String id, String timestamp, String content, String type) {

        try {
            mFrom = new QilexSipProfile(from);
            mTo = new QilexSipProfile(to);
            mId = Long.valueOf(id);
            mTimestamp = Long.valueOf(timestamp).longValue() * 1000;
            mContent = content;
            mType = type;
        } catch (Exception e) {
            LogUtils.e(DBG_TAG, e.toString());
        }
    }

    /**
     * @return the mContent
     */
    public String getContent() {
        return mContent;
    }

    /**
     * @param mContent the mContent to set
     */
    public void setContent(String mContent) {
        this.mContent = mContent;
    }

    /**
     * @return the mTimestamp
     */
    public long getTimestamp() {
        return mTimestamp;
    }

    /**
     * @param mTimestamp the mTimestamp to set
     */
    public void setTimestamp(long mTimestamp) {
        this.mTimestamp = mTimestamp;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getUuid() {
        return mUuid;
    }

    public void setUuid(String mUuid) {
        this.mUuid = mUuid;
    }

    /**
     * Nadawca wiadomosci
     */
    private QilexSipProfile mFrom;

    /**
     * Odbiorca wiadomosci
     */
    private QilexSipProfile mTo;

    /**
     * Identyfikator wiadomosci
     */
    private long mId;

    /**
     * Tresc wiadomosci
     */
    private String mContent;

    /**
     * Czas wyslania wiadomosci
     */
    private long mTimestamp;

    private String mType = "";

    private String mUuid = "";
    
    // Group Chat
    private String mGroupChatMsgType = null;
    
    private String mGroupChatUuid = null;
    
    private String mGroupChatParticipants = null;
    
    private String mGroupChatAdministrator = null;

	public String getGroupChatMsgType() {
		return mGroupChatMsgType;
	}

	public void setGroupChatMsgType(String groupChatMsgType) {
		this.mGroupChatMsgType = groupChatMsgType;
	}

	public String getGroupChatUuid() {
		return mGroupChatUuid;
	}

	public void setGroupChatUuid(String groupChatUuid) {
		this.mGroupChatUuid = groupChatUuid;
	}

	public String getGroupChatParticipants() {
		return mGroupChatParticipants;
	}

	public void setGroupChatParticipants(String groupChatParticipants) {
		this.mGroupChatParticipants = groupChatParticipants;
	}

	public String getGroupChatAdministrator() {
		return mGroupChatAdministrator;
	}

	public void setGroupChatAdministrator(String groupChatAdministrator) {
		this.mGroupChatAdministrator = groupChatAdministrator;
	}

	public boolean isGroupChatMessage() {
		return !Utils.isStringNullOrEmpty(mGroupChatUuid);
	}
	
	public static interface GroupChatType {
		public final String INVITE = "3000";
		
		public final String KICK = "3001";
		
		public final String STATUS = "3002";
		
		public final String MESSAGE = "3003";
	}
	// Group Chat
}
