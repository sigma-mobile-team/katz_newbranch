
package sigma.qilex.sip;

/**
 * Interface Listenera powiadamianego o przychodzących połączeniach Sip
 * 
 * @author roofiox
 */
public interface SipIncomingCallListener {
    /**
     * Callback wywołany w momencie nadejścia połączenia, wywołany dokładnie raz
     * dala każdego połączenia przychodzącego.
     * 
     * @param call
     */
    public void incomingCall(SipCall call);
}
