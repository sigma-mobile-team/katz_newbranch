
package sigma.qilex.sip;

import java.util.Vector;

import pl.katz.aero2.Const;

/**
 * Profil SIP zawiera dane pozwalające zarejestrować użytkownika do JEDNEGO
 * serwera Sip.
 */
public class QilexSipProfile {

    public QilexSipProfile() {

    }

    public QilexSipProfile(String profileUri) throws Exception {
        if (profileUri.startsWith("tel:"))
            profileUri = profileUri.substring(4);
        if (profileUri.startsWith("sip:"))
            profileUri = profileUri.substring(4);
        if (profileUri.startsWith("sips:"))
            profileUri = profileUri.substring(5);
        String[] split = profileUri.split("@");
        mLogin = split[0];
        mServer = split[1];
    }

    public final String getDisplayedName() {
        return mDisplayedName;
    }

    public final void setDisplayedName(String displayedName) {
        this.mDisplayedName = displayedName;
    }

    public final String getLogin() {
        return mLogin;
    }

    public final void setLogin(String mLogin) {
        this.mLogin = mLogin;
    }

    public final Vector<String> getAliases() {
        return mAliases;
    }

    public final void setAliases(Vector<String> aliases) {
        if (aliases == null) {
            mAliases = null;
        } else {
            mAliases = new Vector<String>(aliases);
        }
    }

    public final String getPassword() {
        return mPassword;
    }

    public final void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public final String getServer() {
        return mServer;
    }

    public final void setServer(String mServer) {
        this.mServer = mServer;
    }

    public final String getOutboundProxy() {
        return mOutboundProxy;
    }

    public final void setOutboundProxy(String mOutboundProxy) {
        this.mOutboundProxy = mOutboundProxy;
    }

    public final int getPort() {
        return mPort;
    }

    public final void setPort(int mPort) {
        this.mPort = mPort;
    }

    public final String getInstanceId() {
        return mInstanceId;
    }

    public final void setInstanceId(String instanceId) {
        mInstanceId = instanceId;
    }

    public final void setToken(String token) {
        mToken = token;
    }

    public final String getToken() {
        return mToken;
    }

    public final String getCertificate() {
        return mCertificate;
    }

    public final void setCertificate(String cert) {
        this.mCertificate = cert;
    }

    public final String getPrivateKey() {
        return mPrivateKey;
    }

    public final void setPrivateKey(String key) {
        this.mPrivateKey = key;
    }

    public final String getPublicKey() {
        return mPublicKey;
    }

    public final void setPublicKey(String key) {
        this.mPublicKey = key;
    }

    @Override
    public String toString() {
        return getUriString();
    }

    public String getUriString() {
        return String.format("sip:%s@sip.ct.plus.pl", mLogin);
    }

    public String getUriSSLString() {
        return String.format("sips:%s@sip.ct.plus.pl", mLogin);
    }

    private String getOUTUriString() {
        return String.format("tel:%s", mLogin);
    }

    public void setIsOUt(boolean isOUT) {
        mIsOut = isOUT;
    }

    public boolean isOUT() {
        return mIsOut;
    }

    public String getURI() {
        if (mIsOut)
            return getOUTUriString();

        if (Const.SIP_USE_SIPS) {
            return getUriSSLString();
        } else
            return getUriString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mLogin == null) ? 0 : mLogin.hashCode());
        result = prime * result + ((mOutboundProxy == null) ? 0 : mOutboundProxy.hashCode());
        result = prime * result + ((mPassword == null) ? 0 : mPassword.hashCode());
        result = prime * result + mPort;
        result = prime * result + ((mInstanceId == null) ? 0 : mInstanceId.hashCode());
        result = prime * result + ((mServer == null) ? 0 : mServer.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof QilexSipProfile))
            return false;
        QilexSipProfile other = (QilexSipProfile)obj;
        if (mLogin == null) {
            if (other.mLogin != null)
                return false;
        } else if (!mLogin.equals(other.mLogin))
            return false;
        if (mOutboundProxy == null) {
            if (other.mOutboundProxy != null)
                return false;
        } else if (!mOutboundProxy.equals(other.mOutboundProxy))
            return false;
        if (mPassword == null) {
            if (other.mPassword != null)
                return false;
        } else if (!mPassword.equals(other.mPassword))
            return false;
        if (mPort != other.mPort)
            return false;
        if (mServer == null) {
            if (other.mServer != null)
                return false;
        } else if (!mServer.equals(other.mServer))
            return false;

        if (mInstanceId == null) {
            if (other.mInstanceId != null)
                return false;
        } else if (!mInstanceId.equals(other.mInstanceId))
            return false;
        return true;
    }

    private boolean mIsOut = false;

    /** Wyswietlana nazwa uzytkownika */
    private String mDisplayedName;

    /** Nazwa uzytkownika */
    private String mLogin;

    /** Aliasy */
    private Vector<String> mAliases;

    /** Haslo uzytkownia */
    private String mPassword;

    /** Domena uzytkownika */
    private String mServer;

    /** OutboundProxy - używane przez niektóre usługi ( np. Sip2Sip ) */
    private String mOutboundProxy;

    /** port */
    private int mPort = -1;

    /** instance id */
    private String mInstanceId;

    private String mToken = "ruq5Vgn2DL8603b2bfe082c0c704f412c38ce07e88e1865a66";

    /** certyfikat */
    private String mCertificate = "";

    /** klucz prywatmy */
    private String mPrivateKey = "";

    /** klucz publiczny */
    private String mPublicKey = "";
}
