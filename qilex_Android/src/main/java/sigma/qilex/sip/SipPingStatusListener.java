
package sigma.qilex.sip;

public interface SipPingStatusListener {

    public static final int STATUS_OFFLINE = 0;

    public static final int STATUS_ONLINE = 1;

    public void onPingResult(String from, String to, int status, int offlineMinute);

    public void onReceiveRequest(String from);
}
