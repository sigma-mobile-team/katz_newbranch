
package sigma.qilex.sip;

public interface SipRegistrationListener {

    /**
     * Wywołane po rozpoczęciu rejestracji, równoznaczne z tym że request
     * rejestgracji poszedl do serwer'a. Wywołane raz dla każdej rejestracji.
     * 
     * @param localProfile profil na który się rejestrujemy
     */
    public void onRegistering(QilexSipProfile localProfile);

    /**
     * Rejestracja zakończyła się pomyślnie. Wywołane raz dla każdej
     * rejestracji.
     * 
     * @param localProfile
     * @param expiryTime
     */
    public void onRegistrationDone(QilexSipProfile localProfile, long expiryTime);

    /**
     * Błąd rejestracji, wywołane zamiast RegistrationDone jeśli wystapił błąd.
     * Wywołane raz dla każdej rejestracji.
     * 
     * @param localProfile profil na który była rejestracja
     * @param errorCode kod błędu, jedna ze stałych ERR_
     * @param errorMessage opis błędu - od tego już nie powinna zależeć w żaden
     *            sposób logika aplikacji, więc można tu przekezać cokolwiek
     *            zwróciła nam biblioteka SIP
     */
    public void onRegistrationFailed(QilexSipProfile localProfile, int errorCode, String errorMessage);

    /**
     * Callback o wyrejestrowaniu danego z serwera SIP, wywołany niezależnie czy
     * uda sie poprawnie wyrejestrować czy np. z powodu braku sieci nie uda sie
     * wysłac requestu wyrejestrowania. Callback jest również wywoływany gdy
     * utracimy łączność z serwerem Sip określonym w danym profilu
     * 
     * @param localProfile profil Sip z którego się wyrejestrowaliśmy
     */
    public void onProfileUnregistered(QilexSipProfile localProfile);

    /** w zasadzie to nie błąd więc ma id 0 - anulowano */
    public static final int ERR_CANCELED = 0;

    /** Jakiś błąd ( inny niż te które rozpoznajemy ) */
    public static final int ERR_UNKNOWN_ERROR = -1;

    /** Zły login lub haslo */
    public static final int ERR_BAD_ACCOUNT_DATA = -2;

    /** Problem z siecią, obejmuje także timeout'y */
    public static final int ERR_NETWORK_DOWN = -3;

    /**
     * Z jakichś przyczyn ( np. CP ) dany user ma zablokowane konto i nie może
     * korzystać z usługi
     */
    public static final int ERR_SERVICE_FORBIDDEN = -4;

    /**
     * Timeout - serwer nie odpowiedział w zadanym czasie, jest bardzo
     * prawdopodobne że np. mamy włączone WiFi ale połączenia z internetem nie
     * ma więc m.in w takim przypadku wystąpi ten błąd
     */
    public static final int ERR_TIMEOUT = -5;

}
