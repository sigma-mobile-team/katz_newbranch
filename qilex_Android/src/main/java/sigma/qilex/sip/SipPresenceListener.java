
package sigma.qilex.sip;

public interface SipPresenceListener {
    public enum Status {
        NOT_AVAILABLE, HIDDEN, BUSY, // not implmented
        AVAILABLE
    }

    /**
     * Aktualizacja statusu peera
     * 
     * @param uri
     * @param status
     */
    public void onStatusUpdate(String uri, Status status);
}
