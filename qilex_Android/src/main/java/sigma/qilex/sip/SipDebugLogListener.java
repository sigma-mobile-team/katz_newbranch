
package sigma.qilex.sip;

public interface SipDebugLogListener {

    public void onKeyValueUpdate(String key, String value);

    public void onLogMessage(String logMessage);
}
