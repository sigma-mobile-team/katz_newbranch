
package com.qilex.frifonout.payment;

import com.qilex.frifonout.payment.util.Purchase;

public interface PaymentListener {

    void onConsumeFinished(Purchase purchase);

    void onConsumeFail(Purchase purchase, int errorCode);
}
