
package com.qilex.frifonout.payment;

import java.util.ArrayList;

import android.content.Intent;

public interface IPayment {
    public static final String BASE64_ENCODED_PublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu9HZIfzGNCcaDrGIascMopdvbG8gKpb5Htw1Tw3V5xSpFW7abSLvbTAfospLdYgoEjvPS+T1AbEV1IScA/NhT+4Hn6VIq9C3BJSsXZ9wXkw5qA6klg9gQNsNvBqC5A6PXxd/pR0F6wPVJxNFlfRFEMqBS2QfhW1lSoVj6bzXh3Fja8lIXbZ6YzrVfYGHtvN8lyVKMAZJYYBTjwNFPdUC0zEP+RnksXNVI3041UzG8musM3XTyfI8+UQRVMgs1RQvIoU/eVurifdlWFXIRBf51XGwr2Nj0yvhieyBcHe58oXjnOIC4q5MNHWfNr+2hDv1taRaih8OIJNYAYBcN9ZOTwIDAQAB";

    // (arbitrary) request code for the purchase flow
    public static final int RC_REQUEST = 10001;

    public static final String DEVELOPER_PAYLOAD = "pay_for_frifon_out_balance";

    void setSkus(ArrayList<String> skus);

    void setPlaymentListener(PaymentListener listener);

    boolean handleActivityResult(int requestCode, int resultCode, Intent data);

    void release();

    void complain(String tag, String message);

    /**
     * @type: MANAGED PRODUCT
     * @note: SKUs for our products: the premium upgrade (consumable)
     * @param v
     */
    void buyManagedConsumable(String sku) throws IllegalStateException;

}
