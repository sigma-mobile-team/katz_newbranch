
package com.qilex.frifonout.payment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.qilex.frifonout.payment.util.IabHelper;
import com.qilex.frifonout.payment.util.IabResult;
import com.qilex.frifonout.payment.util.Inventory;
import com.qilex.frifonout.payment.util.Purchase;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.widget.Toast;
import pl.katz.aero2.Const;
import pl.frifon.aero2.R;
import sigma.qilex.utils.LogUtils;

public class PaymentImpl implements IPayment {

    private static boolean DEBUG = Const.DEBUG_MODE;

    private static final String TAG = "PAYMENT_OUT::PaymentImpl";

    private WeakReference<Activity> mActivityRef;

    private IabHelper mHelper;

    PaymentListener mPayListener;

    private ArrayList<String> mSkus = null;

    public PaymentImpl(Activity activity) {
        mActivityRef = new WeakReference<Activity>(activity);
    }

    @Override
    public void setSkus(ArrayList<String> skus) {
        mSkus = skus;
        initHelper();
    }

    @Override
    public void setPlaymentListener(PaymentListener listener) {
        mPayListener = listener;
    }

    public void initHelper() {
        if (mSkus == null)
            return;
        mHelper = new IabHelper(mActivityRef.get(), BASE64_ENCODED_PublicKey);
        mHelper.enableDebugLogging(DEBUG);
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        List<ResolveInfo> resInfos = mActivityRef.get().getPackageManager().queryIntentServices(serviceIntent, 0);
        if (resInfos == null) {
            Toast.makeText(mActivityRef.get(), R.string.google_playservice_not_install, Toast.LENGTH_SHORT).show();
            mHelper = null;
            return;
        }
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }
                if (mHelper == null)
                    return;
                mHelper.queryInventoryAsync(mReceivedInventoryListener);
            }
        });
    }

    // Listener that's called when we finish querying the items and
    // subscriptions we own
    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (mHelper == null)
                return;
            if (result.isFailure()) {
                // Handle failure
                complain("Failed to query inventory: " + result);
                return;
            }
            complain("Query inventory finished.");
            for (String sku : mSkus) {
                Purchase buyTimePurchase = inventory.getPurchase(sku);
                if (buyTimePurchase != null && verifyDeveloperPayload(buyTimePurchase))
                    mHelper.consumeAsync(buyTimePurchase, mConsumeFinishedListener);
            }

        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            complain("Purchase finished: " + result + ", purchase: " + purchase);
            if (mHelper == null)
                return;

            // User Cancel
            if (result.getResponse() == -1005) {
                if (mPayListener != null) {
                    mPayListener.onConsumeFail(purchase, -1005);
                }
            }

            if (result.isFailure()) {
                // Handle error
                if (result.getResponse() == 7) {
                    Toast.makeText(mActivityRef.get(), "Already Owned - OK", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }
            String sku = purchase.getSku();
            if (mSkus.contains(sku)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        @Override
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            complain("Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) {
                return;
            }
            if (result.isSuccess()) {
                // Setup function.
                String sku = purchase.getSku();
                if (mSkus.contains(sku)) {
                    if (mPayListener != null) {
                        mPayListener.onConsumeFinished(purchase);
                    }
                } else {
                    if (mPayListener != null) {
                        mPayListener.onConsumeFail(purchase, -1);
                    }
                }
            } else {
                if (mPayListener != null) {
                    mPayListener.onConsumeFail(purchase, -2);
                }
            }
        }
    };

    /** Verifies the developer payload of a purchase. */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
        complain("DeveloperPayload = " + payload);
        return DEVELOPER_PAYLOAD.equals(payload);
    }

    @Override
    public void release() {
        if (mHelper != null)
            mHelper.dispose();
        mHelper = null;
    }

    public void complain(String message) {
        complain(TAG, message);
    }

    // LOG
    @Override
    public void complain(String tag, String message) {
        LogUtils.e(tag, message);
    }

    @Override
    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null)
            return false;
        return mHelper.handleActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void buyManagedConsumable(String sku) throws IllegalStateException {
        mHelper.launchPurchaseFlow(mActivityRef.get(), sku, RC_REQUEST, mPurchaseFinishedListener, DEVELOPER_PAYLOAD);
    }

}
