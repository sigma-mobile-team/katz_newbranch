
package pl.katz.aero2;

import android.view.ContextThemeWrapper;

import sigma.qilex.utils.Utils;

public class Const {

    public static final String EMAIL_SUPPORT = "wsparcie.coretech@gmail.com";

    public static final boolean IS_FRIFON_LITE = true;
    /**
     * Old server:   https://app.katz.plus.pl/
     * Green server: https://app.katz.plus.pl/frifon_pktl/
     *
     * Change from staging to production server, please review class LoginResponModel, sip_server
     */
    // public static final String SERVER_URL ="https://10.230.73.5/";
//     public static final String SERVER_URL ="https://10.230.73.5/frifon_pktl/";
//    public static final String SERVER_URL = "https://app.katz.plus.pl/";
    public static final String SERVER_URL = "https://app.katz.plus.pl/frifon_pktl/";

    public static boolean DEBUG_MODE = false;

    public static boolean IS_OLD_VERSION = false;

    /**
     * https://developers.google.com/cloud-messaging/android/client Get from
     * test.qilex@gmail.com Project Katz-Android  716599163156
     * Frifon : 302751727813
     */
    public static String GCM_SENDER_ID = "302751727813";

    public static String FOLDER_NAME() {
        return Utils.isInKatzMode() ? "KATZ" : "Frifon";
    }

    // Fonts style
    public static final String BOLD = "bold";

    public static final String LIGHT = "light";

    public static final String MEDIUM = "medium";

    public static final String REGULAR = "regular";

    public static final String OPEN_SANS_REGULAR = "opensans_regular";

    // Basic Strings
    public static final String STR_EMPTY = "";

    public static final String STR_SPACE = " ";

    public static final String STR_COMMA = ",";

    public static final String STR_UNKNOWN = "Unknown";

    public static final String STR_PLUS = "+";

    public static final String STR_HTML_SPACE = "%20";

    public static final String STR_0 = "0";

    public static final boolean SIP_USE_SIPS = false;

    public class QilexRequestHeader {
        public static final String ACCEPT = "application/json";

        public static final String ACCEPT_ENCODING = "UTF-8";

        public static final String CONTENT_TYPE = "application/json; charset=UTF-8";

        public static final String USER_AGENT = "frifon2_android/1";
    }

    public class Config {
        public static final int NUMBER_CHAT_ITEM_LOAD_EACH_SCROLL = 15;

        public static final int OUTGOING_CALL_TIMEOUT_MILLIS = 40000;

        public static final String MONEY_FORMAT = "%s PLN";

        public static final int TIME_MILLIS_DELAY_RE_SYNC_CONTACT_FIRST_TIME = 10000;

        public static final int TIME_MILLIS_LOOPING_SYNCONTACT = 30000;

        public static final int TIME_MILLIS_DELAY_RELOGIN = 10000;

        public static final int TIME_MILLIS_DELAY_REREGISTER = 10000;

        public static final int TIME_MILLIS_DELAY_PING_CONTACT_STATUS = 30000;

        public static final int TIME_MILLIS_DELAY_SEND_IDLE = 6000;

        public static final String AVATAR_UPLOAD_FORMAT = "jpg";

        public static final int TIME_SECOND_SYNC_CONTACT_PERIOD = 60;

        public static final long TIME_MILLIS_MESSAGE_TIMEOUT = 180000;// 3min.

        /**
         * 1 week = 604.800.000
         */
        public static final long TIME_MILLIS_DURATION_CHECK_CONTACT = 604800000; // 1
                                                                                 // week

        public static final long TIME_MILLIS_DURATION_RECHECK_CONTACT = 86400000; // 1
                                                                                  // day
    }
}
