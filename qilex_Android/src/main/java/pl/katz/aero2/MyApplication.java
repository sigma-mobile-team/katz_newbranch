
package pl.katz.aero2;

//import io.fabric.sdk.android.Fabric;
import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;

//import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import pl.frifon.aero2.R;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import pl.katz.aero2.gcm.RegistrationIntentService;
import sigma.qilex.manager.ScreenReceiver;
import sigma.qilex.manager.contact.ContactManager;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.Utils;

/**
 * Initializing something at start application and prepare for get from
 * somewhere (context and sharedPreference)
 */
public class MyApplication extends Application {
	
	public static String STR_HAS_JOIN;
	
	public static String STR_HAS_LEFT;
	
	public static String STR_YOU_ARE_ADDED;
	
	public static String STR_YOU_ARE_KICKED;

    private static final String LOG_TAG = "MyApplication";

    private static Context mContext;

    private static SharedPreferences mSharedPreferences;

    private static ImageLoader imageLoader = ImageLoader.getInstance();

    public static boolean libraryLoaded = false;

    private static volatile boolean applicationInited = false;

    public static volatile Handler appHandler;

    public static volatile boolean mainInterfacePaused = true;

    public static volatile boolean isScreenOn = false;

    public static final String EXTRA_MESSAGE = "message";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final String BROADCAST_ACTION_NEED_PLAY_SERVICE = "BROADCAST_ACTION_NEED_PLAY_SERVICE";

    private static int appIconId = R.drawable.ic_launcher;

    static {
        LogUtils.d(LOG_TAG, "KATZ start - MyApplication static initializer");

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable e) {
                LogUtils.e(LOG_TAG, "UNCAUGHT EXCEPTION: ", e);

                if (Looper.myLooper() == Looper.getMainLooper()) // only for
                                                                 // main
                                                                 // thread
                                                                 // crashes
                    System.exit(-1);
            }
        });
    }

    /**
     * Loading jni file lib.
     */
    static {
        try {
            System.loadLibrary("freefonjni");
            libraryLoaded = true;
        } catch (UnsatisfiedLinkError ule) {
            LogUtils.e(LOG_TAG, "Can't load freefonjni library: " + ule);
            /// FIXME Alert user
            libraryLoaded = false;
        } catch (SecurityException e) {
            LogUtils.e(LOG_TAG, "Encountered a security issue when loading freefonjni library: " + e);
            /// FIXME Alert user
            libraryLoaded = false;
        }

    }

    public static boolean isLibraryLoaded() {
        return libraryLoaded;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        imageLoader.clearMemoryCache();
        imageLoader.clearDiskCache();
    }

    public static Context getAppContext() {
        return mContext;
    }

    public static Resources getAppResource() {
        return mContext.getResources();
    }

    /**
     * Get share preference for save some small data on device
     * 
     * @return
     */
    public static synchronized SharedPreferences getAppPreferences() {
        if (mContext == null)
            return null;

        if (mSharedPreferences == null)
            mSharedPreferences = mContext.getSharedPreferences("Qilex_frifon", MODE_PRIVATE);

        return mSharedPreferences;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Check for app version to clear data
        if (getApplicationInfo().theme == R.style.KatzAppThemeGreen) {
            SharedPreferences prefs = getSharedPreferences("APP_PREF", Context.MODE_PRIVATE);
            if (prefs.getBoolean("AppFirstTimeRun", true)) {
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    int verCode = pInfo.versionCode;
                    if (verCode == 16061315) {
                        clearApplicationData();
                    }
                    prefs.edit().putBoolean("AppFirstTimeRun", false).commit();
                } catch (PackageManager.NameNotFoundException e) { }
            }
        }

//        Fabric.with(this, new Crashlytics());
		mContext = this;
        appIconId = getApplicationInfo().theme == R.style.KatzAppThemeBlue ? R.drawable.ic_launcher : R.drawable.ic_launcher_green;
        appHandler = new Handler(mContext.getMainLooper());
//        configureStrictMode();
        initImageLoader(mContext);
        loadConstantResource();
    }

    public static int getAppIconId() {
        return appIconId;
    }

    @SuppressWarnings("deprecation")
    public static boolean isApplicationInBackground() {
        final ActivityManager am = (ActivityManager)getAppContext().getSystemService(Context.ACTIVITY_SERVICE);
        if (!Utils.hasLlAbove()) {
            // This code is very important to check if app is re-open or not.
            // Need confirm with PhucPV if change.
            try {
                final List<RunningTaskInfo> tasks = am.getRunningTasks(1);
                if (!tasks.isEmpty()) {
                    final ComponentName topActivity = tasks.get(0).topActivity;
                    return !topActivity.getPackageName().equals(getAppContext().getPackageName());
                }
            } catch (SecurityException e) {
                return false;
            }
        }
        return false;
    }

    public static void initApplication() {
        if (applicationInited) {
            return;
        }
        applicationInited = true;
        try {
            final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            final BroadcastReceiver mReceiver = new ScreenReceiver();
            mContext.registerReceiver(mReceiver, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MyApplication app = (MyApplication)MyApplication.mContext;
        app.initGCMServices();

        ContactManager.getInstance().addNewAccount();
    }

    /**
     * Configure strict mode for debug
     */
    private static void configureStrictMode() {
        // when you create a new application you can set the Thread and VM
        // Policy
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());

        StrictMode.ThreadPolicy.Builder builder = new StrictMode.ThreadPolicy.Builder().detectDiskReads()
                .detectDiskWrites().detectNetwork().penaltyLog();
        builder.detectCustomSlowCalls();
        StrictMode.setThreadPolicy(builder.build());

        // If you use StrictMode you might as well define a VM policy too
        try {
            StrictMode.setVmPolicy(
                    new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
                            // API level 11
                            .setClassInstanceLimit(Class.forName("sigma.qilex.ui.activity.IActivity"), 10).penaltyLog()
                            .build());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you
        // may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public static ImageLoader getImageLoader() {
        return imageLoader;
    }

    private void initGCMServices() {
        if (checkPlayServicesExist()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private boolean checkPlayServicesExist() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int errorCheck = api.isGooglePlayServicesAvailable(this);
        if (errorCheck == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(errorCheck)) {
            if (mContext != null) {
                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mContext.sendBroadcast(new Intent(BROADCAST_ACTION_NEED_PLAY_SERVICE));
                    }
                }, 2000);
            }
            return false;
        } else {
            return false;
        }
    }
    
    private void loadConstantResource() {
        STR_HAS_LEFT = " " + getResources().getString(R.string.contact_is_kick);
        STR_HAS_JOIN = " " + getResources().getString(R.string.contact_is_join);
        STR_YOU_ARE_ADDED = getResources().getString(R.string.you_are_added_to_group);
        STR_YOU_ARE_KICKED = getResources().getString(R.string.you_are_kiced_from_group);
    }

    public void clearApplicationData() {
        // Clear application file
        File cacheDirectory = getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    private static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }
        return deletedAll;
    }
}
