
package pl.katz.aero2;

import java.util.Stack;

import android.app.ActivityManager;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;

public class AppManager {
    private static Stack<ActionBarActivity> activityStack;

    private static AppManager instance;

    private AppManager() {
    }

    public static AppManager getAppManager() {
        if (instance == null) {
            instance = new AppManager();
        }
        return instance;
    }

    public synchronized void addActivity(ActionBarActivity activity) {
        if (activityStack == null) {
            activityStack = new Stack<ActionBarActivity>();
        }
        activityStack.add(activity);
    }

    public synchronized ActionBarActivity currentActivity() {
        ActionBarActivity activity = activityStack.lastElement();
        return activity;
    }

    public synchronized void finishActivity() {
        ActionBarActivity activity = activityStack.lastElement();
        if (activity != null) {
            activity.finish();
            activity = null;
        }
    }

    public synchronized void removeActivity(ActionBarActivity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity = null;
        }
    }

    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    public synchronized void appExit(Context context) {
        try {
            finishAllActivity();
            ActivityManager activityMgr = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
            activityMgr.killBackgroundProcesses(context.getPackageName());
            System.exit(0);
        } catch (Exception e) {
        }
    }
}
