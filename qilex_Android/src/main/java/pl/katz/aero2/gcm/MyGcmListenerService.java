/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.katz.aero2.gcm;

import com.google.android.gms.gcm.GcmListenerService;

import android.content.Intent;
import android.os.Bundle;
import sigma.qilex.ui.activity.BaseActivity;
import sigma.qilex.ui.service.QilexService;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;

public class MyGcmListenerService extends GcmListenerService {

    public static final String KEY_EVENT_TYPE = "event_type";

    public static final String EVENT_TYPE_LOGOUT = "logout";

    public static final String EVENT_TYPE_RELOGIN = "relogin";

    public static final String EVENT_TYPE_CALL = "call";

    public static final String EVENT_TYPE_MESSAGE = "message";

    public static final String EVENT_TYPE_CONTACT_REFRESH = "refresh";

    /**
     * Called when message is received.
     * 
     * @param from SenderID of the sender.
     * @param ex Data bundle containing message data as key/value pairs. For
     *            Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle ex) {
        LogUtils.d("gcm", "GCM Message push coming");
        // If application is logged out, do nothing
        if (SharedPrefrerenceFactory.getInstance().getLoggoutStatus() == true) {
            return;
        }

        String eventType = ex.getString(KEY_EVENT_TYPE);
        if (eventType == null) {
            LogUtils.d("gcm", "onMessageReceived eventType null");
            Intent intent = new Intent(getApplicationContext(), QilexService.class);
            getApplicationContext().startService(intent);
            return;
        }
        LogUtils.d("gcm", "onMessageReceived eventType: " + eventType);
        if (EVENT_TYPE_LOGOUT.equals(eventType)) {
            // When app is logged out in other devices, or kick out by other
            // master device
            if (BaseActivity.FLAG_WAITING_FOR_SELF_LOGOUT == false) {
                if (BaseActivity.getService() != null) {
                    BaseActivity.getService().getNotificationFactory().showAppLogoutNotification();
                    BaseActivity.getService().userDeactivate(false, true);
                } else {
                    Intent intent = new Intent(getApplicationContext(), QilexService.class);
                    getApplicationContext().startService(intent);
                }
            }
        } else if (EVENT_TYPE_CALL.equals(eventType)) {
            // When there is a phone call
            LogUtils.d("binhdt", "onMessageReceived frifon_from: " + ex.getString("frifon_from"));
            Intent intent = new Intent(getApplicationContext(), QilexService.class);
            intent.putExtra(QilexService.INTENT_PARAM_ACTION,
                    QilexService.ActionEnum.ACTION_OPEN_INCOMING_CALL_FROM_PUSH);
            intent.putExtra(QilexService.INTENT_PARAM_PEER_NUMBER, ex.getString("frifon_from"));
            getApplicationContext().startService(intent);
        } else if (EVENT_TYPE_MESSAGE.equals(eventType)) {
            // When there is an incoming message
            LogUtils.d("binhdt", "onMessageReceived frifon_from: " + ex.getString("frifon_from"));
            Intent intent = new Intent(getApplicationContext(), QilexService.class);
            intent.putExtra(QilexService.INTENT_PARAM_PEER_NUMBER, ex.getString("frifon_from"));
            getApplicationContext().startService(intent);
        } else if (EVENT_TYPE_CONTACT_REFRESH.equals(eventType)) {
            // When there is a contact change
            LogUtils.d("binhdt", "onMessageReceived contact_id: " + ex.getString("contact_id"));
            Intent intent = new Intent(getApplicationContext(), QilexService.class);
            intent.putExtra(QilexService.INTENT_PARAM_ACTION,
                    QilexService.ActionEnum.ACTION_GET_CONTACT_INFO_FROM_SERVER);
            intent.putExtra(QilexService.INTENT_PARAM_SERVER_ID, ex.getString("contact_id"));
            getApplicationContext().startService(intent);
        } else if (EVENT_TYPE_RELOGIN.equals(eventType)) {
            sendBroadcast(new Intent(QilexService.BROADCAST_ACTION_RELOGIN));
        }
    }
}
