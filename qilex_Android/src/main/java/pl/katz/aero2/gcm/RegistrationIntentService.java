/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.katz.aero2.gcm;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import android.app.IntentService;
import android.content.Intent;
import pl.katz.aero2.Const;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public static RegisterGcmListener mRegisterGcmListener;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            // In the (unlikely) event that multiple refresh operations occur
            // simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {
                // [START register_for_gcm]
                // Initially this call goes out to the network to retrieve the
                // token, subsequent calls
                // are local.
                // [START get_token]
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(Const.GCM_SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                // [END get_token]
                LogUtils.d(TAG, "GCM Registration Token: " + token);

                if (null != mRegisterGcmListener) {
                    mRegisterGcmListener.onGcmRegistered(token);
                }
                SharedPrefrerenceFactory.getInstance().storeRegistrationId(token);
                // TODO: Implement this method to send any registration to your
                // app's servers.
                sendRegistrationToServer(token);

                // You should store a boolean that indicates whether the
                // generated token has been
                // sent to your server. If the boolean is false, send the token
                // to your server,
                // otherwise your server should have already received the token.
                // sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER,
                // true).apply();
                // [END register_for_gcm]
            }
        } catch (Exception e) {
            LogUtils.e(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating
            // our registration data
            // on a third-party server, this ensures that we'll attempt the
            // update at a later time.
            // sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER,
            // false).apply();
            if (null != mRegisterGcmListener) {
                mRegisterGcmListener.onGcmRegisteredFail();
            }

            SharedPrefrerenceFactory.getInstance().setStatusSentTokenToServer(false);
        }
    }

    private void sendRegistrationToServer(String token) {

        SharedPrefrerenceFactory.getInstance().setStatusSentTokenToServer(true);
    }

    /**
     * Handler event when Gcm is registered finish.
     */
    public interface RegisterGcmListener {
        public void onGcmRegistered(String registrationId);

        public void onGcmRegisteredFail();
    }

}
