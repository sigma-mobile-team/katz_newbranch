
package pl.katz.aero2;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.util.Locale;

import sigma.qilex.dataaccess.model.http.UserInfoResponseModel;
import sigma.qilex.manager.account.QilexProfile;
import sigma.qilex.utils.LogUtils;
import sigma.qilex.utils.SharedPrefrerenceFactory;
import sigma.qilex.utils.Utils;
import sigma.qilex.utils.Zip;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.provider.Settings.Secure;

public class UserInfo {

    public static final String LANGUAGE_ENG_ISO = "eng";

    public static final String LANGUAGE_POL_ISO = "pol";

    public static final String LANGUAGE_ENG_CODE = "en";

    public static final String LANGUAGE_POL_CODE = "pl";

    private static UserInfo instance;

    public static UserInfo getInstance() {
        if (instance == null) {
            instance = new UserInfo();
        }
        return instance;
    }

    private String mFirstName;

    private String mLastName;

    private String mAvatarUrl;

    private String mPhoneNumber;

    private String mCountryCode;
    
    private String mPhoneNumberFormatted;

    private String mActivationCode;

    private String mPushToken = "";

    private QilexProfile mLocalProfile;

    public double accountBalance = -1;

    public String deviceId;

    public String sigKey;

    public boolean plusPromotionCalls;

    public String mCachePhoneNo;

    private UserInfo() {
        // Load data from SharePreference.
        loadInfo();
        mLocalProfile = createQilexProfile(MyApplication.getAppContext());
        deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);

        try {
            Signature[] sigs = MyApplication.getAppContext().getPackageManager()
                    .getPackageInfo(MyApplication.getAppContext().getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(sigs[0].toByteArray());
            sigKey = Zip.encodeBase64(md.digest());
            LogUtils.d("KEYHASH", "KEYHASH BASE64: " + sigKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearInfoAndSave() {
        mFirstName = Const.STR_EMPTY;
        mLastName = Const.STR_EMPTY;
        mAvatarUrl = Const.STR_EMPTY;
        mPhoneNumber = Const.STR_EMPTY;
        mCountryCode = Const.STR_EMPTY;
        mActivationCode = Const.STR_EMPTY;
        mPushToken = Const.STR_EMPTY;
        saveInfo();
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }

    public boolean hasAvatar() {
        return !Utils.isStringNullOrEmpty(mAvatarUrl);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber;
        this.mPhoneNumberFormatted = "+" + (mCountryCode + mPhoneNumber);
        this.mCachePhoneNo = phoneNumber;
    }

    public void setCountryCode(String countryCode) {
        this.mCountryCode = countryCode;
        this.mPhoneNumberFormatted = "+" + (mCountryCode + mPhoneNumber);
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public String getPhoneNumberFormatted() {
        return mPhoneNumberFormatted;
    }

    public QilexProfile getLocalProfile() {
        return mLocalProfile;
    }

    public void setLocalProfile(QilexProfile localProfile) {
        this.mLocalProfile = localProfile;
    }

    public void setActivationCode(String activationCode) {
        mActivationCode = activationCode;
    }

    public String getActivationCode() {
        return mActivationCode;
    }

    public void setPushToken(String pushToken) {
        mPushToken = pushToken;
    }

    public String getPushToken() {
        return mPushToken;
    }

    public void applyInfo(UserInfoResponseModel userInfoModel) {
        mAvatarUrl = userInfoModel.user_avatar_uri;
        mFirstName = userInfoModel.user_name;
        mLastName = userInfoModel.user_last_name;
    }

    public String getDisplayName() {
        String output = "";
        if (Utils.isStringNullOrEmpty(mFirstName) == false) {
            output += mFirstName;
            if (Utils.isStringNullOrEmpty(mLastName) == false) {
                output += " " + mLastName;
            }
        } else {
            output = mLastName;
        }
        return output;
    }

    public Bitmap getAvatarBitmap() {
        if (Utils.isStringNullOrEmpty(mAvatarUrl)) {
            return null;
        }
        Bitmap bmp = MyApplication.getImageLoader().loadImageSync(mAvatarUrl);
        return bmp;
    }

    public byte[] getAvatarBytesJpg() {
        if (Utils.isStringNullOrEmpty(mAvatarUrl)) {
            return null;
        }
        Bitmap bmp = MyApplication.getImageLoader().loadImageSync(mAvatarUrl);
        if (bmp == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /**
     * Load all info from Device's Content Provider.
     */
    public void loadInfo() {
        SharedPrefrerenceFactory sharedPrefrerenceFactory = SharedPrefrerenceFactory.getInstance();
        mFirstName = sharedPrefrerenceFactory.getUserInfoFirstName();
        mLastName = sharedPrefrerenceFactory.getUserInfoLastName();
        mAvatarUrl = sharedPrefrerenceFactory.getAvatarUrl();
        mPhoneNumber = sharedPrefrerenceFactory.getUserInfoPhoneNo();
        mCountryCode = sharedPrefrerenceFactory.getUserInfoCountryCode();
        mActivationCode = sharedPrefrerenceFactory.getUserInfoActivationCode();
        mPhoneNumberFormatted = "+" + (mCountryCode + mPhoneNumber);
        mCachePhoneNo = sharedPrefrerenceFactory.loadCachePhoneNo();
    }

    /**
     * Save all info to Device's Content Provider.
     */
    public void saveInfo() {
        SharedPrefrerenceFactory sharedPrefrerenceFactory = SharedPrefrerenceFactory.getInstance();
        sharedPrefrerenceFactory.saveUserInfoFirstName(mFirstName);
        sharedPrefrerenceFactory.saveUserInfoLastName(mLastName);
        sharedPrefrerenceFactory.saveAvatarUrl(mAvatarUrl);
        sharedPrefrerenceFactory.saveUserInfoPhoneNo(mPhoneNumber);
        sharedPrefrerenceFactory.saveUserInfoCountryCode(mCountryCode);
        sharedPrefrerenceFactory.saveUserInfoActivationCode(mActivationCode);
        sharedPrefrerenceFactory.saveCachePhoneNo(mCachePhoneNo);
    }

    public boolean hasPhoneNumber() {
        return !Utils.isStringNullOrEmpty(getPhoneNumber());
    }

    public boolean hasActiveCode() {
        return !Utils.isStringNullOrEmpty(mActivationCode);
    }

    public String getAcceptedLanguage() {
        String language = Locale.getDefault().getISO3Language();
        if (LANGUAGE_ENG_ISO.equals(language)) {
            return LANGUAGE_ENG_CODE;
        } else {
            return LANGUAGE_POL_CODE;
        }
    }

    private QilexProfile createQilexProfile(Context context) {
        QilexProfile profile = new QilexProfile(context);
        profile.setPassword(Const.STR_EMPTY);
        profile.setLogin(mPhoneNumberFormatted);
        return profile;
    }

    public boolean isActive() {
        return hasPhoneNumber() && hasActiveCode();
    }
}
